#!/bin/bash
# -----------------------------------------------------------------------------
# Build & upload the conda package for Linux & MacOSX.
# -----------------------------------------------------------------------------
set -e

# Ensure that `conda` is installed and accessible before running the script:
if ! command -v conda &>/dev/null; then
    echo "Conda is not installed or not in PATH. Exiting."
    exit 1
fi

# Activate the 'base' environment:
conda activate base

# Build the package:
if ! conda build recipe/. -c conda-forge -c morpheme -c mosaic; then
    echo "Build failed! Exiting."
    exit 1
fi

CONDA_BLD_PATH="$(conda info --base)/conda-bld/noarch"

anaconda upload "${CONDA_BLD_PATH}/timagetk-3*" --user mosaic --force

# Clean up:
conda build purge-all

if [[ "${CONDA_BLD_PATH}" == *"conda-bld/noarch"* ]]; then
   rm -rf "$CONDA_BLD_PATH"
else
   echo "Unexpected path for conda-bld artifacts: $CONDA_BLD_PATH"
   exit 1
fi