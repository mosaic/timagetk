import unittest

import numpy as np
from timagetk import SpatialImage
from timagetk.components.labelled_image import LabelledImage
from timagetk.io.dataset import shared_data
from timagetk.third_party.plantseg import DEF_DEVICE
from timagetk.third_party.plantseg import unet_predict
from timagetk.third_party.plantseg import unet_tiled_predict
from timagetk.third_party.plantseg import multicut_segmentation

# Define constants for testing purposes
CONSTANTS = {
    "segmentation_params": {
        "threshold": 0.5,
        "sigma_seeds": 1.0,
        "sigma_weights": 2.0,
        "min_size": 100,
        "alpha": 0.5,
        "apply_nonmax_suppression": False,
        "beta": 0.5,
        "post_minsize": 100
    },
    "expected_labels": {1: 1, 2: 2},
    "expected_dtype": "uint8"
}

class TestUnetPredict(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        """Set up the test environment before each test starts."""
        cls.image = shared_data('synthetic', 'wall')
        cls.model_name = 'confocal_3D_unet_sa_meristem_cells'
        cls.device = DEF_DEVICE

    def test_unet_predict_invalid_pacth(self):
        """Test that the function raises an exception when patch list argument is not valid."""
        invalid_patches = [None, "abc", {}, 123]
        for patch in invalid_patches:
            with self.assertRaises(Exception):
                unet_predict(self.image, model_name=self.model_name, device=self.device, patch=patch)

    def test_unet_predict_return(self):
        """Test that the function returns expected type object when called with no optional arguments."""
        result = unet_predict(self.image)
        self.assertTrue(isinstance(result, SpatialImage))
        self.assertIsNotNone(result)

    def test_unet_predict_valid_output(self):
        """Test whether the output of the function is valid."""
        pred_img = unet_predict(self.image, model_name=self.model_name, device=self.device)
        self.assertEqual(pred_img.shape, self.image.shape)
        self.assertEqual(pred_img.dtype, self.image.dtype)

    def test_unet_predict_invalid_model_name(self):
        """Test that the function raises an exception when the model_name argument is invalid."""
        invalid_model_name = "non_existent_model"
        with self.assertRaises(Exception):
            unet_predict(self.image, model_name=invalid_model_name, device=self.device)


class TestUnetTiledPredict(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        """Set up the test environment before each test starts."""
        cls.image = shared_data('synthetic', 'wall')
        cls.model_name = 'confocal_3D_unet_sa_meristem_cells'
        cls.device = DEF_DEVICE

    def test_unet_tiled_predict_invalid_pacth(self):
        """Test that the function raises an exception when patch list argument is not valid."""
        invalid_patches = [None, "abc", {}, 123]
        for patch in invalid_patches:
            with self.assertRaises(Exception):
                unet_tiled_predict(self.image, model_name=self.model_name, device=self.device, patch=patch)

    def test_unet_predict_return(self):
        """Test that the function returns expected type object when called with no optional arguments."""
        result = unet_predict(self.image)
        self.assertTrue(isinstance(result, SpatialImage))
        self.assertIsNotNone(result)

    def test_unet_tiled_predict_valid_output(self):
        """Test whether the output of the function is valid."""
        pred_img = unet_tiled_predict(self.image, model_name=self.model_name, device=self.device)
        self.assertEqual(pred_img.shape, self.image.shape)
        self.assertEqual(pred_img.dtype, self.image.dtype)

    def test_unet_tiled_predict_invalid_model_name(self):
        """Test that the function raises an exception when the model_name argument is invalid."""
        invalid_model_name = "non_existent_model"
        with self.assertRaises(Exception):
            unet_tiled_predict(self.image, model_name=invalid_model_name, device=self.device)


class TestMulticutSegmentation(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        """Set up the test environment before each test starts."""
        cls.image = shared_data('synthetic', 'wall')

    def test_multicut_segmentation(self):
        # use a dummy image for testing purposes

        expected_segmented_image = multicut_segmentation(self.image, **CONSTANTS["segmentation_params"])
        expected_segments_labels = list(np.unique(expected_segmented_image))
        expected_segments_dtype = str(expected_segmented_image.dtype)

        # Run the multicut_segmentation function
        segmented_image = multicut_segmentation(self.image, **CONSTANTS["segmentation_params"])

        self.assertIsInstance(segmented_image, LabelledImage,
                              "Result should be instance of LabelledImage")
        self.assertListEqual(list(np.unique(segmented_image)), expected_segments_labels,
                             "Segments labels are wrong")
        self.assertEqual(str(segmented_image.dtype), expected_segments_dtype,
                         "Dtype of segmented image is wrong")


if __name__ == '__main__':
    unittest.main()
