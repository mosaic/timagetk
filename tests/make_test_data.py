#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

from timagetk.bin.synthetic_image import large_kwargs
from timagetk.bin.synthetic_image import large_nuclei_kwargs
from timagetk.bin.synthetic_image import large_wall_kwargs
from timagetk.bin.synthetic_image import make_images
from timagetk.bin.synthetic_image import medium_kwargs
from timagetk.bin.synthetic_image import medium_nuclei_kwargs
from timagetk.bin.synthetic_image import medium_wall_kwargs
from timagetk.bin.synthetic_image import small_kwargs
from timagetk.bin.synthetic_image import small_nuclei_kwargs
from timagetk.bin.synthetic_image import small_wall_kwargs
from timagetk.bin.synthetic_image import xsmall_kwargs
from timagetk.bin.synthetic_image import xsmall_nuclei_kwargs
from timagetk.bin.synthetic_image import xsmall_wall_kwargs


def get_synthetic_data(isize, itype):
    return eval(f"make_{isize}_images(itype)")


def make_xsmall_images(itype):
    return make_images(**{itype: True}, dtype="uint8",
                       **xsmall_kwargs, **xsmall_wall_kwargs, **xsmall_nuclei_kwargs)[itype]


def make_small_images(itype):
    return make_images(**{itype: True}, dtype="uint8",
                       **small_kwargs, **small_wall_kwargs, **small_nuclei_kwargs)[itype]


def make_medium_images(itype):
    return make_images(**{itype: True}, dtype="uint8",
                       **medium_kwargs, **medium_wall_kwargs, **medium_nuclei_kwargs)[itype]


def make_large_images(itype):
    return make_images(**{itype: True}, dtype="uint8",
                       **large_kwargs, **large_wall_kwargs, **large_nuclei_kwargs)[itype]
