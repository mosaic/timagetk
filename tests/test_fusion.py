# ------------------------------------------------------------------------------
#  Copyright (c) 2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import unittest

import networkx as nx
import numpy as np

from src.timagetk.tasks.fusion import initial_transformation_graph
from timagetk import Trsf
from timagetk.algorithms.pointmatching import pointmatching
from timagetk.algorithms.resample import resample
from timagetk.algorithms.trsf import create_trsf
from timagetk.components.spatial_image import SpatialImage
from timagetk.io.dataset import shared_data
from timagetk.tasks.fusion import check_all_nodes_connected_to_reference
from timagetk.tasks.fusion import fusion_on_reference
from timagetk.tasks.fusion import iterative_fusion


class TestInitialTransformationGraph(unittest.TestCase):

    def setUp(self):
        self.images = {ma_im_idx: shared_data('flower_multiangle', ma_im_idx) for ma_im_idx in range(3)}
        self.null_transformations = {
            (0, 1): None,
            (0, 2): None
        }
        self.identity_transformations = {
            (0, 1): create_trsf('identity'),
            (0, 2): create_trsf('identity')
        }

    def test_graph_nodes(self):
        # Test if nodes are created correctly
        graph = initial_transformation_graph(self.images, self.identity_transformations, ref_img_idx=0)
        self.assertIn(0, graph.nodes)
        self.assertIn(1, graph.nodes)
        self.assertIn(2, graph.nodes)
        self.assertEqual(graph.nodes[0]['name'], "090223-p58-flo-top.lsm")
        self.assertTrue(graph.nodes[0]['reference'])
        self.assertFalse(graph.nodes[1]['reference'])
        self.assertFalse(graph.nodes[2]['reference'])

    def test_empty_transformations(self):
        # Test when no transformations are provided
        graph = initial_transformation_graph(self.images, init_trsfs={})
        self.assertEqual(len(graph.edges), 0)

    def test_reference_image(self):
        # Test when a different reference image is set
        graph = initial_transformation_graph(self.images, self.null_transformations, ref_img_idx=1)
        self.assertTrue(graph.nodes[1]['reference'])
        self.assertFalse(graph.nodes[0]['reference'])
        self.assertFalse(graph.nodes[2]['reference'])


class TestCheckAllNodesConnectedToReference(unittest.TestCase):

    def test_all_nodes_connected(self):
        trsf_graph = nx.DiGraph()
        trsf_graph.add_node(0, reference=True)
        trsf_graph.add_nodes_from([1, 2, 3])
        trsf_graph.add_edges_from([(1, 0), (2, 0), (3, 0)])
        self.assertTrue(check_all_nodes_connected_to_reference(trsf_graph))

    def test_not_all_nodes_connected(self):
        trsf_graph = nx.DiGraph()
        trsf_graph.add_node(0, reference=True)
        trsf_graph.add_nodes_from([1, 2, 3])
        trsf_graph.add_edges_from([(0, 1), (2, 3)])
        self.assertFalse(check_all_nodes_connected_to_reference(trsf_graph))

    def test_no_reference_node(self):
        trsf_graph = nx.DiGraph()
        trsf_graph.add_nodes_from([0, 1, 2, 3])
        trsf_graph.add_edges_from([(0, 1), (1, 2), (2, 3)])
        with self.assertRaises(ValueError):
            check_all_nodes_connected_to_reference(trsf_graph)

    def test_all_nodes_connected_to_itself_only(self):
        trsf_graph = nx.DiGraph()
        trsf_graph.add_node(0, reference=True)
        trsf_graph.add_nodes_from([1, 2, 3])
        trsf_graph.add_edges_from([(1, 1), (2, 2), (3, 3)])
        self.assertFalse(check_all_nodes_connected_to_reference(trsf_graph))

    def test_empty_graph(self):
        trsf_graph = nx.DiGraph()
        with self.assertRaises(ValueError):
            check_all_nodes_connected_to_reference(trsf_graph)


class TestFusionOnReference(unittest.TestCase):

    def setUp(self):
        self.ma_images = {ma_im_idx: shared_data('flower_multiangle', ma_im_idx) for ma_im_idx in range(3)}
        self.ref_idx = 0
        # Load shared multi-angle landmarks for the first time point (t0) of 'p58' shared time-series
        ref_pts_01, flo_pts_01 = shared_data('flower_multiangle_landmarks', (0, 1))
        ref_pts_02, flo_pts_02 = shared_data('flower_multiangle_landmarks', (0, 2))
        # Creates manual initialization transformations with `pointmatching` algorithm:
        trsf_01 = pointmatching(flo_pts_01, ref_pts_01, template_img=self.ma_images[self.ref_idx], method='rigid')
        trsf_02 = pointmatching(flo_pts_02, ref_pts_02, template_img=self.ma_images[self.ref_idx], method='rigid')
        self.init_trsfs = {(0, 1): trsf_01, (0, 2): trsf_02}

    def test_fusion_on_reference_low_resolution(self):
        expected_vxs = max(self.ma_images[self.ref_idx].get_voxelsize())
        fused_img, transformations = fusion_on_reference(
            self.ma_images,
            method="mean",
            init_trsfs=self.init_trsfs,
            ref_img_idx=0,
            super_resolution=False
        )
        final_vxs = fused_img.get_voxelsize()[0]

        # Assertions to verify expected behavior
        self.assertIsNotNone(fused_img)  # Ensure the result is not None
        self.assertTrue(isinstance(fused_img, SpatialImage))  # Ensure fused image is returned as a SpatialImage
        self.assertTrue(isinstance(transformations, dict))  # Ensure transformations are returned as a dictionary
        self.assertTrue(all(isinstance(trsf, Trsf) for trsf in
                            transformations.values()))  # Ensure transformations are returned as a dictionary
        self.assertIn((0, 1), transformations)  # Ensure key (0, 1) exists in transformations
        self.assertIn((0, 2), transformations)  # Ensure key (0, 2) exists in transformations
        self.assertAlmostEquals(final_vxs, expected_vxs, delta=0.0001)

    def test_fusion_on_reference_super_resolution(self):
        expected_vxs = min(self.ma_images[self.ref_idx].get_voxelsize())
        fused_img, transformations = fusion_on_reference(
            self.ma_images,
            method="mean",
            init_trsfs=self.init_trsfs,
            ref_img_idx=0,
            super_resolution=True
        )
        final_vxs = fused_img.get_voxelsize()[0]

        # Assertions to verify expected behavior
        self.assertIsNotNone(fused_img)  # Ensure the result is not None
        self.assertTrue(isinstance(fused_img, SpatialImage))  # Ensure fused image is returned as a SpatialImage
        self.assertTrue(isinstance(transformations, dict))  # Ensure transformations are returned as a dictionary
        self.assertTrue(all(isinstance(trsf, Trsf) for trsf in
                            transformations.values()))  # Ensure transformations are returned as a dictionary
        self.assertIn((0, 1), transformations)  # Ensure key (0, 1) exists in transformations
        self.assertIn((0, 2), transformations)  # Ensure key (0, 2) exists in transformations
        self.assertAlmostEquals(final_vxs, expected_vxs, delta=0.0001)


class TestIterativeFusion(unittest.TestCase):

    def setUp(self):
        ma_images = {ma_im_idx: shared_data('flower_multiangle', ma_im_idx) for ma_im_idx in range(3)}
        # Down sample image to be faster and use less memory:
        self.ma_images = {idx: resample(img, voxelsize=[min(img.get_voxelsize())*2]*3) for idx, img in ma_images.items()}
        self.ref_idx = 0
        # Load shared multi-angle landmarks for the first time point (t0) of 'p58' shared time-series
        ref_pts_01, flo_pts_01 = shared_data('flower_multiangle_landmarks', (0, 1))
        ref_pts_02, flo_pts_02 = shared_data('flower_multiangle_landmarks', (0, 2))
        # Creates manual initialization transformations with `pointmatching` algorithm:
        trsf_01 = pointmatching(flo_pts_01, ref_pts_01, template_img=self.ma_images[self.ref_idx], method='rigid')
        trsf_02 = pointmatching(flo_pts_02, ref_pts_02, template_img=self.ma_images[self.ref_idx], method='rigid')
        self.init_trsfs = {(0, 1): trsf_01, (0, 2): trsf_02}

    def test_fast_lowres_iterative_fusion(self):
        expected_vxs = max(self.ma_images[self.ref_idx].get_voxelsize())
        fused_img = iterative_fusion(
            self.ma_images,
            method="mean",
            init_trsfs=self.init_trsfs,
            n_iter=2,
            ref_img_idx=0,
            vectorfield_at_last=True,
            super_resolution=False,
            pyramid_highest_level=5,
            pyramid_lowest_level=4
        )
        final_vxs = fused_img.get_voxelsize()[0]

        # Assertions to verify expected behavior
        self.assertIsNotNone(fused_img)  # Ensure the result is not None
        self.assertTrue(isinstance(fused_img, SpatialImage))  # Ensure fused image is returned as a SpatialImage
        self.assertAlmostEquals(final_vxs, expected_vxs, delta=0.0001)

    def test_fast_supres_iterative_fusion(self):
        expected_vxs = min(self.ma_images[self.ref_idx].get_voxelsize())
        fused_img = iterative_fusion(
            self.ma_images,
            method="mean",
            init_trsfs=self.init_trsfs,
            n_iter=2,
            ref_img_idx=0,
            vectorfield_at_last=True,
            super_resolution=True,
            pyramid_highest_level=5,
            pyramid_lowest_level=4
        )
        final_vxs = fused_img.get_voxelsize()[0]

        # Assertions to verify expected behavior
        self.assertIsNotNone(fused_img)  # Ensure the result is not None
        self.assertTrue(isinstance(fused_img, SpatialImage))  # Ensure fused image is returned as a SpatialImage
        self.assertAlmostEquals(final_vxs, expected_vxs, delta=0.0001)


if __name__ == '__main__':
    unittest.main()
