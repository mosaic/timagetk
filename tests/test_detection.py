#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import gc
import unittest

import numpy as np
from scipy.cluster.vq import vq

from timagetk.algorithms.peak_detection import detect_nuclei
from timagetk.algorithms.peak_detection import detect_segmentation_nuclei
from timagetk.algorithms.signal_quantification import quantify_nuclei_signal_intensity
from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image
from timagetk.synthetic_data.nuclei_image import example_nuclei_signal_images
from timagetk.synthetic_data.nuclei_image import nuclei_image_from_point_positions


class TestDetection(unittest.TestCase):

    def setUp(self):
        np.random.seed(42)

        self.n_points = 10
        self.nuclei_radius = 1.5
        self.intensity = 10000

        n_img, s_img, p, s = example_nuclei_signal_images(n_points=self.n_points,
                                                          extent=15.,
                                                          nuclei_radius=self.nuclei_radius,
                                                          signal_intensity=self.intensity)
        self.nuclei_img = n_img
        self.signal_img = s_img
        self.points = p
        self.signals = s

    def tearDown(self):
        del self.n_points
        del self.nuclei_radius
        del self.intensity
        del self.nuclei_img
        del self.signal_img
        del self.points
        del self.signals

    def test_detection(self):
        pos = detect_nuclei(self.nuclei_img, threshold=1)
        self.assertEqual(len(pos), self.n_points)

        points_array = np.array(list(self.points.values()))
        # print(pos)
        # print(points_array)
        matching = vq(pos, points_array)
        self.assertEqual(len(np.unique(matching[0])), self.n_points)
        self.assertTrue(np.all(matching[1] < np.linalg.norm(self.nuclei_img.voxelsize)))

    def test_detection_patch(self):
        pos = detect_nuclei(self.nuclei_img, threshold=1, patch_size=(30, 30, 30))
        self.assertEqual(len(pos), self.n_points)

        points_array = np.array(list(self.points.values()))
        # print(pos)
        # print(points_array)
        matching = vq(pos, points_array)
        self.assertEqual(len(np.unique(matching[0])), self.n_points)
        self.assertTrue(np.all(matching[1] < np.linalg.norm(self.nuclei_img.voxelsize)))

    def test_quantification(self):
        pos = detect_nuclei(self.nuclei_img, threshold=1)

        points_array = np.array(list(self.points.values()))
        signals_array = np.array(list(self.signals.values()))
        matching = vq(pos, points_array)

        signals = quantify_nuclei_signal_intensity(self.nuclei_img, pos)
        self.assertTrue(np.all(np.isclose(signals, signals[0], atol=0.05 * self.intensity)))

        signals = quantify_nuclei_signal_intensity(self.signal_img, pos)
        signals = signals / signals_array[matching[0]]
        self.assertTrue(np.all(np.isclose(signals, signals[0], atol=0.05 * self.intensity)))


class TestDetectionSegmentation(unittest.TestCase):

    def setUp(self):
        np.random.seed(42)

        self.n_layers = 1
        self.n_points = 10
        self.nuclei_radius = 1.5
        self.intensity = 10000

        seg_img, p = example_layered_sphere_labelled_image(n_points=self.n_points,
                                                           n_layers=self.n_layers,
                                                           return_points=True)
        self.seg_img = seg_img
        self.points = p

        n_img = nuclei_image_from_point_positions(self.points,
                                                  extent=seg_img.shape[0] * seg_img.voxelsize[0],
                                                  voxelsize=seg_img.voxelsize,
                                                  nuclei_radius=self.nuclei_radius,
                                                  nuclei_intensity=self.intensity)
        self.nuclei_img = n_img

    def tearDown(self):
        del self.n_layers
        del self.n_points
        del self.nuclei_radius
        del self.intensity
        del self.seg_img
        del self.points
        del self.nuclei_img

    def test_detection_segmentation(self):
        pos = detect_segmentation_nuclei(self.nuclei_img, self.seg_img)
        self.assertEqual(len(pos), len(self.points))
        for c in self.points.keys():
            self.assertTrue(np.linalg.norm(pos[c] - self.points[c]) < np.linalg.norm(self.nuclei_img.voxelsize))


if __name__ == '__main__':
    unittest.main()
    gc.collect()