#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2022 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import gc
import unittest

from tests.make_test_data import get_synthetic_data
from timagetk import SpatialImage, LabelledImage
from timagetk.algorithms.surfcut import surfcut_erosion, surfcut_projection_2d


class TestSurfCut(unittest.TestCase):

    def setUp(self) -> None:
        self.img = get_synthetic_data("small", "wall")
        self.seg_img = get_synthetic_data("small", "labelled")
        self.out_img = None
        self.out_seg_img = None

    def tearDown(self) -> None:
        del self.img
        del self.out_img
        gc.collect()

    def _test_spatial_image(self, out_img):
        self.assertIsInstance(out_img, SpatialImage)
        self.assertIsNotNone(out_img.get_array())

    def _test_labelled_image(self, out_img):
        self.assertIsInstance(out_img, LabelledImage)
        self.assertIsNotNone(out_img.get_array())

    def test_surfcut(self):
        self.out_img = surfcut_erosion(self.img, top_erosion=6, bottom_erosion=9)
        self._test_spatial_image(self.out_img)

    def test_surfcut_projection(self):
        self.out_img, self.out_seg_img = surfcut_projection_2d(self.img, seg_img=self.seg_img, top_erosion=6, bottom_erosion=9)
        self._test_spatial_image(self.out_img)
        self._test_labelled_image(self.out_seg_img)


if __name__ == '__main__':
    unittest.main()
