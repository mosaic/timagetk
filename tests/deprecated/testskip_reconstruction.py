#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import unittest

import numpy as np
from old.algorithms.reconstruction import pts2transfo
from timagetk import Trsf

from timagetk.algorithms.trsf import create_trsf

floating_points = [[238. * 0.200320, 196. * 0.200320, 9.],
                   [204. * 0.200320, 182. * 0.200320, 11.],
                   [180. * 0.200320, 214. * 0.200320, 12.],
                   [201. * 0.200320, 274. * 0.200320, 12.],
                   [148. * 0.200320, 225. * 0.200320, 18.],
                   [248. * 0.200320, 252. * 0.200320, 8.],
                   [305. * 0.200320, 219. * 0.200320, 10.]]

reference_points = [[173. * 0.200320, 151. * 0.200320, 17.],
                    [147. * 0.200320, 179. * 0.200320, 16.],
                    [165. * 0.200320, 208. * 0.200320, 12.],
                    [226. * 0.200320, 204. * 0.200320, 9.],
                    [170. * 0.200320, 254. * 0.200320, 10.],
                    [223. * 0.200320, 155. * 0.200320, 13.],
                    [218. * 0.200320, 109. * 0.200320, 23.]]

res_trsf = [[0.40710149, 0.89363883, 0.18888626, -22.0271968],
            [-0.72459862, 0.19007589, 0.66244094, 51.59203463],
            [0.55608022, -0.40654742, 0.72490964, -0.07837002],
            [0., 0., 0., 1.]]
res_trsf = np.array(res_trsf, dtype='float32')


class TestReconstruct(unittest.TestCase):

    def test_pts2transfo(self):
        transfo = pts2transfo(floating_points, reference_points)
        np.testing.assert_array_almost_equal(transfo, np.array(res_trsf),
                                             decimal=6)

    def test_Trsf_init(self):
        trsf = create_trsf(param_str_2='-identity', trsf_type='AFFINE_3D',
                           trsf_unit='REAL_UNIT')
        allocate_c_bal_matrix(trsf.mat.c_struct, res_trsf)
        # Test trsf type is ``Trsf``:
        self.assertIsInstance(trsf, Trsf)
        # Test we have a linear transformation (affine)
        self.assertTrue(trsf.is_linear())
        # Test we have an affine transformation
        self.assertTrue(trsf.get_type() == "AFFINE_3D")
        # Test the transformation matrix in the Trsf object is the same than the given numpy array
        np.testing.assert_array_almost_equal(res_trsf, trsf.mat.to_np_array(
            dtype='float32'), decimal=6)
