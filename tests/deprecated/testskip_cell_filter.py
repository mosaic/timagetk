#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

#
#       File author(s):
#           Sophie Ribes <sophie.ribes@inria.fr>
#
#       File contributor(s):
#           Guillaume Baty <guillaume.baty@inria.fr>
#           Grégoire Malandain <gregoire.malandain@inria.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

import unittest

import numpy as np

from timagetk.algorithms.morphology import label_filtering
from timagetk.components.labelled_image import LabelledImage
from timagetk.io import imread
from timagetk.io.dataset import shared_data


class TestCellFilter(unittest.TestCase):

    def test_erosion(self):
        """Tests segmented image erosion using wrapped MORPHEME library VT. """
        sp_img_ref = LabelledImage(imread(shared_data('eroded_segmentation.mha')), not_a_label=0)
        sp_img = LabelledImage(imread(shared_data('segmentation_seeded_watershed.inr')), not_a_label=0)
        # ./cellfilter segmentation_seeded_watershed.inr eroded_segmentation.mha -erosion -sphere -radius 1 -iterations 1
        output = label_filtering(sp_img, method='erosion', radius=1, sphere=True)
        np.testing.assert_array_equal(output, sp_img_ref)

    def test_dilation(self):
        """Tests segmented image dilation using wrapped MORPHEME library VT. """
        sp_img_ref = LabelledImage(imread(shared_data('dilated_eroded_segmentation.mha')), not_a_label=0)
        sp_img = LabelledImage(imread(shared_data('eroded_segmentation.mha')), not_a_label=0)
        # ./cellfilter segmentation_seeded_watershed.inr dilated_eroded_segmentation.mha -dilation -sphere -radius 1 -iterations 1
        output = label_filtering(sp_img, method='dilation', radius=1, sphere=True)
        np.testing.assert_array_equal(output, sp_img_ref)
