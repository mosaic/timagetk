#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

#
#       File author(s):
#           Sophie Ribes <sophie.ribes@inria.fr>
#
#       File contributor(s):
#           Guillaume Baty <guillaume.baty@inria.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

import unittest

import numpy as np
from vt import regionalext as vt_regionalext

from timagetk.algorithms.regionalext import regional_extrema
from timagetk.components.spatial_image import SpatialImage
from timagetk.io import imread
from timagetk.io.dataset import shared_data


class TestRegionalExt(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.im_path = shared_data('p58-t0-a0.lsm', 'p58')
        cls.im = imread(cls.im_path)

    def _test_regionalext(self, params):
        ref_im = vt_regionalext(self.im.to_vt_image(), params=params)
        ref_im = SpatialImage(ref_im)
        out_im = regional_extrema(self.im, params=params)
        np.testing.assert_array_equal(ref_im, out_im)

    def test_regionalmax(self):
        params = "-maxima -connectivity 26 -h 3"
        self._test_regionalext(params)

    def test_regionalmin(self):
        params = "-minima -connectivity 26 -h 3"
        self._test_regionalext(params)
