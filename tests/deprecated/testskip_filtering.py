#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

#
#       File author(s):
#           Sophie Ribes <sophie.ribes@inria.fr>
#
#       File contributor(s):
#           Guillaume Baty <guillaume.baty@inria.fr>
#           Grégoire Malandain <gregoire.malandain@inria.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

import unittest

import numpy as np
import vt

from timagetk.algorithms.linearfilter import linearfilter
from timagetk.io import imread
from timagetk.io import imsave
from timagetk.io.dataset import shared_data


class TestFiltering(unittest.TestCase):

    def test_param_change(self):
        """Test that changing given parameters returns different images. """
        sp_img = imread(shared_data('filtering_src.inr'))
        f1 = linearfilter(sp_img, method="smoothing", sigma=1.0)
        f2 = linearfilter(sp_img, method="smoothing", sigma=2.0)
        self.assertFalse(f1.equal_array(f2))

    def _compare_fun_cmd(self, img_ref, out_img):
        self.assertEqual(img_ref.dtype, out_img.dtype)
        # self.assertListEqual(img_ref.voxelsize, out_img.voxelsize)
        np.testing.assert_array_almost_equal(img_ref.voxelsize, out_img.voxelsize, decimal=6)
        self.assertTupleEqual(img_ref.shape, out_img.shape)
        self.assertListEqual(img_ref.origin, out_img.origin)
        # self.assertListEqual(img_ref.extent, out_img.extent)
        np.testing.assert_array_almost_equal(img_ref.extent, out_img.extent, decimal=6)
        np.testing.assert_array_equal(img_ref.get_array(), out_img.get_array())

    def _filter(self, params):
        path_img = shared_data('p58-t0-a0.lsm', 'p58')
        path_img_ref = shared_data('filtering_linearfilter.mha')
        path_img_algo = shared_data('filtering_linearfilter_algo.mha')
        # --- read test image using vt.vtImage
        vt_img = vt.vtImage(path_img)
        # --- apply linear filter using vt library call
        ref_img_vt = vt.linear_filter(vt_img, params)
        assert ref_img_vt is not None
        ref_img_vt.write(path_img_ref)
        # --- read test image using vt.vtImage
        img = imread(path_img)
        # --- apply linear filter using python function
        out_img_algo = linearfilter(img, params=params)
        imsave(path_img_algo, out_img_algo)
        # --- load the cmdline file and compare it to the python function
        ref_img_vt = imread(path_img_ref)
        self._compare_fun_cmd(ref_img_vt, out_img_algo)

    def _filter_cmd(self, params):
        path_img = shared_data('p58-t0-a0.lsm', 'p58')
        path_img_ref = shared_data('filtering_linearfilter.inr')
        # --- apply linear filter using cmdline call
        cmd_status = vt.linear_filter(path_img, params, out_file_path=path_img_ref)
        self.assertTrue(cmd_status == 0)
        # --- read test image
        img = imread(path_img)
        # --- apply linear filter using python function
        out_img = linearfilter(img, params=params)
        # --- load the cmdline file and compare it to the python function
        img_ref = imread(path_img_ref)
        self._compare_fun_cmd(img_ref, out_img)

    def test_gaussian_filtering(self):
        """
        Testing Gaussian filter algorithm.
        """
        # self._filter("-smoothing -sigma 2.0 -unit real")
        self._filter("-smoothing -sigma 2.0 -unit voxel")

    def test_gaussian_filtering_xyz(self):
        """
        Testing direction specific Gaussian filter algorithm.
        """
        # self._filter("-smoothing -sigma 2.0 2.0 1.0 -unit real")
        self._filter("-smoothing -sigma 2.0 2.0 1.0 -unit voxel")

    def test_gaussian_filtering_cmd(self):
        """
        Testing Gaussian filter algorithm.
        """
        # self._filter_cmd("-smoothing -sigma 2.0 -unit real")
        self._filter_cmd("-smoothing -sigma 2.0 -unit voxel")

    def test_gaussian_filtering_xyz_cmd(self):
        """
        Testing direction specific Gaussian filter algorithm.
        """
        # self._filter_cmd("-smoothing -sigma 2.0 2.0 1.0 -unit real")
        self._filter_cmd("-smoothing -sigma 2.0 2.0 1.0 -unit voxel")

    def test_plugin_1(self):
        path_img = shared_data('p58-t0-a0.lsm', 'p58')
        img = imread(path_img)
        algo_img = linearfilter(img, params="-smoothing -sigma 1.0 -unit voxel")
        plugin_img = linear_filtering(img, method='gaussian_smoothing', sigma=1.0, real=False)
        np.testing.assert_array_equal(algo_img, plugin_img)

    def test_plugin_2(self):
        path_img = shared_data('p58-t0-a0.lsm', 'p58')
        img = imread(path_img)
        # algo_img = linearfilter(img, params="-smoothing -sigma 1.0 -unit real")
        algo_img = linearfilter(img, params="-smoothing -sigma 1.0 -unit voxel")
        plugin_img = linear_filtering(img, method='gaussian_smoothing', sigma=1.0, real=False)
        np.testing.assert_array_equal(algo_img, plugin_img)

    def test_plugin_3(self):
        path_img = shared_data('p58-t0-a0.lsm', 'p58')
        img = imread(path_img)
        # algo_img = linearfilter(img, params="-smoothing -sigma 2.0 2.0 1.0 -unit real")
        algo_img = linearfilter(img, params="-smoothing -sigma 2.0 2.0 1.0 -unit voxel")
        plugin_img = linear_filtering(img, method='gaussian_smoothing', sigma=[2.0, 2.0, 1.0], real=False)
        np.testing.assert_array_equal(algo_img, plugin_img)
