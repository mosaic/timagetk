#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

#
#       File author(s):
#           Sophie Ribes <sophie.ribes@inria.fr>
#
#       File contributor(s):
#           Guillaume Baty <guillaume.baty@inria.fr>
#           Grégoire Malandain <gregoire.malandain@inria.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

import unittest

import numpy as np

try:
    from timagetk.io.dataset import shared_data
    from timagetk.io import imread
    from timagetk.algorithms import morphology
    from timagetk.plugins import morphology
except ImportError as e:
    raise ImportError('Import Error: {}'.format(e))


class TestMorpho(unittest.TestCase):

    def test_dilation(self):
        """
        Tests grayscale image dilation using wrapped MORPHEME library VT.
        """
        im = imread(shared_data('filtering_src.inr'))
        im_ref = imread(shared_data('morpho_dilation_default.inr.gz'))
        # $ ./morphology filtering_src.inr morpho_dilation_default.inr.gz -dilation -sphere -radius 1 -iterations 1
        output = morphology(im, params='-dilation')
        np.testing.assert_array_equal(output, im_ref)

    def test_erosion(self):
        """
        Tests grayscale image erosion using wrapped MORPHEME library VT.
        """
        im = imread(shared_data('filtering_src.inr'))
        im_ref = imread(shared_data('morpho_erosion_default.inr.gz'))
        # $ ./morphology filtering_src.inr morpho_dilation_default.inr.gz -erosion -sphere -radius 1 -iterations 1
        output = morphology(im, params='-erosion')
        np.testing.assert_array_equal(output, im_ref)

    def test_plugin_dilation(self):
        """
        Tests grayscale image dilation using morphology plugin.
        """
        im = imread(shared_data('filtering_src.inr'))
        im_ref = imread(shared_data('morpho_dilation_default.inr.gz'))
        output = morphology(im, method='dilation')
        np.testing.assert_array_equal(output, im_ref)

    def test_plugin_erosion(self):
        """
        Tests grayscale image erosion using morphology plugin.
        """
        im = imread(shared_data('filtering_src.inr'))
        im_ref = imread(shared_data('morpho_erosion_default.inr.gz'))
        output = morphology(im, method='erosion')
        np.testing.assert_array_equal(output, im_ref)
