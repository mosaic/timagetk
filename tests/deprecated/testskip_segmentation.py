#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

#
#       File author(s):
#           Sophie Ribes <sophie.ribes@inria.fr>
#
#       File contributor(s):
#           Guillaume Baty <guillaume.baty@inria.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

import unittest

import numpy as np
from timagetk.old_plugins import h_transform
from timagetk.plugins import linear_filtering
from timagetk.plugins import region_labeling
from timagetk.plugins import segmentation

from timagetk.algorithms import connexe
from timagetk.algorithms import linearfilter
from timagetk.algorithms import regionalext
from timagetk.algorithms import watershed
from timagetk.io import imread
from timagetk.io.dataset import shared_data


class TestSegmentation(unittest.TestCase):

    def test_segmentation(self):
        """
        Tests grayscale image segmentation using wrapped MORPHEME library VT.

        Equivalent to following shell commands using compiled VT binaries:
        ./linearFilter $DATA_PATH/segmentation_src.inr $DATA_PATH/smoothed_segmentation.mha -smoothing -sigma 2.0
        ./regionalext $DATA_PATH/smoothed_segmentation.mha - -minima -connectivity 26 -h 5 | ./connexe - $DATA_PATH/segmentation_seeds.mha -low-threshold 1 -high-threshold 3 -labels -connectivity 26
        ./watershed -gradient $DATA_PATH/smoothed_segmentation.mha -seeds $DATA_PATH/segmentation_seeds.mha $DATA_PATH/segmentation_seeded_watershed.mha -l most
        """
        im = imread(shared_data('segmentation_src.inr'))
        im_ref = imread(shared_data('segmentation_seeded_watershed.mha'))
        # - Smoothing:
        smooth_params = "-smoothing -sigma 2.0"
        smooth_img = linearfilter(im, params=smooth_params)

        # - Local minima:
        regext_params = "-minima -connectivity 26 -h 5"
        regext_img = regionalext(smooth_img, params=regext_params)
        # ./linearFilter smoothed_segmentation.mha

        # - Connexe components labelling:
        conn_params = "-low-threshold 1 -high-threshold 3 -labels -connectivity 26"
        conn_img = connexe(regext_img, params=conn_params)

        # - Watershed:
        wat_img = watershed(smooth_img, conn_img)
        # Uncomment clean (rm -vf ...) in gen_image.sh script and uncomment next lines to test step results
        # np.testing.assert_array_equal(smooth_img, imread('segmentation_smooth.inr'))
        # np.testing.assert_array_equal(regext_img, imread('segmentation_regext.inr'))
        # np.testing.assert_array_equal(conn_img, imread('segmentation_connexe.inr'))
        np.testing.assert_array_equal(wat_img, im_ref)

    def test_plugin(self):
        """
        Tests grayscale image segmentation using linear_filtering, h_transform, region_labeling & segmentation plugins.

        Equivalent to following shell commands using compiled VT binaries:
        ./linearFilter $DATA_PATH/segmentation_src.inr $DATA_PATH/smoothed_segmentation.mha -smoothing -sigma 2.0
        ./regionalext $DATA_PATH/smoothed_segmentation.mha - -minima -connectivity 26 -h 5 | ./connexe - $DATA_PATH/segmentation_seeds.mha -low-threshold 1 -high-threshold 3 -labels -connectivity 26
        ./watershed -gradient $DATA_PATH/smoothed_segmentation.mha -seeds $DATA_PATH/segmentation_seeds.mha $DATA_PATH/segmentation_seeded_watershed.mha -l most
        """
        im = imread(shared_data('segmentation_src.inr'))
        im_ref = imread(shared_data('segmentation_seeded_watershed.mha'))
        # - Smoothing:
        smooth_img = linear_filtering(im, sigma=2.0,
                                      method='gaussian_smoothing')
        # - Local minima:
        regext_img = h_transform(smooth_img, h=5, method='min')
        # - Connexe components labelling:
        conn_img = region_labeling(regext_img, low_threshold=1,
                                   high_threshold=3,
                                   method='connected_components')
        # - Watershed:
        wat_img = segmentation(smooth_img, conn_img, control='most',
                               method='seeded_watershed')

        np.testing.assert_array_equal(wat_img, im_ref)
