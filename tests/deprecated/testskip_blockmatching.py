#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import unittest

from timagetk.algorithms.blockmatching import blockmatching
from timagetk.algorithms.trsf import apply_trsf
from timagetk.algorithms.trsf import create_trsf
from timagetk.io import imread
from timagetk.io.dataset import shared_data


class TestBlockmatching(unittest.TestCase):

    def _test_blockmatching(self, reg_type):
        ref_img = imread(shared_dataset("p58", 'intensity')[1])
        if reg_type == 'vectorfield':
            ref_trsf = create_trsf("sinus", trsf_type='vectorfield', template_img=ref_img)
        else:
            ref_trsf = create_trsf("random", trsf_type='affine', seed=2)
        flo_img = apply_trsf(ref_img, ref_trsf)

        trsf_out = blockmatching(flo_img, ref_img, method=reg_type)

        # np.testing.assert_allclose(trsf_out.copy_to_array(),
        #                            inv_trsf(ref_trsf).copy_to_array(),
        #                            rtol=0.1)

    def test_blockmatching_rigid(self):
        self._test_blockmatching("rigid")

    def test_blockmatching_affine(self):
        self._test_blockmatching("affine")

    def test_blockmatching_vectorfield(self):
        self._test_blockmatching("vectorfield")
