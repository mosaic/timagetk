#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# -----------------------------------------------------------------------------

import unittest

from timagetk.plugins.resampling import resample

from timagetk.io import imread
from timagetk.io.dataset import shared_data
from timagetk.tasks.fusion import fusion

files = [shared_data("time_0_cut.inr"),
         shared_data("time_0_cut_rotated1.mha"),
         shared_data("time_0_cut_rotated2.mha")]
ref_fusion_file = shared_data("time_0_cut_fused.mha")

downsampling = 3.


def clean_images():
    """

    """
    import os
    test_path = os.getcwd()
    flist = os.listdir(test_path)
    for f in flist:
        if f.endswith('.inr'):
            os.remove(f)


class TestFusion(unittest.TestCase):

    def test_quick_fusion(self):
        """Test fusion with only one round of registration.
        Structural similarity should be above 98% similarity.
        """
        # Build the list of SpatialImage to use:
        img_list = []
        for f in files:
            img = imread(f)
            print("Original voxelsize: {}".format(img.voxelsize))
            ds_vxs = [vox * downsampling for vox in img.voxelsize]
            print("Down-sampled voxelsize: {}".format(ds_vxs))
            img_list.append(
                resample(img, voxelsize=ds_vxs, interpolation='grey'))

        # Performs fusion:
        fus_img = fusion(img_list, iterations=0, mean_imgs_prefix='test')

        clean_images()

    def test_fusion(self):
        """
        Test fusion script.
        Structural similarity should be above 98% similarity.
        """
        # Build the list of SpatialImage to use:
        img_list = []
        for f in files:
            img = imread(f)
            ds_vxs = [vox * downsampling for vox in img.voxelsize]
            img_list.append(
                resample(img, voxelsize=ds_vxs, interpolation='grey'))

        # Performs fusion:
        fus_img = fusion(img_list, iterations=3, mean_imgs_prefix='test')

        # Compare the obtained image to saved fusion:
        ref_fus_img = imread(ref_fusion_file)
        ds_vxs = [vox * downsampling for vox in ref_fus_img.voxelsize]
        ref_fus_img = resample(ref_fus_img, voxelsize=ds_vxs,
                               interpolation='grey')
        self.assertTrue(
            fus_img.structural_similarity_index(ref_fus_img) >= 0.98)

        clean_images()

    def test_fusion_manual_init(self):
        """
        Test fusion script, with a manual initialisation.
        """
        pass
