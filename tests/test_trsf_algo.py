#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import gc
import unittest

import numpy as np

from tests.make_test_data import get_synthetic_data
from timagetk import LabelledImage
from timagetk import SpatialImage
from timagetk import Trsf
from timagetk.algorithms.trsf import apply_trsf
from timagetk.algorithms.trsf import compose_trsf
from timagetk.algorithms.trsf import create_trsf
from timagetk.algorithms.trsf import inv_trsf
from timagetk.algorithms.trsf import trsfs_averaging
from timagetk.array_util import random_spatial_image
from timagetk.third_party.vt_parser import GRAY_INTERPOLATION_METHODS
from timagetk.third_party.vt_parser import LABEL_INTERPOLATION_METHODS

RANDOM_RIGID = np.array([[0.99124341, -0.12681863, -0.03679038, 1.36747104],
                         [0.11992661, 0.98121385, -0.15111909, 0.29632705],
                         [0.05526395, 0.14538366, 0.9878307, 0.50283413],
                         [0., 0., 0., 1.]])
RANDOM_AFFINE = np.array([[2.19463058, 0.70743615, 0.5516579, 0.77010137],
                          [0.56984262, 1.27385788, 0.32356494, 1.42834459],
                          [0.77246664, 0.76172275, 1.59177451, 1.3742926],
                          [0., 0., 0., 1.]])


class TestCreateTrsf(unittest.TestCase):

    def test_rigid_identity(self):
        trsf = create_trsf('identity', trsf_type='rigid')
        self.assertIsInstance(trsf, Trsf)
        # self.assertTrue(trsf.is_rigid())
        self.assertTrue(trsf.is_affine())
        np.testing.assert_almost_equal(trsf.get_array(), np.identity(4))
        del trsf
        gc.collect()

    def test_affine_identity(self):
        trsf = create_trsf('identity', trsf_type='affine')
        self.assertIsInstance(trsf, Trsf)
        self.assertTrue(trsf.is_affine())
        np.testing.assert_almost_equal(trsf.get_array(), np.identity(4))
        del trsf
        gc.collect()

    def test_vectorfield_identity(self):
        trsf = create_trsf('identity', template_img=random_spatial_image((3, 5, 5)), trsf_type='vectorfield')
        self.assertIsInstance(trsf, Trsf)
        self.assertTrue(trsf.is_vectorfield())
        del trsf
        gc.collect()

    def test_vectorfield_random(self):
        trsf = create_trsf('sinus', template_img=random_spatial_image((3, 5, 5)), trsf_type='vectorfield')
        self.assertIsInstance(trsf, Trsf)
        self.assertTrue(trsf.is_vectorfield())
        del trsf
        gc.collect()

    def test_rigid_random(self):
        trsf = create_trsf('random', trsf_type='rigid', seed=1,
                           angle_range=[0., 0.25], translation_range=[0., 1.5])
        self.assertIsInstance(trsf, Trsf)
        # self.assertTrue(trsf.is_rigid())
        self.assertTrue(trsf.is_affine())
        self.assertIsNotNone(trsf.get_array())
        np.testing.assert_almost_equal(trsf.get_array(), RANDOM_RIGID)
        del trsf
        gc.collect()

    def test_affine_random(self):
        trsf = create_trsf('random', trsf_type='affine', seed=1,
                           angle_range=[0., 0.25], translation_range=[0., 1.5],
                           shear_range=[0.25, 0.5], scale_range=[1., 2.5])
        self.assertIsInstance(trsf, Trsf)
        self.assertTrue(trsf.is_affine())
        self.assertIsNotNone(trsf.get_array())
        np.testing.assert_almost_equal(trsf.get_array(), RANDOM_AFFINE)
        del trsf
        gc.collect()

    def test_vectorfield_random_params(self):
        trsf = create_trsf('sinus', template_img=random_spatial_image((3, 5, 5)), trsf_type='vectorfield',
                           sinusoid_amplitude=[1., 1., 1.], sinusoid_period=[0.55, 0.55, 0.55])
        self.assertIsInstance(trsf, Trsf)
        self.assertTrue(trsf.is_vectorfield())
        del trsf
        gc.collect()


class TestComposeTrsf(unittest.TestCase):

    def setUp(self) -> None:
        self.template = random_spatial_image((3, 5, 5))
        self.lin_trsf = create_trsf('random', trsf_type='rigid')
        self.vf_trsf = create_trsf('sinus', template_img=self.template, trsf_type='vectorfield')

    def tearDown(self) -> None:
        del self.template
        del self.lin_trsf
        del self.vf_trsf
        gc.collect()

    def test_compose(self):
        trsf = compose_trsf([self.lin_trsf, self.vf_trsf])
        self.assertIsInstance(trsf, Trsf)
        self.assertTrue(trsf.is_vectorfield())
        self.assertIsNotNone(trsf.get_array())


class TestRigidTrsf(unittest.TestCase):

    def setUp(self) -> None:
        self.trsf = create_trsf('random', trsf_type='rigid')

    def tearDown(self) -> None:
        del self.trsf
        gc.collect()

    def test_inv_trsf(self):
        out_trsf = inv_trsf(self.trsf)
        self.assertIsInstance(out_trsf, Trsf)
        self.assertIsNotNone(out_trsf.get_array())
        self.assertTrue(out_trsf.is_linear())

    def _test_apply_trsf_to_intensity(self, interpolation):
        image = get_synthetic_data("medium", "wall")
        kwargs = {'quiet': True, 'interpolation': interpolation}
        out_img = apply_trsf(image, self.trsf, **kwargs)
        self.assertIsInstance(out_img, SpatialImage)
        self.assertIsNotNone(out_img.get_array())
        self.assertEqual(image.get_voxelsize(), out_img.get_voxelsize())
        self.assertEqual(image.get_shape(), out_img.get_shape())

    def _test_apply_trsf_to_labelled(self, interpolation):
        image = get_synthetic_data("medium", "labelled")
        kwargs = {'quiet': True, 'interpolation': interpolation}
        if interpolation == 'cellbased':
            kwargs.update({'cell_based_sigma': 2})
        out_img = apply_trsf(image, self.trsf, **kwargs)
        self.assertIsInstance(out_img, LabelledImage)
        self.assertIsNotNone(out_img.get_array())
        self.assertEqual(image.get_voxelsize(), out_img.get_voxelsize())
        self.assertEqual(image.get_shape(), out_img.get_shape())

    def test_apply_trsf(self):
        for interpolation in GRAY_INTERPOLATION_METHODS:
            self._test_apply_trsf_to_intensity(interpolation)
        for interpolation in LABEL_INTERPOLATION_METHODS:
            self._test_apply_trsf_to_labelled(interpolation)

    def test_compose_trsf(self):
        out_trsf = inv_trsf(self.trsf)
        trsf = compose_trsf([self.trsf, out_trsf])
        np.testing.assert_almost_equal(trsf.get_array(), create_trsf('identity').get_array())

    def _test_average_trsf(self, method):
        out_trsf = inv_trsf(self.trsf)
        # trsf = trsfs_averaging([self.trsf, out_trsf], method=method, trsf_type='rigid')
        # the `setUp` create an AFIINE transfo and not a rigid transfo... this is VT stuff!
        trsf = trsfs_averaging([self.trsf, out_trsf], method=method, trsf_type='affine')
        self.assertIsInstance(trsf, Trsf)
        self.assertIsNotNone(trsf.get_array())

    def test_average_trsf(self):
        for method in ['mean', 'robust-mean']:
            self._test_average_trsf(method)


class TestAffineTrsf(unittest.TestCase):

    def setUp(self) -> None:
        self.trsf = create_trsf('random', trsf_type='affine')

    def tearDown(self) -> None:
        del self.trsf
        gc.collect()

    def test_inv_trsf(self):
        out_trsf = inv_trsf(self.trsf)
        self.assertIsInstance(out_trsf, Trsf)
        self.assertIsNotNone(out_trsf.get_array())
        self.assertTrue(out_trsf.is_linear())

    def _test_apply_trsf_to_intensity(self, interpolation):
        image = get_synthetic_data("medium", "wall")
        out_img = apply_trsf(image, self.trsf, interpolation=interpolation)
        self.assertIsInstance(out_img, SpatialImage)
        self.assertIsNotNone(out_img.get_array())

    def _test_apply_trsf_to_labelled(self, interpolation):
        image = get_synthetic_data("medium", "labelled")
        kwargs = {'interpolation': interpolation}
        if interpolation == 'cellbased':
            kwargs.update({'cell_based_sigma': 2})
        out_img = apply_trsf(image, self.trsf, **kwargs)
        self.assertIsInstance(out_img, LabelledImage)
        self.assertIsNotNone(out_img.get_array())

    def test_apply_trsf(self):
        for interpolation in GRAY_INTERPOLATION_METHODS:
            self._test_apply_trsf_to_intensity(interpolation)
        for interpolation in LABEL_INTERPOLATION_METHODS:
            self._test_apply_trsf_to_labelled(interpolation)

    def test_compose_trsf(self):
        out_trsf = inv_trsf(self.trsf)
        trsf = compose_trsf([self.trsf, out_trsf])
        self.assertIsInstance(trsf, Trsf)
        np.testing.assert_almost_equal(trsf.get_array(),
                                       create_trsf('identity').get_array())

    def _test_average_trsf(self, method):
        out_trsf = inv_trsf(self.trsf)
        trsf = trsfs_averaging([self.trsf, out_trsf], method=method, trsf_type='affine')
        self.assertIsInstance(trsf, Trsf)
        self.assertIsNotNone(trsf.get_array())

    def test_average_trsf(self):
        for method in ['mean', 'robust-mean']:
            self._test_average_trsf(method)


class TestVectorFieldTrsf(unittest.TestCase):

    def setUp(self) -> None:
        self.trsf = create_trsf('sinus', template_img=random_spatial_image((3, 5, 5)), trsf_type='vectorfield')

    def tearDown(self) -> None:
        del self.trsf
        gc.collect()

    def test_inv_trsf(self):
        out_trsf = inv_trsf(self.trsf)
        self.assertIsInstance(out_trsf, Trsf)
        self.assertIsNotNone(out_trsf.get_array())
        self.assertTrue(out_trsf.is_vectorfield())
        del out_trsf

    def _test_apply_trsf_to_intensity(self, interpolation):
        image = get_synthetic_data("medium", "wall")
        out_img = apply_trsf(image, self.trsf, interpolation=interpolation)
        self.assertIsInstance(out_img, SpatialImage)
        self.assertIsNotNone(out_img.get_array())
        del image, out_img

    def _test_apply_trsf_to_labelled(self, interpolation):
        image = get_synthetic_data("medium", "labelled")
        kwargs = {'interpolation': interpolation}
        if interpolation == 'cellbased':
            kwargs.update({'cell_based_sigma': 2})
        out_img = apply_trsf(image, self.trsf, **kwargs)
        self.assertIsInstance(out_img, LabelledImage)
        self.assertIsNotNone(out_img.get_array())
        del image, out_img

    def test_apply_trsf(self):
        for interpolation in ["linear"]:
            self._test_apply_trsf_to_intensity(interpolation)
        for interpolation in LABEL_INTERPOLATION_METHODS:
            self._test_apply_trsf_to_labelled(interpolation)

    def test_compose_trsf(self):
        out_trsf = inv_trsf(self.trsf)
        trsf = compose_trsf([self.trsf, out_trsf])
        self.assertIsInstance(trsf, Trsf)
        self.assertIsNotNone(trsf.get_array())
        del trsf, out_trsf

    def _test_average_trsf(self, method):
        out_trsf = inv_trsf(self.trsf)
        trsf = trsfs_averaging([self.trsf, out_trsf], method=method, trsf_type='vectorfield')
        self.assertIsInstance(trsf, Trsf)
        self.assertIsNotNone(trsf.get_array())
        del trsf, out_trsf

    def test_average_trsf(self):
        for method in ['mean', 'robust-mean']:
            self._test_average_trsf(method)


if __name__ == '__main__':
    unittest.main()
