#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import gc
import unittest

from tests.make_test_data import get_synthetic_data
from timagetk.algorithms.regionalext import regional_extrema
from timagetk.components.labelled_image import LabelledImage


class TestRegionalExtrema(unittest.TestCase):

    def setUp(self) -> None:
        self.image = get_synthetic_data("medium", "wall")

    def tearDown(self) -> None:
        del self.image
        gc.collect()

    def _test_labelled_image(self, out_img):
        self.assertIsInstance(out_img, LabelledImage)
        self.assertIsNotNone(out_img.get_array())

    def test_local_minima(self):
        out_img = regional_extrema(self.image, height=5, method='minima')
        self._test_labelled_image(out_img)

    def test_local_maxima(self):
        out_img = regional_extrema(self.image, height=5, method='maxima')
        self._test_labelled_image(out_img)


class TestRegionalExtremaMultiChannel(unittest.TestCase):

    def setUp(self) -> None:
        self.image = get_synthetic_data("medium", "multichannel")

    def tearDown(self) -> None:
        del self.image
        gc.collect()

    def _test_labelled_image(self, out_img):
        self.assertIsInstance(out_img, LabelledImage)
        self.assertIsNotNone(out_img.get_array())

    def test_local_minima(self):
        out_img = regional_extrema(self.image, channel='wall', height=5, method='minima')
        self._test_labelled_image(out_img)

    def test_local_maxima(self):
        out_img = regional_extrema(self.image, channel='nuclei', height=5, method='maxima')
        self._test_labelled_image(out_img)


if __name__ == '__main__':
    unittest.main()
