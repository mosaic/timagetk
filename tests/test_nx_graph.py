import unittest

from timagetk.graphs.nx_graph import udict
from timagetk.graphs.nx_graph import Graph


class TestUdict(unittest.TestCase):

    def test_udict_initialization(self):
        u_edges = udict({(2, 1): {"area": 25.3}})
        self.assertEqual(u_edges[(1, 2)], {"area": 25.3})
        self.assertEqual(u_edges[(2, 1)], {"area": 25.3})

    def test_udict_set_item(self):
        u_edges = udict()
        u_edges[(3, 4)] = {"weight": 10.5}
        self.assertIn((3, 4), u_edges)
        self.assertEqual(u_edges[(4, 3)], {"weight": 10.5})

    def test_udict_update(self):
        u_edges = udict()
        u_edges.update({(5, 6): {"cost": 7.8}})
        self.assertEqual(u_edges[(5, 6)], {"cost": 7.8})

    def test_udict_invalid_key(self):
        u_edges = udict({(2, 1): {"area": 25.3}})
        with self.assertRaises(KeyError):
            _ = u_edges[(3, 4)]

    def test_udict_repr(self):
        u_edges = udict({(2, 1): {"area": 25.3}})
        repr_output = repr(u_edges)
        self.assertTrue(repr_output.startswith("udict"))
        self.assertTrue("(1, 2)" in repr_output)


class TestGraph(unittest.TestCase):
    def setUp(self) -> None:
        self.graph = Graph()

    def tearDown(self) -> None:
        del self.graph

    def test_add_single_node(self):
        self.graph.add_node(1)
        self.assertIn(1, self.graph.nodes())
        self.assertEqual(len(self.graph.nodes()), 1)

    def test_add_multiple_nodes(self):
        nodes = [1, (1, 2), (2, 3)]
        self.graph.add_nodes_from(nodes)
        for node in nodes:
            self.assertIn(node, self.graph.nodes())
        self.assertEqual(len(self.graph.nodes()), len(nodes))

    def test_add_single_edge(self):
        self.graph.add_nodes_from([1, 2])
        self.graph.add_edge(1, 2)
        self.assertIn((1, 2), self.graph.edges())

    def test_add_multiple_edges(self):
        edges = [(1, 2), (2, 3), (3, 4)]
        self.graph.add_edges_from(edges)
        for edge in edges:
            self.assertIn(edge, self.graph.edges())
        self.assertEqual(len(self.graph.edges()), len(edges))

    def test_add_property_to_node(self):
        self.graph.add_node(1, test='foo')
        self.assertEqual(self.graph.nodes[1]["test"], 'foo')

    def test_add_property_to_edge(self):
        self.graph.add_edges_from([(1, 2)], test=0)
        self.assertEqual(self.graph.edges[(1, 2)]["test"], 0)

    def test_get_node_data_single(self):
        self.graph.add_node(1, test='bar')
        node_data = self.graph.get_node_data(1)
        self.assertEqual(node_data, {"test": 'bar'})

    def test_get_node_data_multiple(self):
        self.graph.add_nodes_from([1, 2], key='value')
        node_data = {n_id : self.graph.get_node_data(n_id) for n_id in self.graph.nodes()}
        self.assertEqual(node_data, {1: {"key": "value"}, 2: {"key": "value"}})

    def test_get_edge_data(self):
        self.graph.add_edge(1, 2, weight=5)
        edge_data = self.graph.get_edge_data(1, 2)
        self.assertEqual(edge_data, {"weight": 5})

if __name__ == '__main__':
    unittest.main()
