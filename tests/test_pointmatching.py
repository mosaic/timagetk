#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import gc
import unittest

from timagetk import Trsf
from timagetk.algorithms.pointmatching import pointmatching
from timagetk.array_util import dummy_spatial_image_3D


class TestPointMatching(unittest.TestCase):

    def setUp(self) -> None:
        self.flo_points = [[0, 0, 0], [0, 0, 1], [0, 1, 0], [1, 0, 0]]
        self.ref_points = [[0, 0, 0], [0, 0, 2], [0, 2, 0], [2, 0, 0]]

    def tearDown(self) -> None:
        del self.flo_points
        del self.ref_points

    def _test_trsf(self, trsf):
        self.assertIsInstance(trsf, Trsf)
        self.assertIsNotNone(trsf.get_array())

    def test_rigid_pointmatching(self):
        trsf = pointmatching(self.flo_points, self.ref_points, method="rigid")
        self._test_trsf(trsf)
        self.assertTrue(trsf.is_linear())

    def test_affine_pointmatching(self):
        trsf = pointmatching(self.flo_points, self.ref_points, method="affine")
        self._test_trsf(trsf)
        self.assertTrue(trsf.is_linear())

    def test_vectorfield_pointmatching(self):
        trsf = pointmatching(self.flo_points, self.ref_points, dummy_spatial_image_3D(), method="vectorfield",
                             fluid_sigma=5.0, vector_propagation_distance=20.0, vector_fading_distance=3.0)
        self._test_trsf(trsf)
        self.assertTrue(trsf.is_vectorfield())


if __name__ == '__main__':
    unittest.main()
    gc.collect()