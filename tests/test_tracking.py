#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import unittest

from timagetk.algorithms.trsf import apply_trsf, create_trsf
from timagetk.io.dataset import shared_data

from timagetk.third_party.ctrl.tracking import CellTracking, IterativeCellTracking

class MyTestCase(unittest.TestCase):
    def setUp(self):
        # - Create dummy images at t and t+1 with affine deformation
        self.daughter_img = shared_data('synthetic', 'wall')
        self.daughter_seg = shared_data('synthetic', 'labelled')
        x, y, z = [s * vx / 2 for s, vx in zip(self.daughter_seg.get_shape(), self.daughter_seg.get_voxelsize())]
        trsf = create_trsf('random', trsf_type='affine', angle_range=[0., 0.], translation_range=[0., 0.],
                           scale_range=[1.2, 1.2], shear_range=[0., 0.], params=f'-fixedpoint {x} {y} {z}')
        self.mother_img = apply_trsf(self.daughter_img, trsf)
        self.mother_seg = apply_trsf(self.daughter_seg, trsf, interpolation='cellbased', cell_based_sigma=1)

        self.tracking = CellTracking(
            mother_img=self.mother_img,
            daughter_img=self.daughter_img,
            mother_seg=self.mother_seg,
            daughter_seg=self.daughter_seg,
            reg_lowest_level=3)
        self.tracking.init_lineage()

    def test_eval_cost_from_overlap(self):
        """Test the evaluation of cost from overlap."""
        self.tracking.eval_cost_from_overlap()
        self.assertIsNotNone(self.tracking.cost_dict)
        self.assertIsInstance(self.tracking.cost_dict, dict)
        self.assertGreater(len(self.tracking.cost_dict), 0)

    def test_naive_lineage(self):
        """Test the naive lineage method."""
        lineage_dict = self.tracking.naive_lineage()
        self.assertIsInstance(lineage_dict, dict)

        for key, value_list in lineage_dict.items():
            self.assertEqual(value_list[0], key)

    def test_verify_initialization(self):
        """Test the initialization method."""
        self.tracking._check_init()
        self.assertEqual(self.tracking.trsf_direction, 'backward')

    def test_iterative_initialization(self):
        """Test the initialization of IterativeCellTracking."""
        iterative_tracking = IterativeCellTracking(
            mother_img=self.mother_img,
            daughter_img=self.daughter_img,
            mother_seg=self.mother_seg,
            daughter_seg=self.daughter_seg,
            reg_lowest_level=3,
            max_iter=5)
        self.assertEqual(iterative_tracking.niter, 0)
        self.assertIsNone(iterative_tracking.lineage_dict)
        self.assertIsInstance(iterative_tracking.previous_lineage, dict)
        self.assertIsInstance(iterative_tracking.adj_graph_daughter, type(None))

        iterative_tracking.iterative_naive_tracking()


if __name__ == '__main__':
    unittest.main()
