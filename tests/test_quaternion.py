import unittest

import numpy as np
from timagetk.algorithms.quaternion import _axis_str2tuple
from timagetk.algorithms.quaternion import _axis_translation
from timagetk.algorithms.quaternion import centered_rotation_trsf
from timagetk.algorithms.quaternion import quaternion_rotation_matrix
from timagetk.algorithms.quaternion import quaternion_translation_matrix
from timagetk.algorithms.quaternion import rotation_matrix_from_vec
from timagetk.algorithms.quaternion import ssc
from timagetk.algorithms.quaternion import translation_trsf
from timagetk.algorithms.quaternion import vector_rotation_trsf
from timagetk.algorithms.trsf import apply_trsf
from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image


class TestQuaternionHiddenMethods(unittest.TestCase):

    def test_axis_x(self):
        result = _axis_str2tuple('x')
        self.assertEqual(result, (1, 0, 0))

    def test_axis_y(self):
        result = _axis_str2tuple('y')
        self.assertEqual(result, (0, 1, 0))

    def test_axis_z(self):
        result = _axis_str2tuple('z')
        self.assertEqual(result, (0, 0, 1))

    def test_unknown_axis(self):
        with self.assertRaises(ValueError):
            _axis_str2tuple('a')

    def test_axis_x_translation(self):
        point = [10., 5.]
        expected_output = [0., 10., 5.]
        result = _axis_translation('x', point)
        self.assertEqual(result, expected_output)

    def test_axis_y_translation(self):
        point = [10., 5.]
        expected_output = [10., 0., 5.]
        result = _axis_translation('y', point)
        self.assertEqual(result, expected_output)

    def test_axis_z_translation(self):
        point = [10., 5.]
        expected_output = [10., 5., 0.]
        result = _axis_translation('z', point)
        self.assertEqual(result, expected_output)

    def test_unknown_axis_translation(self):
        point = [10., 5.]
        with self.assertRaises(ValueError):
            _axis_translation('k', point)


class TestQuaternionMethods(unittest.TestCase):

    def assert_array_equal(self, arr1, arr2, msg=""):
        return np.testing.assert_array_equal(arr1, arr2, msg)

    def assert_array_almost_equal(self, arr1, arr2, msg="", dec=6):
        return np.testing.assert_array_almost_equal(arr1, arr2, decimal=dec, err_msg=msg)

    def test_quaternion_types(self):
        qtm = quaternion_translation_matrix(1, 2, 3)
        self.assertIsInstance(qtm, np.ndarray, "Returned object is not a numpy array.")

    def test_quaternion_values(self):
        qtm = quaternion_translation_matrix(1, 2, 3)
        expected_output = np.array([[1., 0., 0., 1.],
                                    [0., 1., 0., 2.],
                                    [0., 0., 1., 3.],
                                    [0., 0., 0., 1.]])

        self.assert_array_almost_equal(qtm, expected_output, "Returned matrix is not as expected.")

    def test_quaternion_identical_inputs(self):
        qtm = quaternion_translation_matrix(2, 2, 2)
        expected_output = np.array([[1., 0., 0., 2.],
                                    [0., 1., 0., 2.],
                                    [0., 0., 1., 2.],
                                    [0., 0., 0., 1.]])

        self.assert_array_almost_equal(qtm, expected_output,
                                       "Returned matrix is not as expected with identical inputs.")

    def test_quaternion_zero_translation(self):
        qtm = quaternion_translation_matrix(0, 0, 0)
        expected_output = np.array([[1., 0., 0., 0.],
                                    [0., 1., 0., 0.],
                                    [0., 0., 1., 0.],
                                    [0., 0., 0., 1.]])

        self.assert_array_almost_equal(qtm, expected_output,
                                       "Returned matrix is not as expected with zero translation.")


class TestQuaternionSSC(unittest.TestCase):

    def assert_array_equal(self, arr1, arr2, msg=""):
        return np.testing.assert_array_equal(arr1, arr2, msg)

    def assert_array_almost_equal(self, arr1, arr2, msg="", dec=6):
        return np.testing.assert_array_almost_equal(arr1, arr2, decimal=dec, err_msg=msg)

    def test_ssc(self):
        # Test with 3-element list
        vec = [1, 2, 3]
        expected = np.array([[0, -3, 2], [3, 0, -1], [-2, 1, 0]])
        result = ssc(vec)
        self.assert_array_equal(result, expected)

        # Test with 3-element numpy array
        vec = np.array([1, 2, 3])
        expected = np.array([[0, -3, 2], [3, 0, -1], [-2, 1, 0]])
        result = ssc(vec)
        self.assert_array_equal(result, expected)

        # Test with non-3-element list
        vec = [1, 2]
        with self.assertRaises(IndexError):
            ssc(vec)

        # Test with non-3-element numpy array
        vec = np.array([1, 2])
        with self.assertRaises(IndexError):
            ssc(vec)

        # Test with non-vector input
        non_vector = "this is not a vector"
        with self.assertRaises(TypeError):
            ssc(non_vector)


class TestQuaternionRotationMatrixFromVector(unittest.TestCase):

    def assert_array_equal(self, arr1, arr2, msg=""):
        return np.testing.assert_array_equal(arr1, arr2, msg)

    def assert_array_almost_equal(self, arr1, arr2, msg="", dec=6):
        return np.testing.assert_array_almost_equal(arr1, arr2, decimal=dec, err_msg=msg)

    def test_rotation_matrix_from_vec(self):
        # Test with correct inputs
        a = [1, 0, 0]
        b = [0, 1, 0]
        expected = np.array([[0., -1., 0.],
                             [1., 0., 0.],
                             [0., 0., 1.]], dtype=np.float64)

        self.assert_array_almost_equal(rotation_matrix_from_vec(a, b), expected)

        # Test to throw ValueError when input vectors are null
        a = [0, 0, 0]
        b = [0, 0, 0]

        with self.assertRaises(ValueError) as exception_context:
            rotation_matrix_from_vec(a, b)

        self.assertEqual('Input vectors must be non-zero.', str(exception_context.exception))

        # Test when input vectors are parallel
        a = [1, 2, 3]
        b = [2, 4, 6]
        expected = np.eye(3)

        self.assert_array_almost_equal(rotation_matrix_from_vec(a, b), expected)

        # Test when input vectors are perpendicular
        a = [1, 0, 0]
        b = [0, 1, 0]
        expected = np.array([[0., -1., 0.],
                             [1., 0., 0.],
                             [0., 0., 1.]], dtype=np.float64)

        self.assert_array_almost_equal(rotation_matrix_from_vec(a, b), expected)


class TestQuaternionRotationMatrix(unittest.TestCase):

    def setUp(self):
        # tolerances for assertion
        self.abs_tol = 1e-9
        self.rel_tol = 1e-9

    def test_axis_x(self):
        q = quaternion_rotation_matrix(90, "x")
        expected = np.array([[1, 0, 0, 0], [0, 0, -1, 0], [0, 1, 0, 0], [0, 0, 0, 1]])

        self.assertTrue(np.allclose(q, expected, atol=self.abs_tol, rtol=self.rel_tol))

    def test_axis_y(self):
        q = quaternion_rotation_matrix(90, "y")
        expected = np.array([[0, 0, 1, 0], [0, 1, 0, 0], [-1, 0, 0, 0], [0, 0, 0, 1]])

        self.assertTrue(np.allclose(q, expected, atol=self.abs_tol, rtol=self.rel_tol))

    def test_axis_z(self):
        q = quaternion_rotation_matrix(90, "z")
        expected = np.array([[0, -1, 0, 0], [1, 0, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])

        # allclose checks if all elements are close enough within the tolerance
        self.assertTrue(np.allclose(q, expected, atol=self.abs_tol, rtol=self.rel_tol))

    def test_angle_range(self):
        with self.assertRaises(AssertionError):
            q = quaternion_rotation_matrix(360, "z")

    def test_invalid_axis(self):
        with self.assertRaises(Exception):
            q = quaternion_rotation_matrix(90, "t")


class TestTranslationTrsf(unittest.TestCase):

    def assert_array_equal(self, arr1, arr2, msg=""):
        return np.testing.assert_array_equal(arr1, arr2, msg)

    def assert_array_almost_equal(self, arr1, arr2, msg="", dec=6):
        return np.testing.assert_array_almost_equal(arr1, arr2, decimal=dec, err_msg=msg)

    def test_valid_translations(self):
        # Test valid translations
        trsf1 = translation_trsf(2, 3, 4)
        self.assert_array_equal(trsf1.get_array(), [[1., 0., 0., -2.],
                                                    [0., 1., 0., -3.],
                                                    [0., 0., 1., -4.],
                                                    [0., 0., 0., 1.]])

        trsf2 = translation_trsf(0, 0, 0)
        self.assert_array_almost_equal(trsf2.get_array(), [[1., 0., 0., 0.],
                                                           [0., 1., 0., 0.],
                                                           [0., 0., 1., 0.],
                                                           [0., 0., 0., 1.]])


class TestQuaternionImage(unittest.TestCase):

    def setUp(self):
        self.img = example_layered_sphere_wall_image()

    def tearDown(self) -> None:
        del self.img

    def assert_array_equal(self, arr1, arr2, msg=""):
        return np.testing.assert_array_equal(arr1, arr2, msg)

    def assert_array_almost_equal(self, arr1, arr2, msg="", dec=6):
        return np.testing.assert_array_almost_equal(arr1, arr2, decimal=dec, err_msg=msg)

    def test_centered_rotation_trsf_zaxis(self):
        r_angle = 10
        trsf_z = centered_rotation_trsf(self.img, r_angle, 'z')
        z_rotated_img = apply_trsf(self.img, trsf_z)
        self.assertIsInstance(z_rotated_img, type(self.img))
        img_shape = self.img.get_shape('z')
        self.assertEqual(img_shape, z_rotated_img.get_shape('z'))

    def test_centered_rotation_trsf_xaxis(self):
        r_angle = 10
        trsf_x = centered_rotation_trsf(self.img, r_angle, 'x')
        x_rotated_img = apply_trsf(self.img, trsf_x)
        self.assertIsInstance(x_rotated_img, type(self.img))
        img_shape = self.img.get_shape('z')
        self.assertEqual(img_shape, x_rotated_img.get_shape('z'))

    def test_centered_rotation_trsf_negative_angle(self):
        r_angle = -10
        trsf_x = centered_rotation_trsf(self.img, r_angle, 'x')
        x_rotated_img = apply_trsf(self.img, trsf_x)
        self.assertIsInstance(x_rotated_img, type(self.img))
        img_shape = self.img.get_shape('z')
        self.assertEqual(img_shape, x_rotated_img.get_shape('z'))

    def test_centered_rotation_trsf_invalid_axis(self):
        r_angle = 10
        with self.assertRaises(ValueError):
            centered_rotation_trsf(self.img, r_angle, 'invalid')

    def test_vector_rotation_trsf_identity(self):
        rot_trsf = vector_rotation_trsf(self.img, [1, 0, 0], [1, 0, 0])
        rotated_img = apply_trsf(self.img, rot_trsf)
        self.assert_array_almost_equal(self.img, rotated_img,
                                       msg="Identity rotation is tested.")

    def test_vector_rotation_trsf_rotation(self):
        rot_trsf = vector_rotation_trsf(self.img, [0, 1, 0], [1, 0, 0])
        rotated_img = apply_trsf(self.img, rot_trsf)
        self.assertEqual(self.img.get_shape(), rotated_img.get_shape(),
                         "Match in shape after rotation is tested.")

    def test_vector_rotation_trsf_result(self):
        rot_trsf = vector_rotation_trsf(self.img, [0, 1, 0], [1, 0, 0])
        t_y = int(self.img.get_extent('y') / 2.) * 2
        expected_rot_trsf = np.array([[0., 1., 0., 0.],
                                      [-1., 0., 0., t_y],
                                      [0., 0., 1., 0.],
                                      [0., 0., 0., 1.]])
        self.assert_array_almost_equal(rot_trsf.get_array(), expected_rot_trsf,
                                       msg="Transformation Matrix validity is tested.")


if __name__ == "__main__":
    unittest.main()
