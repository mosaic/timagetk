#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import unittest

import numpy as np

from timagetk.components.labelled_image import LabelledImage
from timagetk.components.labelled_image import relabel_from_mapping
from timagetk.io.image import default_image_attributes

array_2D = np.array([[1, 2, 7, 7, 1, 1],
                     [1, 6, 5, 7, 3, 3],
                     [2, 2, 1, 7, 3, 3],
                     [1, 1, 1, 4, 1, 1]])

# used to set 'not_a_label'
no_id = 0
# list of unique labels found in the array
uniq = list(np.unique(array_2D))
# we invert the label of two neighbors (error-prone!)
mapping = {3: 7,
           7: 3}


class TestLabelledImage(unittest.TestCase):

    def test_init(self):
        """Test correct initialisation of 'not_a_label' property."""
        # - Test init of 'not_a_label' to None:
        lab_im_2d = LabelledImage(array_2D, **default_image_attributes(array_2D))
        self.assertIsNone(lab_im_2d.not_a_label)

        # - Test init of 'not_a_label' to 'no_id':
        lab_im_2d = LabelledImage(array_2D, not_a_label=no_id, **default_image_attributes(array_2D))
        self.assertEqual(lab_im_2d.not_a_label, no_id)

        # - Test init of 'not_a_label' to value obtained from a LabelledImage as input:
        lab_im_2d = LabelledImage(lab_im_2d)
        self.assertEqual(lab_im_2d.not_a_label, no_id)

    def test_labels(self):
        """Test the labels related methods."""
        lab_im_2d = LabelledImage(array_2D, not_a_label=no_id, **default_image_attributes(array_2D))
        self.assertEqual(lab_im_2d.labels(), uniq)
        self.assertEqual(lab_im_2d.nb_labels(), len(uniq))
        self.assertTrue(lab_im_2d.is_label_in_image(2))
        self.assertFalse(lab_im_2d.is_label_in_image(no_id))

    def test_remove_labels_from_image(self):
        """Test label deletion."""
        lab_im_2d = LabelledImage(array_2D, not_a_label=no_id, **default_image_attributes(array_2D))
        # - Replace 2 by 0:
        lab_im_2d.remove_labels_from_image([2])
        # - Check we did not change the object class (a LabelledImage):
        self.assertTrue(isinstance(lab_im_2d, LabelledImage))
        # - Check we have removed '2' from the array:
        self.assertFalse(2 in lab_im_2d.get_array())
        # - Two ways of checking '2' have been removed from labels attribute:
        self.assertFalse(lab_im_2d.is_label_in_image(2))
        self.assertFalse(2 in lab_im_2d.labels())
        # - Check the 'not_a_label' is now in the array:
        self.assertTrue(lab_im_2d.is_label_in_image(no_id))

    def test_relabelling_1(self):
        """Test relabelling method WITHOUT CLEARING UNMAPPED LABELS."""
        # - Manual definition of the relabelled array with 'clear_unmapped=False':
        relab = np.array([[1, 2, 3, 3, 1, 1],
                          [1, 6, 5, 3, 7, 7],
                          [2, 2, 1, 3, 7, 7],
                          [1, 1, 1, 4, 1, 1]])

        lab_im_2d = LabelledImage(array_2D, not_a_label=no_id, **default_image_attributes(array_2D))
        # - Relabel the array using the mapping:
        lab_im_2d = relabel_from_mapping(lab_im_2d, mapping, clear_unmapped=False)
        # --------
        # - TESTS:
        # --------
        # - Check the relabelling is correct:
        np.testing.assert_equal(lab_im_2d, LabelledImage(relab, not_a_label=no_id, **default_image_attributes(relab)))
        # - We should have the same labels list:
        self.assertEqual(lab_im_2d.labels(), uniq)

    def test_relabelling_2(self):
        """Test relabelling method WITH CLEARING UNMAPPED LABELS."""
        # - Manual definition of the relabelled array with 'clear_unmapped=True':
        relab = np.array([[0, 0, 3, 3, 0, 0],
                          [0, 0, 0, 3, 7, 7],
                          [0, 0, 0, 3, 7, 7],
                          [0, 0, 0, 0, 0, 0]])

        lab_im_2d = LabelledImage(array_2D, not_a_label=no_id, **default_image_attributes(array_2D))
        # - Relabel the array using the mapping, WITH CLEARING:
        lab_im_2d = relabel_from_mapping(lab_im_2d, mapping, clear_unmapped=True)
        # --------
        # - TESTS:
        # --------
        # - Check the relabelling is correct:
        np.testing.assert_equal(lab_im_2d, LabelledImage(relab, not_a_label=no_id, **default_image_attributes(relab)))
        # - We should have only the mapping values:
        self.assertListEqual(sorted(lab_im_2d.labels()), sorted(list(mapping.values())))
