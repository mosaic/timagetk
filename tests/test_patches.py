import unittest

import numpy as np

from timagetk.array_util import check_patch_size, make_patches


class TestCheckPatchSize(unittest.TestCase):

    def setUp(self):
        self.image = np.zeros((10, 10, 10))

    def test_patch_size_large(self):
        patch_size = (13, 13, 13)
        expected_patch_size = (10, 10, 10)
        self.assertEqual(check_patch_size(self.image, patch_size), expected_patch_size)

    def test_patch_size_small(self):
        patch_size = (5, 5, 5)
        expected_patch_size = (5, 5, 5)
        self.assertEqual(check_patch_size(self.image, patch_size), expected_patch_size)

    def test_patch_size_equal(self):
        patch_size = (10, 10, 10)
        expected_patch_size = (10, 10, 10)
        self.assertEqual(check_patch_size(self.image, patch_size), expected_patch_size)

    def test_patch_size_mixed(self):
        patch_size = (5, 7, 13)
        expected_patch_size = (5, 7, 10)
        self.assertEqual(check_patch_size(self.image, patch_size), expected_patch_size)



class TestMakePatches(unittest.TestCase):

    def setUp(self):
        self.image = np.zeros((50, 100, 100))

    def test_patches(self):
        patch_size = (50, 50, 50)
        patches = make_patches(self.image, patch_size)
        expected_n_patches = 9
        self.assertEqual(len(patches), expected_n_patches)

    def test_patches_large(self):
        patch_size = (200, 200, 200)
        patches = make_patches(self.image, patch_size)
        expected_n_patches = 1
        self.assertEqual(len(patches), expected_n_patches)
