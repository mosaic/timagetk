#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import filecmp
import logging
import tempfile
import unittest

import numpy as np

from timagetk.array_util import handmade_2d
from timagetk.array_util import handmade_3d
from timagetk.array_util import random_array
from timagetk.array_util import random_spatial_image
from timagetk.components.multi_channel import MultiChannelImage
from timagetk.components.spatial_image import SpatialImage
from timagetk.io import imread
from timagetk.io import imsave
from timagetk.io.image import read_tiff_image
from timagetk.io.image import save_tiff_image
from timagetk.io.util import INR_EXT
from timagetk.io.util import MHA_EXT
from timagetk.io.util import TIF_EXT
from vt.image import Image


class TestHandmadeIO(unittest.TestCase):
    """Test IO methods `imread` & `imsave` using `handmade2D()` & `handmade3D()`."""

    def setUp(self) -> None:
        self.img_2d = SpatialImage(handmade_2d(), voxelsize=[0.3, 0.4])
        self.img_3d = SpatialImage(handmade_3d(), voxelsize=[0.3, 0.4, 0.5])

    def tearDown(self) -> None:
        del self.img_2d
        del self.img_3d

    def _test_handmade_IO(self, img, img_fmt):
        """Save the SpatialImage from handmade array, read it and compare to original.

        Parameters
        ----------
        handmade_func : fun
            The function returning the handmade array.
        img_fmt : str
            The image format to use for saving the copy, *e.g.* '.tif'
        """
        tmpf = tempfile.NamedTemporaryFile(suffix=img_fmt)
        imsave(tmpf.name, img, force=True)
        img_cp = imread(tmpf.name)
        np.testing.assert_array_equal(img, img_cp)
        np.testing.assert_array_almost_equal(img.voxelsize, img_cp.voxelsize, decimal=6)
        tmpf.close()

    def test_io_2d_mha(self):
        """Test for handmade 2D images."""
        self._test_handmade_IO(self.img_2d, '.mha')

    def test_io_3d_mha(self):
        """Test for handmade 3D images."""
        self._test_handmade_IO(self.img_3d, '.mha')

    def test_io_2d_inr(self):
        """Test for handmade 2D images."""
        self._test_handmade_IO(self.img_2d, '.inr')

    def test_io_3d_inr(self):
        """Test for handmade 3D images."""
        self._test_handmade_IO(self.img_3d, '.inr')

    def test_io_2d_inr_gz(self):
        """Test for handmade 2D images."""
        self._test_handmade_IO(self.img_2d, '.inr.gz')

    def test_io_3d_inr_gz(self):
        """Test for handmade 3D images."""
        self._test_handmade_IO(self.img_3d, '.inr.gz')

    # def test_io_2d_czi(self):
    #     """Test for handmade 2D images."""
    #     self._test_handmade_IO(self.img_2d, '.czi')

    # def test_io_3d_czi(self):
    #     """Test for handmade 3D images."""
    #     self._test_handmade_IO(self.img_3d, '.czi')

    # def test_io_2d_lsm(self):
    #     """Test for handmade 2D images."""
    #     self._test_handmade_IO(self.img_2d, '.lsm')

    # def test_io_3d_lsm(self):
    #     """Test for handmade 3D images."""
    #     self._test_handmade_IO(self.img_3d, '.lsm')

    def test_io_2d_tif(self):
        """Test for handmade 2D images."""
        self._test_handmade_IO(self.img_2d, '.tif')

    def test_io_3d_tif(self):
        """Test for handmade 3D images."""
        self._test_handmade_IO(self.img_3d, '.tif')


class TestRandomImageIO(unittest.TestCase):
    """Test IO methods `imread` & `imsave` using randomly generated images."""

    def _save_random_image(self, ext):
        """Generate a random image and save it to given format.

        Parameters
        ----------
        ext : str
            Image file format to use for saving the image.

        See Also
        --------
        ``timagetk.io.util.POSS_EXT``: the list of available image formats.
        """
        img = random_spatial_image((3, 4, 5))
        # Write copy:
        cp_file = tempfile.NamedTemporaryFile(suffix=f"{ext}")
        imsave(cp_file.name, img, force=True)
        cp_file.close()  # can close now since it only test the writer is OK

    def _test_random_image_IO(self, shape, ext, dtype):
        """Test if saved random SpatialImage IO is correct by comparing array and attributes of original and saved+read image.

        Parameters
        ----------
        shape : list of int
            Shape of the random image to create.
        ext : str
            Image file format to use for saving the image.
        dtype : {"uint8", "unit16"}
            Numpy `dtype` to use to generate the image.

        See Also
        --------
        timagetk.io.util.POSS_EXT
        """
        # ------------------------------- SETUP --------------------------------
        if len(shape) == 2:
            vxs = [0.3, 0.2]
        else:
            vxs = [0.3, 0.2, 0.5]
        # Generate a random SpatialImage:
        img = random_spatial_image(shape, voxelsize=vxs, dtype=dtype)
        # Save it using `imsave` function:
        rand_file = tempfile.NamedTemporaryFile(suffix=f"{ext}")
        imsave(rand_file.name, img, force=True)
        # Read it using `imread` function:
        img_cp = imread(rand_file.name)
        # -------------------------------- TEST --------------------------------
        # - COMPARE `img` & `img_cp` image attributes and array:
        np.testing.assert_almost_equal(img.voxelsize, img_cp.voxelsize, 6)
        np.testing.assert_almost_equal(img.shape, img_cp.shape, 6)
        np.testing.assert_almost_equal(img.origin, img_cp.origin, 6)
        np.testing.assert_almost_equal(img.extent, img_cp.extent, 6)
        np.testing.assert_equal(img.dtype, img_cp.dtype)
        # md_dict = img.metadata_image.get_dict()
        # self.assertDictEqual(md_dict, img2.metadata_image.get_dict())
        self.assertEqual(img.ndim, img_cp.ndim)
        # Test 'filename' & 'filepath' attribute definition:
        self.assertTrue(img_cp.filename != "", msg="Attribute 'filename' is not defined!")
        self.assertTrue(img_cp.filepath != "", msg="Attribute 'filepath' is not defined!")
        np.testing.assert_equal(img.get_array(), img_cp.get_array())
        rand_file.close()

    def _compare_write_imsave(self, shape, ext, dtype):
        """Test if ``imsave`` from ``timagetk.io.image`` function generates similar files than ``vtImage.write()``.

        Parameters
        ----------
        shape : list of int
            Shape of the random image to create.
        ext : str
            Image file format to use for saving the image.
        dtype : {"uint8", "unit16"}
            Numpy `dtype` to use to generate the image.

        See Also
        --------
        timagetk.io.util.POSS_EXT
        """
        logging.debug(f"Given image `shape`: {shape}")
        logging.debug(f"Given image `ext`: {ext}")
        logging.debug(f"Given image `dtype`: {dtype}")
        # ------------------------------- SETUP --------------------------------
        if len(shape) == 2:
            vxs = [0.3, 0.2]
        else:
            vxs = [0.3, 0.2, 0.5]
        # Generate a random array:
        arr = random_array(shape, dtype)
        # Use the random array to make a ``vt.Image``:
        vtim = Image(arr, vxs)
        # Use the random array to make a ``SpatialImage``:
        spim = SpatialImage(arr, voxelsize=vxs)
        # - Save vtImage (write method) & SpatialImage (imsave function) & compare files:
        file = tempfile.NamedTemporaryFile()
        fname = "{}_{}{}"
        vt_fname = fname.format(file.name, 'vt', ext)
        sp_fname = fname.format(file.name, 'sp', ext)
        vtim.write(vt_fname)
        imsave(sp_fname, spim, force=True)
        logging.debug("Temporary vt file: {}".format(vt_fname))
        logging.debug("Temporary sp file: {}".format(sp_fname))
        # -------------------------------- TEST --------------------------------
        self.assertTrue(filecmp.cmp(vt_fname, sp_fname, shallow=False))
        file.close()

    def test_save_random_inr(self):
        """Save a randomly generated SpatialImage as INR, INR.GZ & INR.ZIP. """
        for img_fmt in ['.inr', '.inr.gz', '.inr.zip']:
            self._save_random_image(img_fmt)

    def test_save_random_tif(self):
        """Save a randomly generated SpatialImage as TIF & TIFF. """
        for img_fmt in ['.tif', '.tiff']:
            self._save_random_image(img_fmt)

    def test_save_random_mha(self):
        """Save a randomly generated SpatialImage as MHA. """
        self._save_random_image('.mha')

    def test_save_n_read_random(self):
        """Save and read a randomly generated SpatialImage. """
        for shape in [(3, 5), (3, 4, 5)]:
            for ext in INR_EXT + MHA_EXT + TIF_EXT:
                for dtype in ['uint8', 'uint16']:
                    self._test_random_image_IO(shape, ext, dtype)

    def test_write_imsave(self):
        """Performs the comparison of files obtained from ``vtImage.write()``
        method & ``timagetk.io.imsave`` function for 2D/3D images for every
        known extensions for the two types of data: unsigned 8-bits & 16-bits.
        """
        for shape in [(3, 5), (3, 4, 5)]:
            for ext in INR_EXT + MHA_EXT:
                for dtype in ['uint8', 'uint16']:
                    self._compare_write_imsave(shape, ext, dtype)


# class TestReadImageIO(unittest.TestCase):
#     # Read REAL SpatialImage:
#
#     # --------------------------------------------------------------------------
#     # 2D images:
#     # --------------------------------------------------------------------------
#     def test_read_tif_2D(self):
#         """Read a 2D TIFF file and make sure image metadata are not empty. """
#         img = imread(shared_data('io_2D.tif'))
#         self.assertTrue(img.metadata_image.get_dict() != {})
#
#     def test_read_mha_2D(self):
#         """Read a 2D MHA file and make sure image metadata are not empty. """
#         img = imread(shared_data('io_2D.mha'))
#         self.assertTrue(img.metadata_image.get_dict() != {})
#
#     def test_read_ometif_2D(self):
#         """Read a 2D OME-TIFF file and make sure image metadata are not empty. """
#         img = imread(shared_data('io_2D.ome.tif'))
#         self.assertTrue(img.metadata_image.get_dict() != {})
#
#     def test_read_lsm_2D(self):
#         """Read a 2D LSM image and make sure image metadata are not empty. """
#         img = imread(shared_data('io_2D.lsm'))
#         self.assertTrue(img.metadata_image.get_dict() != {})
#
#     def test_read_inr_2D(self):
#         """Read a 2D INR file and make sure image metadata are not empty. """
#         img = imread(shared_data('io_2D.inr'))
#         self.assertTrue(img.metadata_image.get_dict() != {})
#
#     # --------------------------------------------------------------------------
#     # 3D images:
#     # --------------------------------------------------------------------------
#     def test_read_tif_3D(self):
#         """Read a 3D TIFF file and make sure image metadata are not empty. """
#         img = imread(shared_data('io_3D.tif'))
#         self.assertTrue(img.metadata_image.get_dict() != {})
#
#     def test_read_mha_3D(self):
#         """Read a 3D MHA file and make sure image metadata are not empty. """
#         img = imread(shared_data('io_3D.mha'))
#         self.assertTrue(img.metadata_image.get_dict() != {})
#
#     def test_read_ometif_3D(self):
#         """Read a 3D TIFF file and make sure image metadata are not empty. """
#         img = imread(shared_data('io_3D.ome.tif'))
#         self.assertTrue(img.metadata_image.get_dict() != {})
#
#     def test_read_lsm_3D(self):
#         """Read a 3D LSM image and make sure image metadata are not empty. """
#         img = imread(shared_data('io_3D.lsm'))
#         self.assertTrue(img.metadata_image.get_dict() != {})
#
#     def test_read_inr_3D(self):
#         """Read a 3D INR file and make sure image metadata are not empty. """
#         img = imread(shared_data('io_3D.inr'))
#         self.assertTrue(img.metadata_image.get_dict() != {})


class Test_vtImageIO(unittest.TestCase):
    """Test IO methods from vtImage:
        1. READ a 2D/3D tiff file using vtImage;
        2. WRITE a temporary copy in a known format;
        3. READ the temporary copy;
        4. COMPARE the original and copy (
    """

    # Compare REAL SpatialImage:

    def _compare_read_save_fmt_vt(self, dim, fmt):
        if dim == '2D':
            init_img = Image(handmade_2d())
        else:
            init_img = Image(handmade_2d())
        # Write copy:
        cp_file = tempfile.NamedTemporaryFile()
        cp_fname = f"{cp_file.name}.{fmt}"
        init_img.write(cp_fname)
        # Load copy:
        cp_img = Image(cp_fname)
        # Compare ORIGINAL and COPY:
        np.testing.assert_almost_equal(init_img.copy_to_array(), cp_img.copy_to_array(), decimal=6)
        np.testing.assert_equal(init_img.shape(), cp_img.shape())
        cp_file.close()

    def test_read_tif_save_tif_compare_2D_vt(self):
        """Read a 2D TIFF image, save it as TIF, read it and assert equal. """
        self._compare_read_save_fmt_vt('2D', 'tif')

    def test_read_tif_save_mha_compare_2D_vt(self):
        """Read a 2D TIFF image, save it as MHA, read it and assert equal. """
        self._compare_read_save_fmt_vt('2D', 'mha')

    def test_read_tif_save_lsm_compare_2D_vt(self):
        """Read a 2D TIFF image, save it as LSM, read it and assert equal. """
        self._compare_read_save_fmt_vt('2D', 'lsm')

    def test_read_tif_save_inr_compare_2D_vt(self):
        """Read a 2D TIFF image, save it as INR, read it and assert equal. """
        self._compare_read_save_fmt_vt('2D', 'inr')

    def test_read_tif_save_tif_compare_3D_vt(self):
        """Read a 3D TIFF image, save it as TIF, read it and assert equal. """
        self._compare_read_save_fmt_vt('3D', 'tif')

    def test_read_tif_save_mha_compare_3D_vt(self):
        """Read a 3D TIFF image, save it as MHA, read it and assert equal. """
        self._compare_read_save_fmt_vt('3D', 'mha')

    def test_read_tif_save_lsm_compare_3D_vt(self):
        """Read a 3D TIFF image, save it as LSM, read it and assert equal. """
        self._compare_read_save_fmt_vt('3D', 'lsm')

    def test_read_tif_save_inr_compare_3D_vt(self):
        """Read a 3D TIFF image, save it as INR, read it and assert equal. """
        self._compare_read_save_fmt_vt('3D', 'inr')


# class Test_timagetkIO(unittest.TestCase):
#     # Compare REAL SpatialImage:
#
#     def _compare_read_save_fmt(self, dim, fmt):
#         # Load TIF:
#         if dim == '2D':
#             init_img = vtImage(handmade_2d())
#         else:
#             init_img = vtImage(handmade_2d())
#         tmp_path = tempfile.NamedTemporaryFile()
#         img_path = f"{tmp_path.name}.{fmt}"
#         # Write TIF from SpatialImage:
#         cp_img_path = shared_data('io_{}.{}'.format(dim, fmt), 'results')
#         imsave(cp_img_path, init_img)
#         filecmp.cmp(img_path, cp_img_path, shallow=False)
#
#         # Load TIFF:
#         cp_img = imread(cp_img_path)
#         # Image metadata should be the same:
#         # md_inr = inr_img.metadata_image.get_dict()
#         # md_tif = tif_img.metadata_image.get_dict()
#         # self.assertDictEqual(md_inr, md_tif)
#         np.testing.assert_equal(init_img.voxelsize, cp_img.voxelsize)
#         np.testing.assert_equal(init_img.shape, cp_img.shape)
#         np.testing.assert_equal(init_img.origin, cp_img.origin)
#         np.testing.assert_equal(init_img.dtype, cp_img.dtype)
#         np.testing.assert_equal(init_img.extent, cp_img.extent)
#         self.assertEqual(init_img.ndim, cp_img.ndim)
#         np.testing.assert_array_equal(init_img, cp_img)
#         os.remove(cp_img_path)
#         tmp_path.close()
#
#
#     def test_read_tif_save_tif_compare_2D(self):
#         self._compare_read_save_fmt('2D', 'tif')
#
#     def test_read_tif_save_mha_compare_2D(self):
#         self._compare_read_save_fmt('2D', 'mha')
#
#     def test_read_tif_save_lsm_compare_2D(self):
#         self._compare_read_save_fmt('2D', 'lsm')
#
#     def test_read_tif_save_inr_compare_2D(self):
#         self._compare_read_save_fmt('2D', 'inr')
#
#     def test_read_tif_save_tif_compare_3D(self):
#         self._compare_read_save_fmt('3D', 'tif')
#
#     def test_read_tif_save_mha_compare_3D(self):
#         self._compare_read_save_fmt('3D', 'mha')
#
#     def test_read_tif_save_lsm_compare_3D(self):
#         self._compare_read_save_fmt('3D', 'lsm')
#
#     def test_read_tif_save_inr_compare_3D(self):
#         self._compare_read_save_fmt('3D', 'inr')


class TestTiffIO(unittest.TestCase):
    """Test IO methods `read_tiff_image` & `save_tiff_image` using handmade3D()`."""

    def test_tiff_IO(self):
        """Save the SpatialImage from handmade array, read it and compare to original."""

        arr = handmade_3d()
        vxs = [0.3, 0.4, 0.5]
        img = SpatialImage(arr, voxelsize=vxs)

        tmpf = tempfile.NamedTemporaryFile(suffix=".tif")
        save_tiff_image(tmpf.name, img, force=True)
        img_cp = read_tiff_image(tmpf.name)
        np.testing.assert_array_equal(img, img_cp)
        np.testing.assert_array_almost_equal(img.voxelsize, img_cp.voxelsize, decimal=6)
        tmpf.close()

    def test_tiff_multichannel_IO(self):
        """Save the MultiChannelImage from random arrays, read it and compare to original."""

        channels = ['CH0', 'CH1', 'CH2']
        shp = (3, 4, 5)
        vxs = [0.3, 0.4, 0.5]
        imgs = [random_spatial_image(shp, voxelsize=vxs) for c in channels]
        img = MultiChannelImage(imgs, channel_names=channels)

        tmpf = tempfile.NamedTemporaryFile(suffix=".tif")
        save_tiff_image(tmpf.name, img, force=True)
        img_cp = read_tiff_image(tmpf.name)
        np.testing.assert_array_almost_equal(img.voxelsize, img_cp.voxelsize, decimal=6)
        assert np.all([c in img_cp.channel_names for c in img.channel_names])
        for c in channels:
            np.testing.assert_array_equal(img.get_channel(c), img_cp.get_channel(c))
        tmpf.close()


if __name__ == '__main__':
    unittest.main()
