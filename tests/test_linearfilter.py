#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import gc
import unittest

from tests.make_test_data import get_synthetic_data
from timagetk.algorithms.linearfilter import linearfilter
from timagetk.components.multi_channel import MultiChannelImage
from timagetk.components.spatial_image import SpatialImage


class TestLinearFilter(unittest.TestCase):

    def setUp(self) -> None:
        self.image = get_synthetic_data("small", "wall")

    def tearDown(self) -> None:
        del self.image
        gc.collect()

    def _test_spatial_image(self, out_img):
        self.assertIsInstance(out_img, SpatialImage)
        self.assertIsNotNone(out_img.get_array())

    def test_smoothing(self):
        out_img = linearfilter(self.image, method="smoothing", sigma=1.0)
        self._test_spatial_image(out_img)

    def test_gradient(self):
        out_img = linearfilter(self.image, method="gradient")
        self._test_spatial_image(out_img)

    def test_hessian(self):
        out_img = linearfilter(self.image, method="hessian")
        self._test_spatial_image(out_img)

    def test_laplacian(self):
        out_img = linearfilter(self.image, method="laplacian")
        self._test_spatial_image(out_img)

    def test_gradient_hessian(self):
        out_img = linearfilter(self.image, method="gradient-hessian")
        self._test_spatial_image(out_img)

    def test_gradient_laplacian(self):
        out_img = linearfilter(self.image, method="gradient-laplacian")
        self._test_spatial_image(out_img)

    def test_zero_crossings_hessian(self):
        out_img = linearfilter(self.image, method="zero-crossings-hessian")
        self._test_spatial_image(out_img)

    def test_zero_crossings_laplacian(self):
        out_img = linearfilter(self.image, method="zero-crossings-laplacian")
        self._test_spatial_image(out_img)

    def test_gradient_extrema(self):
        out_img = linearfilter(self.image, method="gradient-extrema")
        self._test_spatial_image(out_img)


class TestLinearFilterMultiChannel(unittest.TestCase):

    def setUp(self) -> None:
        self.image = get_synthetic_data("xsmall", "multichannel")

    def tearDown(self) -> None:
        del self.image
        gc.collect()

    def _test_spatial_image(self, out_img):
        self.assertIsInstance(out_img, SpatialImage)
        self.assertIsNotNone(out_img.get_array())

    def _test_multi_channel_image(self, out_img):
        self.assertIsInstance(out_img, MultiChannelImage)
        self.assertIsNotNone(out_img.get_array())

    def test_smoothing(self):
        out_img = linearfilter(self.image, channel='wall', method="smoothing", sigma=1.0)
        self._test_spatial_image(out_img)
        out_img = linearfilter(self.image, method="smoothing", sigma=1.0)
        self._test_multi_channel_image(out_img)

    def test_gradient(self):
        out_img = linearfilter(self.image, channel='wall', method="gradient")
        self._test_spatial_image(out_img)
        out_img = linearfilter(self.image, method="gradient")
        self._test_multi_channel_image(out_img)

    def test_hessian(self):
        out_img = linearfilter(self.image, channel='wall', method="hessian")
        self._test_spatial_image(out_img)
        out_img = linearfilter(self.image, method="hessian")
        self._test_multi_channel_image(out_img)

    def test_laplacian(self):
        out_img = linearfilter(self.image, channel='wall', method="laplacian")
        self._test_spatial_image(out_img)
        out_img = linearfilter(self.image, method="laplacian")
        self._test_multi_channel_image(out_img)

    def test_gradient_hessian(self):
        out_img = linearfilter(self.image, channel='wall', method="gradient-hessian")
        self._test_spatial_image(out_img)
        out_img = linearfilter(self.image, method="gradient-hessian")
        self._test_multi_channel_image(out_img)

    def test_gradient_laplacian(self):
        out_img = linearfilter(self.image, channel='wall', method="gradient-laplacian")
        self._test_spatial_image(out_img)
        out_img = linearfilter(self.image, method="gradient-laplacian")
        self._test_multi_channel_image(out_img)

    def test_zero_crossings_hessian(self):
        out_img = linearfilter(self.image, channel='wall', method="zero-crossings-hessian")
        self._test_spatial_image(out_img)
        out_img = linearfilter(self.image, method="zero-crossings-hessian")
        self._test_multi_channel_image(out_img)

    def test_zero_crossings_laplacian(self):
        out_img = linearfilter(self.image, channel='wall', method="zero-crossings-laplacian")
        self._test_spatial_image(out_img)
        out_img = linearfilter(self.image, method="zero-crossings-laplacian")
        self._test_multi_channel_image(out_img)

    def test_gradient_extrema(self):
        out_img = linearfilter(self.image, channel='wall', method="gradient-extrema")
        self._test_spatial_image(out_img)
        out_img = linearfilter(self.image, method="gradient-extrema")
        self._test_multi_channel_image(out_img)


if __name__ == '__main__':
    unittest.main()
