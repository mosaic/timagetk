#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import gc
import unittest

from tests.make_test_data import get_synthetic_data
from timagetk import SpatialImage
from timagetk.algorithms.averaging import images_averaging


class TestAveraging(unittest.TestCase):

    def setUp(self) -> None:
        self.img_1 = get_synthetic_data("small", "wall")
        self.img_2 = get_synthetic_data("small", "nuclei")
        self.images = [self.img_1, self.img_2]
        self.out_img = None

    def tearDown(self) -> None:
        del self.img_1
        del self.img_2
        del self.images
        del self.out_img
        gc.collect()

    def _test_spatial_image(self, out_img):
        self.assertIsInstance(out_img, SpatialImage)
        self.assertIsNotNone(out_img.get_array())

    def test_mean(self):
        self.out_img = images_averaging(self.images, method="mean")
        self._test_spatial_image(self.out_img)

    def test_robust_mean(self):
        self.out_img = images_averaging(self.images, method="robust-mean", lts_fraction=0.9)
        self._test_spatial_image(self.out_img)

    def test_median(self):
        self.out_img = images_averaging(self.images, method="median")
        self._test_spatial_image(self.out_img)

    def test_minimum(self):
        self.out_img = images_averaging(self.images, method="minimum")
        self._test_spatial_image(self.out_img)

    def test_maximum(self):
        self.out_img = images_averaging(self.images, method="maximum")
        self._test_spatial_image(self.out_img)

    def test_quantile(self):
        self.out_img = images_averaging(self.images, method="quantile", quantile_value=0.5)
        self._test_spatial_image(self.out_img)

    def test_sum(self):
        self.out_img = images_averaging(self.images, method="sum")
        self._test_spatial_image(self.out_img)

    def test_var(self):
        self.out_img = images_averaging(self.images, method="var")
        self._test_spatial_image(self.out_img)

    def test_stddev(self):
        self.out_img = images_averaging(self.images, method="stddev")
        self._test_spatial_image(self.out_img)


if __name__ == '__main__':
    unittest.main()
