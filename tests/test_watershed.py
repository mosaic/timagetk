#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import gc
import unittest

from tests.make_test_data import get_synthetic_data
from timagetk import LabelledImage
from timagetk import MultiChannelImage
from timagetk import SpatialImage
from timagetk.algorithms.connexe import connected_components
from timagetk.algorithms.linearfilter import gaussian_filter
from timagetk.algorithms.regionalext import regional_extrema
from timagetk.algorithms.watershed import watershed
from timagetk.array_util import intensity_threshold_bimodal


class TestWatershed(unittest.TestCase):

    def setUp(self) -> None:
        self.image = get_synthetic_data("large", "wall")

    def tearDown(self) -> None:
        del self.image
        gc.collect()

    def _prepare_watershed(self):
        # Smooth the image to avoid too many local minima:
        sigma = 1.2
        smooth_img = gaussian_filter(self.image, sigma=sigma)
        # Search for local minima of given height:
        hmin = int(intensity_threshold_bimodal(self.image))
        regext_img = regional_extrema(smooth_img, height=hmin, method='minima')
        # Label detected local minima based on connectivity:
        conn_img = connected_components(regext_img, connectivity=26, low_threshold=1, high_threshold=hmin)
        return smooth_img, conn_img

    def test_watershed(self):
        smooth_img, conn_img = self._prepare_watershed()
        self.assertIsInstance(smooth_img, SpatialImage)
        self.assertIsInstance(conn_img, LabelledImage)
        # Segment with smoothed image and detected seeds:
        for labelchoice in ["first", "min", "most"]:
            labelled_img = watershed(smooth_img, conn_img, labelchoice=labelchoice)
            self.assertIsInstance(labelled_img, LabelledImage)
            self.assertIsNotNone(labelled_img.get_array())


class TestWatershedMultiChannel(unittest.TestCase):

    def setUp(self) -> None:
        self.image = get_synthetic_data("medium", "multichannel")

    def tearDown(self) -> None:
        del self.image
        gc.collect()

    def _prepare_watershed(self):
        # Smooth the image to avoid too many local minima:
        sigma = 1.2
        smooth_img = gaussian_filter(self.image, sigma=sigma)
        # Search for local minima of given height:
        hmin = int(intensity_threshold_bimodal(self.image, channel='wall'))
        regext_img = regional_extrema(smooth_img, channel='wall', height=hmin, method='minima')
        # Label detected local minima based on connectivity:
        conn_img = connected_components(regext_img, connectivity=26, low_threshold=1, high_threshold=hmin)
        return smooth_img, conn_img

    def test_watershed(self):
        smooth_img, conn_img = self._prepare_watershed()
        self.assertIsInstance(smooth_img, MultiChannelImage)
        self.assertIsInstance(conn_img, LabelledImage)
        # Segment with smoothed image and detected seeds:
        for labelchoice in ["first", "min", "most"]:
            labelled_img = watershed(smooth_img, conn_img, channel='wall', labelchoice=labelchoice)
            self.assertIsInstance(labelled_img, LabelledImage)
            self.assertIsNotNone(labelled_img.get_array())


if __name__ == '__main__':
    unittest.main()
