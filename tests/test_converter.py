#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import gc
import unittest

import numpy as np
from vt import vtImage

from timagetk.array_util import random_array
from timagetk.array_util import random_spatial_image
from timagetk.components.spatial_image import SpatialImage
from timagetk.io.dataset import shared_data
from timagetk.third_party.vt_converter import _new_from_vtimage
from timagetk.third_party.vt_converter import vt_to_spatial_image
from timagetk.third_party.vt_image import spatial_to_vt_image
from vt.image import Image


def _assert_equal(vt_img, sp_img):
    arr, voxelsize, origin, _ = _new_from_vtimage(vt_img, copy=False)
    np.testing.assert_array_equal(voxelsize, sp_img.voxelsize)
    np.testing.assert_array_equal(arr, sp_img)


def _assert_almost_equal(vt_img, sp_img, decimals=8):
    arr, voxelsize, origin, _ = _new_from_vtimage(vt_img, copy=False)
    np.testing.assert_array_almost_equal(voxelsize, sp_img.voxelsize, decimals)
    np.testing.assert_array_almost_equal(arr, sp_img, decimals)


class TestRandomImageConvert(unittest.TestCase):

    def test_random_3d_uint8_vt2sp(self):
        # - Image from a created 3D numpy array:
        arr = random_array((3, 4, 5), dtype='uint8')
        vt_img = Image(arr, [0.31, 0.32, 0.33])
        sp_img = vt_to_spatial_image(vt_img)
        self.assertIsInstance(sp_img, SpatialImage)
        _assert_equal(vt_img, sp_img)

    def test_random_3d_uint16_vt2sp(self):
        # - Image from a created 3D numpy array:
        arr = random_array((3, 4, 5), dtype='uint16')
        vt_img = Image(arr, [0.31, 0.32, 0.33])
        sp_img = vt_to_spatial_image(vt_img)
        self.assertIsInstance(sp_img, SpatialImage)
        _assert_equal(vt_img, sp_img)

    def test_random_2d_uint8_vt2sp(self):
        # - Image from a created 3D numpy array:
        arr = random_array((4, 5), dtype='uint8')
        vt_img = Image(arr, [0.32, 0.33])
        sp_img = vt_to_spatial_image(vt_img)
        self.assertIsInstance(sp_img, SpatialImage)
        _assert_equal(vt_img, sp_img)

    def test_random_2d_uint16_vt2sp(self):
        # - Image from a created 3D numpy array:
        arr = random_array((4, 5), dtype='uint16')
        vt_img = Image(arr, [0.32, 0.33])
        sp_img = vt_to_spatial_image(vt_img)
        self.assertIsInstance(sp_img, SpatialImage)
        _assert_equal(vt_img, sp_img)

    def test_random_3d_uint8_sp2vt(self):
        # - Image from a created 3D numpy array:
        sp_img = random_spatial_image((3, 4, 5), dtype='uint8')
        vt_img = spatial_to_vt_image(sp_img)
        self.assertIsInstance(vt_img, Image)
        _assert_equal(vt_img, sp_img)

    def test_random_3d_uint16_sp2vt(self):
        # - Image from a created 3D numpy array:
        sp_img = random_spatial_image((3, 4, 5), dtype='uint16')
        vt_img = spatial_to_vt_image(sp_img)
        self.assertIsInstance(vt_img, Image)
        _assert_equal(vt_img, sp_img)

    def test_random_2d_uint8_sp2vt(self):
        # - Image from a created 3D numpy array:
        sp_img = random_spatial_image((4, 5), dtype='uint8')
        vt_img = spatial_to_vt_image(sp_img)
        self.assertIsInstance(vt_img, Image)
        _assert_equal(vt_img, sp_img)

    def test_random_2d_uint16_sp2vt(self):
        # - Image from a created 3D numpy array:
        sp_img = random_spatial_image((4, 5), dtype='uint16')
        vt_img = spatial_to_vt_image(sp_img)
        self.assertIsInstance(vt_img, Image)
        _assert_equal(vt_img, sp_img)

class TestRandomVtImageConvert(unittest.TestCase):

    def test_random_3d_uint8_vt2sp(self):
        # - Image from a created 3D numpy array:
        arr = random_array((3, 4, 5), dtype='uint8')
        vt_img = vtImage(arr, [0.31, 0.32, 0.33])
        sp_img = vt_to_spatial_image(vt_img)
        self.assertIsInstance(sp_img, SpatialImage)
        _assert_equal(vt_img, sp_img)

    def test_random_3d_uint16_vt2sp(self):
        # - Image from a created 3D numpy array:
        arr = random_array((3, 4, 5), dtype='uint16')
        vt_img = vtImage(arr, [0.31, 0.32, 0.33])
        sp_img = vt_to_spatial_image(vt_img)
        self.assertIsInstance(sp_img, SpatialImage)
        _assert_equal(vt_img, sp_img)

    def test_random_2d_uint8_vt2sp(self):
        # - Image from a created 3D numpy array:
        arr = random_array((4, 5), dtype='uint8')
        vt_img = vtImage(arr, [0.32, 0.33]+[1.])  # need a len-3 voxelsize to init
        sp_img = vt_to_spatial_image(vt_img)
        self.assertIsInstance(sp_img, SpatialImage)
        _assert_equal(vt_img, sp_img)

    def test_random_2d_uint16_vt2sp(self):
        # - Image from a created 3D numpy array:
        arr = random_array((4, 5), dtype='uint16')
        vt_img = vtImage(arr, [0.32, 0.33]+[1.])  # need a len-3 voxelsize to init
        sp_img = vt_to_spatial_image(vt_img)
        self.assertIsInstance(sp_img, SpatialImage)
        _assert_equal(vt_img, sp_img)

    def test_random_3d_uint8_sp2vt(self):
        # - Image from a created 3D numpy array:
        sp_img = random_spatial_image((3, 4, 5), dtype='uint8')
        vt_img = spatial_to_vt_image(sp_img)
        self.assertIsInstance(vt_img, vtImage)
        _assert_equal(vt_img, sp_img)

    def test_random_3d_uint16_sp2vt(self):
        # - Image from a created 3D numpy array:
        sp_img = random_spatial_image((3, 4, 5), dtype='uint16')
        vt_img = spatial_to_vt_image(sp_img)
        self.assertIsInstance(vt_img, vtImage)
        _assert_equal(vt_img, sp_img)

    def test_random_2d_uint8_sp2vt(self):
        # - Image from a created 3D numpy array:
        sp_img = random_spatial_image((4, 5), dtype='uint8')
        vt_img = spatial_to_vt_image(sp_img)
        self.assertIsInstance(vt_img, vtImage)
        _assert_equal(vt_img, sp_img)

    def test_random_2d_uint16_sp2vt(self):
        # - Image from a created 3D numpy array:
        sp_img = random_spatial_image((4, 5), dtype='uint16')
        vt_img = spatial_to_vt_image(sp_img)
        self.assertIsInstance(vt_img, vtImage)
        _assert_equal(vt_img, sp_img)


class TestImageConverterReal(unittest.TestCase):

    def _test_from_vtImage_reader(self, fname):
        vt_im = Image(fname)
        sp_im = vt_to_spatial_image(vt_im)
        self.assertIsInstance(sp_im, SpatialImage)
        # _assert_equal(vt_im, sp_im)
        _assert_almost_equal(vt_im, sp_im)

    def test_lsm_from_vt(self):
        self._test_from_vtImage_reader(shared_data("flower_multiangle", 0))

    def test_inrgz_from_vt(self):
        self._test_from_vtImage_reader(shared_data('flower_confocal', 0))

    # def test_inr_from_vt(self):
    #     self._test_from_vtImage_reader(shared_data("time_0_cut.inr"))

    # def test_mha_from_vt(self):
    #     self._test_from_vtImage_reader(shared_data("time_0_cut_rotated1.mha"))


class TestVtImageConverterReal(unittest.TestCase):

    def _test_from_vtImage_reader(self, fname):
        vt_im = vtImage(fname)
        sp_im = vt_to_spatial_image(vt_im)
        self.assertIsInstance(sp_im, SpatialImage)
        # _assert_equal(vt_im, sp_im)
        _assert_almost_equal(vt_im, sp_im)

    def test_lsm_from_vt(self):
        self._test_from_vtImage_reader(shared_data("flower_multiangle", 0))

    def test_inrgz_from_vt(self):
        self._test_from_vtImage_reader(shared_data('flower_confocal', 0))

    # def test_inr_from_vt(self):
    #     self._test_from_vtImage_reader(shared_data("time_0_cut.inr"))

    # def test_mha_from_vt(self):
    #     self._test_from_vtImage_reader(shared_data("time_0_cut_rotated1.mha"))


if __name__ == '__main__':
    unittest.main()
    gc.collect()