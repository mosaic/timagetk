#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import unittest

import numpy as np

from timagetk.array_util import random_array
from timagetk.array_util import random_spatial_image
from timagetk.components.spatial_image import SpatialImage
from timagetk.io.image import default_image_attributes
from timagetk.third_party.vt_converter import vt_to_spatial_image
from timagetk.util import compute_extent
from timagetk.util import compute_voxelsize
from timagetk.util import now


class TestSpatialImage(unittest.TestCase):

    def test_get_methods_2D(self):
        shape = (5, 5)
        dtype = 'uint8'
        orig = [0, 0]
        vox = [1.0, 1.0]
        axes_order = 'YX'
        unit = 1e-06
        tmp_arr = random_array(shape, dtype=dtype)
        md = {'acquisition_date': now()}
        img = SpatialImage(tmp_arr, origin=orig, voxelsize=vox, axes_order=axes_order, unit=unit, metadata=md)
        # metadata = {'dim': 2, 'get_extent': [5.0, 5.0], 'shape': (5, 5),
        #             'dtype': 'uint8',
        #             'voxelsize': [1.0, 1.0], 'origin': [0, 0], 'max': 1,
        #             'mean': 1.0,
        #             'min': 1}
        # 'min', 'max' & 'mean' are not computed by default anymore:
        metadata = {
            'shape': shape,
            'ndim': len(shape),
            'dtype': dtype,
            'extent': compute_extent(vox, shape),
            'voxelsize': vox,
            'origin': orig,
            'axes_order': axes_order,
            'unit': 1e-06,
        }
        metadata.update(md)

        self.assertEqual(img.dtype, tmp_arr.dtype)
        self.assertEqual(img.voxelsize, vox)
        self.assertEqual(img.shape, tmp_arr.shape)
        self.assertEqual(img.origin, orig)
        self.assertEqual(img.extent, compute_extent(vox, shape))
        self.assertEqual(img.ndim, 2)
        for k, v in img.metadata.items():
            if isinstance(v, np.ndarray) or isinstance(v, list):
                np.testing.assert_almost_equal(v, metadata[k], decimal=6)
            else:
                self.assertEqual(v, metadata[k])

    def test_set_methods_2D(self):
        tmp_arr = np.ones((5, 5), dtype=np.uint8)
        vox = [1.0, 1.0]
        orig = [0, 0]
        img = SpatialImage(tmp_arr, **default_image_attributes(tmp_arr, origin=orig, voxelsize=vox))

        new_vox = [0.5, 0.5]
        new_ext = compute_extent(new_vox, tmp_arr.shape)
        img.voxelsize = new_vox
        self.assertEqual(img.voxelsize, new_vox)
        self.assertEqual(img.metadata['voxelsize'], new_vox)
        self.assertEqual(img.extent, new_ext)
        self.assertEqual(img.metadata['extent'], new_ext)

        new_type = 'uint16'
        arr = img.astype(new_type)
        img = SpatialImage(arr, **default_image_attributes(arr, origin=orig, voxelsize=vox))
        self.assertEqual(img.dtype, new_type)
        self.assertEqual(img.metadata['dtype'], new_type)

        new_orig = [1, 1]
        img.origin = new_orig
        self.assertEqual(img.origin, new_orig)
        self.assertEqual(img.metadata['origin'], new_orig)

        new_ext = [10.0, 5.0]
        new_vox = compute_voxelsize(img.shape, new_ext)
        img.extent = new_ext
        self.assertEqual(img.voxelsize, new_vox)
        self.assertEqual(img.metadata['extent'], new_ext)
        self.assertEqual(img.metadata['voxelsize'], new_vox)

        new_met = img.metadata
        new_met.update({'name': 'img_test'})
        img.metadata = new_met
        self.assertDictEqual(img.metadata, new_met)

        pix_coords, pix_val = [2, 2], 10
        img.set_pixel(pix_coords, pix_val)
        self.assertEqual(img.get_pixel(pix_coords), pix_val)

        # Test numpy view casting compatibility (for example slicing)
        # TODO: MAKE THIS WORKS !!!!

    def test_get_methods_3D(self):
        shape = (3, 4, 5)
        dtype = 'uint8'
        orig = [0, 0, 0]
        vox = [2., 1.0, 1.0]
        axes_order = 'ZYX'
        unit = 1e-06
        tmp_arr = random_array(shape, dtype=dtype)
        md = {'acquisition_date': now()}
        img = SpatialImage(tmp_arr, origin=orig, voxelsize=vox, axes_order=axes_order, unit=unit, metadata=md)
        # metadata = {'dim': 2, 'get_extent': [5.0, 5.0], 'shape': (5, 5),
        #             'dtype': 'uint8',
        #             'voxelsize': [1.0, 1.0], 'origin': [0, 0], 'max': 1,
        #             'mean': 1.0,
        #             'min': 1}
        # 'min', 'max' & 'mean' are not computed by default anymore:
        metadata = {
            'shape': shape,
            'ndim': len(shape),
            'dtype': dtype,
            'extent': compute_extent(vox, shape),
            'voxelsize': vox,
            'origin': orig,
            'axes_order': axes_order,
            'unit': 1e-06,
        }
        metadata.update(md)

        self.assertEqual(img.dtype, tmp_arr.dtype)
        self.assertEqual(img.voxelsize, vox)
        self.assertEqual(img.shape, tmp_arr.shape)
        self.assertEqual(img.origin, orig)
        self.assertEqual(img.extent, compute_extent(vox, shape))
        self.assertEqual(img.ndim, 3)
        for k, v in img.metadata.items():
            if isinstance(v, np.ndarray) or isinstance(v, list):
                np.testing.assert_almost_equal(v, metadata[k], decimal=6)
            else:
                self.assertEqual(v, metadata[k])

    def test_set_methods_3D(self):
        tmp_arr = np.ones((3, 4, 5), dtype=np.uint8)
        vox = [2., 1.0, 1.0]
        orig = [0, 0, 0]
        img = SpatialImage(tmp_arr, **default_image_attributes(tmp_arr, origin=orig, voxelsize=vox))

        new_vox = [1., 0.5, 0.5]
        new_ext = compute_extent(new_vox, tmp_arr.shape)
        img.voxelsize = new_vox
        self.assertEqual(img.voxelsize, new_vox)
        self.assertEqual(img.metadata['voxelsize'], new_vox)
        self.assertEqual(img.extent, new_ext)
        self.assertEqual(img.metadata['extent'], new_ext)

        new_type = 'uint16'
        arr = img.astype(new_type)
        img = SpatialImage(arr, **default_image_attributes(arr, origin=orig, voxelsize=vox))
        self.assertEqual(img.dtype, new_type)
        self.assertEqual(img.metadata['dtype'], new_type)

        new_orig = [1, 1, 1]
        img.origin = new_orig
        self.assertEqual(img.origin, new_orig)
        self.assertEqual(img.metadata['origin'], new_orig)

        new_ext = [10.0, 5.0, 5.0]
        new_vox = compute_voxelsize(img.shape, new_ext)
        img.extent = new_ext
        self.assertEqual(img.voxelsize, new_vox)
        self.assertEqual(img.metadata['extent'], new_ext)
        self.assertEqual(img.metadata['voxelsize'], new_vox)

        new_met = img.metadata
        new_met.update({'name': 'img_test'})
        img.metadata = new_met
        self.assertDictEqual(img.metadata, new_met)

        pix_coords, pix_val = [0, 2, 2], 10
        img.set_pixel(pix_coords, pix_val)
        self.assertEqual(img.get_pixel(pix_coords), pix_val)

    def test_get_extent(self):
        # Initialize a random (uint8) 3D SpatialImage:
        shape = (3, 5, 6)  # ZYX
        voxelsize = [1., 0.5, 0.5]  # ZYX
        img = random_spatial_image(shape, voxelsize=voxelsize, dtype='uint8')
        self.assertTrue(img.get_extent('x') == shape[2] * voxelsize[2] - voxelsize[2])
        self.assertTrue(img.get_extent('y') == shape[1] * voxelsize[1] - voxelsize[1])
        self.assertTrue(img.get_extent('z') == shape[0] * voxelsize[0] - voxelsize[0])

    def test_array_conversion(self):
        # Initialize a random (uint8) 3D SpatialImage:
        img = random_spatial_image((3, 4, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        self.assertTrue(isinstance(img, SpatialImage))
        # Get the array linked to the SpatialImage:
        array = img.get_array()
        self.assertFalse(isinstance(array, SpatialImage))
        self.assertTrue(isinstance(array, np.ndarray))
        np.testing.assert_array_equal(array, img)

    def test_to_vtimage(self):
        img = random_spatial_image((3, 4, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        vt_img = img.to_vt_image()
        self.assertEqual(img.voxelsize, vt_img.spacing())
        self.assertTupleEqual(img.shape, tuple(vt_img.shape()))
        np.testing.assert_array_equal(img.get_array(), vt_img.copy_to_array())

    def test_to_vtimage_to_spimage(self):
        img = random_spatial_image((3, 4, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        vt_img = img.to_vt_image()
        sp_img = vt_to_spatial_image(vt_img)
        self.assertEqual(img.dtype, sp_img.dtype)
        self.assertListEqual(img.voxelsize, sp_img.voxelsize)
        self.assertTupleEqual(img.shape, sp_img.shape)
        self.assertListEqual(img.origin, sp_img.origin)
        self.assertListEqual(img.extent, sp_img.extent)

    def test_isometric(self):
        # TODO
        pass

    def test_is2D(self):
        # Initialize a random (uint8) 2D SpatialImage:
        img = random_spatial_image((5, 5), voxelsize=[0.5, 0.5], dtype='uint8')
        self.assertTrue(img.is2D())
        self.assertFalse(img.is3D())

    def test_is3D(self):
        # Initialize a random (uint8) 3D SpatialImage:
        img = random_spatial_image((3, 4, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        self.assertTrue(img.is3D())
        self.assertFalse(img.is2D())

    def test_to2D(self):
        # Initialize a random (uint8) 2D SpatialImage:
        img = random_spatial_image((1, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        img2d = img.to_2d()
        self.assertTrue(img2d.is2D())
        self.assertFalse(img2d.is3D())
        self.assertTrue(img2d.get_shape() == [5, 5])
        self.assertTrue(img2d.origin == [0, 0])
        self.assertTrue(img2d.voxelsize == [0.5, 0.5])
        self.assertTrue(img2d.dtype == np.uint8)

    def test_to3D(self):
        # Initialize a random (uint8) 2D SpatialImage:
        img = random_spatial_image((5, 5), voxelsize=[0.5, 0.5], dtype='uint8')
        img3d = img.to_3d(flat_axis="z")
        self.assertTrue(img3d.is3D())
        self.assertFalse(img3d.is2D())
        self.assertTrue(img3d.get_shape() == [1, 5, 5])
        self.assertTrue(img3d.origin == [0, 0, 0])
        self.assertTrue(img3d.voxelsize == [1., 0.5, 0.5])
        self.assertTrue(img3d.dtype == np.uint8)
        img3d = img.to_3d(flat_axis="y")
        self.assertTrue(img3d.is3D())
        self.assertFalse(img3d.is2D())
        self.assertTrue(img3d.get_shape() == [5, 1, 5])
        self.assertTrue(img3d.origin == [0, 0, 0])
        self.assertTrue(img3d.voxelsize == [0.5, 1., 0.5])
        self.assertTrue(img3d.dtype == np.uint8)
        img3d = img.to_3d(flat_axis="x")
        self.assertTrue(img3d.is3D())
        self.assertFalse(img3d.is2D())
        self.assertTrue(img3d.get_shape() == [5, 5, 1])
        self.assertTrue(img3d.origin == [0, 0, 0])
        self.assertTrue(img3d.voxelsize == [0.5, 0.5, 1])
        self.assertTrue(img3d.dtype == np.uint8)

    def test_slice(self):
        from timagetk.array_util import random_spatial_image
        # Initialize a random (uint8) 3D SpatialImage:
        img = random_spatial_image((3, 4, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        # Take an existing slice from a 3D image:
        np.testing.assert_array_equal(img.get_slice(1, 'z'), img[1, :, :])
        np.testing.assert_array_equal(img.get_slice(1, 'y'), img[:, 1, :])
        np.testing.assert_array_equal(img.get_slice(1, 'x'), img[:, :, 1])
        # - Set the first z-slice to 1:
        img.set_slice(0, 1, axis='z')
        np.testing.assert_array_equal(img.get_slice(0, 'z'), np.ones_like(img.get_slice(0, 'z')))
        # - Set the first x-slice to a random array of same shape:
        from timagetk.array_util import random_array
        x_arr = random_array((3, 4), dtype='uint8')
        img.set_slice(0, x_arr, axis='x')
        np.testing.assert_array_equal(img.get_slice(0, 'x'), x_arr)

    def test_slice_extent(self):
        from timagetk.array_util import random_spatial_image
        # Initialize a random (uint8) 3D SpatialImage:
        shape = (3, 5, 6)
        voxelsize = [1., 0.5, 0.5]
        img = random_spatial_image(shape, voxelsize=voxelsize, dtype='uint8')
        extent = img.get_extent()
        z_sl = img.get_slice(1, 'z')
        y_sl = img.get_slice(1, 'y')
        x_sl = img.get_slice(1, 'x')
        self.assertTrue(z_sl.get_extent() == [extent[1], extent[2]])
        self.assertTrue(y_sl.get_extent() == [extent[0], extent[2]])
        self.assertTrue(x_sl.get_extent() == [extent[0], extent[1]])

    def test_pixel(self):
        img = random_spatial_image((3, 4, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        voxel_value = 37
        voxel_pos = [1, 1, 1]
        img.set_pixel(voxel_pos, voxel_value)
        self.assertEqual(voxel_value, img.get_pixel(voxel_pos))

    def test_region(self):
        # TODO
        pass

    def test_invert_axis(self):
        img = random_spatial_image((3, 4, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        np.testing.assert_array_equal(img[::-1, :, :], img.invert_axis('z'))
        np.testing.assert_array_equal(img[:, ::-1, :], img.invert_axis('y'))
        np.testing.assert_array_equal(img[:, :, ::-1], img.invert_axis('x'))


if __name__ == '__main__':
    unittest.main()
