#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import gc
import unittest

from tests.make_test_data import get_synthetic_data
from timagetk.algorithms.blockmatching import blockmatching


class TestBlockMatching(unittest.TestCase):

    def setUp(self) -> None:
        self.floating_image = get_synthetic_data("small", "wall")
        self.reference_image = get_synthetic_data("medium", "wall")
        self.trsf = None

    def tearDown(self) -> None:
        del self.floating_image
        del self.reference_image
        del self.trsf
        gc.collect()

    def test_rigid(self):
        self.trsf = blockmatching(self.floating_image, self.reference_image, method='rigid')
        self.assertIsNotNone(self.trsf)
        self.assertTrue(self.trsf.is_linear())
        self.assertIsNotNone(self.trsf.get_array())

    def test_affine(self):
        self.trsf = blockmatching(self.floating_image, self.reference_image, method='affine')
        self.assertIsNotNone(self.trsf)
        self.assertTrue(self.trsf.is_linear())
        self.assertIsNotNone(self.trsf.get_array())

    def test_vectorfield(self):
        self.trsf = blockmatching(self.floating_image, self.reference_image, method='vectorfield')
        self.assertIsNotNone(self.trsf)
        self.assertTrue(self.trsf.is_vectorfield())
        self.assertIsNotNone(self.trsf.get_array())


class TestBlockMatchingMultiChannel(unittest.TestCase):

    def setUp(self) -> None:
        self.floating_image = get_synthetic_data("small", "multichannel")
        self.reference_image = get_synthetic_data("medium", "multichannel")
        self.trsf = None

    def tearDown(self) -> None:
        del self.floating_image
        del self.reference_image
        del self.trsf
        gc.collect()

    def test_rigid(self):
        self.trsf = blockmatching(self.floating_image, self.reference_image,
                             channel='wall', method='rigid')
        self.assertIsNotNone(self.trsf)
        self.assertTrue(self.trsf.is_linear())
        self.assertIsNotNone(self.trsf.get_array())

    def test_affine(self):
        self.trsf = blockmatching(self.floating_image, self.reference_image,
                             channel='wall', method='affine')
        self.assertIsNotNone(self.trsf)
        self.assertTrue(self.trsf.is_linear())
        self.assertIsNotNone(self.trsf.get_array())

    def test_vectorfield(self):
        self.trsf = blockmatching(self.floating_image, self.reference_image,
                             channel='wall', method='vectorfield')
        self.assertIsNotNone(self.trsf)
        self.assertTrue(self.trsf.is_vectorfield())
        self.assertIsNotNone(self.trsf.get_array())


if __name__ == '__main__':
    unittest.main()
