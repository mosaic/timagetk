#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import gc
import unittest

from tests.make_test_data import get_synthetic_data
from timagetk import MultiChannelImage
from timagetk import SpatialImage
from timagetk.algorithms.exposure import equalize_adapthist
from timagetk.algorithms.exposure import gamma_adjust
from timagetk.algorithms.exposure import global_contrast_stretch
from timagetk.algorithms.exposure import log_adjust
from timagetk.algorithms.exposure import rescale_intensity
from timagetk.algorithms.exposure import sigmoid_adjust
from timagetk.algorithms.exposure import slice_contrast_stretch


class TestExposure(unittest.TestCase):

    def setUp(self) -> None:
        self.image = get_synthetic_data("small", "wall")

    def _test_spatial_image(self, out_img):
        self.assertIsInstance(out_img, SpatialImage)
        self.assertIsNotNone(out_img.get_array())

    def test_global_contrast_stretch(self):
        out_img = global_contrast_stretch(self.image)
        self._test_spatial_image(out_img)

    def test_slice_contrast_stretch(self):
        out_img = slice_contrast_stretch(self.image)
        self._test_spatial_image(out_img)

    def test_equalize_adapthist(self):
        out_img = equalize_adapthist(self.image)
        self._test_spatial_image(out_img)

    def test_gamma_adjust(self):
        out_img = gamma_adjust(self.image)
        self._test_spatial_image(out_img)

    def test_log_adjust(self):
        out_img = log_adjust(self.image)
        self._test_spatial_image(out_img)

    def test_sigmoid_adjust(self):
        out_img = sigmoid_adjust(self.image)
        self._test_spatial_image(out_img)

    def test_rescale_intensity(self):
        out_img = rescale_intensity(self.image)
        self._test_spatial_image(out_img)


class TestExposureMultiChannel(unittest.TestCase):

    def setUp(self) -> None:
        self.image = get_synthetic_data("xsmall", "multichannel")

    def _test_spatial_image(self, out_img):
        self.assertIsInstance(out_img, SpatialImage)
        self.assertIsNotNone(out_img.get_array())

    def _test_multi_channel_image(self, out_img):
        self.assertIsInstance(out_img, MultiChannelImage)
        self.assertIsNotNone(out_img.get_array())

    def test_global_contrast_stretch(self):
        out_img = global_contrast_stretch(self.image, channel='wall')
        self._test_spatial_image(out_img)
        out_img = global_contrast_stretch(self.image)
        self._test_multi_channel_image(out_img)

    def test_slice_contrast_stretch(self):
        out_img = slice_contrast_stretch(self.image, channel='wall')
        self._test_spatial_image(out_img)
        out_img = slice_contrast_stretch(self.image)
        self._test_multi_channel_image(out_img)

    def test_equalize_adapthist(self):
        out_img = equalize_adapthist(self.image, channel='wall')
        self._test_spatial_image(out_img)
        out_img = equalize_adapthist(self.image)
        self._test_multi_channel_image(out_img)

    def test_gamma_adjust(self):
        out_img = gamma_adjust(self.image, channel='wall')
        self._test_spatial_image(out_img)
        out_img = gamma_adjust(self.image)
        self._test_multi_channel_image(out_img)

    def test_log_adjust(self):
        out_img = log_adjust(self.image, channel='wall')
        self._test_spatial_image(out_img)
        out_img = log_adjust(self.image)
        self._test_multi_channel_image(out_img)

    def test_sigmoid_adjust(self):
        out_img = sigmoid_adjust(self.image, channel='wall')
        self._test_spatial_image(out_img)
        out_img = sigmoid_adjust(self.image)
        self._test_multi_channel_image(out_img)

    def test_rescale_intensity(self):
        out_img = rescale_intensity(self.image, channel='wall')
        self._test_spatial_image(out_img)
        out_img = rescale_intensity(self.image)
        self._test_multi_channel_image(out_img)


if __name__ == '__main__':
    unittest.main()
    gc.collect()