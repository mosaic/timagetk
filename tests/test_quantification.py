#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2022, all rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import gc
import unittest

import numpy as np

from timagetk import SpatialImage
from timagetk import TissueImage3D
from timagetk.algorithms.signal_quantification import quantify_cell_signal_intensity
from timagetk.io.image import default_image_attributes
from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image


class TestQuantification(unittest.TestCase):

    def setUp(self):
        np.random.seed(42)

        self.n_layers = 1
        self.n_points = 10

        seg_img = example_layered_sphere_labelled_image(n_points=self.n_points,
                                                        n_layers=self.n_layers,
                                                        return_points=False)
        seg_img = TissueImage3D(seg_img, not_a_label=0, background=1, **default_image_attributes(seg_img))

        arr = np.ones_like(seg_img.get_array())
        signal_img = SpatialImage(arr, **default_image_attributes(arr, voxelsize=seg_img.voxelsize))

        self.seg_img = seg_img
        self.signal_img = signal_img

    def tearDown(self):
        del self.n_layers
        del self.n_points
        del self.seg_img
        del self.signal_img

    def test_cell_quantification(self):
        cell_signals = quantify_cell_signal_intensity(self.signal_img, self.seg_img,
                                                      erosion_radius=1., disable_tqdm=True)

        assert np.all([c in cell_signals.keys() for c in self.seg_img.cell_ids()])
        for c in self.seg_img.cell_ids():
            assert cell_signals[c] == 1


if __name__ == '__main__':
    unittest.main()
    gc.collect()