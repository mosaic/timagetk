#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Test the interfacing with ``vt-python``:
  - ``vt.vtImage`` & ``SpatialImage``: image array & attributes ``voxelsize``, ``shape``
"""
import gc
import unittest

import numpy as np
from vt.image import Image

from timagetk.array_util import random_array
from timagetk.components.spatial_image import SpatialImage
from timagetk.io.image import default_image_attributes


class TestVtInterface(unittest.TestCase):

    def _setup(self, shape, dtype):
        ndim = len(shape)
        arr = random_array(shape, dtype)
        vxs = [x / 10. for x in range(3, 3 + ndim)]
        vtim = Image(arr, vxs)
        spim = SpatialImage(arr, **default_image_attributes(arr, voxelsize=vxs))
        return vtim, spim

    def _test_vxs(self, vtim, spim):
        spacing = vtim.spacing()
        err_msg = "Voxelsizes are not equal:"
        err_msg += f"  - vtImage: {spacing}"
        err_msg += f"  - SpatialImage: {spim.voxelsize}"
        np.testing.assert_array_almost_equal(vtim.spacing(), spim.voxelsize,
                                             decimal=6, err_msg=err_msg)

    def _test_array_shape(self, vtim, spim):
        sh = vtim.shape()
        err_msg = "Array shapes are not equal:"
        err_msg += f"  - vtImage: {sh}"
        err_msg += f"  - SpatialImage: {spim.shape}"
        np.testing.assert_array_equal(sh, spim.shape,
                                      err_msg=err_msg)

    def _test_array(self, vtim, spim):
        np.testing.assert_array_equal(vtim.copy_to_array(), spim.get_array())

    def test_2D_uint8(self):
        vtim, spim = self._setup([9, 10], "uint8")
        self._test_vxs(vtim, spim)
        self._test_array_shape(vtim, spim)
        self._test_array(vtim, spim)
        del vtim, spim
        gc.collect()

    def test_2D_uint16(self):
        vtim, spim = self._setup([9, 10], "uint16")
        self._test_vxs(vtim, spim)
        self._test_array_shape(vtim, spim)
        self._test_array(vtim, spim)
        del vtim, spim
        gc.collect()

    def test_2D_int16(self):
        vtim, spim = self._setup([9, 10], "int16")
        self._test_vxs(vtim, spim)
        self._test_array_shape(vtim, spim)
        self._test_array(vtim, spim)
        del vtim, spim
        gc.collect()

    def test_3D_uint8(self):
        vtim, spim = self._setup([9, 10, 11], "uint8")
        self._test_vxs(vtim, spim)
        self._test_array_shape(vtim, spim)
        self._test_array(vtim, spim)
        del vtim, spim
        gc.collect()

    def test_3D_uint16(self):
        vtim, spim = self._setup([9, 10, 11], "uint16")
        self._test_vxs(vtim, spim)
        self._test_array_shape(vtim, spim)
        self._test_array(vtim, spim)
        del vtim, spim
        gc.collect()

    def test_3D_int16(self):
        vtim, spim = self._setup([9, 10, 11], "int16")
        self._test_vxs(vtim, spim)
        self._test_array_shape(vtim, spim)
        self._test_array(vtim, spim)
        del vtim, spim
        gc.collect()


if __name__ == '__main__':
    unittest.main()
