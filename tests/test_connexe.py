#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import gc
import unittest

from timagetk.algorithms.connexe import connected_components
from timagetk.algorithms.regionalext import regional_extrema
from timagetk.array_util import intensity_threshold_percentile
from timagetk.components.labelled_image import LabelledImage

from tests.make_test_data import get_synthetic_data


class TestConnexe(unittest.TestCase):

    def setUp(self) -> None:
        self.image = get_synthetic_data("medium", "wall")

    def tearDown(self) -> None:
        del self.image

    def test_connected_components_labelling(self):
        img = connected_components(self.image, low_threshold=60)
        self.assertIsInstance(img, LabelledImage)
        self.assertIsNotNone(img.get_array())

    def test_connected_localmin_labelling(self):
        hmin = int(intensity_threshold_percentile(self.image))
        extmin_img = regional_extrema(self.image, hmin, "minima")
        img = connected_components(extmin_img, low_threshold=1, high_threshold=hmin)
        self.assertIsInstance(img, LabelledImage)
        self.assertIsNotNone(img.get_array())

    def test_background_detection(self):
        hmin = int(intensity_threshold_percentile(self.image))
        extmin_img = regional_extrema(self.image, hmin, "minima")
        img = connected_components(extmin_img, high_threshold=hmin, max=True)
        self.assertIsInstance(img, LabelledImage)
        self.assertIsNotNone(img.get_array())


class TestConnexeMultiChannel(unittest.TestCase):

    def setUp(self) -> None:
        self.image = get_synthetic_data("medium", "multichannel")

    def tearDown(self) -> None:
        del self.image

    def test_connected_components_labelling(self):
        img = connected_components(self.image, channel='wall', low_threshold=60)
        self.assertIsInstance(img, LabelledImage)
        self.assertIsNotNone(img.get_array())


if __name__ == '__main__':
    unittest.main()
    gc.collect()
