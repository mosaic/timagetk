#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import gc
import unittest

from timagetk.algorithms.slices import combine_slices
from timagetk.algorithms.slices import dilation
from timagetk.algorithms.slices import dilation_by
from timagetk.algorithms.slices import overlapping_slices
from timagetk.algorithms.slices import real_indices


class TestSlices(unittest.TestCase):

    def tearDown(self) -> None:
        del self.slices
        gc.collect()

    def test_dilation(self):
        self.slices = [slice(1, 5)]
        dil_sl = dilation(self.slices)[0]
        self.assertEqual(0, dil_sl.start)
        self.assertEqual(6, dil_sl.stop)

    def test_dilation_by(self):
        self.slices = [slice(1, 5)]
        dil_sl = dilation_by(self.slices, 5)[0]
        self.assertEqual(0, dil_sl.start)
        self.assertEqual(10, dil_sl.stop)

    def test_combine_slices(self):
        self.slices = [slice(2, 5), slice(1, 3)], [slice(2, 4), slice(0, 2)], [slice(3, 8), slice(1, 6)]
        c_sl = combine_slices(self.slices)
        self.assertEqual(2, c_sl[0].start)
        self.assertEqual(8, c_sl[0].stop)
        self.assertEqual(0, c_sl[1].start)
        self.assertEqual(6, c_sl[1].stop)
        del c_sl

    def test_overlapping_slices(self):
        self.slices = [slice(2, 5), slice(1, 3)], [slice(2, 4), slice(0, 2)]
        self.assertTrue(overlapping_slices(*self.slices))
        self.slices = [slice(2, 5), slice(2, 3)], [slice(2, 4), slice(0, 2)]
        self.assertFalse(overlapping_slices(*self.slices))

    def test_real_indices(self):
        self.slices = [slice(2, 5), slice(1, 3)]
        idx = real_indices(self.slices, [2, 3])
        self.assertEqual(idx[0].start, self.slices[0].start * 2)
        self.assertEqual(idx[0].stop, self.slices[0].stop * 2)
        self.assertEqual(idx[1].start, self.slices[1].start * 3)
        self.assertEqual(idx[1].stop, self.slices[1].stop * 3)
        del idx


if __name__ == '__main__':
    unittest.main()
