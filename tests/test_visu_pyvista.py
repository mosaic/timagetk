#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2022, all rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import gc
import unittest

import pyvista as pv

from timagetk import TissueImage3D
from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image
from timagetk.visu.pyvista import tissue_image_unstructured_grid


class TestVisuPyvistaUnstructuredGrid(unittest.TestCase):

    def test_segmentation(self):

        seg_img = example_layered_sphere_labelled_image(n_layers=2)
        seg_img = TissueImage3D(seg_img, not_a_label=0, background=1)
        seg_img.cells.volume()
        seg_img.cells.barycenter()
        seg_img.cells.set_feature("vector_value", {2: [1]*3})
        seg_img.cells.set_feature("tensor_value", {2: [[1]*3]*3})

        grid = tissue_image_unstructured_grid(seg_img, resampling_voxelsize=1)

        self.assertTrue(grid is not None)
        self.assertEqual(grid["vector_value"].shape[0], grid["label"].shape[0])
        self.assertEqual(grid["vector_value"].shape[1], 3)
        self.assertEqual(grid["tensor_value"].shape[0], grid["label"].shape[0])
        # FIXME: Check if it is possible to store an unflattened list
        self.assertEqual(grid["tensor_value"].shape[1], 9)
        # self.assertEqual(grid["tensor_value"].shape[1], 3)
        # self.assertEqual(grid["tensor_value"].shape[2], 3)

if __name__ == '__main__':
    unittest.main()
