#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import gc
import unittest

from timagetk import LabelledImage
from timagetk import MultiChannelImage
from timagetk import SpatialImage
from timagetk.algorithms.morphology import label_filtering
from timagetk.algorithms.morphology import morphology

from tests.make_test_data import get_synthetic_data


class TestIntensityMorphology(unittest.TestCase):

    def setUp(self) -> None:
        self.image = get_synthetic_data("small", "wall")

    def tearDown(self) -> None:
        del self.image

    def _test_spatial_image(self, out_img):
        self.assertIsInstance(out_img, SpatialImage)
        self.assertIsNotNone(out_img.get_array())

    def test_dilation(self):
        out_img = morphology(self.image, method="dilation")
        self._test_spatial_image(out_img)

    def test_erosion(self):
        out_img = morphology(self.image, method="erosion")
        self._test_spatial_image(out_img)

    def test_closing(self):
        out_img = morphology(self.image, method="closing")
        self._test_spatial_image(out_img)

    def test_opening(self):
        out_img = morphology(self.image, method="opening")
        self._test_spatial_image(out_img)

    def test_hat_closing(self):
        out_img = morphology(self.image, method="hat-closing")
        self._test_spatial_image(out_img)

    def test_hat_opening(self):
        out_img = morphology(self.image, method="hat-opening")
        self._test_spatial_image(out_img)

    def test_contrast(self):
        out_img = morphology(self.image, method="contrast")
        self._test_spatial_image(out_img)

    def test_gradient(self):
        out_img = morphology(self.image, method="gradient")
        self._test_spatial_image(out_img)

    def test_oc_alternate_sequential_filter(self):
        out_img = morphology(self.image, method="oc_alternate_sequential_filter")
        self._test_spatial_image(out_img)

    def test_co_alternate_sequential_filter(self):
        out_img = morphology(self.image, method="co_alternate_sequential_filter")
        self._test_spatial_image(out_img)

    def test_coc_alternate_sequential_filter(self):
        out_img = morphology(self.image, method="coc_alternate_sequential_filter")
        self._test_spatial_image(out_img)

    def test_oco_alternate_sequential_filter(self):
        out_img = morphology(self.image, method="oco_alternate_sequential_filter")
        self._test_spatial_image(out_img)


class TestLabelledMorphology(unittest.TestCase):

    def setUp(self) -> None:
        self.image = get_synthetic_data("small", "labelled")

    def tearDown(self) -> None:
        del self.image

    def _test_labelled_image(self, out_img):
        self.assertIsInstance(out_img, LabelledImage)
        self.assertIsNotNone(out_img.get_array())

    def test_dilation(self):
        out_img = label_filtering(self.image, method="dilation")
        self._test_labelled_image(out_img)

    def test_erosion(self):
        out_img = label_filtering(self.image, method="erosion")
        self._test_labelled_image(out_img)

    def test_closing(self):
        out_img = label_filtering(self.image, method="closing")
        self._test_labelled_image(out_img)

    def test_opening(self):
        out_img = label_filtering(self.image, method="opening")
        self._test_labelled_image(out_img)

    def test_oc_alternate_sequential_filter(self):
        out_img = label_filtering(self.image, method="oc_alternate_sequential_filter")
        self._test_labelled_image(out_img)

    def test_co_alternate_sequential_filter(self):
        out_img = label_filtering(self.image, method="co_alternate_sequential_filter")
        self._test_labelled_image(out_img)

    def test_coc_alternate_sequential_filter(self):
        out_img = label_filtering(self.image, method="coc_alternate_sequential_filter")
        self._test_labelled_image(out_img)

    def test_oco_alternate_sequential_filter(self):
        out_img = label_filtering(self.image, method="oco_alternate_sequential_filter")
        self._test_labelled_image(out_img)


class TestMorphologyMultiChannel(unittest.TestCase):

    def setUp(self) -> None:
        self.image = get_synthetic_data("xsmall", "multichannel")

    def tearDown(self) -> None:
        del self.image

    def _test_spatial_image(self, out_img):
        self.assertIsInstance(out_img, SpatialImage)
        self.assertIsNotNone(out_img.get_array())

    def _test_multi_channel_image(self, out_img):
        self.assertIsInstance(out_img, MultiChannelImage)
        self.assertIsNotNone(out_img.get_array())

    def test_dilation(self):
        out_img = morphology(self.image, method="dilation")
        self._test_multi_channel_image(out_img)
        out_img = morphology(self.image, channel='wall', method="dilation")
        self._test_spatial_image(out_img)

    def test_erosion(self):
        out_img = morphology(self.image, method="erosion")
        self._test_multi_channel_image(out_img)
        out_img = morphology(self.image, channel='wall', method="erosion")
        self._test_spatial_image(out_img)

    def test_closing(self):
        out_img = morphology(self.image, method="closing")
        self._test_multi_channel_image(out_img)
        out_img = morphology(self.image, channel='wall', method="closing")
        self._test_spatial_image(out_img)

    def test_opening(self):
        out_img = morphology(self.image, method="opening")
        self._test_multi_channel_image(out_img)
        out_img = morphology(self.image, channel='wall', method="opening")
        self._test_spatial_image(out_img)

    def test_hat_closing(self):
        out_img = morphology(self.image, method="hat-closing")
        self._test_multi_channel_image(out_img)
        out_img = morphology(self.image, channel='wall', method="hat-closing")
        self._test_spatial_image(out_img)

    def test_hat_opening(self):
        out_img = morphology(self.image, method="hat-opening")
        self._test_multi_channel_image(out_img)
        out_img = morphology(self.image, channel='wall', method="hat-opening")
        self._test_spatial_image(out_img)

    def test_contrast(self):
        out_img = morphology(self.image, method="contrast")
        self._test_multi_channel_image(out_img)
        out_img = morphology(self.image, channel='wall', method="contrast")
        self._test_spatial_image(out_img)

    def test_gradient(self):
        out_img = morphology(self.image, method="gradient")
        self._test_multi_channel_image(out_img)
        out_img = morphology(self.image, channel='wall', method="gradient")
        self._test_spatial_image(out_img)

    def test_oc_alternate_sequential_filter(self):
        out_img = morphology(self.image, method="oc_alternate_sequential_filter")
        self._test_multi_channel_image(out_img)
        out_img = morphology(self.image, channel='wall', method="oc_alternate_sequential_filter")
        self._test_spatial_image(out_img)

    def test_co_alternate_sequential_filter(self):
        out_img = morphology(self.image, method="co_alternate_sequential_filter")
        self._test_multi_channel_image(out_img)
        out_img = morphology(self.image, channel='wall', method="co_alternate_sequential_filter")
        self._test_spatial_image(out_img)

    def test_coc_alternate_sequential_filter(self):
        out_img = morphology(self.image, method="coc_alternate_sequential_filter")
        self._test_multi_channel_image(out_img)
        out_img = morphology(self.image, channel='wall', method="coc_alternate_sequential_filter")
        self._test_spatial_image(out_img)

    def test_oco_alternate_sequential_filter(self):
        out_img = morphology(self.image, method="oco_alternate_sequential_filter")
        self._test_multi_channel_image(out_img)
        out_img = morphology(self.image, channel='wall', method="oco_alternate_sequential_filter")
        self._test_spatial_image(out_img)


if __name__ == '__main__':
    unittest.main()
    gc.collect()
