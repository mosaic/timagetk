#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2022, all rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import gc
import unittest

import numpy as np

from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image
from timagetk.tasks.segmentation import watershed_segmentation


class TestSegmentation(unittest.TestCase):

    def setUp(self):
        np.random.seed(42)

        self.n_layers = 1
        self.n_points = 10
        self.intensity = 10000

        img, p = example_layered_sphere_wall_image(n_points=self.n_points,
                                                   n_layers=self.n_layers,
                                                   wall_intensity=self.intensity,
                                                   return_points=True)
        self.img = img
        self.points = p

    def tearDown(self):
        del self.n_layers
        del self.n_points
        del self.intensity
        del self.img
        del self.points
        gc.collect()

    def test_segmentation(self):
        seg_img, seed_img, process = watershed_segmentation(self.img, h_min=100, sigma=0.75, equalize_hist=False)

        self.assertTrue(seed_img.shape == self.img.shape)
        np.testing.assert_almost_equal(seed_img.voxelsize, self.img.voxelsize, decimal=6)

        self.assertTrue(seg_img.shape == self.img.shape)
        np.testing.assert_almost_equal(seg_img.voxelsize, self.img.voxelsize, decimal=6)

        self.assertTrue(len(seed_img.labels()) == len(self.points) + 1)
        self.assertTrue(len(seg_img.labels()) == len(self.points) + 1)


if __name__ == '__main__':
    unittest.main()
