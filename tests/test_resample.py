#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import gc
import unittest

from tests.make_test_data import get_synthetic_data
from timagetk import LabelledImage
from timagetk import MultiChannelImage
from timagetk import SpatialImage
from timagetk.algorithms.resample import isometric_resampling
from timagetk.algorithms.resample import resample
from timagetk.third_party.vt_parser import GRAY_INTERPOLATION_METHODS
from timagetk.third_party.vt_parser import LABEL_INTERPOLATION_METHODS


class TestResampleIntensity(unittest.TestCase):

    def setUp(self) -> None:
        self.image = get_synthetic_data("medium", "wall")
        self.new_vxs = [1., 1., 1.]
        self.new_shape = [50, 50, 50]

    def tearDown(self) -> None:
        del self.image
        del self.new_vxs
        del self.new_shape
        gc.collect()

    def _test_resample_vxs(self, interpolation):
        res_img = resample(self.image, voxelsize=self.new_vxs, interpolation=interpolation)
        self.assertEqual(res_img.get_voxelsize(), self.new_vxs)
        self.assertIsInstance(res_img, SpatialImage)
        self.assertIsNotNone(res_img.get_array())

    def _test_resample_shape(self, interpolation):
        res_img = resample(self.image, shape=self.new_shape, interpolation=interpolation)
        self.assertEqual(res_img.get_shape(), self.new_shape)
        self.assertIsInstance(res_img, SpatialImage)
        self.assertIsNotNone(res_img.get_array())

    def test_resample_vxs(self):
        for interpolation in GRAY_INTERPOLATION_METHODS:
            self._test_resample_vxs(interpolation)

    def test_resample_shape(self):
        for interpolation in GRAY_INTERPOLATION_METHODS:
            self._test_resample_shape(interpolation)


class TestResampleMultiChannel(unittest.TestCase):

    def setUp(self) -> None:
        self.image = get_synthetic_data("medium", "multichannel")
        self.new_vxs = [1., 1., 1.]
        self.new_shape = [50, 50, 50]

    def tearDown(self) -> None:
        del self.image
        del self.new_vxs
        del self.new_shape
        gc.collect()

    def _test_resample_vxs(self, interpolation):
        res_img = resample(self.image, voxelsize=self.new_vxs, interpolation=interpolation)
        self.assertEqual(res_img.get_voxelsize(), self.new_vxs)
        self.assertIsInstance(res_img, MultiChannelImage)
        self.assertIsNotNone(res_img.get_array())
        res_img = resample(self.image, channel='wall', voxelsize=self.new_vxs, interpolation=interpolation)
        self.assertEqual(res_img.get_voxelsize(), self.new_vxs)
        self.assertIsInstance(res_img, SpatialImage)
        self.assertIsNotNone(res_img.get_array())

    def _test_resample_shape(self, interpolation):
        res_img = resample(self.image, shape=self.new_shape, interpolation=interpolation)
        self.assertEqual(res_img.get_shape()[:-1], self.new_shape)
        self.assertIsInstance(res_img, MultiChannelImage)
        self.assertIsNotNone(res_img.get_array())
        res_img = resample(self.image, channel='wall', shape=self.new_shape, interpolation=interpolation)
        self.assertEqual(res_img.get_shape(), self.new_shape)
        self.assertIsInstance(res_img, SpatialImage)
        self.assertIsNotNone(res_img.get_array())

    def test_resample_vxs(self):
        for interpolation in GRAY_INTERPOLATION_METHODS:
            self._test_resample_vxs(interpolation)

    def test_resample_shape(self):
        for interpolation in GRAY_INTERPOLATION_METHODS:
            self._test_resample_shape(interpolation)


class TestResampleLabelled(unittest.TestCase):

    def setUp(self) -> None:
        self.image = get_synthetic_data("medium", "labelled")
        self.new_vxs = [1., 1., 1.]
        self.new_shape = [50, 50, 50]

    def tearDown(self) -> None:
        del self.image
        del self.new_vxs
        del self.new_shape
        gc.collect()

    def _test_resample_vxs(self, interpolation, cell_based_sigma=None):
        res_img = resample(self.image, voxelsize=self.new_vxs, interpolation=interpolation,
                           cell_based_sigma=cell_based_sigma)
        self.assertIsInstance(res_img, LabelledImage)
        self.assertIsNotNone(res_img.get_array())
        self.assertEqual(res_img.get_voxelsize(), self.new_vxs)

    def _test_resample_shape(self, interpolation, cell_based_sigma=None):
        res_img = resample(self.image, shape=self.new_shape, interpolation=interpolation,
                           cell_based_sigma=cell_based_sigma)
        self.assertIsInstance(res_img, LabelledImage)
        self.assertIsNotNone(res_img.get_array())
        self.assertEqual(res_img.get_shape(), self.new_shape)

    def test_resample_vxs(self):
        for interpolation in LABEL_INTERPOLATION_METHODS:
            self._test_resample_vxs(interpolation, cell_based_sigma=1.)

    def test_resample_shape(self):
        for interpolation in LABEL_INTERPOLATION_METHODS:
            self._test_resample_shape(interpolation, cell_based_sigma=1.)


class TestIsoResampleIntensity(unittest.TestCase):

    def setUp(self) -> None:
        self.image = get_synthetic_data("medium", "wall")

    def tearDown(self) -> None:
        del self.image
        gc.collect()

    def _test_iso_resample(self, method, interpolation, vxs):
        res_img = isometric_resampling(self.image, value=method, interpolation=interpolation)
        self.assertIsInstance(res_img, SpatialImage)
        self.assertIsNotNone(res_img.get_array())
        self.assertTrue(res_img.get_voxelsize('z') == vxs)
        del res_img

    def test_iso_resample(self):
        for method in ['min', 'max', 0.5, 1.5]:
            for interpolation in GRAY_INTERPOLATION_METHODS:
                if method == 'min':
                    vxs = min(self.image.get_voxelsize())
                elif method == 'max':
                    vxs = max(self.image.get_voxelsize())
                else:
                    vxs = method
                self._test_iso_resample(method, interpolation, vxs)


class TestIsoResampleMultiChannel(unittest.TestCase):

    def setUp(self) -> None:
        self.image = get_synthetic_data("medium", "multichannel")

    def tearDown(self) -> None:
        del self.image
        gc.collect()

    def _test_iso_resample(self, value, interpolation, vxs):
        res_img = isometric_resampling(self.image, value=value, interpolation=interpolation)
        self.assertIsInstance(res_img, MultiChannelImage)
        self.assertIsNotNone(res_img.get_array())
        self.assertTrue(res_img.get_voxelsize('z') == vxs)
        res_img = isometric_resampling(self.image, value=value, interpolation=interpolation, channel='wall')
        self.assertIsInstance(res_img, SpatialImage)
        self.assertIsNotNone(res_img.get_array())
        self.assertTrue(res_img.get_voxelsize('z') == vxs)
        del res_img

    def test_iso_resample(self):
        for method in ['min', 'max', 0.5, 1.5]:
            for interpolation in GRAY_INTERPOLATION_METHODS:
                if method == 'min':
                    vxs = min(self.image.get_voxelsize())
                elif method == 'max':
                    vxs = max(self.image.get_voxelsize())
                else:
                    vxs = method
                self._test_iso_resample(method, interpolation, vxs)


class TestIsoResampleLabelled(unittest.TestCase):

    def setUp(self) -> None:
        self.image = get_synthetic_data("medium", "labelled")

    def tearDown(self) -> None:
        del self.image
        gc.collect()

    def _test_iso_resample(self, value, interpolation, vxs, cell_based_sigma):
        res_img = isometric_resampling(self.image, value=value, interpolation=interpolation,
                                       cell_based_sigma=cell_based_sigma)
        self.assertIsInstance(res_img, LabelledImage)
        self.assertIsNotNone(res_img.get_array())
        self.assertTrue(res_img.get_voxelsize('z') == vxs)
        del res_img

    def test_iso_resample(self):
        for method in ['min', 'max', 0.5, 1.5]:
            for interpolation in LABEL_INTERPOLATION_METHODS:
                if method == 'min':
                    vxs = min(self.image.get_voxelsize())
                elif method == 'max':
                    vxs = max(self.image.get_voxelsize())
                else:
                    vxs = method
                self._test_iso_resample(method, interpolation, vxs, cell_based_sigma=1.)


if __name__ == '__main__':
    unittest.main()
