#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import gc
import unittest
from math import pi

import numpy as np
from skimage.morphology import ball

from timagetk.components.labelled_image import LabelledImage
from timagetk.io.image import default_image_attributes
from timagetk.third_party.vt_features import get_area
from timagetk.third_party.vt_features import get_barycenter
from timagetk.third_party.vt_features import get_labels
from timagetk.third_party.vt_features import get_volume


def mini_labelled_image(shape=(5, 10, 10), size2=(5, 5, 5), voxelsize=(1, 0.5, 0.5)):
    """A mini labelled image with two labels.

    Parameters
    ----------
    shape : list, optional
        The shape of the array.
    size2 : list, optional
        the ZYX shape of label ``2``.
    voxelsize : list, optional
        The voxelsize of the image.

    Returns
    -------
    LabelledImage
        The mini labelled image

    """
    arr = np.ones(shape, dtype='uint16')
    image = LabelledImage(arr, not_a_label=0, **default_image_attributes(arr, voxelsize=voxelsize))
    zs, ys, xs = size2
    image[:zs, :ys, :xs] = 2
    return image


def mini_shpere_image(radius, voxelsize=(1, 0.5, 0.5)):
    """A mini labelled image with two labels and one as a sphere.

    Parameters
    ----------
    radius : int
        the radius for the sphere of label ``2``.
    voxelsize : list, optional
        The voxelsize of the image.

    Returns
    -------
    LabelledImage
        The mini labelled image containing a sphere.

    """
    arr = ball(radius) + 1
    return LabelledImage(arr, not_a_label=0, **default_image_attributes(arr, voxelsize=voxelsize, dtype='uint16'))


class TestVTFeatures(unittest.TestCase):
    """
    """

    def test_labels(self):
        image = mini_labelled_image()
        labels = get_labels(image)
        self.assertListEqual(labels, [1, 2])

    def test_area(self):
        radius = 100
        area_voxels = 4 * pi * radius ** 2
        image = mini_shpere_image(radius, voxelsize=[0.5, 0.5, 0.5])
        vxs = min(image.get_voxelsize())  # image is resampled to highest resolution
        voxel_ppty = get_area(image, real=False)
        real_ppty = get_area(image, real=True)
        self.assertAlmostEqual(voxel_ppty[2], area_voxels, places=0)
        self.assertAlmostEqual(real_ppty[2], area_voxels * vxs ** 2, places=0)

    def test_volume(self):
        label_2_shape = (5, 5, 5)
        image = mini_labelled_image(size2=label_2_shape)
        voxel_ppty = get_volume(image, real=False)
        real_ppty = get_volume(image, real=True)
        self.assertEqual(voxel_ppty[2], np.prod(label_2_shape))
        self.assertEqual(real_ppty[2], np.prod(label_2_shape) * np.prod(image.get_voxelsize()))

    def test_barycenters(self):
        label_2_shape = (5, 5, 5)
        bary_voxel = [2., 2., 2.]
        image = mini_labelled_image(size2=label_2_shape)
        # Test in voxel units:
        voxel_ppty = get_barycenter(image, real=False)
        self.assertListEqual(list(voxel_ppty[2]), bary_voxel)
        # Test in real units:
        real_ppty = get_barycenter(image, real=True)
        self.assertListEqual(list(real_ppty[2]), list(np.array(bary_voxel) * image.get_voxelsize()[::-1]))

    # def test_covariance(self):
    #     label_2_shape = (5, 5, 5)
    #     cov_mat_voxel = np.diag(np.ones(3)) * 2
    #     image = mini_labelled_image(size2=label_2_shape)
    #     voxel_ppty = get_covariance(image, real=False)
    #     np.testing.assert_array_almost_equal(voxel_ppty[2], cov_mat_voxel)


if __name__ == '__main__':
    unittest.main()
    gc.collect()