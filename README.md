# Tissue Image ToolKit

![Anaconda version](https://anaconda.org/mosaic/timagetk/badges/version.svg)
![Platform availability](https://anaconda.org/mosaic/timagetk/badges/platforms.svg)
![License](https://anaconda.org/mosaic/timagetk/badges/license.svg)
![Latest release date](https://anaconda.org/mosaic/timagetk/badges/latest_release_date.svg)

_TimageTK_ (Tissue Image Toolkit) is a Python package dedicated to **image processing of multicellular architectures**, such as plants or animals.
It is intended for biologists, modellers and computer scientists.

The package provides the following main functionalities:

* **Image Filtering**: gaussian smoothing, gradient, hessian, laplacian, ...
* **Mathematical Morphology**: erosion, dilation, opening, closing, hat transform, sequential filters, ...
* **Intensity Image Registration**: rigid, affine and deformable registration, composition of transformations, sequence registration, multi-view fusion, ...
* **Intensity Image Segmentation**: h-transform, connected-component labeling, watershed, ...
* **Visualisation**: Stack browser, orthogonal views, projection maps and other GUI.
* **Batch Processing**: Simple JSON-based data structure to batch process large data sets.

Read TimageTK [documentation](https://mosaic.gitlabpages.inria.fr/timagetk) for more detailed information.


## Table of Contents

- [Installation](#installation)
- [Quick Start](#quick-start)
- [Organisation](#organisation)
- [Contributing](#contributing)
- [Additional Notes](#additional-notes)

## Installation

Assuming you have `conda` installed on your OS, install `timagetk` in a new conda environment named `titk` with:

```shell
conda create -n titk -c mosaic -c morpheme -c conda-forge timagetk
```

Look at the [installation](https://mosaic.gitlabpages.inria.fr/timagetk/install.html) page of the documentation for more.

## Quick Start

Once you have installed the `timagetk` library you can use it to analyse a biological tissue, for example, to compute the cells' volume in a cell-segmented image.

The following steps are required:

1. load an intensity image
2. performs watershed segmentation
3. compute cells' volume

```python
from timagetk import TissueImage3D
from timagetk.io import imread
from timagetk.tasks.segmentation import watershed_segmentation

# Download an image from Zenodo and read it:
int_image = imread('https://zenodo.org/record/7151866/files/p58-t0-imgFus.inr.gz',
                   hash_value='md5:48f6f9924289037c55ea785273c2fe72')
# Perform watershed cell-based segmentation:
seg_image, seed_image, params = watershed_segmentation(int_image, h_min=10, min_size=50, max_size=800)
# Convert to `TissueImage3D` type to access cell properties computation:
tissue = TissueImage3D(seg_image, background=1)
# Compute cells' volume:
volume = tissue.cells.volume()
```

You may visualize the cells' volume distribution on the 3D watershed cell-based segmentation using `pyvista`:

```python
import pyvista as pv
from timagetk.visu.pyvista import tissue_image_unstructured_grid

bkgd_id = tissue.background  # get the id (label) of the background 
l1_cells = tissue.cells.neighbors(bkgd_id)[bkgd_id]  # list the background's neighbors to get L1 cells
# Create a mesh representation for the cells that belong to the first layer:
grid = tissue_image_unstructured_grid(tissue, labels=l1_cells, resampling_voxelsize=1)
# Plot the cell meshes:
plotter = pv.Plotter()
plotter.add_mesh(grid, scalars='volume', cmap='inferno')
plotter.show()
```

## Organisation

[//]: # (Badges explanations: https://shields.io/badges)

Lead developer: Jonathan Legrand ![badge_jo](https://img.shields.io/badge/Research%20Engineer-CNRS-00294b.svg?style=flat-square)

Coordination:
 - Christophe Godin [![badge_cg](https://img.shields.io/badge/Team%20Leader-Inria-e63312.svg?style=flat-square)](https://team.inria.fr/mosaic/welcome/team-members/christophe-godin/)
 - Grégoire Malandain [![badge_gm](https://img.shields.io/badge/Team%20Leader-Inria-e63312.svg?style=flat-square)](https://www-sop.inria.fr/members/Gregoire.Malandain/)
 - Teva Vernoux [![badge_tv](https://img.shields.io/badge/Team%20Leader-CNRS-00294b.svg?style=flat-square)](https://www.ens-lyon.fr/RDP/signalisation-hormonale-et-developpement/)

Main contributors:
 - Guillaume Cerutti ![badge_gc](https://img.shields.io/badge/Research%20Engineer-INRAE-00a3a6.svg?style=flat-square)
 - Manuel Petit ![badge_mp](https://img.shields.io/badge/Research%20Engineer-Inria-e63312.svg?style=flat-square)

Former contributors: Guillaume Baty, Sophie Ribes, Frederic Boudon, Christophe Pradal

Written in: ![badge_osx](https://img.shields.io/badge/Python3-3.8,%203.9,%203.10,%203.11,%203.12-3776AB.svg?style=flat-square&logo=Python)

Supported OS:
 - ![badge_osx](https://img.shields.io/badge/linux--64-tested-brightgreen.svg?style=flat-square&logo=Ubuntu&logoColor=white)
 - ![badge_osx](https://img.shields.io/badge/osx--64-tested-brightgreen.svg?style=flat-square&logo=Apple&logoColor=white)
 - ![badge_osx_arm](https://img.shields.io/badge/osx--arm64-tested-brightgreen.svg?style=flat-square&logo=Apple&logoColor=white)
 - ![badge_windows](https://img.shields.io/badge/win--64-untested-yellow.svg?style=flat-square)

Licence: GPLv3+, see [LICENSE](LICENSE.md) file.

Active teams:

* Inria team [Mosaic](https://team.inria.fr/mosaic/), RDP-ENS Lyon, UMR5667.
* [Hormonal Signalling and Development](http://www.ens-lyon.fr/RDP/spip.php?rubrique20) team, RDP-ENS Lyon, UMR5667.
* Inria team [Morpheme](http://www-sop.inria.fr/morpheme/), Sophia Antipolis.

Former teams:

* Inria-Cirad-Inra [Virtual Plants](https://team.inria.fr/virtualplants/)
* Inria Project Lab [Morphogenetics](https://team.inria.fr/morphogenetics/)

## Contributing

We welcome all contributions to the development of TimageTK as it is intended as an open-source scientific software.

Have a look at the [contribution guidelines](https://mosaic.gitlabpages.inria.fr/timagetk/contributing.html) in the documentation for more details.

## Additional Notes

Images and other data structures can be rendered using the visualization functions we provide.
However, if you are less experienced with code, using a dedicated interactive image visualization software such as
[Gnomon](https://gitlab.inria.fr/gnomon/gnomon) or [Fiji](https://fiji.sc/) is recommended.

TimageTK leverages the work of some of the most standard Python libraries:

- [Matplotlib](http://matplotlib.org/)
- [NumPy](http://www.numpy.org/)
- [NetworkX](https://networkx.org/)
- [pandas](https://pandas.pydata.org/)
- [scikit-image](http://scikit-image.org)
- [scikit-learn](http://scikit-learn.org)

For 3D visualisations, we rely on:

- [plotly](https://plotly.com/python/)
- [PyVista](https://docs.pyvista.org/)


Other less "standard" libraries, developed by research teams, are used:

- [tifffile](https://pypi.org/project/tifffile/) by [Christoph Gohlke](cgohlke@uci.edu) _et al._
- [czifile](https://pypi.org/project/czifile/) by [Christoph Gohlke](cgohlke@uci.edu) _et al._
- [vt](https://gitlab.inria.fr/morpheme/vt) by [Grégoire Malandain](https://www-sop.inria.fr/members/Gregoire.Malandain/) _et al._
- [vt-python](https://gitlab.inria.fr/morpheme/vt-python) by [Grégoire Malandain](https://www-sop.inria.fr/members/Gregoire.Malandain/) _et al._
- [MorphoNet](https://morphonet.org/) by [Emmanuel Faure](https://www.lirmm.fr/~efaure/) _et al._
- [pytorch-3dunet](https://github.com/wolny/pytorch-3dunet) by Adrian Wolny & [PlantSeg](https://kreshuklab.github.io/plant-seg/) by Lorenzo Cerrone _et al._
