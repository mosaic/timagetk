# -*- coding: utf-8 -*-
import numpy as np
from timagetk.components.labelled_image import LabelledImage

DUMMY_SEG_2D = np.array([
    [2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5],
    [2, 2, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5],
    [2, 2, 2, 4, 4, 4, 4, 4, 4, 4, 5, 5],
    [2, 2, 2, 2, 4, 4, 4, 4, 4, 5, 5, 5],
    [2, 2, 2, 2, 3, 3, 3, 3, 3, 5, 5, 5],
    [2, 2, 2, 2, 3, 3, 3, 3, 3, 5, 5, 5],
    [2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 5, 5],
    [2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 5, 5],
    [2, 2, 2, 7, 3, 3, 3, 3, 3, 3, 6, 6],
    [2, 2, 7, 7, 7, 3, 3, 3, 3, 6, 6, 6],
    [2, 7, 7, 7, 7, 7, 3, 3, 6, 6, 6, 6],
    [7, 7, 7, 7, 7, 7, 7, 6, 6, 6, 6, 6],
    [7, 7, 7, 7, 7, 7, 7, 6, 6, 6, 6, 6]
])

DUMMY_SEG_2D = LabelledImage(DUMMY_SEG_2D, not_a_label=0)

DUMMY_SEG_3D = np.array([
    np.ones_like(DUMMY_SEG_2D),
    DUMMY_SEG_2D,
    DUMMY_SEG_2D,
    DUMMY_SEG_2D,
    DUMMY_SEG_2D
])
DUMMY_SEG_3D = LabelledImage(DUMMY_SEG_3D, not_a_label=0)


CLIQUE_SEG_2D = np.array([
    [2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 4, 4],
    [2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 4, 4],
    [2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 4, 4],
    [2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 4, 4],
    [2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 4, 4],
    [2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 4, 4],
    [2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 4, 4],
    [3, 3, 3, 3, 3, 3, 5, 5, 5, 5, 5, 5],
    [3, 3, 3, 3, 3, 3, 5, 5, 5, 5, 5, 5],
    [3, 3, 3, 3, 3, 3, 5, 5, 5, 5, 5, 5],
    [3, 3, 3, 3, 3, 3, 5, 5, 5, 5, 5, 5],
    [3, 3, 3, 3, 3, 3, 5, 5, 5, 5, 5, 5],
    [3, 3, 3, 3, 3, 3, 5, 5, 5, 5, 5, 5]
])
CLIQUE_SEG_2D = LabelledImage(CLIQUE_SEG_2D, not_a_label=0)

CLIQUE_SEG_3D = np.array([
    np.ones_like(CLIQUE_SEG_2D),
    CLIQUE_SEG_2D,
    CLIQUE_SEG_2D,
    CLIQUE_SEG_2D,
    CLIQUE_SEG_2D
])
CLIQUE_SEG_3D = LabelledImage(CLIQUE_SEG_3D, not_a_label=0)
