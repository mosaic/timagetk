#!/bin/bash

###############################################################################
# Example usages:
###############################################################################
# -- Default run options starts an interactive shell:
# $ ./run.sh
#
# -- Starts unit tests with nose & coverage:
# $ ./run.sh --unittest

user=$USER
uid=$(id -u)
gid=$(id -g)

db_path=''
vtag="latest"
cmd=''
unittest_cmd="nosetests -w timagetk/test -I testskip* --with-coverage --cover-package=timagetk"
mount_option=""

usage() {
  echo "USAGE:"
  echo "  ./run.sh [OPTIONS]
    "

  echo "DESCRIPTION:"
  echo "  Run 'timagetk:<vtag>' container with a mounted local (host) database.
    "

  echo "OPTIONS:"
  echo "  -t, --tag
    Docker image tag to use, default to '$vtag'.
    "
  echo "  -p, --database_path
    Path to the host database to mount inside docker container, requires '--user' if not defautl.
    "
  echo "  -u, --user
    User used during docker image build, default to '$user'.
    "
  echo "  -v, --volume
    Volume mapping for docker, e.g. '/abs/host/dir:/abs/container/dir'. Multiple use is allowed.
    "
  echo "  -c, --cmd
    Defines the command to run at docker startup, by default start an interactive container with a bash shell.
    "
  echo " --unittest_cmd
    Runs unit tests defined in timagetk.
    "
  echo "  -h, --help
    Output a usage message and exit.
    "
}

while [ "$1" != "" ]; do
  case $1 in
  -t | --tag)
    shift
    vtag=$1
    ;;
  -u | --user)
    shift
    user=$1
    ;;
  -p | --database_path)
    shift
    db_path=$1
    ;;
  -c | --cmd)
    shift
    cmd=$1
    ;;
  --unittest_cmd)
    cmd=$unittest_cmd
    ;;
  -v | --volume)
    shift
    if [ "$mount_option" == "" ]
    then
      mount_option="-v $1"
    else
      mount_option="$mount_option -v $1"  # append
    fi
    ;;
  -h | --help)
    usage
    exit
    ;;
  *)
    usage
    exit 1
    ;;
  esac
  shift
done

# Use 'host database path' & 'docker user' to create a bind mount:
if [ "$db_path" != "" ]
then
  mount_option="$mount_option -v $db_path:/tmp/data/"
fi

if [ "$cmd" = "" ]
then
  # Start in interactive mode:
  docker run $mount_option --user $uid:$gid --rm \
    -it timagetk:$vtag /bin/bash
else
  # Start in non-interactive mode (run the command):
  docker run $mount_option --user $uid:$gid --rm \
    timagetk:$vtag \
    bash -c "$cmd"
fi