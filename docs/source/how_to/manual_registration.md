# How-to performs manual 3D registration of intensity images

TimageTK provides a GUI tool to performs manual 3D registration of intensity images using expert landmarks definition on 2D maximum intensity projections and altitude maps.

## Manual definition of landmarks
For the sake of clarity we will use example images from the source repository.

