# Data structures in TimageTK

% Update the table of contents (the left 'quick browse' panel)
```{eval-rst}
.. toctree::
  :maxdepth: 2
  :hidden:

```

TimageTK offers several data structures.
We hereafter introduce them and explain their purpose.

## Overview

Based on the meaning of the values in the array(s) describing the image(s), we can differentiate two main types of images: **intensity** & **labelled** images.

### Intensity images
For *intensity images*, the values in the array(s) describing the image(s) represent a **signal intensity** obtained using microscopy imaging technique.

| Computer Science            | Biology                       |
|-----------------------------|-------------------------------|
| SpatialImage(numpy.ndarray) | IntensityImage(SpatialImage)  |
| MultiAngleImage()           | MultiAngleImage()             |
| MultiChannelImage()         | MultiChannelImage()           |
| TimeSeries()                | TimeSeries()                  |

:::{note}
a `TimeSeries` may be made of an ordered list of `SpatialImage` or `MultiChannelImage`!
:::


### Segmented images
In the case of *segmented images*, the values in the array(s) describing the image(s) represent a **label** or **cell id** obtained by transformation (segmentation and unique labelling) of the *intensity images* using computational technique(s).
It thus refers to an abstracted object.

| Computer Science            | Biology                       |
|-----------------------------|-------------------------------|
| LabelledImage(SpatialImage) | SegmentedImage(LabelledImage) |
| LabelledTimeSeries()        | DynamicTissue()               |

:::{note}
a `LabelledTimeSeries`, may or may not have a lineage tree!
:::


## Class `SpatialImage`
This is our **base image class** inheriting from NumPy `ndarray` class.

It has methods to access and edit **critical image attributes** such as:

  - array shape
  - image origin
  - voxel-size
  - physical extent.

Array modification methods also exists and should be preferred to the ones from NumPy in order to ensure the consistency of the `SpatialImage` data structure.

### Requirements
Use is limited to 2D or 3D images.
Physical properties of the image such as its shape, voxelsize, extent & origin are protected.

```{eval-rst}
.. todo:: Algorithms should preserve reference to original image, add their names and used parameters to metadata (`ProcessingMetadata`).

```


### Specifications
1. Array:
   - inherit from numpy.ndarray;
   - row-major order (C-contiguous);
2. Use hidden attributes behind properties with no setter;
3. Use `Metadata` class (metadata dictionary) & a filename attribute;
4. Use `ProcessingMetadata`;

### Known issues
- changing physical properties can be done by array re-slicing, e.g. `img2 = img[::2, ::2, ::2]` take a slice every two; need to have a method to update image properties & metadata
- no distinctions can be done between intensity and labeled images; use extra classes or attributes?


## Class `LabelledImage`
This class inherit from `SpatialImage` and is **specific** to labelled image representation.
This mean that in this data structure, the array does not represent a signal intensity anymore, but rather the cells position in space.

This type of data is often obtained by segmentation procedures like watershed, snake contours or other ML assisted segmentation methods.

### Requirements
Same as `SpatialImage`.

### Specifications
Same as `SpatialImage`.

### Known issues

- when using an image reader, we have no idea if we are loading a `SpatialImage` or a `LabelledImage` in the case of a 2D or 3D image.


## Class `MultiAngleImage`
This module implement a class dedicated to `multi-angle images`, meaning that we have a set of images representing the same observed object but from different points of view.

:::{important}
A set of views should be obtained in a short time window to ensure growth has not altered the object under observation between the first and the last acquisition.
:::

Since they can be made of a potentially large number of images (that would fill up the available memory), we implemented this class with "on the fly disk file access" instead of loading arrays in memory.

### Requirements

### Specifications

### Known issues
- `imread` cannot returns `MultiAngleImage`, they have to be manually initialized


## Class `MultiChannelImage`
This module implement a class dedicated to `multiple channel images`, meaning that we have a set of images representing the same observed object but presenting signal intensities from different wavelength (like an RGB image).

:::{important}
This should be obtained in a short time window to ensure growth has not altered the object under observation between the first and the last acquisition.
:::

### Requirements

### Specifications

### Known issues
- We have a `read` method (only from CZI files!) but no `save` method!


## Class `TimeSeries`
This is not implemented yet.


## Class `Metadata`
This module implement a generic metadata class.

### Example

```python
>>> import numpy as np
>>> from timagetk.components.metadata import Metadata
>>> md = {'origin': [0, 0], 'shape': (10, 10), 'voxelsize':np.array([0.2, 0.2])}
>>> md = Metadata(md)
>>> md.origin
[0, 0]
>>> md.shape
(10, 10)
>>> md.get_dict()
{'origin': [0, 0], 'shape': (10, 10), 'voxelsize': [0.2, 0.2]}
```

## Class `ImageMetadata`
This module implement a metadata class specific to image information.

### Specifications

- Use 'timagetk' as the metadata main root key to store information
- Use 'class' key for class information
- Use a counter to sort operation orders
- Under each number save a dictionary with details of the modification or about algorithm applied, such as its name and their parameters or the name of the transformation (or even the transformation if linear!).

### Example

```python
>>> import numpy as np
>>> from timagetk.components.spatial_image import SpatialImage
>>> test_array = np.ones((5,5,10), dtype=np.uint8)
>>> image_1 = SpatialImage(test_array, voxelsize=[1., 1., 2.])
>>> image_iso = image_1.isometric_resampling()
>>> image_iso.metadata['timagetk']
{(1, 'resample_isotropic'):
    {'params': {},
    'called':
        {(1, 'resample'):
            {'params':
                {'image': ['', ''],
                'option': 'gray',
                'voxelsize': [1.0, 1.0, 1.0]},
            'called':
                {(1, 'apply_trsf'):
                    {'dtype': None,
                    'image': ['', ''],
                    'param_str_1': '-linear -template-dim 5 5 20 -template-voxel 1.0 1.0 1.0',
                    'param_str_2': ' -resize -interpolation linear',
                    'template_img': ['', ''],
                    'trsf': None}
                }
            }
        }
    }
}
```


## Class `ProcessingMetadata`
This is not implemented yet.

Use the function to add parameters to metadata when performing image modifications.
