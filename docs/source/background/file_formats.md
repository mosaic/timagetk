# File formats

% Update the table of contents (the left 'quick browse' panel)
```{eval-rst}
.. toctree::
  :maxdepth: 2
  :hidden:

```

There is a tremendous amount of image file formats (see [Wikipedia: Image file formats](https://en.wikipedia.org/wiki/Image_file_formats)) and multidimensional images are no exception with more than 150 formats known to bio-formats (see their [documentation](https://docs.openmicroscopy.org/bio-formats/6.3.0/supported-formats.html)).


## Supported formats
For now, we make use of the IO library from `VT` (Morpheme INRIA Team), thus `timagetk` accept the following formats:

- `INR`: allegedly an INRIA image format, no documentation available online. **Should probably be considered as DEPRECATED!** See: [bio-formats: INR](https://docs.openmicroscopy.org/bio-formats/6.3.0/formats/inr.html)
- `TIFF`: **Tagged Image File Format**, also abbreviated `TIF`, is a computer file format for storing raster graphics images, see [Wikipedia: TIFF](https://en.wikipedia.org/wiki/TIFF)
- `MHA`: no documentation available online, unsupported by bio-format. **Should probably be considered as DEPRECATED!**
- `LSM`: See: [bio-formats: Zeiss LSM](https://docs.openmicroscopy.org/bio-formats/5.8.2/formats/zeiss-lsm.html)

Although they are quite common formats, especially `TIFF` & `LSM`, these are very limited options and most of them only work with 2D or 3D images at best.


## Unsupported formats

There should be an effort to be able to handle more than these 4 file formats, notably to support `CZI` files that allows to save multidimensional images with more than 3 dimensions.
Supporting the


## Notes

`OME` & `OME-XML` formats from Omero also appears as a good idea!

`CZI` file can be read using a third party library.