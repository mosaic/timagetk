# Advanced concepts for image representation in computer science

% Update the table of contents (the left 'quick browse' panel)
```{eval-rst}
.. toctree::
  :maxdepth: 2
  :hidden:

```

## Array & memory

Source:[Wikipedia](https://en.wikipedia.org/wiki/Row-_and_column-major_order)

In computer science, **row-major order** (C-contiguous) and **column-major order** (F-contiguous) are methods for storing multidimensional arrays.
The difference between the orders lies in which elements of an array are contiguous in memory.
In row-major order, the consecutive elements of a row reside next to each other, whereas the same holds true for consecutive elements of a column in column-major order.

For matrices in mathematical notation, the first index indicates the row, and the second indicates the column, *e.g.*, given a matrix A, $a_{1,2}$ is in its first row and second column.
This convention is carried over to the syntax in programming languages, although often with indexes starting at 0 instead of 1.

:::{warning}
NumPy arrays are C-contiguous by default.
:::
