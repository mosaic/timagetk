# How to

The following notebooks are illustrated "how-to", presenting a more advance usage of the methods and algorithms to achieve complex tasks such as cell segmentation or temporal registration.

% Update the table of contents (the left 'quick browse' panel)
```{eval-rst}
.. toctree::
  :maxdepth: 2
  :caption: How-to

  how_to/data_organization.md
  how_to/manual_registration.md

  how_to/how_to-cell_based_watershed_segmentation.ipynb
  how_to/how_to-intensity_image_from_segmentation.ipynb
  how_to/how_to-manual_landmarks_definition.ipynb
  how_to/how_to-multi_angle_fusion.ipynb
  how_to/how_to-iterative_multi_angle_fusion.ipynb
  how_to/how_to-time_series_temporal_registration.ipynb
  how_to/how_to-registration_with_manual_initialisation.ipynb

  how_to/vt/VT - Images averaging.ipynb
  how_to/vt/VT - Transformations.ipynb
  how_to/vt/VT_-_Cell_Property.ipynb
  how_to/vt/vt-python - Blockmatching - command line.ipynb
  how_to/vt/vt-python - Memory management.ipynb

```

