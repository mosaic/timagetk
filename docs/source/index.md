---
author: Jonathan Legrand
---

# Tissue Image ToolKit

```{image} https://anaconda.org/mosaic/timagetk/badges/version.svg
:target: https://anaconda.org/mosaic/timagetk
```

```{image} https://anaconda.org/mosaic/timagetk/badges/latest_release_date.svg
:target: https://anaconda.org/mosaic/timagetk
```

```{image} https://anaconda.org/mosaic/timagetk/badges/platforms.svg
:target: https://anaconda.org/mosaic/timagetk
```

```{image} https://anaconda.org/mosaic/timagetk/badges/license.svg
:target: https://anaconda.org/mosaic/timagetk
```

```{image} https://anaconda.org/mosaic/timagetk/badges/downloads.svg
:target: https://anaconda.org/mosaic/timagetk
```

[//]: # ([![License: GPL v3]&#40;https://img.shields.io/badge/License-GPLv3-blue.svg&#41;]&#40;https://www.gnu.org/licenses/gpl-3.0&#41;)

---

% Table of contents

```{eval-rst}
.. toctree::
   :hidden:

   install
   background
   tutorials
   how_to
   reference
   cli
   developer
   contributing
   citing
   credits

```

_TimageTK_ (Tissue Image Toolkit) is a Python package dedicated to **image processing of multicellular architectures**, such as plants or animals.
It is intended for biologists, modellers and computer scientists.

## Overview

The package provides the following main functionalities:

:::{panels}
:container: +full-width text-center
:column: col-md-6 col-lg-4 px-2 py-2
:card: shadow
:header: text-center
:body: text-left

**Image Filtering**
^^^
Linear filter operators such as: Gaussian, gradient, hessian, laplacian, ...
---

**Mathematical Morphology**
^^^
Morphology operator such as: erosion, dilation, opening, closing, hat transform, sequential filters, ...
---

**Image Registration**
^^^
Linear and non-linear block-matching registration, transformation operators, sequence registration, multi-angle fusion, ...
---

**Image Segmentation**
^^^
h-transform, connected component labeling, watershed, ...
---

**Visualisation**
^^^
Stack browser, orthogonal views, projection maps and other GUI.
---

**Batch Processing**
^^^
Simple JSON-based data structure to batch process large data sets.

:::


## Organisation

:::{panels}
:container: +full-width text-center
:column: col-lg-4 px-2 py-2

Jonathan Legrand <br>
{badge}`Lead developer,badge-primary` {badge}`A,badge-info` {badge}`B,badge-info` {badge}`CNRS,badge-danger`
---
Christophe Godin <br>
{badge}`Coordinator,badge-secondary` {badge}`A,badge-info` {badge}`Inria,badge-danger`
---
Grégoire Malandain <br>
{badge}`Coordinator,badge-secondary` {badge}`C,badge-info` {badge}`Inria,badge-danger`
---
Teva Vernoux <br>
{badge}`Coordinator,badge-secondary` {badge}`B,badge-info` {badge}`CNRS,badge-danger`
---
Guillaume Baty <br>
{badge}`Contributor,badge-secondary`
---
Guillaume Cerutti  <br>
{badge}`Contributor,badge-secondary` {badge}`A,badge-info` {badge}`INRAe,badge-danger`
---
Sophie Ribes <br>
{badge}`Contributor,badge-secondary`

:::

## Active Research Teams

:::{panels}
:container: +full-width
:column: col-lg-4 px-2 py-2

{badge}`A,badge-info`
Inria team [Mosaic](https://team.inria.fr/mosaic/), RDP-ENS Lyon, UMR5667.
---
{badge}`B,badge-info`
[Hormonal Signalling and Development](http://www.ens-lyon.fr/RDP/spip.php?rubrique20) team, RDP-ENS Lyon, UMR5667.
---
{badge}`C,badge-info`
Inria team [Morpheme](http://www-sop.inria.fr/morpheme/), Sophia Antipolis.
:::

## Source Code Repository

### Inria GitLab Repository

The Inria [GitLab repository of TimageTK](https://gitlab.inria.fr/mosaic/timagetk) is maintained by members of the Mosaic team.
Use the [Issues](https://gitlab.inria.fr/mosaic/timagetk/-/issues) section to contact us if you have difficulties or issues.

### GitHub Repository

The [GitHub repository of TimageTK](https://github.com/VirtualPlants/timagetk.git) is not maintained anymore, but is kept for historic reasons.


## Contributing

We welcome all contributions to the development of TimageTK as it is intended as an open-source scientific software.

Have a look at the [contribution guidelines](https://mosaic.gitlabpages.inria.fr/timagetk/contributing.html) in the documentation for more details.

## Additional Notes

Images and other data structures can be rendered using the visualization functions we provide.
However, if you are less experienced with code, using a dedicated interactive image visualization software such as
[Gnomon](https://gitlab.inria.fr/gnomon/gnomon) or [Fiji](https://fiji.sc/) is recommended.

TimageTK leverages the work of some of the most standard Python libraries:

- [Matplotlib](http://matplotlib.org/)
- [NumPy](http://www.numpy.org/)
- [NetworkX](https://networkx.org/)
- [pandas](https://pandas.pydata.org/)
- [scikit-image](http://scikit-image.org)
- [scikit-learn](http://scikit-learn.org)

For 3D visualisations, we rely on:

- [plotly](https://plotly.com/python/)
- [PyVista](https://docs.pyvista.org/)


Other less "standard" libraries, developed by research teams, are used:

- [tifffile](https://pypi.org/project/tifffile/) by [Christoph Gohlke](cgohlke@uci.edu) _et al._
- [czifile](https://pypi.org/project/czifile/) by [Christoph Gohlke](cgohlke@uci.edu) _et al._
- [vt](https://gitlab.inria.fr/morpheme/vt) by [Grégoire Malandain](https://www-sop.inria.fr/members/Gregoire.Malandain/) _et al._
- [vt-python](https://gitlab.inria.fr/morpheme/vt-python) by [Grégoire Malandain](https://www-sop.inria.fr/members/Gregoire.Malandain/) _et al._
- [MorphoNet](https://morphonet.org/) by [Emmanuel Faure](https://www.lirmm.fr/~efaure/) _et al._
- [pytorch-3dunet](https://github.com/wolny/pytorch-3dunet) by Adrian Wolny & [PlantSeg](https://kreshuklab.github.io/plant-seg/) by Lorenzo Cerrone _et al._
