# Tutorials

% Update the table of contents (the left 'quick browse' panel)
```{eval-rst}
.. toctree::
  :hidden:
  :maxdepth: 1
  :caption: Tutorials

  tutorials/tutorial-image_io.ipynb
  tutorials/tutorial-linear_filters.ipynb
  tutorials/tutorial-intensity_images_averaging.ipynb
  tutorials/tutorial-intensity_images_registration.ipynb
  tutorials/tutorial-synthetic_data.ipynb
  tutorials/demo-TemporalTissueGraph_GUI.ipynb

```

The following notebooks are illustrated tutorials introducing, the basic usage of some methods or algorithms.


## Inputs & Outputs:
- [Image IO](tutorials/tutorial-image_io.ipynb)


## Intensity image algorithms:
- [Linear filters](tutorials/tutorial-linear_filters.ipynb)
- [Intensity images averaging](tutorials/tutorial-intensity_images_averaging.ipynb)
- [Intensity images registration](tutorials/tutorial-intensity_images_registration.ipynb)


## Graph GUI
- [Temporal Tissue Graph GUI](tutorials/demo-TemporalTissueGraph_GUI.ipynb)