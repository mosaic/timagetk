{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Registering two intensity images"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Aim & explanations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This *how-to* aims at explaining how to performs **image registration** between two intensity images using the basic algorithms from `timagetk`'s `algorithms` module.\n",
    "If it look too complicated, you can always use the `RegistrationPlugin` from the `plugins` module."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are many possible applications to *image registration*, here is a non-exhaustive list of examples:\n",
    "\n",
    " - if the two images are from a time-series, rigid registration will enable a sort of *temporal registration* of the two images that will then be aligned over time, obviously it is possible to use more than two images to obtain a temporally coherent \"film\" (see the corresponding tutorial \"how to register a time-series\");\n",
    " - if the two images are from a time-series, affine registration can give you the deformation undergone by the tissue in the three main axis of deformation;\n",
    " - if the two images are from a time-series, non-linear registration will give you a dense vectorfield that, under the right circumstances, can be used to precisely describe growth of the tissue;\n",
    " - if the two images are from a time-series and you have a cell-based segmentation, non-linear registration will allows you to compute cell lineage;\n",
    " - ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These applications are all based on the `blockmatching` algorithm, just remembers it register the \"signal\" and it is better to register the \"smallest\" (also often the \"youger\" when analysing growing tissues) tissue onto the \"largest\" (the \"older\" in the same way) tissue."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Import the required classes & functions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "from timagetk.algorithms.blockmatching import blockmatching\n",
    "from timagetk.algorithms.trsf import apply_trsf\n",
    "from timagetk.algorithms.trsf import compose_trsf\n",
    "from timagetk.components.multi_channel import BlendImage\n",
    "from timagetk.components.multi_channel import MultiChannelImage\n",
    "from timagetk.io.util import shared_data\n",
    "from timagetk.io import imread\n",
    "from timagetk.visu.stack import orthogonal_view"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the first line `%matplotlib inline` is only required here in the jupyter notebook to display matplotlib figures."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Load online multichannel data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We use a open-source online dataset available on [Zenodo](https://zenodo.org/record/3737795#.XrAYuHUzaV4) acquired by [C.S. Galvan-Ampudia](https://orcid.org/0000-0002-4779-3568).\n",
    "\n",
    "As per the documentation attached to the dataset, the used images are multichannel images:\n",
    "\n",
    "> File names containing qDII-CLV3-DR5 have the following data:\n",
    ">\n",
    ">  - **Channel 1**: DII-VENUS-N7\n",
    ">  - **Channel 2**: pPIN1:PIN1-GFP\n",
    ">  - **Channel 3**: Propidium Iodide (cell walls)\n",
    ">  - **Channel 4**: pRPS5a:TagBFP-SV40\n",
    ">  - **Channel 5**: pCLV3:mCherry-N7\n",
    "\n",
    "We will thus use channel 3 (Propidium Iodide or PI) for registration since it contains a cell-wall targeted signal (staining technique). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ch_names = ['DII-VENUS-N7', 'pPIN1:PIN1-GFP', 'Propidium Iodide', 'pRPS5a:TagBFP-SV40', 'pCLV3:mCherry-N7']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We defines the initial observation (t0) of the **younger tissue** as the *floating image* and the later observation (t0 + 5h) of the **older tissue** as the *reference image*.\n",
    "We do that because we are observing *growing tissues*, hence the *observation frame* of the older tissue might be bigger."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ref_url = \"https://zenodo.org/record/3737795/files/qDII-CLV3-PIN1-PI-E35-LD-SAM4-T10.czi\"\n",
    "ref_img = imread(ref_url, channel_names=ch_names)\n",
    "flo_url = \"https://zenodo.org/record/3737795/files/qDII-CLV3-PIN1-PI-E35-LD-SAM4-T5.czi\"\n",
    "flo_img = imread(flo_url, channel_names=ch_names)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's print some information about the *reference image* & its channels:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(ref_img)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also list the channel names using the `channel_names` attributes as follow:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ref_img.channel_names"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now select the channels to use for the registration algorithm:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ref_ch = ref_img.get_channel('Propidium Iodide')\n",
    "flo_ch = flo_img.get_channel('Propidium Iodide')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Equalizing images contrast  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To make sure the two images have similar \"brightness\" when displayed together, we adjust their constrast by stretching it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from timagetk.algorithms.exposure import global_contrast_stretch\n",
    "ref_ch = global_contrast_stretch(ref_ch)\n",
    "flo_ch = global_contrast_stretch(flo_ch)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's print some information about the selected channel from the reference image:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(ref_ch)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualize PI channels individually"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's have a quick look at the data using othogonal views:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "orthogonal_view(flo_ch)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "orthogonal_view(ref_ch)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualize blended PI channels"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "blend = BlendImage(flo_ch, ref_ch, colors=['green', 'red'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(blend.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "orthogonal_view(blend)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see the images are not \"aligned\".\n",
    "\n",
    "Note the the 'green' floral organ on the lower right corner is a removed organ between the two acquisitions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Translate & rotate with rigid registration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Rigid transformation estimation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We starts by estimating the *rigid transformation* between the two channels using the `blockmatching` algorithm.\n",
    "It will estimate the *optimal* **translation** and **rotation matrix** to apply to \"align\" them properly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trsf_rig = blockmatching(flo_ch, ref_ch, method='rigid')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "type(trsf_rig)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Apply estimated transformation to floating image"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now apply this estimated *rigid transformation* to the **floating image** to register it on the **reference image** frame:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reg_ch = apply_trsf(flo_ch, trsf_rig, interpolation='linear')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualizing blended PI channels after rigid registration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "blend = BlendImage(reg_ch, ref_ch, colors=['green', 'red'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "orthogonal_view(blend)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Linear deformation with affine transformation estimation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now try to explains the previously unexplained deformation using an *affine transformation* between the two *cell-wall channels* using the `blockmatching` algorithm.\n",
    "\n",
    "As we will initialize the algorithm with the previously estimated rigid transformation, we will get an estimate of the *optimal* **scaling** and **shearing matrix** to apply to \"align\" them properly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trsf_paff = blockmatching(flo_ch, ref_ch, method='affine', left_trsf=trsf_rig)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "type(trsf_paff)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Transformations composition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we initialized the `blockmatching` algorithm with a rigid tranformation using the `left_trsf` keyword argument, the returned `Trsf` object contains **only** the affine part of the **full** transformation registering `flo_ch` on `ref_ch`.\n",
    "To get this **full** transformation we thus need to *compose* the two transformations:\n",
    "\\begin{equation*}\n",
    "T_{rigid} \\circ T_{affine}\n",
    "\\end{equation*}\n",
    "\n",
    "This is can be accomplished using the `compose_trsf` algorithm:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trsf_aff = compose_trsf([trsf_rig, trsf_paff])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the `init_trsf` keyword argument would return this composition as an output of the `blockmatching` algorithm, but we detailled it here for the sake of clarity."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Apply estimated transformation to floating image"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now apply this estimated *affine transformation* to the **floating image** to register it on the **reference image** frame:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reg_ch = apply_trsf(flo_ch, trsf_aff, interpolation='linear')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualizing blended PI channels after affine registration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "blend = BlendImage(reg_ch, ref_ch, colors=['green', 'red'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "orthogonal_view(blend)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we note that altought some lateral organs (flower meristems) looks better \"aligned\", a linear deformation seems largely insufficient to explains observed deformation of a growing tissue like the shoot apical meristem.\n",
    "\n",
    "We wil now try to reach an even better alignment with a *non-linear estimation* of the deformation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##  Non-linear deformation with non-linear transformation estimation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now try to explains the previously unexplained deformation using an *non-linear transformation* between the two *cell-wall channels* using the `blockmatching` algorithm.\n",
    "\n",
    "As we will initialize the algorithm with the previously estimated rigid transformation, we will get an estimate of the *optimal* **non-linear deformation** (excluding translation and rotation) to apply to \"align\" them \"perfectly\":"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trsf_vf = blockmatching(flo_ch, ref_ch, method='vectorfield', init_trsf=trsf_rig)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "type(trsf_vf)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the `init_trsf` keyword argument would return this composition as an output of the `blockmatching` algorithm, but we detailled it here for the sake of clarity."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Apply estimated transformation to floating image"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now apply this estimated *non-linear transformation* to the **floating image** to register it on the **reference image** frame:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reg_ch = apply_trsf(flo_ch, trsf_vf, interpolation='linear')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualize blended PI channels after non-linear registration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "blend = BlendImage(reg_ch, ref_ch, colors=['green', 'red'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "orthogonal_view(blend)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can see that, in the central part of the image, signals (cell-wall) of both images is matching well.\n",
    "However, due to the limitation of vector norms and its regularity, lateral organs are not well aligned."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Apply a transformation to the whole multichannel image"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After performing one, or more, of these registrations you may want to apply the transformation of your choosing to the whole multichannel image. Note that it is what would happen if you used the `RegistrationPlugin` with `MultiChannelImage`s."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We here assume that you want to apply the **rigid transformation** to the other channels, but the code is the same for any transformation you have computed beforehand."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "channels = {ch: apply_trsf(flo_img[ch], trsf_rig, interpolation='linear') for ch in flo_img.get_channel_names()}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reg_flo_img = MultiChannelImage(channels)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ch_colors = ['yellow', 'green', 'red', 'blue', 'cyan']\n",
    "print(dict(zip(ch_names, ch_colors)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "orthogonal_view(reg_flo_img.to_blend_image(colors=ch_colors, rtype='uint8'))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
