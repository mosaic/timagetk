{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tutorial on linear filters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from timagetk.algorithms.linearfilter import linearfilter\n",
    "from timagetk.third_party.vt_parser import FILTERING_METHODS\n",
    "from timagetk.io import imread\n",
    "from timagetk.io.util import shared_data\n",
    "from timagetk.visu.mplt import grayscale_imshow"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Linear filters are applied on *intensity images* (grayscale)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "image_path = shared_data('sphere_membrane_0.0.inr.gz', 'sphere')\n",
    "img = imread(image_path)\n",
    "middle_z_slice = img.get_shape('z') // 2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## List of filtering methods"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(FILTERING_METHODS)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Gaussian smoothing"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Gaussian smoothing* is often used to reduce noise and sharp local variations in intensity images."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fimg = linearfilter(img, method='smoothing', sigma=1.0, real=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We advise to use `real=True` to avoid applying anisotropic filtering on anisotropic images."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The 'intensity' of the smoothing is controlled by the parameter `sigma`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fimg2 = linearfilter(img, method='smoothing', sigma=2.0, real=True)\n",
    "fimg4 = linearfilter(img, method='smoothing', sigma=4.0, real=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "img_titles = ['Original', f'Sigma=1.0', f'Sigma=2.0', f'Sigma=4.0']\n",
    "fig, axes = grayscale_imshow([img, fimg, fimg2, fimg4], slice_id=middle_z_slice, title=img_titles, max_per_line=2, figsize=(8, 8), dpi=96)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that it can decrease the contrast!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Gradient filtering"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An **image gradient** is a directional change in the intensity or color in an image.\n",
    "https://en.wikipedia.org/wiki/Image_gradient"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fimg = linearfilter(img, method='gradient')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "img_titles = ['Original', f'Gradient']\n",
    "fig, axes = grayscale_imshow([img, fimg], slice_id=middle_z_slice, title=img_titles, figsize=(8, 8), dpi=96)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Hessian filtering"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In mathematics, the **Hessian matrix** or **Hessian** is a square matrix of second-order partial derivatives of a scalar-valued function, or scalar field.\n",
    "https://en.wikipedia.org/wiki/Hessian_matrix"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fimg = linearfilter(img, method='hessian')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "img_titles = ['Original', f'Hessian']\n",
    "fig, axes = grayscale_imshow([img, fimg], slice_id=middle_z_slice, title=img_titles, figsize=(8, 8), dpi=96)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Laplacian filtering"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Discrete Laplace operator is often used in image processing *e.g.* in edge detection and motion estimation applications.\n",
    "https://en.wikipedia.org/wiki/Discrete_Laplace_operator#Image_Processing"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fimg = linearfilter(img, method='laplacian')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "img_titles = ['Original', f'Laplacian']\n",
    "fig, axes = grayscale_imshow([img, fimg], slice_id=middle_z_slice, title=img_titles, figsize=(8, 8), dpi=96)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Other gradient filters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gfilters = ['gradient-hessian', 'gradient-laplacian', 'gradient-extrema']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fimg = [linearfilter(img, method=fmeth) for fmeth in gfilters]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "img_titles = ['Original'] + gfilters\n",
    "fig, axes = grayscale_imshow([img]+fimg, slice_id=middle_z_slice, title=img_titles, max_per_line=2, figsize=(8, 8), dpi=96)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Zero-crossing filters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "zfilters = ['zero-crossings-hessian', 'zero-crossings-laplacian']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fimg = [linearfilter(img, method=fmeth) for fmeth in zfilters]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "img_titles = ['Original'] + zfilters\n",
    "fig, axes = grayscale_imshow([img]+fimg, slice_id=middle_z_slice, title=img_titles, max_per_line=3, figsize=(8, 8), dpi=96)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
