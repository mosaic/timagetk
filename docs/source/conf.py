# -*- coding: utf-8 -*-
#
# timagetk documentation build configuration file, created by
# sphinx-quickstart on Mon May 30 19:19:49 2016.
#
# This file is execfile()d with the current directory set to its
# containing dir.
#
# Note that not all possible configuration values are present in this
# autogenerated file.
#
# All configuration values have a default; values that are commented out
# serve to show the default.

import sys
from os.path import abspath
from os.path import dirname
from os.path import join

import toml

base_dir = abspath(join(dirname(__file__), "..", ".."))
pyproj = toml.load(join(base_dir, "pyproject.toml"))

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
sys.path.insert(0, base_dir)

# -- Plotly configuration ----------------------------------------------
# Enable plotly figure in the docs
# https://myst-nb.readthedocs.io/en/latest/render/interactive.html
# html_js_files = ["https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.4/require.min.js",
#                  "https://cdn.plot.ly/plotly-2.8.3.min.js"]  # Does not work!
# html_js_files = ["https://cdnjs.cloudflare.com/ajax/libs/require.js/2.3.4/require.min.js"]


# -- General configuration ------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
# needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    # 'myst_parser',  # MyST Markdown parser, activated by 'myst_nb'
    'myst_nb',  # MyST jupyter notebooks parser
    'sphinx.ext.autodoc',  # Include documentation from docstrings
    'sphinx.ext.doctest',  # Test snippets in the documentation
    'sphinx.ext.intersphinx',  # Link to other projects' documentation
    'sphinx.ext.napoleon',  # Support for NumPy and Google style docstrings
    'sphinx.ext.todo',  # Support for todo items
    'sphinx.ext.viewcode',  # Add links to highlighted source code
    'matplotlib.sphinxext.plot_directive',  # To generate plot using matplotlib
    'sphinx.ext.inheritance_diagram',  # Add inheritance diagram directive
    'sphinx_panels',  # Enable panels, cards & tabs usage
    'sphinx_copybutton',  # Enable a copy button in each code-block
    'sphinx-prompt',  # Show a prompt on each line but making it un-selectable, so the reader can just copy and paste
    'sphinxarg.ext',  # To include tables describing command-line arguments for executable scripts
]

# 'rst2pdf.pdfbuilder',
# 'sphinx.ext.pngmath',
# 'sphinx.ext.imgmath',

# Optional MyST syntax:
# https://myst-parser.readthedocs.io/en/latest/using/syntax-optional.html#optional-myst-syntaxes
myst_enable_extensions = [
    "amsmath",
    "dollarmath",
    "colon_fence",
    "deflist"
]
# Activate auto-generated header anchors
myst_heading_anchors = 1

# Define if myst_nb wil run the notebooks:
# https://myst-nb.readthedocs.io/en/latest/use/execute.html#
jupyter_execute_notebooks = "auto"  # 'auto', 'off', 'force', 'cache'
execution_timeout = 300  # maximum time (in seconds) each notebook cell is allowed to run

# sphinx.ext.todo settings:
todo_include_todos = True

# sphinx.ext.napoleon settings:
napoleon_google_docstring = False
napoleon_numpy_docstring = True
napoleon_include_init_with_doc = False
napoleon_include_private_with_doc = False
napoleon_include_special_with_doc = False
napoleon_use_admonition_for_examples = False
napoleon_use_admonition_for_notes = False
napoleon_use_admonition_for_references = False
napoleon_use_ivar = False
napoleon_use_keyword = True
napoleon_use_param = True
napoleon_use_rtype = False

# sphinx.ext.viewcode settings:
viewcode_follow_imported_members = True

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix of source filenames.
source_suffix = '.md'

# The encoding of source files.
# source_encoding = 'utf-8-sig'

# The master toctree document.
master_doc = 'index'

# General information about the project.
project = pyproj["project"]["name"]
copyright = pyproj["project"]["license"]["name"]
short_desc = pyproj["project"]["description"]
author = pyproj["project"]["authors"][0]['name']

# The version info for the project you're documenting, acts as replacement for
# |version| and |release|, also used in various other places throughout the
# built documents.

# The full version, including alpha/beta/rc tags.
version = release = pyproj["project"]["version"]

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
# language = None

# There are two options for replacing |today|: either, you set today to some
# non-false value, then it is used:
# today = ''
# Else, today_fmt is used as the format for a strftime call.
# today_fmt = '%B %d, %Y'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
exclude_patterns = ['build', 'dist', "**.ipynb_checkpoints"]

# The reST default role (used for this markup: `text`) to use for all
# documents.
# default_role = None

# If true, '()' will be appended to :func: etc. cross-reference text.
# add_function_parentheses = True

# If true, the current module name will be prepended to all description
# unit titles (such as .. function::).
# add_module_names = True

# If true, sectionauthor and moduleauthor directives will be shown in the
# output. They are ignored by default.
# show_authors = False

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'colorful'

# A list of ignored prefixes for module index sorting.
# modindex_common_prefix = []

# If true, keep warnings as "system message" paragraphs in the built documents.
# keep_warnings = False


# -- Intersphinx -------------------------------------------------------------
# Configuration for intersphinx: refer to the Python standard library.
intersphinx_mapping = {'python': ('https://docs.python.org/3', None),
                       'pandas': ('https://pandas.pydata.org/pandas-docs/stable/', None),
                       'numpy': ('https://numpy.org/doc/stable/', None),
                       'matplotlib': ('https://matplotlib.org/stable/', None),
                       'scikit-learn': ('https://scikit-learn.org/stable/', None),
                       'scikit-image': ('https://scikit-image.org/docs/stable/', None),
                       'scipy': ('https://docs.scipy.org/doc/scipy/', None)
                       }

# List of `intersphinx_mapping`:
# https://gist.github.com/bskinn/0e164963428d4b51017cebdb6cda5209


# -- Options for HTML output ----------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = 'press'
# html_theme = 'alabaster'

if html_theme == 'press':
    # html_sidebars = {'**': ['util/searchbox.html', 'util/sidetoc.html']}
    html_sidebars = {'**': ['util/searchbox.html', 'globaltoc.html']}
    panels_add_bootstrap_css = True
    globaltoc_maxdepth = 2
    globaltoc_includehidden = False
    globaltoc_collapse = True
    html_logo = '_static/image/timagetk_header_logo_small.png'
    html_css_files = ['css/press_custom.css']
    html_theme_options = {
        "external_links": [
            ("GitLab Inria", "https://gitlab.inria.fr/mosaic/timagetk"),
            ("Anaconda.org", "https://anaconda.org/mosaic/timagetk")
        ]
    }

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
if html_theme == 'alabaster':
    html_css_files = ['css/custom.css']
    html_theme_options = {
        'body_min_width': '700px',
        'body_max_width': '1400px',
        'font_size': '17px',
        'code_font_size': '16px',
        'font_family': 'Lato,proxima-nova,Helvetica Neue,Arial,sans-serif',
        # 'body_min_width': '45em',
        # 'body_max_width': '300em',
        'page_width': '65%',
        'logo': 'image/logo_timagetk.png',
        'fixed_sidebar': True,
        'show_powered_by': True,
        'show_relbar_bottom': True,
        'pre_bg': '#272822'
    }

# Add any paths that contain custom themes here, relative to this directory.
# html_theme_path = []

# The name for this set of Sphinx documents.  If None, it defaults to
# "<project> v<release> documentation".
html_title = "Tissue Image ToolKit Documentation"

# A shorter title for the navigation bar.  Default is the same as html_title.
# html_short_title = None

# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
# html_logo = '_static/image/logo_timagetk.png'

# The name of an image file (within the static path) to use as favicon of the
# docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
html_favicon = '_static/image/icon_timagetk.ico'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static/']

# Add any extra paths that contain custom files (such as robots.txt or
# .htaccess) here, relative to this directory. These files are copied
# directly to the root of the documentation.
# html_extra_path = []

# If not '', a 'Last updated on:' timestamp is inserted at every page bottom,
# using the given strftime format.
html_last_updated_fmt = '%b %d, %Y'

# If true, SmartyPants will be used to convert quotes and dashes to
# typographically correct entities.
# html_use_smartypants = True

# Custom sidebar templates, maps document names to template names.
# html_sidebars = {}

# Additional templates that should be rendered to pages, maps page names to
# template names.
# html_additional_pages = {}

# If false, no module index is generated.
# html_domain_indices = True

# If false, no index is generated.
# html_use_index = True

# If true, the index is split into individual pages for each letter.
# html_split_index = False

# If true, links to the reST sources are added to the pages.
# html_show_sourcelink = True

# If true, "Created using Sphinx" is shown in the HTML footer. Default is True.
html_show_sphinx = True

# If true, "(C) Copyright ..." is shown in the HTML footer. Default is True.
html_show_copyright = True

# If true, an OpenSearch description file will be output, and all pages will
# contain a <link> tag referring to it.  The value of this option must be the
# base URL from which the finished HTML is served.
# html_use_opensearch = ''

# This is the file name suffix for HTML files (*e.g.* ".xhtml").
# html_file_suffix = ``None``

# Output file base name for HTML help builder.
htmlhelp_basename = 'timagetkdoc'

# -- Options for LaTeX output ---------------------------------------------

latex_elements = {
    # The paper size ('letterpaper' or 'a4paper').
    # 'papersize': 'letterpaper',

    # The font size ('10pt', '11pt' or '12pt').
    # 'pointsize': '10pt',

    # Additional stuff for the LaTeX preamble.
    # 'preamble': '',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
    ('index', '{}.tex'.format(project), u'{} documentation'.format(project),
     author, 'manual'),
]

# The name of an image file (relative to this directory) to place at the top of
# the title page.
# latex_logo = None

# For "manual" documents, if this is true, then toplevel headings are parts,
# not chapters.
# latex_use_parts = False

# If true, show page references after internal links.
# latex_show_pagerefs = False

# If true, show URL addresses after external links.
# latex_show_urls = False

# Documents to append as an appendix to all manuals.
# latex_appendices = []

# If false, no module index is generated.
# latex_domain_indices = True


# -- Options for manual page output ---------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, author, manual section).
man_pages = [
    ('index', project, u'{} documentation'.format(project), author, 1)
]

# If true, show URL addresses after external links.
# man_show_urls = False


# -- Options for Texinfo output -------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
    ('index', project, u'{} documentation'.format(project), author, project,
     short_desc, 'Documentation'),
]

# Documents to append as an appendix to all manuals.
# texinfo_appendices = []

# If false, no module index is generated.
# texinfo_domain_indices = True

# How to display URL addresses: 'footnote', 'no', or 'inline'.
# texinfo_show_urls = 'footnote'

# If true, do not generate a @detailmenu in the "Top" node's menu.
# texinfo_no_detailmenu = False


pdf_documents = [
    ('index', u'{} documentation'.format(project),
     u'{} documentation'.format(project), author),
]

# A comma-separated list of custom stylesheets. Example:
pdf_stylesheets = ['sphinx', 'kerning', 'a4']

# A list of folders to search for stylesheets. Example:
pdf_style_path = ['.', '_styles']

# Create a compressed PDF
# Use True/False or 1/0
# Example: compressed=True
# pdf_compressed = False


# A colon-separated list of folders to search for fonts. Example:
# pdf_font_path = ['/usr/share/fonts', '/usr/share/texmf-dist/fonts/']


# Language to be used for hyphenation support
# pdf_language = "en_US"


# Mode for literal blocks wider than the frame. Can be
# overflow, shrink or truncate
# pdf_fit_mode = "shrink"


# Section level that forces a break page.
# For example: 1 means top-level sections start in a new page
# 0 means disabled
# pdf_break_level = 0


# When a section starts in a new page, force it to be 'even', 'odd',
# or just use 'any'
# pdf_breakside = 'any'


# Insert footnotes where they are defined instead of
# at the end.
# pdf_inline_footnotes = True


# verbosity level. 0 1 or 2
# pdf_verbosity = 0


# If false, no index is generated.
# pdf_use_index = True


# If false, no modindex is generated.
# pdf_use_modindex = True


# If false, no coverpage is generated.
# pdf_use_coverpage = True


# Name of the cover page template to use
# pdf_cover_template = 'sphinxcover.tmpl'


# Documents to append as an appendix to all manuals.
# pdf_appendices = []


# Enable experimental feature to split table cells. Use it
# if you get "DelayedTable too big" errors
# pdf_splittables = False


# Set the default DPI for images
# pdf_default_dpi = 72


# Enable rst2pdf extension modules (default is only vectorpdf)
# you need vectorpdf if you want to use sphinx's graphviz support
# pdf_extensions = ['vectorpdf']


# Page template name for "regular" pages
# pdf_page_template = 'cutePage'


# Show Table Of Contents at the beginning?
# pdf_use_toc = True


# How many levels deep should the table of contents be?
pdf_toc_depth = 2

# Add section number to section references
pdf_use_numbered_links = False

# Background images fitting mode
pdf_fit_background_mode = 'scale'
