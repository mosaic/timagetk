# Background in image analysis

% Update the table of contents (the left 'quick browse' panel)
```{eval-rst}
.. toctree::
  :maxdepth: 2
  :hidden:
  :caption: Concepts

  background/conventions.md
  background/file_formats.md
  background/data_structures.md
  background/advanced.md
```

The *Tissue Image ToolKit* is a Python library dedicated to **image processing of multicellular architectures**, and that means working with **multidimensional images** usually acquired using microscopy techniques.

In this explanatory section, we introduce the user to the basic concepts & conventions in *Image Analysis*.
We then get more specific to what TimageTK offers and defines:

- the adopted conventions to represent images
- the available data structures and what they represent
- the supported images format (readers & writers)
- the types of analysis available
