# Command Line Interface

```{eval-rst}
.. toctree::
  :maxdepth: 1
  :caption: CLI

  cli/multiangle_fusion.md
```


## Getting started

The CLI use a minimal data model to ensure consistency across the tools offered by timagetk.

The data model is simple and only consist in:
  - a folders and files structure
  - a single JSON file that regroups the necessary metadata and follows the folders structure.

### Semantic

  - An _experiment_ is a collection of _observations_.
  - Each _observation_ should have a `raw` _dataset_ that contains a (multichannel/multi-angle) image or a time-series.
  - An observation should thus relate to a **single biological sample**.

### File hierarchy

This is an example of the folders and files structure you should expect:

```
Experiment_A/
├── Observation_1/
│   ├── raw/                <- put your raw data there
│   ├── <dataset_name>/     <- created automatically using the CLI tools
│   ├── ...
│   └── Observation_1.json  <- define the basic metadata to perform tasks
├── Observation_2/
│   ├── raw/
│   └── Observation_2.json
:
└── ...
```

### JSON file

The minimal JSON file to create should describe the `object`, the `time` (if temporal observation) and `raw` data.

For example, with the `p58` multi-angle images time-series:

```json
{
    "object": {
        "series_name": "p58",
        "species": "Arabidopsis thaliana",
        "observation_sample": "Floral Meristem",
        "NCBI id": "3702",
        "genotype": "wt"
    },
    "time": {
        "points": [
            0,
            26,
            49
        ],
        "unit": "h"
    },
    "raw": {
        "0": [
            "090223-p58-flo-top.lsm",
            "090223-p58-flo-tilt1.lsm",
            "090223-p58-flo-tilt2.lsm"
        ],
        "1": [
            "090224-p58-flo-top.lsm",
            "090224-p58-flo-tilt1.lsm",
            "090224-p58-flo-tilt2.lsm"
        ],
        "2": [
            "090225-p58-flo-top.lsm",
            "090225-p58-flo-tilt1.lsm",
            "090225-p58-flo-tilt2.lsm"
        ]
    }
}
```

Note that all file paths are relative to the JSON file and thus do not need to be added.

Also, `'object'` description is not mandatory except for `'series_name'`.
However, it is highly recommended to improve the metadata associated to the experiments.
