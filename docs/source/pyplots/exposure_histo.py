#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
These examples show the effect of intensity rescaling on grey level images.
"""

from timagetk.algorithms.exposure import slice_contrast_stretch
from timagetk.algorithms.exposure import equalize_adapthist
from timagetk.io import imread
from timagetk.io.dataset import shared_data
from timagetk.visu.mplt import image_n_hist

# - Example input image path
img_path = shared_data('flower_confocal', 0)
# - Load the image:
sp_img = imread(img_path)
# - Get the middle z-slice:
n_zslices = sp_img.get_shape('z')
mid_zsl = int(n_zslices / 2)
# - Get the middle x-slice:
n_xslices = sp_img.get_shape('x')
mid_xsl = int(n_xslices / 2)

st_img = slice_contrast_stretch(sp_img)
eq_img = equalize_adapthist(sp_img)

list_imgs = [sp_img, st_img, eq_img]
subtitles = ["original", "z-slice contrast stretched", "z-slice adaptive equalization"]

list_z_img = [im.get_slice(mid_zsl, 'z') for im in list_imgs]
list_x_img = [im.get_slice(mid_xsl, 'x') for im in list_imgs]

from timagetk.visu.mplt import grayscale_imshow
grayscale_imshow(list_imgs, title=subtitles, threshold=[45, 60, 70])

# from timagetk.visu.mplt import plot_img_and_hist
# plot_img_and_hist(list_z_img[0])


# - Create a view of the z-slices and the associated histograms:
image_n_hist(list_z_img, img_title=subtitles, title="{} - z-slice {}/{}".format(img_path.filename, mid_zsl, n_zslices))
