#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

from timagetk.algorithms.exposure import slice_contrast_stretch
from timagetk.algorithms.exposure import equalize_adapthist
from timagetk.io import imread
from timagetk.io.dataset import shared_data
from timagetk.visu.mplt import grayscale_imshow

# input image path

img_path = shared_data('flower_confocal', 0)
# .inr format to SpatialImage
sp_img = imread(img_path)

middle_z = int(sp_img.get_shape('z') / 2)

# - Performs z-slice by z-slice contrast stretching:
st_img = slice_contrast_stretch(sp_img)
# - Performs z-slice by z-slice adaptive histogram equalization:
eq_img = equalize_adapthist(sp_img)

zsh = sp_img.get_shape('z')
mid_zsl = int(zsh / 2.)

# - Create a view of the original image and the rescaled versions:
list_imgs = [sp_img, st_img, eq_img]
subtitles = ["original", "contrast stretched", "adaptive equalization"]
grayscale_imshow(list_imgs, title=subtitles, val_range='type', cmap="gray",
                 suptitle="Examples of intensity rescaling - Projection", max_per_line=3, threshold=80)

grayscale_imshow(list_imgs, slice_id=mid_zsl, title=subtitles, val_range='type', cmap="gray",
                 suptitle="Examples of intensity rescaling - z-slice {}/{}".format(mid_zsl, zsh), max_per_line=3)
