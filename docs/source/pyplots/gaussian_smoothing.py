#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
These examples show the effect of Gaussian smoothing on grayscale images.
"""

import numpy as np

from timagetk.algorithms.linearfilter import linearfilter
from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image
from timagetk.visu.mplt import grayscale_imshow

# - Load the image:
sp_img = example_layered_sphere_wall_image()

# - Get the middle z-slice:
n_zslices = sp_img.get_shape('z')
middle_z = int(n_zslices / 2)

# Define list of increasing standard deviation to apply on images:
sigmas = np.arange(0.3, 1.1, 0.2)
n_fig = len(sigmas) + 1  # don't forget original image

# Make a list of the 2D images we will represent:
# -- Add the middle slice of our original image
f_imgs = [sp_img.get_slice(middle_z, 'z')]

# - Gaussian smoothing example with increasing standard deviation:
for n, sigma in enumerate(sigmas):
    gauss_filter_img = linearfilter(sp_img, 'smoothing',
                                    sigma=sigma, real=True)
    f_imgs.append(gauss_filter_img.get_slice(middle_z, 'z'))

# Display the figures:
fig_title = "Gaussian smoothing examples (z {}/{})"
fig_title = fig_title.format(middle_z, n_zslices)
smooth_title = ["Sigma={} voxels".format(s) for s in sigmas]

grayscale_imshow(f_imgs, title=["Original z-slice"] + smooth_title,
                 suptitle=fig_title, max_per_line=n_fig)
