#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
This plots 'contour' intensity projections from intensity images.
"""

from timagetk.algorithms.blockmatching import blockmatching
from timagetk.algorithms.trsf import apply_trsf
from timagetk.algorithms.trsf import create_trsf
from timagetk.io.dataset import shared_data
from timagetk.io import imread

 # EXAMPLE 1: Register an image after a rigid transformation:
ref_img = shared_data('flower_confocal', 0)
 # - Manual creation of a rigid trsf to generate an artificial multi-angles image:
trsf = create_trsf('random', trsf_type='rigid', angle_range=[0.2, 0.25],
                   translation_range=[0.2, 1.2])
trsf.print()
float_img = apply_trsf(ref_img, trsf)
 # - Blockmatching - RIGID registration
trsf_rig = blockmatching(float_img, ref_img, param=True)
img_rig = apply_trsf(float_img, trsf_rig, template_img=ref_img, param=True)

# - Display rigid registration effect
img2plot = [float_img, ref_img, img_rig, ref_img]
mid_z = ref_img.get_shape('z') // 2
img_titles = ["Floating view", "Reference view",
              "Registered view on reference", "Reference view"]

from timagetk.visu.mplt import grayscale_imshow

grayscale_imshow(img2plot,
                      suptitle="Rigid registration of multi-angle images",
                      title=img_titles, val_range=[0, 255], max_per_line=2)
