#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Plot *contour projections* from intensity images."""

from matplotlib import gridspec
from matplotlib.pyplot import figure
from matplotlib.pyplot import imshow
from matplotlib.pyplot import show
from matplotlib.pyplot import subplot
from matplotlib.pyplot import tight_layout
from matplotlib.pyplot import title

from timagetk.algorithms.exposure import equalize_adapthist
from timagetk.algorithms.exposure import slice_contrast_stretch
from timagetk.algorithms.resample import isometric_resampling
from timagetk.io import imread
from timagetk.io.dataset import shared_data
from timagetk.visu.projection import projection

# - Intensity threshold to apply:
int_thres = [50, 75, 100, 125]
n_thres = len(int_thres)

# - We create a grid to assemble all figures:
figure(figsize=[3.5 * 3, 3.5 * n_thres])
gs = gridspec.GridSpec(n_thres, 3)

for n, thres in enumerate(int_thres):
    # - Load the image:
    sp_img = shared_data('flower_confocal', 0)

    # - Make the grayscale image isometric:
    # ``contour_projection``methods assumes that the image is isometric
    sp_img = isometric_resampling(sp_img)

    subplot(gs[n, 0])
    # - Original gray-scale image:
    mip = projection(sp_img, method='contour', threshold=thres)
    imshow(mip.get_array(), cmap="gray", vmin=0, vmax=255, extent=[0, mip.extent[0], 0, mip.extent[1]])
    title("MIP(th={}; Original image)".format(thres))

    # - Make the grayscale image isometric:
    # ``contour_projection``methods assumes that the image is isometric
    sp_img = isometric_resampling(sp_img)

    subplot(gs[n, 1])
    # - Contrast stretched gray-scale image:
    mip = projection(slice_contrast_stretch(sp_img), method='contour', threshold=thres)
    imshow(mip.get_array(), cmap="gray", vmin=0, vmax=255, extent=[0, mip.extent[0], 0, mip.extent[1]])
    title("MIP(th={}; Contrast stretched image)".format(thres))

    # - Make the grayscale image isometric:
    # ``contour_projection``methods assumes that the image is isometric
    sp_img = isometric_resampling(sp_img)

    subplot(gs[n, 2])
    # - Original gray-scale image:
    mip = projection(equalize_adapthist(sp_img), method='contour', threshold=thres)
    imshow(mip.get_array(), cmap="gray", vmin=0, vmax=255, extent=[0, mip.extent[0], 0, mip.extent[1]])
    title("MIP(th={}; Equalized image)".format(thres))

tight_layout()
show()
