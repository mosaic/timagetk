#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
from timagetk.algorithms.linearfilter import linearfilter
from timagetk.algorithms.linearfilter import list_linear_methods
from timagetk.algorithms.resample import isometric_resampling
from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image
from timagetk.visu.mplt import grayscale_imshow

# - Load the image:
sp_img = example_layered_sphere_wall_image(voxelsize=(1.2, 0.6, 0.6))

# Make image isometric: (gaussian smoothing methods assumes image isometry)
sp_img = isometric_resampling(sp_img, value='min', interpolation="linear")

# - Get the middle z-slice:
zsh = sp_img.get_shape('z')
middle_z = int(zsh / 2)

# get list of defined linear filtering methods:
linear_methods = list_linear_methods()

# Need to define the standard deviation to apply for Gaussian smoothing
sigma = 2.

# Make a list of the 2D images we will represent:
# -- Add the middle slice of our original image
f_imgs = [sp_img.get_slice(middle_z, 'z')]

# Enumerate all known `methods` for linear filtering and apply them to the image:
for n, method in enumerate(linear_methods):
    filter_img = linearfilter(sp_img, method, sigma=sigma, real=False)
    f_imgs.append(filter_img.get_slice(middle_z, 'z'))

# Display the figures:
fig_title = "Linear filtering methods examples (z {}/{}, sigma={}vox)"
fig_title = fig_title.format(middle_z, zsh, sigma)

grayscale_imshow(f_imgs, title=["Original z-slice"] + linear_methods,
                 val_range='auto', suptitle=fig_title, max_per_line=5)
