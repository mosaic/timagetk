# Getting started

% Update the table of contents (the left 'quick browse' panel)
```{eval-rst}
.. toctree::
  :maxdepth: 2
  :hidden:
  :caption: Getting started

```

We strongly advise to **create isolated environment** using `conda` (or `venv`).
This allows to get rid of most requirement incompatibilities and access the latest versions of some libraries.

In the following section we will detail how to install TimageTK using its conda package.
To contribute to the development or simply install from the sources, look [here](developer/dev_install.md).

:::{important}
You need to install `miniconda` first if you plan to use a conda environment. Look [here](developer/conda_install.md) to learn more about conda and find instructions to install the latest version of miniconda3.
:::

:::{warning}
For Windows system, we neither test installation from sources, nor provide conda packages.
Although if you have Windows 10, you could try the *Windows Subsystem for Linux*, see the official documentation from Microsoft [here](https://docs.microsoft.com/en-us/windows/wsl/install-win10).
:::


## Installing TimageTK

This is the simplest way of installing TimageTK, but you will not be able to participate to development or build the documentation.

:::{important}
To install TimageTK you should choose **either** section A (new environment) OR B (existing environment).
If you are not sure which one to choose, follow section A.
:::


### A - In a new environment
Once you have installed `Miniconda3` you can create the conda environment (here named `titk`) and install TimageTK in one command:
```shell
conda create -n titk -c mosaic -c morpheme -c conda-forge timagetk
```
:::{note}
The dependencies are managed by conda, but you may need/want to install some optional dependencies like `ipython` or `jupyter-notebook`.
:::

:::{important}
Do not forget to activate your `titk` environment as follows:
```shell
conda activate titk
```
:::


### B - In an existing environment
To install TimageTK in an existing environment of you choosing (here named `my_env`), simply run:
```shell
conda install -n my_env -c mosaic -c morpheme -c conda-forge timagetk
```


## Optional conda packages

Optional packages:
- `ipython`: improved Python shell
- `jupyter-notebook`: required to run the notebooks

### IPython
It may be useful to install `ipython` if you work from the console:
```shell
conda install -c conda-forge ipython
```

### Jupyter
The `jupyter-notebook` package is not installed by default to limit the size of the environment.
If you wish to do so, to run an example notebook from the [gitlab](https://gitlab.inria.fr/mosaic/timagetk/-/tree/develop/notebooks), simply run:
```shell
conda install -c conda-forge jupyter-notebook
```


## Third Party Integrations

### VT

TimageTK relies a lot on the `vt` library using its Python API `vt-python`.
It is thus fully integrated and installed by default when installing `timagetk` conda package.

### MorphoNet

To interact with [MorphoNet](https://morphonet.org/) you need to install the `morphonet` package from pip:

```shell
pip install morphonet
```

### Plant-Seg & PyTorch-CUDA

The [PlantSeg](https://github.com/kreshuklab/plant-seg) library notably allows to predict the position of the cell walls to enhance the intensity image prior to segmentation.


To leverage an Nvidia GPU with CUDA capabilities, you can install `pytorch-cuda`.

To get the CUDA version from `nvidia-smi` and use it to install the corresponding `pytorch-cuda` package, you can follow these steps:

1. Extract the CUDA version from `nvidia-smi` output:

    ```bash
    CUDA_VERSION=$(nvidia-smi | grep -i cuda | awk '{print $(NF-1)}')
    ```

2. Format the version string for use with `pytorch-cuda`:

    ```bash
    PYTORCH_CUDA_VERSION=$(echo $CUDA_VERSION | cut -d'.' -f1-2)
    ```

3. Use the extracted version to install PyTorch with the matching CUDA version:

    ```bash
    conda install pytorch torchvision torchaudio "pytorch-cuda <=${PYTORCH_CUDA_VERSION}" -c pytorch -c nvidia
    ```

4. After installation, verify that PyTorch can use your GPU:

    ```python
    import torch
    print(torch.cuda.is_available())
    ```

You can combine steps 1 to 3 into a single command:

```bash
PYTORCH_CUDA_VERSION=$(nvidia-smi | grep -i cuda | awk '{print $(NF-1)}' | cut -d'.' -f1-2) && conda install pytorch torchvision torchaudio pytorch-cuda=$PYTORCH_CUDA_VERSION -c pytorch -c nvidia
```

Keep in mind that:

1. The CUDA version reported by `nvidia-smi` is the driver version, which may be different from the CUDA toolkit version installed on your system[4].

2. PyTorch may not support the exact CUDA version detected. In such cases, you might need to manually specify a supported version[2][3].

3. Ensure you have the necessary NVIDIA drivers and CUDA toolkit installed on your system before running these commands[5].

## Test installation

To test your install, simply test the import of timagetk with:
```shell
conda activate titk
python -c 'import timagetk'
```
If it all went well, you should not see anything.
On the opposite, if you get a `ModuleNotFoundError: No module named 'timagetk'` that mean the installation failed!


## For developers

To contribute to `timagetk` development, we suggest to follow a different procedure described [here](developer/dev_install.md).