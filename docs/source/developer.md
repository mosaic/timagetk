# Developer documentation

This section gather information useful to developers.

% Update the table of contents (the left 'quick browse' panel)

```{eval-rst}
.. toctree::
  :maxdepth: 1
  :caption: Developers

  developer/conda_install.md
  developer/dev_install.md
  developer/packaging.md
  developer/docker.md
  developer/documentation.md
  developer/proposals.md
  developer/vt_python.md
```
