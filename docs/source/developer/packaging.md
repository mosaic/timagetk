# Packaging

## Packaging with `conda`
To package `timagetk` and upload it to [anaconda cloud](https://anaconda.org/mosaic/timagetk) use the provided recipe in the `conda/` folder with `conda-build`.

To upload you need to log in first with `anaconda login`, we recommend that you follow the official [conda package upload instructions](https://docs.conda.io/projects/conda-build/en/latest/user-guide/tutorials/build-pkgs.html#optional-uploading-new-packages-to-anaconda-org).

:::{important}
Building conda package should be done in the `base` environment!
:::

1. Make sure you have  `conda-build` installed in your `base` environment:
    ```shell
    conda activate base
    conda install conda-build
    ```
2. Call the conda build command, here from the root `timagetk` directory:
    ```shell
    conda build conda/recipe/. -c conda-forge -c morpheme -c mosaic
    ```
3. Convert for OSX:
    ```shell
    conda convert --platform osx-64 ~/miniconda3/conda-bld/linux-64/timagetk-3* -o ~/miniconda3/conda-bld/
    ```
4. To upload to the `mosaic` channel:
    ```shell
    anaconda upload ~/miniconda3/conda-bld/linux-64/timagetk-3* --user mosaic
    anaconda upload ~/miniconda3/conda-bld/osx-64/timagetk-3* --user mosaic
    ```
5. To clean your system after a build:
    ```shell
    conda build purge
    ```

:::{note}
Search in `miniconda3/conda-bld/` to see what's left in the build directory.
:::


## Packaging with `pip`
Currently, no packaging with `pip`. 


## Resources

Package structure: https://docs.python.org/3/tutorial/modules.html#packages