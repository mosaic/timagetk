TimageTK Enhancement Proposals
===

If you want to contribute to the development of TimageTK, the TiEPs is a good place to start!

We hereafter explain the rationale behind our classes implementation and list the enhancement proposals.

% Update the table of contents (the left 'quick browse' panel)
```{eval-rst}
.. toctree::
  :maxdepth: 1

  proposals/global.md
  proposals/spatial_image.md
  proposals/labelled_image.md
  proposals/multi_channel_image.md
  proposals/tissue_image.md
  proposals/tissue_graph.md
  proposals/temporal_tissue_graph.md
  proposals/clustering.md
  proposals/clustering_analysis.md
```
