# VT package

As many algorithm used in TimageTK comes from the ``vt-python`` library, we hereafter introduce the main data structures ``vtImage`` & ``vtTransformation``.

To be as clear as possible we then described how we interfaced ``vt-python`` with our own data structures.

% Update the table of contents (the left 'quick browse' panel)
```{eval-rst}
.. toctree::
  :maxdepth: 1

  vt/vt_vtImage.md
  vt/vt_vtTransformation.md
  vt/vt_interfacing.md
```
