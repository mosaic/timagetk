# Docker containers

It is possible to create docker containers to use TimageTK.
We here introduce how to build a docker image and run it.

## Install docker
To build a docker image, you need to have docker server installed:
- CentOS official [instructions](https://docs.docker.com/install/linux/docker-ce/centos/)
- Debian official [instructions](https://docs.docker.com/install/linux/docker-ce/debian/)
- Fedora official [instructions](https://docs.docker.com/install/linux/docker-ce/fedora/)
- Ubuntu official [instructions](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

:::

:::{important}
To run a docker image on Mac or Windows, you need to have docker desktop installed:
- MacOS [instructions](https://docs.docker.com/docker-for-mac/install/)
- Windows 10 [instructions](https://docs.docker.com/docker-for-windows/install/)

:::

:::{note}
To run the `docker` command without `sudo`, we advise to add your user to the *docker*:
```shell
sudo usermod -aG docker your-user
```
:::

Remember to log out and back in for this to take effect!

:::{important}
Adding a user to the "docker" group grants them the ability to run containers which can be used to obtain root privileges on the Docker host. Refer to Docker Daemon Attack Surface for more information.
:::

Source: [Official docker documentation](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-convenience-script)


## Build docker image
To build the docker image, use the `docker build` (docker [reference](https://docs.docker.com/engine/reference/commandline/build/))command and provide the directory containing the `Dockerfile`:

```shell
docker build -t titk minimal/
docker build -t titk_jupyter jupyter/
```

The first command build the *minimal* image *i.e.* with `python3`, `vt`, `vt-python` & `timagetk` installed in a conda environment.
The second command build an image with the previous requirements and also install `jupyter` to start notebooks at image run.


## Run docker image
To run the built docker image, use the `docker run` (docker [reference](https://docs.docker.com/engine/reference/commandline/run/)) command :
```shell
docker run -it titk
```

You should now have a terminal with the `vt` environment activated, to access the installed libraries, you can use IPython:

```python
from timagetk.io import imread
from timagetk.util import shared_data
im = imread(shared_data('p58-t0-a0.lsm', "p58"))
```


## Run docker image and start a jupyter notebook
To start a jupyter notebook in the run docker image, you have two options, either using the *minimal* docker image, or the one with jupyter already installed.

1. To run the *minimal* docker image and start a jupyter notebook:
    ```shell
    docker run -it -p 8888:8888 titk /bin/bash -c "source activate vt && conda install jupyter -y && jupyter notebook --notebook-dir=~/project/timagetk/notebooks --ip='*' --port=8888 --no-browser"
    ```

2. Or you can use directly the one with jupyter installed:
    ```shell
    docker run -it -p 8888:8888 titk_jupyter
    ```
In both cases, a message will give you the URL to access the notebooks defined in `timagetk`.

:::{note}
To work, the previous command will check if the 8888 port is available on the **host machine**, if not, either kill the processes using this port or change the host port, *e.g.* to 8989: `docker run -it -p 8888:8989 titk_jupyter`.
:::


## Offline sharing of docker images
It is possible to share docker images outside the docker hub ecosystem using the `docker save` (save [reference](https://docs.docker.com/engine/reference/commandline/save/)) and `docker load` (load [reference](https://docs.docker.com/engine/reference/commandline/load/)) commands.

```shell
docker save titk > ~/titk.tar
```

Then share this image with someone or copy it to an offline server.
After doing this, on the new machine, with docker installed, you can load the container:
```shell
docker load < ~/titk.tar
docker image ls
```

```shell
docker docker create ... titk /bin/true
docker run -it titk
```


## Troubleshooting:

To run a `conda build` process from scratch (no-caching), use `--no-cache` option:
```shell
docker build --no-cache -t titk .
```

To list the conda packages installed in the built docker image, run:
```shell
docker run -it titk /bin/bash -c "source /opt/conda/bin/activate && conda activate vt && conda list"
docker run -it titk_jupyter /bin/bash -c "source /opt/conda/bin/activate && conda activate vt && conda list"
```