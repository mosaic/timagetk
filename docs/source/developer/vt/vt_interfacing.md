# Interfacing ``vtImage`` & ``SpatialImage`` data structures

## Attributes/method mapping

In the following table we detail the name mapping between the two data structures:

| Attributes       | vtImage            | SpatialImage                                   |
|------------------|--------------------|------------------------------------------------|
| Array shape      | ``self.shape()``   | ``self.shape`` or ``self.get_shape()``         |
| Pixel/voxel size | ``self.spacing()`` | ``self.voxelsize`` or ``self.get_voxelsize()`` |
| Origin           | ``None``           | ``self.origin``                                |
| Value bit depth  | ``None``           | ``self.dtype``                                 |
| Metadata         | ``None``           | ``self.metadata`` or ``self.get_metadata()``   |


## Equality
In the following section we define equality for previously defines methods or attributes:

Array
```python
vt.vtImage.copy_to_array() == SpatialImage.get_array()
```
Array shape
```python
if sp.is2d():
    vt.vtImage.shape()[:-1][::-1] == SpatialImage.shape
else:
    vt.vtImage.shape()[::-1] == SpatialImage.shape
```
Pixel/voxel size
```python
if sp.is2d():
    vt.vtImage.spacing()[:-1] == SpatialImage.voxelsize
else:
    vt.vtImage.spacing() == SpatialImage.voxelsize
```

## Tests
These equality relationships are tested in `test/test_vt_interface.py`.
