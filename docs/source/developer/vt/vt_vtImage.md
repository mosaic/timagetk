# `vtImage` - Image data structure from VT

## Initialization parameters

### First positional argument `arg0` - image

#### Description
The image filename or array to use to initialize a `vtImage` object.
:::{important}
Input arrays are considered as `[(Z,) Y, X]` sorted.
:::

#### Types
Depending on the type of input given to  `arg0` we can have different behavior:
- `None`: create an empty `vtImage` object;
- `str`: consider it as an image filename and ry to load it with `vt` reader;
- `numpy.ndarray`: array to use as image, see notes about accepted `dtype`;

#### Notes
Accept `numpy.ndarray` encoded as:
- `float32` or `float64`
- `uint8`, `uint16` or `uint32`

:::{warning}
If an array is passed to instantiate a `vt.vtImage`, it should be assigned to a variable first, since it will only copy th pointer!

Do **NOT** do this:
```python
import vt
import numpy as np
img = vt.vtImage(np.ones((5, 10, 10)))
```

Do this instead:
```python
import vt
import numpy as np
arr = np.ones((5, 10, 10))
img = vt.vtImage(arr)  # this will not result in a copy of `arr`!
```
:::


### Second positional argument `arg1` - spacing

#### Description
The physical "spacing" between pixels/voxels of the image.
Equivalent to the voxel-sizes.
:::{warning}
If performing manual initialization from array and voxelsize, array are considered as `[(Z,) Y, X]` sorted and voxelsize as `[X, Y(, Z)]`
:::

### Types
By default, not defining this parameter set it to `None`.

Two types of inputs are accepted:
-  `None`: uses default `1.0` size per axes;
-  `list(float)` of length-3;

#### Notes
:::{warning}
Can not initialize with 2D array and length-2 list of floats! Needs a length-3 list of floats (with 2D array).
:::


## Conversion methods

### Numpy array pointer method `to_array()`

#### Description
Returns the array (a pointer to its memory location).
:::{important}
(planes,) rows, columns sorted (`[(Z,) Y, X]`).
:::
:::{warning}
DESTROY the original array instance, so must be affected to new python object!
:::

#### Type
`numpy.ndarray`

#### Example
```python
import numpy as np
from vt import vtImage
arr = np.random.randint(low=0, high=255, size=[3, 5, 5], dtype="uint8")
im = vtImage(arr)
out_arr = im.to_array()
# im.copy_to_array()  # will return an Error or kill the process `SIGSEGV`!!
type(out_arr)  # but returned numpy.ndarray is accessible!
np.testing.assert_equal(arr, out_arr)
```

### Numpy array creation method `copy_to_array()`

#### Description
Returns a copy of the array.
:::{important}
(planes,) rows, columns sorted (`[(Z,) Y, X]`).
:::

#### Type
`numpy.ndarray`


## Getter methods

### Number of channels getter `dims()`

#### Description
The number of channels in the image.

#### Type
`int`


### Voxel-sizes getter method `spacing()`

#### Description
Returns the pixel or voxel size.
:::{warning}
columns, rows(, planes) sorted (`[X, Y(, Z)]`).
Invert of `copy_to_array()`.
:::

#### Type
`list`

#### Notes
:::{warning}
always of length-3, even with 2D array!
:::


### Shape getter method `shape()`

#### Description
Returns the shape of the array.
:::{warning}
columns, rows(, planes) sorted (`[X, Y(, Z)]`).
Invert of `copy_to_array()` and of `shape` attribute of a NumPy array.
:::

#### Type
`list`

#### Notes


## Setter methods

### Channel number setter `setDims()`

#### Description
Set the number of channels in the image.

#### Type
`int`

#### Notes
:::{caution}
do not comply to [PEP8 naming convention](https://www.python.org/dev/peps/pep-0008/#function-and-variable-names)_ for methods, should be `set_dims`.
:::


### Voxel-sizes setter method `setSpacing()`

#### Description
Set the pixel or voxel size.
:::{warning}
columns, rows(, planes) sorted (`[X, Y(, Z)]`).
Invert of `copy_to_array()`
:::

#### Type
`list`

#### Notes
:::{warning}
always of length-3, even with 2D array!
:::
:::{caution}
do not comply to [PEP8 naming convention](https://www.python.org/dev/peps/pep-0008/#function-and-variable-names)_ for methods, should be `set_spacing`.
:::


### Shape setter method `setShape()`

#### Description
Set the shape of the array.
:::{warning}
columns, rows(, planes) sorted (`[X, Y(, Z)]`).
Invert of `copy_to_array()` and of `shape` attribute of a NumPy array.
:::

#### Type
`list`

#### Notes
:::{caution}
do not comply to [PEP8 naming convention](https://www.python.org/dev/peps/pep-0008/#function-and-variable-names) for methods, should be `set_shape`.
:::


## Other methods


### Print object mathod `print()`

#### Description
Print information about the object.

#### Type
`str`

#### Notes
Example of output:
```
image information:
 - dimensions [x y z] = 50 40 30
 - voxel size [x y z] = 0.630000 0.620000 0.310000
 - image type is: UCHAR
 - image geometry is: _BAL_HOMOTHETY_GEOMETRY_
 - conversion matrix voxel to real:
  0.629999995231628    0.000000000000000    0.000000000000000    0.000000000000000
  0.000000000000000    0.620000004768372    0.000000000000000    0.000000000000000
  0.000000000000000    0.000000000000000    0.310000002384186    0.000000000000000
  0.000000000000000    0.000000000000000    0.000000000000000    1.000000000000000
 - conversion matrix real to voxel:
  1.587301599315625    0.000000000000000    0.000000000000000    0.000000000000000
  0.000000000000000    1.612903213401739    0.000000000000000    0.000000000000000
  0.000000000000000    0.000000000000000    3.225806426803478    0.000000000000000
  0.000000000000000    0.000000000000000    0.000000000000000    1.000000000000000
 - qform_code: 0
 - sform_code: 0
 - min = 0 , max = 254 , mean = 126.500917
```

## Other remarks
No management of metadata.
No `origin` attribute as found in some data structure.


## Python implementation proposal
```python
import vt

class vtImage(vt.vtImage):
    """Image object for the `vt` library. """

    def __init__(image=None, voxelsize=None, **kwargs):
        """
        Parameters
        -----##         image : str or numpy.ndarray, optional
            If a `str` is given, it should be a valid image file to open.
            If a `numpy.ndarray`, it is considered as (Z)YX ordered.
            If `None`, an empty object is built.
        voxelsize : list(float), optional
            Specify the pixel size (if 2D) or voxel size (if 3D), only used if
            `image` is also specified.
            If specified, should be a (planes,) rows & columns or (Z)YX sorted
            list of voxelsize values, else it will be set to [(1.,) 1., 1.].

        Notes
        ##         Accepted type of numpy array are: {"uint8", "uint16", "uint32", "float32", "float64"}.

        Raises
        -##         TypeError
            If incompatible constructor arguments.

        """
        pass

    def to_array(self):
        """ Returns the image array as a `numpy.ndarray` and destroy the object.

        See Also
        ---##         self.to_array(): returns the image array as a (Z)YX sorted `numpy.ndarray`.

        Returns
        --##         numpy.ndarray
            the image array

        """
        pass

    def copy_to_array(self):
        """ Returns the image array as a `numpy.ndarray`.

        See Also
        ---##         self.to_array(): returns the image array as a (Z)YX sorted `numpy.ndarray`.

        Returns
        --##         numpy.ndarray
            the image array

        """
        pass

    def write(self, filename):
        """ Save the image to the disk.

        Parameters
        -----##         filename : str
            Name of the file to save.

        """
        pass

    def shape(self):
        """ Returns the image shape.

        Returns
        --##         list(int)
            (Z)YX sorted shape values.

        Notes
        ##         Length of returned list should match image dimensions.

        """
        pass

    def spacing(self):
        """ Returns the image voxelsize.

        Returns
        --##         list(float)
            (Z)YX sorted voxelsize values.

        Notes
        ##         Length of returned list should match image dimensions.

        """
        pass

    def set_shape(self, shape):
        """ Set the image shape.

        Parameters
        -----##         shape : list(int)
            A (Z)YX list of image shape.

        Notes
        ##         Note sure what happens when you use this on an existing image!
        Should we do a reslicing like arr[0:z_sh, 0:y_sh, 0:x_sh] ?

        """
        pass

    def set_spacing(self, spacing):
        """ Set the image voxelsize.

        Parameters
        -----##         spacing : list(int)
            A (Z)YX list of voxelsize.

        Notes
        ##         This changes the image physical size!

        """
        pass
```