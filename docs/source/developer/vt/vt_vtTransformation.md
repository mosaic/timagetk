# ``vtTransformation`` - Transformation data structure from VT


## Initialization parameters (@25/03/2020)

### First positional argument ``arg0``

#### Description
The image to use to initialize a ``vtTransformation``.
:::{important}
Input arrays are considered as `[(Z,) Y, X]` sorted.
:::

#### Types
Depending on the type of input given to  ``arg0`` we can have different behavior:
 -  ``None``: create an empty ``vtTransformation`` object;
 -  ``str``: consider it as a transformation filename and try to load it with ``vt`` reader;
 -  ``numpy.ndarray``: array to use as transformation, see notes about accepted ``dtype``.

#### Notes
Accept ``numpy.ndarray`` encoded as ``float32`` or ``float64``.
