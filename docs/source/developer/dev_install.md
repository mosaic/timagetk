# Developer Installation Guide

This guide provides steps to set up a development environment for working with `timagetk` and its dependencies. Follow the **Short Version** for quick setup or the **Detailed Version** for step-by-step instructions.

---

## Short Version

From the root folder of the `timagetk` repository, with `visu_core` cloned into the parent directory:

```shell
# Step 1: Create a 'timagetk_dev' environment with development dependencies using Anaconda:
conda env create -n timagetk_dev --file conda/env/timagetk_dev.yaml
conda activate timagetk_dev

# Step 2: Install PlantSeg & dependencies 
conda install -c gnomon -c pytorch -c nvidia plantseg pytorch-gpu 'pytorch-cuda<=12.2'

# Step 3: Install 'visu_core' from source:
python -m pip install -e ../visu_core/.

# Step 4: Install 'timagetk' from source with all dependencies from PyPI:
python -m pip install -e '.[codecs,doc,nb,test]'
```

---

## Detailed Version

### 1. Clone the Repositories

Both `timagetk` and some related projects need to be cloned from their respective Git repositories. From the directory where you want to store the sources (e.g., `~/Projects/`), run:

```shell
# Clone the 'timagetk' repository (use the 'develop' branch):
git clone https://gitlab.inria.fr/mosaic/timagetk.git --branch develop

# Clone the 'visu_core' repository (use the 'develop' branch):
git clone https://gitlab.inria.fr/mosaic/work-in-progress/visu_core.git --branch develop
```

### 2. Set up the Conda Environment

Navigate to the root folder of the cloned `timagetk` repository and create a Conda environment:

```shell
cd timagetk
conda env create -n timagetk_dev --file conda/env/timagetk_dev.yaml
```

This will create an environment named `timagetk_dev` and install all required Conda dependencies.

> **Note:** If the required `morpheme` packages (`vt` and `vt-python`) are not installed automatically, you can manually install them using:
> ```shell
> conda install vt vt-python -c morpheme -c conda-forge
> ```

### 3. Install the Source Code

After setting up the environment, install the source code for both `visu_core` and `timagetk`.

#### Install `visu_core`

Activate the `timagetk_dev` environment, then install `visu_core` from the source by running:

```shell
conda activate timagetk_dev
python -m pip install -e ../visu_core/.
```

The `-e` option enables "editable" mode, allowing you to make changes to the source code without needing to reinstall it after each change.

#### Install `timagetk`

Next, install `timagetk` along with its additional dependencies (e.g., for testing and documentation):

```shell
python -m pip install -e '.[all]'
```

> **Note:** If dependencies for specific features like documentation or testing are not installed, you can manually add them:
> - For documentation: `python -m pip install -e .[doc]`
> - For testing: `python -m pip install -e .[test]`

### 4. Install Third-party Dependencies (`PlantSeg`)

If you need to use the integrated features from the `PlantSeg` package, install it based on your system configuration:

- To use **PlantSeg with a CUDA GPU**, run:
  ```shell
  conda install -c gnomon -c conda-forge -c pytorch -c nvidia plantseg pytorch-cuda 'cuda-version<=12.2' nifty
  ```

- To use **PlantSeg for CPU-only** installation, run:
  ```shell
  conda install -c gnomon -c conda-forge -c pytorch plantseg cpuonly nifty
  ```

---

## Summary

By following the steps above, you will have set up a fully functional developer environment for `timagetk` and its dependencies. This environment supports source code modification, testing, and documentation generation. Use the **Short Version** for quick setup, or delve into the **Detailed Version** for a comprehensive process.

If you encounter any issues, ensure all dependencies are installed correctly and consult the repository documentation for troubleshooting.