# Global proposals

## Algorithms module

The ``algorithms`` module that are image algorithms should work on 2D or 3D ``SpatialImage`` and decorated to work with ``MultiChannelImage``.

To get the documentation of the wrapped functions, we use the ``@wraps`` decorator from the ``functool`` module.

Reference: https://docs.python.org/3/library/functools.html#functools.wraps