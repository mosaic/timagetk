TemporalTissueGraph implementation proposal
===

## Biological observations
The typical object is a discrete **time-series of multicellular tissue** observations (from microscopy).
Cell-based segmentation, manual or algorithmic, is required to access the spatial information hidden in the intensity image.
Cell lineage, is required to access the temporal information.


## Goal
The goal of this data structure is to provide a flexible graph object dedicated to the exploration of discrete dynamic multicellular tissue properties.

- It leverages the `TissueGraph` class.
- **Biological semantic** should be used when possible.


## Structure
A **lineage graph** define the temporal relationships between the -spatial- `TissueGraph` objects.
A dictionary of `TissueGraph` gather the discrete observations of multicellular tissues.

The lineage graph is a `networkx.DiGraph()`.

The time intervals between each observation is required to perform temporal differentiation of spatial properties!


## Elements ids

The topological element ids are now also indexed by time.

### Primal graph
- **temporal cell ids** as nodes: `tcid=(t, cid)` an integer `t` for *time* and an integer `cid` for the *cell id*, *e.g.* `(0, 5)` for cell `5` at time `0`
- **temporal cell-wall ids** as edges: 
    1. `twid=(t, wid)=(t, (cid_i, cid_j))` an integer `t` for *time* and a len-2 tuple of integers `wid=(cid_i, cid_j)` for the *cell-wall id*, *e.g.* `(0, (5, 6))` for cell-wall `(5, 6)` at time `0`
    2. `twid=(tcid_i, tcid_j)=((t, cid_i), (t, cid_j))` two len-2 tuple of integers `tcid_i` and `tcid_j` for the two *temporal cell ids* defining the cell-wall, *e.g.* `((0, 5), (0, 6))` for cells `5` and `6` at time `0`.

### Dual graph
- **temporal cell-vertex ids** as nodes: 
    1. `tvid=(t, vid)=(t, (cid_i, cid_j, cid_k, cid_l))` an integer `t` for *time* and a len-4 tuple of integers `vid=(cid_i, cid_j, cid_k, cid_l)` for the *cell-vertex id*, *e.g.* `(0, (5, 6, 7, 8))` for cell-vertex `(5, 6, 7, 8)` at time `0`
    2. `tvid=(tcid_i, tcid_j, tcid_k, tcid_l)=((t, cid_i), (t, cid_j), (t, cid_k), (t, cid_l))` four len-2 tuple of integers `tcid_i`, `tcid_j`, `tcid_k` and `tcid_l` for the four *temporal cell ids* defining the cell-vertex, *e.g.* `((0, 5), (0, 6), (0, 7), (0, 8))` for cells `5`, `6`, `7` and `8` at time `0`.
- **temporal cell-edge ids** as edges: 
    1. `teid=(t, eid)=(t, (vid_1, vid_2))=(t, ((cid_i1, cid_j1, cid_k1, cid_l1), (cid_i2, cid_j2, cid_k2, cid_l2)))` an integer `t` for *time* and two len-4 tuple of integers `vid_1=(cid_i1, cid_j1, cid_k1, cid_l1)` and `vid_2=(cid_i2, cid_j2, cid_k2, cid_l2)` for the two *cell-vertex id*, *e.g.* `(0, ((5, 6, 7, 8), (5, 6, 7, 9)))` for cell-vertices `(5, 6, 7, 8)` and `(5, 6, 7, 9)` at time `0`
    2. `teid=(tvid_1, tvid_2)=((tcid_i1, tcid_j1, tcid_k1, tcid_l1), (tcid_i2, tcid_j2, tcid_k2, tcid_l2))=(((t, cid_i1), (t, cid_j1), (t, cid_k1), (t, cid_l1)), ((t, cid_i2), (t, cid_j2), (t, cid_k2), (t, cid_l2)))` two len-4 tuple of integers `tvid_1` and `tvid_2` for the two *temporal vertex ids* defining the cell-edge, *e.g.* `(((0, 5), (0, 6), (0, 7), (0, 8)), ((0, 5), (0, 6), (0, 7), (0, 9)))` for the two cell-vertices `(5, 6, 7, 8)` and `(5, 6, 7, 9)` at time `0`.


## Methods

### Creation methods
The first step is to populate the data structure with the spatial and temporal data.

#### Add topological tissue graph
The following methods allow to add topological tissue graph: 
```python
ttg = TemporalTissueGraph()
ttg.add_tissue_graph(tissue, elapsed_time, unit='h')
```

#### Add cell lineage
The following methods allow to add cell lineages as temporal relationships between the tissue graphs:
```python
ttg.add_lineage(ancestor_tp, lineage)
```

### Exploration methods
We want to be able to "explore" the data structure

#### Temporal exploration
```python
ttg.child(tp, cell_id)
ttg.parent(tp, cell_id)
ttg.siblings(tp, cell_id)
```

#### Spatial exploration

:::{warning}
Should we create methods `ttg.neighbors(cid, tp)` or access from `tissueGraph` with `ttg._tissue_graph[tp].neighbors(cid)` ?
:::


### Properties methods

#### Add temporal properties