# Introducing Conda

Conda is a tool to install and manage different software libraries, possibly at different versions, into independent system configurations called **environments**.
Each environment has a name and stores its own version of each library (distinct from the one of your general system) so that you can make installations that would be incompatible with the rest of your system without impacting its functioning and breaking everything!

Miniconda will create a new directory for each environment where it will download libraries, binaries and packages.
The command `source activate environment_name` modifies your environment variables (e.g. `PATH`, `PYTHONPATH`) so that your system points onto this directory.
To recover your general system installation, you simply need to run the command `source deactivate`.


## Installing Miniconda3

:::{warning}
This use the latest `Miniconda3` distribution to use Python3.
:::

Download the **latest** miniconda installer from the (official website)[https://repo.continuum.io]:

  - LINUX: https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
  - MAC: https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh


### Linux instructions

Download manually or use `wget` to perform this download from a shell:
```shell
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh
```

Install miniconda by running the installer in a shell prompt (where you downloaded the installer):
```shell
bash Miniconda3-latest-Linux-x86_64.sh
```

Once installed, you can remove the installer:
```shell
rm Miniconda3-latest-Linux-x86_64.sh
```

### MacOSX instructions

Download manually or use `wget` to perform this download from a shell::
```shell
wget https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
```

Install miniconda by running the installer in a shell prompt (where you downloaded the installer)::
```shell
bash Miniconda3-latest-MacOSX-x86_64.sh
```

Once installed, you can remove the installer::
```shell
rm Miniconda3-latest-MacOSX-x86_64.sh
```