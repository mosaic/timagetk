# Credits

% Update the table of contents (the left 'quick browse' panel)

```{eval-rst}
.. toctree::
  :maxdepth: 2
  :hidden:
  :caption: Credits

```

## Funding

Those who have contributed to TimageTK have received support throughout the years from a variety of sources.

## Research Groups

TimageTK acknowledges support from the following:

* Inria team [Mosaic](https://team.inria.fr/mosaic/), RDP-ENS Lyon, UMR5667
* Inria team [Morpheme](http://www-sop.inria.fr/morpheme/), Sophia Antipolis
* [Hormonal Signalling and Development](http://www.ens-lyon.fr/RDP/spip.php?rubrique20) team, RDP-ENS Lyon, UMR5667

## Former Research Groups

TimageTK acknowledges support former from the following former partners:

* Inria-Cirad-Inra [Virtual Plants](https://team.inria.fr/virtualplants/)
* Inria Project Lab [Morphogenetics](https://team.inria.fr/morphogenetics/)

## Development lead

The development is currently done by Jonathan Legrand, under the supervision of Christophe Godin, Grégoire Malandain and
Teva Vernoux.

## Contributors

This section aims to provide a list of people that have contributed to TimageTK. Creating an exhaustive and detailed
list of contributors is difficult, and the list may be incomplete. Contributors include bug reporters, testers, end
users and academic advisors.

### Algorithm development

* Christophe Godin
* Gaël Michelin
* Grégoire Malandain
* Jonathan Legrand
* Sophie Ribes

### Documentation

* Jonathan Legrand

### Data structures

* Christophe Godin
* Christophe Pradal
* Frédérique Boudon
* Grégoire Malandain
* Jonathan Legrand
* Simon Artzet
* Sophie Ribes

### Examples

* Christophe Godin
* Christophe Pradal
* Frédérique Boudon
* Grégoire Malandain
* Guillaume Cerutti
* Hadrien Oliveri
* Jonathan Legrand
* Simon Artzet
* Sophie Ribes

### Python wrapping

* Grégoire Malandain
* Jonathan Legrand.

### API definition

* Christophe Godin
* Christophe Pradal
* Grégoire Malandain
* Guillaume Baty
* Jonathan Legrand
* Sophie Ribes

### Tests

* Annamaria Kiss-Gabor
* Gaël Michelin
* Grégoire Malandain
* Guillaume Baty
* Jonathan Legrand
* Sophie Ribes

### Packaging

* Guillaume Cerutti
* Jonathan Legrand
