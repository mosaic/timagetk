# Contribution guidelines

% Update the table of contents (the left 'quick browse' panel)

```{eval-rst}
.. toctree::
  :maxdepth: 2
  :hidden:
  :caption: Contributing

```

If you are interested in **contributing to TimageTK development**, consider installing the sources by following the  [installation](developer/dev_install.md) procedure.

Have a look at the [Python Developer's Guide](https://devguide.python.org/), it never hurt to use common good practices.


## Package structure

We hereafter summarize the structure of the package:

```
timagetk/
    ├── conda/
    │   ├── env/
    │   └── recipe/
    ├── data/
    │   ├── p58/
    │   └── sphere/
    ├── docs/
    │   └── source/
    ├── docker/
    ├── examples/
    ├── notebooks/
    ├── src/
    │   ├── timagetk/
    │   │   ├── algorithms/
    │   │   ├── bin/
    │   │   ├── components/
    │   │   ├── features/
    │   │   ├── graphs/
    │   │   ├── gui/
    │   │   ├── io/
    │   │   ├── synthetic_data/
    │   │   ├── tasks/
    │   │   ├── third_party/
    │   │   └── visu/
    └── test/
```

## Folders details

We hereafter details the meaning and contents of the submodules.

### `conda` folder

It contains the conda environments and packaging recipes respectively in `env` and `recipe`.

### `data` folder

It contains the shared data which are mostly images. They are used in docstring and documentation examples.

[//]: # (They should be distributed with the package &#40;pip or conda&#41; and thus may need to be added to the `MANIFEST.in` file.)

### `docs` folder

It contains the sources used to build the online documentation with [Sphinx](https://www.sphinx-doc.org/) and [MyST](https://myst-parser.readthedocs.io/).

The `source` folder contains:

* several Markdown (`.md`) files with the documentation;
* a `conf.py` file used by Sphinx to generate the documentation;
* a `pyplot` folder containing Python scripts used as code examples or to generate plots integrated in the
* documentation;
* a `_static` folder containing static images used in the documentation;
* a `_template` folder containing the layout and CSS file used to render the documentation.

A `build` folder is obtained after building it with Sphinx.
Open the `build/index.html` file with you favorite web browser to view the generated documentation.

### `docker` folder

It contains the recipes to create docker image with a working installation of our Python module.

### `examples` folder

It contains some usage examples of our Python module in the form of Python scripts.

### `notebooks` folder

It contains some usage examples of our Python module in the form of jupyter notebooks.

### `src` folder

It contains the sources of our Python module `timagetk`.

### `test` folder

It contains the unitary test scripts.

## Module structure

For more details about each submodule, please refer to the reference guide in the online documentation!

### `algorithms` submodule

Contains the low-level algorithms packaged by our Python module.

### `bin` submodule

Contains the executable scripts packaged by our Python module.

### `components` submodule

Contains the data structures packaged by our Python module.

### `features` submodule

Contains the cell features quantification algorithms packaged by our Python module.

### `graphs` submodule

Contains the graph data structure and related algorithms packaged by our Python module.

### `io` submodule

Contains the Input/Output (read/write) methods packaged by our Python module.

### `synthetic_data` submodule

Contains the methods to generate synthetic data packaged by our Python module.

### `tasks` submodule

Contains the high-level tasks packaged by our Python module.

### `third_party` submodule

It contains the third-party code we include and ship with our Python module.

### `visu` submodule

Contains the visualisation methods packaged by our Python module.

## Workflow

### General workflow

Edit the Python code, but do not forget to write documentation and tests before submitting a merge request.

You can also contribute by **raising issues** on the dedicated area of the Inria GitLab repository [here](https://gitlab.inria.fr/mosaic/timagetk/issues).

### Git workflow

:::{warning}
**NEVER EVER** work on the `main` branch, always create a new a branch!
Using `gitflow` can be helpful if you are not a git expert.
:::

1. Clone the official repository:
    ```shell
    git clone git@gitlab.inria.fr:mosaic/timagetk.git
    ```
2. Create a development branch:
    1. Get the latest version of `origin/main`:
         ```shell
         git checkout main  # go to your `main` branch (local)
         git pull main  # update it with `origin/main` (remote)
         ```
    2. Create your new branch (choose an explicit name):
         ```shell
         git checkout -b wip_my_branch  # create a branch `wip_my_branch` (local)
         git push --set-origin origin wip_my_branch  # attach branch `wip_my_branch` to origin (remote)
         ```
3. Work on your modifications:
    1. To **add a new file** ``mynewfile.py`` to git version control system::
         ```shell
         git add mynewfile.py
         ```
    2. To **commit changes** to git repository:
         ```shell
         git commit
         ```
    3. To **push changes** to git repository:
         ```shell
         git push origin wip_my_branch  # push your modifications to `origin/wip_my_branch` (remote)
         ```
4. Prepare your work for merging:
    1. Get the latest version of `origin/main`:
         ```shell
         git checkout main  # go to your `main` branch (local)
         git pull origin main  # update it with `origin/main` (remote)
         ```
    2. It is safer to create a new branch for rebase:
         ```shell
         git checkout wip_my_branch  # go to your development branch `wip_my_branch` (local)
         git checkout -b my_branch  # create and checkout new branch `my_branch` (local)
         git rebase main  # rebase `main` branch with `my_branch` (local)
         ```
    3. If main has diverged during your work, fix conflicts (with an IDE), then say to git that conflicts are resolved:
         ```shell
         git add file1
         git rebase --continue  # to finish rebase
         ```
5. Merge branch:
    ```shell
    git checkout main  # go to your `main` branch (local)
    git pull origin main  # update it with `origin/main` (remote)
    git merge --no-ff my_branch  # use ‘--no-ff’ (no fast forward) to create a merge commit
    git push origin main  # Then push it to `origin/main` (remote)
    ```
6. Finalization, check if you find all yours commits on `origin/main`, if yes:
    ```shell
    git branch --delete my_branch  # delete rebasing branch `my_branch` (local)
    git push origin :my_branch  # delete rebasing branch `origin/my_branch` (remote)
    ```

### Tests

#### Dependencies

To test the code and get test coverage reports, we make use of [nose2](https://docs.nose2.io/) & [coverage](http://coverage.readthedocs.org/).

:::{warning}
**CHOOSE between A, B or C!**
:::

##### A. Automatic installation with `poetry`

To install them, install the development dependencies with `poetry`.

```shell
poetry install
```

##### B. Manual installation with `conda`

You can also manually install them with `conda` as follows:

```shell
conda install -c conda-forge nose2[coverage_plugin] coverage[toml]
```

:::{important} Don't forget to do this in the right conda environment!
:::

##### C. Manual installation with `pip`

You can also manually install them with `pip` as follows:

```shell
python -m pip install nose2[coverage_plugin] coverage[toml]
```

:::{important} Don't forget to do this in the right virtual environment if you are using one!
:::

#### Running the tests:

Then, from the root folder, you can test `my_test.py` with:
```shell
nose2  tests/my_test.py
```

Or run all test and see coverage using the `nose2.cfg` configuration file and the `pyproject.toml` file for `coverage` configuration:
```shell
nose2 --config tests/unittests.cfg
```

## Coding guidelines

Here is a non-exhaustive list of guidelines you should follow when contributing to TimageTK:

* Respect PEP8 conventions.
* Document the code with Numpy styled docstrings (in Pycharm go to `File | Settings` then `Tools | Python Integrated Tools | Docstrings | Docstring format`).
* Add examples when possible.
* Use spaces instead of tabs (configure your IDE).
* Only one import per line (in Pycharm go to `File | Settings` then `Editor | Code Style | Python` then `Imports` tab
* and finally select `Always split imports` button option). This makes for larger headers, but it is easier to maintain
* and refactor!
* Add shebang to first line of all Python file: `#!/usr/bin/env python`
* Add encoding declaration to all Python file headers: `# -*- coding: utf-8 -*-`
* Add a "copyright" header.

## Tips

### Text formatting

#### Capitalize first letter of sentence in documentation

Use in `search/replace`, with regexp activated:

* search expression: `(^\s{4,12})(\w+)(\f|\-)(?!:)`
* replace with: `$1\u$2$3`

Notes if you are using pycharm:

* do this in the `Find` window to access the whole list of pattern occurrences.
* activate the search filter in `Comments` only.
* refer to this documentation [section](https://www.jetbrains.com/help/phpstorm/regular-expression-syntax-reference.html#regex-syntax-reference) for regexp syntax.


## Other tools

### Development

We advise to use a decent IDE to develop code.
Our preference goes to the Community Edition of [Pycharm](https://www.jetbrains.com/pycharm/).
Learn to use it, and you may need no other tool listed below since it integrate a lot of functionalities.

### Merge/rebase conflicts

To solve merge or rebase conflicts, we advise to either use:

1. the dedicated PyCharm `Git` tool
2. the GitLab web IDE
3. to use a visual diff and merge tool such as [meld](http://meldmerge.org/).

