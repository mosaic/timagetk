# API Reference

This is the reference guide for TimageTK.

% Update the table of contents (the left 'quick browse' panel)
```{eval-rst}
.. toctree::
  :hidden:
  :maxdepth: 1
  :caption: API Reference

  reference/autodoc_algorithms.md
  reference/autodoc_components.md
  reference/autodoc_io.md
  reference/autodoc_tasks.md
  reference/autodoc_synthetic_data.md
  reference/autodoc_cli.md
  reference/autodoc_visu.md
  reference/autodoc_vt.md
```

::::{panels}
:container: +full-width text-center
:column: col-md-6 col-lg-4 col-xl-3 px-2 py-2
:card: shadow

:::{link-button} reference/autodoc_algorithms.html
:classes: stretched-link btn-link
:text: Algorithms
:::
^^^
Low-level algorithms.
+++
Module `algorithms`
---

:::{link-button} reference/autodoc_cli.html
:classes: stretched-link btn-link
:text: CLI
:::
^^^
Scripts & CLI for batch processing.
---

:::{link-button} reference/autodoc_components.html
:classes: stretched-link btn-link
:text: Components
:::
^^^
Data structures.
+++
Module `components`
---

:::{link-button} reference/autodoc_io.html
:classes: stretched-link btn-link
:text: IO
:::
^^^
Readers & writers.
+++
Module `io`
---

:::{link-button} reference/autodoc_tasks.html
:classes: stretched-link btn-link
:text: Tasks
:::
^^^
High level tasks.
+++
Module `tasks`
---

:::{link-button} reference/autodoc_synthetic_data.html
:classes: stretched-link btn-link
:text: Synthetic data
:::
^^^
Generate synthetic images with code.
+++
Module `synthetic_data`
---

:::{link-button} reference/autodoc_visu.html
:classes: stretched-link btn-link
:text: Visualisation
:::
^^^
Visualisation tools.
+++
Module `visu`
---

:::{link-button} reference/autodoc_vt.html
:classes: stretched-link btn-link
:text: Third party
:::
^^^
Wrappers and converters for other libraries and tools.
+++
Module `third_party`

::::
