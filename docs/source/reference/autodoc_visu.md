# Visualization

% Update the table of contents (the left 'quick browse' panel)
```{eval-rst}
.. toctree::
   :maxdepth: 2
```


## Matplotlib - 2D plotting

You will find method to render 2D matplotlib figures with the `mplt` module:
```{eval-rst}
.. automodule:: timagetk.visu.mplt
   :members:
```


## Stack browsing - Matplotlib interactive visualization

You can access simple interactive stack browsers with the `stack` module:

```{eval-rst}
.. automodule:: timagetk.visu.stack
   :members:
```


## Stack browsing - Creating animations

You can create GIFs and `.mp4` videos (x264 codec) with the `animations` module:

```{eval-rst}
.. automodule:: timagetk.visu.animations
   :members:
```


## Nuclei - Quantitative signal

You can visualize quantitative information about nuclei signal with the functions of the `nuclei` module:

```{eval-rst}
.. automodule:: timagetk.visu.nuclei
   :members:
```


## Time series & time lapse
```{eval-rst}
.. automodule:: timagetk.visu.temporal_projection
   :members:
```


## Matplotlib - Intensity profiles

You will find method to render intensity profile figures with the `profiles` module:
```{eval-rst}
.. automodule:: timagetk.visu.profiles
   :members:
```
