# Algorithms

% Update the table of contents (the left 'quick browse' panel)
```{eval-rst}
.. toctree::
   :hidden:
   :maxdepth: 2
   
   algorithms/autodoc_averaging.md
   algorithms/autodoc_blockmatching.md
   algorithms/autodoc_connexe.md
   algorithms/autodoc_exposure.md
   algorithms/autodoc_linearfilter.md
   algorithms/autodoc_morphology.md
   algorithms/autodoc_peak_detection.md
   algorithms/autodoc_pointmatching.md
   algorithms/autodoc_quaternion.md
   algorithms/autodoc_reconstruction.md
   algorithms/autodoc_regionalext.md
   algorithms/autodoc_resample.md
   algorithms/autodoc_signal_quantification.md
   algorithms/autodoc_template.md
   algorithms/autodoc_topological_elements.md
   algorithms/autodoc_trsf.md
   algorithms/autodoc_watershed.md
```

The `algorithm` module offers an array of tools for image enhancement, transformation, and feature extraction.
From mathematical morphology to filtering algorithms, image registration and peak detection, these methods
allows to refine image data and assist in critical processing workflows for various analysis needs.


::::{panels}
:container: +full-width text-center
:column: col-lg-6 col-xl-3 px-2 py-2
:card: shadow

:::{link-button} algorithms/autodoc_averaging.html
:classes: stretched-link btn-link
:text: Averaging
:::
^^^
Intensity image averaging algorithms.
---

:::{link-button} algorithms/autodoc_blockmatching.html
:classes: stretched-link btn-link
:text: Block-matching
:::
^^^
Intensity image registration algorithm.
---

:::{link-button} algorithms/autodoc_connexe.html
:classes: stretched-link btn-link
:text: Connexe
:::
^^^
Connected components detection algorithm.
---

:::{link-button} algorithms/autodoc_exposure.html
:classes: stretched-link btn-link
:text: Exposure
:::
^^^
Intensity rescaling and edition algorithms.
---

:::{link-button} algorithms/autodoc_linearfilter.html
:classes: stretched-link btn-link
:text: Filtering
:::
^^^
Image filtering operators (Gaussian, ...).
---

:::{link-button} algorithms/autodoc_morphology.html
:classes: stretched-link btn-link
:text: Morphology
:::
^^^
Morphological operators.
---

:::{link-button} algorithms/autodoc_peak_detection.html
:classes: stretched-link btn-link
:text: Peak detection
:::
^^^
Nuclei detection and signal quantification.
---

:::{link-button} algorithms/autodoc_pointmatching.html
:classes: stretched-link btn-link
:text: Point-matching
:::
^^^
Transformation estimation from landmarks.
---

:::{link-button} algorithms/autodoc_quaternion.html
:classes: stretched-link btn-link
:text: Quaternion
:::
^^^
Linear transformation creation with quaternions.
---

:::{link-button} algorithms/autodoc_reconstruction.html
:classes: stretched-link btn-link
:text: Reconstruction
:::
^^^
Surface reconstruction & intensity projections.
---

:::{link-button} algorithms/autodoc_regionalext.html
:classes: stretched-link btn-link
:text: Regional extrema
:::
^^^
Local extrema detection algorithms.
---

:::{link-button} algorithms/autodoc_resample.html
:classes: stretched-link btn-link
:text: Resample
:::
^^^
Image resampling algorithms.
---

:::{link-button} algorithms/autodoc_signal_quantification.html
:classes: stretched-link btn-link
:text: Signal quantification
:::
^^^
Fluorescent signal quantification.
---

:::{link-button} algorithms/autodoc_template.html
:classes: stretched-link btn-link
:text: Template
:::
^^^
Template image creation.
---

:::{link-button} algorithms/autodoc_topological_elements.html
:classes: stretched-link btn-link
:text: Topological elements
:::
^^^
Topological elements detection algorithm.
---

:::{link-button} algorithms/autodoc_trsf.html
:classes: stretched-link btn-link
:text: Transformation
:::
^^^
Transformation operators.
---

:::{link-button} algorithms/autodoc_watershed.html
:classes: stretched-link btn-link
:text: Watershed
:::
^^^
Watershed segmentation algorithm.

::::
