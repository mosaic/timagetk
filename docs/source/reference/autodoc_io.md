# Input/Output

% Update the table of contents (the left 'quick browse' panel)
```{eval-rst}
.. toctree::
   :maxdepth: 3

```

```{contents}
:depth: 3
```


## Image I/O
```{eval-rst}
.. automodule:: timagetk.io.image
   :members:
```

```{eval-rst}
.. |POSS_EXT| replace:: :attr:`timagetk.io.image.POSS_EXT`

```

Accepted image formats are: |POSS_EXT|.

## Transformation I/O
```{eval-rst}
.. automodule:: timagetk.io.trsf
   :members:
```