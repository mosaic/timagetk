# Command line scripts

% Update the table of contents (the left 'quick browse' panel)
```{eval-rst}
.. toctree::
   :maxdepth: 2
```

## Automatic orientation of intensity image
```{eval-rst}
.. argparse::
   :module: timagetk.bin.orientation
   :func: parsing
   :prog: orientation
```

## Image resampling
```{eval-rst}
.. argparse::
   :module: timagetk.bin.resampling
   :func: parsing
   :prog: resampling
```

## Multi-angle images manual landmarks definition
```{eval-rst}
.. argparse::
   :module: timagetk.bin.multiangle_initialisation
   :func: parsing
   :prog: multiangle_initialisation
```

## Multi-angle images fusion
```{eval-rst}
.. argparse::
   :module: timagetk.bin.multiangle_fusion
   :func: parsing
   :prog: multiangle_fusion
```

## Sequence registration
```{eval-rst}
.. argparse::
   :module: timagetk.bin.sequence_registration
   :func: parsing
   :prog: sequence_registration
```

## Temporal projection
```{eval-rst}
.. argparse::
   :module: timagetk.bin.temporal_projection
   :func: parsing
   :prog: temporal_projection
```