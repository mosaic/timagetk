# Topological elements

% Update the table of contents (the left 'quick browse' panel)
```{eval-rst}
.. toctree::
   :maxdepth: 2
```

```{eval-rst}
.. automodule:: timagetk.algorithms.topological_elements
   :members:
```
