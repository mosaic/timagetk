# Components

![](../_static/image/classes.png)

% Update the table of contents (the left 'quick browse' panel)
```{eval-rst}
.. toctree::
  :maxdepth: 2
```

## SpatialImage class
```{eval-rst}
.. inheritance-diagram:: timagetk.components.spatial_image.SpatialImage
   :parts: 1
   :caption: SpatialImage class inheritance diagram.

.. automodule:: timagetk.components.spatial_image
   :members:
   :special-members:
```

## LabelledImage class
```{eval-rst}
.. inheritance-diagram:: timagetk.components.labelled_image.LabelledImage
   :parts: 1
   :caption: LabelledImage class inheritance diagram.

.. automodule:: timagetk.components.labelled_image
   :members:
   :special-members:
```

## MultiAngleImage class
```{important}
This is a work in progress!
```
```{eval-rst}
.. inheritance-diagram:: timagetk.components.multi_angle.MultiAngleImage
   :parts: 1
   :caption: MultiAngleImage class inheritance diagram.

.. automodule:: timagetk.components.multi_angle
   :members:
   :special-members:
```

## MultiChannelImage class
```{important}
This is a work in progress!
```
```{eval-rst}
.. inheritance-diagram:: timagetk.components.multi_channel.MultiChannelImage
   :parts: 1
   :caption: MultiChannelImage class inheritance diagram.

.. automodule:: timagetk.components.multi_channel
   :members:
   :special-members:
```

## TimeSeries class
```{important}
This is a work in progress!
```
```{eval-rst}
.. inheritance-diagram:: timagetk.components.time_series.TimeSeries
   :parts: 1
   :caption: TimeSeries class inheritance diagram.

.. automodule:: timagetk.components.time_series
   :members:
   :special-members:
```

## TissueImage class
```{important}
This is a work in progress!
```
```{eval-rst}
.. inheritance-diagram:: timagetk.components.tissue_image.TissueImage3D
   :parts: 1
   :caption: TissueImage class inheritance diagram.

.. automodule:: timagetk.components.tissue_image
   :members:
   :special-members:
```

## Trsf class
```{eval-rst}
.. automodule:: timagetk.components.trsf
   :members:
   :special-members:
```

## Metadata class
```{important}
This is a work in progress!
```
```{eval-rst}
.. automodule:: timagetk.components.metadata
   :members:
   :special-members:
```
