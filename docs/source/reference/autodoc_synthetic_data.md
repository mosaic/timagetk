# Synthetic data

% Automatic documentation of `synthetic_data` module, uses extension 'sphinxarg.ext'.

% Update the table of contents (the left 'quick browse' panel)
```{eval-rst}
.. toctree::
   :maxdepth: 2
```

```{contents}
:depth: 3
```

## Nuclei image
```{eval-rst}
.. automodule:: timagetk.synthetic_data.nuclei_image
   :members:
```

## Wall image
```{eval-rst}
.. automodule:: timagetk.synthetic_data.wall_image
   :members:
```

## Labelled image
```{eval-rst}
.. automodule:: timagetk.synthetic_data.labelled_image
   :members:
```

## Miscellaneous
```{eval-rst}
.. automodule:: timagetk.synthetic_data.util
   :members:
```
