# Tasks
% Automatic documentation of `task` module, uses extension 'sphinxarg.ext'.

% Update the table of contents (the left 'quick browse' panel)
```{eval-rst}
.. toctree::
   :maxdepth: 2
```

```{contents}
:depth: 3
```

## Multi-angle fusion
```{eval-rst}
.. automodule:: timagetk.tasks.fusion
   :members:
```

## Intensity image segmentation
```{eval-rst}
.. automodule:: timagetk.tasks.segmentation
   :members:
```

## Clustering
```{eval-rst}
.. automodule:: timagetk.tasks.clustering
   :members:
```
