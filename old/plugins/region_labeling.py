#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Sophie Ribes <sophie.ribes@inria.fr>
#
#       File contributor(s):
#           Guillaume Baty <guillaume.baty@inria.fr>
#           Gregoire Malandain <gregoire.malandain@inria.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

"""
This module contain implementation of region labeling algorithms
"""

import logging

import numpy as np
from timagetk.algorithms.connexe import connexe
from timagetk.third_party.vt_parser import general_kwargs
from timagetk.third_party.vt_parser import parallel_kwargs
from timagetk.components.labelled_image import LabelledImage
from timagetk.components.image import _input_img_check
from timagetk.components.spatial_image import SpatialImage
from timagetk.util import _method_check

__all__ = ['region_labeling']

REGION_LABELLING_METHODS = ['connected_components']
DEFAULT_METHOD = 0  # index of the default method in REGION_LABELLING_METHODS
DEF_CONNECT2D = 8
DEF_CONNECT3D = 26


def region_labeling(image, method=None, **kwargs):
    """Region labeling plugin.

    Valid ``method`` values are:

      - **connected_components**, uses connexity to label regions

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    method : str, optional
        used method, by default *connected_components*

    Other Parameters
    ----------------
    low_threshold : int, optional
        low threshold to binarize input image
    high_threshold : int, optional
        high threshold for *hysteresis thresholding*, implies binary output

    Returns
    -------
    LabelledImage
        transformed image with its metadata

    Notes
    -----
    *Hysteresis thresholding* means *double thresholding*, with a lower and upper value.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.io import imread
    >>> from timagetk.io.util import shared_data
    >>> from timagetk.plugins import h_transform, region_labeling
    >>> image_path = shared_data('p58-t0-a0.lsm', "p58")
    >>> image = imread(image_path)

    >>> # - Get a z-slice:
    >>> zslice = 20
    >>> subimg = image.get_slice(zslice, 'z')

    >>> # - H-minima transformation for h-min threshold and 8-connexe pixels:
    >>> hmin = 50
    >>> reg_min_img = h_transform(subimg, h=hmin, method='min')
    >>> # - Connexe component labelling after hysteresis thresholding:
    >>> seed_img = region_labeling(reg_min_img, high_threshold=hmin, param=True)
    >>> seed_ids = np.unique(seed_img)
    >>> print "Found {} seeds: {}".format(len(seed_ids), list(seed_ids))

    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> images = [subimg, reg_min_img, seed_img]
    >>> titles = ["Original (z-slice: {})".format(zslice), "H-minima (h: {})".format(hmin), "Connexe components labelling"]
    >>> grayscale_imshow(images, title=titles, val_range=['type', 'auto', 'auto'], cmap=['gray', 'viridis', 'Set1'])

    See Also
    --------
    timagetk.algorithms.connexe

    """
    # - Assert the 'image' is a SpatialImage instance:
    msg = _input_img_check(image, var_name='intensity image')
    if msg:
        logging.warning(msg)

    # - Set method if None and check it is a valid method:
    method = _method_check(method, REGION_LABELLING_METHODS, DEFAULT_METHOD)

    if method == 'connected_components':
        low_threshold_val = kwargs.pop('low_threshold', 1)
        high_threshold_val = kwargs.pop('high_threshold', None)
        return connected_components(image,
                                    low_threshold=low_threshold_val,
                                    high_threshold=high_threshold_val,
                                    **kwargs)
    else:
        msg = "The required method '{}' is not implemented!"
        raise NotImplementedError(msg.format(method))


def connected_components(image, low_threshold=1, high_threshold=None,
                         **kwargs):
    """Connected components detection and labelling.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    low_threshold : int, optional
        low threshold to binarize input image
    high_threshold : int, optional
        high threshold for hysteresys thresholding

    Other Parameters
    ----------------
    low_threshold : int
        low threshold to binarize input image
    high_threshold : int
        high threshold for hysteresys thresholding
    min_size_cc : int
        minimal size of connected components
    max_size_cc : int
        maximal size of connected components
    connectivity : int
        connectivity to consider a component "connected", should be in
        [4, 6, 8, 10, 18, 26], by default 26
    max : bool
        if True, default is False, keep the largest connected component and thus
        implies binary output

    Returns
    -------
    LabelledImage
        transformed image with its metadata

    """
    DEF_CONNECT = DEF_CONNECT3D if image.is3D() else DEF_CONNECT2D
    connectivity = kwargs.get('connectivity', DEF_CONNECT)
    if connectivity is None:
        connectivity = DEF_CONNECT
    params = "-connectivity {}".format(connectivity)

    extra_params = ""
    extra_params += "-low-threshold {}".format(abs(int(low_threshold)))
    if high_threshold is not None:
        extra_params += " -high-threshold {}".format(abs(int(high_threshold)))

    min_size_cc = kwargs.get('min_size_cc', None)
    max_size_cc = kwargs.get('max_size_cc', None)
    if min_size_cc is not None:
        extra_params += " -min-size-cc {}".format(min_size_cc)
    if max_size_cc is not None:
        extra_params += " -max-size-cc {}".format(max_size_cc)

    # - Parse general kwargs:
    extra_params += general_kwargs(**kwargs)
    # - Parse parallelism kwargs:
    extra_params += parallel_kwargs(**kwargs)

    # - Define if all are labelled or only the biggest component (max)
    only_max_comp = kwargs.get('max', False)
    if only_max_comp:
        extra_params += " -max"
    else:
        extra_params += " -labels"  # one label per connected component

    out_img = connexe(image, param_str_1=params, param_str_2=extra_params,
                      dtype=kwargs.get('dtype', np.uint16))
    # add2md(out_img)
    return LabelledImage(out_img, not_a_label=0)
