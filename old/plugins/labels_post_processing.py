#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Sophie Ribes <sophie.ribes@inria.fr>
#
#       File contributor(s):
#           Guillaume Baty <guillaume.baty@inria.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

"""
This module contain a generic implementation of labels post processing algorithms.
"""
import logging

from timagetk.algorithms.morphology import label_filtering
from timagetk.third_party.vt_parser import general_kwargs
from timagetk.third_party.vt_parser import parallel_kwargs
from timagetk.components.spatial_image import SpatialImage
from timagetk.components.image import _input_img_check
from timagetk.util import _method_check

__all__ = ["labels_post_processing"]

POSS_METHODS = ["erosion", "dilation", "opening", "closing"]
DEFAULT_METHOD = 0  # index of the default method in POSS_METHODS
DEF_RADIUS = 1  # default radius of the structuring elements
DEF_ITERS = 1  # default number of iterations during morphological operations
DEF_CONNECT = 18  # default connectivity during morphological operations


def _lpp_kwargs(**kwargs):
    """
    Set parameters default values and make sure they are of the right type.
    """
    str_param = ""
    # - By default 'radius' is equal to 1:
    radius = abs(int(kwargs.get("radius", DEF_RADIUS)))
    str_param += " -radius %d" % radius
    # - By default 'iterations' is equal to 1:
    iterations = abs(int(kwargs.get("iterations", DEF_ITERS)))
    str_param += " -iterations %d" % iterations
    # - By default 'connectivity' is equal to 18:
    connectivity = abs(int(kwargs.get("connectivity", DEF_CONNECT)))
    str_param += " -connectivity %d" % connectivity

    # - Parse general kwargs:
    str_param += general_kwargs(**kwargs)
    # - Parse parallelism kwargs:
    str_param += parallel_kwargs(**kwargs)

    return str_param


def labels_post_processing(image, method=None, **kwargs):
    """Labels post-processing algorithms.

    Valid ``method`` values are:

      - **erosion**
      - **dilation**
      - **opening**
      - **closing**

    Parameters
    ----------
    image : SpatialImage
        input image to modify
    method : str
        used method (example: 'erosion')

    Other Parameters
    ----------------
    radius : int, optional; default = 1
        radius of the structuring element
    iterations : int, optional; default = 1
        number of iteration of the morphological operation
    connectivity : value in 4|8|10|18 or 26, default = 18
        structuring element is among the 6-, 10-, 18- & 26-neighborhoods.

    Returns
    -------
    SpatialImage
        transformed image with its metadata

    Warnings
    --------
    If your images are not isometric, the morphological operation will not be
    the same in every directions!

    Example
    -------
    >>> from timagetk.old_plugins import labels_post_processing
    >>> from timagetk.io.util import shared_data
    >>> from timagetk.io import imread
    >>> image_path = shared_data('segmentation_seeded_watershed.inr')
    >>> segmented_image = imread(image_path)
    >>> ero_image = labels_post_processing(segmented_image, radius=2, iterations=1, method='erosion')
    >>> open_image = labels_post_processing(segmented_image, method='opening')
    """
    # - Check ``image`` type and isometry:
    msg = _input_img_check(image, var_name='labelled image')
    if msg:
        logging.warning(msg)
    # - Set method if None and check it is a valid method:
    method = _method_check(method, POSS_METHODS, DEFAULT_METHOD)

    if method == "erosion":
        return labels_erosion(image, **kwargs)
    elif method == "dilation":
        return labels_dilation(image, **kwargs)
    elif method == "opening":
        return labels_opening(image, **kwargs)
    elif method == "closing":
        return labels_closing(image, **kwargs)
    else:
        msg = "The required method '{}' is not implemented!"
        raise NotImplementedError(msg.format(method))


def labels_erosion(image, radius=DEF_RADIUS, iterations=DEF_ITERS, **kwargs):
    """Labels erosion.

    Parameters
    ----------
    radius
    iterations
    image : SpatialImage
        input labelled SpatialImage to erode

    Other Parameters
    ----------------
    radius : int, optional
        radius of the structuring element; default = 1
    iterations : int, optional
        number of iteration of the morphological operation; default = 1

    Returns
    -------
    SpatialImage
        transformed image with its metadata
    """
    params = "-erosion"
    params += _lpp_kwargs(radius=radius, iterations=iterations, **kwargs)
    out_img = label_filtering(image, param_str_2=params)
    # add2md(out_img)
    return out_img


def labels_dilation(image, radius=DEF_RADIUS, iterations=DEF_ITERS, **kwargs):
    """Labels dilation.

    Parameters
    ----------
    radius
    iterations
    image : SpatialImage
        input labelled SpatialImage to dilate

    Other Parameters
    ----------------
    radius : int, optional
        radius of the structuring element; default = 1
    iterations : int, optional
        number of iteration of the morphological operation; default = 1

    Returns
    -------
    SpatialImage
        transformed image with its metadata
    """
    params = "-dilation"
    params += _lpp_kwargs(radius=radius, iterations=iterations, **kwargs)
    out_img = label_filtering(image, param_str_2=params)
    # add2md(out_img)
    return out_img


def labels_opening(image, radius=DEF_RADIUS, iterations=DEF_ITERS, **kwargs):
    """Labels opening.

    Parameters
    ----------
    radius
    iterations
    image : SpatialImage
        input ``SpatialImage``

    Other Parameters
    ----------------
    radius : int, optional
        radius of the structuring element; default = 1
    iterations : int, optional
        number of iteration of the morphological operation; default = 1

    Returns
    -------
    SpatialImage
        transformed image with its metadata
    """
    params = "-opening"
    params += _lpp_kwargs(radius=radius, iterations=iterations, **kwargs)
    out_img = label_filtering(image, param_str_2=params)
    # add2md(out_img)
    return out_img


def labels_closing(image, radius=DEF_RADIUS, iterations=DEF_ITERS, **kwargs):
    """Labels closing.

    Parameters
    ----------
    radius
    iterations
    image : SpatialImage
        input ``SpatialImage``

    Other Parameters
    ----------------
    radius : int, optional
        radius of the structuring element; default = 1
    iterations : int, optional
        number of iteration of the morphological operation; default = 1

    Returns
    -------
    SpatialImage
        transformed image with its metadata
    """
    params = "-closing"
    params += _lpp_kwargs(radius=radius, iterations=iterations, **kwargs)
    out_img = label_filtering(image, param_str_2=params)
    # add2md(out_img)
    return out_img
