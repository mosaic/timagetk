#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Sophie Ribes <sophie.ribes@inria.fr>
#
#       File contributor(s):
#           Guillaume Baty <guillaume.baty@inria.fr>
#           Gregoire Malandain <gregoire.malandain@inria.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

"""This module contains a generic implementation of several registration algorithms.

"""

import logging

import numpy as np
from timagetk.algorithms.blockmatching import blockmatching
from timagetk.algorithms.trsf import apply_trsf
from timagetk.algorithms.trsf import compose_trsf
from timagetk.components.image import _input_img_check
from timagetk.components.multi_angle import MultiAngleImage
from timagetk import MultiChannelImage
from timagetk import SpatialImage
from timagetk.third_party.vt_parser import BLOCKMATCHING_METHODS
from timagetk.third_party.vt_parser import DEF_BM_METHOD
from timagetk.util import _method_check
from vt import vtTransformation as Trsf

NOT_BAL_TRSF = "Provided '{}' is not a Trsf!"
NOT_TYPE = "Provided '{}' is not a {} transformation!"


class SpatialRegistrationPlugin(object):
    """SpatialRegistrationPlugin plugin, allows to register floating onto reference image.

    Attributes
    ----------
    ref_img : SpatialImage or MultiChannelImage
        The reference image for registration.
    flo_img : SpatialImage or list of SpatialImage or MultiChannelImage
        A list or a single floating image, i.e. the one(s) to register on the reference.
    ref_channel : str
        Defines the channel to use to compute registration in case of ``MultiChannelImage``.
    registration_method : str
        The registration method to use.
    py_ll : int
        Lowest level at which to compute the deformation, default is 2 (min is 0).
    py_hl : int
        Highest level at which to compute the deformation, default is 5 (max is 5).
    init_trsf : Trsf
        Transformation used to initialize blockmatching registration, will be present in the resulting transformation
        ``self.out_trsf`` after computing registration ``self.register()``.
    left_trsf : Trsf
        Transformation used to initialize blockmatching registration, will NOT be
        present in the resulting transformation ``self.out_trsf`` after computing
        registration ``self.register()``.
    out_trsf : Trsf
        Transformation obtained after computing registration with ``self.register()``.

    TODO: Should probably replace the ``apply_trsf`` with the corresponding plugin ?!.
    """

    def __init__(self, floating_img, reference_img, method=None, **kwargs):
        """Registration plugin, goes from floating to reference image.

        Valid ``method`` values are:

          - **rigid**: only rotations and translations are allowed;
          - **affine**: rotations, translations and linear deformations along orthogonal axis are allowed;
          - **vectorfield**: a vectorfield is obtained mapping each voxels of the floating image to the reference image.

        Parameters
        ----------
        floating_img : SpatialImage or list of SpatialImage or MultiChannelImage
            a list or a single floating image, *ie.* the one(s) to register on the reference
        reference_img : SpatialImage or MultiChannelImage
            reference image for registration
        method : str, optional
            registration method to use, ``rigid`` by default, see ``BLOCKMATCHING_METHODS``.

        Other Parameters
        ----------------
        pyramid_lowest_level : int
            lowest level at which to compute deformation, default is 2 (min is 0)
        pyramid_highest_level : int
            highest level at which to compute deformation, default is 5 (max is 5)
        init_trsf : Trsf, list of Trsf
            if provided (default ``None``) the trsf will be used to initialize
            blockmatching registration and the returned trsf will contain this trsf
        left_trsf : Trsf, list of Trsf
            if provided (default ``None``) the trsf will be used to initialize
            blockmatching registration but returned trsf will NOT contain this trsf
        reference_channel : str
            Use it with ``MultiChannelImage`` to defines the channel to use to
            compute registration.
            A ``MultiChannelImage`` instance will be returned.
        single_channel : str
            Use it with ``MultiChannelImage`` to defines the channel to extract and
            use to compute registration.
            A ``SpatialImage`` instance will be returned.

        Returns
        -------
        Trsf, list of Trsf
            computed transformation matrix (if linear method) or vectorfield (if
            non-linear method), may be a list if a list of floating image is given
        SpatialImage, list of SpatialImage
            registered image, may be a list if a list of floating image is given

        Notes
        -----
        To use ``MultiChannelImage``, both input image should be of this type and a
        keyword argument ``reference_channel`` should be specified to know which
        channel to use as reference. Obviously both image should have this
        channel name. Other channels of floating image will also be registered
        by the transformation obtained on this reference channel.

        If 'vectorfield' registration is required and no ``Trsf`` is given to
        ``init_trsf`` or ``left_trsf``, we compute the rigid transformation
        prior to the vectorfield transformation to initialize it using
        ``init_trsf`` (with composition, `ie.` returned ``Trsf`` is
         'rigid o vectorfield').

        If a list of floating image is given, we returns a list of transformations
        and images sorted as the floating image list.

        If a list of floating image is given, a list of the same length should be
        given as ``init_trsf`` or ``left_trsf``.

        Example
        -------
        >>> from timagetk.io.util import shared_data
        >>> from timagetk.io import imread, imsave
        >>> from timagetk.plugins.registration import SpatialRegistrationPlugin

        >>> # Example #1 - Registering two intensity images:
        >>> ref_path = shared_data('p58-t0-a0.lsm', "p58")
        >>> ref_img = imread(ref_path)
        >>> # Manual creation of a rigid trsf to create artificially rotated image
        >>> from timagetk.algorithms.trsf import create_trsf
        >>> from timagetk.algorithms.trsf import apply_trsf
        >>> trsf = create_trsf('random', trsf_type='rigid', angle_range=[0.2, 0.25], translation_range=[0.2, 1.2])
        >>> trsf.print()
        >>> float_img = apply_trsf(ref_img, trsf)
        >>> # Let's have a look at the objects orientations after rigid transformation:
        >>> from timagetk.plugins.spatial_image import SpatialImagePlugin
        >>> SpatialImagePlugin(ref_img).plot.projection()
        >>> SpatialImagePlugin(float_img).plot.projection()
        >>> # Now we use the SpatialRegistrationPlugin to register the transformed image onto the original one:
        >>> srp = SpatialRegistrationPlugin(float_img, ref_img, method='rigid', pyramid_lowest_level=2, pyramid_highest_level=5)
        >>> trsf_def, img_def = srp.register()
        >>> # Let's have a look at the objects orientations after rigid registration:
        >>> from timagetk.plugins.spatial_image import SpatialImagePlugin
        >>> SpatialImagePlugin(ref_img).plot.projection()
        >>> SpatialImagePlugin(img_def).plot.projection()

        >>> # Using shared data as example, images are multi-angle view of the same floral meristem:
        >>> fname = 'p58-t0-a{}.{}'
        >>> ref_img = imread(shared_data(fname.format(0, 'lsm'), "p58"))
        >>> float_imgs = [imread(shared_data(fname.format(angle_id, 'lsm'), "p58")) for angle_id in range(1, 3)]
        >>> trsf_rig, res_rig = SpatialRegistrationPlugin(float_imgs, ref_img, method='rigid', pyramid_lowest_level=2)

        >>> # Save result images:
        >>> [imsave(shared_data(fname.format(str(angle_id)+"_on_a0", 'inr'), "p58"), res_rig[angle_id-1]) for angle_id in range(1, 3)]

        >>> from timagetk.io.util import shared_data
        >>> from timagetk.io import imread, imsave
        >>> from timagetk.plugins.registration import SpatialRegistrationPlugin
        >>> float_path = "https://zenodo.org/record/3737795/files/qDII-CLV3-PIN1-PI-E35-LD-SAM4.czi"
        >>> ref_path = "https://zenodo.org/record/3737795/files/qDII-CLV3-PIN1-PI-E35-LD-SAM4-T5.czi"
        >>> float_img = imread(float_path)
        >>> ref_img = imread(ref_path)
        >>> # Now we use the SpatialRegistrationPlugin to register the transformed image onto the original one:
        >>> srp = SpatialRegistrationPlugin(float_img, ref_img, method='rigid', pyramid_lowest_level=2, pyramid_highest_level=5, reference_channel="Ch1-T3_EBFP")
        >>> trsf_def, img_def = srp.register()
        >>> print(img_def)
        >>> from os.path import join
        >>> imsave(join(float_img.filepath, float_img.name, "_registered.czi"), img_def)
        >>> # Let's have a look at the objects orientations after rigid registration:
        >>> from timagetk.visu.stack import channels_stack_browser
        >>> channels_stack_browser(img_def, title="Rigid registration")

        """
        self.ref_img = reference_img
        self.flo_img = floating_img
        self.ref_channel = kwargs.get('reference_channel', None)
        logging.info(
            f"Initialized '{self.ref_img.filename}' as reference image!")
        if isinstance(self.flo_img, (SpatialImage, MultiChannelImage)):
            logging.info(
                f"Initialized '{self.flo_img.filename}' as floating image!")
        elif isinstance(self.flo_img, (list, tuple)):
            logging.info(f"Initialized a list of floating images:\n - ")
            logging.info('\n - '.join([flo.filename for flo in self.flo_img]))
        else:
            raise TypeError("Weird... did you read the doc ?!")

        if isinstance(self.ref_img, MultiChannelImage):
            self._check_multi()
        else:
            self._check_spatial()

        # - Set to default method if None OR check it is a valid method:
        method = _method_check(method, BLOCKMATCHING_METHODS, DEF_BM_METHOD)
        self.registration_method = method

        self.py_ll = kwargs.pop('pyramid_lowest_level', 2)
        self.py_hl = kwargs.pop('pyramid_highest_level', 5)
        try:
            assert self.py_ll < self.py_hl
        except AssertionError:
            msg = f"Pyramid lowest level ({self.py_ll}) should be strictly smaller than highest level ({self.py_hl})!"
            raise ValueError(msg)

        # - Provided 'init_trsf' will be used to initialize blockmatching
        # registration and the returned trsf will contain this trsf
        self.init_trsf = kwargs.pop('init_trsf', None)
        # - Provided 'left_trsf' will be used to initialize blockmatching
        # registration but returned trsf will NOT contain this trsf
        self.left_trsf = kwargs.pop('left_trsf', None)
        self._check_trsf()

        # - Dictionary to manage output Trsf:
        # self.out_trsf = {reg: Trsf() for reg in BLOCKMATCHING_METHODS}

    def _check_trsf(self):
        # Make sure we did not get both `init_trsf` & `left_trsf`:
        trsfs = [self.init_trsf, self.left_trsf]
        try:
            assert sum([trsf is not None for trsf in trsfs]) < 2
        except AssertionError:
            raise ValueError(
                "You cannot define both `init_trsf` & `left_trsf`!")

        # - If a list of floating images is given, 'init_trsf' or 'left_trsf' should be a list of same length:
        if isinstance(self.flo_img, list):
            if self.init_trsf is not None:
                try:
                    assert len(self.init_trsf) == len(self.flo_img)
                except AssertionError:
                    msg = f"There should be the same number of `init_trsf` ({len(self.init_trsf)}) and float images ({len(self.flo_img)})!"
                    raise ValueError(msg)
                try:
                    assert all(
                        [isinstance(trsf, Trsf) for trsf in self.init_trsf])
                except AssertionError:
                    msg = "All `init_trsf` inputs should be `Trsf` instances !"
                    raise ValueError(msg)
            else:
                self.init_trsf = [None] * len(self.flo_img)

        # - If a list of floating images is given, 'left_trsf' should be a list of same length:
        if isinstance(self.flo_img, list):
            if self.left_trsf is not None:
                try:
                    assert len(self.left_trsf) == len(self.flo_img)
                except AssertionError:
                    msg = f"There should be the same number of `left_trsf` ({len(self.left_trsf)}) and float images ({len(self.flo_img)})!"
                    raise ValueError(msg)
                try:
                    assert all(
                        [isinstance(trsf, Trsf) for trsf in self.left_trsf])
                except AssertionError:
                    msg = "All `left_trsf` inputs should be `Trsf` instances !"
                    raise ValueError(msg)
            else:
                self.left_trsf = [None] * len(self.flo_img)

    def _check_multi(self):
        # Make sure float image is a MultiChannelImage:
        try:
            assert isinstance(self.flo_img, MultiChannelImage)
        except AssertionError:
            msg = f"When using MultiChannelImage, both reference and float image should be of same type, but got {type(self.flo_img)} as float image!"
            raise TypeError(msg)
        # Make sure reference image is a MultiChannelImage:
        try:
            assert isinstance(self.ref_img, MultiChannelImage)
        except AssertionError:
            msg = f"When using MultiChannelImage, both reference and float image should be of same type, but got {type(self.ref_img)} as reference image!"
            raise TypeError(msg)
        # Make sure the reference channel is defined:
        try:
            assert self.ref_channel is not None
        except AssertionError:
            msg = "When using MultiChannelImage, you should provide a reference channel!"
            raise ValueError(msg)
        # Make sure 'self.ref_channel' is defined in multi-channel float image:
        try:
            assert self.ref_channel in self.flo_img.keys()
        except AssertionError:
            msg = f"Could not find channel '{self.ref_channel}' in float image!"
            raise ValueError(msg)
        # Make sure 'self.ref_channel' is defined in multi-channel reference image:
        try:
            assert self.ref_channel in self.ref_img.keys()
        except AssertionError:
            msg = f"Could not find channel '{self.ref_channel}' in reference image!"
            raise ValueError(msg)

    def _check_spatial(self):
        # Make sure reference image is a SpatialImage:
        msg = _input_img_check(self.ref_img, var_name='reference image')
        if msg:
            logging.warning(msg)

        # - Assert given `floating_img` is a SpatialImage or a list of SpatialImage:
        if isinstance(self.flo_img, list):
            msg = [_input_img_check(float_img,
                                    var_name='floating image #{}'.format(n)) for
                   n, float_img in enumerate(self.flo_img)]
            [logging.warning(m) for m in msg if m]
        else:
            msg = _input_img_check(self.flo_img, var_name='floating image')
            if msg:
                logging.warning(msg)

    @staticmethod
    def _spatial_register(floating_img, reference_img, method, init_trsf,
                          left_trsf, py_ll, py_hl):
        trsf_out = init_trsf
        if method == 'vectorfield' and init_trsf is None and left_trsf is None:
            method = ['rigid', 'vectorfield']

        if 'rigid' in method:
            trsf_out = blockmatching(floating_img, reference_img,
                                     method='rigid',
                                     init_trsf=init_trsf, left_trsf=left_trsf,
                                     pyramid_lowest_level=py_ll,
                                     pyramid_highest_level=py_hl)
        if 'affine' in method:
            trsf_out = blockmatching(floating_img, reference_img,
                                     method='affine',
                                     init_trsf=init_trsf, left_trsf=left_trsf,
                                     pyramid_lowest_level=py_ll,
                                     pyramid_highest_level=py_hl)
        if 'vectorfield' in method:
            trsf_out = blockmatching(floating_img, reference_img,
                                     method='vectorfield',
                                     init_trsf=trsf_out, left_trsf=left_trsf,
                                     pyramid_lowest_level=py_ll,
                                     pyramid_highest_level=py_hl)

        img_out = apply_trsf(floating_img, trsf_out, template_img=reference_img,
                             interpolation='linear')
        return trsf_out, img_out

    def register(self):
        """Performs the registration. """
        if isinstance(self.ref_img, SpatialImage):
            if isinstance(self.flo_img, SpatialImage):
                return self._spatial_register(self.flo_img, self.ref_img,
                                              self.registration_method,
                                              self.init_trsf, self.left_trsf,
                                              self.py_ll, self.py_hl)
            elif isinstance(self.flo_img, (list, tuple)):
                return [self._spatial_register(flo_img, self.ref_img,
                                               self.registration_method,
                                               self.init_trsf[n_im],
                                               self.left_trsf[n_im],
                                               self.py_ll, self.py_hl) for
                        n_im, flo_img in enumerate(self.flo_img)]
            else:
                raise TypeError(
                    f"Don't know what to do with `type(self.flo_img)`: {type(self.flo_img)}!")
        else:
            # MultiChannelImage case:
            flo_img = self.flo_img[self.ref_channel]
            ref_img = self.ref_img[self.ref_channel]
            trsf_out, img_out = self._spatial_register(flo_img, ref_img,
                                                       self.registration_method,
                                                       self.init_trsf,
                                                       self.left_trsf,
                                                       self.py_ll, self.py_hl)
            channel_names = self.ref_img.get_channel_names()
            other_channels = [apply_trsf(self.flo_img[ch], trsf_out,
                                         template_img=ref_img,
                                         interpolation='linear') for ch in
                              channel_names if ch != self.ref_channel]
            img_out = MultiChannelImage([img_out] + other_channels,
                                        channel_names=channel_names)

            return trsf_out, img_out


class StackRegistrationPlugin(object):
    """Register the slices of a 3D image.

    Attributes
    ----------
    image : SpatialImage or MultiChannelImage
        3D image for which the slices needs registration.
    method : srt, optional
        SpatialRegistrationPlugin method to use, should be either 'rigid' or 'translate'.
    invert : bool, optional
        If ``True`` revert the z-axis before registration and again after to return
        th image in its original orientation

    Other Parameters
    ----------------
    get_trsf : bool
        If ``True``, the list of transformations for z-stack registration are
        also returned.

    Returns
    -------
    reg_stack : SpatialImage
        3D image with registered z-slices.
    trsfs : list
        List of transformations for z-stack registration, returned only if keyword
        argument ``get_trsf`` is ``True``.

    Notes
    -----
    We starts with the last slice since we assume this is the slice with the most
    signal (information) to use for registration to ensure stability.
    If its not your case, you can invert the z-axis with ``invert=True``, the returned
    registered image will be oriented as your original image.

    Examples
    --------
    >>> from timagetk.io import imread
    >>> from timagetk.io.util import shared_data
    >>> from timagetk.plugins.registration import StackRegistrationPlugin
    >>> fname = 'p58-t0-a0.lsm'
    >>> image = imread(shared_data(fname))
    >>> reg_method = 'rigid'
    >>> reg_stack = StackRegistrationPlugin(image, method=reg_method)

    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> mid_y = int(image.get_shape('y')/2.)
    >>> grayscale_imshow([image, reg_stack], slice_id=mid_y, axis='y', title=['Original', 'Registered'], suptitle="Z-stack {} registration - y-slice {}/{}".format(reg_method, mid_y, image.get_shape('y')))

    >>> grayscale_imshow([image, reg_stack], title=['Original', 'Registered'], suptitle="Z-stack {} registration - Contour projection".format(reg_method))

    >>> from timagetk.visu.stack import stack_browser
    >>> stack_browser(reg_stack, title="{} registered z-stack".format(reg_method))

    >>> from timagetk.visu.stack import channels_stack_browser
    >>> channels_stack_browser([image, reg_stack], channel_names=['Original', 'Registered'], colors=['red', 'green'], title="Z-stack {} registration".format(reg_method))

    """

    def __init__(self, image, **kwargs):
        """

        Parameters
        ----------
        image
        """
        self.image = image
        self.reference_channel = kwargs.get('reference_channel', None)

    def register_from_first_slice(self, method='rigid', axis='z', **kwargs):
        """Register the slices of a 3D image.

        Starts with the first slice (slice_1) as reference, and the second
        (slice_2) as float, then the registered slice_2 is used to register
        slice_3 and so on until the last slice.
        Using 'rigid' or 'translate' method is advised.

        Parameters
        ----------
        method : srt in {'rigid', 'translate'}, optional
            Registration method to use, 'rigid' by default.
        axis : str in {'x', 'y', 'z'}, optional
            Axis to use for slice registration.

        Returns
        -------
        reg_stack : SpatialImage
            3D image with registered z-slices.

        Notes
        -----
        Between the first and the last slice, you should start with the one
        showing the stronger signal (information) to get better results.

        Examples
        --------
        >>> from timagetk.io import imread
        >>> from timagetk.io.util import shared_data
        >>> from timagetk.plugins.registration import StackRegistrationPlugin
        >>> fname = 'p58-t0-a0.lsm'
        >>> image = imread(shared_data(fname))
        >>> reg_method = 'rigid'
        >>> reg_stack = StackRegistrationPlugin(image, method=reg_method)

        >>> from timagetk.visu.mplt import grayscale_imshow
        >>> mid_y = int(image.get_shape('y')/2.)
        >>> grayscale_imshow([image, reg_stack], slice_id=mid_y, axis='y', title=['Original', 'Registered'], suptitle="Z-stack {} registration - y-slice {}/{}".format(reg_method, mid_y, image.get_shape('y')))

        >>> grayscale_imshow([image, reg_stack], title=['Original', 'Registered'], suptitle="Z-stack {} registration - Contour projection".format(reg_method))

        >>> from timagetk.visu.stack import stack_browser
        >>> stack_browser(reg_stack, title="{} registered z-stack".format(reg_method))

        >>> from timagetk.visu.stack import channels_stack_browser
        >>> channels_stack_browser([image, reg_stack], channel_names=['Original', 'Registered'], colors=['red', 'green'], title="Z-stack {} registration".format(reg_method))

        """

        image = self.register_from_last_slice(self.image.invert_axis(axis),
                                              method=method, **kwargs)
        return image.invert_axis(axis)

    def register_from_last_slice(self, method='rigid', axis='z', **kwargs):
        """

        Parameters
        ----------
        method
        axis
        kwargs

        Returns
        -------


        Examples
        --------
        >>> from timagetk.plugins.registration import StackRegistrationPlugin
        >>> from timagetk.io import imread
        >>> im_fname = "/data/Meristems/Carlos/Nuclei_registration/qDII-CLV3-AHP6-E89-LD-SAM2.czi"
        >>> ch_names = ["qDII", "AHP6", "tagBFP", "CLV3"]
        >>> im = imread(im_fname, channel_names=ch_names)
        >>> print(im.get_channel_names())
        ['qDII', 'AHP6', 'tagBFP', 'CLV3']
        >>> srp = StackRegistrationPlugin(im, reference_channel="tagBFP")
        >>> reg_im, trsf = srp.register_from_last_slice('rigid', 'z')
        >>>

        """
        if isinstance(self.image, MultiChannelImage):
            ref_channel = kwargs.get('reference_channel',
                                     self.reference_channel)
            assert ref_channel is not None
            reg_stacks = []
            # Slice registration of the reference channel:
            reg_stack, stack_trsf = self._register_from_last_slice(
                self.image[ref_channel], method, axis)
            reg_stacks.append(reg_stack)
            # Register the other "floating" channels with the reference stack transformation:
            float_channels = self.image.get_channel_names().remove(ref_channel)
            for ch in float_channels:
                reg_stacks.append(
                    self._apply_stack_trsf(self.image[ch], stack_trsf))
            # Reconstruct the MultiChannelImage object:
            reg_stack = MultiChannelImage(reg_stacks,
                                          [ref_channel] + float_channels)
        elif isinstance(self.image, SpatialImage):
            reg_stack, stack_trsf = self._register_from_last_slice(self.image,
                                                                   method, axis)
        else:
            NotImplementedError("Not yet?!")

        return reg_stack, stack_trsf

    @staticmethod
    def _register_from_last_slice(image, method, axis, **kwargs):
        assert method in BLOCKMATCHING_METHODS
        assert image.is3D()

        axsh = image.get_shape(axis)
        # Create the empty "registered stack" to receive the registered slice
        reg_stack = np.zeros_like(image, dtype=image.dtype)
        reg_stack = SpatialImage(reg_stack, voxelsize=image.voxelsize,
                                 origin=image.origin, metadata=image.metadata)

        # Add the first ref slice, i.e. the last slice of the original stack, to the "registered stack":
        reg_stack.set_slice(axsh - 1,
                            image.get_slice(axsh - 1, axis).get_array(),
                            axis=axis)

        # Registration loop:
        stack_trsf = []
        for n in range(axsh)[::-1][:-1]:
            # Reference slice definition:
            ref_slice = reg_stack.get_slice(n, axis)
            # Floating image is N-1 from original image:
            float_slice = image.get_slice(n - 1, axis)
            # Registration:
            trsf, reg_slice = SpatialRegistrationPlugin(float_slice, ref_slice,
                                                        method=method).register()
            stack_trsf.append(trsf)
            # Add the registered slice to the corresponding "registered stack" slice
            reg_stack.set_slice(n - 1, reg_slice.get_array(), axis=axis)

        return reg_stack, stack_trsf

    @staticmethod
    def _apply_stack_trsf(image, stack_trsf):
        """Apply z-stack transformation to a list of images.

        Parameters
        ----------
        image : SpatialImage
            3D image to which the stack transformation should be applied to.
        stack_trsf : list of Trsf
            The stack transformation list as returned by ``_register_from_last_slice``.

        Returns
        -------
        SpatialImage
            Stack registered 3D image.

        Examples
        --------
        >>> from timagetk.io import imread
        >>> from timagetk.io.util import shared_data
        >>> from timagetk.plugins.registration import StackRegistrationPlugin
        >>> fname = 'p58-t0-a0.lsm'
        >>> image = imread(shared_data(fname))
        >>> reg_method = 'rigid'
        >>> reg_stack, stack_trsf = StackRegistrationPlugin(image, method=reg_method, get_trsf=True)
        >>> reg_stack2 = apply_z_stack_trsf(image, stack_trsf)

        >>> # Combine the two images, they should be perfectly aligneg
        >>> from timagetk.visu.stack import channels_stack_browser
        >>> channels_stack_browser([reg_stack, reg_stack2], channel_names=['Registered', 'Apply z-stack transformation'], colors=['red', 'green'], title="Z-stack {} registration".format(reg_method))

        >>> # TODO: add a multi-channel image to `shared_data` & make a more realistic example

        """
        assert image.is3D()
        zdim = image.get_shape('z')
        assert len(stack_trsf) == zdim - 1

        for n, trsf in enumerate(stack_trsf):
            float_slice_id = zdim - 2 - n
            reg_slice = apply_trsf(image.get_slice(float_slice_id, 'z'), trsf)
            image.set_slice(float_slice_id, reg_slice.get_array()[:, :, 0], 'z')

        return image


class TemporalRegistrationPlugin(object):
    """

    """

    def __init__(self, list_images, method=None, **kwargs):
        """Initialize the temporal registration plugin.

        Parameters
        ----------
        list_images : list of SpatialImage, list(MultiChannelImage), TimeSeries
            Temporal list of image to register.
        method : str, optional
            Used method for registration procedure, 'rigid' by default.

        Other Parameters
        ----------------
        return_images : bool, optional
            if ``True`` (default ``False``), the registered image list is returned too, note
            that it will use much more memory

        """
        # - Check list_images type:
        try:
            assert isinstance(list_images, list)
        except AssertionError:
            msg = "Parameter 'list_images' should be of type 'list', but is: {}"
            raise TypeError(msg.format(type(list_images)))

        # - Check SpatialImage consecutive:
        try:

            assert all([isinstance(img, SpatialImage) for img in list_images])
        except AssertionError:
            msg = "Parameter 'list_images' should be a list of SpatialImages!"
            raise TypeError(msg)

        # - Check SpatialImage consecutive length, this function is useless if length < 3!
        try:
            assert len(list_images) >= 3
        except AssertionError:
            msg = "Parameter 'list_images' should have a minimum length of 3!"
            raise ValueError(msg)

        # -- Defines attributes:
        self.imgs = list_images
        # - Set method if None and check it is a valid method:
        self.method = _method_check(method, BLOCKMATCHING_METHODS,
                                    DEF_BM_METHOD)
        # -- Set if registered images should be computed and returned:
        self.return_img = kwargs.pop('return_images', False)
        self.py_ll = kwargs.pop('pyramid_lowest_level', 2)
        self.py_hl = kwargs.pop('pyramid_highest_level', 5)
        try:
            assert self.py_ll < self.py_hl
        except AssertionError:
            msg = f"Pyramid lowest level ({self.py_ll}) should be strictly smaller than highest level ({self.py_hl})!"
            raise ValueError(msg)

    def _check_multi(self):
        # Make sure float image is a MultiChannelImage:
        try:
            assert np.all(
                [isinstance(img, MultiChannelImage) for img in self.imgs])
        except AssertionError:
            msg = f"When using MultiChannelImage, both reference and float image should be of same type, but got {type(self.flo_img)} as float image!"
            raise TypeError(msg)
        # Make sure reference image is a MultiChannelImage:
        try:
            assert isinstance(self.ref_img, MultiChannelImage)
        except AssertionError:
            msg = f"When using MultiChannelImage, both reference and float image should be of same type, but got {type(self.ref_img)} as reference image!"
            raise TypeError(msg)
        # Make sure the reference channel is defined:
        try:
            assert self.ref_channel is not None
        except AssertionError:
            msg = "When using MultiChannelImage, you should provide a reference channel!"
            raise ValueError(msg)
        # Make sure 'self.ref_channel' is defined in multi-channel float image:
        try:
            assert self.ref_channel in self.flo_img.keys()
        except AssertionError:
            msg = f"Could not find channel '{self.ref_channel}' in float image!"
            raise ValueError(msg)
        # Make sure 'self.ref_channel' is defined in multi-channel reference image:
        try:
            assert self.ref_channel in self.ref_img.keys()
        except AssertionError:
            msg = f"Could not find channel '{self.ref_channel}' in reference image!"
            raise ValueError(msg)

    def _check_spatial(self):
        # Make sure reference image is a SpatialImage:
        msg = _input_img_check(self.ref_img, var_name='reference image')
        if msg:
            logging.warning(msg)

        # - Assert given `floating_img` is a SpatialImage or a list of SpatialImage:
        if isinstance(self.flo_img, list):
            msg = [_input_img_check(float_img,
                                    var_name='floating image #{}'.format(n)) for
                   n, float_img in enumerate(self.flo_img)]
            [logging.warning(m) for m in msg if m]
        else:
            msg = _input_img_check(self.flo_img, var_name='floating image')
            if msg:
                logging.warning(msg)

    def _consecutive_registration(self, **kwargs):
        kwargs['pyramid_lowest_level'] = self.py_ll
        kwargs['pyramid_highest_level'] = self.py_hl
        # - Compute the consecutive transformations, i.e. from one time-point to the next:
        print("\n# - Performing consecutive image registration:")
        consecutive_trsf = consecutive_registration(self.imgs, self.method,
                                                    return_images=False,
                                                    **kwargs)
        # - Compose the consecutive transformations to all register to the last time-point:
        print("\n# - Composing consecutive transformations:")
        list_res_trsf = compose_to_last(consecutive_trsf, self.imgs[-1])

        # - If required, apply the transformations to the images and return them:
        if self.return_img:
            print("# - Applying composed transformations:")
            list_res_img = []
            n_imgs = len(self.imgs)
            for ind, trsf in enumerate(list_res_trsf):
                msg = "Applying t{}/{} composed transformation to t{}..."
                print(msg.format(ind, n_imgs - 1, ind))
                tmp_img = apply_trsf(self.imgs[ind], trsf,
                                     template_img=self.imgs[-1])
                list_res_img.append(tmp_img)
                print("Done.\n")
            # Add last image of temporal sequence to the list of resulting images to returns
            list_res_img.append(self.imgs[-1])
            return list_res_trsf, list_res_img
        else:
            return list_res_trsf

    def forward_registration(self):
        """Performs forward temporal registration.

        First compute T(1,0) transformations, i.e. T(t_0 <- t_1), for all n in
        [1, N], where N=length(list_images), then compose them to obtain the list of
        transformations registering all images on the first one:
        [T(1,0); T(2,0); ...; T(N,0)].

        Returns
        -------
        list_trsf : list of Trsf
            list of Trsf, i.e. transformation matrix or vectorfields
        list_res_img : list of SpatialImage, optional
            list of sequentially registered SpatialImage, returned if optional
            ``return_images`` is ``True``.

        Examples
        --------
        >>> from timagetk.io.util import shared_data
        >>> from timagetk.io import imread, imsave
        >>> from timagetk.plugins.registration import TemporalRegistrationPlugin
        >>> float_path = "/data/Meristems/Carlos/Nuclei_registration/qDII-CLV3-AHP6-E89-LD-SAM2.czi"
        >>> ref_path = "/data/Meristems/Carlos/Nuclei_registration/qDII-CLV3-AHP6-E89-LD-SAM2-T4.czi"
        >>> float_img = imread(float_path)
        >>> ref_img = imread(ref_path)
        >>> # Now we use the SpatialRegistrationPlugin to register the transformed image onto the original one:
        >>> srp = TemporalRegistrationPlugin([float_img, ref_img], method='rigid', reference_channel="Ch1-T3_EBFP")
        >>> trsf_def, img_def = srp.register()
        >>> print(img_def)

        """
        return self._consecutive_registration(self.imgs, self.method,
                                              self.return_img)

    def backward_registration(self):
        """Performs backward temporal registration.

        First compute T(n,n+1) transformations, i.e. T(t_n -> t_n+1), for all n in
        [1, N], where N=length(list_images), then compose them to obtain the list of
        transformations registering all images on the last one:
        [T(0,N); T(1,N); ...; T(N-1,N)].

        Returns
        -------
        list_trsf : list of Trsf
            list of Trsf, i.e. transformation matrix or vectorfields
        list_res_img : list of SpatialImage, optional
            list of sequentially registered SpatialImage, returned if optional
            ``return_images`` is ``True``.
        """
        return self._consecutive_registration(self.imgs[::-1], self.method,
                                              self.return_img)


REG_MSG = "\nPerforms {} registration of t{} on t{}:"


def reference_registration(list_images, method, **kwargs):
    """Registration of each images of the list onto the first one.

    Parameters
    ----------
    list_images : list of SpatialImage
        list of time sorted SpatialImage
    method : str
        used method, by default 'rigid'

    Other Parameters
    ----------------
    return_images : bool, optional
        if ``True`` (default ``False``), the registered image list is returned too
    ref_index : int, optional
        Index of the reference image, 0 by default, *i.e.* the first one of the list.

    Returns
    -------
    list of Trsf
        list of composed transformation
    list of SpatialImage
        list of images after rigid registration

    Notes
    -----
    The list of returned image is sorted as the input list image.
    The reference is also included in first position (index 0).

    Examples
    --------
    >>> from timagetk.io import imread
    >>> from timagetk.io.util import shared_data
    >>> from timagetk.plugins.registration import reference_registration

    >>> # Using shared data as example, images are multi-angle view of the same floral meristem:
    >>> fname = 'p58-t0-a{}.lsm'
    >>> list_files = [shared_data(fname.format(a), 'p58') for a in range(3)]
    >>> list_img = [imread(fname) for fname in list_files]
    >>> # Down-sampling the images to speed up the example:
    >>> from timagetk.algorithms.resample import resample
    >>> list_img = [resample(im, shape=[sh/2. for sh in im.shape]) for im in list_img]
    >>> # Register all other images on the first of the list:
    >>> trsf_list, list_img = reference_registration(list_img, "vectorfield", return_images=True)
    >>> # Display the registration on first result as multi-channel image:
    >>> from os.path import split
    >>> from timagetk.visu.stack import channels_stack_browser
    >>> channels_stack_browser(list_img, [split(f)[1] for f in list_files], ['red', 'green', 'blue'], title="Fusion on first - Non-linear registration")

    >>> # Register all other images on the first of the list:
    >>> trsf_list, list_img = reference_registration(list_files, "vectorfield", return_images=True)
    """
    return_img = kwargs.pop('return_images', False)
    ref_idx = kwargs.pop('ref_index', 0)

    # --- registration on reference
    list_res_trsf = []
    list_res_img = []
    ref_img = list_images.pop(ref_idx)
    for idx, sp_img in enumerate(list_images):
        print(REG_MSG.format(method, idx, ref_idx))
        trsf_rig, res_rig = SpatialRegistrationPlugin(sp_img, ref_img,
                                                      method=method, **kwargs).register()
        # --- add computed transfo to returned list:
        list_res_trsf.append(trsf_rig)
        # --- add image to returned list if required, else delete:
        if return_img:
            list_res_img.append(res_rig)
        else:
            del res_rig

    if return_img:
        return list_res_trsf, [ref_img] + list_res_img
    else:
        return list_res_trsf


def consecutive_registration(list_images, method, **kwargs):
    """Consecutive registration of each images onto the next one in the list.

    Parameters
    ----------
    list_images : list of SpatialImage
        list of time sorted SpatialImage
    method : str
        used method, by default 'rigid'

    Other Parameters
    ----------------
    return_images : bool, optional
        if ``True`` (default ``False``), the registered image list is returned too

    Returns
    -------
    list_compo_trsf : list of Trsf
        list of composed transformation
    list_res_img : list of SpatialImage
        list of images after rigid registration

    Example
    -------
    >>> from timagetk.io.util import shared_data
    >>> from timagetk.io import imread
    >>> from timagetk.tasks.registration import consecutive_registration
    >>> times = [0, 1, 2]
    >>> list_fnames = ['p58-t{}_INT_down_interp_2x.inr.gz'.format(time) for time in times]
    >>> list_images = [imread(shared_data(fname)) for fname in list_fnames]

    >>> # Compute rigid registration of images in `list_image`:
    >>> list_trsf, list_imgs = consecutive_registration_rigid(list_images, return_images=True)

    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> grayscale_imshow(list_imgs,"Consecutive RIGID registration",list_fnames,val_range=[0, 255])

    """
    return_img = kwargs.pop('return_images', False)

    # --- consecutive pairwise registration
    list_res_trsf = []
    list_res_img = []
    for idx, sp_img in enumerate(list_images):
        if idx < len(list_images) - 1:
            ref_img = list_images[idx + 1]
            # --- rigid registration:
            print(REG_MSG.format(method, idx, idx + 1))
            trsf_rig, res_rig = SpatialRegistrationPlugin(sp_img, ref_img,
                                                          method=method,
                                                          **kwargs).register()
            # --- add computed transfo to returned list:
            list_res_trsf.append(trsf_rig)
            # --- add image to returned list if required, else delete:
            if return_img:
                list_res_img.append(res_rig)
            else:
                del res_rig
        else:
            if return_img:
                # add last reference image
                list_res_img.append(list_images[-1])

    if return_img:
        return list_res_trsf, list_res_img
    else:
        return list_res_trsf


def compose_to_last(consecutive_trsf, template_img):
    """Compose a sequence of consecutive transformations.

    Parameters
    ----------
    consecutive_trsf : list of Trsf
        temporal sequence of consecutive transformations, e.g. [T(0, 1), T(1, 2), ..., T(N-1,N)]
    template_img : SpatialImage
        template image used as reference to give the shape and voxelsize of the
        image, should be the last image used when computing consecutive
        registration

    Returns
    -------
    list_compo_trsf : list of Trsf
        list of composed transformations, e.g. [T(0, N), T(1, N), ..., T(N-1,N)]

    See Also
    --------
    timagetk.algorithms.trsf.compose_trsf: the transformations composition algorithm

    Example
    -------
import timagetk.tasks.consecutive_registration    >>> from timagetk.io.util import shared_data
    >>> from timagetk.io import imread
    >>> from timagetk.tasks.consecutive_registration import consecutive_registration
    >>> from timagetk.tasks.consecutive_registration.sequence_registration import compose_to_last
    >>> times = [0, 1, 2]
    >>> list_fnames = ['p58-t{}_INT_down_interp_2x.inr.gz'.format(time) for time in times]
    >>> list_images = [imread(shared_data(fname)) for fname in list_fnames]
    >>> # - Performs consecutive rigid registration:
    >>> list_trsf = consecutive_registration(list_images, method='rigid')
    >>> list_compo_trsf = compose_to_last(list_trsf, list_images[-1])

    """
    n_imgs = len(consecutive_trsf) + 1
    # --- Transformations composition to obtain T(t_n -> t_N):
    list_compo_trsf, list_res_img = [], []
    for ind in range(len(consecutive_trsf)):
        if ind < len(consecutive_trsf) - 1:
            msg = "\nTransformations composition to get trsf_{}/{}..."
            print(msg.format(ind, n_imgs - 1))
            # matrix multiplication
            comp_trsf = compose_trsf(consecutive_trsf[ind:],
                                     template_img=template_img)
            list_compo_trsf.append(comp_trsf)
            print("Done.\n")
        else:
            # 't_N-1'to't_N' transformation does not need composition!
            list_compo_trsf.append(consecutive_trsf[-1])

    return list_compo_trsf


class MultiAngleRegistrationPlugin(MultiAngleImage):
    """Register MultiAngleImage image to fuse their information.

    """

    def __init__(self, multiangle):
        """Constructor.

        Parameters
        ----------
        multiangle : MuliAngle
            Multi-angles image to fuse.

        """
        super().__init__(multiangle)

    def register_on_reference(self, ref_id=0, method='rigid', **kwargs):
        """Register all angles on reference image.

        Parameters
        ----------
        ref_id : int, optional
            Angle id to use as reference image.
        method: {'rigid', 'affine', 'vectorfield'}
            Registration method to use.

        Returns
        -------
        SpatialImage
            The image obtained after registration and averaging of angle images.

        Notes
        -----
        The transformations "affine" & "vectorfield" do not have the 'rigid'
        part, this mean that you may have to compose them.

        """
        # Starts by performing all rigid registrations if affine or vectorfield is required
        # This rigid transformation will be used as initialization later...
        if method in ["affine", "vectorfield"]:
            self.register_on_reference(ref_id, method='rigid', **kwargs)

        # Get the reference image:
        ref_img = self.get_angle_image(ref_id)
        # Defines the index of floating image:
        flo_index = list(range(self.nb_angles))
        flo_index.remove(ref_id)
        # Registration loop:
        for flo_id in flo_index:
            # Get the floating image:
            flo_img = self.get_angle_image(flo_id)
            # Use the 'rigid' trsf to initialize blockmatching:
            if method in ["affine", "vectorfield"]:
                init_trsf = self.trsfs['rigid'][0][(flo_id, ref_id)]
            else:
                init_trsf = None
            trsf = blockmatching(flo_img, ref_img, method=method,
                                 left_trsf=init_trsf, **kwargs)
            trsf_fname = self._get_trsf_fname(flo_id, ref_id, 0, method)
            trsf.write(trsf_fname)
            del trsf
            self.trsfs[method][0][(flo_id, ref_id)] = trsf_fname

        # Average the transformations:
