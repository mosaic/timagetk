#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Sophie Ribes <sophie.ribes@inria.fr>
#
#       File contributor(s):
#           Guillaume Baty <guillaume.baty@inria.fr>
#           Gregoire Malandain <gregoire.malandain@inria.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

"""
This module contain a generic implementation of several cell filtering algorithms.
"""
import logging

from timagetk.algorithms.morphology import label_filtering
from timagetk.third_party.vt_parser import DEF_RADIUS, DEF_ITERS
from timagetk.third_party.vt_parser import cellfilter_kwargs
from timagetk.components.spatial_image import SpatialImage
from timagetk.components.image import _input_img_check
from timagetk.util import _method_check

__all__ = ["cell_filtering"]

CELL_FILTER_METHODS = [
    "erosion", "dilation", "opening", "closing",
    "oc_alternate_sequential_filter", "co_alternate_sequential_filter",
    "coc_alternate_sequential_filter", "oco_alternate_sequential_filter"
]
DEF_CF_METHOD = "erosion"

def cell_filtering(image, method=DEF_CF_METHOD, **kwargs):
    """Cell filtering plugin.

    Valid ``method`` values are:

      - **erosion**
      - **dilation**
      - **opening**
      - **closing**
      - **oc_alternate_sequential_filter**
      - **co_alternate_sequential_filter**
      - **coc_alternate_sequential_filter**
      - **oco_alternate_sequential_filter**

    Parameters
    ----------
    image : SpatialImage
        Labelled image to filter.
    method : str, optional
        Filtering method to use on labelled images, "erosion" by default.

    Other Parameters
    ----------------
    radius : int, optional
        Radius of the structuring element, default is 1
    connectivity : int, optional
        Use it to override the default "sphere" parameter for the structuring element, equivalent to "connectivity=18".
    iterations : int, optional
        number of time to apply the morphological operation, default is 1.
    max_radius : int, optional
        maximum value reached by "radius", starting from 1, when performing "sequential filtering" method
        Defaults to ``3``.

    Returns
    -------
    SpatialImage
        transformed image with its metadata

    Raises
    ------
    TypeError
        if the given ``image`` is not a ``SpatialImage``
    NotImplementedError
        if the given ``method`` is not defined in the list of possible methods ``CELL_FILTER_METHODS``

    Example
    -------
    >>> from timagetk.old_plugins import cell_filtering
    >>> from timagetk.io.util import shared_data
    >>> from timagetk.io import imread
    >>> image_path = shared_data("time_0_cut.inr")
    >>> image = imread(image_path)
    >>> dilation_image = cell_filtering(image, radius=2, iterations=2, method="dilation")
    >>> oc_asf_image = cell_filtering(image, max_radius=3, method="oc_alternate_sequential_filter")
    """
    # - Assert the "image" is a SpatialImage instance:
    msg = _input_img_check(image, var_name='labelled image')
    if msg:
        logging.warning(msg)

    # - Set / check given method is valid:
    method = _method_check(method, CELL_FILTER_METHODS, DEF_CF_METHOD)

    max_radius_val = kwargs.pop("max_radius", 3)
    if method == "erosion":
        return cell_filtering_erosion(image, **kwargs)
    elif method == "dilation":
        return cell_filtering_dilation(image, **kwargs)
    elif method == "opening":
        return cell_filtering_opening(image, **kwargs)
    elif method == "closing":
        return cell_filtering_closing(image, **kwargs)
    elif method == "oc_alternate_sequential_filter":
        return cell_filtering_oc_alternate_sequential_filter(image,
                                                             max_radius_val,
                                                             **kwargs)
    elif method == "co_alternate_sequential_filter":
        return cell_filtering_co_alternate_sequential_filter(image,
                                                             max_radius_val,
                                                             **kwargs)
    elif method == "coc_alternate_sequential_filter":
        return cell_filtering_coc_alternate_sequential_filter(image,
                                                              max_radius_val,
                                                              **kwargs)
    elif method == "oco_alternate_sequential_filter":
        return cell_filtering_oco_alternate_sequential_filter(image,
                                                              max_radius_val,
                                                              **kwargs)
    else:
        msg = "The required method '{}' is not implemented!"
        raise NotImplementedError(msg.format(method))


def cell_filtering_erosion(image, radius=DEF_RADIUS, iterations=DEF_ITERS,
                           **kwargs):
    """Erosion on segmented image.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    radius : int
        radius to use for structuring element, default is 1
    iterations : int
        number of iterations to performs with structuring element, default is 1

    Returns
    -------
    SpatialImage
        transformed image with its metadata
    """
    params = "-erosion"
    params += cellfilter_kwargs(radius=radius, iterations=iterations, **kwargs)
    out_img = label_filtering(image, param_str_2=params)
    # add2md(out_img)
    return out_img


def cell_filtering_dilation(image, radius=DEF_RADIUS, iterations=DEF_ITERS,
                            **kwargs):
    """Dilation on segmented image.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    radius : int
        radius to use for structuring element, default is 1
    iterations : int
        number of iterations to performs with structuring element, default is 1

    Returns
    -------
    SpatialImage
        transformed image with its metadata
    """
    params = "-dilation"
    params += cellfilter_kwargs(radius=radius, iterations=iterations, **kwargs)
    out_img = label_filtering(image, param_str_2=params)
    # add2md(out_img)
    return out_img


def cell_filtering_opening(image, radius=DEF_RADIUS, iterations=DEF_ITERS,
                           **kwargs):
    """Opening on segmented image.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    radius : int
        radius to use for structuring element, default is 1
    iterations : int
        number of iterations to performs with structuring element, default is 1

    Returns
    -------
    SpatialImage
        transformed image with its metadata
    """
    params = "-opening"
    params += cellfilter_kwargs(radius=radius, iterations=iterations, **kwargs)
    out_img = label_filtering(image, param_str_2=params)
    # add2md(out_img)
    return out_img


def cell_filtering_closing(image, radius=DEF_RADIUS, iterations=DEF_ITERS,
                           **kwargs):
    """Closing on segmented image.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    radius : int
        radius to use for structuring element, default is 1
    iterations : int
        number of iterations to performs with structuring element, default is 1

    Returns
    -------
    SpatialImage
        transformed image with its metadata
    """
    params = "-closing"
    params += cellfilter_kwargs(radius=radius, iterations=iterations, **kwargs)
    out_img = label_filtering(image, param_str_2=params)
    # add2md(out_img)
    return out_img


def cell_filtering_oc_alternate_sequential_filter(image, max_radius=None,
                                                  **kwargs):
    """Opening Closing alternate sequential filter on segmented image.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    max_radius : int, optional
        maximum radius to use, starting at 1

    Returns
    -------
    SpatialImage
        transformed image and its metadata
    """
    if max_radius is None:
        max_radius = 1

    max_radius = abs(int(max_radius))
    sizes = range(1, max_radius + 1)
    out_img = image
    for size in sizes:
        out_img = cell_filtering_opening(out_img, radius=size, **kwargs)
        out_img = cell_filtering_closing(out_img, radius=size, **kwargs)

    # add2md(out_img)
    return out_img


def cell_filtering_co_alternate_sequential_filter(image, max_radius=None,
                                                  **kwargs):
    """Closing Opening alternate sequential filter on segmented image.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    max_radius : int, optional
        maximum radius to use, starting at 1

    Returns
    -------
    SpatialImage
        transformed image and its metadata
    """
    if max_radius is None:
        max_radius = 1

    max_radius = abs(int(max_radius))
    sizes = range(1, max_radius + 1)
    out_img = image
    for size in sizes:
        out_img = cell_filtering_closing(out_img, radius=size, **kwargs)
        out_img = cell_filtering_opening(out_img, radius=size, **kwargs)

    # add2md(out_img)
    return out_img


def cell_filtering_coc_alternate_sequential_filter(image, max_radius=None,
                                                   **kwargs):
    """Closing Opening Closing alternate sequential filter on segmented image.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    max_radius : int, optional
        maximum radius to use, starting at 1

    Returns
    -------
    SpatialImage
        transformed image and its metadata
    """
    if max_radius is None:
        max_radius = 1

    max_radius = abs(int(max_radius))
    sizes = range(1, max_radius + 1)
    out_img = image
    for size in sizes:
        out_img = cell_filtering_closing(out_img, radius=size, **kwargs)
        out_img = cell_filtering_opening(out_img, radius=size, **kwargs)
        out_img = cell_filtering_closing(out_img, radius=size, **kwargs)

    # add2md(out_img)
    return out_img


def cell_filtering_oco_alternate_sequential_filter(image, max_radius=None,
                                                   **kwargs):
    """Opening Closing Opening alternate sequential filter on segmented image.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    max_radius : int, optional
        maximum radius to use, starting at 1

    Returns
    -------
    SpatialImage
        transformed image and its metadata
    """
    if max_radius is None:
        max_radius = 1

    max_radius = abs(int(max_radius))
    sizes = range(1, max_radius + 1)
    out_img = image
    for size in sizes:
        out_img = cell_filtering_opening(out_img, radius=size, **kwargs)
        out_img = cell_filtering_closing(out_img, radius=size, **kwargs)
        out_img = cell_filtering_opening(out_img, radius=size, **kwargs)

    # add2md(out_img)
    return out_img
