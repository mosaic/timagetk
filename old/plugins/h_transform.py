#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Sophie Ribes <sophie.ribes@inria.fr>
#
#       File contributor(s):
#           Guillaume Baty <guillaume.baty@inria.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

"""
This module contains a generic implementation of several h_transform algorithms.
"""
import logging

from timagetk.algorithms import regionalext
from timagetk.third_party.vt_parser import general_kwargs
from timagetk.third_party.vt_parser import parallel_kwargs
from timagetk.components.spatial_image import SpatialImage
from timagetk.components.image import _input_img_check
from timagetk.util import _method_check

__all__ = ['h_transform']

H_TRANSFORM_METHODS = ['min', 'max']
DEFAULT_METHOD = 0  # index of the default method in H_TRANSFORM_METHODS
DEF_H = 1  # default h-transform value
DEF_CONNECT2D = 8
DEF_CONNECT3D = 26


def h_transform(image, method=None, **kwargs):
    """Height transform plugin for regional extrema detection.

    Valid ``method`` values are:

      - **min**, for detection of connexe regional minima
      - **max**, for detection of connexe regional maxima

    Parameters
    ----------
    image : SpatialImage
        Input image to transform
    method : str, optional
        Used method, by default 'min'

    Other Parameters
    ----------------
    h : int, optional
        Height value to use in transformation, default is 1
    connectivity : int, optional
        connectivity to consider a component "connected", should be in [4, 6, 8, 10, 18, 26], by default 26

    Returns
    -------
    SpatialImage
        transformed image with its metadata

    Raises
    ------
    TypeError
        if the given ``image`` is not a ``SpatialImage`` instance
    NotImplementedError
        if the given ``method`` is not defined in the list of possible methods
        ``H_TRANSFORM_METHODS``

    Notes
    -----
    A regional minimum, resp. maximum, *M* of an image *I* at elevation *t*
    is a connected component of pixels (or voxels) with the value *t* whose external
    boundary pixels (or voxels) have a value strictly greater, resp. less, than *t* [#]_.
    The result image is then a binary image, where 1 indicate a regional minimum,
    resp. maximum.

    Providing a contrast criterion *h* allows to remove the regional minima,
    resp. maxima, whose depth is higher, resp. lower, to this threshold level *h*.
    It is then called a *h-minima* transformation, resp. *h-maxima*.
    The result image is then a height image, i.e. the pixels (or voxels) values
    correspond to the depth of the regional extrema.

    Connectivity is among the 4-, 6-, 8-, 18-, 26-neighborhoods. 4 and 8 are for
    2D images, the others for 3D images.

    See Also
    --------
    timagetk.algorithms.regionalext

    References
    ----------
    .. [#] *Morphological Image Analysis: Principles and Applications*, P. Soille. `Link <https://books.google.fr/books?id=ZFzxCAAAQBAJ&lpg=PA201&ots=-p8--YG2em&dq=regional%20extrema%20image%20analysis&hl=fr&pg=PA201#v=onepage&q=regional%20extrema%20image%20analysis&f=false>`_

    Example
    -------
    >>> from timagetk.old_plugins import h_transform
    >>> import numpy as np
    >>> from timagetk.io.util import shared_data
    >>> from timagetk.io import imread
    >>> from timagetk.algorithms.regionalext import regionalext
    >>> img_path = shared_data('time_0_cut.inr')
    >>> image = imread(img_path)

    >>> # - Get a z-slice:
    >>> zslice = 10
    >>> img10 = image.get_slice(zslice, 'z')

    >>> # - H-minima transformation for range of thresholds and 8-connexe pixels:
    >>> hmin = 25
    >>> extmin_imgs = regionalext(img10, param_str_1='-minima -connectivity 8', param_str_2='-h {}'.format(hmin))
    >>> reg_min_img = h_transform(img10, h=hmin, method='min', connectivity=8)
    >>> extmin_imgs.equal_array(reg_min_img)  # Should be True!

    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> grayscale_imshow([img10, extmin_imgs, reg_min_img],title=["Original", "regionalext", "h_transform"],cmaps=['gray', 'viridis', 'viridis'])

    >>> # - H-maxima transformation on 3D SpatialImage:
    >>> reg_max_image = h_transform(image, h=3, method='max')

    """
    # - Assert the 'image' is a SpatialImage instance:
    msg = _input_img_check(image, var_name='intensity image')
    if msg:
        logging.warning(msg)

    # - Set method if None and check it is a valid method:
    method = _method_check(method, H_TRANSFORM_METHODS, DEFAULT_METHOD)

    if 'min' in method:
        return h_min_transform(image, **kwargs)
    else:
        return h_max_transform(image, **kwargs)


def _ht_kwargs(**kwargs):
    """
    Set parameters default values and make sure they are of the right type.
    """
    str_param = ""
    # - By default 'h' is equal to DEF_H:
    h = abs(int(kwargs.get('h', DEF_H)))
    str_param += ' -h %d' % h

    conn = kwargs.get('connectivity', None)
    if conn is not None:
        str_param += '-connectivity %d' % conn
    else:
        ndim = kwargs.get('ndim', 3)
        if ndim == 3:
            str_param += '-connectivity %d' % DEF_CONNECT3D
        elif ndim == 2:
            str_param += '-connectivity %d' % DEF_CONNECT2D
        else:
            raise ValueError("Image dimensions shoud be 2 or 3, got {}!".format(ndim))

    # - Parse general kwargs:
    str_param += general_kwargs(**kwargs)
    # - Parse parallelism kwargs:
    str_param += parallel_kwargs(**kwargs)

    return str_param


def h_min_transform(image, h=DEF_H, **kwargs):
    """Minimum h-extrema transformation

    Detect regional minima with a depth lower than given height *h*.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    h : int, optional
        height value to use in transformation, default is 1

    Other Parameters
    ----------------
    connectivity : int, optional
        connectivity to consider a component "connected", should be in
        [4, 6, 8, 10, 18, 26], by default 26

    Returns
    -------
    SpatialImage
        transformed image with its metadata
    """
    params = _ht_kwargs(h=h, ndim=image.ndim, **kwargs)
    out_img = regionalext(image, param_str_1='-minima', param_str_2=params)
    # add2md(out_img)
    return out_img


def h_max_transform(image, h=DEF_H, **kwargs):
    """Maximum h-extrema transformation

    Detect regional maxima with a depth higher than given height *h*.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    h : int, optional
        height value to use in transformation, default is 1

    Other Parameters
    ----------------
    connectivity : int, optional
        connectivity to consider a component "connected", should be in
        [4, 6, 8, 10, 18, 26], by default 26

    Returns
    -------
    SpatialImage
        transformed image with its metadata
    """
    params = _ht_kwargs(h=h, ndim=image.ndim, **kwargs)
    out_img = regionalext(image, param_str_1='-maxima', param_str_2=params)
    # add2md(out_img)
    return out_img
