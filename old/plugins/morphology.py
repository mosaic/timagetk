#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Sophie Ribes <sophie.ribes@inria.fr>
#
#       File contributor(s):
#           Guillaume Baty <guillaume.baty@inria.fr>
#           Gregoire Malandain <gregoire.malandain@inria.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

"""
This module contain a generic implementation of several grayscale morphology algorithms.
"""
import logging

from timagetk.algorithms import morphology
from timagetk.third_party.vt_parser import DEF_RADIUS, DEF_ITERS, \
    morphology_kwargs, MORPHOLOGY_METHODS, DEF_MORPHO_METHOD
from timagetk.components.spatial_image import SpatialImage
from timagetk.components.image import _input_img_check
from timagetk.util import _method_check

# __all__ = ['morphology']


def morphology(image, method=None, **kwargs):
    """Grayscale morphology plugin.

    Valid ``method`` values are:

      - **erosion**
      - **dilation**
      - **opening**
      - **closing**
      - **morpho_gradient**
      - **contrast**
      - **hat_transform**
      - **inverse_hat_transform**
      - **oc_alternate_sequential_filter**
      - **co_alternate_sequential_filter**
      - **coc_alternate_sequential_filter**
      - **oco_alternate_sequential_filter**

    Parameters
    ----------
    image : SpatialImage
        input image to modify, should be a 'grayscale' (intensity) image
    method : str, optional
        used method, by default 'erosion'

    Other Parameters
    ----------------
    radius : int, optional
        radius of the structuring element, default is 1
    iterations : int, optional
        number of time to apply the morphological operation, default is 1
    max_radius : int, optional
        maximum value reached by 'radius', starting from 1, when performing
        'sequential filtering' methods, default is 3
    connectivity : int, optional
        use it to override the default 'sphere' parameter for the structuring
        element, equivalent to 'connectivity=18'

    Returns
    -------
    SpatialImage
        image and metadata

    Raises
    ------
    TypeError
        if the given ``image`` is not a ``SpatialImage``
    NotImplementedError
        if the given ``method`` is not defined in the list of possible methods ``MORPHOLOGY_METHODS``

    Example
    -------
    >>> from timagetk.io.util import shared_data
    >>> from timagetk.io import imread
    >>> from timagetk.plugins import morphology
    >>> image_path = shared_data('time_0_cut.inr')
    >>> image = imread(image_path)
    >>> dilation_image = morphology(image, radius=2, iterations=2, method='dilation')
    >>> oc_asf_image = morphology(image, max_radius=3, method='oc_alternate_sequential_filter')
    """
    # - Assert the 'image' is a SpatialImage instance:
    msg = _input_img_check(image, var_name='intensity image')
    if msg:
        logging.warning(msg)

    # - Set method if None and check it is a valid method:
    method = _method_check(method, MORPHOLOGY_METHODS, DEF_MORPHO_METHOD)

    max_radius_val = kwargs.pop('max_radius', 3)
    if method == 'erosion':
        return morphology_erosion(image, **kwargs)
    elif method == 'dilation':
        return morphology_dilation(image, **kwargs)
    elif method == 'opening':
        return morphology_opening(image, **kwargs)
    elif method == 'closing':
        return morphology_closing(image, **kwargs)
    elif method == 'morpho_gradient':
        return morphology_gradient(image, **kwargs)
    elif method == 'contrast':
        return morphology_contrast(image, **kwargs)
    elif method == 'hat_transform':
        return morphology_hat_transform(image, **kwargs)
    elif method == 'inverse_hat_transform':
        return morphology_inverse_hat_transform(image, **kwargs)
    elif method == 'oc_alternate_sequential_filter':
        return morphology_oc_alternate_sequential_filter(image,
                                                         max_radius_val,
                                                         **kwargs)
    elif method == 'co_alternate_sequential_filter':
        return morphology_co_alternate_sequential_filter(image,
                                                         max_radius_val,
                                                         **kwargs)
    elif method == 'coc_alternate_sequential_filter':
        return morphology_coc_alternate_sequential_filter(image,
                                                          max_radius_val,
                                                          **kwargs)
    elif method == 'oco_alternate_sequential_filter':
        return morphology_oco_alternate_sequential_filter(image,
                                                          max_radius_val,
                                                          **kwargs)
    else:
        msg = "The required method '{}' is not implemented!"
        raise NotImplementedError(msg.format(method))


def morphology_erosion(image, radius=DEF_RADIUS, iterations=DEF_ITERS,
                       **kwargs):
    """Morpholocial erosion on grayscale image.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    radius : int, optional
        radius to use for structuring element, default is 1
    iterations : int, optional
        number of iterations to performs with structuring element, default is 1

    Returns
    -------
    SpatialImage
        transformed image with its metadata
    """
    params = '-erosion'
    params += morphology_kwargs(radius=radius, iterations=iterations, **kwargs)
    out_img = morphology(image, param_str_2=params)
    # add2md(out_img)
    return out_img


def morphology_dilation(image, radius=DEF_RADIUS, iterations=DEF_ITERS,
                        **kwargs):
    """Morpholocial dilation on grayscale image.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    radius : int, optional
        radius to use for structuring element, default is 1
    iterations : int, optional
        number of iterations to performs with structuring element, default is 1

    Returns
    -------
    SpatialImage
        transformed image with its metadata
    """
    params = '-dilation'
    params += morphology_kwargs(radius=radius, iterations=iterations, **kwargs)
    out_img = morphology(image, param_str_2=params)
    # add2md(out_img)
    return out_img


def morphology_opening(image, radius=DEF_RADIUS, iterations=DEF_ITERS,
                       **kwargs):
    """Morpholocial opening on grayscale image.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    radius : int, optional
        radius to use for structuring element, default is 1
    iterations : int, optional
        number of iterations to performs with structuring element, default is 1

    Returns
    -------
    SpatialImage
        transformed image with its metadata
    """
    params = '-opening'
    params += morphology_kwargs(radius=radius, iterations=iterations, **kwargs)
    out_img = morphology(image, param_str_2=params)
    # add2md(out_img)
    return out_img


def morphology_closing(image, radius=DEF_RADIUS, iterations=DEF_ITERS,
                       **kwargs):
    """Closing on grayscale image.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    radius : int, optional
        radius to use for structuring element, default is 1
    iterations : int, optional
        number of iterations to performs with structuring element, default is 1

    Returns
    -------
    SpatialImage
        transformed image with its metadata
    """
    params = '-closing'
    params += morphology_kwargs(radius=radius, iterations=iterations, **kwargs)
    out_img = morphology(image, param_str_2=params)
    # add2md(out_img)
    return out_img


def morphology_hat_transform(image, radius=DEF_RADIUS,
                             iterations=DEF_ITERS, **kwargs):
    """Hat transform on grayscale image.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    radius : int, optional
        radius to use for structuring element, default is 1
    iterations : int, optional
        number of iterations to performs with structuring element, default is 1

    Returns
    -------
    SpatialImage
        transformed image with its metadata
    """
    params = '-closinghat'
    params += morphology_kwargs(radius=radius, iterations=iterations, **kwargs)
    out_img = morphology(image, param_str_2=params)
    # add2md(out_img)
    return out_img


def morphology_inverse_hat_transform(image, radius=DEF_RADIUS,
                                     iterations=DEF_ITERS,
                                     **kwargs):
    """Inverse hat transform on grayscale image.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    radius : int, optional
        radius to use for structuring element, default is 1
    iterations : int, optional
        number of iterations to performs with structuring element, default is 1

    Returns
    -------
    SpatialImage
        transformed image with its metadata
    """
    params = '-openinghat'
    params += morphology_kwargs(radius=radius, iterations=iterations, **kwargs)
    out_img = morphology(image, param_str_2=params)
    # add2md(out_img)
    return out_img


def morphology_gradient(image, radius=DEF_RADIUS, iterations=DEF_ITERS,
                        **kwargs):
    """Morphological gradient on grayscale image.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    radius : int, optional
        radius to use for structuring element, default is 1
    iterations : int, optional
        number of iterations to performs with structuring element, default is 1

    Returns
    -------
    SpatialImage
        transformed image with its metadata
    """
    params = '-gradient'
    params += morphology_kwargs(radius=radius, iterations=iterations, **kwargs)
    out_img = morphology(image, param_str_2=params)
    # add2md(out_img)
    return out_img


def morphology_contrast(image, radius=DEF_RADIUS, iterations=DEF_ITERS,
                        **kwargs):
    """Contrast enhancement on grayscale image.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    radius : int, optional
        radius to use for structuring element, default is 1
    iterations : int, optional
        number of iterations to performs with structuring element, default is 1

    Returns
    -------
    SpatialImage
        transformed image with its metadata
    """
    params = '-contrast'
    params += morphology_kwargs(radius=radius, iterations=iterations, **kwargs)
    out_img = morphology(image, param_str_2=params)
    # add2md(out_img)
    return out_img


def morphology_oc_alternate_sequential_filter(image, max_radius=None,
                                              **kwargs):
    """Opening Closing alternate sequential filter on grayscale image.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    max_radius : int, optional
        maximum spherical element radius to use, starts at 1, default is 1

    Returns
    -------
    SpatialImage
        transformed image and its metadata
    """
    if max_radius is None:
        max_radius = 1

    max_radius = abs(int(max_radius))
    sizes = range(1, max_radius + 1)
    out_img = image
    for size in sizes:
        print("\n# - Alternate sequential filter - element size: {}".format(size))
        out_img = morphology_opening(out_img, radius=size, **kwargs)
        out_img = morphology_closing(out_img, radius=size, **kwargs)

    # add2md(out_img)
    return out_img


def morphology_co_alternate_sequential_filter(image, max_radius=None,
                                              **kwargs):
    """Closing Opening alternate sequential filter on grayscale image.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    max_radius : int, optional
        maximum spherical element radius to use, starts at 1, default is 1

    Returns
    -------
    SpatialImage
        transformed image and its metadata

    """
    if max_radius is None:
        max_radius = 1

    max_radius = abs(int(max_radius))
    sizes = range(1, max_radius + 1)
    out_img = image
    for size in sizes:
        print("\n# - Alternate sequential filter - element size: {}".format(size))
        out_img = morphology_closing(out_img, radius=size, **kwargs)
        out_img = morphology_opening(out_img, radius=size, **kwargs)

    # add2md(out_img)
    return out_img


def morphology_coc_alternate_sequential_filter(image, max_radius=None,
                                               **kwargs):
    """Closing Opening Closing alternate sequential filter on grayscale image.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    max_radius : int, optional
        maximum spherical element radius to use, starts at 1, default is 1

    Returns
    -------
    SpatialImage
        transformed image and its metadata
    """
    if max_radius is None:
        max_radius = 1

    max_radius = abs(int(max_radius))
    sizes = range(1, max_radius + 1)
    out_img = image
    for size in sizes:
        print("\n# - Alternate sequential filter - element size: {}".format(size))
        out_img = morphology_closing(out_img, radius=size, **kwargs)
        out_img = morphology_opening(out_img, radius=size, **kwargs)
        out_img = morphology_closing(out_img, radius=size, **kwargs)

    # add2md(out_img)
    return out_img


def morphology_oco_alternate_sequential_filter(image, max_radius=None,
                                               **kwargs):
    """Opening Closing Opening alternate sequential filter on grayscale image.

    Parameters
    ----------
    image : SpatialImage
        input image to transform
    max_radius : int, optional
        maximum spherical element radius to use, starts at 1, default is 1

    Returns
    -------
    SpatialImage
        transformed image and its metadata
    """
    if max_radius is None:
        max_radius = 1

    max_radius = abs(int(max_radius))
    sizes = range(1, max_radius + 1)
    out_img = image
    for size in sizes:
        print("\n# - Alternate sequential filter - element size: {}".format(size))
        out_img = morphology_opening(out_img, radius=size, **kwargs)
        out_img = morphology_closing(out_img, radius=size, **kwargs)
        out_img = morphology_opening(out_img, radius=size, **kwargs)

    # add2md(out_img)
    return out_img
