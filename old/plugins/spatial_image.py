#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

"""Plugin dedicated to SpatialImage.

"""

import numpy as np
from timagetk.components.spatial_image import DICT_TYPES
from timagetk import SpatialImage
from timagetk.util import check_type
from timagetk.util import type_to_range

# - Array equality testing methods:
EQ_METHODS = ['max_error', 'cum_error']
# - Array equality error:
EPS = 1e-9


def equal_image_array(img1, img2, method='max_error', error=EPS):
    """Compare two SpatialImage arrays.

    Parameters
    ----------
    img1 : SpatialImage
        The first image to use for comparison.
    img2 : SpatialImage
        The second image to use for comparison.
    method : str in {'max_error', 'cum_error'}, optional
            type of "error measurement", choose among:
              - "max_error": max difference accepted for a given pixel
              - "cum_error": max cumulative (sum) difference for the whole array
    error : float, optional
            maximum difference accepted between the two arrays, should be
            strictly inferior to this value to return True, default: EPS

    Returns
    -------
    bool
        Returns ``True`` if the ``SpatialImage``s are the same, else ``False``.

    See Also
    --------
    timagetk.plugins.equal_spatial_image: similar to this function but also
    compare metadata dictionary.

    Example
    -------
    >>> from timagetk.plugins.spatial_image import equal_image_array
    >>> from timagetk.array_util import random_spatial_image
    >>> img1 = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
    >>> img2 = random_spatial_image((3, 4, 4), voxelsize=[1., 0.5, 0.5], dtype='uint8')
    >>> equal_image_array(img1, img2, method='max_error', error=1e-09)
    Given images have different shapes: (3, 5, 5) != (3, 4, 4).
    False
    >>> img2 = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
    >>> equal_image_array(img1, img2, method='max_error', error=1e-09)
    Max difference between arrays is greater than '1e-09'.
    False

    """
    # Make sure we have SpatialImage instances:
    check_type(img1, 'img1', SpatialImage)
    check_type(img2, 'img2', SpatialImage)
    # Make sure the comparison method is defined:
    try:
        assert method in EQ_METHODS
    except AssertionError:
        msg = "Unknown method '{}', should be in {}."
        raise ValueError(msg.format(method, EQ_METHODS))

    # - Starts by testing the shapes are equal:
    if img1.shape != img2.shape:
        msg = f"Given images have different shapes: {img1.shape} != {img2.shape}."
        print(msg)
        return False

    # - Test array equality:
    img1_type = str(img1.dtype)
    if img1_type.startswith('u'):
        # unsigned case is problematic for 'np.subtract'
        tmp_type = DICT_TYPES[img1_type[1:]]
    else:
        tmp_type = img1_type
    # - Compute the difference between the two arrays:
    out_img = np.abs(np.subtract(img1, img2).astype(tmp_type)).astype(img1_type)
    # - Try to find non-null values in this array:
    non_null_idx = np.nonzero(out_img)
    if len(non_null_idx[0]) != 0:
        non_null = out_img[non_null_idx]
        if method == 'max_error':
            # max difference accepted for a given pixel:
            equal = bool(np.max(non_null) < error)
        else:
            # max cumulative (sum) difference for the whole array:
            equal = bool(np.sum(non_null) < error)
    else:
        equal = True

    if not equal:
        m = 'Max' if method == 'max_error' else 'Cumulative'
        print(f"{m} difference between arrays is greater than '{error}'.")

    return equal


def equal_spatial_image(img1, img2, method='max_error', error=EPS):
    """Compare two SpatialImage arrays and their metadata dictionaries.

    Parameters
    ----------
    img1 : SpatialImage
        The first image to use for comparison.
    img2 : SpatialImage
        The second image to use for comparison.
    method : str in {'max_error', 'cum_error'}, optional
            type of "error measurement", choose among:
              - "max_error": max difference accepted for a given pixel
              - "cum_error": max cumulative (sum) difference for the whole array
    error : float, optional
            maximum difference accepted between the two arrays, should be
            strictly inferior to this value to return True, default: EPS

    Returns
    -------
    bool
        Returns ``True`` if the ``SpatialImage``s are the same, else ``False``.

    See Also
    --------
    timagetk.plugins.equal_image_array: similar to this function but only
    compare the arrays.

    Example
    -------
    >>> from timagetk.plugins.spatial_image import equal_spatial_image
    >>> from timagetk.array_util import random_spatial_image
    >>> img1 = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
    >>> img2 = random_spatial_image((3, 4, 4), voxelsize=[1., 0.5, 0.5], dtype='uint8')
    >>> equal_spatial_image(img1, img2, method='max_error', error=1e-09)
    Given images have different shapes: (3, 5, 5) != (3, 4, 4).
    SpatialImages metadata are different.
    False
    >>> img2 = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
    >>> equal_spatial_image(img1, img2, method='max_error', error=1e-09)
    Max difference between arrays is greater than '1e-09'.
    False

    """
    equal = False
    # - Test array equality:
    t_arr = equal_image_array(img1, img2, method=method, error=error)
    # - Test metadata equality:
    md_ref = img1.metadata
    md = img2.metadata
    t_met = all([k in md and v == md[k] for k, v in md_ref.items()])
    if not t_met:
        print("SpatialImages metadata are different.")

    return t_arr and t_met


class SpatialImagePlugin(object):
    """

    """

    def __init__(self, image, **kwargs):
        """

        Example
        -------
        >>> from timagetk.io import imread
        >>> from timagetk.io.util import shared_data
        >>> from timagetk.plugins.spatial_image import SpatialImagePlugin
        >>> image_path = shared_data('io_3D.tif')
        >>> spim = imread(image_path)
        >>> # Use the dedicated plugins to access higher-level methods...
        >>> spip = SpatialImagePlugin(spim)
        >>> # ... like histogram plotting:
        >>> spip.plot.histogram()
        >>> # ... like histogram plotting:
        >>> spip.plot.projection('maximum')
        >>> # ... like image resampling methods:
        >>> iso_img = spip.resample.to_isometric()

        """
        # super().__init__()
        self.image = image
        self.plot = SpatialImagePlotting(image)
        self.resample = SpatialImageResampling(image)

    def __str__(self):
        """Method called when printing the object."""
        msg = "SpatialImagePlugin instance with following metadata:\n"
        md = self.metadata
        msg += '\n'.join(['   - {}: {}'.format(k, v) for k, v in md.items()])
        return msg

    # --------------------------------------------------------------------------
    #
    # Image comparisons
    #
    # --------------------------------------------------------------------------

    def equal(self, sp_img, error=EPS, method='max_error'):
        """Equality test between two ``SpatialImage``.

        Uses array equality and metadata matching.

        Parameters
        ----------
        sp_img : SpatialImage
            another ``SpatialImage`` instance to test for array equality
        error : float, optional
            maximum difference accepted between the two arrays (default=EPS)
        method : str in {'max_error', 'cum_error'}, optional
            type of "error measurement", choose among (default='max_error'):
              - "max_error": max difference accepted for a given pixel
              - "cum_error": max cumulative (sum) difference for the whole array

        Returns
        -------
        bool
            True/False if (array and metadata) are equal/or not

        Notes
        -----
        Metadata equality test compare defined self.metadata keys to their
        counterpart in 'sp_img'. Hence, a missing key in 'sp_im' or a different
        value will return ``False``.

        Example
        -------
        >>> import numpy as np
        >>> from timagetk.plugins.spatial_image import SpatialImagePlugin
        >>> from timagetk.array_util import random_spatial_image
        >>> img1 = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> img2 = random_spatial_image((3, 4, 4), voxelsize=[1., 0.5, 0.5], dtype='uint8')

        >>> spip = SpatialImagePlugin(img1)
        >>> spip.equal(img1)
        True
        >>> spip.equal(img2)
        Max difference between arrays is greater than '1e-09'.
        False

        """
        return equal_spatial_image(self.image, sp_img, method=method, error=error)

    def equal_array(self, sp_img, method='max_error', error=EPS):
        """
        Test array equality between two ``SpatialImage``.

        Parameters
        ----------
        sp_img : SpatialImage
            another ``SpatialImage`` instance to test for array equality
        method : str in {'max_error', 'cum_error'}, optional
            type of "error measurement", choose among:
              - "max_error": max difference accepted for a given pixel
              - "cum_error": max cumulative (sum) difference for the whole array
        error : float, optional
            maximum difference accepted between the two arrays, should be
            strictly inferior to this value to return True, default: EPS

        Returns
        -------
        bool
            True/False if arrays are equal/or not

        Example
        -------
        >>> import numpy as np
        >>> from timagetk.plugins.spatial_image import SpatialImagePlugin
        >>> from timagetk.array_util import random_spatial_image
        >>> img1 = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> img2 = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')

        >>> spip = SpatialImagePlugin(img1)
        >>> spip.equal_array(img1)
        True
        >>> spip.equal_array(img2)
        Max difference between arrays is greater than '1e-09'.
        False

        """
        return equal_spatial_image(self, sp_img, method=method, error=error)


# --------------------------------------------------------------------------
#
# Plotting methods
#
# --------------------------------------------------------------------------

class SpatialImagePlotting(object):
    """Class grouping plotting methods."""

    def __init__(self, image, **kwargs):
        self.image = image

    def cumulative_distribution(self, nbins=256):
        """Plot cumulative distribution function of image.

        Parameters
        ----------
        nbins : int
            number of bins for the cumulative histogram

        Notes
        -----
        Range is automatically set to the type of values in the array.

        Example
        -------
        >>> from timagetk.plugins.spatial_image import SpatialImagePlotting
        >>> from timagetk.array_util import random_spatial_image
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> image = SpatialImagePlotting(img)
        >>> image.cumulative_distribution()

        """
        import matplotlib.pyplot as plt
        from skimage.exposure import cumulative_distribution

        img_cdf, bins = cumulative_distribution(self.image.get_array(), nbins=nbins)
        mini, maxi = type_to_range(self.image)
        plt.figure()
        plt.plot(bins, img_cdf, 'r')
        ttl = "Cumulative distribution function{}"
        fname = self.image.filename
        plt.title(ttl.format(" -" + fname if fname != "" else ""))
        plt.xlim(mini, maxi)
        plt.xlabel("Voxel values.")
        plt.show()
        return

    def histogram(self, nbins=256):
        """Plot histogram pixel intensity of image.

        Parameters
        ----------
        nbins : int
            number of bins for the histogram

        Notes
        -----
        Range is automatically set to the type of values in the array.

        Example
        -------
        >>> from timagetk.plugins.spatial_image import SpatialImagePlotting
        >>> from timagetk.array_util import random_spatial_image
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> image = SpatialImagePlotting(img)
        >>> image.histogram()

        """
        import matplotlib.pyplot as plt
        mini, maxi = type_to_range(self.image)
        plt.figure()
        plt.hist(self.image.get_array().ravel(), bins=nbins)
        ttl = "Histogram{}"
        fname = self.image.filename
        plt.title(ttl.format(" -" + fname if fname != "" else ""))
        plt.xlim(mini, maxi)
        plt.xlabel("Voxel values.")
        plt.show()
        return

    def projection(self, method='contour', **kwargs):
        """Plot Intensity Projection.

        Parameters
        ----------
        method : str, optional
            method to use for projection, see ``PROJECTION_METHODS`` for available methods

        Other Parameters
        ----------------
        threshold : int
            Threshold to use with the 'contour' method
        axis : str in {'x', 'y', 'z'}
            Axis to use for projection
        invert_axis : bool
            If ``True``, default ``False``, revert the selected axis before 2D projection.
        title : str
            Title to add to the figure.
        cmap : str
            The colormap to use for intensity coloring, 'grey' by default.

        See Also
        --------
        PROJECTION_METHODS : The list of available projection methods.
        timagetk.plugins.projection.projection : The method creating the 2D projections.

        Examples
        --------
        >>> from timagetk.plugins.spatial_image import SpatialImagePlotting
        >>> from timagetk.array_util import random_spatial_image
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> image = SpatialImagePlotting(img)
        >>> # Generate the classical "maximum" intensity projection:
        >>> image.projection('maximum')
        >>> # Generate the "contour" projection with automatic threshold selection:
        >>> image.projection('contour')

        """
        import matplotlib.pyplot as plt
        from timagetk.visu.projection import projection

        fname = self.image.filename
        default_title = f"Intensity projection - {method}{' - ' + fname if fname != '' else ''}"
        ttl = kwargs.get('title', default_title)

        mip = projection(self.image, method=method, **kwargs)
        plt.figure()
        plt.imshow(mip, cmap=kwargs.get('cmap', 'gray'))
        plt.title(ttl)
        plt.show()
        return
