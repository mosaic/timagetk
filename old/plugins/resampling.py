#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

import numpy as np
from timagetk.algorithms.resample import resample
from timagetk.algorithms.template import get_iso_shape
from timagetk.algorithms.template import get_iso_voxelsize
from timagetk.algorithms.template import iso_template
from timagetk import LabelledImage
from timagetk import SpatialImage


class ResamplingPlugin(object):
    """Plugin to resample images with various strategies.

    Attributes
    ----------
    image : SpatialImage or LabelledImage
        The image to resample.

    Examples
    --------
    >>> from timagetk.plugins.resampling import ResamplingPlugin
    >>> from timagetk.io import imread
    >>> from timagetk.io.util import shared_data
    >>> # Loading an example grayscale image and show info:
    >>> img = imread(shared_data("sphere_membrane_0.0.inr.gz", "sphere"))
    >>> print(img)
    >>> rsp = ResamplingPlugin(img)
    >>> iso_img = rsp.isometric(0.5)
    >>> print(iso_img)
    >>> res_img = rsp.to_shape([50, 50, 50])
    >>> print(res_img)

    """

    def __init__(self, image):
        """Constructor.

        Parameters
        ----------
        image : SpatialImage or LabelledImage
            The image to resample.

        """
        try:
            assert image.voxelsize != []
        except AssertionError:
            raise ValueError("Input image has an EMPTY voxelsize attribute!")

        self.image = image

    def to_shape(self, shape, interpolation=None):
        """Resample image to given shape.

        Parameters
        ----------
        shape : list, optional
            The (Z)YX sorted shape to use for image resampling.
        interpolation : str in {"nearest", "linear", "cspline", "cellbased"}, optional
            Use one of the ``GRAY_INTERPOLATION_METHODS`` with grayscale images.
            Use one of the ``LABEL_INTERPOLATION_METHODS`` with labelled images.
            By default, (None) try to guess it from the type of ``image``.

        Returns
        -------
        SpatialImage or LabelledImage
            The resampled image.

        Examples
        --------
        >>> from timagetk.plugins.resampling import ResamplingPlugin
        >>> from timagetk.io import imread
        >>> from timagetk.io.util import shared_data
        >>> # Loading an example grayscale image and show info:
        >>> img = imread(shared_data("sphere_membrane_0.0.inr.gz", "sphere"))
        >>> print(img)
        >>> rsp = ResamplingPlugin(img)
        >>> res_img = rsp.to_shape([50, 50, 50])
        >>> print(res_img)

        """
        return resample(self.image, shape=shape, interpolation=interpolation)

    def to_voxelsize(self, voxelsize, interpolation=None):
        """Resample image to given voxelsize.

        Parameters
        ----------
        voxelsize : list, optional
            The (Z)YX sorted voxelsize to use for image resampling.
        interpolation : str in {"nearest", "linear", "cspline", "cellbased"}, optional
            Use one of the ``GRAY_INTERPOLATION_METHODS`` with grayscale images.
            Use one of the ``LABEL_INTERPOLATION_METHODS`` with labelled images.
            By default (None) try to guess it from the type of ``image``.

        Returns
        -------
        SpatialImage or LabelledImage
            The resampled image.

        Examples
        --------
        >>> from timagetk.plugins.resampling import ResamplingPlugin
        >>> from timagetk.io import imread
        >>> from timagetk.io.util import shared_data
        >>> # Loading an example grayscale image and show info:
        >>> img = imread(shared_data("sphere_membrane_0.0.inr.gz", "sphere"))
        >>> print(img)
        >>> rsp = ResamplingPlugin(img)
        >>> res_img = rsp.to_voxelsize([0.5, 0.4, 0.3])
        >>> print(res_img)

        """
        return resample(self.image, voxelsize=voxelsize,
                        interpolation=interpolation)

    def isometric(self, method='max', interpolation=None):
        """Resample image to isometric voxelsize.

        Parameters
        ----------
        method : float or str in {'min', 'max'}, optional
            Change voxelsize to 'min', 'max' (default) of original voxelsize or
            to a given value.
        interpolation : str in {"nearest", "linear", "cspline", "cellbased"}, optional
            Use one of the ``GRAY_INTERPOLATION_METHODS`` with grayscale images.
            Use one of the ``LABEL_INTERPOLATION_METHODS`` with labelled images.
            By default (None) try to guess it from the type of ``image``.

        Returns
        -------
        SpatialImage or LabelledImage
            The resampled image.

        Examples
        --------
        >>> from timagetk.plugins.resampling import ResamplingPlugin
        >>> from timagetk.io import imread
        >>> from timagetk.io.util import shared_data
        >>> # Loading an example grayscale image and show info:
        >>> img = imread(shared_data("sphere_membrane_0.0.inr.gz", "sphere"))
        >>> print(img)
        >>> rsp = ResamplingPlugin(img)
        >>> res_img = rsp.isometric(0.3)
        >>> print(res_img)

        """
        new_vox = get_iso_voxelsize(self.image, method)

        if np.allclose(self.image.voxelsize, new_vox, atol=1.e-6):
            print("Image is already isometric with voxelsize: {}!".format(
                self.image.voxelsize))
            return self.image

        return resample(self.image, voxelsize=new_vox,
                        interpolation=interpolation)

    def get_isometric_voxelsize(self, method='max'):
        """Returns the isometric voxelsize of the image.

        Parameters
        ----------
        method : float or str in {'min', 'max'}, optional
            Change voxelsize to 'min', 'max' (default) of original voxelsize or
            to a given value.

        Returns
        -------
        list
            Isometric voxelsize of the SpatialImage.

        Examples
        --------
        >>> from timagetk.plugins.resampling import ResamplingPlugin
        >>> from timagetk.io import imread
        >>> from timagetk.io.util import shared_data
        >>> # Loading an example grayscale image and show info:
        >>> img = imread(shared_data("sphere_membrane_0.0.inr.gz", "sphere"))
        >>> print(img)
        >>> rsp = ResamplingPlugin(img)
        >>> rsp.get_isometric_voxelsize(0.3)

        """
        return get_iso_voxelsize(self.image, method)

    def get_isometric_shape(self, method='max'):
        """Returns the isometric shape of the image.

        Parameters
        ----------
        method : float or str in {'min', 'max'}, optional
            Change voxelsize to 'min', 'max' (default) of original voxelsize or
            to a given value.

        Returns
        -------
        list
            Isometric shape of the SpatialImage.

        Examples
        --------
        >>> from timagetk.plugins.resampling import ResamplingPlugin
        >>> from timagetk.io import imread
        >>> from timagetk.io.util import shared_data
        >>> # Loading an example grayscale image and show info:
        >>> img = imread(shared_data("sphere_membrane_0.0.inr.gz", "sphere"))
        >>> print(img)
        >>> rsp = ResamplingPlugin(img)
        >>> rsp.get_isometric_shape(333)

        """
        return get_iso_shape(self.image, method)

    def get_isometric_template(self, method='max'):
        """Returns the isometric template of the image.

        Parameters
        ----------
        method : float or str in {'min', 'max'}, optional
            Change voxelsize to 'min', 'max' (default) of original voxelsize or
            to a given value.

        Returns
        -------
        SpatialImage
            Isometric SpatialImage template.

        Examples
        --------
        >>> from timagetk.plugins.resampling import ResamplingPlugin
        >>> from timagetk.io import imread
        >>> from timagetk.io.util import shared_data
        >>> # Loading an example grayscale image and show info:
        >>> img = imread(shared_data("sphere_membrane_0.0.inr.gz", "sphere"))
        >>> print(img)
        >>> rsp = ResamplingPlugin(img)
        >>> tmp_img = rsp.get_isometric_template(333)
        >>> print(tmp_img)

        """
        return iso_template(self.image, method)
