#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Sophie Ribes <sophie.ribes@inria.fr>
#
#       File contributor(s):
#           Guillaume Baty <guillaume.baty@inria.fr>
#           Gregoire Malandain <gregoire.malandain@inria.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

"""Generic implementation of several segmentation algorithms.

"""

import numpy as np
from scipy.ndimage import binary_fill_holes
from timagetk.algorithms.connexe import connexe
from timagetk.algorithms.exposure import global_contrast_stretch
from timagetk.algorithms.linearfilter import gaussian_filter
from timagetk.algorithms.linearfilter import linearfilter
from timagetk.algorithms.regionalext import regionalext
from timagetk.algorithms.watershed import watershed
from timagetk import SpatialImage
from timagetk.components.spatial_image import assert_spatial_image
from timagetk.io import imread
from timagetk.plugins.resampling import ResamplingPlugin
from timagetk.tasks.segmentation import li_threshold


class SegmentationPlugin(object):
    """Segmentation plugin."""

    def __init__(self, image, seeds_image=None, **kwargs):
        """Constructor.

        Parameters
        ----------
        image : str or SpatialImage
            Input image to transform.
        seeds_image : LabelledImage, optional
            Seed image to use for initialisation of watershed algorithm.
            Required if `method` is "seeded_watershed".

        """
        # - Read image if a string or assert its a SpatialImage
        if isinstance(image, str):
            image = imread(image)
        else:
            assert_spatial_image(image, obj_name='image')
        self.image = image
        # - Read image if a string:
        if isinstance(seeds_image, str):
            seeds_image = imread(seeds_image)
        self.seeds_image = seeds_image
        # - Initialize empty attributes:
        self.label_image = None  # segmented/labelled image
        self.input_image = None  # image used as input for segmentation algorithm

    def get_input_image(self):
        """Get the image used as input for segmentation algorithm

        Returns
        -------
        SpatialImage
            The image used as input for segmentation algorithm

        """
        return self.input_image

    def get_seeds_image(self):
        """Get the seeds used for segmentation algorithm.

        Returns
        -------
        SpatialImage
            The seeds used for segmentation algorithm.

        """
        return self.seeds_image

    def get_label_image(self):
        """Get the labelled image from segmentation algorithm.

        Returns
        -------
        LabelledImage
            The labelled image from segmentation algorithm.

        """
        return self.label_image

    def seeded_watershed(self, hmin, sigma=None, **kwargs):
        """Use seeded watershed algorithm to performs segmentation.

        Uses ``h_transform`` & ``region_labeling`` with given h-minima to
        generate seeds later used by the ``watershed`` algorithm.
        If `sigma` is defined, a *gaussian smoothing* ``linear_filtering`` is
        applied to input image prior to seed detection and watershed. Parameter
        ``real=True`` may also be specified using keyword argument.

        Parameters
        ----------
        hmin : int
            Height value to use in h-extrema transformation
        sigma : float, optional
            Sigma value used to smooth the input image, use `0` to skip.
            If ``None``, automatically set to ``1.5 * max(image.voxelsize)``.

        Other Parameters
        ----------------
        control : str
            Deal with watershed conflicts, default is *first*.
        real : bool
            If ``True`` (default), ``sigma`` is in real units, else in voxel.
        isometric : bool
            If ``True``, default ``False``, the image is first resampled to
            isometric voxelsize using the min voxelsize value.

        Returns
        -------
        LabelledImage
            Labelled image with its metadata.

        Notes
        -----
        If an isometric resampling is required, it will be performed prior to
        any other algorithm! Beware this will increase computation time,
        notably for the ``regionalext`` algorithm.

        If ``real`` is ``True``, make sure to provide a ``sigma`` superior or
        equal to the minimal voxelsize.

        See Also
        --------
        WATERSHED_CONTROLS : for a list of available control method for watershed.
        timagetk.algorithm.linearfilter : for explanations about linear filtering methods.
        timagetk.algorithm.regionalext : for explanations about the height transform algorithm.
        timagetk.algorithm.connexe: for explanations about the region labelling algorithm.
        timagetk.algorithm.watershed: for explanations about the watershed algorithm.

        Example
        -------
        >>> from timagetk.io.util import shared_data
        >>> from timagetk.io import imread
        >>> from timagetk.plugins.segmentation import SegmentationPlugin
        >>> segment = SegmentationPlugin(shared_data('p58-t0-a0.lsm', 'p58'))
        >>> seg_img = segment.seeded_watershed(hmin=15, sigma=1.2)

        >>> # - Show original image and obtained segmentation:
        >>> from timagetk.visu.mplt import grayscale_imshow
        >>> zslice = 40
        >>> titles = ["Original", "Labelled"]
        >>> grayscale_imshow([segment.image, seg_img], slice_id=zslice, title=titles, val_range=['type', 'auto'], cmap=['gray', 'glasbey'])

        >>> # - Show original, input, seeds and obtained segmentation:
        >>> titles = ["Original", 'Input', "Seeds", "Labelled"]
        >>> images = [segment.image, segment.input_image, segment.seeds_image, segment.label_image]
        >>> grayscale_imshow(images, slice_id=zslice, title=titles, val_range=['type', 'type', 'auto', 'auto'], cmap=['gray', 'gray', 'glasbey', 'glasbey'])

        """
        # - Performs isometric resampling if required:
        isometric = kwargs.get('isometric', False)
        if isometric:
            image = ResamplingPlugin(self.image).isometric(method='min')
        else:
            image = self.image
        # Image contrast stretching:
        image = global_contrast_stretch(image)
        # - Performs Gaussian smoothing if required:
        min_sigma_val = np.max(image.voxelsize)
        if sigma is None:
            sigma = 1.5 * min_sigma_val
        if sigma >= min_sigma_val:
            image = gaussian_filter(image, sigma=sigma, real=True)
        else:
            raise ValueError(f"Given sigma ({sigma}) is inferior to image max voxelsize ({min_sigma_val}!")
        self.input_image = image
        # - Local minima detection:
        ext_img = regionalext(self.input_image, method='minima', height=hmin, **kwargs)
        # - Connexe component labelling of detected local minima:
        self.seeds_image = connexe(ext_img, low_threshold=1, high_threshold=hmin, **kwargs)
        n_seeds = len(np.unique(self.seeds_image)) - 1  # '0' is in the list!
        print("Detected {} seeds!".format(n_seeds))
        # - Watershed segmentation:
        self.label_image = watershed(self.input_image, self.seeds_image, **kwargs)

        return self.label_image

    def background_detection(self, threshold=None, sigma=None, **kwargs):
        """Detect the background position, *ie.* not part of the object.

        Parameters
        ----------
        threshold : int, optional
            The threshold to use for bacground detection.
            If ``None``, use the ``li_threshold`` method to estimate it.
        sigma : float, optional
            Sigma value used to smooth the input image, use `0` to skip.
            If ``None``, automatically set to ``1.5 * max(image.voxelsize)``.

        Other Parameters
        ----------------
        real : bool
            If ``True`` (default), ``sigma`` is in real units, else in voxel.

        Returns
        -------
        SpatialImage
            Mask image (binary) where ``1`` indicate background position.

        See Also
        --------
        li_threshold : Compute threshold value by Li’s iterative Minimum Cross
        Entropy method on 3D images.
        scipy.ndimage.binary_fill_holes : Fill holes in binary image.

        Notes
        -----
        This strategy assumes the background is where "there is no signal".
        We fill holes in the detected structure per slice in z axis.

        Example
        -------
        >>> from timagetk.io.util import shared_data
        >>> from timagetk.io import imread
        >>> from timagetk.plugins.segmentation import SegmentationPlugin
        >>> segment = SegmentationPlugin(shared_data('p58-t0-a0.lsm', 'p58'))
        >>> bkgd_img = segment.background_detection(hmin=15, sigma=1.2)
        >>> from timagetk.visu.stack import stack_browser
        >>> stack_browser(bkgd_img, val_range=[0,1])

        """
        # Image contrast stretching:
        image = global_contrast_stretch(self.image)
        # - Performs Gaussian smoothing if required:
        min_sigma_val = np.max(image.voxelsize)
        if sigma is None:
            sigma = 1.5 * min_sigma_val
        if sigma >= min_sigma_val:
            image = linearfilter(image, method="smoothing", sigma=sigma,
                                 **kwargs)
        else:
            raise ValueError(
                f"Given sigma ({sigma}) is inferior to max image voxelsize ({min_sigma_val}!")

        if threshold is None:
            threshold = li_threshold(image)

        th_image = connexe(image, low_threshold=threshold, **kwargs)
        th_arr = th_image.get_array() != 0
        th_arr = binary_fill_holes(th_arr)
        # Fill holes by z-slices:
        n_sl = th_arr.shape[0]
        for sl in range(n_sl):
            th_arr[sl, :, :] = binary_fill_holes(th_arr[sl, :, :])
        # Invert image to get background "mask":
        th_arr = th_arr == 0
        return SpatialImage(th_arr.astype('uint8'))
