#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------


"""
These examples shows the effect of some filters on labelled images.
"""

from matplotlib import gridspec
from matplotlib.pyplot import figure
from matplotlib.pyplot import imshow
from matplotlib.pyplot import show
from matplotlib.pyplot import subplot
from matplotlib.pyplot import tight_layout
from matplotlib.pyplot import title
from timagetk.io import imread
from timagetk.old_plugins.cell_filtering import CELL_FILTER_METHODS
from timagetk.old_plugins.cell_filtering import cell_filtering
from timagetk.io.util import shared_data

# - Example input image path
img_path = shared_data('time_3_seg.inr')
# - Load the image:
sp_img = imread(img_path)

# - Get the middle z-slice:
n_zslices = sp_img.get_shape('z')
middle_z = int(n_zslices / 2)

# - To get the list of defined cell filtering methods:
print(CELL_FILTER_METHODS)

# Define list of increasing standard deviation to apply on images:
n_fig = len(CELL_FILTER_METHODS) + 1  # don't forget original image

# - We create a grid to assemble all figures:
figure(figsize=[3.5 * n_fig, 4])
gs = gridspec.GridSpec(1, n_fig)

# - Get the middle z-slice of the original labelled image and add it as first image:
subplot(gs[0, 0])
imshow(sp_img.get_slice(middle_z, 'z').get_array(), cmap="prism")
title("original z-slice (z {}/{})".format(middle_z, n_zslices))

radius = 2
for n, method in enumerate(CELL_FILTER_METHODS):
    print("Apply cell filtering: {}".format(method))
    # Define where to add the image in the plot:
    subplot(gs[0, n + 1])
    # Perform gaussian smoothing:
    filter_img = cell_filtering(sp_img, method, radius=radius)
    imshow(filter_img.get_slice(middle_z, 'z').get_array(), cmap="prism")
    title('{} (radius={})'.format(method, radius))

# - Display the images:
tight_layout()
show()
