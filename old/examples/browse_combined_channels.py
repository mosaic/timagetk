# -*- coding: utf-8 -*-
from timagetk.io import imread
from timagetk.plugins import registration
from timagetk.io.util import shared_data
from timagetk.visu.stack import channels_stack_browser

float_img = imread(shared_data('time_0_cut.inr'))
ref_img = imread(shared_data('time_1_cut.inr'))
trsf_rig, res_rig = registration(float_img, ref_img, method='rigid')

channels_stack_browser([ref_img, res_rig], channel_names=['t0 on t1', 't1'],
                       colors=['green', 'red'],
                       title='Channel combination example - Rigid registration',
                       axis='z')
