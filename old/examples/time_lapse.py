#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

"""
This example illustrate how to obtain a time-lapse animation (gif) from a sequence of intensity images.
"""

from timagetk.io import imread
from timagetk.io.util import shared_data
from timagetk.visu.temporal_projection import mips2gif
from timagetk.visu.temporal_projection import sequence_projections

times = [0, 1, 2]
list_fnames = ['p58-t{}_INT_down_interp_2x.inr.gz'.format(time) for time in times]
list_images = [imread(shared_data(fname, "p58")) for fname in list_fnames]

# Register image list then generates the max intensity projections:
mips = sequence_projections(list_images, threshold=55, register=True, export_png=True)

# Generate GIF animation:
mips2gif(mips, shared_data("p58_TimeLapse.gif", "p58"))
