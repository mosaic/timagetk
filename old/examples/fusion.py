#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------


"""
This exemple shows how to do multi-angle fusion from grey level images.
"""
from timagetk.io import imread
from timagetk.plugins.fusion import fusion
from timagetk.algorithms.metrics import structural_similarity_index
from timagetk.algorithms.resample import resample
from timagetk.io.util import shared_data

# Defines files location:
files = [shared_data("time_0_cut.inr"),
         shared_data("time_0_cut_rotated1.mha"),
         shared_data("time_0_cut_rotated2.mha")]
ref_fusion_file = shared_data("time_0_cut_fused.mha")

# Defines downsampling factor (speed up computation):
downsampling = 2.

# Loop reading and downsampling the images to fuse:
img_list = []
for f in files:
    print(f)
    img = imread(f)
    ds_vxs = [vox * downsampling for vox in img.voxelsize]
    img_list.append(resample(img, voxelsize=ds_vxs, interpolation='grey'))

# Performs multi-angle fusion:
fus_img = fusion(img_list, 0)

# Read the "reference fusion" file and apply same downsampling:
ref_fus_img = imread(ref_fusion_file)
ds_vxs = [vox * downsampling for vox in ref_fus_img.voxelsize]
ref_fus_img = resample(ref_fus_img, voxelsize=ds_vxs, interpolation='grey')

# Compute the structural similarity index between the "reference fusion", and the one we just computed:
ssim = structural_similarity_index(fus_img, ref_fus_img)

# Test if the images are 98% similar (according to structural similarity):
print(ssim >= 0.98)