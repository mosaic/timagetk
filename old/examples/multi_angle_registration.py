#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

from timagetk.algorithms.quaternion import centered_rotation_about_axis
from timagetk.io import imread
from timagetk.io import imsave
from timagetk.plugins import registration
from timagetk.visu.projection import contour_projection
from timagetk.io.util import shared_data
from timagetk.visu.mplt import grayscale_imshow

# Using shared data as example, images are multi-angle view of the same floral meristem:
fname = 'p58-t0-a{}{}.{}'
list_imgs = [imread(shared_data(fname.format(a, '', 'lsm'), "p58")) for a in range(3)]

# Create Max Intensity Projections and display them:
mips = [contour_projection(img, threshold=45) for img in list_imgs]
img_names = [fname.format(a, '', 'lsm') for a in range(3)]
# grayscale_imshow(mips, title="MultiAngleImage Max Intensity Projections",
#                  img_title=img_names, range=[0, 255])

# As you can see on the previous representation, the angle images 1 & 2 are
# rotated compared to angle image 0 (ref.):
#  - p58-t0-a1.lsm is counter-clockwise rotated by 90°;
#  - p58-t0-a2.lsm is rotated by 180°;

# Apply these rotations before registration:
list_imgs[1] = centered_rotation_about_axis(list_imgs[1], -90, 'z')
list_imgs[1] = centered_rotation_about_axis(list_imgs[1], 30, 'x')
list_imgs[2] = centered_rotation_about_axis(list_imgs[2], 180, 'z')
list_imgs[2] = centered_rotation_about_axis(list_imgs[2], -30, 'y')

# Create Max Intensity Projections of rotated images and display them:
mips_man = [contour_projection(img, threshold=45) for img in
            list_imgs]
img_names_man = [fname.format(a, '-rotated', 'lsm') for a in range(3)]
# grayscale_imshow(mips_man, title="MultiAngleImage Max Intensity Projections (rotated)",
#                  img_title=img_names_man, range=[0, 255])

# Defines reference image and floating images:
ref_img = list_imgs[0]
float_imgs = list_imgs[1:]

# Performs RIGID registration
trsf_rig, res_rig = registration(float_imgs, ref_img, method='rigid',
                                 pyramid_lowest_level=1)

# Create Max Intensity Projections of rotated images and display them:
mips_rig = [contour_projection(img, threshold=45) for img in
            [ref_img] + list(res_rig)]
img_names_rig = [fname.format(a, '-rigid_reg', 'lsm') for a in range(3)]
# grayscale_imshow(mips_rig, title="MultiAngleImage Max Intensity Projections (rigid registered)",
#                  img_title=img_names_rig, range=[0, 255])

grayscale_imshow(mips + mips_man + mips_rig,
                 title=img_names + img_names_man + img_names_rig,
                 suptitle="MultiAngleImage Max Intensity Projections (rigid registered)",
                 max_per_line=3)

# Performs AFFINE registration
trsf_aff, res_aff = registration(float_imgs, ref_img, method='affine',
                                 pyramid_lowest_level=1)

# Create Max Intensity Projections of rotated images and display them:
mips = [contour_projection(img, threshold=45) for img in
        [ref_img] + list(res_aff)]
img_names = [fname.format(a, 'lsm') for a in range(3)]
grayscale_imshow(mips, title=img_names,
                 suptitle="MultiAngleImage Max Intensity Projections (affine registered)")

# Save result images:
[imsave(shared_data(fname.format(str(a) + "_on_a0", 'inr'), "p58"), res_rig[a - 1]) for a
 in range(1, 3)]
