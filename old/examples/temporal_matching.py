# -*- coding: utf-8 -*-
# imports
import os
import pickle

from timagetk.io import imread
from timagetk.unverified.temporal_matching import TemporalMatching
from timagetk.io.util import shared_data

out_path = './results/'
if not os.path.isdir(out_path):
    new_fold = os.path.join(os.getcwd(),'results')
    os.mkdir(new_fold)

# We consider two different times
times = [3,4]
back_id_list = [1, 1]
segmentation_list = []

for ind, val in enumerate(times):
    img = imread(shared_data('time_{}_seg.inr'.format(val)))
    # subimage extraction
    shape = img.shape
    indices = [0, shape[0] - 1, 0, shape[1] - 1, 0, 5]
    img = img.get_region(region=indices)
    segmentation_list.append(img)

# temporal matching
obj_tm = TemporalMatching(segmentation_list=segmentation_list,
                          feature_space_list=None,
                          background_ids=back_id_list, erode=True)

# computation of admissible matching
adm_match_list = obj_tm.get_admissible_matching_list()

# matching criterion (default is None)
criterion = 'Jaccard coefficient'
# computation of normalized matching costs
norm_cost_list = obj_tm.compute_normalized_cost(adm_match_list, criterion=criterion)

init_graph = obj_tm.build_init_graph(norm_cost_list)
# iterative matching
matching_dict = obj_tm.iterative_matching(norm_cost_list, init_graph)

obj_tm.write_event_cost_file(out_path, matching_dict, times)

lineage = obj_tm.lineage_dict(matching_dict)

matched_imgs = obj_tm.res_images(matching_dict)

from timagetk.io import imsave
for n, img in enumerate(matched_imgs[:2]):
    imsave(os.path.join(out_path, 'time_{}_seg_matched.inr'.format(times[n])), img)


# init_graph = obj_tm.build_init_graph(norm_cost_list)
#
# import networkx as nx
# source_id = 0
# sink_id = init_graph.number_of_nodes() - 1
# # - Compute the max flow min cost on the flow graph:
# mfmc = nx.algorithms.flow.max_flow_min_cost(init_graph, source_id, sink_id,
#                                             capacity='capacity',
#                                             weight='weight')
#
# mapping = {}
# appear, disappear = [], []
# for source, targets in mfmc.items():
#     mapping[source] = []
#     for target in targets:
#         has_flow = mfmc[source][target] == 1
#         if has_flow and target != sink_id:
#             mapping[source].append(target)
#     # Remove empty mapping:
#     if mapping[source] == []:
#         mapping.pop(source)
#     # Remove nodes targeting 'appear' or 'disappear' nodes
#     elif len(mapping[source]) == 1:
#         target = mapping[source][0]
#         # Look if target node is 'appear':
#         try:
#             init_graph.node[target]['label'] == 'appear'
#         except KeyError:
#             pass
#         else:
#             appear.append(source)
#             try:
#                 mapping.pop(source)
#             except KeyError:
#                 pass
#         # Look if target node is 'disappear':
#         try:
#             init_graph.node[target]['label'] == 'disappear'
#         except KeyError:
#             pass
#         else:
#             disappear.append(source)
#             try:
#                 mapping.pop(source)
#             except KeyError:
#                 pass
#
# try:
#     mapping.pop(source_id)
# except KeyError:
#     pass
# try:
#     mapping.pop(sink_id)
# except KeyError:
#     pass
#
# mapping
#
# appear
# disappear


poss_opt = ['matching','iterative_matching']
# opt = 'matching'
opt = 'iterative_matching'

if opt in poss_opt:
    if opt=='matching':
        # appear/disappear cost
        alpha_cost = int((80/100.0)*obj_tm.max_cost)
        # matching
        matching_dict = obj_tm.matching(norm_cost_list, alpha_cost)
    elif opt=='iterative_matching':
        # initial graph
        init_graph = obj_tm.build_init_graph(norm_cost_list)
        # iterative matching
        matching_dict = obj_tm.iterative_matching(norm_cost_list, init_graph)

    # write a dictionary of matching
    pkl_path = ''.join([out_path, 'example_track_matching_dict.pkl'])
    pkl_file = open(str(pkl_path), 'wb')
    # save as pickle object (serialization)
    pickle.dump(matching_dict, pkl_file)
    pkl_file.close()

    # resulting images
    img_list = obj_tm.res_images(matching_dict)
    # save results
    res_name = 'example_track_initial_cells.inr'
    imsave(out_path+res_name, img_list[0])
    res_name = 'example_track_final_cells.inr'
    imsave(out_path+res_name, img_list[1])
    res_name = 'example_track_disapp_cells.inr'
    imsave(out_path+res_name, img_list[2])
    res_name = 'example_track_app_cells.inr'
    imsave(out_path+res_name, img_list[3])
else:
    print('Poss. opt: {}'.format(poss_opt))
