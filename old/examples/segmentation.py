#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Sophie Ribes <sophie.ribes@inria.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

"""
This exemple shows how to performs seeded watershed segmentation on grey level images.
"""

import os

from timagetk.io import imread
from timagetk.io import imsave
from timagetk.plugins import h_transform
from timagetk.old_plugins import labels_post_processing
from timagetk.plugins import linear_filtering
from timagetk.plugins import morphology
from timagetk.plugins import region_labeling
from timagetk.plugins import segmentation
from timagetk.io.util import shared_data

out_path = './results/'  # to save results
if not os.path.isdir(out_path):
    new_fold = os.path.join(os.getcwd(), 'results')
    os.mkdir(new_fold)

# we consider an input image
# SpatialImage instance
input_img = imread(shared_data('input.tif'))

# optional denoising block
smooth_img = linear_filtering(input_img, sigma=2.0,
                              method='gaussian_smoothing')
asf_img = morphology(smooth_img, max_radius=3,
                     method='co_alternate_sequential_filter')

ext_img = h_transform(asf_img, h=150,
                      method='min')
con_img = region_labeling(ext_img, low_threshold=1,
                          high_threshold=150,
                          method='connected_components')
seg_img = segmentation(smooth_img, con_img, control='first',
                       method='seeded_watershed')

# optional post processig block
pp_img = labels_post_processing(seg_img, radius=1,
                                iterations=1,
                                method='labels_erosion')

res_name = 'example_segmentation.tif'
imsave(out_path + res_name, pp_img)
