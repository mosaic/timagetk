# -*- coding: utf-8 -*-
from timagetk.io import imread
from timagetk.io.util import shared_data
from timagetk.visu.mplt import stack_browser
fname = 'p58-t0-a0.lsm'
img = imread(shared_data(fname))
# - Browse the grayscale image by z-slices:
stack_browser(img, fname)
# - Browse the grayscale image by x-slices, with 'viridis' colormap:
stack_browser(img, fname, cmap='viridis', axis='x')