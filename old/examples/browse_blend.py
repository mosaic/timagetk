# -*- coding: utf-8 -*-
from timagetk.components.spatial_image import SpatialImage
from timagetk.io import imread
from timagetk.plugins.segmentation import auto_seeded_watershed
from timagetk.io.util import shared_data
from timagetk.visu.stack import stack_browser
from timagetk.components.multi_channel import label_blending

img = imread(shared_data('time_0_cut.inr'))

seg_img = auto_seeded_watershed(img, hmin=5, sigma=0.3, real=True)
blend = label_blending(seg_img, img)

stack_browser(SpatialImage(blend, voxelsize=img.voxelsize),
              title='Label & intensity images blending', cmap=None)