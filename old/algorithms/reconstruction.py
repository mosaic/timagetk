#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
from timagetk.algorithms.reconstruction import im2surface
from timagetk.algorithms.reconstruction import surface2im
from timagetk.components.spatial_image import assert_spatial_image
from timagetk.components.spatial_image import assert_spatial_image
from timagetk.components.spatial_image import assert_spatial_image


def end_margin(img, width, axis=None, null_value=0):
    """ Set selected end margin axis to a null value.

    Parameters
    ----------
    img : numpy.ndarray
        A ZXY array
    width : int
        Size of the margin
    axis : int, optional
        Axis along which the margin is edited, None (default) edit them all.
    null_value : int, optional
        The null value to use for the end margins, 0 by default.

    Returns
    -------
    numpy.ndarray
        input array with the end margin set to a null value.

    See Also
    --------
    stroke

    Example
    -------
    >>> from timagetk.array_util import random_spatial_image
    >>> from timagetk.algorithms.reconstruction import end_margin
    >>> img = random_spatial_image((3, 4, 5), dtype='uint8')
    >>> end_margin(img, 1)

    """
    xdim, ydim, zdim = img.shape
    mat = np.zeros((xdim, ydim, zdim), img.dtype)
    mat = mat * null_value

    if axis is None:
        mat[:-width, :-width, :-width] = img[:-width, :-width, :-width]
    elif axis == 0:
        mat[:-width, :, :] = img[:-width, :, :]
    elif axis == 1:
        mat[:, :-width, :] = img[:, :-width, :]
    elif axis == 2:
        mat[:, :, :-width] = img[:, :, :-width]
    else:
        raise ValueError(f"Unknown axis '{axis}' for {img.ndim}D array.")

    return mat


def spatialise_matrix_points(points, image, mip_thresh=45):
    """
    Given a list of points in matrix coordinates (i.e. i,j,k - but k is ignored anyway),
    and a spatial image, returns a list of points in real space (x,y,z) with the Z coordinate
    recomputed from an altitude map extracted from image. This implies that ``points`` were placed
    on the mip/altitude map result of ``contour_projection`` applied to ``image`` with ``mip_thresh``.

    Parameters
    ----------
    points : list(tuple(float,float,float)),
        file 2D points to spatialise.
        Can also be a filename pointing to a numpy-compatible list of points.
    image : SpatialImage
        image or path to image to use to spatialise the points.
    mip_thresh : int, float
        threshold used to compute the original altitude map for points.

    Returns
    -------
    points3d : list [of tuple [of float, float, float]]
        3D points in REAL coordinates.
    """
    assert_spatial_image(image, obj_name='image')

    if isinstance(points, str):
        points = np.loadtxt(points)

    mip, _ = im2surface(image, threshold_value=mip_thresh)
    return surface2im(points, mip)


def surface_landmark_matching(ref_img, ref_pts, flo_img, flo_pts,
                              ref_pts_already_spatialised=False,
                              flo_pts_already_spatialised=False,
                              mip_thresh=45):
    """
    Computes the registration of ``flo_img`` to ``ref_img`` by minimizing distances
    between ref_pts and flo_pts.

    Parameters
    ----------
    ref_img : SpatialImage, str
        image or path to image to use to reference image.
    ref_pts : list
        ordered sequence of 2D/3D points to use as reference landmarks.
    flo_img : SpatialImage, str
        image or path to image to use to floating image
    flo_pts : list
        ordered sequence of 2D/3D points to use as floating landmarks.
    ref_pts_already_spatialised : bool
        If ``True``, consider reference points are already in REAL 3D space.
    flo_pts_already_spatialised : bool
        If ``True``, consider floating points are already in REAL 3D space.
    mip_thresh : int, float
        used to recompute altitude map to project points in 3D if they aren't
        spatialised.

    Returns
    -------
    trsf : numpy.ndarray
        The result is a 4x4 **resampling voxel matrix** (i.e. from ``ref_img``
        to ``flo_img``).

    Notes
    -----
    If ``ref_pts_already_spatialised`` and ``flo_pts_already_spatialised`` are ``True``
    and ``ref_pts`` and ``flo_pts`` are indeed in real 3D coordinates, then this is
    exactly a landmark matching registration

    """
    assert_spatial_image(ref_img, obj_name='ref_img')
    assert_spatial_image(flo_img, obj_name='flo_img')

    if isinstance(ref_pts, str):
        ref_pts = np.loadtxt(ref_pts)

    if isinstance(flo_pts, str):
        flo_pts = np.loadtxt(flo_pts)

    if not ref_pts_already_spatialised:
        print("spatialising reference")
        ref_spa_pts = spatialise_matrix_points(ref_pts, ref_img, mip_thresh=mip_thresh)
    else:
        print("not spatialising reference")
        ref_spa_pts = ref_pts

    if not flo_pts_already_spatialised:
        print("spatialising floating")
        flo_spa_pts = spatialise_matrix_points(flo_pts, flo_img, mip_thresh=mip_thresh)
    else:
        print("not spatialising floating")
        flo_spa_pts = flo_pts

    trs = pts2transfo(ref_spa_pts, flo_spa_pts)

    # -- trs is from ref_img to flo_img, in other words it is T-1,
    # a resampling matrix to put flo_img into ref_img space. ref_pts and
    # flo_pts are in real coordinates so the matrix is also in
    # real coordinates and must be converted to voxels --
    trs_vox = matrix_real2voxels(trs, flo_img.voxelsize, ref_img.voxelsize)

    return trs_vox


def pts2transfo(x, y):
    """ Infer rigid transformation from control point pairs using quaternions.

    The quaternion representation is used to register two point sets with known
    correspondences.
    It computes the rigid transformation as a solution to a least squares
    formulation of the problem.

    The rigid transformation, defined by the rotation :math:`R` and the
    translation :math:`t`, is optimized by minimizing the cost function:

    .. math::

        C(R,t) = \sum_i{|yi - R.xi - t|^2}

    The optimal translation :math:`t` is given by:

    .. math::

        t = y_b - R.x_b

    with :math:`x_b` and :math:`y_b` the barycenters of two point sets

    The optimal rotation :math:`R` using quaternions is optimized by minimizing
    the following cost function :

    .. math::

        C(q) = \sum_i{|yi'*q - q*xi'|^2}

    with :math:`yi'` and :math:`xi'` converted to barycentric coordinates and identified by quaternions

    With the matrix representations :

    .. math::

        yi'*q - q*xi' = Ai.q
        C(q) = q^T.|\sum(A^T.A)|.q = q^T.B.q


    with:

    .. math::
       :nowrap:

        \begin{eqnarray}
           0 & (xn\_i - yn\_i) & (xn\_j - yn\_j) & (xn\_k - yn\_k) \\\\
           -(xn\_i - yn\_i) & 0 & (-xn\_k - yn\_k) & (xn\_j + yn\_j) \\\\
           -(xn\_j - yn\_j) & -(-xn\_k - yn\_k) & 0 & (-xn\_i - yn\_i) \\\\
           -(xn\_k - yn\_k) & -(xn\_j + yn\_j) & -(-xn\_i - yn\_i) & 0
        \end{eqnarray}


    The unit quaternion representing the best rotation is the unit eigenvector
    corresponding to the smallest eigenvalue of the matrix -B :

    .. math::

        v = a, b.i, c.j, d.k

    The orthogonal matrix corresponding to a rotation by the unit quaternion

    .. math::

        v = a + bi + cj + dk

    with :math:`|z| = 1`, is given by :

    .. math::

        R = array([ [a*a + b*b - c*c - d*d,       2bc - 2ad      ,       2bd + 2ac      ],
                    [      2bc + 2ad      , a*a - b*b + c*c - d*d,       2cd - 2ab      ],
                    [      2bd - 2ac      ,       2cd + 2ab      , a*a - b*b - c*c + d*d] ])


    Parameters
    ----------
    x : list
        list of points
    y : list
        list of points

    Returns
    -------
    T : numpy.array
        array (R,t) which correspond to the optimal rotation and translation

    Notes
    -----
    .. math::

        T = | R t |
            | 0 1 |

    with T.shape(4,4)

    Example
    -------
    >>> from timagetk.algorithms.reconstruction import pts2transfo
    >>> # x and y, two point sets with 7 known correspondences
    >>> x = [[238.*0.200320, 196.*0.200320, 9.],
             [204.*0.200320, 182.*0.200320, 11.],
             [180.*0.200320, 214.*0.200320, 12.],
             [201.*0.200320, 274.*0.200320, 12.],
             [148.*0.200320, 225.*0.200320, 18.],
             [248.*0.200320, 252.*0.200320, 8.],
             [305.*0.200320, 219.*0.200320, 10.]]
    >>> y = [[173.*0.200320, 151.*0.200320, 17.],
             [147.*0.200320, 179.*0.200320, 16.],
             [165.*0.200320, 208.*0.200320, 12.],
             [226.*0.200320, 204.*0.200320, 9.],
             [170.*0.200320, 254.*0.200320, 10.],
             [223.*0.200320, 155.*0.200320, 13.],
             [218.*0.200320, 109.*0.200320, 23.]]
    >>> pts2transfo(x, y)
    array([[  0.40710149,   0.89363883,   0.18888626, -22.0271968 ],
           [ -0.72459862,   0.19007589,   0.66244094,  51.59203463],
           [  0.55608022,  -0.40654742,   0.72490964,  -0.07837002],
           [  0.        ,   0.        ,   0.        ,   1.        ]])
    """
    # compute barycenters
    # nx vectors of dimension kx
    if not isinstance(x, np.ndarray):
        x = np.array(x)
    nx, kx = x.shape
    x_barycenter = x.sum(0) / float(nx)

    # nx vectors of dimension kx
    if not isinstance(y, np.ndarray):
        y = np.array(y)
    ny, ky = y.shape
    y_barycenter = y.sum(0) / float(ny)

    # Check there are the same number of vectors
    assert nx == ny

    # converting to barycentric coordinates
    x = x - x_barycenter
    y = y - y_barycenter

    # Change of basis (y -> x)
    # ~ y = y - x_barycenter

    # compute of A = yi*q - q*xi
    #             = array([ [       0       ,  (xn_i - yn_i) , (xn_j - yn_j)  ,  (xn_k - yn_k) ],
    #                       [-(xn_i - yn_i) ,        0       , (-xn_k - yn_k) ,  (xn_j + yn_j) ],
    #                       [-(xn_j - yn_j) , -(-xn_k - yn_k),      0         ,  (-xn_i - yn_i)],
    #                       [-(xn_k - yn_k) , -(xn_j + yn_j) , -(-xn_i - yn_i),         0      ] ])
    #

    arr_a = np.zeros([nx, 4, 4])

    arr_a[:, 0, 1] = x[:, 0] - y[:, 0]
    arr_a[:, 0, 2] = x[:, 1] - y[:, 1]
    arr_a[:, 0, 3] = x[:, 2] - y[:, 2]

    arr_a[:, 1, 0] = -arr_a[:, 0, 1]
    arr_a[:, 1, 2] = -x[:, 2] - y[:, 2]
    arr_a[:, 1, 3] = x[:, 1] + y[:, 1]

    arr_a[:, 2, 0] = -arr_a[:, 0, 2]
    arr_a[:, 2, 1] = -arr_a[:, 1, 2]
    arr_a[:, 2, 3] = -x[:, 0] - y[:, 0]

    arr_a[:, 3, 0] = -arr_a[:, 0, 3]
    arr_a[:, 3, 1] = -arr_a[:, 1, 3]
    arr_a[:, 3, 2] = -arr_a[:, 2, 3]

    # compute of B = Sum [A^T.A]
    arr_b = np.zeros([nx, 4, 4])
    arr_a_t = arr_a.transpose(0, 2, 1)

    # Maybe there is an another way to do not the "FOR" loop
    for i in range(nx):
        arr_b[i] = np.dot(arr_a_t[i], arr_a[i])

    arr_b = arr_b.sum(0)

    # The solution q minimizes the sum of the squares of the errors : C(R) = q^T.B.q is done by
    # the eigenvector corresponding to the biggest eigenvalue of the matrix -B

    eig_val, eig_vect = np.linalg.eig(-arr_b)
    max_ind = np.argmax(eig_val)
    # The orthogonal matrix corresponding to a rotation by the unit quaternion q = a + bi + cj + dk (with |q| = 1) is given by
    #   R = array([ [a*a + b*b - c*c - d*d,       2bc - 2ad      ,       2bd + 2ac],
    #               [      2bc + 2ad      , a*a - b*b + c*c - d*d,       2cd - 2ab],
    #               [      2bd - 2ac      ,       2cd + 2ab      , a*a - b*b - c*c + d*d] ])
    #

    # eigenvector corresponding to the biggest eigenvalue
    vm = eig_vect[:, max_ind]

    arr = np.zeros([3, 3])
    arr[0, 0] = vm[0] * vm[0] + vm[1] * vm[1] - vm[2] * vm[2] - vm[3] * vm[3]
    arr[0, 1] = 2 * vm[1] * vm[2] - 2 * vm[0] * vm[3]
    arr[0, 2] = 2 * vm[1] * vm[3] + 2 * vm[0] * vm[2]

    arr[1, 0] = 2 * vm[1] * vm[2] + 2 * vm[0] * vm[3]
    arr[1, 1] = vm[0] * vm[0] - vm[1] * vm[1] + vm[2] * vm[2] - vm[3] * vm[3]
    arr[1, 2] = 2 * vm[2] * vm[3] - 2 * vm[0] * vm[1]

    arr[2, 0] = 2 * vm[1] * vm[3] - 2 * vm[0] * vm[2]
    arr[2, 1] = 2 * vm[2] * vm[3] + 2 * vm[0] * vm[1]
    arr[2, 2] = vm[0] * vm[0] - vm[1] * vm[1] - vm[2] * vm[2] + vm[3] * vm[3]

    # Compute of the matrix (R,t) which correspond to the optimal rotation and translation
    #  M = | R t | = array(4,4)
    #      | 0 1 |
    #

    # compute the optimal translation
    t = y_barycenter - np.dot(arr, x_barycenter)

    T = np.zeros([4, 4])
    T[0:3, 0:3] = arr
    T[0:3, 3] = t
    T[3, 3] = 1.

    return T


def matrix_real2voxels(matrix, target_res, source_res):
    """
    Converts a transform matrix (M) expressed in real coordinates
    (a transform from space_r to space_r) into a matrix M' from space_1 to space_2
    where space_s is the voxel space from which M comes from and space_t the
    one where it will end, and space_r is the real common space.

    Parameters
    ----------
    matrix : numpy.array
        a 4x4 numpy.array.
    target_res : tuple
        a 3-uple of unit vectors for the space_2, e.g. : (1.,2.,1)
    source_res : tuple
        a 3-uple of unit vectors for the space_1, e.g. : (2.,1.,3)

    Returns
    -------
    numpy.array
        matrix in "voxel" coordinates (M' mapping space_1 to space_2 , instead of space_r to space_r).
    """
    assert matrix.shape == (4, 4)
    # vx_t, vy_t, vz_t = target_res
    # vx_s, vy_s, vz_s = source_res

    # TODO: this is wrong, no time to check why just now --
    # return np.array( [ [matrix[0,0]* vx_s /vx_t, matrix[0,1]* vx_s /vy_t, matrix[0,2]* vx_s /vz_t, matrix[0,3]/ vx_t],
    #                    [matrix[1,0]* vy_s /vx_t, matrix[1,1]* vy_s /vy_t, matrix[1,2]* vy_s /vz_t, matrix[1,3]/ vy_t],
    #                    [matrix[2,0]* vz_s /vx_t, matrix[2,1]* vz_s /vy_t, matrix[2,2]* vz_s /vz_t, matrix[2,3]/ vz_t],
    #                    [0.,                      0.,                      0.,                                  1.] ] )

    res = matrix.copy()
    h_out = np.diag(source_res)
    res[0:3, 0:3] = np.dot(res[0:3, 0:3], h_out)

    size_in = list(map(lambda x: 1. / x, target_res))
    h_in = np.diag(size_in)
    res[0:3, :] = np.dot(h_in, res[0:3, :])
    assert (res[3, 0:3] == (0, 0, 0)).all()
    assert res[3, 3] == 1
    return res