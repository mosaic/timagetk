#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

"""
This example illustrate the impact of grayscale image pre-processing on watershed algorithm.
Seed image is the same in all cases and is obtained from the z-slice contrasted and smoothed image.
"""

from matplotlib import gridspec
from matplotlib.pyplot import imshow
from matplotlib.pyplot import show
from matplotlib.pyplot import subplot
from matplotlib.pyplot import title
from timagetk.io import imread
from timagetk.algorithms.exposure import global_contrast_stretch
from timagetk.algorithms.linearfilter import linearfilter
from timagetk.algorithms.morphology import morphology
from timagetk.algorithms.regionalext import regionalext
from timagetk.algorithms.connexe import connexe
from timagetk.algorithms.watershed import watershed
from timagetk.io.util import shared_data

input_img = imread(shared_data('p58-t0-a0.lsm', "p58"))
# - Get the middle z-slice to speed up the computation:
n_zslices = input_img.get_shape('z')
middle_z = int(n_zslices / 2)

# ------------------------------------------------------------------------------
# - SEED EXTRACTION
# ------------------------------------------------------------------------------
input_img = global_contrast_stretch(input_img)

sigma = 1.0
smooth_img = linearfilter(input_img, method='smoothing', sigma=sigma, real=True)

max_rad = 1
asf_img = morphology(smooth_img, max_radius=max_rad,
                     method='co_alternate_sequential_filter')
h_min = 15
ext_img = regionalext(smooth_img, h=h_min, method='minima')

low_th = 1
high_th = h_min
con_img = connexe(ext_img, low_threshold=low_th,
                          high_threshold=high_th,
                          method='connected_components')

# ------------------------------------------------------------------------------
# - SEEDED WATERSHED
# ------------------------------------------------------------------------------
control = 'most'
seg_img1 = watershed(input_img, con_img, control=control,
                        method='seeded_watershed')


seg_img2 = watershed(smooth_img, con_img, control=control,
                        method='seeded_watershed')


seg_img3 = watershed(asf_img, con_img, control=control,
                        method='seeded_watershed')


# ------------------------------------------------------------------------------
# - PLOT GRAYSCALE IMAGES
# ------------------------------------------------------------------------------
n_col = 3
n_row = 2
figure(figsize=[4.5 * n_col, 4 * n_row])
gs = gridspec.GridSpec(n_row, n_col)

# - Create a view of the z-slice contrast stretching:
subplot(gs[0, 0])
imshow(input_img.get_slice(middle_z, 'z').get_array(), cmap="gray", vmin=0, vmax=255)
title('Contrast stretched')
axis('off')

# - Create a view of the smoothed grayscale image:
subplot(gs[0, 1])
imshow(smooth_img.get_slice(middle_z, 'z').get_array(), cmap="gray", vmin=0, vmax=255)
title('Gaussian smoothing (sigma={})'.format(sigma))
axis('off')

# - Create a view of the grayscale image after Closing-Opening ASF:
subplot(gs[0, 2])
imshow(asf_img.get_slice(middle_z, 'z').get_array(), cmap="gray", vmin=0, vmax=255)
title('Closing-Opening ASF (max_radius={})'.format(max_rad))
axis('off')


# ------------------------------------------------------------------------------
# - PLOT WATERSHED RESULTS
# ------------------------------------------------------------------------------
# - Create a view of the segmented image based on contrast stretched image:
subplot(gs[1, 0])
imshow(seg_img1.get_slice(middle_z, 'z').get_array(), cmap="prism")
title('Watershed (contrast, {})'.format(control))
axis('off')

# - Create a view of the segmented image based on smoothed image:
subplot(gs[1, 1])
imshow(seg_img2.get_slice(middle_z, 'z').get_array(), cmap="prism")
title('Watershed (smoothed, {})'.format(control))
axis('off')

# - Create a view of the segmented image based on C/O ASF image:
subplot(gs[1, 2])
imshow(seg_img3.get_slice(middle_z, 'z').get_array(), cmap="prism")
title('Watershed (co-asf, {})'.format(control))
axis('off')

tight_layout()
show()
