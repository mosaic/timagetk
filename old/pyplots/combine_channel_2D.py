# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
from timagetk.io import imread
from timagetk.plugins import registration
from timagetk.io.util import shared_data
from timagetk.components.multi_channel import combine_channels

float_img = imread(shared_data('time_0_cut.inr'))
ref_img = imread(shared_data('time_1_cut.inr'))
trsf_rig, res_rig = registration(float_img, ref_img, method='rigid')
blend = combine_channels([ref_img, res_rig])

plt.imshow(blend[:, :, 5, :].transpose((1, 0, 2)))
plt.title('Channel combination example - Rigid registration')
plt.show()
