#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------


"""
This example shows how to performs seeded watershed segmentation on grey level images.
"""
import numpy as np
from matplotlib import gridspec
from matplotlib.pyplot import figure
from matplotlib.pyplot import imshow
from matplotlib.pyplot import show
from matplotlib.pyplot import subplot
from matplotlib.pyplot import suptitle
from matplotlib.pyplot import tight_layout
from matplotlib.pyplot import title
from timagetk.algorithms.exposure import slice_contrast_stretch
from timagetk.io import imread
from timagetk.plugins import h_transform
from timagetk.plugins import linear_filtering
from timagetk.plugins import region_labeling
from timagetk.plugins import segmentation
from timagetk.io.util import shared_data

input_img = imread(shared_data('p58-t0-a0.lsm', "p58"))

# - Crop around the middle z-slice to speed up the computation:
n_zslices = input_img.get_shape('z')
middle_z = int(n_zslices / 2)
xsh, ysh, zsh = input_img.shape
region = [0, xsh-1, 0, ysh-1, middle_z-5, middle_z+5]
img = input_img.get_region(region)

# - Performs z-slice contrast stretching:
img = slice_contrast_stretch(img)

# - Performs Gaussian smoothing:
sigma = 1.0
smooth_img = linear_filtering(img, sigma=sigma, real=False,
                              method='gaussian_smoothing')

# - Performs the Height-transform:
h_min = 30
ext_img = h_transform(img, h=h_min, method='min')

# - Performs local minima detection and labelling:
low_th = 1
high_th = h_min
con_img = region_labeling(ext_img, low_threshold=low_th,
                          high_threshold=high_th,
                          method='connected_components')
np.unique(con_img)
# ------------------------------------------------------------------------------
# - Show the effects of changing 'control' parameter in 'seeded_watershed':
# ------------------------------------------------------------------------------
from timagetk.plugins.segmentation import WATERSHED_CONTROLS

# List possible control methods of seeded watershed algorithm:
print(WATERSHED_CONTROLS)

n_col = len(WATERSHED_CONTROLS) + 1
figure(figsize=[4 * n_col, 4])
suptitle('ZOOM-IN [100:200, 100:200]')
gs = gridspec.GridSpec(1, n_col)

subplot(gs[0, 0])
crop_im = img.get_slice(5, 'z').get_array()[100:200, 100:200]
imshow(crop_im, cmap="gray", vmin=0, vmax=255)
title('Contrast stretched')

for n, control in enumerate(WATERSHED_CONTROLS):
    # - Create a view of the segmented image:
    seg_img = segmentation(smooth_img, con_img, control=control,
                           method='seeded_watershed')
    subplot(gs[0, n + 1])
    crop_seg = seg_img.get_slice(5, 'z').get_array()[100:200, 100:200]
    imshow(crop_seg, cmap="prism")
    title('Seeded watershed (control={})'.format(control))

tight_layout()
show()
