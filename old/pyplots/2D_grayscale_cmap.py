# -*- coding: utf-8 -*-

"""
2D grayscale image display example.
"""
from timagetk.io import imread
from timagetk.io.util import shared_data
from timagetk.visu.mplt import grayscale_imshow

fname = 'p58-t0-a0.lsm'
img = imread(shared_data(fname, 'p58'))

# - Get the middle z-slice:
zsh = img.get_shape('z')
middle_z = int(zsh / 2)

# Use the same z-slice image twice with different colormaps:
cm = ['gray', 'viridis']
grayscale_imshow([img, img], slice_id=middle_z, title=cm, val_range='auto',
                 cmap=cm, suptitle="Colormap selection")
