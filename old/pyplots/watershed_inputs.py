#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

"""
This exemple shows how to performs seeded watershed segmentation on grey level images.
"""
from matplotlib import gridspec
from matplotlib.pyplot import axis
from matplotlib.pyplot import figure
from matplotlib.pyplot import imshow
from matplotlib.pyplot import show
from matplotlib.pyplot import subplot
from matplotlib.pyplot import tight_layout
from matplotlib.pyplot import title
from timagetk.algorithms.exposure import slice_contrast_stretch
from timagetk.io import imread
from timagetk.plugins import h_transform
from timagetk.plugins import linear_filtering
from timagetk.plugins import morphology
from timagetk.plugins import region_labeling
from timagetk.plugins import segmentation
from timagetk.io.util import shared_data

input_img = imread(shared_data('p58-t0-a0.lsm', "p58"))

# - Crop around the middle z-slice to speed up the computation:
n_zslices = input_img.get_shape('z')
middle_z = int(n_zslices / 2)
img = input_img.get_slice(middle_z, 'z')


# - SEED extraction:
# - Contrast stretching of the original image:
contrast_img = slice_contrast_stretch(img.to_3d())
contrast_img = contrast_img.to_2d()

# - Smoothing the contrasted image:
sigma = 1.0
smooth_img = linear_filtering(contrast_img, sigma=sigma, real=False,
                              method='gaussian_smoothing')

# - Perform height-transform:
h_min = 10
ext_img = h_transform(smooth_img, h=h_min, method='min')

# - Performs local minima detection and labelling:
low_th = 1
high_th = h_min
con_img = region_labeling(ext_img, low_threshold=low_th,
                          high_threshold=high_th,
                          method='connected_components')
wat_control = "most"
n_col = 3
n_row = 5
figure(figsize=[4 * n_col, 4 * n_row])
gs = gridspec.GridSpec(n_row, n_col)

# ------------------------------------------------------------------------------
# - ORIGINAL IMAGE input:
# ------------------------------------------------------------------------------
# - Create a view of the original z-slice:
subplot(gs[0, 0])
imshow(img[100:200, 100:200].get_array(), cmap="gray", vmin=0, vmax=255)
title("original z-slice (z {}/{})".format(middle_z, n_zslices))
axis('off')

# - Create a view of the local minima detection and labelling:
subplot(gs[0, 1])
imshow(con_img[100:200, 100:200].get_array(), cmap="prism")
title('Local minima & labelling ({}-{})'.format(low_th, high_th))
axis('off')

# - Create a view of the segmented image:
seg_img = segmentation(img, con_img, control=wat_control,
                       method='seeded_watershed')
subplot(gs[0, 2])
imshow(seg_img[100:200, 100:200].get_array(), cmap="prism")
title('Seeded watershed (control={})'.format(wat_control))
axis('off')

# ------------------------------------------------------------------------------
# - CONTRASTED IMAGE input:
# ------------------------------------------------------------------------------
# - Create a view of the z-slice contrast stretching:
subplot(gs[1, 0])
imshow(contrast_img[100:200, 100:200].get_array(), cmap="gray", vmin=0, vmax=255)
title('Contrast stretched')
axis('off')

# - Create a view of the local minima detection and labelling:
subplot(gs[1, 1])
imshow(con_img[100:200, 100:200].get_array(), cmap="prism")
title('Local minima & labelling ({}-{})'.format(low_th, high_th))
axis('off')

# - Create a view of the segmented image:
seg_img = segmentation(contrast_img, con_img, control=wat_control,
                       method='seeded_watershed')
subplot(gs[1, 2])
imshow(seg_img[100:200, 100:200].get_array(), cmap="prism")
title('Seeded watershed (control={})'.format(wat_control))
axis('off')

# ------------------------------------------------------------------------------
# - ORIGINAL SMOOTHED IMAGE input:
# ------------------------------------------------------------------------------
# - Create a view of the smoothed image:
subplot(gs[2, 0])
imshow(smooth_img[100:200, 100:200].get_array(), cmap="gray", vmin=0, vmax=255)
title('Gaussian smoothing (sigma={})'.format(sigma))
axis('off')

# - Create a view of the local minima detection and labelling:
subplot(gs[2, 1])
imshow(con_img[100:200, 100:200].get_array(), cmap="prism")
title('Local minima & labelling ({}-{})'.format(low_th, high_th))
axis('off')

# - Create a view of the segmented image:
seg_img = segmentation(smooth_img, con_img, control=wat_control,
                       method='seeded_watershed')
subplot(gs[2, 2])
imshow(seg_img[100:200, 100:200].get_array(), cmap="prism")
title('Seeded watershed (control={})'.format(wat_control))
axis('off')

# ------------------------------------------------------------------------------
# - CONTRASTED & SMOOTHED IMAGE input:
# ------------------------------------------------------------------------------
# - Create a view of the smoothed image:
sigma = 1.0
smooth_img = linear_filtering(contrast_img, sigma=sigma, real=False,
                              method='gaussian_smoothing')
subplot(gs[3, 0])
imshow(smooth_img[100:200, 100:200].get_array(), cmap="gray", vmin=0, vmax=255)
title('Contrasted & smoothed')
axis('off')

# - Create a view of the local minima detection and labelling:
subplot(gs[3, 1])
imshow(con_img[100:200, 100:200].get_array(), cmap="prism")
title('Local minima & labelling ({}-{})'.format(low_th, high_th))
axis('off')

# - Create a view of the segmented image:
seg_img = segmentation(smooth_img, con_img, control=wat_control,
                       method='seeded_watershed')
subplot(gs[3, 2])
imshow(seg_img[100:200, 100:200].get_array(), cmap="prism")
title('Seeded watershed (control={})'.format(wat_control))
axis('off')

# ------------------------------------------------------------------------------
# - CONTRASTED & SMOOTHED & CLOSING/OPENING ASF IMAGE input:
# ------------------------------------------------------------------------------
# - Create a view of the grayscale image after Closing-Opening ASF:
max_rad = 1
asf_img = morphology(smooth_img, max_radius=max_rad,
                     method='co_alternate_sequential_filter')
subplot(gs[4, 0])
imshow(smooth_img[100:200, 100:200].get_array(), cmap="gray", vmin=0, vmax=255)
title('Constrast & smooth & co-asf')
axis('off')

# - Create a view of the local minima detection and labelling:
subplot(gs[4, 1])
imshow(con_img[100:200, 100:200].get_array(), cmap="prism")
title('Local minima & labelling ({}-{})'.format(low_th, high_th))
axis('off')

# - Create a view of the segmented image:
seg_img = segmentation(asf_img, con_img, control=wat_control,
                       method='seeded_watershed')
subplot(gs[4, 2])
imshow(seg_img[100:200, 100:200].get_array(), cmap="prism")
title('Seeded watershed (control={})'.format(wat_control))
axis('off')

tight_layout()
show()
