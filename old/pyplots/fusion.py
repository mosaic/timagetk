#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------


"""
This shows initial multi-angle images and fusion.
"""

from matplotlib.pyplot import axis
from matplotlib.pyplot import imshow
from matplotlib.pyplot import show
from matplotlib.pyplot import title
from timagetk.io import imread
from timagetk.plugins.fusion import fusion_on_first
from timagetk.algorithms.resample import resample
from timagetk.io.util import shared_data
# Defines files location:
from timagetk.visu.mplt import grayscale_imshow
from timagetk.components.multi_channel import combine_channels

files = [shared_data("time_0_cut.inr"),
         shared_data("time_0_cut_rotated1.mha"),
         shared_data("time_0_cut_rotated2.mha")]
# ref_fusion_file = shared_data("time_0_cut_fused.mha")

# Defines downsampling factor (speed up computation):
downsampling = 2.

# Loop reading and downsampling the images to fuse:
img_list = []
for f in files:
    print(f)
    img = imread(f)
    print(img.voxelsize)
    ds_vxs = [vox * downsampling for vox in img.voxelsize]
    img_list.append(resample(img, voxelsize=ds_vxs, interpolation='grey'))
    print(img_list[-1].voxelsize)


# Performs multi-angle fusion:
trsf_list, reg_img_list = fusion_on_first(img_list, registration_method="rigid")


# - Get the middle z-slice:
n_zslices = img_list[0].get_shape('z')
middle_z = int(n_zslices / 2)

grayscale_imshow(img_list, slice_id=middle_z, axis='z', cmap="gray", title=["Reference", "Angle 1", "Angle 2"])

grayscale_imshow(reg_img_list, slice_id=middle_z, axis='z', cmap="gray", title=["Reference", "Angle 1", "Angle 2"])

# Display fusion image:
imshow(combine_channels(reg_img_list, colors=('red', 'green', 'blue'))[:, :, middle_z, :])
title("Rigid registration")
axis('off')
show()