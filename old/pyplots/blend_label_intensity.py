# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
from timagetk.io import imread
from timagetk.plugins.segmentation import auto_seeded_watershed
from timagetk.io.util import shared_data
from timagetk.components.multi_channel import label_blending

img = imread(shared_data('time_0_cut.inr'))
z_slice = 20
sub_img = img.get_slice(z_slice, 'z')
seg_img = auto_seeded_watershed(sub_img, hmin=5, sigma=0.3, real=True)
blend = label_blending(seg_img, sub_img)

plt.imshow(blend)
plt.title('Label & intensity images blending')
plt.show()
