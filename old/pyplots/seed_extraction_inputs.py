#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

"""
These examples shows how to performs seeded watershed segmentation on grey level images.
"""
from matplotlib import gridspec
from matplotlib.pyplot import axis
from matplotlib.pyplot import figure
from matplotlib.pyplot import imshow
from matplotlib.pyplot import show
from matplotlib.pyplot import subplot
from matplotlib.pyplot import tight_layout
from matplotlib.pyplot import title
from timagetk.algorithms.exposure import slice_contrast_stretch
from timagetk.io import imread
from timagetk.plugins import h_transform
from timagetk.plugins import linear_filtering
from timagetk.plugins import morphology
from timagetk.plugins import region_labeling
from timagetk.io.util import shared_data

input_img = imread(shared_data('p58-t0-a0.lsm', "p58"))
# - Crop around the middle z-slice to speed up the computation:
n_zslices = input_img.get_shape('z')
middle_z = int(n_zslices / 2)
input_img = input_img.get_slice(middle_z, 'z')

n_col = 3
n_row = 5
figure(figsize=[4 * n_col, 4 * n_row])
gs = gridspec.GridSpec(n_row, n_col)

# ------------------------------------------------------------------------------
# - ORIGINAL IMAGE input:
# ------------------------------------------------------------------------------
# - Create a view of the original z-slice:
subplot(gs[0, 0])
imshow(input_img[100:200, 100:200].get_array(), cmap="gray", vmin=0, vmax=255)
title("original z-slice (z {}/{})".format(middle_z, n_zslices))
axis('off')

# - Create a view of the Height-transform:
h_min = 15
ext_img = h_transform(input_img, h=h_min, method='min')

subplot(gs[0, 1])
imshow(ext_img[100:200, 100:200].get_array(), cmap="viridis")
title('Height-transform (h_min={})'.format(h_min))
axis('off')

# - Create a view of the local minima detection and labelling:
low_th = 1
high_th = h_min
con_img = region_labeling(ext_img, low_threshold=low_th,
                          high_threshold=high_th,
                          method='connected_components')

subplot(gs[0, 2])
imshow(con_img[100:200, 100:200].get_array(), cmap="prism")
title('Local minima & labelling ({}-{})'.format(low_th, high_th))
axis('off')

# ------------------------------------------------------------------------------
# - CONTRASTED IMAGE input:
# ------------------------------------------------------------------------------
# - Create a view of the z-slice contrast stretching:
contrast_img = slice_contrast_stretch(input_img.to_3d())
contrast_img = contrast_img.to_2d()

subplot(gs[1, 0])
imshow(contrast_img[100:200, 100:200].get_array(), cmap="gray", vmin=0, vmax=255)
title('Contrast stretched')
axis('off')

# - Create a view of the Height-transform:
h_min = 15
ext_img = h_transform(contrast_img, h=h_min, method='min')

subplot(gs[1, 1])
imshow(ext_img[100:200, 100:200].get_array(), cmap="viridis")
title('Height-transform (h_min={})'.format(h_min))
axis('off')

# - Create a view of the local minima detection and labelling:
low_th = 1
high_th = h_min
con_img = region_labeling(ext_img, low_threshold=low_th,
                          high_threshold=high_th,
                          method='connected_components')

subplot(gs[1, 2])
imshow(con_img[100:200, 100:200].get_array(), cmap="prism", vmin=2)
title('Local minima & labelling ({}-{})'.format(low_th, high_th))

# ------------------------------------------------------------------------------
# - ORIGINAL SMOOTHED IMAGE input:
# ------------------------------------------------------------------------------
# - Create a view of the smoothed image:
sigma = 1.0
smooth_img = linear_filtering(input_img, sigma=sigma, real=False,
                              method='gaussian_smoothing')

subplot(gs[2, 0])
imshow(smooth_img[100:200, 100:200].get_array(), cmap="gray", vmin=0, vmax=255)
title('Gaussian smoothing (sigma={})'.format(sigma))
axis('off')

# - Create a view of the Height-transform:
h_min = 15
ext_img = h_transform(smooth_img, h=h_min, method='min')

subplot(gs[2, 1])
imshow(ext_img[100:200, 100:200].get_array(), cmap="viridis")
title('Height-transform (h_min={})'.format(h_min))
axis('off')

# - Create a view of the local minima detection and labelling:
low_th = 1
high_th = h_min
con_img = region_labeling(ext_img, low_threshold=low_th,
                          high_threshold=high_th,
                          method='connected_components')

subplot(gs[2, 2])
imshow(con_img[100:200, 100:200].get_array(), cmap="prism", vmin=2)
title('Local minima & labelling ({}-{})'.format(low_th, high_th))
axis('off')

# ------------------------------------------------------------------------------
# - CONTRASTED & SMOOTHED IMAGE input:
# ------------------------------------------------------------------------------
# - Create a view of the smoothed image:
sigma = 1.0
smooth_img = linear_filtering(contrast_img, sigma=sigma, real=False,
                              method='gaussian_smoothing')

subplot(gs[3, 0])
imshow(smooth_img[100:200, 100:200].get_array(), cmap="gray", vmin=0, vmax=255)
title('Contrast & Smoothed'.format(sigma))
axis('off')

# - Create a view of the Height-transform:
h_min = 15
ext_img = h_transform(smooth_img, h=h_min, method='min')

subplot(gs[3, 1])
imshow(ext_img[100:200, 100:200].get_array(), cmap="viridis")
title('Height-transform (h_min={})'.format(h_min))
axis('off')

# - Create a view of the local minima detection and labelling:
low_th = 1
high_th = h_min
con_img = region_labeling(ext_img, low_threshold=low_th,
                          high_threshold=high_th,
                          method='connected_components')

subplot(gs[3, 2])
imshow(con_img[100:200, 100:200].get_array(), cmap="prism", vmin=2)
title('Local minima & labelling ({}-{})'.format(low_th, high_th))
axis('off')

# ------------------------------------------------------------------------------
# - CONTRASTED & SMOOTHED & CLOSING/OPENING ASF IMAGE input:
# ------------------------------------------------------------------------------
# - Create a view of the grayscale image after Closing-Opening ASF:
max_rad = 1
asf_img = morphology(smooth_img, max_radius=max_rad,
                     method='co_alternate_sequential_filter')

subplot(gs[4, 0])
imshow(asf_img[100:200, 100:200].get_array(), cmap="gray", vmin=0, vmax=255)
title('Constrast & smooth & co-asf')
axis('off')

# - Create a view of the Height-transform:
h_min = 15
ext_img = h_transform(asf_img, h=h_min, method='min')

subplot(gs[4, 1])
imshow(ext_img[100:200, 100:200].get_array(), cmap="viridis")
title('Height-transform (h_min={})'.format(h_min))
axis('off')

# - Create a view of the local minima detection and labelling:
low_th = 1
high_th = h_min
con_img = region_labeling(ext_img, low_threshold=low_th,
                          high_threshold=high_th,
                          method='connected_components')

subplot(gs[4, 2])
imshow(con_img[100:200, 100:200].get_array(), cmap="prism", vmin=2)
title('Local minima & labelling ({}-{})'.format(low_th, high_th))
axis('off')

tight_layout()
show()
