#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Regional Extrema Detection Module

This module provides tools for detecting local minima and maxima in intensity images.
It is particularly useful for image processing tasks, such as identifying regions or *seeds*
for segmentation algorithms like the watershed algorithm.

Key Features

- Detection of local minima in a membrane-marked intensity image, aiding in locating "seed" points.
- Detection of local maxima in a nuclei-marked intensity image, aiding in identifying nuclei positions.
- Supports 2D and 3D images with varying connectivity options (e.g., 4, 6, 8, 18, 26).
- Flexible method keyword options for detecting "minima" or "maxima."
- Compatibility with multi-channel images, allowing specific channel selection.
- Provides labelled images as outputs, indicating detected regions.

Usage Examples

Example 1 - Detect regional minima in a 3D image:

>>> import numpy as np
>>> from timagetk.io.dataset import shared_data
>>> from timagetk.algorithms.regionalext import regional_extrema
>>> image = shared_data('flower_confocal', 0)
>>> regmin_img = regional_extrema(image, height=3, method='minima')
>>> np.unique(regmin_img)

Example 2 - Detect regional minima in a 2D image slice:

>>> zslice = 10
>>> subimg = image.get_slice(zslice, 'z')  # Extract a z-slice of the 3D image
>>> hmins = [10, 25, 50]  # Heights for detection
>>> extmin_imgs = {h: regional_extrema(subimg, h, "minima", connectivity=8) for h in hmins}
>>> images = [subimg] + [extmin_imgs[h] for h in hmins]
>>> titles = ["Original"] + ["H-minima (h: {})".format(h) for h in hmins]

The module simplifies the process of detecting regional extrema, making it a valuable tool for
image segmentation and analysis tasks.
"""

import numpy as np
from vt import regionalext as vt_regionalext

from timagetk.bin.logger import get_logger
from timagetk.components.image import assert_spatial_image
from timagetk.components.labelled_image import LabelledImage
from timagetk.tasks.decorators import singlechannel_wrapper
from timagetk.third_party.vt_converter import _new_from_vtimage
from timagetk.third_party.vt_parser import DEF_RE_METHOD
from timagetk.third_party.vt_parser import general_kwargs
from timagetk.third_party.vt_parser import parallel_kwargs
from timagetk.third_party.vt_parser import regionalext_kwargs

log = get_logger(__name__)


@singlechannel_wrapper
def regional_extrema(image, height, method=DEF_RE_METHOD, **kwargs):
    """Regional extrema detection algorithm.

    Valid `method` values are:

      - **minima**, for detection of connected regional minima
      - **maxima**, for detection of connected regional maxima

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        Input image to use for regional extrema detection.
    height : int or float
        The height of the regional extrema to detect.
    method : {'minima', 'maxima'}
        The method to use for regional extrema detection, ``DEF_RE_METHOD`` by default.

    Other Parameters
    ----------------
    channel : str
        If a ``MultiChannelImage`` is used as input image, select the channel to use with this algorithm.
    connectivity : {4, 6, 8, 10, 18, 26}
        The connectivity of the structuring elements, ``DEF_CONNECTIVITY`` by default.
        Connectivity is among the 4-, 6-, 8-, 18-, 26-neighborhoods.
        4 and 8 are for 2D images, the others for 3D images.
    params : str
        CLI parameter string used by ``vt.cell_filter`` method.

    Returns
    -------
    timagetk.LabelledImage
        Image with labelled regional minima or maxima.

    Raises
    ------
    TypeError
        If ``image`` is not a ``timagetk.LabelledImage``.
    ValueError
        If the image returned by ``vt.regionalext`` is ``None``.

    See Also
    --------
    timagetk.third_party.vt_parser.REGIONALEXT_METHODS
    timagetk.third_party.vt_parser.DEF_RE_METHOD
    timagetk.third_party.vt_parser.DEF_CONNECTIVITY
    timagetk.third_party.vt_parser.regionalext_kwargs
    timagetk.third_party.vt_parser.general_kwargs
    timagetk.third_party.vt_parser.parallel_kwargs

    Notes
    -----
    A regional minimum, resp. maximum, *M* of an image *I* at elevation *t* is a connected component of pixels
    (or voxels) with the value *t* whose external boundary pixels (or voxels) have a value strictly greater, resp.
    less, than *t* [Soille]_.
    The result image is then a binary image, where ``1`` indicate a regional minimum, resp. maximum.

    Providing a contrast criterion *h* remove the regional minima, resp. maxima, whose depth is higher, resp. lower,
    to this threshold level *h*.
    It is then called a *h-minima* transformation, resp. *h-maxima*.
    The result image is then a height image, *i.e.* the pixels (or voxels) values correspond to the depth of the regional extrema.


    References
    ----------
    .. [Soille] `Morphological Image Analysis: Principles and Applications, P. Soille  <https://books.google.fr/books?id=ZFzxCAAAQBAJ&lpg=PA201&ots=-p8--YG2em&dq=regional%20extrema%20image%20analysis&hl=fr&pg=PA201#v=onepage&q=regional%20extrema%20image%20analysis&f=false>`_

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.regionalext import regional_extrema
    >>> from timagetk.visu.mplt import grayscale_imshow

    >>> # Example #1 - height-5 local minima detection on 3D image:
    >>> image = shared_data('flower_confocal', 0)
    >>> regmin_img = regional_extrema(image, height=3, method='minima')
    >>> np.unique(regmin_img)

    >>> # Example #2 - local minima detection on 2D image for various height values:
    >>> zslice = 10
    >>> subimg = image.get_slice(zslice, 'z')
    >>> hmins = [10, 25, 50]
    >>> n_hmins = len(hmins)
    >>> extmin_imgs = {hmin: regional_extrema(subimg, hmin, "minima", connectivity=8) for hmin in hmins}
    >>> images = [subimg] + [extmin_imgs[hmin] for hmin in hmins]
    >>> titles = ["Original (z-slice: {})".format(zslice)] + ["H-minima (h: {})".format(hmin) for hmin in hmins]
    >>> fig = grayscale_imshow(images, title=titles, val_range=['type']+['auto']*n_hmins, cmap=['gray']+['viridis']*n_hmins)

    """
    from timagetk.components.image import get_image_attributes
    assert_spatial_image(image, obj_name='image')
    attr = get_image_attributes(image, exclude=["voxelsize"])

    # - Take care of extra parameters given as a string:
    extra_params = kwargs.pop("params", "")
    if not extra_params.startswith(" "):
        extra_params = " " + extra_params
    # - Parse regionalext parameters:
    params, kwargs = regionalext_kwargs(height, method, ndim=image.ndim, **kwargs)
    # - Parse general kwargs:
    params += general_kwargs(**kwargs)
    # - Parse parallelism kwargs:
    params += parallel_kwargs(**kwargs)
    # - Combine parsed parameters with extra parameters:
    params += extra_params
    log.debug(f"`vt.regionalext` params: '{params}'")

    vt_img = image.to_vt_image()
    out_img = vt_regionalext(vt_img, params)

    if out_img is None:
        null_msg = "VT method 'regionalext' returned NULL image!"
        raise ValueError(null_msg)
    else:
        arr, vxs, _, _ = _new_from_vtimage(out_img, copy=False)
        attr["dtype"] = 'uint8' if len(np.unique(arr)) < 255 else "uint16"
        return LabelledImage(arr, voxelsize=vxs, not_a_label=0, **attr)
