#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Signal Quantification Module

This module is designed to facilitate the quantification of image signal intensities in 3D labelled images,
offering robust functionality for image processing and analysis.
It is primarily useful for tasks that require precise measurements of signal intensities within segmented regions,
at interfaces between regions, or at specific 3D points, such as in biological tissue imaging.

Key Features

- **Quantify Cell Signal Intensity**:
  Compute the average intensity of signals within cells (or regions) of a 3D image.
- **Quantify Interface Signal Intensity**:
  Analyze signal intensities at the interfaces between adjacent cells, within a user-defined distance.
- **Quantify Nuclei Signal Intensity**:
  Measure signal intensities at specific nuclei points in 3D space, using Gaussian smoothing for refined values.
- **Support for Multichannel Images**:
  Extract intensity values across multiple channels of an image with clear separation of channel-specific signals.
- **Customizable Parameters**:
  Options for user-defined regions, erosion radii, and interface distances to tailor the signal quantification process.

Usage Examples

1. **Quantify Cell Signal Intensity**:
    ```python
    from timagetk import SpatialImage
    from timagetk.components.tissue_image import TissueImage3D

    signal_img = SpatialImage.load("path_to_signal_image.tif")
    tissue = TissueImage3D.load("path_to_segmented_image.tif")

    cell_signals = quantify_cell_signal_intensity(signal_img, tissue)
    print(cell_signals)  # Outputs: {cell_id: average_signal, ...}
    ```

2. **Quantify Interface Signal Intensity**:
    ```python
    interface_signals = quantify_cell_interface_signal_intensity(
        signal_img, tissue, interface_distance=2.0
    )
    print(interface_signals)  # Outputs: {(cell1, cell2): average_signal, ...}
    ```

3. **Quantify Nuclei Signal Intensity**:
    ```python
    import numpy as np
    nuclei_points = np.array([[10.0, 20.0, 30.0], [15.0, 25.0, 35.0]])  # XYZ coordinates

    nuclei_signals = quantify_nuclei_signal_intensity(signal_img, nuclei_points)
    print(nuclei_signals)  # Outputs: [intensity_at_nucleus1, intensity_at_nucleus2, ...]
    ```

This module streamlines the processing of 3D images, delivering flexibility and efficiency for quantifying
biological and other spatial signals.
"""

import numpy as np
import scipy.ndimage as nd
from tqdm.autonotebook import tqdm

from timagetk import MultiChannelImage
from timagetk import SpatialImage
from timagetk.algorithms.morphology import label_filtering
from timagetk.bin.logger import get_logger

log = get_logger(__name__)


def _average_cell_signals(signal_img, seg_img, cell_ids):
    """Average the computed cell signal by its volume.

    Parameters
    ----------
    signal_img : timagetk.SpatialImage
        Intensity image with the signal to quantify.
    seg_img : timagetk.LabelledImage
        Labelled image to use for signal quantification.
    cell_ids : list of int
        List of cell ids.

    Returns
    -------
    dict
        Cell signal intensity dictionary, ``{cid: signal(cid)}`` for ``cid`` in `cell_ids`

    """
    cell_total_signals = nd.sum(signal_img.get_array().astype(float), seg_img.get_array(), index=cell_ids)
    cell_volumes = nd.sum(np.ones_like(seg_img.get_array()), seg_img.get_array(), index=cell_ids)
    cell_signals = dict(zip(cell_ids, cell_total_signals / cell_volumes))
    return cell_signals


def quantify_cell_signal_intensity(signal_img, tissue, cell_ids=None, erosion_radius=0., disable_tqdm=False):
    """Quantify average image intensity inside 3D segmented_cells.

    Parameters
    ----------
    signal_img : timagetk.SpatialImage or timagetk.MultiChannelImage
        Intensity image with the signal to quantify.
    tissue : timagetk.TissueImage3D
        Labelled image to use for signal quantification.
    cell_ids : int or list of int, optional
        If ``None`` (default), returns values for all cell ids found in `tissue`.
        Else, should be a cell id of a list of cell ids.
        Note that given cell ids are checked for existence within the array.
    erosion_radius : float, optional
        Radius by which to erode cells before averaging signal.

    Returns
    -------
    dict
        Cell signal intensity dictionary, ``{cid: signal(cid)}`` for ``cid`` in `cell_ids`
        or ``{channel: {cid: signal(cid)} }`` for ``cid`` in `cell_ids` if multichannel.

    """
    if erosion_radius > 0:
        seg_img = label_filtering(tissue, method='erosion', radius=erosion_radius, iterations=1)
    else:
        seg_img = tissue

    if cell_ids is None:
        cell_ids = tissue.cell_ids()
    else:
        if any(c not in tissue.cell_ids() for c in cell_ids):
            log.warning("Removing cells that are not in the image!")
        cell_ids = [c for c in cell_ids if c in tissue.cell_ids()]

    cell_bboxes = tissue.boundingbox(labels=cell_ids)
    cell_signals = {}
    if isinstance(signal_img, MultiChannelImage):
        cell_signals = {channel_name: {} for channel_name in signal_img.channel_names}
    for c in tqdm(cell_ids, unit="cell", desc="Extracting cell average signals", disable=disable_tqdm):
        cell_seg_img = seg_img[cell_bboxes[c]]
        if isinstance(signal_img, SpatialImage):
            cell_signal_img = signal_img[cell_bboxes[c]]
            cell_signals.update(_average_cell_signals(cell_signal_img, cell_seg_img, cell_ids=[c]))
        elif isinstance(signal_img, MultiChannelImage):
            for channel_name in signal_img.channel_names:
                cell_signal_img = signal_img.get_channel(channel_name)[cell_bboxes[c]]
                cell_signals[channel_name].update(_average_cell_signals(cell_signal_img, cell_seg_img, cell_ids=[c]))

    return cell_signals


def quantify_cell_interface_signal_intensity(signal_img, tissue, cell_ids=None, interface_distance=1.):
    """Quantify average image intensity at interfaces between 3D segmented_cells.

    Parameters
    ----------
    signal_img : timagetk.SpatialImage or timagetk.MultiChannelImage
        Intensity image with the signal to quantify.
    tissue : timagetk.TissueImage3D
        Labelled image to use for signal quantification.
    cell_ids : int or list of int, optional
        If ``None`` (default), returns values for all cell ids found in `tissue`.
        Else, should be a cell id of a list of cell ids.
        Note that given cell ids are checked for existence within the array.
    interface_distance : float, optional
        Distance up to which voxels are considered to belong to the interface.

    Returns
    -------
    dict
        Cell interface signal intensity dictionary, ``{(c1, c2): signal((c1, c2))}`
        or ``{channel: {(c1, c2): signal((c1, c2))} }`` if multichannel.

    """
    if cell_ids is None:
        cell_ids = tissue.labels()
    else:
        if any(c not in tissue.labels() for c in cell_ids):
            log.warning("Removing cells that are not in the image!")
        cell_ids = [c for c in cell_ids if c in tissue.labels()]

    dimension = tissue.ndim
    elements = tissue.topological_elements(element_order=[dimension-1])[dimension-1]

    image_bboxes = tissue.boundingbox(labels=cell_ids)

    # TODO: Use wall bboxes instead of cell bboxes ?
    cell_bboxes = {}
    cell_images = {}
    cell_coords = {}
    for c in tqdm(cell_ids, unit="cell"):
        if c in image_bboxes and image_bboxes[c] is not None:
            cell_bboxes[c] = tuple([slice(np.maximum(image_bboxes[c][i].start - int(np.ceil(interface_distance/tissue.voxelsize[i])), 0),
                                          np.minimum(image_bboxes[c][i].stop + int(np.ceil(interface_distance/tissue.voxelsize[i])), tissue.shape[i]),
                                          None)
                                    for i in range(3)])
        else:
            cell_bboxes[c] = tuple([slice(0, 0, None) for i in range(3)])
        cell_images[c] = tissue[cell_bboxes[c]]==c
        cell_coords [c] = np.transpose(np.where(cell_images[c])) + np.array([cell_bboxes[c][i].start for i in range(3)])

    considered_interfaces = [(c1, c2) for c1, c2 in elements.keys() if c1 in cell_ids and c2 in cell_ids]

    cell_interface_coords = {}
    for c1, c2 in tqdm(considered_interfaces, unit='interface'):
        interface_elements = elements[(c1, c2)] * np.array(tissue.voxelsize)

        interface_coords = np.array(list(cell_coords[c1]) + list(cell_coords[c2]))
        interface_voxels = interface_coords * np.array(tissue.voxelsize)

        voxel_vectors = interface_voxels[:, np.newaxis] - interface_elements[np.newaxis, :]
        voxel_distances = np.linalg.norm(voxel_vectors, axis=2)

        voxel_close = np.any(voxel_distances <= interface_distance, axis=1)
        cell_interface_coords[(c1, c2)] = tuple(np.transpose(interface_coords[voxel_close]))

    if isinstance(signal_img, SpatialImage):
        cell_interface_signals = {c: np.mean(signal_img.get_array()[cell_interface_coords[c]])
                                  for c in cell_interface_coords.keys()}
    elif isinstance(signal_img, MultiChannelImage):
        cell_interface_signals = {channel_name:  {c: np.mean(signal_img.get_channel(channel_name).get_array()[cell_interface_coords[c]])
                                                  for c in cell_interface_coords.keys()}
                                  for channel_name in signal_img.channel_names}

    return cell_interface_signals


def quantify_nuclei_signal_intensity(signal_img, nuclei_points, nuclei_sigma=0.5):
    """Quantify image intensity at the level of 3D nuclei points.

    Parameters
    ----------
    signal_img : timagetk.SpatialImage
        Intensity image with the nuclei signal to quantify.
    nuclei_points : numpy.ndarray
        Nuclei 3D positions, in physical XYZ coordinates.
    nuclei_sigma : float, optional
        Standard deviation of the Gaussian kernel used to average local signal.

    Returns
    -------
    numpy.ndarray
        Nuclei quantified signal values.

    """
    voxelsize = np.array(signal_img.voxelsize)
    filtered_signal_img = nd.gaussian_filter(signal_img.get_array().astype(float),
                                             sigma=nuclei_sigma / voxelsize,
                                             order=0)

    # go back to ZYX image convention
    coords = np.array(nuclei_points[:, [2, 1, 0]] / voxelsize, int)
    points_signal = filtered_signal_img[(coords[:, 0], coords[:, 1], coords[:, 2])]

    return points_signal
