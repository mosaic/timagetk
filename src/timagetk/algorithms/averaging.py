#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Image Averaging Algorithms

This module provides functionality to apply various image averaging techniques to a list of spatial images.

Features

- **Support for multiple averaging methods:**
    - **mean**, compute the mean value for each voxel;
    - **robust-mean**, compute the trimmed mean keeping only a fraction of values for each voxel;
    - **median**, compute the median value for each voxel;
    - **minimum**, keep the minimum value for each voxel;
    - **maximum**, keep the maximum value for each voxel;
    - **quantile**, retain only the given ``quantile_value`` for each voxel;
    - **sum**, compute the sum for each voxel;
    - **var**, compute the variance for each voxel;
    - **stddev**, compute the standard deviation for each voxel.
- **Compatibility with masks**: Apply optional masks to the input images for selective averaging.
- **Customizable parameters**: Adjustable parameters like quantile values, robust mean fraction, and window size.
- **Error handling**: Ensures input images are validated and returns meaningful exceptions when issues occur.

Usage Examples

>>> from timagetk.algorithms.averaging import images_averaging
>>> from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image
>>> # Generate example spatial images
>>> image = example_layered_sphere_wall_image()
>>> list_images = [image, image, image]
>>> # Apply Mean Averaging
>>> mean_image = images_averaging(list_images, method='mean')
>>> # Apply Robust Mean Averaging with 80% data retention
>>> rob_mean_image = images_averaging(list_images, method='robust-mean', lts_fraction=0.8)
>>> # Apply Quantile Averaging at 80th percentile
>>> q_mean_image = images_averaging(list_images, method='quantile', quantile_value=0.8)


Notes

- All input images should share the same physical properties, such as shape and voxel size.
- The quantile method allows flexible value extraction based on percentile values:

  - `0` corresponds to the minimum value (same as "min" method).
  - `0.5` corresponds to the median value (same as "median" method).
  - `1` corresponds to the maximum value (same as "max" method).

"""

from vt import mean_images as vt_mean_images

from timagetk.bin.logger import get_logger
from timagetk.components.spatial_image import SpatialImage
from timagetk.io.util import assert_all_files
from timagetk.third_party.vt_image import check_vt_image_not_null
from timagetk.third_party.vt_parser import averaging_kwargs
from timagetk.third_party.vt_parser import general_kwargs
from timagetk.third_party.vt_parser import parallel_kwargs

log = get_logger(__name__)


def images_averaging(images, method=None, masks=None, **kwargs):
    """Images averaging algorithm.

    Parameters
    ----------
    images : list[timagetk.SpatialImage]
        List of ``SpatialImage`` to average.
    method : {"mean", "robust-mean", "median", "minimum", "maximum", "quantile", "sum", "var", "stddev"}, optional
        Image averaging method to use, "mean" by default.
    masks : list[timagetk.SpatialImage], optional
        If any, a list of masks to apply to ``images`` before averaging.

    Other Parameters
    ----------------
    window : list
        Window size for processing, default is [1, 1(, 1)].
    quantile_value : float
        Quantile of the retained value, should be in the ``[0, 1]`` range.
        See notes for detailled explanations.
    lts_fraction : float in [0, 1]
        Fraction of points to be kept for the computation of the robust mean (trimmed estimation).
        Should be in the ``[0, 1]`` range.
    params : str, optional
        CLI parameter string used by ``vt.mean_images`` method.

    Returns
    -------
    timagetk.SpatialImage
        Average image and its metadata.

    Raises
    ------
    ValueError
        If the image returned by ``vt.mean_images`` is ``None``.

    See Also
    --------
    timagetk.third_party.vt_parser.AVERAGING_METHODS
    timagetk.third_party.vt_parser.DEF_AVG_METHOD
    timagetk.third_party.vt_parser.averaging_kwargs
    timagetk.third_party.vt_parser.general_kwargs
    timagetk.third_party.vt_parser.parallel_kwargs

    Notes
    -----
    All images should have the same physical properties: ``shape`` & ``voxelsize``.

    Explanations for ``quantile_value`` parameters:

    - ``0``:   minimum value, equivalent to "min" method
    - ``0.5``: median value,  equivalent to "median" method
    - ``1``:   maximum value, equivalent to "max" method

    References
    ----------
    .. [Truncated_mean] https://en.wikipedia.org/wiki/Truncated_mean
    .. [Quantile] https://en.wikipedia.org/wiki/Quantile

    Examples
    --------
    >>> from timagetk.algorithms.averaging import images_averaging
    >>> from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image
    >>> image = example_layered_sphere_wall_image()
    >>> list_images = [image, image, image]
    >>> mean_image = images_averaging(list_images, method='mean')
    >>> rob_mean_image = images_averaging(list_images, method='robust-mean', lts_fraction=0.8)
    >>> q_mean_image = images_averaging(list_images, method='quantile', quantile_value=0.8)

    """
    from timagetk.components.image import assert_all_spatial_image
    from timagetk.components.image import get_image_attributes

    if isinstance(images[0], SpatialImage):
        # - Check if all input images are indeed ``SpatialImage``:
        assert_all_spatial_image(images)
        attr = get_image_attributes(images[0], extra=['filename'])
        # - And convert to ``Image`` type:
        vt_imgs = [img.to_vt_image() for img in images]
    elif isinstance(images[0], str):
        # case where file names are given
        assert_all_files(images)
        vt_imgs = images
        from timagetk.io import imread
        attr = get_image_attributes(imread(images[0]), extra=['filename'])
    else:
        raise TypeError(f"Unknown input type '{[type(img) for img in images]}' for `images`!")

    if masks is not None:
        if isinstance(masks[0], SpatialImage):
            # - Check if all input masks are indeed ``SpatialImage``:
            assert_all_spatial_image(masks)
            # - And convert to ``Image`` type:
            vt_masks = [mask.to_vt_image() for mask in masks]
        elif isinstance(masks[0], str):
            # case where file names are given
            assert_all_files(masks)
            vt_masks = masks
        else:
            raise TypeError(f"Unknown input types '{[type(mask) for mask in masks]}' for `masks`!")
    else:
        vt_masks = None

    # - Parse ``mean_images`` parameters:
    params, kwargs = averaging_kwargs(method, **kwargs)
    # - Parse general kwargs:
    params += general_kwargs(**kwargs)
    # - Parse parallelism kwargs:
    params += parallel_kwargs(**kwargs)
    log.debug(f"`vt.mean_images` params: '{params}'")

    out_img = vt_mean_images(vt_imgs, vt_masks, params, **kwargs)
    check_vt_image_not_null(out_img, 'averaging')

    if out_img is None:
        null_msg = "VT method 'mean_images' returned NULL image!"
        raise ValueError(null_msg)
    else:
        return SpatialImage(out_img, **attr)
