#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2024 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""This module provides a reimplementation of the SurfCut algorithm.

This method allows to extract the surface layer of a 3D tissue image, notably
to create 2D projections where the cell contours are more visible.

"""

from copy import deepcopy
from typing import Literal
from typing import Optional
from typing import Tuple
from typing import Union

import numpy as np
import scipy.ndimage as nd
from skimage.filters import threshold_otsu

from timagetk import LabelledImage
from timagetk import SpatialImage


def surfcut_erosion(
        img: SpatialImage, orientation: Literal['up', 'down'] = 'up',
        gaussian_sigma: float = 1., top_erosion: int = 8,
        bottom_erosion: int = 15, return_mask: bool = False
) -> Union[SpatialImage, Tuple[SpatialImage, SpatialImage]]:
    """Extract the upper surface of a 3D image using the SurfCut algorithm.

    The method uses two successive erosions on a binary mask obtained from
    a smoothed version of the image to extract only the image layer located
    between the two eroded surfaces.

    Method reimplemented from: Ö. Erguvan, M. Louveaux, O. Hamant, S. Verger:
    ImageJ SurfCut: a user-friendly pipeline for high-throughput extraction
    of cell contours from 3D image stacks (BMC Biology, 2019).

    Parameters
    ----------
    img : timagetk.SpatialImage
        The intensity image on which to extract the surface.
    orientation : str
        Whether to keep upper or lower part of the image.
    gaussian_sigma : float
        Sigma for the Gaussian smoothing (in µm).
    top_erosion : int
        Number of erosion steps to reach surface top.
    bottom_erosion : int
        Number of erosion steps to reach surface bottom.
    return_mask : bool
        Whether to return the binary mask along with the image

    Returns
    -------
    timagetk.SpatialImage
        A 3D image where only the surface intensity remains.
    Optional[timagetk.SpatialImage]
        A binary mask where only the surface voxels remain.

    References
    ----------
    https://bmcbiol.biomedcentral.com/articles/10.1186/s12915-019-0657-1

    Examples
    --------
    >>> import matplotlib.pyplot as plt
    >>> from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image
    >>> from timagetk.algorithms.surfcut import surfcut_erosion
    >>> img = example_layered_sphere_wall_image()
    >>> surfcut_img = surfcut_erosion(img, top_erosion=6, bottom_erosion=9)
    >>> # Visualize the results:
    >>> figure = plt.figure(figsize=(20, 10))
    >>> figure.add_subplot(1,2,1)
    >>> # Show the original image in max intensity projection:
    >>> figure.gca().imshow(img.get_array().max(axis=0), cmap='gray', vmin=0, vmax=255, title='Maximum Intensity Projection')
    >>> figure.add_subplot(1,2,2)
    >>> # Show the surfcut projection:
    >>> figure.gca().imshow(surfcut_img.get_array().max(axis=0), cmap='gray', vmin=0, vmax=255, title='SurfCut Projection')
    >>> figure.show()

    """
    # Get the raw array data from the image
    padded_img = img.get_array()
    # Calculate padding height based on Gaussian sigma and voxel size
    pad_height = int(np.ceil(gaussian_sigma / img.voxelsize[0]))

    # Add padding zeros depending on orientation (up/down)
    if orientation == "up":
        padded_img = np.concatenate([padded_img, np.zeros_like(padded_img)[:pad_height]], axis=0)
    elif orientation == "down":
        padded_img = np.concatenate([np.zeros_like(padded_img)[:pad_height], padded_img], axis=0)

    # Apply Gaussian smoothing with sigma scaled by voxel size
    smooth_img = nd.gaussian_filter(padded_img, sigma=gaussian_sigma / np.array(img.voxelsize))

    # Calculate Otsu threshold and create binary image
    threshold = threshold_otsu(smooth_img.ravel())
    binary_img = smooth_img > threshold

    # Fill holes in the 3D binary image
    binary_img = nd.binary_fill_holes(binary_img)
    # Create coordinate grid for z-projection
    zzz, _, _ = np.mgrid[0:binary_img.shape[0], 0:binary_img.shape[1], 0:binary_img.shape[2]].astype(float)
    # Create z-projection mask
    proj = zzz * (binary_img != 0)
    proj[proj == 0.] = np.nan

    # Process projection based on orientation
    if orientation == "up":
        # For upward orientation, take maximum z values
        with np.errstate(invalid='ignore'):  # Suppress runtime warnings for All-NaN slices
            proj = np.nanmax(proj, axis=0)
        proj[np.isnan(proj)] = 0
        binary_img = zzz <= proj[np.newaxis]
    elif orientation == "down":
        # For downward orientation, take minimum z values
        with np.errstate(invalid='ignore'):  # Suppress runtime warnings for All-NaN slices
            proj = np.nanmin(proj, axis=0)
        proj[np.isnan(proj)] = binary_img.shape[0] - 1
        binary_img = zzz >= proj[np.newaxis]

    # Apply erosion operations to create top and bottom surfaces
    top_img = nd.binary_erosion(binary_img, iterations=top_erosion, border_value=1)
    bottom_img = nd.binary_erosion(binary_img, iterations=bottom_erosion, border_value=1)

    # Create final mask by XOR of top and bottom surfaces
    mask_img = np.logical_xor(top_img, bottom_img)

    # Remove padding based on orientation
    if orientation == "up":
        mask_img = mask_img[:-pad_height]
    else:
        mask_img = mask_img[pad_height:]

    # Apply mask to original image
    surfcut_img = deepcopy(img)
    surfcut_img[mask_img == 0] = 0

    # Return results based on return_mask parameter
    if return_mask:
        return surfcut_img, mask_img
    else:
        return surfcut_img


def surfcut_projection_2d(
        img: SpatialImage, seg_img: Optional[LabelledImage] = None,
        orientation: Literal['up', 'down'] = 'up', gaussian_sigma: float = 1.,
        top_erosion: int = 8, bottom_erosion: int = 15
) -> Union[SpatialImage, Tuple[SpatialImage, LabelledImage]]:
    """Make a 2D projection of an image using the SurfCut mask.

    The function extracts the binary mask with the SurfCut method, then applies
    to compute a max-intensity projection of the upper surface of the image. If
    a corresponding segmented image is passed as input, it also uses the median
    z-slice of the mask to compute a 2D projection of the segmented image.

    Parameters
    ----------
    img : timagetk.SpatialImage
        The intensity image on which to compute the 2D projection.
    seg_img : Optional[timagetk.LabelledImage]
        The segmented image on which to apply the 2D projection.
    orientation : str
        Whether to keep upper or lower part of the image.
    gaussian_sigma : float
        Sigma for the Gaussian smoothing (in µm).
    top_erosion : int
        Number of erosion steps to reach surface top.
    bottom_erosion : int
        Number of erosion steps to reach surface bottom.

    See Also
    --------
    timagetk.algorithms.surfcut.surfcut_erosion

    References
    ----------
    https://bmcbiol.biomedcentral.com/articles/10.1186/s12915-019-0657-1

    Returns
    -------
    timagetk.SpatialImage
        The 2D projection of the intensity image.
    Optional[timagetk.LabelledImage]
        The 2D projection of the segmented image.

    Examples
    --------
    >>> import matplotlib.pyplot as plt
    >>> from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image
    >>> from timagetk.algorithms.surfcut import surfcut_projection_2d
    >>> img = example_layered_sphere_wall_image()
    >>> surfcut_img_2d = surfcut_projection_2d(img, top_erosion=6, bottom_erosion=9)
    >>> # Visualize the results:
    >>> figure = plt.figure(figsize=(20, 10))
    >>> figure.add_subplot(1,2,1)
    >>> # Show the original image in max intensity projection:
    >>> figure.gca().imshow(img.get_array().max(axis=0), cmap='gray', vmin=0, vmax=255, title='Maximum Intensity Projection')
    >>> figure.add_subplot(1,2,2)
    >>> # Show the surfcut projection:
    >>> figure.gca().imshow(surfcut_img_2d, cmap='gray', vmin=0, vmax=255, title='SurfCut Projection')
    >>> figure.show()
    """
    # Apply SurfCut erosion to get processed image and binary mask
    surfcut_img, surfcut_mask = surfcut_erosion(
        img, return_mask=True, orientation=orientation,
        gaussian_sigma=gaussian_sigma,
        top_erosion=top_erosion, bottom_erosion=bottom_erosion
    )

    # Create 2D projection by taking maximum intensity along z-axis
    surfcut_img_2d = SpatialImage(surfcut_img.get_array().max(axis=0), voxelsize=img.voxelsize[1:])

    if seg_img is not None:
        # Create coordinate ranges for x, y, z dimensions
        x_range = range(seg_img.shape[2])
        y_range = range(seg_img.shape[1])
        z_range = range(seg_img.shape[0])

        # Create 3D array of z-coordinates
        zzz = np.tile(z_range, (seg_img.shape[1], seg_img.shape[2], 1)).transpose((2, 0, 1))

        # Convert binary mask to float with NaN for zero values
        nan_mask = surfcut_mask.astype(float)
        nan_mask[surfcut_mask == 0] = np.nan

        # Calculate mean z-position for each x,y position
        zz = np.nanmean(nan_mask * zzz, axis=0)
        zz[np.isnan(zz)] = zzz.max()  # Replace NaN with maximum z value

        # Create meshgrid for y,x coordinates
        yy, xx = np.meshgrid(y_range, x_range, indexing='ij')

        # Round z-coordinates and ensure they're within valid range
        zz = np.round(zz).astype(int)
        zz = np.maximum(0, np.minimum(zz, seg_img.shape[0] - 1))

        # Create flattened coordinate arrays for indexing
        coords = tuple([zz.ravel(), yy.ravel(), xx.ravel()])

        # Extract values from segmented image at calculated coordinates
        surfcut_seg_img_2d = seg_img.get_array()[coords].reshape(xx.shape)
        surfcut_seg_img_2d = LabelledImage(surfcut_seg_img_2d, voxelsize=seg_img.voxelsize[1:])

        return surfcut_img_2d, surfcut_seg_img_2d
    else:
        return surfcut_img_2d
