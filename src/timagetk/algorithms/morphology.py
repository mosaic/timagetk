#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Mathematical Morphology Module

This module provides a suite of mathematical morphology operations for image processing,
designed for both grayscale intensity images and segmented (labelled) images.
It is particularly useful for shape-based image transformations, analyzing structures, and noise reduction in images.

Key Features

1. **Basic Morphological Operations**:

   - Dilation, Erosion, Opening (erosion then dilation), Closing (dilation then erosion).

2. **Advanced Morphological Transformations**:

   - Hat-closing: Enhances small dark structures.
   - Hat-opening: Enhances small bright structures.
   - Gradient: Computes the morphological gradient.

3. **Alternate Sequential Filters**:

   - `oc_alternate_sequential_filter`: Alternates opening and closing with increasing size.
   - `co_alternate_sequential_filter`: Alternates closing and opening with increasing size.
   - `coc_alternate_sequential_filter`: Chains closing-opening-closing alternations.
   - `oco_alternate_sequential_filter`: Chains opening-closing-opening alternations.

4. **Label Filtering**:

   - Includes morphological operations that work directly on labelled, segmented images.

This module simplifies and automates the process of applying morphological transformations to both 2D and 3D images.
"""

from vt import morpho as vt_morpho
from vt.cellfilter import cellfilter as vt_cell_filter

from timagetk.bin.logger import get_logger
from timagetk.components.image import assert_spatial_image
from timagetk.components.image import get_image_attributes
from timagetk.components.labelled_image import LabelledImage
from timagetk.components.labelled_image import assert_labelled_image
from timagetk.components.spatial_image import SpatialImage
from timagetk.tasks.decorators import multichannel_wrapper
from timagetk.third_party.vt_converter import _new_from_vtimage
from timagetk.third_party.vt_parser import DEF_MORPHO_METHOD
from timagetk.third_party.vt_parser import INTENSITY_MORPHOLOGY_METHODS
from timagetk.third_party.vt_parser import LABELLED_MORPHOLOGY_METHODS
from timagetk.third_party.vt_parser import general_kwargs
from timagetk.third_party.vt_parser import morphology_kwargs
from timagetk.third_party.vt_parser import parallel_kwargs

#: Default radius of the structuring elements
DEF_MAX_RADIUS = 3

log = get_logger(__name__)


@multichannel_wrapper
def morphology(image, method=DEF_MORPHO_METHOD, **kwargs):
    """Mathematical morphology algorithms for intensity images.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        Apply the morphological operator to this intensity image.
    method : str in ``INTENSITY_MORPHOLOGY_METHODS``, optional
        The mathematical morphology operator to use, `DEF_RE_METHOD` by default.

    Other Parameters
    ----------------
    channel : str
        If a ``MultiChannelImage`` is used as input image, the algorithm will be applied only to this channel.
        Else it will be applied to all channels of the multichannel image.
    radius : int, optional
        Radius of the structuring element. Defaults to ``1``.
    iterations : int, optional
        Number of time to apply the morphological operation. Defaults to ``1``.
    max_radius : int, optional
        Maximum value reached by `radius`, starting from ``1``, when performing 'sequential filtering' methods.
        Defaults to ``DEF_MAX_RADIUS``.
    connectivity : int, optional
        Use it to override the default 'sphere' parameter for the structuring element.
        Using ``sphere=True`` is equivalent to using ``connectivity=18``.
    sphere : bool
        If ``True``,  the structuring element is the true Euclidean sphere.
    params : str, optional
        CLI parameter string used by ``vt.morpho`` method.

    Returns
    -------
    timagetk.SpatialImage or timagetk.MultiChannelImage
        Intensity image after morphological operation.

    Raises
    ------
    TypeError
        If `image` is not a ``timagetk.SpatialImage``.
    ValueError
        If the image returned by ``vt.morpho`` is ``None``.

    See Also
    --------
    timagetk.algorithm.morphology.DEF_MAX_RADIUS
    timagetk.third_party.vt_parser.INTENSITY_MORPHOLOGY_METHODS
    timagetk.third_party.vt_parser.DEF_MORPHO_METHOD
    timagetk.third_party.vt_parser.morphology_kwargs
    timagetk.third_party.vt_parser.structuring_element_kwargs
    timagetk.third_party.vt_parser.general_kwargs
    timagetk.third_party.vt_parser.parallel_kwargs

    Notes
    -----
    Using the `connectivity` parameter override the `sphere` parameter.

    Connectivity is among the 4-, 6-, 8-, 18-, 26-neighborhoods.
    4 and 8 are for 2D images, the others for 3D images.

    Example
    -------
    >>> from timagetk.algorithms.morphology import morphology, DEF_MORPHO_METHOD
    >>> from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> img = example_layered_sphere_wall_image()

    >>> # EXAMPLE #1 - Apply morphology with default parameters:
    >>> out_img = morphology(img)
    >>> # - Display morphological filtering effect:
    >>> mid_z = img.get_shape('z') // 2
    >>> img_titles = ["Original", f"{DEF_MORPHO_METHOD}"]
    >>> fig = grayscale_imshow([img, out_img], slice_id=mid_z, suptitle="Effect of morphological filtering", title=img_titles, val_range=[0, 255])

    >>> # EXAMPLE #2 - Apply morphology with opening-closing alternate sequential filter:
    >>> out_img = morphology(img, 'oc_alternate_sequential_filter')
    >>> # - Display morphological filtering effect:
    >>> mid_z = img.get_shape('z') // 2
    >>> img_titles = ["Original", 'Opening-closing alternate sequential filter']
    >>> fig = grayscale_imshow([img, out_img], slice_id=mid_z, suptitle="Effect of morphological filtering", title=img_titles, val_range=[0, 255])

    """
    # - Assert 'image' is a SpatialImage instance:
    assert_spatial_image(image, obj_name='intensity image')
    attr = get_image_attributes(image, extra=['filename'])

    # - Take care of extra parameters given as a string:
    extra_params = kwargs.pop("params", "")
    if not extra_params.startswith(" "):
        extra_params = " " + extra_params

    if 'ndim' not in kwargs.keys():
        kwargs['ndim'] = image.ndim
    # - Parse regionalext parameters:
    params, kwargs = morphology_kwargs(method, methods_list=INTENSITY_MORPHOLOGY_METHODS, **kwargs)
    # - Parse general kwargs:
    params += general_kwargs(**kwargs)
    # - Parse parallelism kwargs:
    params += parallel_kwargs(**kwargs)
    # - Combine parsed parameters with extra parameters:
    params += extra_params
    log.debug(f"`vt.morpho` params: '{params}'")

    if method == 'oc_alternate_sequential_filter':
        return morphology_oc_alternate_sequential_filter(image, **kwargs)
    elif method == 'co_alternate_sequential_filter':
        return morphology_co_alternate_sequential_filter(image, **kwargs)
    elif method == 'coc_alternate_sequential_filter':
        return morphology_coc_alternate_sequential_filter(image, **kwargs)
    elif method == 'oco_alternate_sequential_filter':
        return morphology_oco_alternate_sequential_filter(image, **kwargs)
    else:
        vt_img = image.to_vt_image()
        out_img = vt_morpho(vt_img, params, **kwargs)

    if out_img is None:
        null_msg = "VT method 'morpho' returned NULL image!"
        raise ValueError(null_msg)
    else:
        return SpatialImage(out_img, **attr)


def morphology_oc_alternate_sequential_filter(image, max_radius=DEF_MAX_RADIUS, **kwargs):
    """Opening Closing alternate sequential filter on grayscale image.

    Parameters
    ----------
    image : timagetk.SpatialImage
        Apply the morphological operator to this intensity image.
    max_radius : int, optional
        Maximum value reached by `radius`, starting from ``1``. Defaults to ``DEF_MAX_RADIUS``.

    Returns
    -------
    timagetk.SpatialImage
        Transformed intensity image and its metadata

    See Also
    --------
    timagetk.algorithm.morphology.DEF_MAX_RADIUS

    Example
    -------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.io import imread
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> img = shared_data("flower_confocal", 0)
    >>> from timagetk.algorithms.morphology import morphology_oc_alternate_sequential_filter
    >>> asf = morphology_oc_alternate_sequential_filter(img, 2)
    >>> fig = grayscale_imshow([img, asf], slice_id=img.get_shape('z')//2, axis='z', title=['Original', 'OC asf'])

    """
    max_radius = abs(int(max_radius))
    sizes = range(1, max_radius + 1)
    out_img = image
    for size in sizes:
        if kwargs.get('verbose', False):
            log.info(f"Opening/Closing alternate sequential filter with an element of size {size}")
        out_img = morphology(out_img, "opening", radius=size, params="-noverbose", **kwargs)
        out_img = morphology(out_img, "closing", radius=size, params="-noverbose", **kwargs)

    return out_img


def morphology_co_alternate_sequential_filter(image, max_radius=DEF_MAX_RADIUS, **kwargs):
    """Closing Opening alternate sequential filter on grayscale image.

    Parameters
    ----------
    image : timagetk.SpatialImage
        Apply the morphological operator to this intensity image.
    max_radius : int, optional
        Maximum value reached by `radius`, starting from ``1``. Defaults to ``DEF_MAX_RADIUS``.

    Returns
    -------
    timagetk.SpatialImage
        Transformed intensity image and its metadata.

    See Also
    --------
    timagetk.algorithm.morphology.DEF_MAX_RADIUS

    Example
    -------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.io import imread
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> img = shared_data("flower_confocal", 0)
    >>> from timagetk.algorithms.morphology import morphology_co_alternate_sequential_filter
    >>> asf = morphology_co_alternate_sequential_filter(img, 2)
    >>> fig = grayscale_imshow([img, asf], slice_id=img.get_shape('z')//2, axis='z', title=['Original', 'CO asf'])

    """
    max_radius = abs(int(max_radius))
    sizes = range(1, max_radius + 1)
    out_img = image
    for size in sizes:
        if kwargs.get('verbose', False):
            log.info(f"Closing/Opening alternate sequential filter with an element of size {size}")
        out_img = morphology(out_img, "closing", radius=size, params="-noverbose", **kwargs)
        out_img = morphology(out_img, "opening", radius=size, params="-noverbose", **kwargs)

    return out_img


def morphology_coc_alternate_sequential_filter(image, max_radius=DEF_MAX_RADIUS, **kwargs):
    """Closing Opening Closing alternate sequential filter on grayscale image.

    Parameters
    ----------
    image : timagetk.SpatialImage
        Apply the morphological operator to this intensity image.
    max_radius : int, optional
        Maximum value reached by `radius`, starting from ``1``. Defaults to ``DEF_MAX_RADIUS``.

    Returns
    -------
    timagetk.SpatialImage
        Transformed intensity image and its metadata.

    See Also
    --------
    timagetk.algorithm.morphology.DEF_MAX_RADIUS

    Example
    -------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.io import imread
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> img = shared_data("flower_confocal", 0)
    >>> from timagetk.algorithms.morphology import morphology_coc_alternate_sequential_filter
    >>> asf = morphology_coc_alternate_sequential_filter(img, 2)
    >>> fig = grayscale_imshow([img, asf], slice_id=img.get_shape('z')//2, axis='z', title=['Original', 'COC asf'])

    """
    max_radius = abs(int(max_radius))
    sizes = range(1, max_radius + 1)
    out_img = image
    for size in sizes:
        if kwargs.get('verbose', False):
            log.info(f"Closing/Opening/Closing alternate sequential filter with an element of size {size}")
        out_img = morphology(out_img, "closing", radius=size, params="-noverbose", **kwargs)
        out_img = morphology(out_img, "opening", radius=size, params="-noverbose", **kwargs)
        out_img = morphology(out_img, "closing", radius=size, params="-noverbose", **kwargs)

    return out_img


def morphology_oco_alternate_sequential_filter(image, max_radius=DEF_MAX_RADIUS, **kwargs):
    """Opening Closing Opening alternate sequential filter on grayscale image.

    Parameters
    ----------
    image : timagetk.SpatialImage
        Apply the morphological operator to this intensity image.
    max_radius : int, optional
        Maximum value reached by `radius`, starting from ``1``. Defaults to ``DEF_MAX_RADIUS``.

    Returns
    -------
    timagetk.SpatialImage
        Transformed intensity image and its metadata.

    See Also
    --------
    timagetk.algorithm.morphology.DEF_MAX_RADIUS

    Example
    -------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.io import imread
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> img = shared_data("flower_confocal", 0)
    >>> from timagetk.algorithms.morphology import morphology_oco_alternate_sequential_filter
    >>> asf = morphology_oco_alternate_sequential_filter(img, 2)
    >>> fig = grayscale_imshow([img, asf], slice_id=img.get_shape('z')//2, axis='z', title=['Original', 'OCO asf'])

    """
    max_radius = abs(int(max_radius))
    sizes = range(1, max_radius + 1)
    out_img = image
    for size in sizes:
        if kwargs.get('verbose', False):
            log.info(f"Opening/Closing/Opening alternate sequential filter with an element of size {size}")
        out_img = morphology(out_img, "opening", radius=size, params="-noverbose", **kwargs)
        out_img = morphology(out_img, "closing", radius=size, params="-noverbose", **kwargs)
        out_img = morphology(out_img, "opening", radius=size, params="-noverbose", **kwargs)

    return out_img


def label_filtering(image, method=DEF_MORPHO_METHOD, **kwargs):
    """Mathematical morphology algorithms for labelled images.

    Valid ``method`` values are:

      - **dilation**
      - **erosion**
      - **closing**: dilation then erosion
      - **opening**: erosion then dilation
      - **hat-closing**: hat transform ('closing' - `image`), enhances small dark structures
      - **hat-opening**: inverse hat transform (`image` - 'opening'), enhances small bright structures
      - **gradient**: morphological gradient ('dilated' - 'eroded')
      - **oc_alternate_sequential_filter** alternates opening and closing operators with increasing element `radius`.
      - **co_alternate_sequential_filter** alternates closing and opening operators with increasing element `radius`.
      - **coc_alternate_sequential_filter** alternates closing, opening and closing operators with increasing element `radius`.
      - **oco_alternate_sequential_filter** alternates opening, closing and opening operators with increasing element `radius`.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Apply the morphological operator to this segmented image.
    method : str in ``INTENSITY_MORPHOLOGY_METHODS``, optional
        The mathematical morphology operator to use, `DEF_RE_METHOD` by default.

    Other Parameters
    ----------------
    radius : int, optional
        Radius of the structuring element. Defaults to ``1``.
    iterations : int, optional
        Number of time to apply the morphological operation. Defaults to ``1``.
    max_radius : int, optional
        Maximum value reached by `radius`, starting from ``1``, when performing 'sequential filtering' methods.
        Defaults to ``3``.
    connectivity : int, optional
        Use it to override the default 'sphere' parameter for the structuring element.
        Using ``sphere=True`` is equivalent to using ``connectivity=18``.
    sphere : bool
        If ``True``,  the structuring element is the true euclidean sphere.
    params : str, optional
        CLI parameter string used by ``vt.cell_filter`` method.

    Returns
    -------
    timagetk.LabelledImage
        Segmented image after morphological operation.

    Raises
    ------
    TypeError
        If ``image`` is not a ``timagetk.LabelledImage``.
    ValueError
        If the image returned by ``vt.cell_filter`` is ``None``.

    See Also
    --------
    timagetk.algorithm.morphology.DEF_MAX_RADIUS
    timagetk.third_party.vt_parser.INTENSITY_MORPHOLOGY_METHODS
    timagetk.third_party.vt_parser.DEF_MORPHO_METHOD
    timagetk.third_party.vt_parser.morphology_kwargs
    timagetk.third_party.vt_parser.structuring_element_kwargs
    timagetk.third_party.vt_parser.general_kwargs
    timagetk.third_party.vt_parser.parallel_kwargs

    Notes
    -----
    Using the `connectivity` parameter override the `sphere` parameter.

    Connectivity is among the 4-, 6-, 8-, 18-, 26-neighborhoods.
    4 and 8 are for 2D images, the others for 3D images.

    Example
    -------
    >>> from timagetk.algorithms.morphology import label_filtering, DEF_MORPHO_METHOD
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> img = shared_data("synthetic", 'labelled')

    >>> # EXAMPLE #1 - Apply label filtering with default parameters:
    >>> out_img = label_filtering(img)
    >>> # - Display label filtering effect:
    >>> mid_z = img.get_shape('z') // 2
    >>> img_titles = ["Original", f"{DEF_MORPHO_METHOD}"]
    >>> fig = grayscale_imshow([img, out_img], slice_id=mid_z, suptitle="Effect of label filtering", title=img_titles, cmap='glasbey')

    >>> # EXAMPLE #2 - Apply label filtering with opening-closing alternate sequential filter:
    >>> out_img = label_filtering(img, 'oc_alternate_sequential_filter')
    >>> # - Display label filtering effect:
    >>> mid_z = img.get_shape('z') // 2
    >>> img_titles = ["Original", 'Opening-closing alternate sequential filter']
    >>> fig = grayscale_imshow([img, out_img], slice_id=mid_z, suptitle="Effect of label filtering", title=img_titles, cmap='glasbey')

    """
    # - Assert 'image' is a SpatialImage instance:
    assert_labelled_image(image, obj_name='image')
    attr = get_image_attributes(image, extra=['filename'])

    # TODO: if not_a_label != 0: should relabel not_a_label to 0 (if possible) since VT will...
    # -- erode and set eroded voxels to 0
    # -- dilate where 0 is set
    not_a_label = attr['not_a_label']
    if not_a_label != 0:
        warn_msg = f"It would be safer to have `not_a_label` equal to 0, got {not_a_label}!"
        log.warning(warn_msg)

    # - Take care of extra parameters given as a string:
    extra_params = kwargs.pop("params", "")
    if not extra_params.startswith(" "):
        extra_params = " " + extra_params

    if 'ndim' not in kwargs.keys():
        kwargs['ndim'] = image.ndim
    # - Parse regionalext parameters:
    params, kwargs = morphology_kwargs(method, methods_list=LABELLED_MORPHOLOGY_METHODS, **kwargs)
    # - Parse general kwargs:
    params += general_kwargs(**kwargs)
    # - Parse parallelism kwargs:
    params += parallel_kwargs(**kwargs)
    # - Combine parsed parameters with extra parameters:
    params += extra_params
    log.debug(f"`vt.cellfilter` params: '{params}'")

    if method == 'oc_alternate_sequential_filter':
        return label_filtering_oc_alternate_sequential_filter(image, **kwargs)
    elif method == 'co_alternate_sequential_filter':
        return label_filtering_co_alternate_sequential_filter(image, **kwargs)
    elif method == 'coc_alternate_sequential_filter':
        return label_filtering_coc_alternate_sequential_filter(image, **kwargs)
    elif method == 'oco_alternate_sequential_filter':
        return label_filtering_oco_alternate_sequential_filter(image, **kwargs)
    else:
        vt_img = image.to_vt_image()
        out_img = vt_cell_filter(vt_img, params, **kwargs)

    if out_img is None:
        null_msg = "VT method 'cell_filter' returned NULL image!"
        raise ValueError(null_msg)
    else:
        arr, vxs, _, _ = _new_from_vtimage(out_img, copy=False)
        return LabelledImage(arr, **attr)


def label_filtering_oc_alternate_sequential_filter(image, max_radius=DEF_MAX_RADIUS, **kwargs):
    """Opening Closing alternate sequential filter on labelled image.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Apply the morphological operator to this labelled image.
    max_radius : int, optional
        Maximum value reached by `radius`, starting from ``1``. Defaults to ``DEF_MAX_RADIUS``.

    Returns
    -------
    timagetk.LabelledImage
        Transformed labelled image and its metadata.

    See Also
    --------
    timagetk.algorithm.morphology.DEF_MAX_RADIUS

    Example
    -------
    >>> from timagetk import LabelledImage
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> img = shared_data('flower_labelled', 1)
    >>> from timagetk.algorithms.morphology import label_filtering_oc_alternate_sequential_filter
    >>> asf = label_filtering_oc_alternate_sequential_filter(img, 2)
    >>> fig = grayscale_imshow([img, asf], slice_id=img.get_shape('z')//2, axis='z', title=['Original', 'OC asf'], cmap='glasbey')

    """
    max_radius = abs(int(max_radius))
    sizes = range(1, max_radius + 1)
    out_img = image
    for size in sizes:
        if kwargs.get('verbose', False):
            log.info(f"Opening/Closing alternate sequential filter with an element of size {size}")
        out_img = label_filtering(out_img, "opening", radius=size, params="-noverbose", **kwargs)
        out_img = label_filtering(out_img, "closing", radius=size, params="-noverbose", **kwargs)

    return out_img


def label_filtering_co_alternate_sequential_filter(image, max_radius=DEF_MAX_RADIUS, **kwargs):
    """Closing Opening alternate sequential filter on labelled image.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Apply the morphological operator to this labelled image.
    max_radius : int, optional
        Maximum value reached by `radius`, starting from ``1``. Defaults to ``DEF_MAX_RADIUS``.

    Returns
    -------
    timagetk.LabelledImage
        Transformed labelled image and its metadata.

    See Also
    --------
    timagetk.algorithm.morphology.DEF_MAX_RADIUS

    Example
    -------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> img = shared_data('flower_labelled', 1)
    >>> from timagetk.algorithms.morphology import label_filtering_co_alternate_sequential_filter
    >>> asf = label_filtering_co_alternate_sequential_filter(img, 2)
    >>> fig = grayscale_imshow([img, asf], slice_id=img.get_shape('z')//2, axis='z', title=['Original', 'OC asf'], cmap='glasbey')

    """
    max_radius = abs(int(max_radius))
    sizes = range(1, max_radius + 1)
    out_img = image
    for size in sizes:
        if kwargs.get('verbose', False):
            log.info(f"Closing/Opening alternate sequential filter with an element of size {size}")
        out_img = label_filtering(out_img, "opening", radius=size, params="-noverbose", **kwargs)
        out_img = label_filtering(out_img, "closing", radius=size, params="-noverbose", **kwargs)

    return out_img


def label_filtering_coc_alternate_sequential_filter(image, max_radius=DEF_MAX_RADIUS, **kwargs):
    """Closing Opening Closing alternate sequential filter on labelled image.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Apply the morphological operator to this labelled image.
    max_radius : int, optional
        Maximum value reached by `radius`, starting from ``1``. Defaults to ``DEF_MAX_RADIUS``.

    Returns
    -------
    timagetk.LabelledImage
        Transformed labelled image and its metadata.

    See Also
    --------
    timagetk.algorithm.morphology.DEF_MAX_RADIUS

    Example
    -------
    >>> from timagetk import LabelledImage
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> img = shared_data('flower_labelled', 1)
    >>> from timagetk.algorithms.morphology import label_filtering_coc_alternate_sequential_filter
    >>> asf = label_filtering_coc_alternate_sequential_filter(img, 2)
    >>> fig = grayscale_imshow([img, asf], slice_id=img.get_shape('z')//2, axis='z', title=['Original', 'COC asf'], cmap='glasbey')

    """
    max_radius = abs(int(max_radius))
    sizes = range(1, max_radius + 1)
    out_img = image
    for size in sizes:
        if kwargs.get('verbose', False):
            log.info(f"Closing/Opening/Closing alternate sequential filter with an element of size {size}")
        out_img = label_filtering(out_img, "closing", radius=size, params="-noverbose", **kwargs)
        out_img = label_filtering(out_img, "opening", radius=size, params="-noverbose", **kwargs)
        out_img = label_filtering(out_img, "closing", radius=size, params="-noverbose", **kwargs)

    return out_img


def label_filtering_oco_alternate_sequential_filter(image, max_radius=DEF_MAX_RADIUS, **kwargs):
    """Opening Closing Opening alternate sequential filter on labelled image.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Apply the morphological operator to this labelled image.
    max_radius : int, optional
        Maximum value reached by `radius`, starting from ``1``. Defaults to ``DEF_MAX_RADIUS``.

    Returns
    -------
    timagetk.LabelledImage
        Transformed labelled image and its metadata.

    See Also
    --------
    timagetk.algorithm.morphology.DEF_MAX_RADIUS

    Example
    -------
    >>> from timagetk import LabelledImage
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> img = shared_data('flower_labelled', 0)
    >>> from timagetk.algorithms.morphology import label_filtering_oco_alternate_sequential_filter
    >>> asf = label_filtering_oco_alternate_sequential_filter(img, 2)
    >>> fig = grayscale_imshow([img, asf], slice_id=img.get_shape('z')//2, axis='z', title=['Original', 'OCO asf'], cmap='glasbey')

    """
    max_radius = abs(int(max_radius))
    sizes = range(1, max_radius + 1)
    out_img = image
    for size in sizes:
        if kwargs.get('verbose', False):
            log.info(f"Opening/Closing/Opening alternate sequential filter with an element of size {size}")
        out_img = label_filtering(out_img, "opening", radius=size, params="-noverbose", **kwargs)
        out_img = label_filtering(out_img, "closing", radius=size, params="-noverbose", **kwargs)
        out_img = label_filtering(out_img, "opening", radius=size, params="-noverbose", **kwargs)

    return out_img
