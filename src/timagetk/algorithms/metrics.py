#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Image Metrics Module

This module provides utility functions for assessing the similarity and quality of images.
It is designed to compute a variety of numerical metrics that evaluate the relationship between two input images.

Key Features

1. **Structural Similarity Index (SSIM)**:

   - Measures the perceptual similarity between two images by taking texture and structure into account.
   - Range between ``O`` (low similarity) and ``1`` (high similarity).
   - This metric is particularly useful for assessing how much the structural content in an image has changed due to compression, noise, or other transformations.
   - See also: `Structural Similarity Index <https://en.wikipedia.org/wiki/Structural_similarity>`_ on Wikipedia.
   - See also: `Image quality assessment: From error visibility to structural similarity <https://ieeexplore.ieee.org/document/1284395>`_ on IEEE.

2. **Mean Squared Error (MSE)**:

   - Computes the mean squared difference between two images, providing a simple measure of pixel-wise similarity.
   - Range from ``O`` (identical images) to ``infinity`` (no similarity).
   - Note that this metric is not a true measure of similarity, as it does not take texture into account.
   - Also, the scale of the MSE depends on the image resolution and intensity range.
   - However, it is a useful measure in the context of image registration, where the MSE is used as a similarity measure.
   - See also: `Mean Squared Error <https://en.wikipedia.org/wiki/Mean_squared_error>`_ on Wikipedia.
   - See also: `Mean squared error: Love it or leave it? A new look at Signal Fidelity Measures <https://ieeexplore.ieee.org/document/4775883>`_ on IEEE.

3. **Normalized Root Mean Squared Error (NRMSE)**:

   - A normalized comparison metric with different normalization types (e.g., Euclidean, Min-Max, Mean).
   - Range from ``O`` (identical images) to ``1`` (no similarity).
   - NRMSE is a normalization of the MSE, which helps to make the metric independent of the image scale or intensity range.
   - The normalization method (e.g., Euclidean, Min-Max, or Mean) defines how the error is scaled.

4. **Peak Signal-to-Noise Ratio (PSNR)**:

   - PSNR is commonly used to assess image quality after compression, noise reduction, or reconstruction.
   - The PSNR is usually expressed in dB (decibels).
   - It measures the ratio between the maximum possible power of the signal (image) and the noise affecting the quality of its representation.
   - The maximum possible power of the signal is the maximum possible power of the image dtype (e.g., 255 for uint8 images).
   - The noise affecting the quality of its representation is estimated as the noise power that is not captured by the signal.
   - The PSNR is a non-negative value, where higher values indicate better quality.
   - Range from ``O`` to ``infinity``, though values typically fall between **20 dB and 50 dB** for practical cases.
   - A value above 30 dB usually indicates good image quality, while below 20 dB indicates a large amount of noise or distortion.
   - See also: `Peak Signal-to-Noise Ratio <https://en.wikipedia.org/wiki/Peak_signal-to-noise_ratio>`_ on Wikipedia.

Summary of Key Interpretations

| **Metric** | **Good Value** | **Indicates**                                            |
| ---------- | -------------- | -------------------------------------------------------- |
| **SSIM**   | Close to 1     | High perceptual similarity, considering image structure. |
| **MSE**    | Close to 0     | Low pixel-wise differences.                              |
| **NRMSE**  | Close to 0     | Normalized pixel-level similarity.                       |
| **PSNR**   | 30 dB          | High-quality image (low distortion or noise).            |

These metrics help evaluate images in different contexts, such as comparing original and compressed images,
assessing denoising algorithms, or detecting changes in an image dataset.
Use them depending on whether you need pixel-wise comparison (MSE/NRMSE) or perceptual similarity (SSIM/PSNR).

Usage Examples

>>> from timagetk.algorithms.metrics import structural_similarity_index
>>> from timagetk.algorithms.metrics import mean_squared_difference
>>> from timagetk.algorithms.metrics import normalized_root_mean_squared_difference
>>> from timagetk.algorithms.metrics import peak_signal_to_noise_ratio
>>> from timagetk.io.dataset import shared_data
>>> from timagetk.algorithms.blockmatching import blockmatching
>>> from timagetk.algorithms.trsf import apply_trsf
>>> # Loading an example grayscale image:
>>> img1 = shared_data('flower_multiangle', 0)  # p58-t0 multiangle top view (reference image in fusion)
>>> img2 = shared_data('flower_confocal', 0)  # result of p58-t0 multiangle images fusion
>>> trsf_12 = blockmatching(img1, img2, method='affine')  # affine blockmatching
>>> img1_res = apply_trsf(img1, trsf_12, template_img=img2, interpolation='linear')  # apply the transformation to the image
>>> # Compute the SSIM metric:
>>> ssim_metric = structural_similarity_index(img1_res, img2)
>>> print(f"SSIM metric: {ssim_metric}")
SSIM metric: 0.7203930704650399
>>> # Mean Squared Error (MSE)
>>> mse = mean_squared_difference(img1_res, img2)
>>> print(f"MSE: {mse}")
MSE: 415.0316584948015
>>> # Normalized Root Mean Squared Error (NRMSE)
>>> nrmse = normalized_root_mean_squared_difference(img1_res, img2, norm_type='mean')
>>> print(f"NRMSE: {nrmse}")
NRMSE: 0.4306962130284195
>>> # Peak Signal-to-Noise Ratio (PSNR)
>>> psnr = peak_signal_to_noise_ratio(img1_res, img2)
>>> print(f"PSNR: {psnr}")
PSNR: 21.949991350348654

This module leverages robust methods from the `skimage.metrics` library to compute these metrics,
ensuring accurate and reliable results for applications requiring image quality assessment.
"""

import numpy as np
from skimage.metrics import mean_squared_error
from skimage.metrics import normalized_root_mse
from skimage.metrics import peak_signal_noise_ratio
from skimage.metrics import structural_similarity


def structural_similarity_index(img1, img2, full_ssim=False):
    """Compute the mean structural similarity index between two images.

    When comparing images, the mean squared error (MSE)–while simple to implement–is not highly indicative of
    perceived similarity.
    Structural similarity aims to address this shortcoming by taking texture into account [Wang_Bovik2004]_, [Wang_Bovik2009]_.

    Parameters
    ----------
    img1, img2 : Image
        Images to compare.
    full_ssim : bool, optional
        If ``True``, return the full structural similarity image instead of the mean value. Defaults to ``False``.

    Returns
    -------
    float
        The mean structural similarity, ranging between ``O`` (low similarity) to ``1`` (high similarity).
    timagetk.SpatialImage
        The full SSIM image.

    See Also
    --------
    skimage.metrics.structural_similarity

    References
    ----------
    .. [Wang_Bovik2004] Z. Wang, A. C. Bovik, H. R. Sheikh & E. P. Simoncelli, **Image quality assessment: From error visibility to structural similarity**, *IEEE Transactions on Image Processing*, vol. 13, no. 4, pp. 600-612, Apr. 2004.
    .. [Wang_Bovik2009] Z. Wang, A.C. Bovik, **Mean squared error: Love it or leave it? A new look at Signal Fidelity Measures**, *Signal Processing Magazine, IEEE*, vol. 26, no. 1, pp. 98-117, Jan. 2009.

    Examples
    --------
    >>> from timagetk.algorithms.metrics import structural_similarity_index
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.blockmatching import blockmatching
    >>> from timagetk.algorithms.trsf import apply_trsf
    >>> # Loading an example grayscale image:
    >>> img1 = shared_data('flower_multiangle', 0)  # p58-t0 multiangle top view (reference image in fusion)
    >>> img2 = shared_data('flower_confocal', 0)  # result of p58-t0 multiangle images fusion
    >>> trsf_12 = blockmatching(img1, img2, method='affine')  # affine blockmatching
    >>> img1_res = apply_trsf(img1, trsf_12, template_img=img2, interpolation='linear')  # apply the transformation to the image
    >>> # Compute the SSIM metric:
    >>> ssim_metric = structural_similarity_index(img1_res, img2)
    >>> print(f"SSIM metric: {ssim_metric}")
    SSIM metric: 0.7203930704650399

    """
    # Compare shape first, no need to go further if not equal!
    try:
        np.testing.assert_array_equal(img1.shape, img2.shape)
    except AssertionError:
        msg = "Given 'sp_img' has a different shape ({}) than this one ({})!"
        raise ValueError(msg.format(img2.shape, img1.shape))

    mssim = structural_similarity(img1.get_array(), img2.get_array(), full=full_ssim)
    if full_ssim:
        return mssim[-1]
    else:
        return mssim


def mean_squared_difference(img1, img2):
    """Compute the mean squared difference between this image and another.

    Parameters
    ----------
    img1, img2 : Image
        Images to compare.

    Returns
    -------
    float
        The mean squared error.

    See Also
    --------
    skimage.metrics.mean_squared_error

    Examples
    --------
    >>> from timagetk.algorithms.metrics import structural_similarity_index
    >>> from timagetk.algorithms.metrics import mean_squared_difference
    >>> from timagetk.algorithms.metrics import normalized_root_mean_squared_difference
    >>> from timagetk.algorithms.metrics import peak_signal_to_noise_ratio
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.blockmatching import blockmatching
    >>> from timagetk.algorithms.trsf import apply_trsf
    >>> # Loading an example grayscale image:
    >>> img1 = shared_data('flower_multiangle', 0)  # p58-t0 multiangle top view (reference image in fusion)
    >>> img2 = shared_data('flower_confocal', 0)  # result of p58-t0 multiangle images fusion
    >>> trsf_12 = blockmatching(img1, img2, method='affine')  # affine blockmatching
    >>> img1_res = apply_trsf(img1, trsf_12, template_img=img2, interpolation='linear')  # apply the transformation to the image
    >>> # Mean Squared Error (MSE)
    >>> mse = mean_squared_difference(img1_res, img2)
    >>> print(f"MSE: {mse}")
    MSE: 415.0316584948015
    """
    return mean_squared_error(img1.get_array(), img2.get_array())


def normalized_root_mean_squared_difference(img1, img2, norm_type='Euclidean'):
    """Compute the mean squared difference between this image and another.

    Parameters
    ----------
    img1, img2 : Image
        Images to compare.
    norm_type : {'euclidean', 'min-max', 'mean'}
        Controls the normalization method to use in the denominator of the NRMSE.

    Returns
    -------
    float
        The NRMSE metric.

    Notes
    -----
    There is no standard method of normalization across the literature [NRMSE]_.

    The methods available here are as follows:

      * **Euclidean**: normalize by the averaged Euclidean norm of ``im_true``
      * **min-max**: normalize by the intensity range of ``im_true``.
      * **mean**: normalize by the mean of ``im_true``.

    The averaged Euclidean norm is ``NRMSE = RMSE * sqrt(N) / ||im_true||``,
    where ``|| . ||`` denotes the Frobenius norm and ``N = im_true.size``.

    This result is equivalent to: ``NRMSE = ||im_true - im_test|| / ||im_true||``.

    See Also
    --------
    skimage.metrics.normalized_root_mse

    References
    ----------
    .. [NRMSE] https://en.wikipedia.org/wiki/Root-mean-square_deviation

    Examples
    --------
    >>> from timagetk.algorithms.metrics import structural_similarity_index
    >>> from timagetk.algorithms.metrics import mean_squared_difference
    >>> from timagetk.algorithms.metrics import normalized_root_mean_squared_difference
    >>> from timagetk.algorithms.metrics import peak_signal_to_noise_ratio
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.blockmatching import blockmatching
    >>> from timagetk.algorithms.trsf import apply_trsf
    >>> # Loading an example grayscale image:
    >>> img1 = shared_data('flower_multiangle', 0)  # p58-t0 multiangle top view (reference image in fusion)
    >>> img2 = shared_data('flower_confocal', 0)  # result of p58-t0 multiangle images fusion
    >>> trsf_12 = blockmatching(img1, img2, method='affine')  # affine blockmatching
    >>> img1_res = apply_trsf(img1, trsf_12, template_img=img2, interpolation='linear')  # apply the transformation to the image
    >>> # Normalized Root Mean Squared Error (NRMSE)
    >>> nrmse = normalized_root_mean_squared_difference(img1_res, img2, norm_type='mean')
    >>> print(f"NRMSE: {nrmse}")
    NRMSE: 0.4306962130284195
    """
    return normalized_root_mse(img1.get_array(), img2.get_array(), normalization=norm_type)


def peak_signal_to_noise_ratio(img1, img2):
    """Compute the peak signal-to-noise ratio (PSNR) for an image.

    Parameters
    ----------
    img1, img2 : Image type
        Images to compare.

    Returns
    -------
    float
        The PSNR metric [PSNR]_.

    See Also
    --------
    skimage.metrics.peak_signal_noise_ratio

    References
    ----------
    .. [PSNR] https://en.wikipedia.org/wiki/Peak_signal-to-noise_ratio

    Examples
    --------
    >>> from timagetk.algorithms.metrics import structural_similarity_index
    >>> from timagetk.algorithms.metrics import mean_squared_difference
    >>> from timagetk.algorithms.metrics import normalized_root_mean_squared_difference
    >>> from timagetk.algorithms.metrics import peak_signal_to_noise_ratio
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.blockmatching import blockmatching
    >>> from timagetk.algorithms.trsf import apply_trsf
    >>> # Loading an example grayscale image:
    >>> img1 = shared_data('flower_multiangle', 0)  # p58-t0 multiangle top view (reference image in fusion)
    >>> img2 = shared_data('flower_confocal', 0)  # result of p58-t0 multiangle images fusion
    >>> trsf_12 = blockmatching(img1, img2, method='affine')  # affine blockmatching
    >>> img1_res = apply_trsf(img1, trsf_12, template_img=img2, interpolation='linear')  # apply the transformation to the image
    >>> # Peak Signal-to-Noise Ratio (PSNR)
    >>> psnr = peak_signal_to_noise_ratio(img1_res, img2)
    >>> print(f"PSNR: {psnr}")
    PSNR: 21.949991350348654
    """
    return peak_signal_noise_ratio(img1.get_array(), img2.get_array())
