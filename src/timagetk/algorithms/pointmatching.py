#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Pointmatching and Transformation Application

This module provides tools to estimate transformations based on paired landmarks
(also referred to as point matching) and apply transformations to sets of 3D point coordinates.
It is designed to simplify point matching and transformation processes, making it useful
for applications in image registration and coordinate transformations.

Main Functionality

1. **Point Matching (`pointmatching`)**:

  - Estimates transformations between two sets of paired points (landmarks).
  - Supports multiple transformation methods, including rigid, affine, and vector field.
  - Handles advanced configurations such as regularization, propagation distance, and
    estimation types for precise control.

2. **Transformation Application (`apply_trsf_to_points`)**:

  - Applies a transformation to a set of 3D points.
  - Handles transformations in both voxel and real-world coordinate systems.
  - Includes support for additional settings like parallelism and customization.

Key Features

- Transformations supported: Rigid, Affine, and Vector Field.
- Highly configurable estimation parameters for fine-tuning.
- Designed for large-scale, parallel execution when required.
- Comprehensive validation of input data to minimize user errors.

Usage Examples

```python
from timagetk.algorithms.pointmatching import pointmatching, apply_trsf_to_points

# Example data
flo_points = [[0, 0, 0], [0, 0, 1], [0, 1, 0], [1, 0, 0]]
ref_points = [[0, 0, 0], [0, 0, 2], [0, 2, 0], [2, 0, 0]]

# Estimate affine transformation
trsf = pointmatching(flo_points, ref_points, method="affine")

# Apply transformation to floating points
registered_points = apply_trsf_to_points(flo_points, trsf)
print(registered_points)
```

This module is a critical component for tasks requiring spatial alignment between point sets
and the application of transformations, particularly in image processing and 3D reconstruction contexts.
"""

from vt import apply_trsf_to_points as vt_apply_trsf_to_points
from vt import pointmatching as vt_pointmatching
from vt import vtImage
from vt import vtPointList
from vt.image import Image

from timagetk.bin.logger import get_logger
from timagetk.components.spatial_image import SpatialImage
from timagetk.components.trsf import Trsf
from timagetk.third_party.vt_parser import DEF_BM_METHOD
from timagetk.third_party.vt_parser import general_kwargs
from timagetk.third_party.vt_parser import parallel_kwargs
from timagetk.third_party.vt_parser import pointmatching_kwargs

log = get_logger(__name__)


def pointmatching(points_flo, points_ref, template_img=None, method=DEF_BM_METHOD, **kwargs):
    """Computes a transformation using paired points (*aka.* landmarks).

    The resulting transformation will allow to resample the floating image
    (from which the floating points are drawn) onto the reference one.
    Points are assumed to be paired according to their index in both lists.

    Parameters
    ----------
    points_flo : list
        The X, Y [, Z] point list to be registered (floating points).
    points_ref : list
        The X, Y [, Z] reference point list (still points).
    template_img : timagetk.SpatialImage, optional
        Template image used for the dimensions & voxelsize of the output transformation vectorfield.
        Required for *vectorfield* method.
    method : {'rigid', 'affine', 'vectorfield'}, optional
        The registration method to use, ``DEF_BM_METHOD`` by default.

    Other Parameters
    ----------------
    fluid_sigma : float or list[float]
        Sigma for fluid regularization, *i.e.* field interpolation and regularization for pairings.
        Retricted to vector-field.
    vector_propagation_distance : float
        Defines the propagation distance of initial pairings (*i.e.* displacements).
        This implies the same displacement for the spanned sphere.
        Retricted to vector-field. Distance is in world units (not voxels).
    vector_fading_distance : float
        Area of fading for initial pairings (*i.e.* displacements).
        This allows progressive transition towards null displacements and thus avoid discontinuites.
        Distance is in world units (not voxels).
    vector_propagation_type : str in {"direct", "skiz"}
        Defines how vector are propagated, see notes for more details.
    estimator_type : {"wlts", "lts", "wls", "ls"}
        Transformation estimator, see notes for more details.
    lts_fraction : float
        If defined, set the fraction of pairs that are kept, used with trimmed estimations.
    lts_deviation : float
        If defined, set the threshold to discard pairings (see notes), used with trimmed estimations.
    lts_iterations : int
        If defined, set the maximal number of iterations, used with trimmed estimations.
    real : bool, optional
        Define if the given points are in voxel or real units (default)
    params : str, optional
        CLI parameter string used by ``vt.pointmatching`` method.

    Returns
    -------
    Trsf
        The ``vt`` transformation object resampling the floating points onto the reference points.

    Raises
    ------
    TypeError
        If `template_img` is not a ``timagetk.LabelledImage``, if not ``None``.
    ValueError
        If the list of points are not of equal size.
        If there is less than four points in the paired lists when using a linear `method`.
        If the transformation returned by ``vt.pointmatching`` is ``None``.

    Notes
    -----
    Definitions of available `estimator_type` values:

    - **wlts**: weighted least trimmed squares (default);
    - **lts**: least trimmed squares, see [lts]_ for more details;
    - **wls**: weighted least squares, see [wls]_ for more details;
    - **ls**: least squares, see [ls]_ for more details.

    Parameter `lts_deviation` is used in the formulae: ``threshold = average + lts_deviation * standard_deviation``

    See Also
    --------
    timagetk.third_party.vt_parser.BLOCKMATCHING_METHODS
    timagetk.third_party.vt_parser.DEF_BM_METHOD
    timagetk.third_party.vt_parser.PM_ESTIMATORS
    timagetk.third_party.vt_parser.DEF_PM_ESTIMATOR
    timagetk.third_party.vt_parser.PROPAGATION_TYPES
    timagetk.third_party.vt_parser.DEF_PROPAGATION_TYPE
    timagetk.third_party.vt_parser.pointmatching_kwargs
    timagetk.third_party.vt_parser.general_kwargs
    timagetk.third_party.vt_parser.parallel_kwargs

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.algorithms.pointmatching import pointmatching
    >>> flo_points = np.array([[0, 0, 0], [0, 0, 1], [0, 1, 0], [1, 0, 0]])
    >>> ref_points = np.array([[0, 0, 0], [0, 0, 2], [0, 2, 0], [2, 0, 0]])
    >>> # - Estimate 3D affine transformation, with points as real unit coordinates:
    >>> trsf = pointmatching(flo_points, ref_points, method="affine")
    >>> trsf.print()
    transformation type is AFFINE_3D
    transformation unit is REAL_UNIT
       0,500000000000000    0,000000000000000    0,000000000000000    0,000000000000000
       0,000000000000000    0,500000000000000    0,000000000000000    0,000000000000000
       0,000000000000000    0,000000000000000    0,500000000000000    0,000000000000000
       0,000000000000000    0,000000000000000    0,000000000000000    1,000000000000000
    >>> from timagetk.algorithms.trsf import inv_trsf
    >>> trsf_inv = inv_trsf(trsf)
    >>> trsf_inv.copy_to_array()
    array([[2., 0., 0., 0.],
           [0., 2., 0., 0.],
           [0., 0., 2., 0.],
           [0., 0., 0., 1.]])
    >>> # - Estimate 3D rigid transformation, with points as real unit coordinates:
    >>> trsf = pointmatching(flo_points, ref_points, method='rigid', params=" -unit real")
    >>> inv_trsf(trsf).copy_to_array()
    array([[1.  , 0.  , 0.  , 0.25],
           [0.  , 1.  , 0.  , 0.25],
           [0.  , 0.  , 1.  , 0.25],
           [0.  , 0.  , 0.  , 1.  ]])
    >>> # - Estimate 3D vectorfield transformation, with points as real unit coordinates:
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.pointmatching import pointmatching
    >>> template = shared_data("flower_confocal", 1)
    >>> flo_points, ref_points = shared_data('flower_multiangle_landmarks', (0, 1))
    >>> trsf = pointmatching(flo_points, ref_points, template, method='vectorfield', fluid_sigma=5.0, vector_propagation_distance=20.0, vector_fading_distance=3.0, params="-unit real -propagation skiz")
    >>> trsf.print()

    """
    # Check the list of points are of equal size:
    try:
        assert len(points_flo) == len(points_ref)
    except AssertionError:
        raise ValueError("Both list of point should be of same size!")
    # If a linear transformation you need at least four points:
    if method in ('rigid', 'affine'):
        try:
            assert len(points_flo) >= 4
        except AssertionError:
            raise ValueError(f"You need at least four pairs of points, but got {len(points_flo)}!")

    points_flo = vtPointList(points_flo)
    points_ref = vtPointList(points_ref)

    if template_img is not None:
        if isinstance(template_img, SpatialImage):
            template_img = template_img.to_vt_image()
        else:
            assert isinstance(template_img, (Image, vtImage))

    # - Take care of extra parameters given as a string:
    extra_params = kwargs.pop("params", "")
    if not extra_params.startswith(" "):
        extra_params = " " + extra_params
    # - Parse pointmatching parameters:
    params, kwargs = pointmatching_kwargs(method, **kwargs)
    # - Parse general kwargs:
    params += general_kwargs(**kwargs)
    # - Parse parallelism kwargs:
    params += parallel_kwargs(**kwargs)
    # - Combine parsed parameters with extra parameters:
    params += extra_params
    log.debug(f"`vt.pointmatching` params: '{params}'")

    out_trsf = vt_pointmatching(points_flo, points_ref, ref=template_img, params=params, **kwargs)

    if out_trsf is None:
        null_msg = "VT method 'pointmatching' returned a NULL transformation!"
        raise ValueError(null_msg)
    else:
        return Trsf(out_trsf)


def apply_trsf_to_points(points, trsf, **kwargs):
    """Apply a given transformation to a set of XYZ point coordinates.

    Parameters
    ----------
    points : numpy.ndarray
        A [N, 3] array of 3D point coordinates, XYZ sorted.
    trsf : Trsf
        The transformation to apply to the points.

    Returns
    -------
    numpy.ndarray
        The registered [N, 3] array of XYZ point coordinates.

    Notes
    -----
    If the points are in real coordinates, you should provide a transformation with a 'real' `unit_type` attribute!

    See Also
    --------
    timagetk.third_party.vt_parser.general_kwargs
    timagetk.third_party.vt_parser.parallel_kwargs

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.trsf import inv_trsf
    >>> from timagetk.algorithms.pointmatching import pointmatching
    >>> from timagetk.algorithms.pointmatching import apply_trsf_to_points
    >>> ref_pts = np.loadtxt(shared_data('p58_t0_reference_ldmk-01.txt', 'p58'))
    >>> flo_pts = np.loadtxt(shared_data('p58_t0_floating_ldmk-01.txt', 'p58'))
    >>> trsf = pointmatching(flo_pts, ref_pts, method='rigid', real=True)
    >>> reg_pts = apply_trsf_to_points(flo_pts, inv_trsf(trsf))
    >>> print(ref_pts)
    >>> print(reg_pts)
    >>> print(flo_pts)

    """
    points = vtPointList(points)

    # - Take care of extra parameters given as a string:
    extra_params = kwargs.pop("params", "")
    if not extra_params.startswith(" "):
        extra_params = " " + extra_params
    # - Parse point-matching parameters:
    params, kwargs = "", {}  # nothing specific to parse for `vt.apply_trsf_to_points`
    # - Parse general kwargs:
    params += general_kwargs(**kwargs)
    # - Parse parallelism kwargs:
    params += parallel_kwargs(**kwargs)
    # - Combine parsed parameters with extra parameters:
    params += extra_params
    log.debug(f"`vt.apply_trsf_to_points` params: '{params}'")

    return vt_apply_trsf_to_points(points, trsf, params=params).copy_to_array()
