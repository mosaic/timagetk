#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Clustering Algorithms Module

This module provides a comprehensive set of clustering-related tools and functions for handling and manipulating
clusters in graph structures or vector-based datasets.
It is useful for applications that require clear and efficient management and computation on cluster assignments,
 distance matrices, graph-based topologies, and standardization techniques.

Key Features

- **Cluster Analysis**:
    - Identify unique clusters (`unique_groups`).
    - Group or subgroup elements by various criteria (`group_elements_by`, `subgroup_by`).
    - Count and calculate the percentage of elements in clusters (`n_elements_by_groups`, `pc_elements_by_groups`).

- **Distance Computation**:
    - Generate distance matrices for numerical or ordinal data (`distance_matrix_from_vector`).
    - Compute topological and Euclidean distance matrices from graph structures (`topological_distance_matrix`, `euclidean_distance_matrix`).

- **Standardization**:
    - Standardize distance data using L1 or L2 norms to normalizing pairwise distance matrices (`standardisation`).

- **Cluster Property Validation**:
    - Validate and manage clusters (`_check_clustering`, `cluster_ids`, `elements_by_cluster`, `n_elements_by_cluster`).

- **Matrix and Graph Utilities**:
    - Create weight matrices based on cluster distance matrices for further use in processing (`create_weight_matrix`).
    - Ensure correct dimensions and validity of distance matrices (`_check_distance_matrix`, `_check_distance_matrix_clustering`).

This module is tailored for use in applications like biological tissue analysis, where managing clusters and distance mappings is key for data interpretation and visualisation.
"""
from collections.abc import Iterable
from itertools import product

import numpy as np
from sklearn.metrics import pairwise_distances

from timagetk.bin.logger import get_logger
from timagetk.features.array_tools import euclidean_distance
from timagetk.graphs.temporal_tissue_graph import TemporalTissueGraph
from timagetk.graphs.tissue_graph import TissueGraph
from timagetk.util import clean_type

log = get_logger(__name__)


def unique_groups(groups, exclude=None):
    """Return the list of unique group ids from the groups list or dictionary.

    Parameters
    ----------
    groups : list or dict
        The list of groups to transfrom.
        If a dictionary, consider it as an id based dictionary of group ids.
    exclude : str or int or list or set, optional
        The list or set of groups to exclude.

    Returns
    -------
    list
        The list of the unique group ids.

    Examples
    --------
    >>> from timagetk.algorithms.clustering import unique_groups
    >>> clusters = [1, 2, 3, 2, 2, -1, 3, 3, 1, 2, 2]  # a list of group ids
    >>> unique_groups(clusters)
    [1, 2, 3, -1]
    >>> unique_groups(clusters, exclude=-1)
    [1, 2, 3]
    >>> clustering = {2:1, 3:2, 4:3, 5:2, 6:2, 7:-1, 8:3, 9:3, 10:1, 11:2, 12:2}  # an id based dict of groups
    >>> unique_groups(clustering, exclude=-1)
    [1, 2, 3]
    >>> identity = ['A', 'B', 'B', 'A', 'D', 'A', 'C', 'A', 'C', 'C']  # a list of group ids
    >>> unique_groups(identity, exclude='D')
    ['A', 'B', 'C']
    >>> identity = {2:'FM', 3:'FM', 4:'FM', 5:'FM', 6:'FM', 7:'FM', 8:'FM', 9:'margin'}  # an id based dict of groups
    >>> unique_groups(identity, exclude='margin')
    ['FM']

    """
    if isinstance(groups, dict):
        unique_ids = set(groups.values())
    else:
        unique_ids = set(groups)
    # Exclude some group(s) if any:
    if isinstance(exclude, str):
        unique_ids -= {exclude}
    elif not isinstance(exclude, Iterable):
        unique_ids -= {exclude}
    else:
        unique_ids -= set(exclude)
    return list(unique_ids)


def subgroup_by(id_dict):
    """Subgrouping a tuple indexed dictionary.

    Parameters
    ----------
    id_dict : dict
        Id based dictionaries.

    Returns
    -------
    dict
        The subgrouped dictionary on the top level.

    Examples
    --------
    >>> from timagetk.algorithms.clustering import subgroup_by
    >>> from timagetk.algorithms.clustering import group_elements_by
    >>> clustering = {2:1, 3:2, 4:3, 5:2, 6:2, 7:-1, 8:3, 9:3, 10:1, 11:2, 12:2}
    >>> layers = {2:1, 3:1, 4:1, 5:1, 6:1, 7:1, 8:2, 9:2, 10:2, 11:3, 12:4}
    >>> elts_by_groups = group_elements_by(clustering, layers, exclude=[-1, [3, 4]])
    >>> print(elts_by_groups)
    {(1, 1): [2], (1, 2): [10], (2, 1): [3, 5, 6], (2, 2): [], (3, 1): [4], (3, 2): [8, 9]}
    >>> subgroup_by(elts_by_groups)
    {1: {1: [2], 2: [10]}, 2: {1: [3, 5, 6], 2: []}, 3: {1: [4], 2: [8, 9]}}

    """
    keys = list(id_dict.keys())
    try:
        assert len(keys[0]) > 1
    except AssertionError:
        raise ValueError("The `id_dict` should be indexed by iterable keys of length greater than 1!")

    high_level_keys = set()
    for k in id_dict.keys():
        high_level_keys |= {k[0]}

    sub = {k: {} for k in high_level_keys}
    for k, v in id_dict.items():
        if len(k[1:]) == 1:
            sub[k[0]][k[1]] = v
        else:
            sub[k[0]][k[1:]] = v

    return sub


def group_elements_by(*id_dicts, exclude=None):
    """Group elements from id based dictionaries.

    Parameters
    ----------
    id_dicts : dict
        Id based dictionaries.
    exclude : list or None, optional
        List of ids to exclude from the id dictionaries.

    Returns
    -------
    dict
        Dictionary of elements indexed by groups.

    Raises
    ------
    ValueError
        If the list of ids to exclude is not of same length than `id_dicts`.

    Examples
    --------
    >>> from timagetk.algorithms.clustering import group_elements_by
    >>> clustering = {2:1, 3:2, 4:3, 5:2, 6:2, 7:-1, 8:3, 9:3, 10:1, 11:2, 12:2}
    >>> group_elements_by(clustering, exclude=[-1])
    {1: [2, 10], 2: [3, 5, 6, 11, 12], 3: [4, 8, 9]}
    >>> layers = {2:1, 3:1, 4:1, 5:1, 6:1, 7:1, 8:2, 9:2, 10:2, 11:3, 12:4}
    >>> group_elements_by(layers, exclude=[[3, 4]])
    {1: [2, 3, 4, 5, 6, 7], 2: [8, 9, 10]}
    >>> group_elements_by(clustering, layers, exclude=[-1, [3, 4]])
    {(1, 1): [2],
     (1, 2): [10],
     (2, 1): [3, 5, 6],
     (2, 2): [],
     (3, 1): [4],
     (3, 2): [8, 9]}
    >>> group_elements_by(layers, clustering, exclude=[[3, 4], -1])
    {(1, 1): [2],
     (1, 2): [3, 5, 6],
     (1, 3): [4],
     (2, 1): [10],
     (2, 2): [],
     (2, 3): [8, 9]}
    >>> identity = {2:'FM', 3:'FM', 4:'FM', 5:'FM', 6:'FM', 7:'FM', 8:'FM', 9:'margin', 10:'margin', 11:'margin', 12:'margin'}
    >>> group_elements_by(clustering, layers, identity, exclude=[-1, [3, 4], 'margin'])
    {(1, 1, 'FM'): [2],
     (1, 2, 'FM'): [],
     (2, 1, 'FM'): [3, 5, 6],
     (2, 2, 'FM'): [],
     (3, 1, 'FM'): [4],
     (3, 2, 'FM'): [8]}

    """
    n_id_dicts = len(id_dicts)
    if exclude is None:
        exclude = [None] * n_id_dicts
    if n_id_dicts == 1 and not isinstance(exclude, Iterable):
        exclude = [exclude]
    # Verify the given list of ids to exclude is of same length than given number of `id_dicts`:
    try:
        assert n_id_dicts == len(exclude)
    except AssertionError:
        raise ValueError("The `exclude` list is not of same length than `id_dicts`!")

    # Get the lists of uniques groups from `id_dicts` (values):
    uniques = []
    for i, id_dict in enumerate(id_dicts):
        uniques.append(unique_groups(id_dict, exclude=exclude[i]))
    # Get the product of uniques groups:
    if n_id_dicts == 1:
        products = {q: [] for q in uniques[0]}
    else:
        products = {q: [] for q in product(*uniques)}

    # Get set of ids from `id_dicts` (keys):
    ids = set()
    for id_dict in id_dicts:
        ids.update(set(id_dict.keys()))

    # Sort the set of ids per their groups' assignment:
    if n_id_dicts == 1:
        for i in ids:
            prod_i = id_dicts[0].get(i, None)
            try:
                products[prod_i].append(i)
            except:
                pass
    else:
        for i in ids:
            prod_i = tuple([id_dict.get(i, None) for id_dict in id_dicts])
            try:
                products[prod_i].append(i)
            except:
                pass

    return products


def n_elements_by_groups(*id_dicts, exclude=None):
    """Number of elements by groups from id based dictionaries.

    Parameters
    ----------
    id_dicts : dict
        Id based dictionaries.
    exclude : list or None, optional
        List of ids to exclude from the id dictionaries.

    Returns
    -------
    dict
        Dictionary of number of elements indexed by groups.

    Examples
    --------
    >>> from timagetk.algorithms.clustering import n_elements_by_groups
    >>> from timagetk.algorithms.clustering import group_elements_by
    >>> clustering = {2:1, 3:2, 4:3, 5:2, 6:2, 7:-1, 8:3, 9:3, 10:1, 11:2, 12:2}
    >>> group_elements_by(clustering, exclude=[-1])
    {1: [2, 10], 2: [3, 5, 6, 11, 12], 3: [4, 8, 9]}
    >>> n_elements_by_groups(clustering, exclude=[-1])
    {1: 2, 2: 5, 3: 3}
    >>> layers = {2:1, 3:1, 4:1, 5:1, 6:1, 7:1, 8:2, 9:2, 10:2, 11:3, 12:4}
    >>> group_elements_by(clustering, layers, exclude=[-1, [3, 4]])
    {(1, 1): [2],
     (1, 2): [10],
     (2, 1): [3, 5, 6],
     (2, 2): [],
     (3, 1): [4],
     (3, 2): [8, 9]}
    >>> n_elements_by_groups(clustering, layers, exclude=[-1, [3, 4]])
    {(1, 1): 1, (1, 2): 1, (2, 1): 3, (2, 2): 0, (3, 1): 1, (3, 2): 2}
    >>> n_elements_by_groups(layers, clustering, exclude=[[3, 4], -1])
    {(1, 1): 1, (1, 2): 3, (1, 3): 1, (2, 1): 1, (2, 2): 0, (2, 3): 2}

    """
    return {gid: len(elts) for gid, elts in group_elements_by(*id_dicts, exclude=exclude).items()}


def pc_elements_by_groups(*id_dicts, exclude=None, sub_groups=False):
    """Percentage of elements by groups from id based dictionaries.

    Parameters
    ----------
    id_dicts : dict
        Id based dictionaries.
    exclude : list or None, optional
        List of ids to exclude from the id dictionaries.

    Returns
    -------
    dict
        Dictionary of percentage of elements indexed by groups.

    Examples
    --------
    >>> from timagetk.algorithms.clustering import pc_elements_by_groups
    >>> from timagetk.algorithms.clustering import n_elements_by_groups
    >>> clustering = {2:1, 3:2, 4:3, 5:2, 6:2, 7:-1, 8:3, 9:3, 10:1, 11:2, 12:2}
    >>> n_elements_by_groups(clustering, exclude=[-1])
    {1: 2, 2: 5, 3: 3}
    >>> pc_elements_by_groups(clustering, exclude=[-1])
    {1: 0.2, 2: 0.5, 3: 0.3}
    >>> layers = {2:1, 3:1, 4:1, 5:1, 6:1, 7:1, 8:2, 9:2, 10:2, 11:3, 12:4}
    >>> n_elements_by_groups(layers, exclude=[[3, 4]])
    {1: 6, 2: 3}
    >>> pc_elements_by_groups(layers, exclude=[[3, 4]])
    {1: 0.6666666666666666, 2: 0.3333333333333333}
    >>> n_elements_by_groups(clustering, layers, exclude=[-1, [3, 4]])
    {(1, 1): 1, (1, 2): 1, (2, 1): 3, (2, 2): 0, (3, 1): 1, (3, 2): 2}
    >>> pc_elements_by_groups(clustering, layers, exclude=[-1, [3, 4]])
    {(1, 1): 0.125,
     (1, 2): 0.125,
     (2, 1): 0.375,
     (2, 2): 0.0,
     (3, 1): 0.125,
     (3, 2): 0.25}
    >>> n_elements_by_groups(layers, clustering, exclude=[[3, 4], -1])
    {(1, 1): 1, (1, 2): 3, (1, 3): 1, (2, 1): 1, (2, 2): 0, (2, 3): 2}
    >>> pc_elements_by_groups(layers, clustering, exclude=[[3, 4], -1])
    {(1, 1): 0.125,
     (1, 2): 0.375,
     (1, 3): 0.125,
     (2, 1): 0.125,
     (2, 2): 0.0,
     (2, 3): 0.25}
    >>> pc_elements_by_groups(layers, clustering, exclude=[[3, 4], -1], sub_groups=True)
    {1: {1: 0.2, 2: 0.6, 3: 0.2},
     2: {1: 0.3333333333333333, 2: 0.0, 3: 0.6666666666666666}}
    >>> pc_elements_by_groups(clustering, layers, exclude=[-1, [3, 4]], sub_groups=True)
    {1: {1: 0.5, 2: 0.5},
     2: {1: 1.0, 2: 0.0},
     3: {1: 0.3333333333333333, 2: 0.6666666666666666}}

    """
    n_elts_by_groups = n_elements_by_groups(*id_dicts, exclude=exclude)

    def _pc_elements_by_groups(n_elts_by_groups):
        pc = {}
        total = sum(list(n_elts_by_groups.values()))
        for gid, n_elts in n_elts_by_groups.items():
            if total == 0:
                pc[gid] = 0.
            else:
                pc[gid] = n_elts / total
        return pc

    if sub_groups:
        pc = {gid: _pc_elements_by_groups(n_elts_dict) for gid, n_elts_dict in subgroup_by(n_elts_by_groups).items()}
    else:
        pc = _pc_elements_by_groups(n_elts_by_groups)

    return pc


def distance_matrix_from_vector(data, variable_types="numeric"):
    """Function creating a distance matrix based on a vector (list) of values.

    Parameters
    ----------
    data : list or numpy.ndarray
        Vector of values (1D).
    variable_types : {"numeric", "ordinal"}
        Type of variable, used to defines method for distance computation.

    Returns
    -------
    numpy.ndarray
        Distance matrix

    Notes
    -----
    Each values are attached to an individual.
    - "numeric" variables can be 1D, 2D or 3D.
    - "ordinal" variables must be 1D.

    Examples
    --------
    >>> # Example #1 - Test data:
    >>> import numpy as np
    >>> from timagetk.algorithms.clustering import distance_matrix_from_vector
    >>> distance_matrix_from_vector([0.5, 2., np.nan, 3.5])
    array([[0. , 1.5, nan, 3. ],
           [1.5, 0. , nan, 1.5],
           [nan, nan, nan, nan],
           [3. , 1.5, nan, 0. ]])
    >>> distance_matrix_from_vector([1, 2, np.nan, 3], "ordinal")
    array([[ 0.,  1., nan,  2.],
           [ 1.,  0., nan,  1.],
           [nan, nan, nan, nan],
           [ 2.,  1., nan,  0.]])
    >>> # Example #2 - Real data from tissue image:
    >>> from timagetk import TissueImage3D
    >>> from timagetk.algorithms.clustering import distance_matrix_from_vector
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.tissue_graph import tissue_graph_from_image
    >>> from matplotlib import pyplot as plt
    >>> tissue = TissueImage3D(shared_data('flower_labelled', 0), background=1, not_a_label=0)
    >>> data = list(tissue.cells.volume().values())  # get the lis of cell volume
    >>> plt.hist(data, bins=100)  # some outliers (big values) from segmentation errors
    >>> plt.title("Cell volume histogram")
    >>> plt.close()
    >>> # Remove the outliers from the list of cells volume:
    >>> from timagetk.features.array_tools import quantile_exclusion
    >>> data = quantile_exclusion(data, q=0.98)
    >>> plt.hist(data, bins=50)  # cell volume distribution is now "clearer"
    >>> plt.title("Cell volume histogram (trimmed)")
    >>> plt.close()
    >>> # Compute the distance matrix and display it:
    >>> mat = distance_matrix_from_vector(data, 'numeric')
    >>> plt.imshow(mat, cmap="viridis")
    >>> plt.colorbar()
    >>> plt.title("Distance matrix - Cell volume")
    >>> plt.close()

    """
    if isinstance(data, (list, set, tuple)):
        data = np.array(data)
    elif isinstance(data, np.ndarray):
        try:
            assert data.ndim == 1
        except AssertionError:
            raise ValueError(f"Wrong dimension for the input ndarray `data`. Got '{data.ndim}', expected '1'!")
    else:
        raise TypeError(f"Unknown type for input `data`: {clean_type(data)}!")

    N = len(data)
    dist_mat = np.zeros(shape=[N, N], dtype=float)

    is_nan = np.isnan(data)
    if True in is_nan:
        data = np.nan_to_num(data, nan=0)

    if variable_types == "numeric":
        dist_mat = pairwise_distances(data.reshape(-1, 1), metric='euclidean', n_jobs=-1, force_all_finite=False)

    if variable_types == "ordinal":
        from scipy.stats import rankdata
        # In case of ordinal variables, observed values are replaced by ranked values.
        rank = rankdata(data, method="ordinal")
        dist_mat = np.abs(rank[np.newaxis, :] - rank[:, np.newaxis])
        dist_mat = dist_mat.astype(float)  # convert to float to be able to add np.nan later (if necessary)

    if True in is_nan:
        dist_mat[:, is_nan] = dist_mat[is_nan, :] = np.nan

    np.fill_diagonal(dist_mat, 0.)
    return dist_mat


def standardisation(data, norm='L1', variable_types=None, outliers=None):
    """Data standardisation procedure.

    Parameters
    ----------
    data : numpy.ndarray or list
        Pairwise distance matrix if a ``numpy.ndarray`` or vector of values if a ``list``.
    norm : {"L1", "L2"}, optional
        Define which standardisation metric to apply to the data, "L1" by default.
    variable_types : {"numeric", "ordinal"}, optional
        Used only if a vector of values is provided for `data`.
    outliers : numpy.ndarray or list, optional
        Array or list of booleans, sorted in the same order as `data` (and thus of the same length).
        Define if the associated value is an outlier or not. By default, no outliers.

    Returns
    -------
    numpy.ndarray
        Standardized pairwise distance matrix.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.algorithms.clustering import standardisation
    >>> # EXAMPLE #1: Compute & standardize a distance matrix from a vector of values (with NaN):
    >>> smat = standardisation([1., 2., 3., np.nan])
    >>> print(smat)
    [[0.   0.75 1.5   nan]
     [0.75 0.   0.75  nan]
     [1.5  0.75 0.    nan]
     [ nan  nan  nan 0.  ]]
    >>> # EXAMPLE #2: Compute & standardize a distance matrix from a vector of values (with outliers):
    >>> smat = standardisation([1., 2., 3., 10.], outliers=[0, 0, 0, 1])
    >>> print(smat)
    [[0.   0.75 1.5   nan]
     [0.75 0.   0.75  nan]
     [1.5  0.75 0.    nan]
     [ nan  nan  nan 0.  ]]
    >>> # EXAMPLE #3: Compute & standardize a distance matrix from a real dataset:
    >>> from timagetk import TissueImage3D
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.components.tissue_image import cell_layers_from_image
    >>> tissue = TissueImage3D(shared_data('flower_labelled', 0), background=1, not_a_label=0)
    >>> l1_cells = cell_layers_from_image(tissue, [1])[1]  # get the list of cells from the first layer
    >>> print(f"Found {len(l1_cells)} cells in the first layer")
    >>> data = list(tissue.cells.volume(l1_cells).values())  # get the list of cell volume
    >>> print(f"Original volume dataset with {len(data)} values")
    >>> # Remove the outliers from the list of cells volume:
    >>> from timagetk.features.array_tools import quantile_exclusion
    >>> data = quantile_exclusion(data, q=0.95)
    >>> print(f"Volume dataset with {len(data)} values after exclusion of values above the 95th percentile")
    >>> # Compute the distance matrix:
    >>> from timagetk.algorithms.clustering import distance_matrix_from_vector
    >>> mat = distance_matrix_from_vector(data, 'numeric')
    >>> # Standardize the distance matrix:
    >>> smat = standardisation(mat)
    >>> from matplotlib import pyplot as plt
    >>> plt.title("Distance matrix for p58-t0 L1 cell volumes.")
    >>> ax = plt.imshow(smat, cmap='viridis')
    >>> plt.colorbar(ax)
    >>> plt.show()

    """
    if (norm.upper() != 'L1') and (norm.upper() != 'L2'):
        raise ValueError("Undefined standardisation metric")

    # -- Identifying case where given `data` is a 1D numpy.ndarray:
    if isinstance(data, np.ndarray):
        row_array = (data.shape[1] == 1) and (data.shape[0] > 1)
        col_array = (data.shape[0] == 1) and (data.shape[1] > 1)
        if row_array or col_array:
            data.tolist()
        else:
            # Check we have a square 2D array:
            _check_distance_matrix(data)
            dist_mat = data

    # -- Creating the pairwise distance matrix if not provided in `data`:
    if isinstance(data, list):
        list_of_list = isinstance(data[0], list)
        if list_of_list:
            square_list_of_list = all([len(data) == len(data_i) for data_i in data])
            # If a square list of list, it is a pairwise distance matrix.
            if square_list_of_list:
                # Convert to a numpy array:
                dist_mat = np.array(data)
            else:
                raise ValueError("Provided data is a list of list but it is not square!")
        else:
            # Create the distance matrix if a 1D vector:
            if variable_types is None:
                log.warning("No `variable_types` defined to compute the distance matrix from vector...")
                log.info("Using 'numeric' type.")
                variable_types = "numeric"
            log.info("Computing the distance matrix...")
            dist_mat = distance_matrix_from_vector(data, variable_types)

    # -- Handling outliers by setting their value to ``np.nan`` (will exclude them of standardisation value computation).
    if outliers is not None:
        if len(outliers) != dist_mat.shape[0]:
            log.warning("The `outliers` list provided is not of the same length than the data!!")
            log.info(f"Outliers list length = {len(outliers)}")
            log.info(f"Distance matrix shape = {dist_mat.shape}")
        else:
            outliers_index = [n for n, i in enumerate(outliers) if i]
            dist_mat[outliers_index, :] = dist_mat[:, outliers_index] = np.nan

    # -- Perform standardization:
    # - Get the number of missing values in the pairwise distance matrix:
    np.fill_diagonal(dist_mat, 0.)
    nan_mask = np.isnan(dist_mat)
    nb_missing_values = np.sum(nan_mask)
    # - Get the number of samples in the distance matrix:
    N = dist_mat.shape[0]
    # - Compute the normalization factor:
    if norm.upper() == "L1":
        # Compute L1 metric normalization factor:
        norm_factor = np.nansum(dist_mat) / (N * (N - 1) - nb_missing_values)
    else:
        # Compute L2 metric normalization factor:
        norm_factor = np.nansum(dist_mat ** 2) / (N * (N - 1) - nb_missing_values)

    return dist_mat / float(norm_factor)


def create_weight_matrix(N, weight, standard_distance_matrix):
    """Create the square weight matrix.

    Parameters
    ----------
    N : int
        Size of the square weight matrix to creates.
    weight : float
        Weight to use in this weight matrix.
    standard_distance_matrix : numpy.ndarray
        Standardized distance matrix, used to localize ``np.nan`` values.

    Returns
    -------
    numpy.ndarray
        A square weight matrix.

    """
    tmp_w_mat = np.zeros(shape=[N, N], dtype=float)
    tmp_w_mat.fill(weight)
    tmp_w_mat[np.where(np.isnan(standard_distance_matrix))] = np.nan
    return tmp_w_mat


def _check_distance_matrix(distance_matrix):
    """Assert the distance matrix is a square 2D matrix.

    Parameters
    ----------
    distance_matrix : numpy.ndarray
        Distance matrix used to create the clustering.

    Raises
    ------
    ValueError
        If the distance matrix is not a 2D matrix.
        If the distance matrix is not a square matrix.
    """
    # Assert the distance matrix is a 2D matrix:
    try:
        assert distance_matrix.ndim == 2
    except AssertionError:
        raise ValueError(f"The distance matrix should be a 2D matrix, got {distance_matrix.ndim}D!")
    # Assert the distance matrix is a SQUARE 2D matrix:
    try:
        ni, nj = distance_matrix.shape
        assert ni == nj
    except AssertionError:
        raise ValueError(
            f"The distance matrix should be a square 2D matrix, got a matrix of shape {distance_matrix.shape}!")
    return


def _check_distance_matrix_clustering(distance_matrix, clustering):
    """Assert the distance matrix and the list of cluster ids are of same size.

    Parameters
    ----------
    distance_matrix : numpy.ndarray
        Distance matrix used to create the clustering.
    clustering : list
        Resulting list of cluster ids.

    Raises
    ------
    ValueError
        If the distance matrix and list of cluster ids are not of same size.
    """
    _check_distance_matrix(distance_matrix)
    nd, _ = distance_matrix.shape
    nc = len(clustering)
    # Assert the distance matrix and the list of cluster ids are of same size:
    try:
        assert nd == nc
    except AssertionError:
        raise ValueError(f"The distance matrix ({nd}x{nd}) and list of cluster ids ({nc}) should be of same size!")
    return


def _check_clustering(clustering, outliers_cluster_id=None):
    """Check the clustering validity, from a list of cluster labels or an id based clustering dictionary.

    Parameters
    ----------
    clustering : list or dict
        A list of cluster labels or an id based clustering dictionary.
    outliers_cluster_id : None or int or str, optional
        The id of the cluster regrouping the outliers to be ignored.

    Raises
    ------
    ValueError
        If there is at least one cluster with a single element.
        If there is only one cluster.

    Examples
    --------
    >>> from timagetk.algorithms.clustering import _check_clustering
    >>> clustering = {2:1, 3:2, 4:3, 5:2, 6:2, 7:-1, 8:3, 9:3, 10:1, 11:2, 12:2}  # an id based dict of clusters
    >>> _check_clustering(clustering)  # cluster `-1` has only one element, this is wrong!
    ValueError: A cluster contain only one element!
    >>> _check_clustering(clustering, outliers_cluster_id=-1)  # should be ok now!

    """
    n_elt_by_cluster = n_elements_by_cluster(clustering, outliers_cluster_id)
    if 1 in n_elt_by_cluster.values():
        log.info(f"Number of elements by cluster: {n_elt_by_cluster}")
        raise ValueError("A cluster contain only one element!")
    if len(n_elt_by_cluster) == 1:
        raise ValueError("There is only one cluster!")
    return


def cluster_ids(clustering, outliers_cluster_id=None):
    """Return the list of valid cluster ids, from a list of cluster labels or an id based clustering dictionary.
    We consider a cluster id to be valid if it's not declared as the one regrouping the outliers.

    Parameters
    ----------
    clustering : list or dict
        A list of cluster labels or an id based clustering dictionary.
    outliers_cluster_id : None or int or str, optional
        The id of the cluster regrouping the outliers to be ignored.

    Returns
    -------
    list
        The list of valid cluster ids.

    Examples
    --------
    >>> from timagetk.algorithms.clustering import cluster_ids
    >>> cluster_labels = [1, 2, 3, 2, 2, -1, 3, 3, 1, 2, 2]  # an id based list of cluster labels
    >>> cluster_ids(clustering)
    [1, 2, 3, -1]
    >>> clustering = {2:1, 3:2, 4:3, 5:2, 6:2, 7:-1, 8:3, 9:3, 10:1, 11:2, 12:2}  # an id based dict of clusters
    >>> cluster_ids(clustering)
    [1, 2, 3, -1]
    >>> cluster_ids(clustering, outliers_cluster_id=-1)
    [1, 2, 3]

    """
    return unique_groups(clustering, exclude=outliers_cluster_id)


def elements_by_cluster(clustering, outliers_cluster_id=None):
    """Return the element ids grouped by cluster id, from an id based clustering dictionary.

    Parameters
    ----------
    clustering : dict
        An id based clustering dictionary.
    outliers_cluster_id : None or int or str, optional
        The id of the cluster regrouping the outliers to be ignored.

    Returns
    -------
    dict
        Element ids grouped by cluster id.

    Examples
    --------
    >>> from timagetk.algorithms.clustering import elements_by_cluster
    >>> clustering = {2:1, 3:2, 4:3, 5:2, 6:2, 7:-1, 8:3, 9:3, 10:1, 11:2, 12:2}  # an id based dict of clusters
    >>> elements_by_cluster(clustering)
    {1: [2, 10], 2: [3, 5, 6, 11, 12], 3: [4, 8, 9], -1: [7]}
    >>> elements_by_cluster(clustering, outliers_cluster_id=-1)
    {1: [2, 10], 2: [3, 5, 6, 11, 12], 3: [4, 8, 9]}
    >>> clustering = {2:'A', 3:'B', 4:'B', 5:'A', 6:'D', 7:'A', 8:'C', 9:'A', 10:'C', 11:'C'}  # an id based dict of clusters
    >>> elements_by_cluster(clustering, outliers_cluster_id='D')
    {'C': [8, 10, 11], 'B': [3, 4], 'A': [2, 5, 7, 9]}

    """
    return group_elements_by(clustering, exclude=outliers_cluster_id)


def n_elements_by_cluster(clustering, outliers_cluster_id=None):
    """Return the number of element by clusters, from a list of cluster labels or an id based clustering dictionary.

    Parameters
    ----------
    clustering : list or dict
        A list of cluster labels or an id based clustering dictionary.
    outliers_cluster_id : None or int or str, optional
        The id of the cluster regrouping the outliers to be ignored.

    Returns
    -------
    dict
        Number of ids grouped by cluster id.

    Examples
    --------
    >>> from timagetk.algorithms.clustering import n_elements_by_cluster
    >>> cluster_labels = [1, 2, 3, 2, 2, -1, 3, 3, 1, 2, 2]  # an id based list of cluster labels
    >>> n_elements_by_cluster(cluster_labels)
    {1: 2, 2: 5, 3: 3, -1: 1}
    >>> n_elements_by_cluster(cluster_labels, outliers_cluster_id=-1)
    {1: 2, 2: 5, 3: 3}
    >>> clustering = {2:1, 3:2, 4:3, 5:2, 6:2, 7:-1, 8:3, 9:3, 10:1, 11:2, 12:2}  # an id based dict of clusters
    >>> n_elements_by_cluster(clustering)
    {1: 2, 2: 5, 3: 3, -1: 1}
    >>> n_elements_by_cluster(clustering, outliers_cluster_id=-1)
    {1: 2, 2: 5, 3: 3}
    >>> clustering = {2:'A', 3:'B', 4:'B', 5:'A', 6:'D', 7:'A', 8:'C', 9:'A', 10:'C', 11:'C'}  # an id based dict of clusters
    >>> n_elements_by_cluster(clustering, outliers_cluster_id='D')
    {'A': 4, 'B': 2, 'C': 3}

    """
    if isinstance(clustering, (list, tuple)):
        from collections import Counter
        if isinstance(clustering, dict):
            n_ids_by_cluster = dict(Counter(clustering.values()))
        else:
            n_ids_by_cluster = dict(Counter(clustering))
        # Remove the entry related to the outlier cluster:
        if outliers_cluster_id is not None and outliers_cluster_id in n_ids_by_cluster:
            n_ids_by_cluster.pop(outliers_cluster_id)
        return n_ids_by_cluster
    else:
        return n_elements_by_groups(clustering, exclude=outliers_cluster_id)


def topological_distance_matrix(graph, ids=None):
    """Compute the topological distance matrix based on the graph.

    Parameters
    ----------
    graph : timagetk.graphs.TissueGraph or timagetk.graphs.TemporalTissueGraph
        The (temporal) tissue graph to extract the topological distance matrix from.
    ids : list, optional
        List of ids to use for topological distance matrix creation.

    Examples
    --------
    >>> from timagetk.io.graph_dataset import example_tissue_graph    >>> from matplotlib import pyplot as plt
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph
    >>> from timagetk.algorithms.clustering import topological_distance_matrix
    >>> # Compute the topological distance matrix from a TissueGraph
    >>> tg = example_tissue_graph()
    >>> d_topo = topological_distance_matrix(tg)
    >>> plt.imshow(d_topo, interpolation='none', aspect='equal', origin='upper')
    >>> # Compute the topological distance matrix from a TemporalTissueGraph
    >>> ttg = example_temporal_tissue_graph()
    >>> d_topo = topological_distance_matrix(ttg)
    >>> plt.imshow(d_topo, interpolation='none', aspect='equal', origin='upper')

    """
    if ids is None:
        if isinstance(graph, TemporalTissueGraph):
            ids = [(tp, i) for tp in range(graph.nb_time_points) for i in graph.cell_ids(tp)]
        elif isinstance(graph, TissueGraph):
            ids = list(graph.cell_ids())
        else:
            raise TypeError(f"Unknown type for `graph` data structure `{clean_type(graph)}`...")

    log.info("Computing the standardized topological distance matrix...")
    import time
    t = time.time()
    if isinstance(graph, TemporalTissueGraph):
        # - Extraction of the topological distance between all vertex of a time point:
        topo_dist = {(tp, i): graph._tissue_graph[tp].topological_distance(i) for (tp, i) in ids}
        # - Transformation into a distance matrix
        dist_arr = np.array([[(topo_dist[(ti, i)][j] if i != j and ti == tj else np.nan)
                              for (ti, i) in ids] for (tj, j) in ids])
    else:
        # - Extraction of the topological distance between all vertex of a time point:
        topo_dist = {cid: graph.topological_distance(cid) for cid in ids}
        # - Transformation into a distance matrix
        dist_arr = np.array([[(topo_dist[i][j] if i != j else np.nan) for i in ids] for j in ids])
    np.fill_diagonal(dist_arr, 0)
    log.info(f"Distance matrix created in {time.time() - t}s")

    return dist_arr


def euclidean_distance_matrix(graph, ids=None):
    """Compute the Euclidean distance matrix based on the graph.

    Parameters
    ----------
    graph : timagetk.graphs.TissueGraph or timagetk.graphs.TemporalTissueGraph
        The (temporal) tissue graph to extract the topological distance matrix from.
        The graph must contain a 'barycenter' cell property!
    ids : list, optional
        List of ids to use for Euclidean distance matrix creation.

    Examples
    --------
    >>> from matplotlib import pyplot as plt
    >>> from timagetk.graphs.tissue_graph import example_tissue_graph
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph
    >>> from timagetk.algorithms.clustering import euclidean_distance_matrix
    >>> # Compute the Euclidean distance matrix from a TissueGraph
    >>> tg = example_tissue_graph()
    >>> d_eucl = euclidean_distance_matrix(tg)
    >>> plt.imshow(d_eucl, interpolation='none', aspect='equal', origin='upper')
    >>> # Compute the Euclidean distance matrix from a TemporalTissueGraph
    >>> ttg = example_temporal_tissue_graph()
    >>> d_eucl = euclidean_distance_matrix(ttg)
    >>> plt.imshow(d_eucl, interpolation='none', aspect='equal', origin='upper')

    """
    if ids is None:
        if isinstance(graph, TemporalTissueGraph):
            ids = [(tp, i) for tp in range(graph.nb_time_points) for i in graph.cell_ids(tp)]
        elif isinstance(graph, TissueGraph):
            ids = list(graph.cell_ids())
        else:
            raise TypeError(f"Unknown type for `graph` data structure `{clean_type(graph)}`...")

    log.info("Computing the Euclidean distance matrix...")
    import time
    t = time.time()
    if isinstance(graph, TemporalTissueGraph):
        bary = {(tp, cid): graph._tissue_graph[tp].cell_property(cid, 'barycenter', default=np.nan) for (tp, cid) in
                ids}
        dist_arr = np.array([[euclidean_distance(bary[(ti, i)], bary[(tj, j)] if i != j and ti == tj else np.nan)
                              for (ti, i) in ids] for (tj, j) in ids])
    else:
        bary = {cid: graph.cell_property(cid, 'barycenter', default=np.nan) for cid in ids}
        # - Transformation into a distance matrix
        dist_arr = np.array([[(euclidean_distance(bary[i], bary[j]) if i != j else np.nan) for i in ids] for j in ids])
    np.fill_diagonal(dist_arr, 0)
    log.info(f"Distance matrix created in {time.time() - t}s")

    return dist_arr
