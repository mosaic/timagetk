#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Surface Reconstruction and Intensity Projection Module

This module provides methods and algorithms for reconstructing the outer surface of objects in 3D images,
as well as generating intensity or label projections.
It is designed for use with 3D biological or segmented images to facilitate the visualisation of cellular
or tissue structures.
This module is particularly useful for researchers working in computational biology or image analysis who require
surface projections, altitude maps, or contour extractions from volumetric data.

Key Features

- **Surface reconstruction**: Create altitude maps and intensity contour projections from 3D images.
- **Segmentation projection & analysis**: Project labelled images, generating meaningful 2D representations
  for visualizing labelled regions.
- **Contour extraction**: Highlight or identify boundaries within segmented images.
- **Image padding & region modification**: Apply padding with specific values or add margin operations
  to adjust regions of interest in 3D datasets.
- **General altitude map computation**: Compute projection maps based on specified orientations, axis, and image thresholds.

Usage Examples

A minimal example demonstrating surface projection and altitude map generation:

```python
from timagetk.io.dataset import shared_data
from timagetk.algorithms.reconstruction import image_surface_projection
from timagetk.visu.mplt import grayscale_imshow

# Load a shared sample dataset (e.g., confocal flower dataset)
image = shared_data('flower_confocal', 0)

# Compute surface projection and the associated altitude map:
proj_image, alt_map = image_surface_projection(image, orientation=1, gaussian_sigma=1.0)

# Visualize output using matplotlib visualisation utilities:
grayscale_imshow([proj_image, alt_map],
                 title=["Surface Projection", "Altitude Map"],
                 cmap=["gray", "viridis"])
```
"""

import numpy as np
from scipy import ndimage as nd
from tqdm.autonotebook import tqdm

from timagetk import TissueImage3D
from timagetk.algorithms.connexe import connected_components
from timagetk.algorithms.resample import isometric_resampling
from timagetk.array_util import guess_image_orientation
from timagetk.array_util import guess_intensity_threshold
from timagetk.bin.logger import get_logger
from timagetk.components.image import assert_spatial_image
from timagetk.components.image import get_image_attributes
from timagetk.components.labelled_image import LabelledImage
from timagetk.components.labelled_image import connectivity_4
from timagetk.components.labelled_image import connectivity_6
from timagetk.components.labelled_image import connectivity_8
from timagetk.components.metadata import IMAGE_MD_TAGS
from timagetk.components.spatial_image import SpatialImage
from timagetk.tasks.decorators import multichannel_wrapper
from timagetk.tasks.decorators import singlechannel_wrapper

log = get_logger(__name__)


def end_margin(img, width, axis=None, null_value=0):
    """Set selected end margin axis to a null value.

    Parameters
    ----------
    img : numpy.ndarray
        A 3D array to modify.
    width : int
        Size of the margin.
    axis : int, optional
        Axis along which the margin is edited. Edit them all by default.
    null_value : int, optional
        The null value to use for the end margins, ``0`` by default.

    Returns
    -------
    numpy.ndarray
        Input array with the end margin set to a null value.

    See Also
    --------
    stroke

    Example
    -------
    >>> from timagetk.algorithms.reconstruction import end_margin
    >>> from timagetk.array_util import random_spatial_image
    >>> from timagetk import SpatialImage
    >>> from timagetk.visu.stack import orthogonal_view
    >>> img = random_spatial_image((5, 10, 10), dtype='uint8')
    >>> img_m = SpatialImage(end_margin(img, 1), voxelsize=img.get_voxelsize())
    >>> orthogonal_view(img, cmap='viridis', suptitle="Original")
    >>> orthogonal_view(img_m, cmap='viridis', suptitle="+end_margin")

    """
    xdim, ydim, zdim = img.shape
    mat = np.zeros((xdim, ydim, zdim), img.dtype)
    mat = mat * null_value

    if axis is None:
        mat[width:-width, width:-width, width:-width] = img[width:-width, width:-width, width:-width]
    elif axis == 0:
        mat[width:-width, :, :] = img[width:-width, :, :]
    elif axis == 1:
        mat[:, width:-width, :] = img[:, width:-width, :]
    elif axis == 2:
        mat[:, :, width:-width] = img[:, :, width:-width]
    else:
        raise ValueError(f"Unknown axis '{axis}' for {img.ndim}D array.")

    return mat


@multichannel_wrapper
def padding(image, z_pad=(0, 0), y_pad=(0, 0), x_pad=(0, 0), pad_value=0):
    """Set an outline of values to an image.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.LabelledImage or timagetk.TissueImage3D or timagetk.MultiChannelImage or numpy.ndarray
        A 3D image to modify by applying padding.
        In case of a numpy array, consider the axes to be ZYX sorted.
    z_pad : (int, int), optional
        Padding to apply to the beginning and end of the z-axis.
        Defaults to ``(0, 0)``.
    y_pad : (int, int), optional
        Padding to apply to the beginning and end of the y-axis.
        Defaults to ``(0, 0)``.
    x_pad : (int, int), optional
        Padding to apply to the beginning and end of the x-axis.
        Defaults to ``(0, 0)``.
    pad_value : int, optional
        The padding value to use. Defaults to ``0``.

    Returns
    -------
    timagetk.SpatialImage or timagetk.LabelledImage or timagetk.TissueImage3D or timagetk.MultiChannelImage or numpy.ndarray
        Input image with the padding.

    See Also
    --------
    end_margins

    Example
    -------
    >>> from timagetk.array_util import random_spatial_image
    >>> from timagetk.algorithms.reconstruction import padding
    >>> from timagetk import SpatialImage
    >>> from timagetk.visu.stack import orthogonal_view
    >>> img = random_spatial_image((5, 8, 8), dtype='uint8')
    >>> # Example: Add a slice of null value on top of stack:
    >>> img_s = padding(img, (1, 0))
    >>> print(img.shape)
    (5, 8, 8)
    >>> print(img_s.shape)
    (6, 8, 8)
    >>> orthogonal_view(img, cmap='viridis', suptitle="Original")
    >>> orthogonal_view(img_s, cmap='viridis', suptitle="Padded image")

    """
    try:
        assert image.ndim >= 3
    except AssertionError:
        log.error("Padding can only be applied to 3D images!")
        return None

    from timagetk.components.image import get_image_class
    Image = get_image_class(image)
    attr = get_image_attributes(image, extra=['filename'])

    zsh, ysh, xsh = image.shape
    # -- Create the padded image:
    mat = np.ones([z_pad[0] + zsh + z_pad[1],
                   y_pad[0] + ysh + y_pad[1],
                   x_pad[0] + xsh + x_pad[1]], image.dtype) * pad_value
    # -- Copy the images values to the right position in padded image:
    mat[z_pad[0]: zsh + z_pad[0],
    y_pad[0]: ysh + y_pad[0],
    x_pad[0]: xsh + x_pad[0]] = image

    return Image(mat, **attr)


@singlechannel_wrapper
def im2surface(image, threshold_value=45, front_half_only=False, **kwargs):
    """Contour altitude map and intensity projection.

    This function computes a surfacic view of the object, according to a revisited version of the method described in [Barbier]_.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        Image to use for altitude map and intensity projection.
    threshold_value : int or float
        Consider intensities greater than this threshold.
    front_half_only : bool
        If ``True``, only consider the first half of all slices along the last physical axis.
        Defaults to ``False``.

    Other Parameters
    ----------------
    altimap_smoothing : bool
        If ``True``, default is ``False``, smooth the altitude map obtained from the mask.

    Returns
    -------
    timagetk.SpatialImage
        Contour intensity projection.
    timagetk.SpatialImage
        Altitude of maximum intensity projection.

    References
    ----------
    .. [Barbier] Barbier de Reuille, P. , Bohn‐Courseau, I. , Godin, C. and Traas, J. (2005), *A protocol to analyse cellular dynamics during plant development*. **The Plant Journal**, 44: 1045-1053. `DOI <https://doi.org/10.1111/j.1365-313X.2005.02576.x>`_.

    Example
    -------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.reconstruction import im2surface
    >>> from timagetk.visu.stack import stack_panel
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> # - Get 'p58' shared intensity image:
    >>> image = shared_data('flower_confocal', 0)
    >>> mip_img, alt_img = im2surface(image, 60, altimap_smoothing=True)
    >>> fig = grayscale_imshow([mip_img, alt_img], suptitle="im2surface", title=["Contour Projection", "Altitude Map"], val_range='auto', cmap=['gray', 'viridis'])

    """
    assert_spatial_image(image, 'image')
    vxs = image.voxelsize
    ori = image.origin
    dtype = image.dtype

    # - Definition of ZYX flat 3D structuring elements:
    connect_4 = connectivity_4().reshape(1, 3, 3)
    connect_6 = connectivity_6()
    connect_8 = connectivity_8().reshape(1, 3, 3)

    image = image.invert_axis("z")
    # - Detect the 'background' surrounding the object:
    from timagetk.algorithms.linearfilter import linearfilter
    labeling = connected_components(linearfilter(image, "smoothing", sigma=3, real=False),
                                    low_threshold=threshold_value)

    # - Performs a series of dilation and erosion:
    iterations = 15
    dilation1 = nd.binary_dilation(labeling, connect_8, iterations)
    del labeling
    iterations = 10
    erosion1 = nd.binary_erosion(dilation1, connect_8, iterations, border_value=1)
    del dilation1
    iterations = 15
    dilation2 = nd.binary_dilation(erosion1, connect_8, iterations)
    del erosion1
    iterations = 4
    erosion2 = nd.binary_erosion(dilation2, connect_4, iterations, border_value=1)
    del dilation2
    iterations = 15
    erosion3 = nd.binary_erosion(erosion2, connect_8, iterations, border_value=1)
    del erosion2
    iterations = 1
    erosion4 = nd.binary_erosion(erosion3, connect_6, iterations, border_value=1)
    erosion4 = end_margin(erosion4, 1)  # Set the margins to a null value
    iterations = 3
    erosion5 = nd.binary_erosion(erosion4, connect_6, iterations, border_value=1)
    del erosion4
    iterations = 9
    erosion6 = nd.binary_erosion(erosion5, connect_8, iterations, border_value=1)
    del erosion5
    m_xor = np.logical_xor(erosion3, erosion6)
    del erosion3
    del erosion6

    mat2 = m_xor
    mat2[:-mat2.shape[0] - 1, :, :] = 0

    mat2 = np.ubyte(mat2)
    m_and = np.where(mat2 == 1, image, 0)
    del mat2

    if front_half_only:
        m_and[m_and.shape[2] / 2:, :, :] = 0

    z, y, x = m_and.shape
    m_alt = m_and.argmax(0).reshape(x, y)

    try:
        assert m_alt is not None
    except AssertionError:
        raise ValueError("Obtained altitude map is None!")

    if kwargs.get('altimap_smoothing', False):
        m_alt = nd.gaussian_filter(m_alt.astype(float), sigma=5, mode='nearest')

    md = image.metadata
    # Clear image tags, they will be recomputed by SpatialImage.__new__
    for attr in IMAGE_MD_TAGS + ['extent']:
        md.pop(attr)
    # Get the "altitude map":
    m_alt = SpatialImage(m_alt, origin=ori[1:], voxelsize=vxs[1:], metadata=md, unit=image.unit)
    # Get the "contour projection" image:
    m_mip = m_and.max(axis=0).reshape(x, y)
    m_mip = SpatialImage(m_mip, origin=ori[1:], voxelsize=vxs[1:], metadata=md, dtype=dtype, unit=image.unit)
    return m_mip, m_alt


def surface2im(points, altitude_map, vz=1):
    """Convert points from contour projection to the real world.

    Parameters
    ----------
    points : list
        List of points from the maximum intensity projection.
    altitude_map : timagetk.SpatialImage
        Altitude of maximum intensity projection.

    Returns
    -------
    list
        List of points in the real world

    Examples
    --------
    >>> from timagetk.algorithms.reconstruction import surface2im
    >>> from timagetk.algorithms.reconstruction import altitude_map
    >>> from timagetk.io.dataset import shared_data
    >>> ref_img = shared_data('flower_confocal', 0)
    >>> alti = altitude_map(ref_img, threshold=45, orientation=1)
    >>> xyz = surface2im([[50, 50], [200, 200]], alti, ref_img.get_voxelsize('z'))
    >>> print(xyz)
    [[20.03200054168701, 20.03200054168701, 63.7017617225647], [80.12800216674805, 80.12800216674805, 19.23072052001953]]

    """
    assert_spatial_image(altitude_map, obj_name='altitude')
    vx, vy = altitude_map.voxelsize[::-1]
    coord = []
    for pt in points:
        x, y = pt
        coord.append([x * vx, y * vy, altitude_map[y, x] * vz])
    return coord


@singlechannel_wrapper
def altitude_map(image, gaussian_sigma=1., height=3., threshold=None, orientation=None, **kwargs):
    """Compute a surface projection image and associated altitude map.

    Height values follows the slices indexing, so ``0`` for the first slice (hence the importance of ``orientation``).
    They range in ``[0, axis_shape - 1]`` with ``axis_shape`` being the size of the Z projected axis.
    They are thus in **voxel unit** and not in real units.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        The intensity image to project.
    gaussian_sigma : float, optional
        Sigma value, in real units, of the Gaussian filter applied to the image.
        Skip this step by setting the value to ``0`` or ``None``. Defaults to ``1``.
        This value is divided by the image voxelsize.
    height : float, optional
        Height parameter.
    threshold : int or float, optional
        Instensity value threshold to create the mask.
        By default, try to guess it with ``guess_intensity_threshold``.
    orientation: {-1, 1}, optional
        Defines the orientation of the projection, try to guess it by default.
        If ``-1`` we consider the z-axis starts at the bottom of the object and goes upward.
        If ``1`` we consider the z-axis starts at the top of the object and goes downward.

    Other Parameters
    ----------------
    axis : str
        The axis to use for projection, defaults to "Z".
    channel : str
        If a ``MultiChannelImage`` is used as input `image`, select the channel to use with this algorithm.
    altimap_smoothing : bool
        If ``True``, default is ``False``, smooth the altitude map obtained from the mask.

    Returns
    -------
    timagetk.SpatialImage
        YX 2D altitude map, height values starting from the first slice defined by the image orientation.

    See Also
    --------
    timagetk.array_util.guess_intensity_threshold
    timagetk.array_util.guess_image_orientation

    Examples
    --------
    >>> from timagetk.algorithms.reconstruction import altitude_map
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> img = shared_data('flower_confocal', 0)
    >>> # EXAMPLE #1 - Get z-altitude map:
    >>> alti = altitude_map(img, threshold=45, orientation=1)
    >>> fig = grayscale_imshow(alti, title="Altitude map", cmap='viridis', val_range='auto')
    >>> # EXAMPLE #2 - Smooth the returned z-altitude map with Gaussian filter:
    >>> alti = altitude_map(img, threshold=45, orientation=1, altimap_smoothing=True)
    >>> fig = grayscale_imshow(alti, title="Altitude map", cmap='viridis', val_range='auto')
    >>> # EXAMPLE #3 - Get y-altitude map:
    >>> alti = altitude_map(img, threshold=45, orientation=1, axis="y")
    >>> fig = grayscale_imshow(alti, title="Altitude map", cmap='viridis', val_range='auto')
    >>> # EXAMPLE #4 - Get -y-altitude map:
    >>> alti = altitude_map(img, threshold=45, orientation=-1, axis="y")
    >>> fig = grayscale_imshow(alti, title="Altitude map", cmap='viridis', val_range='auto')

    """
    proj_axis = kwargs.get('axis', 'z').upper()
    # Get the shape of the projected axis, this is the max altitude value!
    proj_axis_shape = image.get_shape(proj_axis)

    img_slice = image.get_slice(0, axis=proj_axis)  # easy way to obtain origin, voxelsize & metadata attributes
    attr = get_image_attributes(img_slice)
    # Change the returned dtype according to max altitude value:
    if proj_axis_shape > 256:
        attr['dtype'] = 'uint16'
    else:
        attr['dtype'] = 'uint8'

    # Re-order axes to get the projected axis in first position
    axes_order = proj_axis + image.axes_order.replace(proj_axis, '')
    if axes_order != image.axes_order:
        image = image.transpose(axes_order)
    # Check the orientation is correctly defined:
    try:
        assert orientation in [None, -1, 1]
    except AssertionError:
        raise ValueError(f"Valid values for 'orientation' are 'None', '-1', or '1', got '{orientation}'!")

    # -- Try to guess the intensity threshold value if none given:
    if threshold is None:
        threshold = guess_intensity_threshold(image)

    # -- Try to guess the image orientation if none given:
    if orientation is None:
        orientation = guess_image_orientation(image)

    # -- Smooth the image prior to thresholding to get smoother mask
    if gaussian_sigma is not None and gaussian_sigma != 0:
        smooth_img = nd.gaussian_filter(image.get_array(), gaussian_sigma / np.array(image.voxelsize))
    else:
        smooth_img = image.get_array()

    # -- Create the binary mask image from the intensity image:
    # - Mask image by intensity threshold
    mask_img = (smooth_img > threshold)
    del smooth_img
    # - Fill holes in mask image to get a solid object
    mask_img = nd.binary_fill_holes(mask_img)
    # - Erode the mask to see signal (and not end up just above it)
    erosion_iterations = int(height / image.get_voxelsize(proj_axis))
    mask_img = nd.binary_erosion(mask_img, iterations=erosion_iterations, border_value=0)

    # -- Create the Altitude Map depending on orientation:
    sl_arr = img_slice.get_array(dtype='int16')
    if orientation == -1:
        alti = proj_axis_shape * np.ones_like(sl_arr)
        for i in range(proj_axis_shape)[::-1]:
            alti[(mask_img[i, :, :] > 0) & (alti > (proj_axis_shape - 1))] = i
        f_mask = alti == proj_axis_shape
        alti[f_mask] = 0
    else:
        alti = -np.ones_like(sl_arr).astype(np.int16)
        for i in range(proj_axis_shape):
            alti[(mask_img[i, :, :] > 0) & (alti < 0)] = i
        f_mask = alti == -1
        alti[f_mask] = proj_axis_shape - 1

    # -- Gaussian smoothing of the altimap, if required:
    if kwargs.get('altimap_smoothing', False):
        if gaussian_sigma is not None and gaussian_sigma != 0:
            sigma = gaussian_sigma / min(image.get_voxelsize())
        else:
            sigma = 1
        # Apply the gaussian filter to a limited domain:
        # See: https://stackoverflow.com/questions/59685140/python-perform-blur-only-within-a-mask-of-image

        inv_f_mask = np.array(~f_mask).astype(float)
        f_alti = nd.gaussian_filter(alti * inv_f_mask, sigma=sigma)
        weights = nd.gaussian_filter(inv_f_mask, sigma=sigma)
        f_alti /= weights + 0.000001
        f_alti *= inv_f_mask
        if orientation == -1:
            f_alti[f_mask] = 0
        else:
            f_alti[f_mask] = proj_axis_shape - 1
        alti = f_alti
    # -- Re-create the SpatialImage instance to return
    return SpatialImage(alti, **attr)


def _contour_from_segmentation(seg_image, background_id, connectivity=1, iterations=1):
    """Hidden method to get the contour image, that is the background outline.

    Parameters
    ----------
    seg_image : timagetk.LabelledImage or timagetk.TissueImage3D
        The segmented image to use to detect the background position.
    background_id : int
        The label associated to the background.
    connectivity : {1, 2, 3}, optional
        The connectivity of the binary structuring element. Defaults to ``1``.
    iterations : int, optional
        The number of iterations of binary dilation by the structuring element. Defaults to ``1``.

    Returns
    -------
    numpy.array
        The array with the background outline.

    """
    # -- Get the background outward outline:
    bkgd_im = seg_image == background_id
    struct = nd.generate_binary_structure(3, connectivity)
    contour_im = nd.binary_dilation(bkgd_im, structure=struct, iterations=iterations) ^ bkgd_im
    return contour_im


def contour_from_segmentation(seg_image, background_id=1, orientation=None, **kwargs):
    """Get the contour image from the labelled image, that is the background outline.

    Parameters
    ----------
    seg_image : timagetk.LabelledImage or timagetk.TissueImage3D
        The labelled image to use to detect the background position.
    background_id : int, optional
        The label associated to the background, defaults to ``1``.
        If a ``TissueImage3D`` instance is given as `seg_image`, it will use its ``background`` attribute.
    orientation : {None, -1, 1}, optional
        Defines the orientation of the projection.
        By default (``None``), try to guess it with ``guess_image_orientation``.
        If `-1` we consider the z-axis starts at the bottom of the object and goes upward.
        If `1` we consider the z-axis starts at the top of the object and goes downward.

    Other Parameters
    ----------------
    iso_upsampling : bool
        If ``True``, up-sample the image to the minimum voxelsize.
        This is usefull if you have an axis with a lower resolution, typically the z-axis.
    interpolation : {'nearest', 'cellbased'}
        Interpolation method to use during `iso_upsampling`, if any. Defaults to 'cellbased'.
    cell_based_sigma : float
        Required when using "cellbased" interpolation method, sigma for cell-based interpolation. In voxel units!
    connectivity : {1, 2, 3}
        The connectivity of the binary structuring element. Defaults to ``1``.
    iterations : int
        The number of iterations of binary dilation by the structuring element. Defaults to ``1``.

    Returns
    -------
    timagetk.SpatialImage
        Contour image, that is the background outline.

    See Also
    --------
    scipy.ndimage.generate_binary_structure
    scipy.ndimage.binary_dilation

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.algorithms.reconstruction import contour_from_segmentation
    >>> from timagetk import LabelledImage
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.stack import stack_browser
    >>> # - Get 'p58-t1' shared segmented image:
    >>> seg_image = shared_data('flower_labelled', 1)
    >>> # - Get the contour:
    >>> contour = contour_from_segmentation(seg_image, orientation=1, connectivity=1, iterations=2)
    >>> b = stack_browser(contour, title="Contour map", cmap='gray', val_range=[0, 1])
    >>> # - Get the labelled contour:
    >>> labelled_contour = LabelledImage(seg_image * contour)
    >>> b = stack_browser(labelled_contour, title="Labelled contour map", cmap='glasbey', val_range='auto', colorbar=True)

    """
    attr = get_image_attributes(seg_image, exclude=["not_a_label"], extra=['filename'])

    # -- Assert the validity of the `orientation` parameter:
    try:
        assert orientation in [None, -1, 1]
    except AssertionError:
        raise ValueError(f"Valid values for 'orientation' are 'None', '-1', or '1', got '{orientation}'!")
    # -- If a TissueImage3D, compare given `background_id` to its `background` attribute
    if isinstance(seg_image, TissueImage3D) and seg_image.background != background_id:
        log.warning(
            f"Given `background_id` ({background_id}) is different from the `background` attribute of the TissueImage3D ({seg_image.background})!")
        log.info("Replaced `background_id` by the `background` attribute.")
        background_id = seg_image.background
    # -- Assert the validity of the `background_id` parameter:
    try:
        assert background_id in seg_image
    except AssertionError:
        log.warning(f"The provided background id ({background_id}) is NOT in the segmented image!")
        return None

    # -- Invert the z-axis if required:
    if orientation == -1:
        seg_image.invert_axis('z')

    # -- Performs isometric up-sampling if required:
    if kwargs.pop('iso_upsampling', False):
        seg_image = isometric_resampling(seg_image, value='min', interpolation=kwargs.pop('interpolation', 'cellbased'),
                                         cell_based_sigma=kwargs.pop('cell_based_sigma', 1))

    # -- Get the background outline:
    contour_im = _contour_from_segmentation(seg_image, background_id,
                                            kwargs.get('connectivity', 1), kwargs.get('iterations', 1))

    # -- Re-create the SpatialImage instance to return
    return SpatialImage(contour_im, **attr)


def altimap_from_segmentation(seg_image, axis='z', background_id=1, orientation=None, **kwargs):
    """Get an altitude map from a segmentation, using the background position.

    Height values follows the slices indexing, so ``0`` for the first slice (hence the importance of ``orientation``).
    They range in ``[0, axis_shape - 1]`` with ``axis_shape`` being the size of the Z projected axis.
    They are thus in **voxel unit** and not in real units.

    Parameters
    ----------
    seg_image : timagetk.LabelledImage or timagetk.TissueImage3D
        The segmented image to use to detect the background position.
    axis : {'x', 'y', 'z'}, optional
        The axis to use for projection, default to ``z``.
    background_id : int, optional
        The label associated to the background, defaults to ``1``.
        If a ``TissueImage3D`` instance is given as `seg_image`, it will use its ``background`` attribute.
    orientation : {None, -1, 1}, optional
        Defines the orientation of the projection.
        By default (``None``), try to guess it with ``guess_image_orientation``.
        If `-1` we consider the z-axis starts at the bottom of the object and goes upward.
        If `1` we consider the z-axis starts at the top of the object and goes downward.

    Other Parameters
    ----------------
    iso_upsampling : bool
        If ``True``, up-sample the image to the minimum voxelsize.
        This is usefull if you have an axis with a lower resolution, typically the z-axis.
    interpolation : {'nearest', 'cellbased'}
        Interpolation method to use during `iso_upsampling`, if any. Defaults to 'cellbased'.
    cell_based_sigma : float
        Required when using "cellbased" interpolation method, sigma for cell-based interpolation. In voxel units!
    altimap_smoothing : bool
        If ``True``, default is ``False``, smooth the altitude map obtained from the mask.
    gaussian_sigma : float, optional
        Sigma value, in real units, of the Gaussian filter applied to the altutide image.
        This value is divided by the image voxelsize.
    connectivity : {1, 2, 3}
        The connectivity of the binary structuring element.
        Defaults to ``1``.
    iterations : int
        The number of iterations of binary dilation by the structuring element.
        Defaults to ``1``.

    Returns
    -------
    timagetk.SpatialImage
        YX 2D altitude map, height values starting from the first slice defined by the image orientation.

    See Also
    --------
    timagetk.algorithms.reconstruction.contour_from_segmentation

    Examples
    --------
    >>> from timagetk.algorithms.reconstruction import altimap_from_segmentation
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> # - Get 'p58-t0' shared segmented image:
    >>> seg_image = shared_data('flower_labelled', 0)
    >>> alti = altimap_from_segmentation(seg_image, altimap_smoothing=True)
    >>> fig = grayscale_imshow(alti, title="Altitude map / Z-axis", cmap='viridis', val_range='auto')
    >>> alti = altimap_from_segmentation(seg_image, axis='x', altimap_smoothing=False)
    >>> fig = grayscale_imshow(alti, title="Altitude map / X-axis", cmap='viridis', val_range='auto')
    >>> alti = altimap_from_segmentation(seg_image, axis='y', altimap_smoothing=False)
    >>> fig = grayscale_imshow(alti, title="Altitude map / Y-axis", cmap='viridis', val_range='auto')

    """
    # -- Assert we have a 3D image:
    try:
        assert seg_image.is3D()
    except AssertionError:
        log.error("Surface projection can only be applied to 3D images!")
        return None
    # -- Assert the validity of the `axis` parameter:
    try:
        assert axis.lower() in ["z", "y", "x"]
    except AssertionError:
        raise ValueError(f"Valid values for 'axis' are ['z', 'y', 'x'], got '{axis}'!")
    # -- Assert the validity of the `orientation` parameter:
    try:
        assert orientation in [None, -1, 1]
    except AssertionError:
        raise ValueError(f"Valid values for 'orientation' are 'None', '-1', or '1', got '{orientation}'!")
    # -- If a TissueImage3D, compare given `background_id` to its `background` attribute
    if isinstance(seg_image, TissueImage3D) and seg_image.background != background_id:
        log.warning(
            f"Given `background_id` ({background_id}) is different from the `background` attribute of the TissueImage3D ({seg_image.background})!")
        log.info("Replaced `background_id` by the `background` attribute.")
        background_id = seg_image.background
    # -- Assert the validity of the `background_id` parameter:
    try:
        assert background_id in seg_image
    except AssertionError:
        log.warning(f"The provided background id ({background_id}) is NOT in the segmented image!")
        return None

    # -- Invert the z-axis if required:
    if orientation == -1:
        seg_image.invert_axis('z')

    # -- Performs isometric up-sampling if required:
    if kwargs.pop('iso_upsampling', False):
        seg_image = isometric_resampling(seg_image, value='min', interpolation=kwargs.pop('interpolation', 'cellbased'),
                                         cell_based_sigma=kwargs.pop('cell_based_sigma', 1))

    # -- Get the background outline:
    contour_im = _contour_from_segmentation(seg_image, background_id,
                                            kwargs.get('connectivity', 1), kwargs.get('iterations', 1))

    max_height = seg_image.get_shape(axis)
    img_slice = seg_image.get_slice(0, axis)  # easy way to obtain origin, voxel-size & metadata attributes
    # -- Initialize the altimap to max height:
    alti = np.ones([seg_image.get_shape(ax.lower()) for ax, _ in img_slice.axes_order_dict.items()]) * max_height

    if axis.lower() == 'z':
        # -- Get the altitude as the lowest point for all outline points:
        for z, y, x in np.array(np.where(contour_im == 1)).T:
            alti[y, x] = min([alti[y, x], z])
    elif axis.lower() == "y":
        # -- Get the altitude as the lowest point for all outline points:
        for z, y, x in np.array(np.where(contour_im == 1)).T:
            alti[z, -x] = min([alti[z, -x], y])
    else:
        # -- Get the altitude as the lowest point for all outline points:
        for z, y, x in np.array(np.where(contour_im == 1)).T:
            alti[z, y] = min([alti[z, y], x])

    # -- Gaussian smoothing of the altimap, if required:
    sigma = kwargs.get('sigma', None)
    if kwargs.get('altimap_smoothing', False):
        if sigma is not None and sigma != 0:
            ax = list(img_slice.axes_order_dict.keys())[0]
            sigma = sigma / seg_image.get_voxelsize(ax)
        else:
            sigma = 1
        alti = nd.gaussian_filter(alti.astype(float), sigma=sigma, mode='nearest')

    # -- Re-create the SpatialImage instance to return
    attr = get_image_attributes(img_slice)
    return SpatialImage(alti, **attr)


def altimap_surface_projection(image, altimap, axis='z', offset=1, **kwargs):
    """Return the surface from the altitude map with image values.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.LabelledImage
        The image to extract values from (at given altitude).
    altimap : timagetk.SpatialImage
        The altitude map at witch image values should be taken from.
    axis : {'x', 'y', 'z'}, optional
        The axis to use for projection, default to ``z``.
        Should be the same as for the computation of the altimap.
    offset : int, optional
        The offset to apply to the `altimap`, defaults to ``1``.

    Returns
    -------
    numpy.ndarray
        The 2D image at given altitude.

    Examples
    --------
    >>> from timagetk.algorithms.reconstruction import altimap_surface_projection
    >>> from timagetk.algorithms.reconstruction import altimap_from_segmentation
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> int_image = shared_data("flower_confocal", 1)  # get 'p58-t1' shared intensity image
    >>> seg_image = shared_data('flower_labelled', 1)  # get 'p58-t1' shared labelled image
    >>> # Example #1 - Project the labelled image along the Z-axis:
    >>> alti = altimap_from_segmentation(seg_image)
    >>> proj = altimap_surface_projection(seg_image, alti)
    >>> f = fig = grayscale_imshow([alti, proj], suptitle='Z-axis projection', title=["Altitude map", "Contour map"], cmap=['viridis', 'glasbey'], val_range='auto')

    >>> # Example #2 - Project the intensity image along the Z-axis:
    >>> alti = altimap_from_segmentation(seg_image)
    >>> proj = altimap_surface_projection(int_image, alti)
    >>> f = fig = grayscale_imshow([alti, proj], suptitle='Z-axis projection', title=["Altitude map", "Intensity contour map"], cmap=['viridis', 'gray'], val_range=['auto', 'dtype'])

    >>> # Example #3 - Project along the X-axis:
    >>> alti = altimap_from_segmentation(seg_image, axis='x')
    >>> proj = altimap_surface_projection(int_image, alti, axis='x')
    >>> f = fig = grayscale_imshow([alti, proj], suptitle='X-axis projection', title=["Altitude map", "Intensity contour map"], cmap=['viridis', 'gray'], val_range=['auto', 'dtype'])

    >>> # Example #4 - Project along the Y-axis:
    >>> alti = altimap_from_segmentation(seg_image, axis='y')
    >>> proj = altimap_surface_projection(int_image, alti, axis='y')
    >>> f = fig = grayscale_imshow([alti, proj], suptitle='Y-axis projection', title=["Altitude map", "Intensity contour map"], cmap=['viridis', 'gray'], val_range=['auto', 'dtype'])

    >>> # Example #5 - Project the labelled image along the Y-axis:
    >>> alti = altimap_from_segmentation(seg_image, axis='y')
    >>> proj = altimap_surface_projection(seg_image, alti, axis='y')
    >>> f = fig = grayscale_imshow([alti, proj], suptitle='Y-axis projection', title=["Altitude map", "Contour map"], cmap=['viridis', 'glasbey'], val_range='auto')

    """
    from timagetk.components.image import get_image_class

    # -- Assert we have a 3D image:
    try:
        assert image.is3D()
    except AssertionError:
        log.error("Surface projection can only be applied to 3D images!")
        return None

    # Get the TimageTK class of image to return
    Image = get_image_class(image)
    # Use the axis slice to get the image attributes to return
    sl_img = image.get_slice(0, axis=axis)
    attr = get_image_attributes(sl_img)
    axo_dict = sl_img.axes_order_dict

    # Apply an offset to the altitude map, if any
    if isinstance(offset, int) and offset != 0:
        altimap += offset

    # Create the first and second axis coordinate grids
    ax0, ax1 = np.meshgrid(*[np.arange(image.get_shape(ax)) for ax in axo_dict.keys()], indexing='ij')
    coords = np.array([altimap.get_array(), ax0, ax1])  # [axis-alti, axis0, axis1] 2D arrays
    # Convert it to an array of 3D coordinates of shape: [axis0.size, axis1.size, 3]
    coords = np.transpose(coords, (1, 2, 0))

    # Get the axes limits:
    if axis.lower() == "z":
        axes_limit = np.array([image.get_shape(ax) for ax in 'ZYX']) - 1
    elif axis.lower() == "y":
        axes_limit = np.array([image.get_shape(ax) for ax in 'YZX']) - 1
    else:
        axes_limit = np.array([image.get_shape(ax) for ax in 'XZY']) - 1

    # Limit the `(axis-alti, axis0, axis1)` coordinates to their min and max values
    coords = np.maximum(np.minimum(coords, axes_limit), 0)
    coords[np.isnan(coords)] = 0
    # Make sure type is integers:
    coords = coords.astype(int)

    # Transform into a 2D array of 3D coordinates
    coords = np.concatenate(coords)  # shape is now (axis0.size * axis1.size, 3)
    # Transform into a tuple of 3 1D-arrays of axis-coordinate
    coords = tuple(np.transpose(coords))

    # Re-oder axis-coordinate to match ZYX ordered intensity image
    if axis.lower() == "z":
        coords = (coords[0], coords[1], coords[2])  # already ZYX coordinates
    elif axis.lower() == "y":
        coords = (coords[1], coords[0], coords[2])  # YZX coordinates => ZYX coordinates
    else:
        coords = (coords[1], coords[2], coords[0])  # XZY coordinates => ZYX coordinates

    # Create intensity projection by getting intensity values from determined coordinates
    proj = image.get_array()[coords].reshape(np.array(altimap.shape))

    return Image(proj, **attr)


@singlechannel_wrapper
def image_surface_projection(image, gaussian_sigma=1., height=3., threshold=None, orientation=None, **kwargs):
    """Compute a surface projection image and associated altitude map.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        The intensity image to project.
    gaussian_sigma : float, optional
        Sigma value, in real units, of the Gaussian filter applied to the image.
        Skip this step by setting the value to ``0`` or ``None``. Defaults to ``1``.
        This value is divided by the image voxelsize.
    height : float, optional
        Height parameter.
    threshold : int or float, optional
        Intensity value threshold to create the mask.
        By default, try to guess it with ``guess_intensity_threshold``.
    orientation: {None, -1, 1}, optional
        Defines the orientation of the projection.
        By default (``None``), try to guess it with ``guess_image_orientation``.
        If `-1` we consider the z-axis starts at the bottom of the object and goes upward.
        If `1` we consider the z-axis starts at the top of the object and goes downward.

    Other Parameters
    ----------------
    axis : str
        The axis to use for projection, defaults to "Z".
    iso_upsampling : bool
        If ``True`` (default), up-sample the image to the minimum voxelsize.
        This is usefull if you have an axis with a lower resolution, typically the z-axis.
    interpolation : {'linear', 'cspline'}
        Interpolation method to use during `iso_upsampling`, if any. Defaults to 'linear'.
    channel : str
        If a ``MultiChannelImage`` is used as input `image`, select the channel to use with this algorithm.
    altimap_smoothing : bool
        If ``True``, default is ``False``, smooth the altitude map obtained from the mask.

    See Also
    --------
    timagetk.array_util.guess_intensity_threshold
    timagetk.array_util.guess_image_orientation

    Returns
    -------
    timagetk.SpatialImage
        YX 2D intensity projection image.
    timagetk.SpatialImage
        YX 2D altitude map, height values starting from the first slice defined by the image orientation.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.algorithms.reconstruction import image_surface_projection
    >>> from timagetk import SpatialImage
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> # - Get 'p58' shared intensity image:
    >>> img = shared_data('flower_confocal', 0)
    >>> # -- EXAMPLE #1 - Use automatic thresholding
    >>> proj, alti = image_surface_projection(img, orientation=1)
    >>> fig = grayscale_imshow([proj, alti], title=["Intensity projection map", "Altitude map"], cmap=['gray', 'viridis'], val_range=['type', 'auto'])
    >>> # -- EXAMPLE #2 - Use 60-th percentile as threshold
    >>> threshold = np.percentile(img, 60)
    >>> print(f"60-th percentile intensity threshold: {threshold}")
    >>> proj, alti = image_surface_projection(img, threshold=threshold, orientation=1)
    >>> fig = grayscale_imshow([proj, alti], title=["Intensity projection map", "Altitude map"], cmap=['gray', 'viridis'], val_range=['type', 'auto'])
    >>> # -- EXAMPLE #3 - Project along x-axis
    >>> proj, alti = image_surface_projection(img, orientation=1, axis='x')
    >>> fig = grayscale_imshow([proj, alti], suptitle="X-axis projection", title=["Intensity projection map", "Altitude map"], cmap=['gray', 'viridis'], val_range=['type', 'auto'])
    >>> # -- EXAMPLE #4 - Project along y-axis
    >>> proj, alti = image_surface_projection(img, orientation=1, axis='y')
    >>> fig = grayscale_imshow([proj, alti], suptitle="Y-axis projection", title=["Intensity projection map", "Altitude map"], cmap=['gray', 'viridis'], val_range=['type', 'auto'])
    >>> # -- EXAMPLE #5 - Project along -y-axis
    >>> y_shape = img.get_shape('y')
    >>> threshold = np.percentile(img[:, y_shape//2: y_shape-1, :], 70)  # brighter on this side, so higher threshold
    >>> proj, alti = image_surface_projection(img, threshold=threshold, orientation=-1, axis='y')
    >>> fig = grayscale_imshow([proj, alti], suptitle="Y-axis projection", title=["Intensity projection map", "Altitude map"], cmap=['gray', 'viridis'], val_range=['type', 'auto'])

    """
    if kwargs.pop('iso_upsampling', True):
        image = isometric_resampling(image, value='min', interpolation=kwargs.pop('interpolation', 'linear'))

    altimap = altitude_map(image, gaussian_sigma=gaussian_sigma, height=height, threshold=threshold,
                           orientation=orientation, **kwargs)

    # Create the corresponding Intensity Projection Map
    surface_img = altimap_surface_projection(image, altimap, **kwargs)

    return surface_img, altimap


def project_segmentation(seg_img, axis='z', orientation=1, background=1, return_background=True):
    """Project a segmented image for given axis and direction.

    This method consider the background as transparent and mimic a surfacic projection.

    Parameters
    ----------
    seg_img : timagetk.LabelledImage
        The segmented image to project.
    axis : {'x', 'y', 'z'}, optional
        The axis to use for projection, default to ``z``.
    orientation : {1, -1}, optional
        Direction in which to move along the selected axis, ``1`` by default
        ``1`` follow increasing values along selected axis.
        ``-1`` follow decreasing values along selected axis.
    background : int, optional
        The label value associated to the background, ``1`` by default
    return_background : bool, optional
        If ``True`` (default), add the background in the returned projection, else leave it to ``0``

    Returns
    -------
    timagetk.LabelledImage
        The projected segmentation along given axis & direction

    Notes
    -----
    The returned ``LabelledImage`` declare ``0`` as ``not_a_label``.
    Voxel-sizes, origin and metadata are kept from original segmented image.

    Examples
    --------
    >>> from timagetk.algorithms.reconstruction import project_segmentation
    >>> from timagetk import TissueImage3D
    >>> from timagetk.io.dataset import shared_data
    >>> # Get the 'sphere' example labelled image:
    >>> seg_img = TissueImage3D(shared_data('flower_labelled', 0), not_a_label=0, background=1)
    >>> # Create a 'top-view' z-projection:
    >>> z_proj = project_segmentation(seg_img)
    >>> from timagetk.visu.util import greedy_colormap
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> fig = grayscale_imshow(z_proj, cmap=greedy_colormap(z_proj), title="Segmentation projection map")
    >>> # Get a realistic labelled image 'p58'
    >>> seg_img = TissueImage3D(shared_data('flower_labelled', 0), not_a_label=0, background=1)
    >>> x_projs = [project_segmentation(seg_img,'x',1), project_segmentation(seg_img,'x',-1)]
    >>> y_projs = [project_segmentation(seg_img,'y',1), project_segmentation(seg_img,'y',-1)]
    >>> fig = grayscale_imshow([z_proj] + x_projs + y_projs, suptitle="Segmentation projection map", title=["z-proj", "x-proj", "-x-proj", "y-proj", "-y-proj"], cmap='glasbey', val_range='auto', max_per_line=5)

    """
    if orientation not in (1, -1):
        raise ValueError(f"Valid values for `direction` argument are `1` or `-1`, got '{orientation}'!")

    slices_range = list(range(seg_img.get_shape(axis)))
    if orientation == -1:
        slices_range = slices_range[::-1]

    axis_index = seg_img.axes_order.index(axis.capitalize())
    axes = list(range(seg_img.ndim))
    axes.remove(axis_index)
    transpose_order = (axis_index, ) + tuple(axes)
    seg_array = np.transpose(seg_img.get_array(), transpose_order)

    # Initialize empty projection image as first slice along selected axis
    proj = np.zeros_like(seg_img.get_slice(0, axis).get_array())
    # Loop to add the label values to projection image with masks
    for sl_idx in tqdm(slices_range, unit='slice'):
        # Create a mask defining where the projection map should be updated
        mask = proj == 0  # where it's still equal to 0
        sl = seg_array[sl_idx] * mask # get the slice and "mask it"
        sl[sl == background] = 0  # set background to 0 to avoid adding it to existing map value
        proj += sl  # add this to the projection map

    if return_background:
        proj[proj == 0] = getattr(seg_img, "background", 1)  # remaining 0 should be background position

    img_slice = seg_img.get_slice(0, axis=axis)  # easy way to obtain required attributes in right dimensionality
    attr = get_image_attributes(img_slice)
    return LabelledImage(proj, **attr)
