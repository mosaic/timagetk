#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Linear Filtering Module

This module provides a set of linear filtering algorithms designed to process and enhance intensity or labelled images.
These filters are particularly useful for image preprocessing, feature extraction and analysis.

Key Features

- **Gaussian Filter**:
  Smoothens the image by applying a Gaussian blur, useful for noise reduction.
- **Gradient**:
  Computes the gradient magnitude of the image, highlighting edges.
- **Hessian**:
  Involves second-order derivatives to extract structural ridges and curvatures.
- **Laplacian**:
  Computes Laplacian of the image, often used for edge detection.
- **Zero-Crossing Methods**:
  Detect zero-crossings in the Hessian or Laplacian image to identify features like edges or contours.
- **Multi-Channel Support**:
  Supports operations on multi-channel images, allowing per-channel filtering.

Use this module to explore and transform your images using advanced filtering techniques.
"""

import scipy.ndimage as nd
from vt import linear_filter as vt_linearfilter

from timagetk.bin.logger import get_logger
from timagetk.components.spatial_image import SpatialImage
from timagetk.tasks.decorators import multichannel_wrapper
from timagetk.third_party.vt_parser import DEF_F_METHOD
from timagetk.third_party.vt_parser import FILTERING_METHODS
from timagetk.third_party.vt_parser import general_kwargs
from timagetk.third_party.vt_parser import linfilter_kwargs
from timagetk.third_party.vt_parser import parallel_kwargs

log = get_logger(__name__)


@multichannel_wrapper
def gaussian_filter(image, sigma=1.0, real=True, **kwargs):
    """Gaussian filter.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        Intensity image to filter.
    sigma : float or list[float], optional
        The sigma to apply for every axis if a float, for each axis if a list of floats.
    real : bool, optional
        Define if the `sigma` to apply is in voxel or real units (default).

    Other Parameters
    ----------------
    channel : str
        If a ``MultiChannelImage`` is used as input image, the algorithm will be applied only to this channel.
        Else it will be applied to all channels of the multichannel image.

    Returns
    -------
    timagetk.SpatialImage
        The filtered image.

    See Also
    --------
    scipy.ndimage.gaussian_filter

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.linearfilter import gaussian_filter
    >>> from timagetk.visu.mplt import grayscale_imshow

    >>> # EXAMPLE #1 - Apply linear filtering with default parameters:
    >>> img = shared_data("synthetic", 'nuclei')
    >>> # -- Gaussian smoothing, sigma=1.0 in real units
    >>> f_img = gaussian_filter(img, sigma=0.5)
    >>> # - Display linear filtering effect:
    >>> mid_z = img.get_shape('z') // 2
    >>> img_titles = ["Original", "Sigma=0.5µm"]
    >>> fig = grayscale_imshow([img, f_img], slice_id=mid_z, suptitle="Effect of Gaussian filtering", title=img_titles)

    """
    from timagetk.components.image import assert_spatial_image
    from timagetk.components.image import get_image_attributes
    # - Check if input image is indeed a ``SpatialImage`` & get its attributes:
    assert_spatial_image(image, obj_name='image')
    attr = get_image_attributes(image, extra=['filename'])

    if real:
        vxs = image.get_voxelsize()
        if isinstance(sigma, list):
            sigma = [s / vxs[i] for i, s in enumerate(sigma)]
        else:
            sigma = [s / vxs[i] for i, s in enumerate([sigma] * image.ndim)]
        log.debug(f"Converted sigma values for scipy.ndimage: {sigma}")

    return SpatialImage(nd.gaussian_filter(image, sigma), **attr)


@multichannel_wrapper
def linearfilter(image, method=DEF_F_METHOD, sigma=1.0, real=True, **kwargs):
    """Linear filtering algorithms.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        Intensity image(s) to filter.
    method : {"smoothing", "gradient", "hessian", "laplacian", "zero-crossings-hessian", "zero-crossings-laplacian", "gradient-hessian", "gradient-laplacian", "gradient-extrema"}, optional
        The method to use for linear filtering. Defaults to 'smoothing'.
    sigma : float or list(float), optional
        The sigma to apply for every axis if a float, for each axis if a list of floats.
    real : bool, optional
        Define if the `sigma` to apply is in voxel or real units (default).

    Other Parameters
    ----------------
    channel : str
        If a ``MultiChannelImage`` is used as input image, the algorithm will be applied only to this channel.
        Else it will be applied to all channels of the multichannel image.
    gaussian_type : {"deriche", "fidrich", "young-1995", "young-2002", "gabor-young-2002", "convolution"}
        Type of implementation to use for the gaussian methods.
        All except "convolution" are recursive filters.
        Method "convolution" is a convolution with a truncated gaussian mask.
    negative : bool
        If ``True`` zero-crossings are set on the negative values.
    positive : bool
        If ``True`` zero-crossings are set on the positive values, default when using a zero-crossing method.
    2D : bool
        If ``True``, performs 2D filtering of XY slices in a 3D volume.
    edges : bool
        If ``True``, edge-oriented normalization of first order derivative filters.
    params : str, optional
        CLI parameter string used by ``vt.linear_filter`` method.

    Returns
    -------
    timagetk.SpatialImage
        The filtered image.

    Raises
    ------
    TypeError
        If ``image`` is not a ``timagetk.SpatialImage``.
    ValueError
        If the image returned by ``vt.linear_filter`` is ``None``.

    See Also
    --------
    timagetk.third_party.vt_parser.FILTERING_METHODS
    timagetk.third_party.vt_parser.DEF_F_METHOD
    timagetk.third_party.vt_parser.GAUSSIAN_IMPLEMENTATION
    timagetk.third_party.vt_parser.DEF_GAUSS
    timagetk.third_party.vt_parser.linfilter_kwargs
    timagetk.third_party.vt_parser.general_kwargs
    timagetk.third_party.vt_parser.parallel_kwargs

    Notes
    -----
    Beware of `sigma` values as they are in real units by default.

    Changing to voxel may lead to erroneous result if the image is not isometric.
    An option is to provide a list of `sigma` values adapted to the voxel-size.

    Examples
    --------
    >>> from timagetk import SpatialImage
    >>> from timagetk import MultiChannelImage
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.linearfilter import linearfilter
    >>> from timagetk.visu.mplt import grayscale_imshow

    >>> # EXAMPLE #1 - Apply linear filtering with default parameters:
    >>> img = shared_data("synthetic", 'nuclei')
    >>> # -- Gaussian smoothing, sigma=1.0 in real units
    >>> out_img = linearfilter(img, sigma=0.5, param=True)
    >>> # - Display linear filtering effect:
    >>> mid_z = img.get_shape('z') // 2
    >>> img_titles = ["Original", "Gaussian, sigma=0.5µm"]
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> fig = grayscale_imshow([img, out_img], slice_id=mid_z, suptitle="Effect of linear filtering", title=img_titles, val_range=[0, 255])

    >>> # EXAMPLE #2 - Apply linear filtering with no-default parameters:
    >>> # -- Gaussian smoothing, sigma=2.0 in real units
    >>> out_img = linearfilter(img, method="smoothing", sigma=2., real=False)
    >>> # - Display linear filtering effect:
    >>> mid_z = img.get_shape('z') // 2
    >>> img_titles = ["Original", "Gaussian, sigma=2voxels"]
    >>> fig = grayscale_imshow([img, out_img], slice_id=mid_z, suptitle="Effect of linear filtering", title=img_titles, val_range=[0, 255])

    >>> # EXAMPLE #3 - Apply linear filtering on a multichannel image:
     >>> im_url = "https://zenodo.org/record/3737795/files/qDII-CLV3-PIN1-PI-E35-LD-SAM4.czi"
    >>> ch_names = ['DII-VENUS-N7', 'pPIN1:PIN1-GFP', 'Propidium Iodide', 'pRPS5a:TagBFP-SV40', 'pCLV3:mCherry-N7']
    >>> img = imread(im_url, channel_names=ch_names)
    >>> # Apply the algorithm to all channels (with same parameters):
    >>> out_img = linearfilter(img, method="smoothing", sigma=2., real=False)
    >>> isinstance(out_img, MultiChannelImage)
    True
    >>> # Apply the algorithm to a single channel and get the resulting SpatialImage:
    >>> out_img = linearfilter(img, channel='Propidium Iodide', method="smoothing", sigma=2., real=False)
    >>> isinstance(out_img, MultiChannelImage)
    False
    >>> isinstance(out_img, SpatialImage)
    True

    """
    from timagetk.components.image import assert_spatial_image
    from timagetk.components.image import get_image_attributes

    # - Check if input image is indeed a ``SpatialImage`` & get the attributes:
    assert_spatial_image(image, obj_name='image')
    attr = get_image_attributes(image, exclude=["dtype"], extra=['filename'])

    # - Take care of extra parameters given as a string:
    extra_params = kwargs.pop("params", "")
    if extra_params != "" and not extra_params.startswith(" "):
        extra_params = " " + extra_params

    # - Parse ``linearfilter`` parameters:
    params, kwargs = linfilter_kwargs(method, sigma=sigma, real=real, **kwargs)
    # - Parse general kwargs:
    params += general_kwargs(**kwargs)
    # - Parse parallelism kwargs:
    params += parallel_kwargs(**kwargs)
    # - Combine parsed parameters with extra parameters:
    params += extra_params
    log.debug(f"`vt.linear_filter` params: '{params}'")

    vt_img = image.to_vt_image()
    out_img = vt_linearfilter(vt_img, params)

    if out_img is None:
        null_msg = "VT method 'linear_filter' returned NULL image!"
        raise ValueError(null_msg)
    else:
        return SpatialImage(out_img, **attr)


def list_linear_methods():
    """List the available methods to call with ``linear_filtering``.

    Returns
    -------
    list[str]
        The list of available methods.

    """
    return FILTERING_METHODS
