#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Image Resampling Module

This module provides functionality for resampling intensity and labelled images, enabling operations
like voxel resizing, shape transformation, and isometric resampling.
It is especially helpful for processing large 3D images, ensuring they meet specific spatial resolutions
or memory requirements.

Key Features

- Resample images using target voxel sizes or shape dimensions.
- Support for multiple interpolation methods, including nearest, linear, cubic spline, and cell-based.
- Built-in utility functions to calculate optimal shapes or voxel sizes for memory optimization.
- Handle intensity images, labelled images, and multi-channel image formats.
- Isometric resampling for creating isotropic images with specified or automatically computed voxel sizes.

Usage Examples

>>> from timagetk.algorithms.resample import resample, new_shape_from_max
>>> from timagetk.io.dataset import shared_data
>>> img = shared_data('flower_confocal', 0)  # Load an example grayscale image
>>> print(f"Original shape: {img.shape}, voxel size: {img.voxelsize}")

>>> # Example 1: Resampling to a specific shape
>>> new_shape = new_shape_from_max(img.get_shape(), 200)
>>> resampled_img = resample(img, shape=new_shape, interpolation='linear')
>>> print(f"Resampled shape: {resampled_img.shape}")

>>> # Example 2: Resampling to a new voxel size
>>> resampled_img = resample(img, voxelsize=[0.4, 0.3, 0.2])
>>> print(f"Resampled voxel size: {resampled_img.voxelsize}")

>>> # Example 3: Isometric resampling
>>> from timagetk.algorithms.resample import isometric_resampling
>>> iso_img = isometric_resampling(img, value='max', interpolation='linear')
>>> print(f"Isometric shape: {iso_img.shape}, voxel size: {iso_img.voxelsize}")
"""

import numpy as np

from timagetk.algorithms.trsf import apply_trsf
from timagetk.algorithms.trsf import create_trsf
from timagetk.bin.logger import get_logger
from timagetk.tasks.decorators import multichannel_wrapper
from timagetk.util import dimensionality_test

log = get_logger(__name__)


@multichannel_wrapper
def resample(image, voxelsize=None, shape=None, interpolation=None, **kwargs):
    """Resample an image using given shape or voxel-size.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage or timagetk.LabelledImage
        The image to resample.
    voxelsize : list, optional
        The (Z)YX sorted voxel-size to use for image resampling.
    shape : list, optional
        The (Z)YX sorted shape to use for image resampling.
    interpolation : {"nearest", "linear", "cspline", "cellbased"}, optional
        Interpolation method to use. See the "Notes" section for detailled explanations.
        By default, try to guess it based on the type of input `image`.
        Use one of the ``GRAY_INTERPOLATION_METHODS`` with grayscale (intensity) images.
        Use one of the ``LABEL_INTERPOLATION_METHODS`` with labelled (segmented) images.

    Other Parameters
    ----------------
    channel : str
        If a ``MultiChannelImage`` is used as input image, the algorithm will be applied only to this channel.
        Else it will be applied to all channels of the multichannel image.
    cell_based_sigma : float
        Required when using "cellbased" interpolation method, sigma for cell-based interpolation. In voxel units!

    Returns
    -------
    timagetk.SpatialImage or timagetk.MultiChannelImage or timagetk.LabelledImage
        The resampled image.

    See Also
    --------
    timagetk.third_party.vt_parser.GRAY_INTERPOLATION_METHODS
    timagetk.third_party.vt_parser.LABEL_INTERPOLATION_METHODS
    timagetk.third_party.vt_parser.DEF_GRAY_INTERP_METHOD
    timagetk.third_party.vt_parser.DEF_LABEL_INTERP_METHOD
    timagetk.third_party.vt_parser.template_kwargs
    timagetk.third_party.vt_parser.interpolate_kwargs
    timagetk.third_party.vt_parser.general_kwargs
    timagetk.third_party.vt_parser.parallel_kwargs

    Notes
    -----
    Note that you have to provide either a target ``voxelsize`` or ``shape``.

    Interpolation methods definitions:

     - **nearest**: nearest neighbor interpolation (for binary or label images)
     - **linear**: bi- or tri-linear interpolation (for intensity images)
     - **cspline**: cubic spline (for intensity images)
     - **cellbased**: cell based interpolation (for label images)

    Using *linear* interpolation method might decrease the contrast compared to *cspline*.

    Using *cspline* interpolation method requires a larger amount of memory than the *linear* interpolation method.

    Use *nearest* interpolation with a binary image.

    Examples
    --------
    >>> from timagetk.algorithms.resample import resample
    >>> from timagetk.algorithms.template import iso_template
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> # Loading an example grayscale image and show info:
    >>> img = shared_data('flower_confocal', 0)
    >>> print(f"Input image ZYX shape: {img.shape}")
    Input image ZYX shape: (160, 230, 230)
    >>> print(f"Input image ZYX voxel-size: {img.voxelsize}")
    Input image ZYX voxel-size: [0.40064001083374023, 0.40064001083374023, 0.40064001083374023]

    >>> # EXAMPLE 1 - Resampling a grayscale image with 'shape':
    >>> from timagetk.algorithms.resample import new_shape_from_max
    >>> new_shape = new_shape_from_max(img.get_shape(), 200)
    >>> print(new_shape)
    [59, 256, 256]
    >>> out_img = resample(img, shape=new_shape, interpolation='linear')
    >>> print(f"Output image ZYX shape: {out_img.shape}")
    Output image ZYX shape: (59, 256, 256)
    >>> print(f"Output image ZYX voxel-size: {out_img.voxelsize}")
    Output image ZYX voxel-size: [1.0, 0.35994917154312134, 0.35994917154312134]
    >>> fig = grayscale_imshow([img, out_img], slice_id=20, title=['Original', 'XY rescaled'])

    >>> # EXAMPLE 2 - Resampling a grayscale image with 'voxelsize':
    >>> out_img = resample(img, voxelsize=[0.4, 0.3, 0.2])
    >>> print(f"Output image ZYX shape: {out_img.shape}")
    Output image ZYX shape: (148, 307, 461)
    >>> print(f"Output image ZYX voxel-size: {out_img.voxelsize}")
    Output image ZYX voxel-size: [1.0, 0.35994917154312134, 0.35994917154312134]

    >>> # EXAMPLE 3 - Resampling of a LabelledImage:
    >>> from timagetk import LabelledImage
    >>> img = shared_data('flower_labelled', 0)
    >>> print(img.get_voxelsize())
    [0.25, 0.25, 0.25]
    >>> print(img.get_shape())
    [225, 225, 225]
    >>> print(len(img.labels()))
    64
    >>> out_img = resample(img, voxelsize=[0.5, 0.5, 0.5])
    >>> print(out_img.get_voxelsize())
    [0.5, 0.5, 0.5]
    >>> print(out_img.get_shape())
    [113, 113, 113]
    >>> print(len(out_img.labels()))
    64
    >>> # EXAMPLE 4 - Effect of resampling method choice (linear vs. cspline) on grayscale image:
    >>> img = shared_data('flower_confocal', 0)
    >>> linear_img = resample(img, voxelsize=[0.5, 0.5, 0.5], interpolation='linear')
    >>> cspline_img = resample(img, voxelsize=[0.5, 0.5, 0.5], interpolation='cspline')
    >>> fig = grayscale_imshow([img, linear_img, cspline_img], slice_id=5, title=['Original', 'Linear', 'Cubic spline'], val_range=['type', 'type', 'type'])

    """
    ndim = image.ndim

    if shape is not None and voxelsize is None:
        dimensionality_test(ndim, shape)
        log.info(f"Resampling using provided shape: {shape}")
        params = f"-dimension {' '.join(list(map(str, shape[::-1])))} -resize"
    elif voxelsize is not None and shape is None:
        if isinstance(voxelsize, (float, int)):
            voxelsize = [voxelsize] * ndim
        else:
            dimensionality_test(ndim, voxelsize)
        log.info(f"Resampling using provided voxel-sizes: {voxelsize}")
        params = f"-voxel-size {' '.join(list(map(str, voxelsize[::-1])))} -resize"
    else:
        msg = "You have to provide either `voxelsize` OR `shape` as input!"
        raise ValueError(msg)

    # - Performs resampling using 'apply_trsf':
    out_img = apply_trsf(image, interpolation=interpolation, params=params, **kwargs)
    # add2md(img)
    return out_img


def new_shape_from_max(shape, max_dim, round_func=np.ceil, iso_transform=False):
    """Compute a new shape from a max dimension, only down-sizing the shape.

    Parameters
    ----------
    shape : list or tuple
        Current image shape.
    max_dim : int
        Max allowed dimension for the shape
    round_func : func, optional
        Function used to round the new shape (to integers).
    iso_transform : bool, optional
        If ``True``, apply the down-sizing equally to all dimensions.

    Returns
    -------
    numpy.ndarray
        New image shape.

    Examples
    --------
    >>> from timagetk.algorithms.resample import new_shape_from_max
    >>> from timagetk.io.dataset import shared_data
    >>> # Loading an example grayscale image and show info:
    >>> img = shared_data('flower_confocal', 0)
    >>> current_shape = img.get_shape()
    >>> print(current_shape)
    [160, 230, 230]
    >>> new_shape = new_shape_from_max(current_shape, 200)
    >>> print(new_shape)
    [160, 200, 200]
    >>> new_shape = new_shape_from_max(current_shape, 128)
    >>> print(new_shape)
    [128, 128, 128]
    >>> new_shape = new_shape_from_max(current_shape, 200, iso_transform=True)
    >>> print(new_shape)  # default "rounding function" is `np.ceil`
    [140, 201, 201]
    >>> new_shape = new_shape_from_max(current_shape, 128, iso_transform=True)
    >>> print(new_shape)
    [90, 128, 128]

    """
    maxi = np.max(shape)
    if maxi > max_dim:
        coef = maxi / float(max_dim)
        if iso_transform:
            new_shape = np.array(shape) / coef
            return np.array(list(map(int, map(round_func, new_shape))))
        else:
            new_shape = [min(sh, max_dim) for sh in shape]
            return np.array(list(map(int, map(round_func, new_shape))))
    else:
        return np.array(list(map(int, shape)))


def new_voxelsize_from_max_shape(image, max_dim, round_func=np.ceil, iso_transform=False):
    """Compute a new voxelsize from a max dimension, only down-sizing the shape.

    Parameters
    ----------
    image : SaptialImage
        Current image.
    max_dim : int
        Max allowed dimension for the shape
    round_func : func, optional
        Function used to round the new shape (to integers).
    iso_transform : bool, optional
        If ``True``, apply the down-sizing equally to all dimensions.

    Returns
    -------
    list
        New image shape as a list of integers.

    Examples
    --------
    >>> from timagetk.algorithms.resample import new_voxelsize_from_max_shape
    >>> from timagetk.io.dataset import shared_data
    >>> # Loading an example grayscale image and show info:
    >>> img = shared_data('flower_confocal', 0)
    >>> current_vxs = img.get_voxelsize()
    >>> print(current_vxs)
    [0.40064001083374023, 0.40064001083374023, 0.40064001083374023]
    >>> new_vxs = new_voxelsize_from_max_shape(img, 200)
    >>> print(new_vxs)
    [0.40064001 0.46073601 0.46073601]
    >>> new_vxs = new_voxelsize_from_max_shape(img, 128)
    >>> print(new_vxs)
    [0.50080001 0.71990002 0.71990002]
    >>> new_vxs = new_voxelsize_from_max_shape(img, 200, iso_transform=True)
    >>> print(new_vxs)  # default "rounding function" is `np.ceil`
    [0.4578743  0.45844379 0.45844379]
    >>> new_vxs = new_voxelsize_from_max_shape(img, 128, iso_transform=True)
    >>> print(new_vxs)
    [0.71224891 0.71990002 0.71990002]

    """
    current_shape = np.array(image.get_shape())
    current_vxs = np.array(image.get_voxelsize())
    new_shape = new_shape_from_max(current_shape, max_dim, round_func=round_func, iso_transform=iso_transform)
    return current_shape / np.array(new_shape) * current_vxs


def new_voxelsize_from_max_nbytes(image, max_nbytes, decimal=4):
    """Compute a new voxelsize from a max dimension, only down-sizing the shape.

    Parameters
    ----------
    image : SaptialImage
        Current image.
    max_dim : int
        Max allowed bytes size for the image.

    Returns
    -------
    list
        New image shape as a list of integers.

    Examples
    --------
    >>> from timagetk.algorithms.resample import new_voxelsize_from_max_nbytes
    >>> from timagetk.algorithms.resample import resample
    >>> from timagetk.io.dataset import shared_data
    >>> # Loading an example grayscale image and show info:
    >>> image = shared_data('flower_confocal', 0)
    >>> current_vxs = image.get_voxelsize()
    >>> print(current_vxs)
    [0.40064001083374023, 0.40064001083374023, 0.40064001083374023]
    >>> print(image.nbytes)
    8464000
    >>> new_vxs = new_voxelsize_from_max_nbytes(image, 846400)
    >>> print(new_vxs)
    [0.40064001, 0.46073601, 0.46073601]
    >>> new_img = resample(image, voxelsize=new_vxs)
    >>> print(new_img.get_shape())
    >>> print(new_img.get_voxelsize())
    >>> print(new_img.nbytes)

    """
    import copy as cp
    from timagetk.util import compute_shape
    current_shape = np.array(image.get_shape())
    original_vxs = np.array(image.get_voxelsize())
    current_extent = np.array(image.get_extent())
    dtype = image.dtype
    bs = np.zeros([1], dtype=dtype).nbytes

    n_iter = 0
    low_vxs = cp.deepcopy(original_vxs)
    high_vxs = current_shape
    prev_vxs = high_vxs

    def _f(vxs):
        return bs * np.prod(compute_shape(vxs, current_extent))

    eps = 3 * 1 / (10 ** decimal)
    while np.round(np.sum(high_vxs - low_vxs), decimal) > eps or _f(low_vxs) > max_nbytes:
        if n_iter >= 100:
            break
        n_iter += 1
        new_vxs = np.round((high_vxs + low_vxs) / 2, decimal)

        if np.sum(prev_vxs - new_vxs) == 0:
            low_vxs = new_vxs
            break
        else:
            prev_vxs = new_vxs

        if _f(new_vxs) == max_nbytes:
            low_vxs = new_vxs
            break
        elif _f(new_vxs) < max_nbytes:
            high_vxs = new_vxs
        else:
            low_vxs = new_vxs

    return low_vxs


#: List of valid isometric resampling methods.
ISO_RESAMPLE_METHODS = ['min', 'max']


@multichannel_wrapper
def isometric_resampling(image, value=None, interpolation=None, **kwargs):
    """Isometric resampling of a SpatialImage.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage or timagetk.LabelledImage
        The image to resample.
    value : {'min', 'max', float}
        Change voxel-size to 'min' or 'max' of original voxel-size or to a given value.
    interpolation : {"nearest", "linear", "cspline", "cellbased"}
        Interpolation method to use. See the "Notes" section for detailled explanations.
        By default, try to guess it based on the type of input `image`.
        Use one of the ``GRAY_INTERPOLATION_METHODS`` with grayscale (intensity) images.
        Use one of the ``LABEL_INTERPOLATION_METHODS`` with labelled (segmented) images.

    Other Parameters
    ----------------
    channel : str
        If a ``MultiChannelImage`` is used as input image, the algorithm will be applied only to this channel.
        Else it will be applied to all channels of the multichannel image.
    cell_based_sigma : float
        Required when using "cellbased" interpolation method, sigma for cell-based interpolation. In voxel units!

    Returns
    -------
    timagetk.SpatialImage or timagetk.MultiChannelImage or timagetk.LabelledImage
        The resampled image.

    See Also
    --------
    timagetk.third_party.vt_parser.GRAY_INTERPOLATION_METHODS
    timagetk.third_party.vt_parser.LABEL_INTERPOLATION_METHODS
    timagetk.third_party.vt_parser.DEF_GRAY_INTERP_METHOD
    timagetk.third_party.vt_parser.DEF_LABEL_INTERP_METHOD
    timagetk.third_party.vt_parser.apply_trsf_kwargs
    timagetk.third_party.vt_parser.general_kwargs
    timagetk.third_party.vt_parser.parallel_kwargs

    Notes
    -----
    Interpolation methods definitions:

     - **nearest**: nearest neighbor interpolation (for binary or label images)
     - **linear**: bi- or tri-linear interpolation (for intensity images)
     - **cspline**: cubic spline (for intensity images)
     - **cellbased**: cell based interpolation (for label images)

    Using *linear* interpolation method might decrease the contrast compared to *cspline*.

    Using *cspline* interpolation method requires a larger amount of memory than the *linear* interpolation method.

    Use *nearest* interpolation with a binary image.

    Examples
    --------
    >>> from timagetk.algorithms.resample import isometric_resampling
    >>> from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image
    >>> image = example_layered_sphere_wall_image(voxelsize=[1.0, 0.5, 0.5])
    >>> print(f"Input image ZYX shape: {image.shape}")
    Input image ZYX shape: (60, 120, 120)
    >>> print(f"Input image ZYX voxel-size: {image.voxelsize}")
    Input image ZYX voxel-size: [1.0, 0.5, 0.5]

    >>> # Example #1 - Compute the smallest isometric image w.r.t. voxel-size (smallest shape, maximal voxel-size)
    >>> iso_img = isometric_resampling(image, 'max', interpolation='linear')
    >>> print(f"Small isometric image ZYX shape: {iso_img.shape}")
    Small isometric image ZYX shape: (60, 60, 60)
    >>> print(f"Small isometric image ZYX voxel-size: {iso_img.voxelsize}")
    Small isometric image ZYX voxel-size: [1.0, 1.0, 1.0]

    >>> # Example #2 - Compute the largest isometric image w.r.t. voxel-size (biggest shape, minimal voxel-size)
    >>> iso_img = isometric_resampling(image, 'min', interpolation='linear')
    >>> print(f"Large isometric image ZYX shape: {iso_img.shape}")
    Large sometric image ZYX shape: (120, 120, 120)
    >>> print(f"Large isometric image ZYX voxel-size: {iso_img.voxelsize}")
    Large isometric image ZYX voxel-size: [0.5, 0.5, 0.5]

    >>> # Example #3 - Compute an isometric image to a given voxel-size:
    >>> iso_img = isometric_resampling(image, 0.75, interpolation='linear')
    >>> print(f"Isometric image ZYX shape: {iso_img.shape}")
    Isometric image ZYX shape: (80, 80, 80)
    >>> print(f"Isometric image ZYX voxel-size: {iso_img.voxelsize}")
    Isometric image ZYX voxel-size: [0.75, 0.75, 0.75]

    """
    method = kwargs.pop('method', None)  # Remove `method` from kwargs to avoid args problem in apply_trsf
    if method is not None and value is None:
        value = method
        log.warning(f"DEPRECATION NOTICE: use 'value' instead of 'method' as argument in 'isometric_resampling'!")

    if value not in ISO_RESAMPLE_METHODS and not isinstance(value, float):
        raise ValueError("Possible values for 'methods' are a float, 'min' or 'max'.")

    if isinstance(value, str):
        if value.startswith('min'):
            vxs = min(image.voxelsize)
        else:
            vxs = max(image.voxelsize)
    else:
        vxs = float(value)

    id_trsf = create_trsf('identity', template_img=image, trsf_type='rigid')
    return apply_trsf(image, trsf=id_trsf, interpolation=interpolation, params=f'-iso {vxs}', **kwargs)
