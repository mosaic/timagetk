#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Connected Components Detection and Labelling

This module provides functionality for detecting and labelling connected components in 2D or 3D images.
It is useful in various image processing workflows, such as segmenting objects, identifying regions,
and extracting meaningful structures from image data.

Main Functionality

- Detect connected components using configurable thresholds and connectivity settings.
- Supports input images with intensity, height, or labelled values, either single or multichannel.
- Offers several options such as filtering based on size, keeping the largest component, or binary outputs.
- The detected components are returned as a labelled image or optionally analyzed for additional metrics like size sorting.

Key Features

- Flexible thresholding options (`low_threshold`, `high_threshold`) to binarize and process images.
- Configurable connectivity (2D: 4- or 8-neighborhood; 3D: 6-, 10-, 18-, or 26-neighborhood).
- Ability to keep the largest components (`max`) or filter by minimum size (`min_size_cc`).
- Generate binary or labelled outputs with options for size ranking (`sort_sizes`) or uniform labeling (`binary_output`).

This module makes complex operations like connected component detection and labeling straightforward,
supporting a range of image processing applications.

**WARNING**: documentation of `vt-python` states to use `-label` as argument for labelling connected components,
but you should use `-labels`!
"""

import numpy as np
from vt import connexe as vt_connexe

from timagetk.bin.logger import get_logger
from timagetk.components.labelled_image import LabelledImage
from timagetk.tasks.decorators import singlechannel_wrapper
from timagetk.third_party.vt_converter import _new_from_vtimage
from timagetk.third_party.vt_parser import connexe_kwargs
from timagetk.third_party.vt_parser import general_kwargs
from timagetk.third_party.vt_parser import parallel_kwargs

log = get_logger(__name__)


@singlechannel_wrapper
def connected_components(image, **kwargs):
    """Connected components labeling.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage or timagetk.LabelledImage
        Intensity or height image to label by detecting connected components.

    Other Parameters
    ----------------
    channel : str
        If a ``MultiChannelImage`` is used as input image, select the channel to use with this algorithm.
    low_threshold : int or float
        Low threshold to binarize input image.
    high_threshold : int or float
        High threshold to binarize input image.
    max : bool
        If ``True`` keep the largest connected component (implies binary output).
        Else all detected components are kept. Equivalent to `max_number_cc=1`.
    min_size_cc : int
        Minimal size, in voxels, of the connected component.
        Otherwise, all detected components are kept by default.
    max_number_cc : int
        Maximal number of connected components to keep, the largest are kept first.
        Otherwise, all detected components are kept by default.
    connectivity : {4, 6, 8, 10, 18, 26}
        The connectivity of the structuring elements, ``DEF_CONNECTIVITY`` by default.
    binary_output : bool
        If ``True`` all valid connected components have the same value (1).
        Else each detected components have their own label (default).
    label : bool
        If ``True`` (default) returns one label per connected component.
    size : bool
        If ``True`` the value attributed to the points of a connected component is the size of the connected components
        This allows selecting w.r.t to size afterward.
    sort_sizes : bool
        If ``True`` the labels are ordered by decreasing size of connected components, implies ``label=True`` (unless changed afterwards).
    params : str, optional
        CLI parameter string used by ``vt.connexe`` method.

    Returns
    -------
    timagetk.LabelledImage
        Connected component image.

    Raises
    ------
    TypeError
        If ``image`` is not a ``SpatialImage`` or a ``timagetk.LabelledImage``.
    ValueError
        If the image returned by ``vt.connexe`` is ``None``.

    See Also
    --------
    timagetk.third_party.vt_parser.connexe_kwargs
    timagetk.third_party.vt_parser.general_kwargs
    timagetk.third_party.vt_parser.parallel_kwargs

    Notes
    -----
    Connectivity is among the 4-, 6-, 8-, 18-, 26-neighborhoods.
    4 and 8 are for 2D images, the others for 3D images.

    Example
    -------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.regionalext import regional_extrema
    >>> from timagetk.algorithms.connexe import connected_components
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> image = shared_data("flower_confocal", 0)

    >>> # - Example #1: Connected components labelling of threshold intensity image:
    >>> labeling = connected_components(image, low_threshold=60)
    >>> titles = [f"Original", "Connexe components labeling"]
    >>> fig = grayscale_imshow([image, labeling], slice_id=20, title=titles, val_range=['type', 'auto'], cmap=['gray', 'viridis'])

    >>> # - Example #2: Connected components labelling of local minima image:
    >>> zslice = 20
    >>> subimg = image.get_slice(zslice, 'z')
    >>> # - H-minima transformation for h-min threshold and 8-connexe pixels:
    >>> hmin = 45
    >>> extmin_img = regional_extrema(subimg, hmin, "minima", connectivity=8)
    >>> # - Connected component labelling after hysteresis thresholding:
    >>> seed_image = connected_components(extmin_img, connectivity=8, low_threshold=1, high_threshold=hmin)
    >>> images = [subimg, extmin_img, seed_image]
    >>> titles = [f"Original (z-slice: {zslice})", f"Local minima (height={hmin})", "Connexe components labelling"]
    >>> fig = grayscale_imshow(images, title=titles, val_range=['type', 'auto', 'auto'], cmap=['gray', 'viridis', 'glasbey'])

    >>> # - EXAMPLE #3: Background detection (assumed to be the biggest label)
    >>> zslice = 20
    >>> subimg = image.get_slice(zslice, 'z')
    >>> hmin = int(subimg.max() // 30)
    >>> extmin_img = regional_extrema(subimg, hmin, "minima")
    >>> bkgd_seed = connected_components(extmin_img, high_threshold=hmin, max=True)
    >>> bkgd_seed.labels()
    >>> fig = grayscale_imshow([subimg, bkgd_seed], val_range=['type', [0, 255]], cmap=['gray', 'viridis'])

    """
    from timagetk.components.image import assert_spatial_image
    from timagetk.components.image import get_image_attributes
    # - Check if input image is indeed a ``SpatialImage`` & get its attributes:
    assert_spatial_image(image, obj_name='image')
    attr = get_image_attributes(image, extra=['filename'])

    # If only component of max size is labelled, it is similar to a mask and is thus stored as uint8:
    if kwargs.get("max", False):
        attr['dtype'] = 'uint8'

    # - Take care of extra parameters given as a string:
    extra_params = kwargs.pop("params", "")
    if not extra_params.startswith(" "):
        extra_params = " " + extra_params
    # - Parse connexe parameters:
    if 'high_threshold' not in kwargs:
        kwargs['high_threshold'] = image.max()
    if 'low_threshold' not in kwargs:
        kwargs['low_threshold'] = 0
    params, kwargs = connexe_kwargs(ndim=image.ndim, **kwargs)
    # - Parse general kwargs:
    params += general_kwargs(**kwargs)
    # - Parse parallelism kwargs:
    params += parallel_kwargs(**kwargs)
    # - Combine parsed parameters with extra parameters:
    params += extra_params
    log.debug(f"`vt.connexe` params: '{params}'")

    vt_img = image.to_vt_image()
    out_img = vt_connexe(vt_img, params, **kwargs)

    if out_img is None:
        null_msg = "VT method 'connexe' returned NULL image!"
        raise ValueError(null_msg)
    else:
        arr, _, _, _ = _new_from_vtimage(out_img, copy=False)
        attr.setdefault("not_a_label", 0)
        attr["dtype"] = 'uint8' if len(np.unique(arr)) < 255 else "uint16"
        return LabelledImage(arr, **attr)

    # if '-max'.encode('utf-8') in params or '-max'.encode('utf-8') in params:
    #     out_img = out_img.astype('bool')
    #     # Automatic detection & relabelling of background (0) & object (1):
    #     # ==> Signal (intensity) is located where the object is!
    #     sum_0 = np.sum(image * out_img == 0)
    #     sum_1 = np.sum(image * out_img)
    #     if sum_0 > sum_1:
    #         out_img = out_img == 0
    #     return LabelledImage(out_img.astype('uint8'), not_a_label=0)
    # else:
    #     return LabelledImage(out_img, not_a_label=0)
