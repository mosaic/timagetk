#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Quaternion Transformation Module

This module provides utility functions for creating, manipulating, and applying quaternion-based transformations
for image processing. It is particularly useful for creating and performing affine and rigid transformations,
such as translations and rotations, on images, including precise control of transformations around specific axes and points.

Key Features

- **Translation Transformations**: Generate quaternions for translations in 3D space.
- **Rotation Transformations**: Create rotation matrices for rotations around specific axes such as `x`, `y`, or `z`.
- **Point-Centered and Image-Centered Rotations**: Perform rotations around a specified 3D point or image center.
- **Vector Alignment**: Compute transformations to align one vector with another, useful for image alignment or orientation adjustments.
- **Utility Functions**: Includes helpers for computing skew-symmetric matrices, rotation matrices, and transformation composition.

Example Usages

**Translation Transformation**
```python
from timagetk.algorithms.trsf import apply_trsf
from timagetk.algorithms.quaternion import translation_trsf

# Create a transformation that translates an image by 20 units in X and 10 in Y
trsf = translation_trsf(tx=20, ty=10, tz=0)
translated_image = apply_trsf(image, trsf)
```

**Rotation Transformation**
```python
from timagetk.algorithms.trsf import apply_trsf
from timagetk.algorithms.quaternion import rotation_trsf

# Rotate an image around the Z-axis by 45 degrees
rotation = rotation_trsf(angle=45, axis="z")
rotated_image = apply_trsf(image, rotation)
```

**Point Centered Rotation**
```python
from timagetk.algorithms.trsf import apply_trsf
from timagetk.algorithms.quaternion import point_rotation_trsf

# Rotate around Z-axis by 90 degrees centered at the point (50, 50)
center_point = [50, 50]
rotation = point_rotation_trsf(angle=90, axis="z", point=center_point)
rotated_image = apply_trsf(image, rotation)
```

**Vector Alignment**
```python
from timagetk.algorithms.quaternion import vector_rotation_trsf
from timagetk.algorithms.trsf import apply_trsf

# Align an image's orientation from an initial vector [1, 0, 0] to a reference vector [0, 1, 0]
rotation = vector_rotation_trsf(image, ref_vector=[0, 1, 0], init_vector=[1, 0, 0])
aligned_image = apply_trsf(image, rotation)
```

All transformations are defined in real units (not voxel units) by default and can be conveniently applied
using `timagetk`'s image transformation utilities.
"""
from math import cos
from math import radians
from math import sin

import numpy as np
from timagetk.algorithms.trsf import apply_trsf
from timagetk.algorithms.trsf import compose_trsf
from timagetk.algorithms.trsf import inv_trsf
from timagetk.components.trsf import Trsf


def _axis_str2tuple(axis):
    """Convert axis string value to vector.

    Parameters
    ----------
    axis : {"x", "y", "z"}
        Rotation axis.

    Returns
    -------
    tuple
        Rotation axis vector.

    """
    if axis == 'x':
        u = (1, 0, 0)
    elif axis == 'y':
        u = (0, 1, 0)
    elif axis == 'z':
        u = (0, 0, 1)
    else:
        msg = "Unknown axis '{}', choose among 'x', 'y' or 'z'!"
        raise ValueError(msg.format(axis))

    return u


def _axis_translation(axis, point):
    """Defines translation values parallel to axis and to the given point.

    Parameters
    ----------
    axis : {"x", "y", "z"}
        Rotation axis.
    point : list
        Translation to apply to axis, should be a length-2 list of values.

    Returns
    -------
    float
        Translation for x-axis, in real units (not voxels).
    float
        Translation for y-axis, in real units (not voxels).
    float
        Translation for z-axis, in real units (not voxels).

    Examples
    --------
    >>> from timagetk.algorithms.quaternion import _axis_translation
    >>> #To get a translation of 10 in x, 5 in y and parallel to z-axis:
    >>> tx, ty, tz = _axis_translation('z', [10., 5.])
    >>> print([tx, ty, tz])

    """
    if axis == 'x':
        tx, ty, tz = 0, point[0], point[1]
    elif axis == 'y':
        tx, ty, tz = point[0], 0, point[1]
    elif axis == 'z':
        tx, ty, tz = point[0], point[1], 0
    else:
        msg = "Unknown axis '{}', choose among 'x', 'y' or 'z'!"
        raise ValueError(msg.format(axis))

    return list(map(float, (tx, ty, tz)))


def quaternion_translation_matrix(tx, ty, tz):
    """Return quaternion for image translation.

    Q = [[1., 0., 0., tx],
         [0., 1., 0., ty],
         [0., 0., 1., tz],
         [0., 0., 0., 1.]]

    Parameters
    ----------
    tx : int or float
        Translation along x-axis, in real units (not voxels).
    ty : int or float
        Translation along y-axis, in real units (not voxels).
    tz : int or float
        Translation along z-axis, in real units (not voxels).

    Returns
    -------
    numpy.ndarray
        Quaternion defining the translation.

    Examples
    --------
    >>> from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image
    >>> from timagetk.algorithms.quaternion import quaternion_translation_matrix
    >>> from timagetk import Trsf
    >>> img = example_layered_sphere_wall_image()
    >>> # Example 1 - Defines an x & y translation by half the extent (*i.e.* real units)
    >>> xe, ye, ze = img.get_extent()[::-1]
    >>> tx=round(xe/2., 3); ty=round(ye/2., 3); tz=0
    >>> quat = quaternion_translation_matrix(tx, ty, tz)
    >>> # Apply this translation:
    >>> from timagetk.algorithms.trsf import apply_trsf, inv_trsf
    >>> trsf = Trsf(quat)  # Trsf objects are in real units by default
    >>> print(trsf.get_array())
    [[ 1.   0.   0.  29.7]
     [ 0.   1.   0.  29.7]
     [ 0.   0.   1.   0. ]
     [ 0.   0.   0.   1. ]]
    >>> translated_img = apply_trsf(img, inv_trsf(trsf))  # invert the transformation to "move away" from the origin
    >>> # Display result images using middle z-slice:
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> mid_zsl = img.get_shape('z') // 2
    >>> img_names = ["Original image", "Translated image"]
    >>> fig = grayscale_imshow([img.get_slice(mid_zsl, 'z'), translated_img.get_slice(mid_zsl, 'z')], suptitle=f"Translated by {tx}µm in X and {ty}µm in Y", title=img_names)

    >>> # Example 2 - Defines an x & y translation by 20 voxels in X and 10 in Y:
    >>> xv, yv, zv = img.get_voxelsize()[::-1]
    >>> tx=round(20*xv, 3); ty=round(10*yv, 3); tz=0
    >>> quat = quaternion_translation_matrix(tx, ty, tz)
    >>> # Apply this translation:
    >>> from timagetk.algorithms.trsf import apply_trsf
    >>> trsf = Trsf(quat)
    >>> print(trsf.get_array())
    [[ 1.  0.  0. 12.]
     [ 0.  1.  0.  6.]
     [ 0.  0.  1.  0.]
     [ 0.  0.  0.  1.]]
    >>> translated_img = apply_trsf(img, inv_trsf(trsf))  # invert the transformation to "move away" from the origin
    >>> # Display result images using 'contour projections':
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> mid_zsl = img.get_shape('z') // 2
    >>> img_names = ["Original image", "Translated image"]
    >>> fig = grayscale_imshow([img.get_slice(mid_zsl, 'z'), translated_img.get_slice(mid_zsl, 'z')], suptitle="Translated by 20 voxels in X and 10 in Y", title=img_names)

    """
    translation_q = np.identity(4, dtype=float)
    translation_q[0, 3] = tx
    translation_q[1, 3] = ty
    translation_q[2, 3] = tz

    return translation_q


def quaternion_rotation_matrix(angle, axis):
    """Return quaternion for rotation around axis.

    Parameters
    ----------
    angle : float
        Angle, in degree, of the rotation around the axis.
        Should be in the ``]-360, 360[`` range.
    axis : {"x", "y", "z"}
        Rotation axis.

    Returns
    -------
    numpy.ndarray
        Quaternion defining the rotation of given angle around given axis [wiki_rotmat]_.

    References
    ----------
    .. [wiki_rotmat] https://en.wikipedia.org/wiki/Rotation_matrix#Rotation_matrix_from_axis_and_angle

    Examples
    --------
    >>> from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image
    >>> from timagetk.algorithms.quaternion import quaternion_rotation_matrix
    >>> from timagetk import Trsf
    >>> img = example_layered_sphere_wall_image()
    >>> r_angle = 10
    >>> quat = quaternion_rotation_matrix(r_angle, "z")
    >>> from timagetk.algorithms.trsf import apply_trsf, inv_trsf
    >>> trsf = Trsf(quat)  # Trsf objects are in real units by default
    >>> print(trsf.get_array())
    [[ 0.98480775 -0.17364818  0.          0.        ]
     [ 0.17364818  0.98480775  0.          0.        ]
     [ 0.          0.          1.          0.        ]
     [ 0.          0.          0.          1.        ]]
    >>> rotated_img = apply_trsf(img, inv_trsf(trsf))  # invert the transformation to rotate clockwise around the origin
    >>> # Display result images using middle z-slice:
    >>> mid_zsl = img.get_shape('z') //2
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> img_names = ["Original", "Rotated by {}°".format(r_angle)]
    >>> fig = grayscale_imshow([img.get_slice(mid_zsl, 'z'), rotated_img.get_slice(mid_zsl, 'z')], title=img_names, suptitle="Clockwise rotation along z-axis at origin")

    """
    assert abs(angle) < 360

    # - Initialize 3x3 rotation matrix:
    # -- Convert degree angle to radians:
    r = radians(angle)
    # -- Convert axis string value to vector
    axis_v = _axis_str2tuple(axis)
    # -- Defines identity matrix
    id_mat = np.identity(3)
    # -- Compute the Cross Product Matrix:
    ux, uy, uz = axis_v
    cpm = np.array([[0, -uz, uy], [uz, 0, -ux], [-uy, ux, 0]])
    # -- Compute the Tensor Product:
    tp = np.tensordot(axis_v, axis_v, axes=0)
    # -- Defines rotation matrix:
    rotation_mat = cos(r) * id_mat + sin(r) * cpm + (1 - cos(r)) * tp

    # - Defines quaternion:
    quaternion = np.zeros((4, 4), dtype=float)
    quaternion[:3, :3] = rotation_mat
    quaternion[3, 3] = 1.

    return quaternion


def translation_trsf(tx, ty, tz):
    """Return a transformation, as a ``Trsf`` instance, for image translation.

    Parameters
    ----------
    tx : int or float
        Translation for x-axis, in real units (not voxels).
    ty : int or float
        Translation for y-axis, in real units (not voxels).
    tz : int or float
        Translation for z-axis, in real units (not voxels).

    Returns
    -------
    trsf
        Trsf defining the translation, to use with ``apply_trsf``.

    Notes
    -----
    Function ``apply_trsf`` has a different convention than the one used in ``quaternion_translation_matrix``, thus the translations are inverted.

    Examples
    --------
    >>> from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image
    >>> from timagetk.algorithms.quaternion import translation_trsf
    >>> from timagetk.algorithms.trsf import apply_trsf
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> # Defines a negative x-translation, this will move the image toward the x-axis origin
    >>> trsf = translation_trsf(tx=-20., ty=0, tz=0)
    >>> print(trsf)
    vtTransformation : {
     type    : AFFINE_3D,
     unit    : real,
     }
    >>> print(trsf.get_array())
    [[ 1.  0.  0. 20.]
     [ 0.  1.  0.  0.]
     [ 0.  0.  1.  0.]
     [ 0.  0.  0.  1.]]
    >>> img = example_layered_sphere_wall_image()
    >>> # Apply this translation using shared data as example:
    >>> translated_img = apply_trsf(img, trsf)
    >>> # Display result images using middle z-slice:
    >>> mid_zsl = img.get_shape('z') //2
    >>> img_names = ["Original image", "Translated image"]
    >>> fig = grayscale_imshow([img.get_slice(mid_zsl, 'z'), translated_img.get_slice(mid_zsl, 'z')], img_title=img_names)
    >>> # Defines a y-translation by half the y-extent, this will move the image by away from the y-axis origin
    >>> xe, ye, ze = img.get_extent()
    >>> trsf = translation_trsf(tx=0, ty=ye/2., tz=0)
    >>> print(trsf)
    vtTransformation : {
     type    : AFFINE_3D,
     unit    : real,
     }
    >>> print(trsf.get_array())
    [[  1.           0.           0.           0.        ]
     [  0.           1.           0.         -45.97333385]
     [  0.           0.           1.           0.        ]
     [  0.           0.           0.           1.        ]]
    >>> # Apply this translation using shared data as example:
    >>> translated_img = apply_trsf(img, trsf)
    >>> # Display result images using middle z-slice:
    >>> img_names = ["Original image", "Translated image"]
    >>> fig = grayscale_imshow([img.get_slice(mid_zsl, 'z'), translated_img.get_slice(mid_zsl, 'z')], title=img_names)

    """
    return inv_trsf(Trsf(quaternion_translation_matrix(tx, ty, tz)))


def rotation_trsf(angle, axis):
    """Return a transformation, as a ``Trsf`` instance, for rotation around given axis.

    Parameters
    ----------
    angle : float
        angle, in degree, of the rotation around the axis. Should be in the ``]-360, 360[`` range.
    axis : {"x", "y", "z"}
        Rotation axis.

    Returns
    -------
    Trsf
        Rotation of given angle around axis, to use with ``apply_trsf``.

    See Also
    --------
    timagetk.algorithms.trsf.apply_trsf

    Examples
    --------
    >>> from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image
    >>> from timagetk.algorithms.trsf import apply_trsf
    >>> from timagetk.algorithms.quaternion import rotation_trsf
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> img = example_layered_sphere_wall_image()
    >>> # Origin centered z-axis rotation by 10°:
    >>> trsf_z10 = rotation_trsf(10, "z")
    >>> rotated_imgz10 = apply_trsf(img, trsf_z10)
    >>> # Origin centered x-axis rotation by 10°:
    >>> trsf_x10 = rotation_trsf(10, "x")
    >>> rotated_imgx10 = apply_trsf(img, trsf_x10)
    >>> # Display result images using middle z-slice:
    >>> mid_zsl = img.get_shape('z') //2
    >>> img_names = ["Original image", "Clockwise z-rotation by 10°", "Clockwise x-rotation by 10°"]
    >>> fig = grayscale_imshow([img.get_slice(mid_zsl, 'z'), rotated_imgz10.get_slice(mid_zsl, 'z'), rotated_imgx10.get_slice(mid_zsl, 'z')], suptitle="Origin centered rotations", title=img_names)

    """
    return inv_trsf(Trsf(quaternion_rotation_matrix(angle, axis)))


def point_rotation_trsf(angle, axis, point):
    """Return a transformation, as a ``Trsf`` instance, for rotation around axis and point.

    Parameters
    ----------
    angle : float
        Angle, in degree, of the clockwise rotation around the axis.
        Should be in the ``]-360, 360[`` range.
    axis : {"x", "y", "z"}
        Rotation axis.
    point : list of float
        XYZ coordinates of the rotation point, pay attention to the associated ``trsf_unit``.
        This corresponds to the translation to apply to axes not selected in ``axis``.

    Returns
    -------
    Trsf
        Rotation of given angle around axis, centered at point, to use with ``apply_trsf``.

    See Also
    --------
    timagetk.algorithms.trsf.apply_trsf

    Examples
    --------
    >>> from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image
    >>> from timagetk.algorithms.trsf import apply_trsf
    >>> from timagetk.algorithms.quaternion import point_rotation_trsf
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> img = example_layered_sphere_wall_image()
    >>> x, y, z = img.get_extent()[::-1]
    >>> xy_center = (x/2., y/2.)
    >>> xz_center = (x/2., z/2.)
    >>> r_angle = 10
    >>> # Rotate image around centered z-axis:
    >>> z_rotate_trsf = point_rotation_trsf(r_angle, 'z', xy_center)
    >>> z_rotated_img = apply_trsf(img, z_rotate_trsf)
    >>> # Rotate image around centered y-axis:
    >>> y_rotate_trsf = point_rotation_trsf(r_angle, 'y', xz_center)
    >>> y_rotated_img = apply_trsf(img, y_rotate_trsf)
    >>> # Display result images using middle z-slice:
    >>> mid_zsl = img.get_shape('z') //2
    >>> img_names = ["Original image", f"Z-axis {r_angle}° rotated image", f"X-axis {r_angle}° rotated image"]
    >>> fig = grayscale_imshow([img.get_slice(mid_zsl, 'z'), z_rotated_img.get_slice(mid_zsl, 'z'), y_rotated_img.get_slice(mid_zsl, 'z')], suptitle="Axis centered rotation", title=img_names)

    """
    tx, ty, tz = _axis_translation(axis, point)
    trans_trsf = translation_trsf(tx, ty, tz)
    rotat_trsf = rotation_trsf(angle, axis)
    return compose_trsf([inv_trsf(trans_trsf), rotat_trsf, trans_trsf])


def centered_rotation_trsf(image, angle, axis):
    """Rotate an image around centered axis.

    Parameters
    ----------
    image : timagetk.SpatialImage
        Image to rotate
    angle : float
        Angle, in degree, of the rotation around the axis. Should be in the ``]-360, 360[`` range.
    axis : {"x", "y", "z"}
        Rotation axis.

    Returns
    -------
    Trsf
        Rotation of given angle around axis, image centered, to use with ``apply_trsf``.

    See Also
    --------
    timagetk.algorithms.trsf.apply_trsf

    Examples
    --------
    >>> from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image
    >>> from timagetk.algorithms.trsf import apply_trsf
    >>> from timagetk.algorithms.quaternion import centered_rotation_trsf
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> img = example_layered_sphere_wall_image()
    >>> r_angle = 10
    >>> # Rotate around z-axis by 90° clockwise:
    >>> trsf_z = centered_rotation_trsf(img, r_angle, 'z')
    >>> z_rotated_img = apply_trsf(img, trsf_z)
    >>> # Rotate around x-axis by 90° clockwise:
    >>> trsf_x = centered_rotation_trsf(img, r_angle, 'x')
    >>> x_rotated_img = apply_trsf(img, trsf_x)
    >>> # Display result images using middle z-slice:
    >>> mid_zsl = img.get_shape('z')//2
    >>> img_names = ["Original image", f"Z-axis {r_angle}° rotated image", f"X-axis {r_angle}° rotated image"]
    >>> fig = grayscale_imshow([img.get_slice(mid_zsl, 'z'), z_rotated_img.get_slice(mid_zsl, 'z'), x_rotated_img.get_slice(mid_zsl, 'z')], suptitle="Axis centered rotation", title=img_names)

    """
    tx, ty, tz = np.array(image.get_extent()[::-1]) / 2.
    return point_rotation_trsf(angle, axis, point=[tx, ty, tz])


def ssc(v):
    """Skew-Symmetric Cross-Product Matrix.

    Parameters
    ----------
    v : array-like
        The vector.

    Returns
    -------
    numpy.ndarray
        The skew-symmetric cross-product matrix.

    Notes
    -----
    The skew-symmetric matrix for a vector :math:`\mathbf{v} = [v_0, v_1, v_2] )` is given by:

    .. math::
        \text{ssc}(\mathbf{v}) = \begin{bmatrix} 0 & -v_2 & v_1 \ v_2 & 0 & -v_0 \ -v_1 & v_0 & 0 \end{bmatrix}
    """
    return np.array([[0, -v[2], v[1]], [v[2], 0, -v[0]], [-v[1], v[0], 0]])


def rotation_matrix_from_vec(a, b):
    """Compute a rotation matrix aligning one vector w.r.t. another.

    Parameters
    ----------
    a : array-like
        A vector.
    b : array-like
        A reference vector.

    Returns
    -------
    numpy.ndarray
        The rotation matrix.

    """
    a = np.asarray(a)
    b = np.asarray(b)
    # Check if the input vector are null
    if np.all(a == 0) or np.all(b == 0):
        raise ValueError("Input vectors must be non-zero.")

    cross_ab = np.cross(a, b)
    norm_cross_ab = np.linalg.norm(cross_ab)
    # Check the input vectors are parallel
    if norm_cross_ab == 0:
        # Return identity matrix if input vectors are parallel
        return np.eye(3)

    ssc_matrix = ssc(cross_ab)
    dot_ab = np.dot(a, b)
    return np.eye(3) + ssc_matrix + np.dot(ssc_matrix, ssc_matrix) * (1 - dot_ab) / (norm_cross_ab ** 2)


def vector_rotation_trsf(image, ref_vector, init_vector):
    """Return a transformation to align an image using an initial vector and a target vector.

    Parameters
    ----------
    image : timagetk.SpatialImage
        An image to rotate
    ref_vector : list or numpy.ndarray
        A reference 3D vector to align the initial vector to.
    init_vector : list or numpy.ndarray
        A 3D vector giving the initial image orientation.

    Returns
    -------
    timagetk.Trsf
        The rotation matrix, image centered, to use with ``apply_trsf``.

    Examples
    --------
    >>> from timagetk.algorithms.quaternion import vector_rotation_trsf
    >>> from timagetk.algorithms.trsf import apply_trsf
    >>> from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> img = example_layered_sphere_wall_image()
    >>> rot_trsf = vector_rotation_trsf(img, [0,1,0], [1,0,0])  # rotate X-axis on Y-axis
    >>> z_rotated_img = apply_trsf(img, rot_trsf)
    >>> # Display result images using middle z-slice:
    >>> mid_zsl = img.get_shape('z')//2
    >>> img_names = ["Original image", "Rotated image"]
    >>> fig = grayscale_imshow([img.get_slice(mid_zsl, 'z'), z_rotated_img.get_slice(mid_zsl, 'z')], suptitle="Axis centered rotation", title=img_names)

    """
    tissue_center = np.array(image.get_extent())[::-1] // 2  # expect to be XYZ sorted!
    # - Construct the rigid transformation as follows:
    #   1. T1: translate the tissue's center at the origin
    #   2. R1: rotate the translated tissue
    #   3. T2: translate (back) the tissue's center at its original position
    T1, T2, R1 = np.eye(4), np.eye(4), np.eye(4)  # required order XYZ
    T1[:3, -1] = -tissue_center
    T2[:3, -1] = tissue_center
    rot_mat = rotation_matrix_from_vec(ref_vector, init_vector)
    R1[:3, :3] = rot_mat
    return Trsf((T2.dot(R1)).dot(T1))
