#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Template Image Utilities

This module provides tools for creating and manipulating image templates, particularly focusing on
resampling images into isometric templates.
It is useful for applications where uniform voxel sizes or consistent spatial resolutions are required
for image processing tasks.

Key Features

- **Isometric Template Creation**:

  - Resample images to isometric voxel sizes for consistent spatial representation.
  - Support for different strategies to define the output voxel size:
    - Minimum voxel size for higher resolution.
    - Maximum voxel size for reduced memory usage.
    - User-defined voxel size for specific requirements.

- **Integration with timagetk**:

  - Works seamlessly with `timagetk` formats like `SpatialImage` and `LabelledImage`.

Usage Examples

1. Resample an image with the smallest isometric template (maximal voxel size):

   ```python
   from timagetk.algorithms.template import iso_template
   from timagetk.io.dataset import shared_data

   img = shared_data('flower_confocal', 0)  # Load an example image
   small_template = iso_template(img, value="max")
   print(f"Small template shape: {small_template.get_shape()}")  # [59, 92, 92]
   print(f"Small template voxelsize: {small_template.get_voxelsize()}")  # [1.0, 1.0, 1.0]
   ```

2. Generate the largest isometric template (minimal voxel size):

   ```python
   large_template = iso_template(img, value="min")
   print(f"Large template shape: {large_template.get_shape()}")  # [295, 460, 460]
   print(f"Large template voxelsize: {large_template.get_voxelsize()}")  # [0.2, 0.2, 0.2]
   ```

3. Define an isometric template with a custom voxel size:

   ```python
   custom_template = iso_template(img, value=0.5)
   print(f"Custom template shape: {custom_template.get_shape()}")
   print(f"Custom template voxelsize: {custom_template.get_voxelsize()}")  # [0.5, 0.5, 0.5]
   ```

This module simplifies the creation of templates used for image processing tasks, ensuring consistency
and flexibility in generating resampled images.
"""


def iso_template(image, value='max'):
    """Return the isometric template of the image.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.LabelledImage
        The image to resample.
    value : {'min', 'max', float}, optional
        Change voxelsize to 'min', 'max' (default) of original voxelsize or to a given value.

    Returns
    -------
    timagetk.SpatialImage
        Isometric SpatialImage template.

    Examples
    --------
    >>> from timagetk.algorithms.template import iso_template
    >>> from timagetk.io.dataset import shared_data
    >>> # Loading an example grayscale image and show info:
    >>> img = shared_data('flower_confocal', 0)
    >>> print(f"Original image ZYX shape: {img.get_shape()}")
    Original image ZYX shape: [59, 460, 460]
    >>> print(f"Original image ZYX voxelsize: {img.get_voxelsize()}")
    Original image ZYX voxelsize: [1.0, 0.20031953747828882, 0.20031953747828882]

    >>> # Example #1 - Compute the smallest isometric template w.r.t. voxel-size (smallest shape, maximal voxel-size)
    >>> small_template = iso_template(img, value="max")
    >>> print(f"Small template image ZYX shape: {small_template.get_shape()}")
    Small template image ZYX shape: [59, 92, 92]
    >>> print(f"Small template image ZYX voxelsize: {small_template.get_voxelsize()}")
    Small template image ZYX voxelsize: [1.0, 1.0, 1.0]

    >>> # Example #2 - Compute the largest isometric template w.r.t. voxel-size (biggest shape, minimal voxel-size)
    >>> large_template = iso_template(img, value="min")
    >>> print(f"Large template image ZYX shape: {large_template.get_shape()}")
    Large template image ZYX shape: [295, 460, 460]
    >>> print(f"Large template image ZYX voxelsize: {large_template.get_voxelsize()}")
    Large template image ZYX voxelsize: [0.20031953747828882, 0.20031953747828882, 0.20031953747828882]

    >>> # Example #3 - Compute an isometric template to a given voxel-size:
    >>> template = iso_template(img, value=0.5)
    >>> print(f"Template image ZYX shape: {template.get_shape()}")
    Template image ZYX shape: [116, 184, 184]
    >>> print(f"Template image ZYX voxelsize: {template.get_voxelsize()}")
    Template image ZYX voxelsize: [0.5, 0.5, 0.5]

    """
    import numpy as np
    from timagetk.algorithms.resample import isometric_resampling
    from timagetk.components.image import get_image_attributes
    from timagetk.components.image import get_image_class
    template = np.zeros_like(image, dtype='uint8')  # to reduce memory footprint
    attr = get_image_attributes(image, exclude=["shape"])
    Image = get_image_class(image)
    template = Image(template, **attr)
    return isometric_resampling(template, value=value, interpolation='linear')
