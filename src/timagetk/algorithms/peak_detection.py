#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Peak detection module.

This module is designed to provide methods for detecting local signal peaks in intensity images,
with a focus on identifying and quantifying nuclei in biological imagery. This is particularly useful
for analyzing cellular images, identifying nuclei positions, and measuring signal intensity locally
for further biological data processing.

Purpose

The module allows robust detection of 3D signal peaks using a scale-space approach based on Gaussian filtering.
This supports quantitative analysis of 3D biological images, assisting in tasks like nuclei localization or
segmentation refinement.

Main Functionality

The module includes the following key features:

- **Gaussian Scale Space Transformation:** Generates multi-scale representations of intensity signals using Gaussian filters.
- **Scale-Space Peak Detection:** Detects local intensity maxima across multi-scale representations to find 3D peaks.
- **Nuclei Detection:** Identifies locations of nuclei in intensity images, outputting their physical coordinates.
- **Nuclei Detection with Segmentation Constraints:** Detects a single nucleus per segmented cell, associating it with cell regions.

Usage Examples

Below is an example demonstrating how to use the module to detect nuclei in a synthetic biological intensity image:

```python
import numpy as np
from timagetk.algorithms.peak_detection import detect_nuclei
from timagetk.synthetic_data.nuclei_image import example_nuclei_image
import matplotlib.pyplot as plt

# Create a synthetic nuclei intensity image
nuclei_img, ground_truth_points = example_nuclei_image(
    n_points=10, extent=15., nuclei_radius=1.5, nuclei_intensity=10000, return_points=True
)

# Detect nuclei positions
nuclei_positions = detect_nuclei(nuclei_img, threshold=1500, radius_range=(1.3, 1.7))

# visualisation
plt.figure(figsize=(10, 10))
true_coords = np.array(list(ground_truth_points.values()))  # Ground-truth coordinates
plt.scatter(true_coords[:, 0], true_coords[:, 1], color='red', label='Ground-truth')  # Red: Ground-truth
plt.scatter(nuclei_positions[:, 0], nuclei_positions[:, 1], color='green', marker='x', label='Detected Nuclei')  # Green: Detected peaks
plt.imshow(nuclei_img.max(axis=0), cmap='gray', extent=(0, nuclei_img.extent[2], nuclei_img.extent[1], 0))  # Max intensity projection
plt.legend()
plt.title('Nuclei Detection Visualized on Synthetic Data')
plt.show()
```

By leveraging the functionality of this module, users can efficiently process and analyze 3D biological data for
detecting signal peaks and localizing cellular structures.
"""

import numpy as np
from scipy.cluster.vq import vq
from scipy.ndimage import gaussian_filter
from scipy.ndimage import laplace
from tqdm.autonotebook import tqdm

from timagetk.array_util import make_patches
from timagetk.bin.logger import get_logger
from timagetk.components.labelled_image import LabelledImage
from timagetk.tasks.decorators import singlechannel_wrapper

log = get_logger(__name__)


def gaussian_scale_space(image, sigmas):
    """Apply the Gaussian scale space transform to an intensity image.

    Parameters
    ----------
    image : timagetk.SpatialImage
        Intensity image to transform.
    sigmas : list
        Real unit sigma to use with Gaussian filter.

    Returns
    -------
    numpy.ndarray
        4D array with scale as first dimension and filtered arrays on the 3 other.

    Examples
    --------
    >>> from timagetk.algorithms.peak_detection import gaussian_scale_space
    >>> from timagetk.synthetic_data.nuclei_image import example_nuclei_image
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> nuclei_img = example_nuclei_image(n_points=10, extent=15., nuclei_radius=1.5, nuclei_intensity=10000, return_points=False)
    >>> gss_img = gaussian_scale_space(nuclei_img, [0.5, 1, 1.5])
    >>> # Let's display the detected nuclei as a 2D top-view projection:
    >>> import matplotlib.pyplot as plt
    >>> fig, axs = plt.subplots(1, 3)
    >>> fig.set_size_inches(9, 3)
    >>> axs[0].imshow(gss_img[0].max(axis=0), cmap='gray', extent=(0, nuclei_img.extent[2], nuclei_img.extent[1], 0))  # Add maximum intensity projection of the nuclei signal
    >>> axs[1].imshow(gss_img[1].max(axis=0), cmap='gray', extent=(0, nuclei_img.extent[2], nuclei_img.extent[1], 0))  # Add maximum intensity projection of the nuclei signal
    >>> axs[2].imshow(gss_img[2].max(axis=0), cmap='gray', extent=(0, nuclei_img.extent[2], nuclei_img.extent[1], 0))  # Add maximum intensity projection of the nuclei signal
    >>> plt.suptitle("Example of Gaussian scale space transform on synthetic data")
    >>> plt.show()

    """
    voxelsize = np.array(image.voxelsize)
    scale_space = []
    for sigma in sigmas:
        # Apply Gaussian filter
        gaussian_img = gaussian_filter(np.array(image, np.float64), sigma=sigma / voxelsize, order=0)
        # Add scale dimensionality
        scale_space += [np.sqrt(sigma) * gaussian_img]

    return np.array(scale_space)


def scale_space_transform(image, sigmas):
    """Apply the scale space transform to an intensity image.

    Parameters
    ----------
    image : timagetk.SpatialImage
        Intensity image to transform.
    sigmas : list[float]
        List of scales used to generate the scale-space

    Returns
    -------
    numpy.ndarray
        4D array with scale as first dimension and arrays on the 3 other.

    """
    # TODO: DEPRECATED
    size = np.array(image.shape)
    spacing = np.array(image.voxelsize)
    scale_space = np.zeros((len(sigmas), size[0], size[1], size[2]), dtype=float)

    for i in range(len(sigmas)):
        log.debug("Sigma : ", np.exp(spacing * sigmas[i]))
        if i == 0:
            _ = gaussian_filter(image, sigma=np.exp(spacing * sigmas[i]), order=0)
        else:
            gaussian_img = gaussian_filter(np.array(image, np.float64), sigma=np.exp(spacing * sigmas[i]), order=0)

            laplace_img = laplace(gaussian_img)
            scale_space[i, :, :, :] = laplace_img
            # scale_space[i, : , : , : ] = gaussian_img
            _ = gaussian_img

    return scale_space


def _scale_spatial_local_max(scale_space_images, s, x, y, z, neighborhood=1, voxelsize=(1, 1, 1)):
    """Return true if point [s,x,y,z] is located at a local maxima.

    Parameters
    ----------
    scale_space_images : numpy.ndarray
        Scale-space transform of a 3d image.
    s : int
        Scale coordinate.
    x, y, z : int
        Point coordinates.
    neighborhood : int
        Size, in real unit, of the neighborhood to consider.
    voxelsize : list
        The voxel-size of the scale space image.

    Returns
    -------
    bool
        ``True`` if point [s,x,y,z] is located at a local maxima else ``False``.

    """
    scales = scale_space_images.shape[0]
    image_neighborhood = np.array(np.ceil(neighborhood / np.array(voxelsize)), int)
    neighborhood_coords = np.mgrid[0:scales + 1, -image_neighborhood[0]:image_neighborhood[0] + 1,
                          -image_neighborhood[1]:image_neighborhood[1] + 1,
                          -image_neighborhood[2]:image_neighborhood[2] + 1]
    neighborhood_coords = np.concatenate(
        np.concatenate(np.concatenate(np.transpose(neighborhood_coords, (1, 2, 3, 4, 0))))) + np.array([0, x, y, z])
    neighborhood_coords = np.minimum(np.maximum(neighborhood_coords, np.array([0, 0, 0, 0])),
                                     np.array(scale_space_images.shape) - 1)
    neighborhood_coords = np.unique(neighborhood_coords, axis=0)
    neighborhood_coords = tuple(np.transpose(neighborhood_coords))

    return scale_space_images[neighborhood_coords].max() == scale_space_images[s, x, y, z]


def detect_peaks_3D_scale_space(scale_space_images, sigmas, threshold=None, voxelsize=[1, 1, 1]):
    """Identify local maxima in a 4D scale-space image.

    Parameters
    ----------
    scale_space_images : numpy.ndarray
        Scale-space transform of a 3d image.
    sigmas : list[float]
        List of scales used to generate the scale-space.
    threshold : int, optional
        Minimal intensity to be reached by a candidate maximum.
    voxelsize : list, optional
        The voxel-size of the scale space image.

    Returns
    -------
    numpy.ndarray
        Containing 4D coordinates of local maxima, sorted as S, Z, Y, X.

    """
    log.info("Detecting local peaks...")
    # scales = scale_space_images.shape[0]
    # rows = scale_space_images.shape[1]
    # cols = scale_space_images.shape[2]
    # slices = scale_space_images.shape[3]

    peaks = []
    if threshold is None:
        threshold = np.percentile(scale_space_images, 90)
        log.info(f"Set threshold to {threshold}")
    points = np.array(np.where(scale_space_images > threshold)).transpose()
    n_points = points.shape[0]
    if n_points != 0:
        log.info(f"Found {n_points} potential peaks (scale space image thresholding)")
    else:
        log.critical("Could not find any peaks by thresholding the scale space image!")
        return np.array([])

    x_left_max = np.concatenate([scale_space_images[:, :-1, :, :] - scale_space_images[:, 1:, :, :],
                                 np.zeros_like(scale_space_images)[:, 0:1, :, :]], axis=1) > 0
    y_left_max = np.concatenate([scale_space_images[:, :, :-1, :] - scale_space_images[:, :, 1:, :],
                                 np.zeros_like(scale_space_images)[:, :, 0:1, :]], axis=2) > 0
    z_left_max = np.concatenate([scale_space_images[:, :, :, :-1] - scale_space_images[:, :, :, 1:],
                                 np.zeros_like(scale_space_images)[:, :, :, 0:1]],
                                axis=3) > 0
    x_right_max = np.concatenate([np.zeros_like(scale_space_images)[:, 0:1, :, :],
                                  scale_space_images[:, 1:, :, :] - scale_space_images[:, :-1, :, :]], axis=1) > 0
    y_right_max = np.concatenate([np.zeros_like(scale_space_images)[:, :, 0:1, :],
                                  scale_space_images[:, :, 1:, :] - scale_space_images[:, :, :-1, :]], axis=2) > 0
    z_right_max = np.concatenate([np.zeros_like(scale_space_images)[:, :, :, 0:1],
                                  scale_space_images[:, :, :, 1:] - scale_space_images[:, :, :, :-1]], axis=3) > 0
    local_max = x_left_max & x_right_max & y_left_max & y_right_max & z_left_max & z_right_max

    points = np.array(np.where((scale_space_images > threshold) & local_max)).transpose()
    n_points = points.shape[0]
    if n_points != 0:
        log.info(f"Found {n_points} potential peaks (scale space image local max)")
    else:
        log.critical("Could not find any peaks in the scale space image!")
        return np.array([])

    for _p, point in tqdm(enumerate(points), total=points.shape[0], unit='point'):
        if _scale_spatial_local_max(scale_space_images, point[0], point[1], point[2], point[3],
                                    neighborhood=sigmas[point[0]], voxelsize=voxelsize):
            peaks.append(point)

    return np.array(peaks)


@singlechannel_wrapper
def detect_nuclei(nuclei_img, threshold=3000., radius_range=(0.8, 1.4), step=0.2, patch_size=(100, 100, 100),
                  return_scales=False):
    """Detect nuclei positions in a (16-bit) nuclei marker SpatialImage.

    Parameters
    ----------
    nuclei_img : timagetk.SpatialImage or timagetk.MultiChannelImage
        Intensity image with nuclei targeted signal.
    threshold : float, optional
        Response to Gaussian filter threshold.
        # FIXME: use the binary threshold detection instead ?
    radius_range : tuple, optional
        Range of nuclei size to detect.
    step : float, optional
        Step of radius range.
    patch_size : list, optional
        Patch size to use for detection. Defaults to ``(100, 100, 100)``.
    return_scales: bool
        Whether to return the scale at which each has been detected

    Other Parameters
    ----------------
    channel : str
        If a ``MultiChannelImage`` is used as input image, select the channel to use with this algorithm.

    Returns
    -------
    numpy.ndarray
        Nuclei 3D positions in physical XYZ coordinates.

    Examples
    --------
    >>> import numpy as np
    >>> from scipy.cluster.vq import vq
    >>> from timagetk.algorithms.peak_detection import detect_nuclei
    >>> from timagetk.synthetic_data.nuclei_image import example_nuclei_image
    >>> # Let's start by creating a synthetic nuclei intensity image:
    >>> nuclei_img, points = example_nuclei_image(n_points=10, extent=15., nuclei_radius=1.5, nuclei_intensity=10000, return_points=True)
    >>> # Now we can detect nuclei positions on this synthetic image:
    >>> nuclei_arr = detect_nuclei(nuclei_img, threshold=1500, radius_range=(1.3, 1.7))
    >>> n_nuclei = nuclei_arr.shape[0]
    >>> # It is now possible to compare detected nuclei positions & known position (ground-truth):
    >>> true_points = np.array(list(points.values()))  # convert the dictionary into an XYZ sorted array of points coordinates
    >>> matching = vq(true_points, nuclei_arr)
    >>> # Let's display the detected nuclei as a 2D top-view projection:
    >>> import matplotlib.pyplot as plt
    >>> plt.figure(figsize=(10, 10))
    >>> plt.scatter(true_points[:, 0], true_points[:, 1], color='red', label='Ground-truth')  # Show X & Y coordinates of ground truth coordinates in red
    >>> plt.scatter(nuclei_arr[:, 0], nuclei_arr[:, 1], color='green', marker="x", label='Prediction')  # Show X & Y coordinates of predicted coordinates in green
    >>> plt.imshow(nuclei_img.max(axis=0), cmap='gray', extent=(0, nuclei_img.extent[2], nuclei_img.extent[1], 0))  # Add maximum intensity projection of the nuclei signal
    >>> plt.title("Example of nuclei detection on synthetic data")
    >>> plt.legend()
    >>> plt.show()

    """
    # Get physical voxel dimensions
    voxelsize = np.array(nuclei_img.voxelsize)

    # Calculate range of Gaussian sigmas based on nuclei radius range
    n_sigmas = int(np.ceil(np.log(radius_range[1] / radius_range[0]) / step))
    sigmas = np.exp(np.linspace(np.log(radius_range[0]), np.log(radius_range[1]), n_sigmas))

    # Split image into overlapping patches
    patch_slices = make_patches(nuclei_img.get_array(), patch_size)
    # Calculate center coordinates for each patch
    patch_centers = [
        [(s.start + s.stop) / 2 for s in patch_slice]
        for patch_slice in patch_slices
    ]

    patch_peaks = []
    log.info(f"Detecting peaks over {len(patch_slices)} patches")

    # Process each patch
    for i_p, patch_slice in enumerate(patch_slices):
        # Create scale space by applying Gaussian filters
        scale_space = gaussian_scale_space(nuclei_img[patch_slice], sigmas)
        # Detect peaks in 3D scale space
        peaks = detect_peaks_3D_scale_space(
            scale_space, sigmas, threshold=threshold,
            voxelsize=np.array(nuclei_img.voxelsize)
        )
        # Adjust peak coordinates to global image coordinates
        origin = np.array([s.start for s in patch_slice])
        if len(peaks) > 0:
            peaks[:, 1:] += origin
            # Keep only peaks that belong to current patch
            patch_matching = vq(peaks[:, 1:], patch_centers)[0]
            peaks = peaks[patch_matching == i_p]
        patch_peaks += [peaks]

    # Combine peaks from all patches
    if any([len(p) > 0 for p in patch_peaks]):
        peaks = np.concatenate([p for p in patch_peaks if len(p) > 0])
        n_peaks = peaks.shape[0]
    else:
        n_peaks = 0

    if n_peaks != 0:
        log.info(f"Detected {n_peaks} peaks as nuclei!")
    else:
        log.critical("Could not find any peaks in the provided image!")
        return None

    # Map peak indices to their corresponding scales
    peak_scales = dict(list(zip(np.arange(n_peaks), sigmas[peaks[:, 0]])))

    # Log frequency of detections at each scale
    log.debug(dict(list(
        zip(sigmas, [(np.array(list(peak_scales.values())) == s).sum() / float(len(peak_scales)) for s in sigmas]))))

    # Convert from ZYX image coordinates to physical XYZ coordinates
    if not return_scales:
        return (peaks[:, 1:] * voxelsize)[:, [2, 1, 0]]
    else:
        return (peaks[:, 1:] * voxelsize)[:, [2, 1, 0]], sigmas[peaks[:, 0]]


def detect_segmentation_nuclei(nuclei_img, seg_img, background_label=1, radius_range=(0.8, 1.4), step=0.2):
    """Detect nuclei positions in a nuclei ``SpatialImage`` using a ``LabelledImage``.

    The function detects spherical blobs of image intensity in a nuclei marker image, but uses a segmented membrane
    marker image to provide cell regions in the image.
    Only one nucleus is detected within each cell region of the segmented image, and its 3D position is associated with
    the cell label.

    Parameters
    ----------
    nuclei_img : timagetk.SpatialImage
        Intensity image with nuclei targeted signal.
    seg_img : timagetk.LabelledImage
        Segmented image with cells in which to detect nuclei.
    background_label : int
        Label corresponding to the background region.
    radius_range : tuple, optional
        Range of nuclei size to detect.
    step : float, optional
        Step of radius range.

    Returns
    -------
    dict
        Nuclei 3D position for each cell label, in physical XYZ coordinates.

    """
    # Get voxel size and image dimensions
    voxelsize = np.array(nuclei_img.voxelsize)
    size = np.array(nuclei_img.shape)

    # Convert input to LabelledImage if needed
    if not isinstance(seg_img, LabelledImage):
        seg_img = LabelledImage(seg_img, no_label_id=0)

    # Calculate number of sigma values for scale-space analysis
    n_sigmas = int(np.ceil(np.log(radius_range[1] / radius_range[0]) / step))
    # Generate logarithmically spaced sigma values
    sigmas = np.exp(np.linspace(np.log(radius_range[0]), np.log(radius_range[1]), n_sigmas))

    # Dictionary to store nuclei positions for each cell
    cell_nuclei = {}

    # Process each cell label except background
    for c in tqdm([l for l in seg_img.labels() if l != background_label], unit='label'):
        # Get bounding box for current cell
        bbox = seg_img.boundingbox(c)[c]
        # Calculate Gaussian scale space for cell region
        cell_scale_space = gaussian_scale_space(nuclei_img[bbox] * (seg_img[bbox] == c), sigmas)
        # Create 4D meshgrid for sigma values and physical coordinates
        ss, zz, yy, xx = np.meshgrid(sigmas,
                                     np.arange(size[0])[bbox[0]] * voxelsize[0],
                                     np.arange(size[1])[bbox[1]] * voxelsize[1],
                                     np.arange(size[2])[bbox[2]] * voxelsize[2], indexing='ij')
        # Convert coordinates to physical space
        scale_space_points = np.transpose([zz.ravel(), yy.ravel(), xx.ravel()])
        # Store nucleus position (converting from ZYX to XYZ coordinates)
        cell_nuclei[c] = scale_space_points[np.argmax(cell_scale_space)][[2, 1, 0]]

    return cell_nuclei
