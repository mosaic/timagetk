#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Watershed Segmentation Module

The watershed segmentation module provides functionality for segmenting intensity images, typically
used in identifying and labeling regions in tissue-like images.
This module is useful in image analysis pipelines for biology, microscopy, or other domains requiring
precise region segmentation.

Key Features

- **Seeded Watershed Algorithm**: Implements a robust watershed algorithm using labelled seed images and
  configurable options to handle segmentation conflicts.
- **Seed Detection**: Detects seeds (markers) using local minima detection and connected component labeling,
  essential for initiating the watershed segmentation process.
- **High Customizability**: Parameters such as connectivity, label handling, and preprocessing options provide
  flexibility for diverse use cases.
- **2D and 3D Segmentation Support**: Supports both 2D image slices and full 3D volumes.

Usage Examples

**Example 1: Segmenting a 2D Image Slice**

```python
from timagetk.io.dataset import shared_data
from timagetk.algorithms.linearfilter import gaussian_filter
from timagetk.algorithms.regionalext import regional_extrema
from timagetk.algorithms.connexe import connected_components
from timagetk.algorithms.watershed import watershed

# Load 2D image slice
image = shared_data('flower_confocal', 0)
zslice_img = image.get_slice(25, 'z')

# Smooth image to reduce noise
smooth_img = gaussian_filter(zslice_img, sigma=1.2)

# Detect seeds via regional extrema
h_min = 50
seeds = regional_extrema(smooth_img, height=h_min, method='minima')

# Label seeds and segment with watershed
seed_labels = connected_components(seeds, connectivity=8)
segmented_img = watershed(smooth_img, seed_labels)
```

**Example 2: Detecting Seeds in a 3D Image**

```python
from timagetk.io.dataset import shared_data
from timagetk.algorithms.watershed import seeds_detection

# Load a 3D image
image = shared_data('flower_confocal', 0)

# Detect seeds (local minima) in 3D
h_min = 15
seeds = seeds_detection(image, h_min=h_min)

# Visualize the seed image
from timagetk.visu.stack import stack_browser
stack_browser(seeds, title="Seed image", cmap="glasbey", val_range='auto')
```

This module simplifies the complex process of image segmentation by providing high-level utilities and
built-in options for preprocessing, conflict resolution, and visualization of results.
"""

import numpy as np
from vt import watershed as vt_watershed

from timagetk.bin.logger import get_logger
from timagetk.components.image import assert_spatial_image
from timagetk.components.labelled_image import LabelledImage
from timagetk.components.labelled_image import assert_labelled_image
from timagetk.tasks.decorators import singlechannel_wrapper
from timagetk.third_party.vt_converter import _new_from_vtimage
from timagetk.third_party.vt_parser import general_kwargs
from timagetk.third_party.vt_parser import parallel_kwargs
from timagetk.third_party.vt_parser import watershed_kwargs

log = get_logger(__name__)


@singlechannel_wrapper
def watershed(image, seeds, **kwargs):
    """Seeded watershed algorithm.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        An intensity image to segment.
    seeds : timagetk.LabelledImage
        A seeds image, each seed should have an unique value.

    Other Parameters
    ----------------
    channel : str
        If a ``MultiChannelImage`` is used as input `image`, select the channel to use with this algorithm.
    labelchoice : str in {"first", "min", "most"}
        How to deal with "conflicts", *i.e.* where several labels meet.
        See the "Notes" section for a detailled explanations.
    max_iterations : int
        Use this to set a maximal number of iterations, stop the algorithm before convergence (by default).
    params : str, optional
        CLI parameter string used by ``vt.watershed`` method.

    Returns
    -------
    timagetk.LabelledImage
        The segmented image.

    Raises
    ------
    TypeError
        If ``image`` is not a ``SpatialImage``.
        If ``seeds`` is not a ``timagetk.LabelledImage``.
    ValueError
        If the image returned by ``vt.watershed`` is ``None``.

    See Also
    --------
    timagetk.third_party.vt_parser.LABELCHOICE_METHODS
    timagetk.third_party.vt_parser.DEF_LABELCHOICE
    timagetk.third_party.vt_parser.watershed_kwargs
    timagetk.third_party.vt_parser.general_kwargs
    timagetk.third_party.vt_parser.parallel_kwargs

    Notes
    -----
    Explanations of available ``labelchoice`` keyword argument:

     - **first**: the first label wins (default);
     - **min**: the less represented label wins;
     - **most**: the most represented label wins;

    Example
    -------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.linearfilter import gaussian_filter
    >>> from timagetk.algorithms.regionalext import regional_extrema
    >>> from timagetk.algorithms.connexe import connected_components
    >>> from timagetk.algorithms.watershed import watershed
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> image = shared_data('flower_confocal', 0)

    >>> # EXAMPLE #1 - 2D segmentation
    >>> zslice = 25
    >>> zslice_img = image.get_slice(zslice, 'z')
    >>> # Smooth the 2D slice to avoid too many local minimas:
    >>> sigma = 1.2
    >>> smooth_img = gaussian_filter(zslice_img, sigma=sigma)
    >>> # Search for local minimas:
    >>> hmin = 50
    >>> regext_img = regional_extrema(smooth_img,height=hmin,method='minima')
    >>> # Label detected local minima based on connectivity:
    >>> conn_img = connected_components(regext_img,connectivity=8,low_threshold=1,high_threshold=hmin)
    >>> # Segment the 2D slice with smoothed image and detected seeds:
    >>> labelled_img = watershed(smooth_img, conn_img)
    >>> # Display each steps:
    >>> images = [zslice_img, smooth_img, regext_img, conn_img, labelled_img]
    >>> titles = [f"Original (z-slice: {zslice})", f"Smoothed (sigma={sigma})", f"Local minima (height={hmin})", "Connexe components labelling", "Watershed"]
    >>> fig = grayscale_imshow(images, title=titles, val_range=['type', 'type', 'auto', 'auto', 'auto'], cmap=['gray', 'gray', 'viridis', 'glasbey', 'glasbey'], max_per_line=5)

    >>> # EXAMPLE #2 - 3D segmentation
    >>> # Smooth the 2D slice to avoid too many local minimas:
    >>> sigma = 1.2
    >>> smooth_img = gaussian_filter(image, sigma=sigma)
    >>> # Search for local minimas:
    >>> hmin = 5
    >>> regext_img = regional_extrema(smooth_img,height=hmin,method='minima')
    >>> # Label detected local minima based on connectivity:
    >>> conn_img = connected_components(regext_img,connectivity=26,low_threshold=1,high_threshold=hmin)
    >>> # Segment the 2D slice with smoothed image and detected seeds:
    >>> labelled_img = watershed(smooth_img, conn_img)
    >>> # Display each steps:
    >>> zslice = 25
    >>> images = [image.get_slice(zslice, 'z'), smooth_img.get_slice(zslice, 'z'), regext_img.get_slice(zslice, 'z'), conn_img.get_slice(zslice, 'z'), labelled_img.get_slice(zslice, 'z')]
    >>> titles = [f"Original (z-slice: {zslice})", f"Smoothed (sigma={sigma})", f"Local minima (height={hmin})", "Connexe components labelling", "Watershed"]
    >>> fig = grayscale_imshow(images, title=titles, val_range=['type', 'type', 'auto', 'auto', 'auto'], cmap=['gray', 'gray', 'viridis', 'glasbey', 'glasbey'], max_per_line=5)

    """
    from timagetk.components.image import get_image_attributes
    # - Check image is a ``SpatialImage``:
    assert_spatial_image(image, obj_name='image')
    # - Check seeds is a ``LabelledImage``:
    assert_labelled_image(seeds, obj_name='seeds')
    attr = get_image_attributes(image, exclude=["dtype", "voxelsize"])

    # - Take care of extra parameters given as a string:
    extra_params = kwargs.pop("params", "")
    if not extra_params.startswith(" "):
        extra_params = " " + extra_params
    # - Parse regionalext parameters:
    params, kwargs = watershed_kwargs(**kwargs)
    # - Parse general kwargs:
    params += general_kwargs(**kwargs)
    # - Parse parallelism kwargs:
    params += parallel_kwargs(**kwargs)
    # - Combine parsed parameters with extra parameters:
    params += extra_params
    log.debug(f"`vt.watershed` params: '{params}'")

    vt_img = image.to_vt_image()
    vt_seeds = seeds.to_vt_image()
    out_img = vt_watershed(vt_img, vt_seeds, params)

    if out_img is None:
        null_msg = "VT method 'watershed' returned NULL image!"
        raise ValueError(null_msg)
    else:
        arr, vxs, _, _ = _new_from_vtimage(out_img, copy=False)
        attr["dtype"] = 'uint8' if len(np.unique(arr)) < 255 else "uint16"
        return LabelledImage(arr, voxelsize=vxs, not_a_label=0, **attr)


def seeds_detection(image, h_min, **kwargs):
    """Seed detection algorithm by local minima detection and labelling.

    Parameters
    ----------
    image : timagetk.SpatialImage
        An intensity image to use for local minima detection.
    h_min: int
        A height-minima value to use for local minima detection.

    Other Parameters
    ----------------
    connectivity : int in {4, 6, 8, 10, 18, 26}
        The connectivity of the structuring elements, ``18`` by default.

    See Also
    --------
    timagetk.algorithms.connexe

    Returns
    -------
    timagetk.LabelledImage
        The seeds image.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.watershed import seeds_detection
    >>> from timagetk.visu.stack import stack_browser
    >>> image = shared_data("flower_confocal", 0)
    >>> seeds = seeds_detection(image, h_min=15)
    >>> fig = stack_browser(seeds, title="Seed image", cmap="glasbey", val_range='auto')

    """
    from timagetk.algorithms.connexe import connected_components
    from timagetk.algorithms.regionalext import regional_extrema

    connectivity = kwargs.get('connectivity', 18)

    # - Local minima detection:
    log.info("Local minima detection...")
    ext_img = regional_extrema(image, height=h_min, method='minima')

    # - Connected component labelling of detected local minima:
    log.info("Connected component labelling...")
    seeds_image = connected_components(ext_img, connectivity=connectivity, low_threshold=1, high_threshold=h_min)
    # Print some stuff about seed image
    n_seeds = len(np.unique(seeds_image)) - 1  # '0' is in the list!
    log.info("Detected {} seeds!".format(n_seeds))

    return seeds_image
