#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Cluster Matching Module

This module provides tools for analyzing and matching clusters of cells in tissue graphs based on their properties
and clustering information. It is a key utility for temporal tissue graph analysis, especially for comparing and
matching cell clusters between different states or samples of tissue.

Main Functionality

The module includes functions to:

- Group cell property values by clusters.
- Compute cost matrices between clusters of two graphs using various distance metrics, including Euclidean distances,
  Wasserstein distances, and ranked Wasserstein distances, all based on weighted cell property values.
- Match clusters between two graphs based on the computed cost matrices.

These features make the module valuable for tasks such as quantifying differences between tissue samples, analyzing
evolution in tissue structures, and performing comparative studies of tissue models.

Key Features

1. Grouping of Cell Properties: Aggregate property values by cluster to facilitate statistical analysis.
2. Cost Matrix Computation:
   - Euclidean distances weighted by property medians.
   - Wasserstein distances with optional standardization.
   - Ranked Wasserstein distances providing a rank-based comparison.
3. Cluster Matching: Assign clusters from one graph to the most similar clusters in another based on cost matrices.

Usage Examples

>>> from timagetk.components.clustering import example_clusterer
>>> from timagetk.algorithms.cluster_matching import wasserstein_distance_matrix_from_properties
>>> from timagetk.algorithms.cluster_matching import match_cluster_from_cost
>>> # Define clustering properties and weights
>>> properties = ['volume', "log_relative_value('volume',1)"]
>>> weights_a = [0.5, 0.5]
>>> weights_b = [0.5, 0.5]
>>> # Create and compute clustering for two sample graphs
>>> clusterer_a = example_clusterer()
>>> clusterer_a.assemble_matrix(properties, weights_a)
>>> clusterer_a.compute_clustering(method='ward', n_clusters=4)
>>> graph_a = clusterer_a.graph
>>> clustering_name_a = clusterer_a.clustering_name()
>>> clusterer_b = example_clusterer()
>>> clusterer_b.assemble_matrix(properties, weights_b)
>>> clusterer_b.compute_clustering(method='ward', n_clusters=5)
>>> graph_b = clusterer_b.graph
>>> clustering_name_b = clusterer_b.clustering_name()
>>> # Compute Wasserstein distance matrix between clusters
>>> cost, cluster_idx_a, cluster_idx_b = wasserstein_distance_matrix_from_properties(graph_a,graph_b,clustering_name_a,clustering_name_b,properties,weights_a,weights_b)
>>> # Match clusters based on the cost matrix
>>> matched_clusters = match_cluster_from_cost(cost, cluster_idx_a, cluster_idx_b)
>>> print("Matched Clusters:", matched_clusters)
[(1, 1), (2, 2), (3, 3), (4, 5)]

This module is designed as a plug-and-play component for tissue analysis pipelines, enabling researchers to
efficiently compare and correlate clusters across different tissue graphs.
"""

from math import sqrt

import numpy as np
from scipy.stats import rankdata

from timagetk.bin.logger import get_logger

log = get_logger(__name__)


def cell_property_by_clusters(graph, clustering_name, property_name, **kwargs):
    """Group the cell property values by clusters.

    Parameters
    ----------
    graph : timagetk.graphs.TemporalTissueGraph
        The tissue graph with the cell `clustering` and `property`.
    clustering_name: str
        The name of the clustering to use to group.
    property_name: str
        The name of the property to group.

    Other Parameters
    ----------------
    default_cluster_id : any
        The default value for the cell group, ``None`` by default.
    default_ppty : any
        The default value for the cell property, ``np.nan`` by default.
    only_defined_values : bool
        If ``True`` (default), return the value of a cell property if it is defined, _i.e._ not equal to `default_ppty_val`.
        Else, return the value of all cells defined in `clustering_name` and set it to `default_ppty_val` is non-existant in the `graph`.

    Returns
    -------
    dict
        A cluster indexed dictionary of cell property values.

    Examples
    --------
    >>> from timagetk.algorithms.cluster_matching import cell_property_by_clusters
    >>> from timagetk.components.clustering import example_clusterer
    >>> properties = ['volume', "log_relative_value('volume',1)"]  # selected cell properties
    >>> weights = [0.5, 0.5]  # weights vector
    >>> # Compute the cell clustering using the weights vector & selected cell properties:
    >>> clusterer = example_clusterer()
    >>> clusterer.assemble_matrix(properties, weights)
    >>> clusterer.compute_clustering(method='ward', n_clusters=4)
    >>> graph = clusterer.graph
    >>> clustering_name = clusterer.clustering_name()
    >>> cell_property_by_clusters(graph, clustering_name, 'volume')
    >>>
    >>>

    """
    from timagetk.graphs.graph_analysis import cell_property_by_group

    if 'default_cluster_id' in kwargs:
        kwargs['default_group_id'] = kwargs.pop('default_cluster_id')

    return cell_property_by_group(graph, clustering_name, property_name, **kwargs)


def median_euclidian_distance_matrix_from_properties(graph_a, graph_b, clustering_name_a, clustering_name_b, properties,
                                                     weights_a, weights_b):
    """Return the cost matrix between clusters based on the weighted sum of normalized property median values.

    Parameters
    ----------
    graph_a, graph_b : timagetk.graphs.TemporalTissueGraph
        The temporal tissue graphs to use for clustering matching.
    clustering_name_a, clustering_name_b : str
        Name of the clustering. Should be an existing `graph.cell_property` in each graph.
    properties : list of str
        List of used properties for selected clustering.
    weights_a, weights_b : list of float
        List of used weights for selected clustering.

    Returns
    -------
    numpy.ndarray
        The cost matrix.
    dict
        The dictionary mapping the rows index of the cost matrix to the cluster ids for `graph_a`.
    dict
        The dictionary mapping the columns index of the cost matrix to the cluster ids for `graph_b`.

    Examples
    --------
    >>> from timagetk.components.clustering import example_clusterer
    >>> properties = ['volume', "log_relative_value('volume',1)"]
    >>> weights_a = [0.5, 0.5]  # first weights vector
    >>> weights_b = [0.4, 0.6]  # second weights vector
    >>> # Compute the first clustering using the first weights vector:
    >>> clusterer_a = example_clusterer()
    >>> clusterer_a.assemble_matrix(properties, weights_a)
    >>> clusterer_a.compute_clustering(method='ward', n_clusters=4)
    >>> graph_a = clusterer_a.graph
    >>> clustering_name_a = clusterer_a.clustering_name()
    >>> # Compute the second clustering using the second weights vector:
    >>> clusterer_b = example_clusterer()
    >>> clusterer_b.assemble_matrix(properties, weights_b)
    >>> clusterer_b.compute_clustering(method='ward', n_clusters=4)
    >>> graph_b = clusterer_b.graph
    >>> clustering_name_b = clusterer_b.clustering_name()
    >>> # Compute the distance matrix between obtained clusters:
    >>> from timagetk.algorithms.cluster_matching import median_euclidian_distance_matrix_from_properties
    >>> cost, cluster_idx_a, cluster_idx_b = median_euclidian_distance_matrix_from_properties(graph_a, graph_b, clustering_name_a, clustering_name_b, properties, weights_a, weights_b)
    >>> print(cost)

    """
    # Get the clustering dictionary (indexed by tcids)
    clustering_a = graph_a.cell_property_dict(clustering_name_a, default=-1, only_defined=True)
    clustering_b = graph_b.cell_property_dict(clustering_name_b, default=-1, only_defined=True)
    # Get the associated lists of cluster ids:
    clusters_ids_a = list(set(clustering_a.values()))
    clusters_ids_b = list(set(clustering_b.values()))

    # Get the values by cluster and property:
    val_by_q_by_ppty_a = {}
    for ppty in properties:
        val_by_q_by_ppty_a[ppty] = {q: [] for q in clusters_ids_a}
        for (t, cid), q in clustering_a.items():
            val_by_q_by_ppty_a[ppty][q].append(graph_a.cell_property(t, cid, ppty))

    # Get the values by cluster and property:
    val_by_q_by_ppty_b = {}
    for ppty in properties:
        val_by_q_by_ppty_b[ppty] = {q: [] for q in clusters_ids_b}
        for (t, cid), q in clustering_b.items():
            val_by_q_by_ppty_b[ppty][q].append(graph_b.cell_property(t, cid, ppty))

    # Compute the unit variance by property:
    std_by_ppty_a = {ppty: np.nanstd(list(graph_a.cell_property_dict(ppty, default=np.nan, only_defined=True).values()))
                     for ppty in properties}
    std_by_ppty_b = {ppty: np.nanstd(list(graph_b.cell_property_dict(ppty, default=np.nan, only_defined=True).values()))
                     for ppty in properties}

    # Normalize data to the [0, 1] unit range:
    n_val_by_q_by_ppty_a = {ppty: {q: val_by_q[q] / std_by_ppty_a[ppty] for q in val_by_q} for ppty, val_by_q in
                            val_by_q_by_ppty_a.items()}
    n_val_by_q_by_ppty_b = {ppty: {q: val_by_q[q] / std_by_ppty_b[ppty] for q in val_by_q} for ppty, val_by_q in
                            val_by_q_by_ppty_b.items()}

    # Compute the medians by cluster and property:
    medians_by_q_by_ppty_a = {ppty: [np.nanmedian(val_by_q[q]) for q in val_by_q] for ppty, val_by_q in
                              n_val_by_q_by_ppty_a.items()}
    medians_by_q_by_ppty_b = {ppty: [np.nanmedian(val_by_q[q]) for q in val_by_q] for ppty, val_by_q in
                              n_val_by_q_by_ppty_b.items()}

    # Compute the "median" for each cluster by computing the weighted sum of medians by property:
    medians_by_q_a = {q: np.sum([weights_a[n] * medians_by_q_by_ppty_a[ppty][nq]
                                 for n, ppty in enumerate(properties)]) for nq, q in enumerate(clusters_ids_a)}
    medians_by_q_b = {q: np.sum([weights_b[n] * medians_by_q_by_ppty_b[ppty][nq]
                                 for n, ppty in enumerate(properties)]) for nq, q in enumerate(clusters_ids_b)}

    # Compute the cost as the euclidean distance between each weighted sum of medians by cluster:
    cost = np.zeros(shape=(len(clusters_ids_a), len(clusters_ids_b)))
    for na, qa in enumerate(clusters_ids_a):
        for nb, qb in enumerate(clusters_ids_b):
            cost[na, nb] = sqrt(abs(medians_by_q_a[qa] - medians_by_q_b[qb]) ** 2)

    # Get the dictionary mapping the rows/columns index of the cost matrix to the cluster ids:
    cluster_idx_a = dict(enumerate(clusters_ids_a))
    cluster_idx_b = dict(enumerate(clusters_ids_b))
    return cost, cluster_idx_a, cluster_idx_b


def wasserstein_distance_matrix_from_properties(graph_a, graph_b, clustering_name_a, clustering_name_b, properties,
                                                weights_a, weights_b):
    """Return the cost matrix between clusters based on the weighted sum of normalized property median values.

    Parameters
    ----------
    graph_a, graph_b : timagetk.graphs.TemporalTissueGraph
        The temporal tissue graphs to use for clustering matching.
    clustering_name : str
        Name of the clustering. Should be an existing `graph.cell_property`.
    properties : list of str
        List of used properties for selected clustering.
    weights_a, weights_b : list of float
        List of used weights for selected clustering.

    Returns
    -------
    numpy.ndarray
        The cost matrix.
    dict
        The dictionary mapping the rows index of the cost matrix to the cluster ids for `graph_a`.
    dict
        The dictionary mapping the columns index of the cost matrix to the cluster ids for `graph_b`.

    Examples
    --------
    >>> from timagetk.components.clustering import example_clusterer
    >>> properties = ['volume', "log_relative_value('volume',1)"]
    >>> weights_a = [0.5, 0.5]  # first weights vector
    >>> weights_b = [0.5, 0.5]  # second weights vector
    >>> # Compute the first clustering using the first weights vector:
    >>> clusterer_a = example_clusterer()
    >>> clusterer_a.assemble_matrix(properties, weights_a)
    >>> clusterer_a.compute_clustering(method='ward', n_clusters=4)
    >>> graph_a = clusterer_a.graph
    >>> clustering_name_a = clusterer_a.clustering_name()
    >>> # Compute the second clustering using the second weights vector:
    >>> clusterer_b = example_clusterer()
    >>> clusterer_b.assemble_matrix(properties, weights_b)
    >>> clusterer_b.compute_clustering(method='ward', n_clusters=5)
    >>> graph_b = clusterer_b.graph
    >>> clustering_name_b = clusterer_b.clustering_name()
    >>> # Compute the distance matrix between obtained clusters:
    >>> from timagetk.algorithms.cluster_matching import wasserstein_distance_matrix_from_properties
    >>> cost, cluster_idx_a, cluster_idx_b = wasserstein_distance_matrix_from_properties(graph_a,graph_b,clustering_name_a,clustering_name_b,properties,weights_a,weights_b)

    """
    # Get the clustering dictionary (indexed by tcids)
    clustering_a = graph_a.cell_property_dict(clustering_name_a, default=-1, only_defined=True)
    clustering_b = graph_b.cell_property_dict(clustering_name_b, default=-1, only_defined=True)
    # Get the associated lists of cluster ids:
    clusters_ids_a = list(set(clustering_a.values()))
    clusters_ids_b = list(set(clustering_b.values()))

    # Get the values by cluster and property:
    val_by_q_by_ppty_a = {}
    for ppty in properties:
        val_by_q_by_ppty_a[ppty] = {q: [] for q in clusters_ids_a}
        for (t, cid), q in clustering_a.items():
            val_by_q_by_ppty_a[ppty][q].append(graph_a.cell_property(t, cid, ppty, default=np.nan))

    # Get the values by cluster and property:
    val_by_q_by_ppty_b = {}
    for ppty in properties:
        val_by_q_by_ppty_b[ppty] = {q: [] for q in clusters_ids_b}
        for (t, cid), q in clustering_b.items():
            val_by_q_by_ppty_b[ppty][q].append(graph_b.cell_property(t, cid, ppty, default=np.nan))

    # Compute the cost as the standardized wasserstein distance between cluster:
    from scipy.stats import wasserstein_distance
    cost = np.zeros(shape=(len(clusters_ids_a), len(clusters_ids_b)))
    for na, (qa) in enumerate(clusters_ids_a):
        for nb, (qb) in enumerate(clusters_ids_b):
            for ppty in properties:
                data_a = val_by_q_by_ppty_a[ppty][qa]
                data_b = val_by_q_by_ppty_b[ppty][qb]
                std = np.std(np.concatenate([data_a, data_b]))
                w_dist = wasserstein_distance(data_a, data_b)
                if std != 0.:
                    std_w_dist = w_dist / std
                else:
                    std_w_dist = 0.
                cost[na, nb] += std_w_dist

    # Get the dictionary mapping the rows/columns index of the cost matrix to the cluster ids:
    cluster_idx_a = dict(enumerate(clusters_ids_a))
    cluster_idx_b = dict(enumerate(clusters_ids_b))
    return cost, cluster_idx_a, cluster_idx_b


def wasserstein_distance_matrix_from_ranked_properties(graph_a, graph_b, clustering_name_a, clustering_name_b,
                                                       properties, weights_a, weights_b):
    """Return the cost matrix between clusters based on the ranked wasserstein distance.

    Parameters
    ----------
    graph_a, graph_b : timagetk.graphs.TemporalTissueGraph
        The temporal tissue graphs to use for clustering matching.
    clustering_name : str
        Name of the clustering. Should be an existing `graph.cell_property`.
    properties : list of str
        List of used properties for selected clustering.
    weights_a, weights_b : list of float
        List of used weights for selected clustering.

    Returns
    -------
    numpy.ndarray
        The cost matrix.
    dict
        The dictionary mapping the rows index of the cost matrix to the cluster ids for `graph_a`.
    dict
        The dictionary mapping the columns index of the cost matrix to the cluster ids for `graph_b`.

    Examples
    --------
    >>> from timagetk.components.clustering import example_clusterer
    >>> properties = ['volume', "log_relative_value('volume',1)"]
    >>> weights_a = [0.5, 0.5]  # first weights vector
    >>> weights_b = [0.5, 0.5]  # second weights vector
    >>> # Compute the first clustering using the first weights vector:
    >>> clusterer_a = example_clusterer()
    >>> clusterer_a.assemble_matrix(properties, weights_a)
    >>> clusterer_a.compute_clustering(method='ward', n_clusters=4)
    >>> graph_a = clusterer_a.graph
    >>> clustering_name_a = clusterer_a.clustering_name()
    >>> # Compute the second clustering using the second weights vector:
    >>> clusterer_b = example_clusterer()
    >>> clusterer_b.assemble_matrix(properties, weights_b)
    >>> clusterer_b.compute_clustering(method='ward', n_clusters=5)
    >>> graph_b = clusterer_b.graph
    >>> clustering_name_b = clusterer_b.clustering_name()
    >>> # Compute the distance matrix between obtained clusters:
    >>> from timagetk.algorithms.cluster_matching import wasserstein_distance_matrix_from_ranked_properties
    >>> cost, cluster_idx_a, cluster_idx_b = wasserstein_distance_matrix_from_ranked_properties(graph_a, graph_b, clustering_name_a, clustering_name_b, properties, weights_a, weights_b)

    """
    # Get the clustering dictionary (indexed by tcids)
    clustering_a = graph_a.cell_property_dict(clustering_name_a, default=-1, only_defined=True)
    clustering_b = graph_b.cell_property_dict(clustering_name_b, default=-1, only_defined=True)
    # Get the associated lists of cluster ids:
    clusters_ids_a = list(set(clustering_a.values()))
    clusters_ids_b = list(set(clustering_b.values()))

    # Get the ranked values by cluster and property:
    rval_by_q_by_ppty_a = {}
    for ppty in properties:
        val_by_ppty_a = graph_a.cell_property_dict(ppty, default=np.nan, only_defined=True)
        ranked_val_by_ppty_a = dict(zip(val_by_ppty_a.keys(), rankdata(list(val_by_ppty_a.values()))))
        rval_by_q_by_ppty_a[ppty] = {q: [] for q in clusters_ids_a}
        for tcid, q in clustering_a.items():
            rval_by_q_by_ppty_a[ppty][q].append(ranked_val_by_ppty_a[tcid])

    # Get the ranked values by cluster and property:
    rval_by_q_by_ppty_b = {}
    for ppty in properties:
        val_by_ppty_b = graph_b.cell_property_dict(ppty, default=np.nan, only_defined=True)
        ranked_val_by_ppty_b = dict(zip(val_by_ppty_b.keys(), rankdata(list(val_by_ppty_b.values()))))
        rval_by_q_by_ppty_b[ppty] = {q: [] for q in clusters_ids_b}
        for tcid, q in clustering_b.items():
            rval_by_q_by_ppty_b[ppty][q].append(ranked_val_by_ppty_b[tcid])

    # Compute the cost as the standardized wasserstein distance between cluster:
    from scipy.stats import wasserstein_distance
    cost = np.zeros(shape=(len(clusters_ids_a), len(clusters_ids_b)))
    for na, (qa) in enumerate(clusters_ids_a):
        for nb, (qb) in enumerate(clusters_ids_b):
            for ppty in properties:
                data_a = rval_by_q_by_ppty_a[ppty][qa]
                data_b = rval_by_q_by_ppty_b[ppty][qb]
                w_dist = wasserstein_distance(data_a, data_b)
                cost[na, nb] += w_dist

    # Get the dictionary mapping the rows/columns index of the cost matrix to the cluster ids:
    cluster_idx_a = dict(enumerate(clusters_ids_a))
    cluster_idx_b = dict(enumerate(clusters_ids_b))
    return cost, cluster_idx_a, cluster_idx_b


def match_cluster_from_cost(cost, cluster_idx_a, cluster_idx_b):
    """Match clusters of the two FeatureClusterer instances based on their properties.

    Parameters
    ----------
    cost : numpy.ndarray
        The cost matrix.
    cluster_idx_a : dict
        The dictionary mapping the rows index of the cost matrix to the cluster ids for `graph_a`.
    cluster_idx_b : dict
        The dictionary mapping the columns index of the cost matrix to the cluster ids for `graph_b`.

    Returns
    -------
    list of tuple
        List of matched pairs of clusters.

    Examples
    --------
    >>> from timagetk.components.clustering import example_clusterer
    >>> properties = ['volume', "log_relative_value('volume',1)"]
    >>> weights_a = [0.5, 0.5]
    >>> weights_b = [0.5, 0.5]
    >>> clusterer_a = example_clusterer()
    >>> clusterer_a.assemble_matrix(properties, weights_a)
    >>> clusterer_a.compute_clustering(method='ward', n_clusters=4)
    >>> graph_a = clusterer_a.graph
    >>> clustering_name_a = clusterer_a.clustering_name()
    >>> clusterer_b = example_clusterer()
    >>> clusterer_b.assemble_matrix(properties, weights_b)
    >>> clusterer_b.compute_clustering(method='ward', n_clusters=5)
    >>> graph_b = clusterer_b.graph
    >>> clustering_name_b = clusterer_b.clustering_name()
    >>> from timagetk.algorithms.cluster_matching import wasserstein_distance_matrix_from_properties
    >>> cost, cluster_idx_a, cluster_idx_b = wasserstein_distance_matrix_from_properties(graph_a,graph_b,clustering_name_a,clustering_name_b,properties,weights_a,weights_b)
    >>> from timagetk.algorithms.cluster_matching import match_cluster_from_cost
    >>> match_cluster_from_cost(cost, cluster_idx_a, cluster_idx_b)
    [(1, 1), (2, 2), (3, 3), (4, 5)]

    """
    from scipy.optimize import linear_sum_assignment
    match = list(zip(*linear_sum_assignment(cost)))
    return [(cluster_idx_a[q_a], cluster_idx_b[q_b]) for (q_a, q_b) in match]
