#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Intensity Image Registration Using Block Matching

This module provides methods for registering intensity-based images using the block-matching algorithm.
The primary utility of this module is to align images, making it particularly useful in domains such as
medical imaging, microscopy, and other applications where precise image overlay is required.

Main Functionality

- Implements the `blockmatching` function for image registration, enabling both rigid, affine, and deformable transformations.
- Provides tools for multi-dimensional pyramid-level computation and optimization, aiding in hierarchical registration strategies.
- Extends the functionality for various image types, including single-channel and multi-channel images, with flexible parameterization.

Key Features

1. **Block-Matching Algorithm**: Allows registration using methods like 'rigid', 'affine', 'vectorfield', and 'similitude'.
2. **Initialization**: Provides support for initial transformations (`init_trsf`) to improve registration results.
3. **Pyramid Levels**: Includes functionality for defining pyramid levels based on voxel sizes or image sizes.
4. **Regularization**: Supports various transformation constraints like elastic or fluid regularization.
5. **Extensive Parameterization**: Offers tunable thresholds, estimators, and pyramid strategies for optimal image alignment.
6. **Error Handling and Validation**: Validates input image types and ensures compatibility with spatial image requirements.

Usage Examples

To register a *floating image* onto a *reference image* using the **affine** registration method:

>>> from timagetk.algorithms.blockmatching import blockmatching
>>> from timagetk.io.dataset import shared_data
>>> from timagetk.algorithms.trsf import apply_trsf
>>> from timagetk.visu.mplt import grayscale_imshow
>>> # Load reference and floating images
>>> reference_image = shared_data('flower_confocal', 0)
>>> floating_image = shared_data('flower_confocal', 1)
>>> # Perform block-matching registration with affine transformation
>>> transformation = blockmatching(floating_image, reference_image, method="affine")
>>> # Apply the resulting transformation to align the floating image onto the reference
>>> aligned_image = apply_trsf(floating_image, transformation, template_img=reference_image)
>>> # Visualize the results
>>> fig = grayscale_imshow([reference_image, aligned_image], title=['Reference', 'Aligned Floating'])

This example illustrates the registration of a floating image onto a reference using affine transformation.
With its versatile parameterization and robust methods, the module ensures accurate and efficient image alignment.
"""

import numpy as np

from vt import blockmatching as vt_blockmatching

from timagetk.bin.logger import get_logger
from timagetk.components.image import assert_spatial_image
from timagetk.components.trsf import Trsf
from timagetk.tasks.decorators import dual_singlechannel_wrapper
from timagetk.third_party.vt_parser import DEF_BM_METHOD
from timagetk.third_party.vt_parser import blockmatching_kwargs
from timagetk.third_party.vt_parser import general_kwargs
from timagetk.third_party.vt_parser import parallel_kwargs

NOT_TRSF = "Provided '{}' is not a ``Trsf`` instance!"
NOT_LINEAR = "Provided '{}' is not a linear transformation!"

log = get_logger(__name__)


@dual_singlechannel_wrapper
def blockmatching(floating_image, reference_image, method=DEF_BM_METHOD, init_trsf=None, left_trsf=None, **kwargs):
    """Block-matching registration algorithm.

    Registers a ``floating_image`` :math:`I_{flo}` onto a ``reference_image`` :math:`I_{ref}`.
    The returned transformation goes from the reference frame (:math:`I_{ref}`) towards the floating frame (:math:`I_{flo}`), that is :math:`T(I_{flo} \leftarrow I_{ref})`.
    It accept a transformation as input, useful to initialize the algorithm.

    Parameters
    ----------
    floating_image : timagetk.SpatialImage or timagetk.MultiChannelImage
        The image to register :math:`I_{flo}`.
    reference_image : timagetk.SpatialImage or timagetk.MultiChannelImage
        The reference image :math:`I_{ref}` to use for registration of the `floating_image`.
    method : {'similitude', 'rigid', 'affine', 'vectorfield'}, optional
        The registration method to use, ``'rigid'`` by default.
    init_trsf : Trsf, optional
        If given, it is used to initialise the registration and the returned transformation will contain it (composition).
    left_trsf : Trsf, optional
        If given, it is used to initialise the registration but the returned transformation will NOT contain it (no composition).

    Other Parameters
    ----------------
    channel : str
        If ``MultiChannelImage`` are used as input images, use this argument to specify the name of the channel to use.
    pyramid_lowest_level : int
        Lowest level at which to compute deformation, default is ``DEF_PY_LL`` and min value is 0.
    pyramid_highest_level : int
        Highest level at which to compute deformation, default is ``DEF_PY_HL`` and max value is 5.
    pyramid_lowest_voxel : float or list of floats, optional
        Automatically set `lowest_pyramid_level` to match the given voxelsize.
    pyramid_highest_voxel : float or list of floats, optional
        Automatically set `pyramid_highest_level` to match the given voxelsize.
    estimator : {'wlts', 'lts', 'wls', 'ls'}, optional
        Transformation estimator. 'wlts' by default.
    elastic_sigma : float, optional
        Transformation regularization, sigma for elastic regularization (only for vector field).
        This is equivalent to a gaussian filtering of the final deformation field.
    fluid_sigma : float, optional
        Sigma for fluid regularization ie field interpolation and regularization for pairings (only for vector field).
        This is equivalent to a gaussian filtering of the incremental deformation field.
    lts_fraction : float, optional
        If defined, set the fraction of pairs that are kept, used with trimmed estimations. ``0.5`` by default.
    lts_deviation : float, optional
        If defined, set the threshold to discard pairings, used with trimmed estimations.
        See the "Notes" section for detailled explanations.
    lts_iterations : int, optional
        If defined, set the maximal number of iterations, used with trimmed estimations. ``100`` by default.
    pyramid_gaussian_filtering : bool, optional
        Before subsampling, the images are filtered (*i.e.* smoothed) by a gaussian kernel. ``False`` by default.
    floating_low_threshold : int, optional
        Intensity values inferior or equal to low threshold are not considered in block selection, minimum by default.
    floating_high_threshold : int, optional
        Intensity values superior or equal to high threshold are not considered in block selection, maximum by default.
    reference_low_threshold : int, optional
        Intensity values inferior or equal to low threshold are not considered in block selection, minimum by default.
    reference_high_threshold : int, optional
        Intensity values superior or equal to high threshold are not considered in block selection, maximum by default.
    param : bool
        If ``True``, default ``False``, print the used parameters.
    verbose : bool
        If ``True``, default ``False``, increase code verbosity.
    time : bool
        If ``True``, default ``False``, print the CPU & USER elapsed time.
    debug : bool
        If ``True``, default ``False``, print the debug parameters.
    quiet : bool
        If ``True``, default ``False``, no prints from C algorithm.
    params : str
        CLI parameter string used by ``vt.blockmatching`` method.

    Returns
    -------
    Trsf
        A transformation object.

    Raises
    ------
    TypeError
        If `floating_image` or `reference_image` are not ``SpatialImage`` instances.
        If `init_trsf` or `left_trsf` are not ``Trsf``.
        If `init_trsf` or `left_trsf` are not linear when the `method` is.
    ValueError
        If the transformation returned by ``vt.blockmatching`` is ``None``.

    Notes
    -----
    Definitions of available `estimator_type` values:

    - **wlts**: weighted least trimmed squares (default);
    - **lts**: least trimmed squares, see [lts]_ for more details;
    - **wls**: weighted least squares, see [wls]_ for more details;
    - **ls**: least squares, see [ls]_ for more details.

    Parameter `lts_deviation` is used in the formulae: ``threshold = average + lts_deviation * standard_deviation``

    References
    ----------
    .. [lts] https://en.wikipedia.org/wiki/Least_trimmed_squares
    .. [wls] https://en.wikipedia.org/wiki/Weighted_least_squares
    .. [ls] https://en.wikipedia.org/wiki/Least_squares

    See Also
    --------
    timagetk.third_party.vt_parser.BLOCKMATCHING_METHODS
    timagetk.third_party.vt_parser.DEF_BM_METHOD
    timagetk.third_party.vt_parser.DEF_BM_PARAMS
    timagetk.third_party.vt_parser.DEF_PY_LL
    timagetk.third_party.vt_parser.DEF_PY_HL
    timagetk.third_party.vt_parser.blockmatching_kwargs
    timagetk.third_party.vt_parser.general_kwargs
    timagetk.third_party.vt_parser.parallel_kwargs
    vt.blockmatching

    Examples
    --------
    >>> from timagetk.algorithms.blockmatching import blockmatching
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.trsf import apply_trsf
    >>> from timagetk.algorithms.trsf import create_trsf
    >>> from timagetk.algorithms.trsf import inv_trsf
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> ref_img = shared_data('flower_confocal', 0)

    >>> # EXAMPLE 1: Register an image obtained from a known affine transformation:
    >>> # Step 1: Manual creation of an affine transformation
    >>> trsf = create_trsf('random', trsf_type='affine', seed=2)
    >>> trsf.print()
    transformation type is AFFINE_3D
    transformation unit is REAL_UNIT
       0.963804180000000    0.162980220000000    0.300021320000000   -8.606880260000001
       0.046052760000000    0.974053870000000    0.191821950000000    8.992787580000000
      -0.013510030000000    0.166504470000000    1.178620960000000   -2.356495470000000
       0.000000000000000    0.000000000000000    0.000000000000000    1.000000000000000
    >>> # Step 2: Apply it to the reference image to generate an artificial multi-angles image
    >>> float_img = apply_trsf(ref_img, trsf)
    >>> fig = grayscale_imshow([ref_img, float_img], title=['Reference', 'Floating'], threshold=30)
    >>> # Step 3: Blockmatching registration
    >>> trsf_aff = blockmatching(float_img, ref_img, method="affine")
    >>> aff_img = apply_trsf(float_img, trsf_aff, template_img=ref_img, interpolation="linear")
    >>> fig = grayscale_imshow([ref_img, aff_img], title=['Reference', 'Registered floating'], threshold=30)
    >>> # Compare generated 'random' trsf with the one computed by blockmatching:
    >>> import numpy as np
    >>> np.allclose(trsf.copy_to_array(), inv_trsf(trsf_aff).copy_to_array(), rtol=0.2)
    >>> # Apply the computed trsf to float image and display rigid registration:
    >>> img_rig = apply_trsf(float_img, trsf_rig, template_img=ref_img)
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> img2plot = [float_img, ref_img, img_rig, ref_img]
    >>> mid_z = ref_img.get_shape('z')//2
    >>> img_titles = ["Floating view", "Reference view", "Registered view on reference", "Reference view"]
    >>> fig = grayscale_imshow(img2plot, suptitle="Rigid registration of multi-angle images", title=img_titles, val_range=[0, 255], max_per_line=2, threshold=40)

    >>> # EXAMPLE 2: Register two images that comes from a true multi-angles observation:
    >>> floating_image = shared_data('flower_multiangle', 1)
    >>> reference_image = shared_data('flower_multiangle', 0)
    >>> # - RIGID registration
    >>> trsf_rig = blockmatching(floating_image, reference_image, param=True)
    >>> img_rig = apply_trsf(floating_image, trsf_rig, template_img=reference_image, param=True)
    >>> # - Display rigid registration effect
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> img2plot = [floating_image, reference_image, img_rig, reference_image]
    >>> mid_z = reference_image.get_shape('z')//2
    >>> img_titles = ["Floating view", "Reference view", "Registered view on reference", "Reference view"]
    >>> fig = grayscale_imshow(img2plot, suptitle="Rigid registration of multi-angle images", title=img_titles, val_range=[0, 255], max_per_line=2, threshold=40)

    >>> # EXAMPLE 2.1: Register two images that comes from a true multi-angles observation:
    >>> from timagetk.algorithms.pointmatching import pointmatching
    >>> from timagetk.algorithms.trsf import inv_trsf
    >>> floating_image = shared_data('flower_multiangle', 1)
    >>> reference_image = shared_data('flower_multiangle', 0)
    >>> flo_pts = np.loadtxt(shared_data('p58_t0_floating_ldmk-01.txt', 'p58'))
    >>> ref_pts = np.loadtxt(shared_data('p58_t0_reference_ldmk-01.txt', 'p58'))
    >>> # Creates manual initialization transformations with `pointmatching` algorithm:
    >>> man_trsf = pointmatching(flo_pts, ref_pts)
    >>> img_man_reg = apply_trsf(floating_image, man_trsf, template_img=reference_image, param=True)
    >>> img2plot = [floating_image, reference_image, img_man_reg, reference_image]
    >>> img_titles = ["Floating view", "Reference view", "Manually registered view", "Reference view"]
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> fig = grayscale_imshow(img2plot, suptitle="Rigid registration of multi-angle images", title=img_titles, val_range=[0, 255], max_per_line=2, threshold=40)
    >>> # - RIGID registration
    >>> trsf_rig = blockmatching(floating_image, reference_image, init_trsf=man_trsf, param=True)
    >>> img_rig = apply_trsf(floating_image, trsf_rig, template_img=reference_image, param=True)
    >>> # - Display rigid registration effect
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> img2plot = [floating_image, reference_image, img_rig, reference_image]
    >>> mid_z = reference_image.get_shape('z')//2
    >>> img_titles = ["Floating view", "Reference view", "Registered view on reference", "Reference view"]
    >>> fig = grayscale_imshow(img2plot, suptitle="Rigid registration of multi-angle images", title=img_titles, val_range=[0, 255], max_per_line=2, threshold=40)

    >>> # EXAMPLE 3: Use deformable (non-linear) registration:
    >>> # - DEFORMABLE registration is initialized with rigid transformation:
    >>> params = '-trsf-type vectorfield -py-ll 2'
    >>> trsf_def = blockmatching(floating_image, reference_image, init_trsf=trsf_rig, params=params)
    >>> img_def = apply_trsf(floating_image, trsf_def, template_img=reference_image)
    >>> # - Display vectorfield registration effect
    >>> img2plot = [floating_image, reference_image, img_def, reference_image]
    >>> mid_z = reference_image.get_shape('z')//2
    >>> img_titles = ["t0", "t1", "Registered t0 on t1", "t1"]
    >>> fig = grayscale_imshow(img2plot, slice_id=mid_z, suptitle="Deformable registration of multi-angle images", title=img_titles, val_range=[0, 255], max_per_line=2)

    """
    # - Check if both input images are ``SpatialImage``:
    assert_spatial_image(floating_image, obj_name='floating_image')
    assert_spatial_image(reference_image, obj_name='reference_image')

    # - Make sure the provided initialisation `left_trsf` matrix is linear:
    msg = ""
    if left_trsf is not None:
        try:
            assert isinstance(left_trsf, Trsf)
        except AssertionError:
            raise TypeError(NOT_TRSF.format("left_trsf"))
        else:
            msg = " with a 'left' initialisation matrix"
        # If a linear blockmatching is required, make sure given `left_trsf` is linear:
        if method != 'vectorfield':
            try:
                assert not left_trsf.is_vectorfield()
            except AssertionError:
                raise TypeError(NOT_LINEAR.format('left_trsf'))
    # - Make sure the provided initialisation `init_trsf` matrix is linear:
    if init_trsf is not None:
        try:
            assert isinstance(init_trsf, Trsf)
        except AssertionError:
            raise TypeError(NOT_TRSF.format("init_trsf"))
        else:
            msg = " with an initialisation matrix"
        # If a linear blockmatching is required, make sure given `init_trsf` is linear:
        if method != 'vectorfield':
            try:
                assert init_trsf.is_rigid() or init_trsf.is_affine()
            except AssertionError:
                raise TypeError(NOT_LINEAR.format('init_trsf'))

    # - Automatically set the pyramid levels according to the given pyramid voxelsizes
    # TODO: put it in the blockmatching_kwargs ?
    pyramid_lowest_voxel = kwargs.pop("pyramid_lowest_voxel", None)
    pyramid_highest_voxel = kwargs.pop("pyramid_highest_voxel", None)

    if pyramid_lowest_voxel and pyramid_highest_voxel:
        py_ll, py_hl = set_pyramid_levels(reference_image,
                                          highest_voxelsize=pyramid_highest_voxel,
                                          lowest_voxelsize=pyramid_lowest_voxel)

        kwargs["pyramid_lowest_level"] = py_ll
        kwargs["pyramid_highest_level"] = py_hl
        log.info(f"Automatically set the pyramid level to [{py_ll}, {py_hl}] to match the given "
                 f"voxelsize: [{pyramid_lowest_voxel}, {pyramid_highest_voxel}] respectively.")

    # - Take care of extra parameters given as a string:
    extra_params = kwargs.pop("params", "")
    if extra_params != "" and not extra_params.startswith(" "):
        extra_params = " " + extra_params
    # - Parse blockmatching parameters:
    params, kwargs = blockmatching_kwargs(method, **kwargs)
    # - Parse general kwargs:
    params += general_kwargs(**kwargs)
    # - Parse parallelism kwargs:
    params += parallel_kwargs(**kwargs)
    # - Combine parsed parameters with extra parameters:
    params += extra_params
    log.debug(f"`vt.blockmatching` params: '{params}'")

    # - Performs blockmatching RIGID registration:
    log.info(f"{method.upper()} registration of '{floating_image.filename}' on '{reference_image.filename}'{msg}...")
    out_trsf = vt_blockmatching(floating_image.to_vt_image(), reference_image.to_vt_image(),
                                trsf_init=init_trsf, trsf_left=left_trsf,
                                params=params, **kwargs)

    if out_trsf is None:
        null_msg = "VT method 'blockmatching' returned NULL transformation!"
        raise ValueError(null_msg)
    else:
        return Trsf(out_trsf)

def set_pyramid_levels(image, highest_voxelsize=6., lowest_voxelsize=1., **kwargs):
    """Sets the number of pyramid levels according to the highest and lowest voxel sizes.

    Parameters
    ----------
    image : timagetk.SpatialImage
        The input image for which the pyramid levels are to be set.
    highest_voxelsize : float or list of floats
        The largest voxel size of the image in pyramid level strategy. Assuming an isotropic voxelsize if
        `highest_voxelsize` is a float.
    lowest_voxelsize : float or list of floats
        The smallest voxel size of the image in pyramid level strategy. Assuming an isotropic voxelsize if
        `lowest_voxelsize` is a float.

    Optional Parameters
    -------------------
    method : str, optional
        The comparison method used to select the best pyramid level. Either 'l2' or 'minmax" (default).
    min_img_size : int, optional
        The minimum allowable size for any dimension of the image at the lowest level of the pyramid.
        Defaults to 16 voxels.
    block_size : int, optional
        The minimum block size for any dimension in the pyramid. This parameter ensures that the image is not reduced
        below a meaningful size for analysis. Defaults to 5 voxels.

    Returns
    -------
    pyramid_lowest_level, pyramid_highest_level : int
        The pyramid levels corresponding to the lowest and highest voxel sizes.s.

    Examples
    --------
    >>> from timagetk.algorithms.blockmatching import set_pyramid_levels
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> img = shared_data('flower_confocal', 0)
    >>> py_ll, py_hl = set_pyramid_levels(img)
    >>> print(py_ll, py_hl)
    (1, 4)
    """

    if not isinstance(highest_voxelsize, list):
        highest_voxelsize = [highest_voxelsize] * image.ndim
    if not isinstance(lowest_voxelsize, list):
        lowest_voxelsize = [lowest_voxelsize] * image.ndim

    method = kwargs.get('method', 'minmax')
    min_img_size = kwargs.get('min_img_size', 16)
    block_size = kwargs.get('block_size', 5)

    pyramid_dict = compute_pyramid_infos(image, min_img_size=min_img_size, block_size=block_size, quiet=True)
    pyramid_voxelsizes = pyramid_dict['voxelsize']

    best_match_high = float('inf')
    best_match_low = float('inf')
    pyramid_highest_level = None
    pyramid_lowest_level = None

    for level, voxelsize in pyramid_voxelsizes.items():
        # Comparing voxel sizes between the current level and target voxel sizes
        if method == 'l2':
            # Option 1: Using L2 norm to calculate the overall difference
            # This is useful for assessing the total difference, but can sometimes obscure significant discrepancies
            # in a specific dimension.
            diff_high = sum((vs - hv) ** 2 for vs, hv in zip(voxelsize, highest_voxelsize))
            diff_low = sum((vs - lv) ** 2 for vs, lv in zip(voxelsize, lowest_voxelsize))
        elif method == 'minmax':
            # Option 2: Min-Max strategy to identify the largest absolute difference per dimension
            # It is particularly useful for avoiding large discrepancies in critical dimensions
            diff_high = max(abs(vs - hv) for vs, hv in zip(voxelsize, highest_voxelsize))
            diff_low = max(abs(vs - lv) for vs, lv in zip(voxelsize, lowest_voxelsize))
        else:
            raise ValueError('Wrong comparison method selected.')

        log.debug(f"Level {level}: diff_high={diff_high}, diff_low={diff_low}")

        if diff_high < best_match_high:
            best_match_high = diff_high
            pyramid_highest_level = level
            log.debug(f"New best match for highest voxelsize found at level {level} with difference {diff_high}")

        if diff_low < best_match_low:
            best_match_low = diff_low
            pyramid_lowest_level = level
            log.debug(f"New best match for lowest voxelsize found at level {level} with difference {diff_low}")

    return pyramid_lowest_level, pyramid_highest_level

def compute_pyramid_infos(image, min_img_size=16, block_size=5, quiet=False):
    """Computes the multi-level pyramid info (shape & voxelsize) of a given image.

    The pyramid strategy reduces the image size by half at each level until either the minimum image size (`min_img_size`)
    or the minimum block size (`block_size`) is reached. This ensures meaningful dimensions for analysis at all levels.
    The number of pyramid levels is determined based on the maximum dimension of the original image and the minimum
    allowable size, ensuring that the image is not reduced below a useful size for analysis.


    Shape & voxelsize of the images from this function should be identical to the `buildPyramidImage` CLI function from
    the `vt` library.
    See also the bal-pyramid.c file from the vt library.

    Parameters
    ----------
    image : timagetk.SpatialImage
        The input image for which the pyramid is to be computed.
    min_img_size : int, optional
        The minimum allowable size for any dimension of the image at the lowest level of the pyramid.
        Defaults to 16 voxels.
    block_size : int, optional
        The minimum block size for any dimension in the pyramid. This parameter ensures that the image is not reduced
        below a meaningful size for analysis. Defaults to 5 voxels.
    quiet : bool, optional
        If set to True, suppresses the printing of information about each pyramid level as it is computed. Defaults to False.

    Returns
    -------
    pyramid_info : dict
        A dictionary containing two keys: 'shape' and 'voxelsize'. Each key maps to another dictionary where keys
        are the pyramid levels (starting from 0) and values are NumPy arrays representing the dimensions ('shape')
        or voxel sizes ('voxelsize') at each level.

    Examples
    --------
    >>> from timagetk.algorithms.blockmatching import compute_pyramid_infos
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> img = shared_data('flower_confocal', 0)
    >>> pyramid_info = compute_pyramid_infos(img)

    Notes
    -----
    - The function iteratively halves the dimensions of the previous level, adjusting voxel sizes accordingly,
      until constraints (`min_img_size` or `block_size`) prevent further reduction.
    - It calculates the necessary number of levels based on the original image's maximum dimension and constraints,
      ensuring no level is reduced below a meaningful size for analysis.
    """

    image_shape = np.array(image.get_shape())  # ZYX order
    image_vx = np.array(image.get_voxelsize())  # ZYX order

    # Calculate the number of levels needed based on the minimum size & block_size.
    # This determines how many times the image can be reduced before reaching the minimum size or block size.
    max_dim = np.max(image.get_shape())
    n_levels = int(np.ceil(np.log2(max_dim / min(min_img_size, block_size))))

    # Get the image size along all dimensions using the pyramidal strategies.
    # This calculates the expected shape of the image at each level of the pyramid.
    py_shape = fill_pyramid_dimensions(image_shape, n_levels + 1)
    max_dim = np.max(py_shape[0, :])

    pyramid_info = {'shape': {}, 'voxelsize': {}}
    for level in range(n_levels + 1):
        # Find indices where dimensions do not exceed max_dim for this level.
        # These indices are used to select the appropriate dimensions from the py_shape array for the current level.
        iz, iy, ix = (np.argmax(py_shape[:, dim] <= max_dim) for dim in range(3))

        current_py_shape = np.array([py_shape[iz, 0], py_shape[iy, 1], py_shape[ix, 2]])
        current_py_vx = (image_vx * image_shape) / current_py_shape

        if np.any(current_py_shape < min_img_size) or np.any(current_py_shape < block_size):
            if not quiet:
                log.info(f"Stopping at level {level} due to minimal dimension or block size.")
            break

        if not quiet:
            log.info(f"Level {level} : {current_py_shape} ({current_py_vx})")

        # Adjust max_dim for the next level, if necessary.
        # This step recalculates max_dim based on the dimensions available for the next level,
        # ensuring that the reduction process considers the actual dimensions at each step.
        if level < n_levels:
            next_dims = (py_shape[min(iz + 1, len(py_shape) - 1), 0],
                         py_shape[min(iy + 1, len(py_shape) - 1), 1],
                         py_shape[min(ix + 1, len(py_shape) - 1), 2])
            max_dim = np.max(next_dims)

        pyramid_info['shape'][level] = current_py_shape
        pyramid_info['voxelsize'][level] = current_py_vx

    return pyramid_info

def fill_pyramid_dimensions(dims, levels):
    """Calculate the dimensions for each level of an image pyramid.

    This function computes the dimensions for each level of a pyramid given the original image dimensions and the
    desired number of pyramid levels. It ensures that each subsequent level is half the size of the previous level,
    rounding down to the nearest integer value. If a dimension's size becomes less than 1, it is set to 1, ensuring
    that every level has valid dimensions.

    Parameters
    ----------
    dims : array_like
        The dimensions of the original image in ZYX order (depth, height, width).
    levels : int
        The total number of levels in the pyramid including the original image level.

    Returns
    -------
    numpy.ndarray
        A 2D array where each row represents the dimensions (ZYX order) of the image at a specific pyramid level.
        The first row corresponds to the original image dimensions, and each subsequent row represents the next level
        in the pyramid.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.algorithms.blockmatching import fill_pyramid_dimensions
    >>> original_dims = np.array([256, 256, 256])
    >>> levels = 4
    >>> pyramid_dims = fill_pyramid_dimensions(original_dims, levels)
    >>> print(pyramid_dims)
    [[256 256 256]
     [128 128 128]
     [ 64  64  64]
     [ 32  32  32]]

    Notes
    -----
    - The function calculates the dimensions for each level by iteratively halving the dimensions of the previous level.
    - If the calculation results in a dimension size less than 1, that dimension size is set to 1. This ensures that even at high pyramid levels, where the image might be significantly reduced in size, each dimension remains valid.
    - The dimensions are rounded down to the nearest integer at each level to ensure that the resulting dimensions are integers.

    See Also
    --------
    compute_pyramid : Function that uses the dimensions calculated by this function to build an image pyramid.

    """
    if levels <= 0:
        return np.array([])

    d = np.zeros((3, levels), dtype=int)
    d[:, 0] = dims

    if levels <= 1:
        return d

    powers = np.log2(dims)

    for i in range(3):
        if powers[i] - int(powers[i]) < 1e-16:
            d[i, 1] = d[i, 0] // 2
        else:
            d[i, 1] = int(2 ** int(powers[i]))

        for l in range(2, levels):
            d[i, l] = d[i, l - 1] // 2
            if d[i, l] < 1:
                d[i, l] = 1

    return d.T
