#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Image Transformation Algorithms Module.

This module is part of the TimageTK library and provides a collection of robust
functions and algorithms for working with image transformations.
It allows for the creation, inversion, application, composition, and averaging of image
transformation matrices.
These utilities are particularly useful for image processing pipelines that involve intensity and labelled images.

Key Features

- **Creation of Transformations**: Generate identity, random, or sinusoidal
  transformations with support for rigid, affine, and vector field types.
- **Transformation Inversion**: Invert transformation matrices to enable reverse
  transformations with customized options.
- **Apply Transformations**: Apply transformations to images with multiple
  interpolation methods (e.g., nearest, linear, cubic, cell-based) suitable for intensity
  and labelled images.
- **Compose Transformations**: Combine multiple transformations into a single
  composite transformation.
- **Transformation Averaging**: Compute the average of multiple transformation
  matrices with options for robust methods.
- **Support for Multi-dimensional and Multi-channel Images**: Works with 2D, 3D,
  intensity, labelled, and multi-channel images within a flexible interface.

Usage Examples

Create an identity transformation and apply it to an image:

```python
from timagetk.algorithms.trsf import create_trsf, apply_trsf
from timagetk.io import imread
# Load an example image
image = imread("example_image.inr.gz")
# Create an identity transformation
identity_trsf = create_trsf()
# Apply the transformation to the image
transformed_image = apply_trsf(image, identity_trsf)
print(transformed_image.get_voxelsize())
```

Compose multiple transformations:

```python
from timagetk.algorithms.trsf import compose_trsf, create_trsf
trsf1 = create_trsf('random', trsf_type='rigid')
trsf2 = create_trsf('random', trsf_type='affine')
composite_trsf = compose_trsf([trsf1, trsf2])
composite_trsf.print()
```

Apply a random transformation with interpolation:

```python
from timagetk.algorithms.trsf import apply_trsf, create_trsf
from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image
image = example_layered_sphere_labelled_image([50, 50, 50])
random_trsf = create_trsf('random', trsf_type='affine', seed=42)
transformed_image = apply_trsf(image, random_trsf, interpolation='nearest')
```

Average multiple transformations:

```python
from timagetk.algorithms.trsf import trsfs_averaging, create_trsf
trsf1 = create_trsf('random', trsf_type='affine', seed=1)
trsf2 = create_trsf('random', trsf_type='affine', seed=2)
averaged_trsf = trsfs_averaging([trsf1, trsf2], method="mean")
averaged_trsf.print()
```
"""

import numpy as np
from timagetk.bin.logger import get_logger
from timagetk.components.image import get_image_class
from timagetk.components.spatial_image import SpatialImage
from timagetk.components.trsf import DEF_TRSF_TYPE
from timagetk.components.trsf import Trsf
from timagetk.tasks.decorators import int16_wrapper
from timagetk.tasks.decorators import multichannel_wrapper
from timagetk.third_party.vt_converter import vt_to_spatial_image
from timagetk.third_party.vt_image import check_vt_image_not_null
from timagetk.third_party.vt_parser import DEF_CREATE_TRSF
from timagetk.third_party.vt_parser import apply_trsf_kwargs
from timagetk.third_party.vt_parser import create_trsf_kwargs
from timagetk.third_party.vt_parser import general_kwargs
from timagetk.third_party.vt_parser import inv_trsf_kwargs
from timagetk.third_party.vt_parser import parallel_kwargs
from timagetk.third_party.vt_parser import template_kwargs
from timagetk.third_party.vt_parser import trsfs_averaging_kwargs
from timagetk.util import check_type
from timagetk.util import same_type
from vt import apply_trsf as vt_apply_trsf
from vt import compose_trsf as vt_compose_trsf
from vt import create_trsf as vt_create_trsf
from vt import inv_trsf as vt_inv_trsf
from vt import mean_trsfs as vt_mean_trsfs
from vt import vtImage
from vt.image import Image

log = get_logger(__name__)

#: The null transformation
NULL_TRSF = np.zeros([4, 4], dtype=float)
NULL_TRSF[3, 3] = 1.


def inv_trsf(trsf, template_img=None, **kwargs):
    """Inversion of a transformation matrix.

    Parameters
    ----------
    trsf : timagetk.Trsf
        Transformation object to invert.
    template_img : timagetk.SpatialImage, optional.
        Control output transformation geometry, used only with vectorfield transformations.

    Other Parameters
    ----------------
    error : float
        Absolute error (in real world unit) to determine convergence.
    iteration : int
        Maximal number of iterations to reach convergence.
    params : str
        C style command line parameters used by ``vt.inv_trsf`` method, ``DEF_INV_TRSF`` by default.

    Returns
    -------
    timagetk.Trsf
        The inverted transformation object.

    Raises
    ------
    TypeError
        If ``trsf`` is not a ``Trsf``.
        If ``template_img`` is not a ``SpatialImage``, if not ``None``.

    See Also
    --------
    timagetk.third_party.vt_parser.template_kwargs
    timagetk.third_party.vt_parser.inv_trsf_kwargs
    timagetk.third_party.vt_parser.general_kwargs
    timagetk.third_party.vt_parser.parallel_kwargs

    Example
    -------
    >>> from timagetk.algorithms.trsf import create_trsf
    >>> from timagetk.algorithms.trsf import inv_trsf
    >>> trsf_in = create_trsf('random', trsf_type='rigid')
    >>> trsf_in.print()
    >>> trsf_out = inv_trsf(trsf_in)
    >>> type(trsf_out)
    >>> trsf_out.print()

    >>> trsf_in = create_trsf('random')
    >>> trsf_out = inv_trsf(trsf_in)
    >>> type(trsf_out)
    >>> trsf_out.print()

    >>> from timagetk.array_util import random_spatial_image
    >>> img = random_spatial_image([15, 40, 40], voxelsize=[0.5, 0.21, 0.21])
    >>> trsf = create_trsf('sinus', template_img=img, trsf_type='vectorfield')
    >>> trsf_out = inv_trsf(trsf)

    """
    check_type(trsf, 'trsf', Trsf)

    # - Take care of extra parameters given as a string:
    extra_params = kwargs.pop("params", "")
    if not extra_params.startswith(" "):
        extra_params = " " + extra_params
    # - Parse inv_trsf parameters:
    params, kwargs = inv_trsf_kwargs(**kwargs)
    # - If a template image is given, make sure it's a `SpatialImage` or a `vt.vtImage`:
    if template_img is not None:
        assert isinstance(template_img, (SpatialImage, Image, vtImage))
        if isinstance(template_img, SpatialImage):
            template_img = template_img.to_vt_image()

    # - Parse general kwargs:
    params += general_kwargs(**kwargs)
    # - Parse parallelism kwargs:
    params += parallel_kwargs(**kwargs)
    # - Combine parsed parameters with extra parameters:
    params += extra_params
    log.debug(f"`vt.inv_trsf` params: '{params}'")

    return Trsf(vt_inv_trsf(trsf, template_image=template_img, params=params))


@multichannel_wrapper
@int16_wrapper
def apply_trsf(image, trsf=None, template_img=None, interpolation=None, **kwargs):
    """Apply a transformation to an image.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.LabelledImage
        Image to transform.
    trsf : timagetk.Trsf, optional
        Input transformation, default is identity, goes from the result image towards the input image.
    template_img : timagetk.SpatialImage, optional
        Template image used for the dimensions & voxelsize of the output transformation vectorfield.
    interpolation : {"nearest", "linear", "cspline", "cellbased"}
        Interpolation method to use. See the "Notes" section for a detailled explanations.

    Other Parameters
    ----------------
    isotropic_voxel : float
        If set, ignore given ``trsf`` & ``template_img`` and performs isometric resampling to given voxelsize.
    cell_based_sigma : float
        Required when using "cellbased" interpolation method, sigma for cell-based interpolation. In voxel units!
    params : str
        CLI parameter string used by ``vt.apply_trsf`` method.

    Returns
    -------
    timagetk.SpatialImage or timagetk.LabelledImage or timagetk.MultiChannelImage or timagetk.TissueImage2D or timagetk.TissueImage3D
        The transformed image.

    Raises
    ------
    TypeError
        If ``image`` is not a ``SpatialImage``.
        If ``trsf`` is not a ``Trsf``, if not ``None``.
        If ``template_img`` is not a ``SpatialImage``, if not ``None``.

    See Also
    --------
    timagetk.third_party.vt_parser.GRAY_INTERPOLATION_METHODS
    timagetk.third_party.vt_parser.LABEL_INTERPOLATION_METHODS
    timagetk.third_party.vt_parser.DEF_GRAY_INTERP_METHOD
    timagetk.third_party.vt_parser.DEF_LABEL_INTERP_METHOD
    timagetk.third_party.vt_parser.apply_trsf_kwargs
    timagetk.third_party.vt_parser.general_kwargs
    timagetk.third_party.vt_parser.parallel_kwargs

    Notes
    -----
    To apply a transformation to a segmented image, use '-cellbased' in
    ``params`` instead of default ``DEF_APPLY_TRSF``.

    Interpolation methods definitions:

     - **nearest**: nearest neighbor interpolation (for binary or label images)
     - **linear**: bi- or tri-linear interpolation (for intensity images)
     - **cspline**: cubic spline (for intensity images)
     - **cellbased**: cell based interpolation (for label images)

    Example
    -------
    >>> from timagetk.algorithms.trsf import apply_trsf
    >>> from timagetk.algorithms.trsf import create_trsf
    >>> from timagetk.io import imread
    >>> from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> # EXAMPLE #1 - Isotropic resampling of intensity image:
    >>> img_url = "https://zenodo.org/record/3737630/files/sphere_membrane_t0.inr.gz"
    >>> image = imread(img_url)
    >>> print(image.get_voxelsize())
    [0.25, 0.25, 0.25]
    >>> print(image.get_shape())
    [225, 225, 225]
    >>> image_iso = apply_trsf(image, isotropic_voxel=0.5)
    >>> print(image_iso.get_voxelsize())
    [0.5, 0.5, 0.5]
    >>> print(image_iso.get_shape())
    [113, 113, 113]

    >>> # EXAMPLE #2 - Apply a random affine transformation, using 'linear' interpolation, to intensity image:
    >>> trsf = create_trsf('random', seed=5)  # we use a seed to generate the same random array for example purposes
    >>> out_image = apply_trsf(image, trsf, interpolation='linear')
    >>> mid_zsl = image.get_shape('z')//2
    >>> fig = grayscale_imshow([image, out_image], mid_zsl, title=['Original', 'Transformed'], suptitle="Random affine transformation", val_range="auto")

    >>> # EXAMPLE #3 - Apply a random affine transformation, using 'nearest' and 'cellbased' interpolation, to labelled image:
    >>> from timagetk import TissueImage3D
    >>> url = 'https://zenodo.org/record/3737630/files/sphere_membrane_t0_seg.inr.gz'
    >>> image = imread(url, TissueImage3D, background=1, not_a_label=0)
    >>> out_nearest = apply_trsf(image, trsf, interpolation='nearest')
    >>> out_cellbased = apply_trsf(image, trsf, interpolation='cellbased', cell_based_sigma=2)
    >>> mid_zsl = image.get_shape('z')//2
    >>> fig = grayscale_imshow([image, out_nearest, out_cellbased], mid_zsl, title=['Original', 'Nearest', f'Cell based'], suptitle="Random affine transformation", cmap='viridis', val_range="auto")

    >>> # EXAMPLE #4 - Apply a random vectorfield transformation, using 'cellbased' interpolation, to labelled image:
    >>> vf_trsf = create_trsf('sinus', template_img=image, trsf_type='vectorfield')
    >>> out_cellbased = apply_trsf(image, vf_trsf, interpolation='cellbased', cell_based_sigma=0.2)
    >>> fig = grayscale_imshow([image, out_cellbased], mid_zsl, title=['Original', f'Cell based'], suptitle="Random non-linar transformation", cmap='viridis', val_range="auto")

    >>> # EXAMPLE #5 - Isotropic resampling of multichannel intensity image:
    >>> img_url = "https://zenodo.org/record/3737795/files/qDII-CLV3-PIN1-PI-E35-LD-SAM4.czi"
    >>> ch_names = ["DII-VENUS-N7", "pPIN1:PIN1-GFP", "Propidium Iodide", "pRPS5a:TagBFP-SV40", "pCLV3:mCherry-N7"]
    >>> image = imread(img_url, channel_names=ch_names)
    >>> image_iso = apply_trsf(image, isotropic_voxel=0.5)

    >>> # EXAMPLE #6 - Isotropic resampling of 16bit labelled image:
    >>> image = example_layered_sphere_labelled_image(voxelsize=[1.0, 0.5, 0.5], dtype='int16')
    >>> image_iso = apply_trsf(image, isotropic_voxel=0.5, interpolation='cellbased', cell_based_sigma=2)

    """
    from timagetk.components.image import get_image_attributes
    attr = get_image_attributes(image, exclude=["shape", "voxelsize"], extra=['filename'])
    Image = get_image_class(image)

    # - If a template image is given, make sure it's a `SpatialImage` or a `vt.vtImage`:
    if template_img is not None:
        assert isinstance(template_img, (SpatialImage, Image, vtImage))
        if isinstance(template_img, SpatialImage):
            attr.update(get_image_attributes(image, exclude=["shape", "voxelsize"]))
            template_img = template_img.to_vt_image()

    # - If a transformation is given, make sure it's a `Trsf` or a `str`:
    if trsf is not None:
        assert isinstance(trsf, (str, Trsf))
        if isinstance(trsf, str):
            trsf = create_trsf(trsf)
    else:
        # Create an identity transformation object by default
        trsf = create_trsf("identity", trsf_type='rigid')

    # To performs isometric resampling, trsf & template_img should be ignored:
    if "isotropic_voxel" in kwargs.keys():
        template_img = None

    if trsf.is_vectorfield() and template_img is not None:
        template_img = None
        log.warning('Can not apply vectorfield transformation with template image!')
        log.info('Applying vectorfield transformation WITHOUT template image!')

    # - Take care of extra parameters given as a string:
    extra_params = kwargs.pop("params", "")
    if not extra_params.startswith(" "):
        extra_params = " " + extra_params
    # - Parse apply_trsf parameters:
    params, kwargs = apply_trsf_kwargs(image, interpolation, **kwargs)
    # - Parse general kwargs:
    params += general_kwargs(**kwargs)
    # - Parse parallelism kwargs:
    params += parallel_kwargs(**kwargs)

    # - Combine parsed parameters with extra parameters:
    params += extra_params
    log.debug(f"`vt.apply_trsf` params: '{params}'")

    vt_img = image.to_vt_image()
    out_vt_img = vt_to_spatial_image(vt_apply_trsf(vt_img, trsf, template_img, params=params, **kwargs))
    check_vt_image_not_null(out_vt_img, 'apply_trsf')

    # Re-apply detected input `image` type to obtained image:
    out_img = Image(out_vt_img, voxelsize=out_vt_img.voxelsize, **attr)
    # - Since 'apply_trsf' only works on 3D images, it might have converted it to 3D:
    if 1 in out_img.shape:
        out_img = out_img.to_2d()
    # add2md(out_img)
    return out_img


def compose_trsf(list_trsf, template_img=None, **kwargs):
    """Composition of transformations.

    Parameters
    ----------
    list_trsf : list[timagetk.Trsf] or list[str]
        List of transformations to compose.
    template_img : timagetk.SpatialImage or timagetk.LabelledImage, optional
        The template to use, required for vectorfields composition.

    Other Parameters
    ----------------
    params : str
        CLI parameter string used by ``vt.compose_trsf`` method.

    Returns
    -------
    timagetk.Trsf
        Composition of given transformations.

    Raises
    ------
    TypeError
        If ``list_trsf`` is not a ``list`` of ``Trsf``.
        If ``template_img`` is not a ``SpatialImage``, when specified.

    See Also
    --------
    timagetk.third_party.vt_parser.general_kwargs
    timagetk.third_party.vt_parser.parallel_kwargs

    Examples
    --------
    >>> from timagetk.algorithms.trsf import compose_trsf
    >>> from timagetk.algorithms.trsf import create_trsf
    >>> trsf_1 = create_trsf('random', trsf_type='rigid')
    >>> trsf_2 = create_trsf('random', trsf_type='affine')

    >>> comp_trsf = compose_trsf([trsf_1, trsf_2])
    >>> comp_trsf.print()

    >>> from timagetk.array_util import random_spatial_image
    >>> img = random_spatial_image([15, 40, 40], voxelsize=[0.5, 0.21, 0.21])
    >>> trsf_3 = create_trsf('sinus', template_img=img)
    >>> comp_trsf = compose_trsf([trsf_1, trsf_2, trsf_3])
    >>> comp_trsf.print()

    """
    # Check that the list of transformations is not empty:
    if not list_trsf:
        raise ValueError("The list_trsf parameter must not be empty.")

    # Check that the list of transformations is a list of Trsf:
    check_type(list_trsf, 'list_trsf', list)
    for n, trsf in enumerate(list_trsf):
        check_type(trsf, 'trsf[{}]'.format(n), Trsf)

    params = ""
    if template_img is not None:
        assert isinstance(template_img, SpatialImage)
        # compose_trsf take template shape as XYZ
        sh = template_img.shape[::-1]
        # compose_trsf take template voxelsize as XYZ
        vxs = template_img.voxelsize[::-1]
        params += template_kwargs(sh, vxs, **kwargs)

    # - Take care of extra parameters given as a string:
    extra_params = kwargs.pop("params", "")
    if not extra_params.startswith(" "):
        extra_params = " " + extra_params
    # - Parse general kwargs:
    params += general_kwargs(**kwargs)
    # - Parse parallelism kwargs:
    params += parallel_kwargs(**kwargs)
    # - Combine parsed parameters with extra parameters:
    params += extra_params
    log.debug(f"`vt.compose_trsf` params: '{params}'")

    return Trsf(vt_compose_trsf(list_trsf, params))


def trsfs_averaging(list_trsf, method="mean", **kwargs):
    """Transformations averaging algorithm.

    Parameters
    ----------
    list_trsf : list[timagetk.Trsf] or list[str]
        List of transformations, as ``Trsf`` instances or file paths to ``Trsf`` files.
    method : {'mean', 'robust-mean'}
        Transformations averaging method to use.

    Other Parameters
    ----------------
    trsf_type : {"rigid", "affine", "vectorfield"}
        Type of transformation to create.
    fluid_sigma : float or list of float
        Sigma for fluid regularization, *i.e.* field interpolation and regularization for pairings (only for vector field)
    estimator_type : {"wlts", "lts", "wls", "ls"}
        Transformation estimator. See the "Notes" section for a detailled explanations.
    lts_fraction : float
        If defined, set the fraction of pairs that are kept, used with trimmed estimations.
    lts_deviation : float
        If defined, set the threshold to discard pairings (see notes), used with trimmed estimations.
    lts_iterations : int
        If defined, set the maximal number of iterations, used with trimmed estimations.

    Returns
    -------
    timagetk.Trsf
        The average transformation.

    See Also
    --------
    timagetk.third_party.vt_parser.trsfs_averaging_kwargs
    timagetk.third_party.vt_parser.general_kwargs
    timagetk.third_party.vt_parser.parallel_kwargs

    TODO: create and use temporary filename with `out_file_path` kwarg when using list of str as input then load it to return Trsf

    Example
    -------
    >>> from timagetk.algorithms.trsf import trsfs_averaging
    >>> from timagetk.algorithms.trsf import create_trsf
    >>> trsf_1 = create_trsf('random', trsf_type='affine', seed=1)
    >>> trsf_2 = create_trsf('random', trsf_type='affine', seed=2)
    >>> trsf_out = trsfs_averaging([trsf_1, trsf_2])
    >>> trsf_out.print()

    >>> trsf_1 = create_trsf('random', trsf_type='rigid', seed=1)
    >>> trsf_2 = create_trsf('random', trsf_type='affine', seed=2)
    >>> trsf_out = trsfs_averaging([trsf_1, trsf_2])
    >>> trsf_out.print()

    >>> from timagetk.array_util import random_spatial_image
    >>> img = random_spatial_image([15, 40, 40], voxelsize=[0.5, 0.21, 0.21])
    >>> trsf_1 = create_trsf('sinus', trsf_type='vectorfield', template_img=img)
    >>> trsf_2 = create_trsf('sinus', trsf_type='vectorfield', template_img=img)
    >>> trsf_out = trsfs_averaging([trsf_1, trsf_2], trsf_type='vectorfield', fluid_sigma=1.5)

    >>> # Use files instead of Trsf:
    >>> from tempfile import NamedTemporaryFile as Tmp
    >>> trsf_fname_1 = Tmp().name + '.trsf'
    >>> trsf_fname_2 = Tmp().name + '.trsf'
    >>> from timagetk.algorithms.trsf import create_trsf
    >>> create_trsf('random', trsf_type='affine', seed=1).write(trsf_fname_1)
    >>> create_trsf('random', trsf_type='affine', seed=2).write(trsf_fname_2)
    >>> from vt import mean_trsfs
    >>> from tempfile import gettempdir
    >>> from os.path import join
    >>> mean_fname = join(gettempdir(), 'mean.trsf')
    >>> mean_trsfs([trsf_fname_1, trsf_fname_2], out_file_path=mean_fname, params="-trsf-type affine -mean")
    >>> from timagetk import Trsf
    >>> mean_trsf = Trsf(mean_fname)
    >>> print(mean_trsf.get_array())

    >>> # Use files instead of Trsf:
    >>> from tempfile import NamedTemporaryFile as Tmp
    >>> trsf_fname_1 = Tmp().name + '.trsf'
    >>> trsf_fname_2 = Tmp().name + '.trsf'
    >>> from timagetk.algorithms.trsf import create_trsf
    >>> from timagetk.array_util import random_spatial_image
    >>> img = random_spatial_image([15, 40, 40], voxelsize=[0.5, 0.21, 0.21])
    >>> trsf_1 = create_trsf('sinus', template_img=img, trsf_type='vectorfield')
    >>> trsf_1.write(trsf_fname_1)
    >>> trsf_2 = create_trsf('sinus', template_img=img, trsf_type='vectorfield')
    >>> trsf_2.write(trsf_fname_2)
    >>> from vt import mean_trsfs
    >>> from tempfile import gettempdir
    >>> from os.path import join
    >>> mean_fname = join(gettempdir(), 'mean.trsf')
    >>> mean_trsfs([trsf_fname_1, trsf_fname_2], out_file_path=mean_fname, params="-trsf-type vectorfield -mean")
    >>> from timagetk import Trsf
    >>> mean_trsf = Trsf(mean_fname)
    >>> mean_trsf.print()

    >>> trsf_out = trsfs_averaging([trsf_fname_1, trsf_fname_2])
    >>> trsf_out.print()

    """
    check_type(list_trsf, 'list_trsf', list)

    # - Check for stupid case with less than two transformations:
    try:
        assert len(list_trsf) >= 2
    except AssertionError:
        msg = "Input `list_trsf` must contains a minimum of two objects!"
        msg += f" Got {len(list_trsf)}!"
        raise TypeError(msg)

    # Transformations should be all ``Trsf`` or  all ``str``
    try:
        assert same_type(list_trsf, Trsf) or same_type(list_trsf, str)
    except AssertionError:
        raise TypeError("All input transformations must be Trsf instances.")

    if isinstance(list_trsf[0], Trsf):
        # Try to determine the trsf-type of returned Trsf:
        if all(trsf.is_linear() for trsf in list_trsf):
            auto_trsf_type = "affine"
        elif all(trsf.is_vectorfield() for trsf in list_trsf):
            auto_trsf_type = "affine"
        else:
            raise TypeError(f"There is a mixture of transformation types: {[trsf.get_type() for trsf in list_trsf]}")
    else:
        auto_trsf_type = ""

    # Use automatically detected transformation type if no input for `trsf_type`:
    trsf_type = kwargs.pop('trsf_type', auto_trsf_type)

    # - Take care of extra parameters given as a string:
    extra_params = kwargs.pop("params", "")
    if not extra_params.startswith(" "):
        extra_params = " " + extra_params

    # - Parse blockmatching parameters:
    params, kwargs = trsfs_averaging_kwargs(method, trsf_type, **kwargs)
    # - Parse general kwargs:
    params += general_kwargs(**kwargs)
    # - Parse parallelism kwargs:
    params += parallel_kwargs(**kwargs)
    # - Combine parsed parameters with extra parameters:
    params += extra_params
    log.debug(f"`vt.mean_trsf` params: '{params}'")

    return Trsf(vt_mean_trsfs(list_trsf, params))


def create_trsf(trsf=DEF_CREATE_TRSF, template_img=None, trsf_type=DEF_TRSF_TYPE, **kwargs):
    """Create a transformation, can be *identity*, *random* or *sinus*.

    Parameters
    ----------
    trsf : {"identity", "random", "sinus"}, optional
        Name of the transformation to create, default is *identity*.
        Use *random* with linear trnasformations.
        Use *sinus* with non-linear transformation
    template_img : timagetk.SpatialImage or timagetk.LabelledImage, optional
        Image used to defines the template shape and voxelsize, required with non-linear transformations (vectorfield).
    trsf_type : {"rigid", "affine", "vectorfield"}, optional
        Type of transformation to create, default is ``DEF_TRSF_TYPE``.

    Other Parameters
    ----------------
    seed : int
        The seed to use for the random generator of linear transformations.
    angle_range : [float, float]
        Angle range of the linear tranformation, expressed in radian.
        Limited to "rigid" & "affine" tranformations.
    translation_range : [float, float]
        Angle range of the linear tranformation, expressed in ??? FIXME.
        Limited to "rigid" & "affine" tranformations.
    shear_range : [float, float]
        Shear range of the linear tranformation, expressed in ??? FIXME.
        Limited to "affine" tranformations.
    scale_range : [float, float]
        Scale range of the linear tranformation, expressed in ??? FIXME.
        Limited to "affine" tranformations.
    sinusoid_amplitude : [float, [float, [float]]]
        Sinusoid amplitude of the non-linear tranformation, expressed in radian.
        Limited to "vectorfield" tranformations.
    sinusoid_period : [float, [float, [float]]]
        Sinusoid period of the non-linear tranformation, expressed in radian.
        Limited to "vectorfield" tranformations.
    params : str
        CLI parameter string used by ``vt.create_trsf`` method.

    Returns
    -------
    timagetk.Trsf
        The created transformation object.

    Raises
    ------
    TypeError
        If ``trsf`` is not a ``str``.
        If ``trsf_type`` is not a ``str``.
        If ``template_img`` is not a ``SpatialImage``, if not ``None``.
    ValueError
        If ``trsf`` is not in ``CREATE_TRSF``.
        If ``trsf_type`` is not in ``TRSF_TYPE``.

    See Also
    --------
    timagetk.third_party.vt_parser.CREATE_TRSF
    timagetk.third_party.vt_parser.TRSF_TYPE
    timagetk.third_party.vt_parser.DEF_CREATE_TRSF
    timagetk.third_party.vt_parser.DEF_TRSF_TYPE
    timagetk.third_party.vt_parser.create_trsf_kwargs
    timagetk.third_party.vt_parser.general_kwargs
    timagetk.third_party.vt_parser.parallel_kwargs

    Notes
    -----
    To create "random" linear transformations, use 'random'.

    To create "random" non-linear transformations, use 'sinus' and provide a template image.

    Examples
    --------
    >>> from timagetk.algorithms.trsf import create_trsf
    >>> # Example #1 - Create identity matrix, *i.e.* no-deformation:
    >>> identity_trsf = create_trsf()
    >>> identity_trsf.print()
    transformation type is AFFINE_3D
    transformation unit is REAL_UNIT
       1.000000000000000    0.000000000000000    0.000000000000000    0.000000000000000
       0.000000000000000    1.000000000000000    0.000000000000000    0.000000000000000
       0.000000000000000    0.000000000000000    1.000000000000000    0.000000000000000
       0.000000000000000    0.000000000000000    0.000000000000000    1.000000000000000

    >>> # Example #2 - Create random AFFINE transformation matrix:
    >>> identity_trsf = create_trsf('random', trsf_type='affine', seed=1)
    >>> identity_trsf.print()
    transformation type is AFFINE_3D
    transformation unit is REAL_UNIT
       0.798876800000000   -0.721781810000000    0.074926290000000    0.268018200000000
       0.315911350000000    0.308944260000000   -0.604184840000000    9.044594500000001
       0.702414430000000    0.746748760000000    0.664103850000000    8.323901360000001
       0.000000000000000    0.000000000000000    0.000000000000000    1.000000000000000
    >>> identity_trsf.is_rigid()
    False
    >>> identity_trsf.is_affine()
    True

    >>> # Example #3 - Create random RIGID transformation matrix:
    >>> trsf = create_trsf('random', trsf_type='rigid', angle_range=[0., 0.25], translation_range=[0., 1.5], seed=1)
    >>> trsf.print()
    transformation type is AFFINE_3D
    transformation unit is REAL_UNIT
       0.991243410000000   -0.126818630000000   -0.036790380000000    1.367471040000000
       0.119926610000000    0.981213850000000   -0.151119090000000    0.296327050000000
       0.055263950000000    0.145383660000000    0.987830700000000    0.502834130000000
       0.000000000000000    0.000000000000000    0.000000000000000    1.000000000000000

    >>> # Example #4 - Create random VECTORFIELD transformation:
    >>> from timagetk.array_util import random_spatial_image
    >>> img = random_spatial_image([15, 40, 40], voxelsize=[0.5, 0.21, 0.21])
    >>> trsf = create_trsf('sinus', template_img=img, trsf_type='vectorfield')
    >>> trsf.print()

    """
    if isinstance(template_img, SpatialImage):
        template_img = template_img.to_vt_image()

    if trsf == 'sinus':
        trsf_type = "vectorfield"
    # - Specific options for vectorfield based transformations:
    if trsf_type == "vectorfield":
        err_msg = "To create a 'vectorfield' transformation a template image is required!"
        try:
            assert template_img is not None
        except AssertionError:
            raise ValueError(err_msg)
        # - Add 'ndim' as keyword argument (used by ``create_trsf_kwargs``):
        kwargs.update({"ndim": template_img.copy_to_array().ndim})

    # - Add 'trsf_type' as keyword argument (used by ``create_trsf_kwargs``):
    kwargs.update({"trsf_type": trsf_type})
    # - Parse create_trsf parameters:
    params, kwargs = create_trsf_kwargs(trsf, **kwargs)
    # - Parse general kwargs:
    params += general_kwargs(**kwargs)
    # - Parse parallelism kwargs:
    params += parallel_kwargs(**kwargs)

    # - Take care of extra parameters given as a string:
    extra_params = kwargs.pop("params", "")
    if not extra_params.startswith(" "):
        extra_params = " " + extra_params
    params += extra_params
    log.debug(f"`vt.create_trsf` params: '{params}'")

    # TODO: use `image_fixedpoint` param with image 'origin' ?
    return Trsf(vt_create_trsf(template_img, params=params))
