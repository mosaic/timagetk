#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Cropping Algorithms for Tissue Image Analysis

This module provides efficient cropping and boundary detection algorithms, aimed at processing
tissue images or volumetric datasets.
These tools are useful for isolating regions of interest, optimizing data processing,
and preparing images for downstream analysis.

Key Functionality

1. **minimial_boundingbox**: Calculates the minimal bounding box of a labelled image to focus only on relevant regions.
2. **find_axis_boundaries**: Identifies boundaries of objects in an image by analyzing pixel intensity profiles along axes using different methods (percentile or maximum).
3. **max_percentile_profiles**: Generates max percentile intensity profiles along any axis for advanced visualisation and object detection.
4. **threshold_max_widget**: Provides an interactive matplotlib widget for cropping based on intensity thresholding.
5. **threshold_max_percentile_widget**: Facilitates cropping through interactive percentile-based intensity thresholding.

The provided functions ensure robust boundary computation, profiling, and intensity thresholding enabling
advanced image analysis within biological and medical imaging workflows.
"""

import numpy as np
import scipy.ndimage as nd
from timagetk.bin.logger import get_logger
from timagetk.components.tissue_image import AbstractTissueImage
from timagetk.tasks.decorators import singlechannel_wrapper
from timagetk.visu.mplt import grayscale_imshow

log = get_logger(__name__)


def mimimal_boundingbox(image, background=1):
    """Search the minimal bounding box for the given labelled image.

    Parameters
    ----------
    image : timagetk.LabelledImage
        The labelled image to find bounding boxes for.

    Returns
    -------
    tuple of slice
        The tuple of slice giving the bounding box coordinates.

    Examples
    --------
    >>> from timagetk import TissueImage3D
    >>> from timagetk.algorithms.cropping import mimimal_boundingbox
    >>> from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image
    >>> image = TissueImage3D(example_layered_sphere_labelled_image(n_points=10, n_layers=1, extent=50.), background=1)
    >>> im_slices = mimimal_boundingbox(image)
    >>> image[im_slices]
    """
    if isinstance(image, AbstractTissueImage):
        background = image.background

    mask_image = image != background
    return nd.find_objects(mask_image == 1, max_label=1)[0]


@singlechannel_wrapper
def find_axis_boundaries(image, axis, threshold, method='percentile', max_pc=99., **kwargs):
    """Find image boundaries using a threshold on the profiles of each axis.

    Available `methods` are:

     - 'percentile': apply a threshold on the computed max percentile value per axis slices
     - 'maximum': apply a threshold on the computed max value per axis slices

    Parameters
    ----------
    image : timagetk.SpatialImage
        Image to use to compute the max percentile profile.
    axis : str
        Axis to slice to build the profile.
    threshold : float
        Min intensity threshold, slices with values below this one will be removed
    method : {'percentile', 'maximum'}
        Method used to detect the object boundaries.
    max_pc : float in range of [0,100], optional
        Percentile to compute, which must be between 0 and 100 inclusive.

    Other Parameters
    ----------------
    channel : str
        If a ``MultiChannelImage`` is used as input image, select the channel to use with this algorithm.

    Returns
    -------
    bound_start, bound_stop : int, int
        Start and stop slices to consider for cropping given image.

    Notes
    -----
    `max_pc` is required only for method **percentile**.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.cropping import find_axis_boundaries
    >>> img = shared_data('flower_confocal', 0)
    >>> find_axis_boundaries(img, 'x', 35)

    """
    if method == 'percentile':
        prof = _max_percentile_profile(image, axis, max_pc, n_jobs=kwargs.get('n_jobs', -1))
    elif method == 'maximum':
        prof = _max_profile(image, axis, n_jobs=kwargs.get('n_jobs', -1))
    else:
        raise ValueError("Unknown method '{}'!".format(method))

    axis_dim = image.get_shape(axis) - 1
    # Forward boundary search:
    slice_id = 0
    while prof[slice_id] < threshold and slice_id != axis_dim:
        slice_id += 1

    # Stop here if reached the last slice:
    if slice_id == axis_dim:
        msg = "No boundary found for {}-axis, consider lowering the threshold ({})!"
        log.warning(msg.format(axis, threshold))
        return [0, axis_dim]
    else:
        bound_start = slice_id

    # backward boundary search
    slice_id = axis_dim
    while prof[slice_id] < threshold and slice_id != bound_start:
        slice_id -= 1

    if slice_id == bound_start:
        msg = "Boundary search for {}-axis yield same start & stop, consider lowering the threshold ({})!"
        log.info(msg.format(axis, threshold))
        return [0, axis_dim]
    else:
        bound_stop = slice_id + 1

    return bound_start, bound_stop


# def find_axis_boundaries_percentile(image, axis, threshold, max_pc=99.):
#     """Find image boundaries using a threshold on the percentile profiles of
#     each axis.
# 
#     Parameters
#     ----------
#     image : timagetk.SpatialImage
#         Image to use to compute the max percentile profile.
#     axis : str
#         Axis to slice to build the profile.
#     threshold : float
#         Max percentile intensity threshold, slices with values below this one will
#         be removed
#     max_pc : float in range of [0,100]
#         Percentile to compute, which must be between 0 and 100 inclusive.
# 
#     Returns
#     -------
# 
#     """
#     axis_dim = image.get_shape(axis) - 1
#     # Forward boundary search:
#     slice_id = 0
#     while np.percentile(image.get_slice(slice_id, axis),
#                         max_pc) < threshold and slice_id != axis_dim:
#         slice_id += 1
# 
#     # Stop here if reached the last slice:
#     if slice_id == axis_dim:
#         msg = "WARNING: no boundary found for {}-axis, consider lowering the threshold ({})!"
#         print(msg.format(axis, threshold))
#         return [0, axis_dim]
#     else:
#         bound_start = slice_id
# 
#     # backward boundary search
#     slice_id = axis_dim
#     while np.percentile(image.get_slice(slice_id, axis),
#                         max_pc) < threshold and slice_id != bound_start:
#         slice_id -= 1
# 
#     if slice_id == bound_start:
#         msg = "Boundary search for {}-axis yield same start & stop, consider lowering the threshold ({})!"
#         print(msg.format(axis, threshold))
#         return [0, axis_dim]
#     else:
#         bound_stop = slice_id + 1
# 
#     return bound_start, bound_stop


def _bar_profile(profile, axis, ax):
    """Generate a barplot of given profile and subplot.

    Parameters
    ----------
    profile : iterable
        Profile to plot
    axis : str
        Name of the used axis
    ax : AxesSubplot
        Where to plot the figure
    """
    import matplotlib.pyplot as plt
    ax.bar(range(len(profile)), profile, width=1.)
    plt.title("{}-axis profile".format(axis))
    plt.xlabel("{}-slices".format(axis))
    plt.ylabel("Voxel intensities")
    return ax


def slice_max(sl):
    """Get the max of a given array.

    Parameters
    ----------
    sl : ndarray
        A list of slices in which the biggest one should be found.

    Returns
    -------
    Any
        Max value of the given array.

    """
    return np.max(sl)


def _max_profile(image, axis, n_jobs=-1):
    """Generate the max intensity profile of a given axis.

    The profile is built by computing the max percentile of each slice along given axis.

    Parameters
    ----------
    image : timagetk.SpatialImage
        Image to use to compute the max percentile profile.
    axis : str
        Axis to slice to build the profile.
    n_jobs : int, optional
        The maximum number of concurrently running jobs.
        If ``-1`` (default) all CPUs are used.
        If ``1`` is given, no parallel computing code is used at all.
        For `n_jobs` below ``-1``, ``n_cpus + 1 + n_jobs`` are used.
        Thus for ``n_jobs = -2``, all CPUs but one are used.

    Returns
    -------
    list
        The computed max percentile profile

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.cropping import _max_percentile_profile
    >>> image = shared_data('flower_confocal', 0)
    >>> _max_percentile_profile(image, axis='z', max_pc=99.5)

    """
    import time
    from timagetk.util import elapsed_time
    from joblib import Parallel
    from joblib import delayed
    log.info("Max profile estimation for {}-axis...".format(axis))
    start_time = time.time()  # start the timer
    slices = view_as_slices(image, axis)

    if axis == 'x':
        prof = Parallel(n_jobs=n_jobs)(
            delayed(slice_max)(slices[0, 0, :, :, n, 0]) for n in
            range(image.get_shape(axis)))
    elif axis == 'y':
        prof = Parallel(n_jobs=n_jobs)(
            delayed(slice_max)(slices[0, 0, :, n, :, 0]) for n in
            range(image.get_shape(axis)))
    else:
        prof = Parallel(n_jobs=n_jobs)(
            delayed(slice_max)(slices[0, 0, n, :, :, 0]) for n in
            range(image.get_shape(axis)))

    log.info(elapsed_time(start_time))
    return prof


def slice_max_pc(arr, q):
    """Return the q-th percentile of given array.

    Parameters
    ----------
    arr : ndarray
        Array to use to compute percentile.
    q : float
        The percentile to use.

    Returns
    -------
    float
        The q-th percentile of array.

    """
    return np.percentile(arr, q)


def view_as_slices(image, axis):
    """Slice view of the image along an axis.

    Parameters
    ----------
    image : timagetk.SpatialImage
        Image to slice
    axis : str
        Axis along which image should be sliced

    Returns
    -------
    slices : numpy.ndarray
        Slice view of the array (copy)

    Notes
    -----
    Use slices[0, 0, :, n, :, 0] with ``n`` in [0, dim(axis)] to browse by x-slice.

    Use slices[0, n, 0, :, 0, :] with ``n`` in [0, dim(axis)] to browse by y-slice.

    Use slices[0, 0, n, :, :, 0] with ``n`` in [0, dim(axis)] to browse by z-slice.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.cropping import view_as_slices
    >>> image = shared_data('flower_confocal', 0)
    >>> image = image[:, : , ::2]
    >>> print(image.shape)
    (160, 230, 115)  # ZYX sorted!
    >>> slices = view_as_slices(image, axis='x')
    >>> print(slices.shape)
    (1, 1, 115, 160, 230, 1)
    >>> slices = view_as_slices(image, axis='y')
    >>> print(slices.shape)
    (1, 230, 1, 160, 1, 115)
    >>> slices = view_as_slices(image, axis='z')
    >>> print(slices.shape)
    (1, 1, 115, 160, 230, 1)

    """
    from skimage.util import view_as_blocks
    zdim, ydim, xdim = image.shape
    if axis == 'x':
        bs = (zdim, ydim, 1)
    elif axis == 'y':
        bs = (zdim, 1, xdim)
    else:
        bs = (zdim, ydim, 1)
    slices = view_as_blocks(image.get_array(), block_shape=bs)
    return slices


def _max_percentile_profile(image, axis, max_pc, n_jobs=-1):
    """Generate the max percentile profile of a given axis.

    The profile is built by computing the max percentile of each slice along given axis.

    Parameters
    ----------
    image : timagetk.SpatialImage
        Image to use to compute the max percentile profile.
    axis : str
        Axis to slice to build the profile.
    max_pc : float
        Percentile to compute, which must be between 0 and 100 inclusive.
    n_jobs : int, optional
        The maximum number of concurrently running jobs.
        If ``-1`` (default) all CPUs are used.
        If ``1`` is given, no parallel computing code is used at all.
        For ``n_jobs`` below ``-1``, ``n_cpus + 1 + n_jobs`` are used.
        Thus, for ``n_jobs = -2``, all CPUs but one are used.

    Returns
    -------
    list
        The computed max percentile profile

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.cropping import _max_percentile_profile
    >>> image = shared_data('flower_confocal', 0)
    >>> _max_percentile_profile(image, axis='x', max_pc=99.5)

    """
    import time
    from timagetk.util import elapsed_time
    from joblib import Parallel
    from joblib import delayed
    log.info("Max percentile profile estimation for {}-axis...".format(axis))
    start_time = time.time()  # start the timer
    slices = view_as_slices(image, axis)
    dim_axis = image.get_shape(axis)
    if axis == 'x':
        prof = Parallel(n_jobs=n_jobs)(
            delayed(slice_max_pc)(slices[0, 0, :, n, :, 0], max_pc) for n in range(dim_axis))
    elif axis == 'y':
        prof = Parallel(n_jobs=n_jobs)(
            delayed(slice_max_pc)(slices[0, n, 0, :, 0, :], max_pc) for n in range(dim_axis))
    else:
        prof = Parallel(n_jobs=n_jobs)(
            delayed(slice_max_pc)(slices[0, 0, n, :, :, 0], max_pc) for n in range(dim_axis))

    log.info(elapsed_time(start_time))
    return prof


def max_percentile_profiles(image, axis=None, max_pc=99.5, **kwargs):
    """Generate a profile representation of the image max percentile values along a given axis.

    Parameters
    ----------
    image : timagetk.SpatialImage
        Image to use to compute the max percentile profile(s).
    axis : str, optional
        Axis to slice to build the profile, default is ``None`` which use them all.
    max_pc : float in range of [0,100], optional
        Percentile to compute, which must be between 0 and 100 inclusive, default is 99.5.

    Other Parameters
    ----------------
    profile_width : float
        Profile width in inch (default=5.)
    no_show : bool
        If ``True``, do not call the blocking 'show' function from matplotlib
    n_jobs : int, optional
        The maximum number of concurrently running jobs.
        If ``-1`` (default) all CPUs are used.
        If ``1`` is given, no parallel computing code is used at all.
        For ``n_jobs`` below ``-1``, ``n_cpus + 1 + n_jobs`` are used.
        Thus, for ``n_jobs = -2``, all CPUs but one are used.

    Returns
    -------
    list[matplotlib.axes.Axes]
        The list of axes with the profile(s).
        XYZ ordered if no `axis` was specified.
    list of list of float
        The list of profile(s) values along given axis.
        XYZ ordered if no `axis` was specified.

    Examples
    --------
    >>> from timagetk.algorithms.cropping import max_percentile_profiles
    >>> from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image
    >>> image = example_layered_sphere_wall_image()
    >>> ax, profiles = max_percentile_profiles(image, axis='z')
    >>> print(profiles[0][:10])  # print the first 10 values of the z-axis profile
    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    >>> ax, profiles = max_percentile_profiles(image)
    >>> print(profiles[0][:10])  # print the first 10 values of the x-axis profile
    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

    """
    import matplotlib.pyplot as plt
    from matplotlib import gridspec

    n_dim = image.ndim
    width = kwargs.get('profile_width', 6.)
    axes = []
    profiles = []
    if axis is None:
        plt.figure(figsize=(width * n_dim, 5.))
        gs = gridspec.GridSpec(1, n_dim)
        for n, axis in enumerate(['x', 'y', 'z'][:n_dim]):
            ax = plt.subplot(gs[0, n])
            prof = _max_percentile_profile(image, axis, max_pc, n_jobs=kwargs.get('n_jobs', -1))
            ax = _bar_profile(prof, axis, ax)
            profiles.append(prof)
            axes.append(ax)
    else:
        n_figs = 1
        plt.figure(figsize=(width * n_figs, 5.))
        ax = plt.subplot()
        prof = _max_percentile_profile(image, axis, max_pc, n_jobs=kwargs.get('n_jobs', -1))
        ax = _bar_profile(prof, axis, ax)
        profiles.append(prof)
        axes.append(ax)

    if kwargs.get('no_show', False):
        pass
    else:
        plt.show()

    return axes, profiles


def threshold_max_percentile_widget(image, max_pc=99.5, threshold=8., **kwargs):
    """Matplotlib widget to crop image based on max percentile thresholding.

    Parameters
    ----------
    image : timagetk.SpatialImage
        Image to use to compute the max percentile profile(s).
    max_pc : float, optional
        Percentile to compute, which must be between 0 and 100 inclusive, default is 99.5.
    threshold : float
        Min percentile threshold, slices with values below this one will be cropped

    Other Parameters
    ----------------
    profile_width : float
        Profile width in inch (default=5.)
    n_jobs : int, optional
        The maximum number of concurrently running jobs.
        If ``-1`` (default) all CPUs are used.
        If ``1`` is given, no parallel computing code is used at all.
        For ``n_jobs`` below ``-1``, ``n_cpus + 1 + n_jobs`` are used.
        Thus, for ``n_jobs = -2``, all CPUs but one are used.

    Returns
    -------
    timagetk.SpatialImage
        The cropped image.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.cropping import threshold_max_percentile_widget
    >>> image = shared_data('flower_confocal', 0)
    >>> crop_img = threshold_max_percentile_widget(image, 99.)

    """
    import matplotlib.pyplot as plt
    from matplotlib import gridspec
    from matplotlib.widgets import Slider
    from timagetk.visu.projection import projection

    n_dim = image.ndim
    width = kwargs.get('profile_width', 6.)
    zdim, ydim, xdim = image.get_shape() - np.array([1, 1, 1])

    def _force_aspect(ax_im, ratio):
        """Force the axis aspect ratio to given ratio."""
        im = ax_im.get_images()[0]
        ext = im.extent
        ax_im.set_aspect(abs((ext[1] - ext[0]) / (ext[3] - ext[2])) / ratio)
        return

    bounds = {}
    for _n, axis in enumerate(['x', 'y', 'z'][:n_dim]):
        bounds[axis] = find_axis_boundaries(image, axis, threshold=threshold, max_pc=max_pc,
                                            n_jobs=kwargs.get('n_jobs', -1))

    def _crop_img(evt):
        print(bounds)

    fig = plt.figure(figsize=(width * n_dim, 5.))
    fig.canvas.mpl_connect('close_event', _crop_img)
    gs = gridspec.GridSpec(2, n_dim)
    plt.subplots_adjust(left=0.05, bottom=0.2)

    th_lines = {}
    for n, axis in enumerate(['x', 'y', 'z'][:n_dim]):
        # Subplot for projections & boundary lines
        ax_im = plt.subplot(gs[0, n])
        proj = projection(image, method='maximum', axis=axis)
        ax_im = fig = grayscale_imshow(proj, title="Max projection {}-axis".format(axis), val_range='auto', axe=ax_im)
        _force_aspect(ax_im, proj.get_extent('x') / proj.get_extent('y'))
        if axis == 'x':
            xsl_ys, = ax_im.plot([bounds['y'][0], bounds['y'][0]], [0, zdim], color='green')
            xsl_ye, = ax_im.plot([bounds['y'][1], bounds['y'][1]], [0, zdim], color='green')
            xsl_zs, = ax_im.plot([0, ydim], [bounds['z'][0], bounds['z'][0]], color='blue')
            xsl_ze, = ax_im.plot([0, ydim], [bounds['z'][1], bounds['z'][1]], color='blue')
        elif axis == 'y':
            ysl_xs, = ax_im.plot([bounds['x'][0], bounds['x'][0]], [0, zdim], color='red')
            ysl_xe, = ax_im.plot([bounds['x'][1], bounds['x'][1]], [0, zdim], color='red')
            ysl_zs, = ax_im.plot([0, xdim], [bounds['z'][0], bounds['z'][0]], color='blue')
            ysl_ze, = ax_im.plot([0, xdim], [bounds['z'][1], bounds['z'][1]], color='blue')
        else:
            zsl_xs, = ax_im.plot([bounds['x'][0], bounds['x'][0]], [0, ydim], color='red')
            zsl_xe, = ax_im.plot([bounds['x'][1], bounds['x'][1]], [0, ydim], color='red')
            zsl_ys, = ax_im.plot([0, xdim], [bounds['y'][0], bounds['y'][0]], color='green')
            zsl_ye, = ax_im.plot([0, xdim], [bounds['y'][1], bounds['y'][1]], color='green')
        # Subplot for profile & threshold lines
        ax_prof = plt.subplot(gs[1, n])
        prof = _max_percentile_profile(image, axis, max_pc, n_jobs=kwargs.get('n_jobs', -1))
        ax_prof = _bar_profile(prof, axis, ax_prof)
        th_lines[axis] = plt.plot([0, image.get_shape(axis) - 1], [threshold, threshold], color='red')

    axcolor = 'lightgoldenrodyellow'
    axz = plt.axes([0.25, 0.1, 0.65, 0.03], axisbg=axcolor)

    maxi = 200
    thres_slider = Slider(axz, label='threshold', valmin=0, valmax=maxi,
                          closedmax=True, valinit=threshold, valfmt="%1.0f")

    def update(val):
        """Parameters
        ----------
        val
        """
        thres = thres_slider.val
        for _n, axis in enumerate(['x', 'y', 'z'][:n_dim]):
            th_lines[axis][0].set_ydata([thres, thres])
            bounds[axis] = find_axis_boundaries(image, axis, threshold=thres, max_pc=max_pc)

        xsl_ys.set_xdata([bounds['y'][0], bounds['y'][0]])
        xsl_ye.set_xdata([bounds['y'][1], bounds['y'][1]])
        xsl_zs.set_ydata([bounds['z'][0], bounds['z'][0]])
        xsl_ze.set_ydata([bounds['z'][1], bounds['z'][1]])
        ysl_xs.set_xdata([bounds['x'][0], bounds['x'][0]])
        ysl_xe.set_xdata([bounds['x'][1], bounds['x'][1]])
        ysl_zs.set_ydata([bounds['z'][0], bounds['z'][0]])
        ysl_ze.set_ydata([bounds['z'][1], bounds['z'][1]])
        zsl_xs.set_xdata([bounds['x'][0], bounds['x'][0]])
        zsl_xe.set_xdata([bounds['x'][1], bounds['x'][1]])
        zsl_ys.set_ydata([bounds['y'][0], bounds['y'][0]])
        zsl_ye.set_ydata([bounds['y'][1], bounds['y'][1]])
        fig.canvas.draw_idle()

    thres_slider.on_changed(update)

    plt.show()

    xs, xe = bounds['x']
    ys, ye = bounds['y']
    zs, ze = bounds['z']
    return image[xs:xe, ys:ye, zs:ze]


def threshold_max_widget(image, max_pc=99.5, threshold=8., **kwargs):
    """Matplotlib widget to crop image based on max intensity thresholding.

    Parameters
    ----------
    image : timagetk.SpatialImage
        Image to use to compute the max percentile profile(s).
    max_pc : float, optional
        Percentile to compute, which must be between 0 and 100 inclusive, default is ``99.5``.
    threshold : float
        Min percentile threshold, slices with values below this one will be cropped

    Other Parameters
    ----------------
    profile_width : float
        Profile width in inch (default=5.)
    n_jobs : int, optional
        The maximum number of concurrently running jobs.
        If ``-1`` (default) all CPUs are used.
        If ``1`` is given, no parallel computing code is used at all.
        For ``n_jobs`` below ``-1``, ``n_cpus + 1 + n_jobs`` are used.
        Thus, for ``n_jobs = -2``, all CPUs but one are used.

    Returns
    -------
    timagetk.SpatialImage
        The cropped image.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.cropping import threshold_max_widget
    >>> img = shared_data('flower_confocal', 0)
    >>> crop_img = threshold_max_widget(img, 99.)

    """
    import matplotlib.pyplot as plt
    from matplotlib import gridspec
    from matplotlib.widgets import Slider
    from timagetk.visu.projection import projection

    n_dim = image.ndim
    width = kwargs.get('profile_width', 6.)
    zdim, ydim, xdim = image.get_shape() - np.array([1, 1, 1])

    def _force_aspect(ax_im, ratio):
        """Force the axis aspect ratio to given ratio."""
        im = ax_im.get_images()[0]
        ext = im.extent
        ax_im.set_aspect(abs((ext[1] - ext[0]) / (ext[3] - ext[2])) / ratio)
        return

    bounds = {}
    for _n, axis in enumerate(['x', 'y', 'z'][:n_dim]):
        bounds[axis] = find_axis_boundaries(image, axis, threshold=threshold, method='maximum',
                                            n_jobs=kwargs.get('n_jobs', -1))

    def _crop_img(evt):
        print(bounds)

    fig = plt.figure(figsize=(width * n_dim, 5.))
    fig.canvas.mpl_connect('close_event', _crop_img)
    gs = gridspec.GridSpec(2, n_dim)
    plt.subplots_adjust(left=0.05, bottom=0.2)

    th_lines = {}
    for n, axis in enumerate(['x', 'y', 'z'][:n_dim]):
        # Subplot for projections & boundary lines
        ax_im = plt.subplot(gs[0, n])
        proj = projection(image, method='maximum', axis=axis)
        ax_im = fig = grayscale_imshow(proj, title="Max projection {}-axis".format(axis),
                                 val_range='auto', axe=ax_im)
        _force_aspect(ax_im, proj.get_extent('x') / proj.get_extent('y'))
        if axis == 'x':
            xsl_ys, = ax_im.plot([bounds['y'][0], bounds['y'][0]], [0, zdim], color='green')
            xsl_ye, = ax_im.plot([bounds['y'][1], bounds['y'][1]], [0, zdim], color='green')
            xsl_zs, = ax_im.plot([0, ydim], [bounds['z'][0], bounds['z'][0]], color='blue')
            xsl_ze, = ax_im.plot([0, ydim], [bounds['z'][1], bounds['z'][1]], color='blue')
        elif axis == 'y':
            ysl_xs, = ax_im.plot([bounds['x'][0], bounds['x'][0]], [0, zdim], color='red')
            ysl_xe, = ax_im.plot([bounds['x'][1], bounds['x'][1]], [0, zdim], color='red')
            ysl_zs, = ax_im.plot([0, xdim], [bounds['z'][0], bounds['z'][0]], color='blue')
            ysl_ze, = ax_im.plot([0, xdim], [bounds['z'][1], bounds['z'][1]], color='blue')
        else:
            zsl_xs, = ax_im.plot([bounds['x'][0], bounds['x'][0]], [0, ydim], color='red')
            zsl_xe, = ax_im.plot([bounds['x'][1], bounds['x'][1]], [0, ydim], color='red')
            zsl_ys, = ax_im.plot([0, xdim], [bounds['y'][0], bounds['y'][0]], color='green')
            zsl_ye, = ax_im.plot([0, xdim], [bounds['y'][1], bounds['y'][1]], color='green')
        # Subplot for profile & threshold lines
        ax_prof = plt.subplot(gs[1, n])
        prof = _max_profile(image, axis)
        ax_prof = _bar_profile(prof, axis, ax_prof)
        th_lines[axis] = plt.plot([0, image.get_shape(axis) - 1], [threshold, threshold], color='red')

    axcolor = 'lightgoldenrodyellow'
    axz = plt.axes([0.25, 0.1, 0.65, 0.03], axisbg=axcolor)

    maxi = 200
    thres_slider = Slider(axz, label='threshold', valmin=0, valmax=maxi,
                          closedmax=True, valinit=threshold, valfmt="%1.0f")

    def update(val):
        """Parameters
        ----------
        val
        """
        thres = thres_slider.val
        for _n, axis in enumerate(['x', 'y', 'z'][:n_dim]):
            th_lines[axis][0].set_ydata([thres, thres])
            bounds[axis] = find_axis_boundaries(image, axis, threshold=thres, method='maximum')

        xsl_ys.set_xdata([bounds['y'][0], bounds['y'][0]])
        xsl_ye.set_xdata([bounds['y'][1], bounds['y'][1]])
        xsl_zs.set_ydata([bounds['z'][0], bounds['z'][0]])
        xsl_ze.set_ydata([bounds['z'][1], bounds['z'][1]])
        ysl_xs.set_xdata([bounds['x'][0], bounds['x'][0]])
        ysl_xe.set_xdata([bounds['x'][1], bounds['x'][1]])
        ysl_zs.set_ydata([bounds['z'][0], bounds['z'][0]])
        ysl_ze.set_ydata([bounds['z'][1], bounds['z'][1]])
        zsl_xs.set_xdata([bounds['x'][0], bounds['x'][0]])
        zsl_xe.set_xdata([bounds['x'][1], bounds['x'][1]])
        zsl_ys.set_ydata([bounds['y'][0], bounds['y'][0]])
        zsl_ye.set_ydata([bounds['y'][1], bounds['y'][1]])
        fig.canvas.draw_idle()

    thres_slider.on_changed(update)

    plt.show()

    xs, xe = bounds['x']
    ys, ye = bounds['y']
    zs, ze = bounds['z']
    return image[xs:xe, ys:ye, zs:ze]
