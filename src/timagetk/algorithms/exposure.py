#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Exposure Adjustment Algorithms for Image Processing

This module provides a variety of image exposure adjustment techniques based on the functionalities found in the
'exposure' module of `scikit-image <https://scikit-image.org/>`_. The algorithms are designed to assist in stretching
or scaling intensity values of images, making them suitable for optimizing visualisation or enhancing specific features
in imaging data.

Key Features

- **Global Contrast Stretching**: Adjusts image intensities globally based on specified percentiles.
- **Slice-based Contrast Stretching**: Adjusts contrast slice by slice along a specified axis, useful for 3D images.
- **Adaptive Histogram Equalization (AHE)**: Improves image contrast adaptively using contextual regions.
- **Gamma Adjustment**: Applies gamma correction to brighten or darken an image.
- **Logarithmic Adjustment**: Applies logarithmic correction (or inverse-log) for dynamic range adjustments.
- **Sigmoid Adjustment**: Applies sigmoid function transformation to enhance contrast.
- **Intensity Rescaling**: Rescales or stretches image intensities between desired input and output ranges.

Usage Examples

>>> from timagetk.io.dataset import shared_data
>>> from timagetk.algorithms.exposure import global_contrast_stretch, equalize_adapthist
>>> from timagetk.visu.mplt import grayscale_imshow
>>> # Load a sample image
>>> image = shared_data('flower_confocal', 0)
>>> # Global Contrast Stretching
>>> stretched_image = global_contrast_stretch(image)
>>> grayscale_imshow([image, stretched_image], title=["Original", "Stretched"])
>>> # Adaptive Histogram Equalization
>>> equalized_image = equalize_adapthist(image)
>>> grayscale_imshow([image, equalized_image], title=["Original", "Equalized"])

These functions are particularly useful for image preprocessing in computational imaging pipelines where intensity
normalization or enhancement is required.
"""

import numpy as np
from skimage.util import img_as_ubyte
from skimage.util import img_as_uint
from tqdm import tqdm

from timagetk.components.spatial_image import SpatialImage
from timagetk.components.image import assert_spatial_image
from timagetk.components.image import get_image_attributes
from timagetk.tasks.decorators import multichannel_wrapper

try:
    from skimage import exposure
except ImportError:
    msg = "Could not import `scikit-image` package, please install it!"
    msg += "\n"
    msg += "Use `conda install scikit-image` or `pip install scikit-image -U`."
    raise ImportError(msg)


# ------------------------------------------------------------------------------
# - Contrast stretching:
# ------------------------------------------------------------------------------
def _contrast_stretch(image, pc_min=2, pc_max=99):
    """Return image after stretching its intensity levels.

    Go from the lower / upper percentile values to the "dtype range" of the `image`.
    For example, stretch it to the ``[0, 65535]`` range if the given `image` is of type 'uint16'.

    Parameters
    ----------
    image : numpy.ndarray
        Array or image to stretch, can be 2D or 3D.
    pc_min : int or float, optional
        Lower percentile to use for image stretching. Defaults to ``2``.
    pc_max : int or float, optional
        Upper percentile to use for image stretching. Defaults to ``99``.

    Returns
    -------
    numpy.ndarray
        Stretched array.

    See Also
    --------
    numpy.percentile
    skimage.exposure.rescale_intensity

    """
    low, up = np.percentile(image, [pc_min, pc_max])
    return exposure.rescale_intensity(image, in_range=(low, up))


@multichannel_wrapper
def global_contrast_stretch(image, pc_min=2, pc_max=99):
    """Performs global image contrast stretching.

    Contrast stretching is here performed using lower and upper percentile of the image intensity values to the min and
    max value of the image dtype.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        Image to stretch, can be 2D or 3D.
    pc_min : int or float, optional
        Lower percentile to use for image stretching. Defaults to ``2``.
    pc_max : int or float, optional
        Upper percentile to use for image stretching. Defaults to ``99``.

    Other Parameters
    ----------------
    channel : str
        If a ``MultiChannelImage`` is used as input image, the algorithm will be applied only to this channel.
        Else it will be applied to all channels of the multichannel image.

    Returns
    -------
    timagetk.SpatialImage
        Stretched image.

    See Also
    --------
    timagetk.algorithms.exposure.slice_contrast_stretch
    timagetk.algorithms.exposure.rescale_intensity
    numpy.percentile

    Example
    -------
    >>> from timagetk.algorithms.exposure import global_contrast_stretch
    >>> from timagetk.visu.mplt import intensity_histogram
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image
    >>> image = example_layered_sphere_wall_image()
    >>> stretch_img = global_contrast_stretch(image)
    >>> intensity_histogram([image, stretch_img], suptitle="Effect of global stretching on intensity distribution")

    >>> fig = grayscale_imshow([image, stretch_img], slice_id=5, title=["Original", "Contrasted"],suptitle="Contrast stretching (z-slice=5/{})".format(img.shape[-1]))

    """
    assert_spatial_image(image, obj_name='image')
    attr = get_image_attributes(image, extra=['filename'])

    im = image.get_array()
    im = _contrast_stretch(im, pc_min, pc_max)
    im = SpatialImage(im, **attr)
    # add2md(im)
    return im


@multichannel_wrapper
def slice_contrast_stretch(image, pc_min=2, pc_max=99, axis='z', threshold=None, disable_tqdm=False):
    """Performs slice by slice contrast stretching.

    Contrast stretching is here performed using lower and upper percentile of the image values to the min and max value
    of the image dtype.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        Image to stretch, can be 2D or 3D.
    pc_min : int or float, optional
        Lower percentile to use for image stretching. Defaults to ``2``.
    pc_max : int or float, optional
        Upper percentile to use for image stretching. Defaults to ``99``.
    axis : str in {'z', 'y', 'x'}, optional
        Selected axe to move along, 'z' by default.
    threshold : int, optional
        Intensity threshold of slice to equalize, slices with a max intensity value below this one will be ignored.
        By default, no threshold is used.
    disable_tqdm : bool, optional
        Set to `True` to disable tqdm progress bar. Defaults to `False`.

    Other Parameters
    ----------------
    channel : str
        If a ``MultiChannelImage`` is used as input image, the algorithm will be applied only to this channel.
        Else it will be applied to all channels of the multichannel image.

    Returns
    -------
    timagetk.SpatialImage
        Stretched image.

    See Also
    --------
    timagetk.algorithms.exposure.global_contrast_stretch

    Notes
    -----
    Obviously this is only for 3D images, with 2D images use ``global_contrast_stretch``.

    Beware that, if you don't specify the threshold, this method will also stretch slices without signal in the case
    where the observed object is not touching all margins of your image!

    Example
    -------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.exposure import slice_contrast_stretch
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> from timagetk.visu.mplt import intensity_histogram
    >>> image = shared_data('flower_confocal', 0)
    >>> zsh, ysh, xsh = image.shape
    >>> # Slice by slice contrast in 'Z' direction:
    >>> out_img = slice_contrast_stretch(image)
    >>> zsl = 15
    >>> fig = grayscale_imshow([image, out_img], slice_id=zsl, axis='z', title=["Original", "Contrasted"], suptitle=f"Contrast stretching (z-slice={zsl}/{zsh})")
    >>> intensity_histogram([image, out_img], suptitle="Effect of slice-by-slice stretching on intensity distribution")

    """
    assert_spatial_image(image, obj_name='image')
    attr = get_image_attributes(image, extra=['filename'])

    axsh = image.get_shape(axis)
    if threshold is None:
        cs = [_contrast_stretch(image.get_slice(n, axis=axis), pc_min, pc_max) for n in range(0, axsh)]
    else:
        cs = [_contrast_stretch(image.get_slice(n, axis=axis), pc_min, pc_max) if np.percentile(
            image.get_slice(n, axis=axis), 98) >= threshold else image.get_slice(n, axis=axis)
              for n in tqdm(range(0, axsh), unit="slice", disable=disable_tqdm)]

    # Re-create the NumPy array:
    if axis == "z":
        # cs is a list of z-slices, and z-slices are YX ordered:
        im = np.array(cs)
    elif axis == "y":
        # cs is a list of y-slices, and y-slices are ZX ordered:
        im = np.array(cs).transpose([1, 0, 2])
    else:
        # cs is a list of x-slices, and x-slices are ZY ordered:
        im = np.array(cs).transpose([1, 2, 0])

    im = SpatialImage(im, **attr)
    # add2md(im)
    return im


# ------------------------------------------------------------------------------
# - Adaptive histogram equalisation:
# ------------------------------------------------------------------------------

@multichannel_wrapper
def equalize_adapthist(image, kernel_size=1 / 5., clip_limit=0.01, nbins=256):
    """Performs adaptive histogram equalization.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        Image from which to extract the slice
    kernel_size : int or list of int, optional
        Defines the shape of contextual regions used in the algorithm.
        If iterable is passed, it must have the same number of elements as image.ndim (without color channel).
        If integer, it is broadcasted to each image dimension.
        By default, kernel_size is 1/8 of image height by 1/8 of its width.
    clip_limit : float, optional
        Clipping limit, normalized between 0 and 1 (higher values give more contrast).
    nbins : int, optional
        Number of gray bins for histogram ("data range").

    Other Parameters
    ----------------
    channel : str
        If a ``MultiChannelImage`` is used as input image, the algorithm will be applied only to this channel.
        Else it will be applied to all channels of the multichannel image.

    Returns
    -------
    timagetk.SpatialImage
        Equalized intensity image

    See Also
    --------
    skimage.exposure.equalize_adapthist
    numpy.percentile


    Notes
    -----
    Obviously this is only for 3D images, with 2D images use ``global_contrast_stretch``.

    Beware that, if you don't specify the threshold, this method will also stretch slices without signal in the case
    where the observed object is not touching all margins of your image!

    Example
    -------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.exposure import equalize_adapthist
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> from timagetk.visu.stack import stack_browser
    >>> from timagetk.visu.mplt import intensity_histogram
    >>> image = shared_data("flower_confocal", 0)
    >>> zsh, ysh, xsh = image.shape
    >>> # Slice by slice equalization in 'Z' direction:
    >>> out_img = equalize_adapthist(image)
    >>> zsl = 15
    >>> fig = grayscale_imshow([image, out_img], slice_id=zsl, axis='z', suptitle=f"adaptive histogram equalization (z-slice={zsl}/{zsh})", title=["Original", "Equalized"])
    >>> intensity_histogram([image, out_img], suptitle="Effect of adaptive histogram equalization on intensity distribution")

    """
    assert_spatial_image(image, obj_name='image')
    attr = get_image_attributes(image, extra=['filename'])

    # Reorder axis order from (z, y, x) to (x, y, z)
    arr = image.get_array().transpose()
    # Rescale image data to range [0, 1]
    arr = np.clip(arr, np.percentile(arr, 3), np.percentile(arr, 99.5))
    arr = (arr - arr.min()) / (arr.max() - arr.min())

    ks = np.multiply(image.get_shape(), kernel_size)
    arr = exposure.equalize_adapthist(arr, clip_limit=clip_limit, kernel_size=ks, nbins=nbins)

    if image.dtype == np.uint8:
        arr = img_as_ubyte(arr)
    else:
        arr = img_as_uint(arr)

    im = SpatialImage(arr.transpose(), **attr)
    return im


@multichannel_wrapper
def gamma_adjust(image, gamma=1., gain=1.):
    """Performs Gamma Correction on the input image.

    Also known as **Power Law Transform**.
    This function transforms the input image pixelwise according to the equation ``O = I**gamma`` after scaling each
    pixel to the range 0 to 1.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        Input image, can be 2D or 3D.
    gamma : float, optional
        Non-negative real number. Defaults to ``1.``.
    gain : float, optional
        The constant multiplier. Defaults to ``1.``.

    Other Parameters
    ----------------
    channel : str
        If a ``MultiChannelImage`` is used as input image, the algorithm will be applied only to this channel.
        Else it will be applied to all channels of the multichannel image.

    Returns
    -------
    timagetk.SpatialImage
        Gamma corrected output image.

    See Also
    --------
    skimage.exposure.adjust_gamma
    timagetk.algorithms.exposure.log_adjust

    Notes
    -----
    For ``gamma > 1``, the output image will be **darker** than the input image.

    For ``gamma < 1``, the output image will be **brighter** than the input image.

    References
    ----------
    `Wikipedia - Gamma correction <https://en.wikipedia.org/wiki/Gamma_correction>`_
    `Scikit-image - Adjust gamma <https://scikit-image.org/docs/dev/api/skimage.exposure.html#skimage.exposure.adjust_gamma>`_

    Example
    -------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.exposure import gamma_adjust
    >>> from timagetk.visu.mplt import grayscale_imshow, image_n_hist
    >>> image = shared_data("flower_confocal", 0)
    >>> zsh, ysh, xsh = image.shape
    >>> gamma_range = [0.5, 1.5]
    >>> out_imgs = [gamma_adjust(image, gamma) for gamma in gamma_range]
    >>> zsl = 15
    >>> fig = grayscale_imshow([image] + out_imgs, slice_id=zsl, axis='z', title=["Original"]+[f"Gamma={g}" for g in gamma_range], suptitle=f"Gamma adjustement (z-slice={zsl}/{zsh})")
    >>> image_n_hist([image.get_slice(zsl)] + [img.get_slice(zsl) for img in out_imgs], img_title=["Original"]+[f"Gamma={g}" for g in gamma_range], title=f"Gamma adjustement (z-slice={zsl}/{zsh})")

    """
    assert_spatial_image(image, obj_name='image')
    attr = get_image_attributes(image, extra=['filename'])

    zsh = image.get_shape('z')
    cs = [exposure.adjust_gamma(image.get_slice(n), gamma=gamma, gain=gain) for n in range(0, zsh)]

    im = SpatialImage(np.array(cs), **attr)
    # add2md(im)
    return im


@multichannel_wrapper
def log_adjust(image, gain=1, inv=False):
    """Performs Logarithmic correction on the input image.

    This function transforms the input image pixelwise according to the
    equation ``O = gain*log(1 + I)`` after scaling each pixel to the range 0 to 1.
    For inverse logarithmic correction, the equation is ``O = gain*(2**I - 1)``.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        Input image, can be 2D or 3D.
    gain : float, optional
        The constant multiplier. Default value is ``1``.
    inv : float, optional
        If ``True``, it performs inverse logarithmic correction, else correction will be logarithmic.
        Defaults to ``False``.

    Other Parameters
    ----------------
    channel : str
        If a ``MultiChannelImage`` is used as input image, the algorithm will be applied only to this channel.
        Else it will be applied to all channels of the multichannel image.

    Returns
    -------
    timagetk.SpatialImage
        Logarithm corrected output image.

    See Also
    --------
    skimage.exposure.adjust_gamma
    timagetk.algorithms.exposure.gamma_adjust

    References
    ----------
    `Scikit-image - Adjust log <https://scikit-image.org/docs/dev/api/skimage.exposure.html#adjust-log>`_

    Example
    -------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.exposure import log_adjust
    >>> from timagetk.visu.mplt import grayscale_imshow, image_n_hist
    >>> image = shared_data("flower_confocal", 0)
    >>> zsh, ysh, xsh = image.shape
    >>> gain_range = [0.5, 1.5]
    >>> out_imgs = [log_adjust(image, gain) for gain in gain_range]
    >>> zsl = 15
    >>> fig = grayscale_imshow([image] + out_imgs, slice_id=zsl, axis='z', title=["Original"]+[f"Gain={g}" for g in gain_range], suptitle=f"Log adjustement (z-slice={zsl}/{zsh})")
    >>> image_n_hist([image.get_slice(zsl)] + [img.get_slice(zsl) for img in out_imgs], img_title=["Original"]+[f"Gain={g}" for g in gain_range], title=f"Log adjustement (z-slice={zsl}/{zsh})")

    """
    assert_spatial_image(image, obj_name='image')
    attr = get_image_attributes(image, extra=['filename'])

    zsh = image.get_shape('z')
    cs = [exposure.adjust_log(image.get_slice(n), gain=gain, inv=inv) for n in range(0, zsh)]

    im = SpatialImage(np.array(cs), **attr)
    # add2md(im)
    return im


@multichannel_wrapper
def sigmoid_adjust(image, cutoff=0.5, gain=10, inv=False):
    """Performs Sigmoid Correction on the input image.

    Also known as **Contrast Adjustment**.
    This function transforms the input image pixelwise according to the equation: ``O = 1/(1 + exp*(gain*(cutoff - I)))``
    after scaling each pixel to the range 0 to 1.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        Input image, can be 2D or 3D.
    cutoff : float, optional
        Cutoff of the sigmoid function that shifts the characteristic curve in horizontal direction.
        Defaults to ``0.5``.
    gain : float, optional
        The constant multiplier in exponential's power of sigmoid function.
        Defaults to ``10``.
    inv : bool, optional
        If ``True``, returns the negative sigmoid correction. Defaults to ``False``.

    Other Parameters
    ----------------
    channel : str
        If a ``MultiChannelImage`` is used as input image, the algorithm will be applied only to this channel.
        Else it will be applied to all channels of the multichannel image.

    Returns
    -------
    timagetk.SpatialImage
        Sigmoid corrected output image.

    See Also
    --------
    skimage.exposure.adjust_sigmoid

    References
    ----------
    Gustav J. Braun, *Image Lightness Rescaling Using Sigmoidal Contrast Enhancement Functions* `PDF <http://www.cis.rit.edu/fairchild/PDFs/PAP07.pdf>`_

    `Scikit-image - Sigmoid adjust <https://scikit-image.org/docs/dev/api/skimage.exposure.html#adjust-sigmoid>`_

    Example
    -------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.exposure import sigmoid_adjust
    >>> from timagetk.visu.mplt import grayscale_imshow, image_n_hist
    >>> image = shared_data("flower_confocal", 0)
    >>> zsh, ysh, xsh = image.shape
    >>> cutoff_range = [0.2, 0.5, 0.8]
    >>> out_imgs = [sigmoid_adjust(image, gain) for gain in cutoff_range]
    >>> zsl = 15
    >>> fig = grayscale_imshow([image] + out_imgs, slice_id=zsl, axis='z', title=["Original"]+[f"Cutoff={co}" for co in cutoff_range], suptitle=f"Sigmoid adjustement (z-slice={zsl}/{zsh})")
    >>> image_n_hist([image.get_slice(zsl)] + [img.get_slice(zsl) for img in out_imgs], img_title=["Original"]+[f"Cutoff={co}" for co in cutoff_range], title=f"Sigmoid adjustement (z-slice={zsl}/{zsh})")

    """
    assert_spatial_image(image, obj_name='image')
    attr = get_image_attributes(image, extra=['filename'])

    zsh = image.get_shape('z')
    cs = [exposure.adjust_sigmoid(image.get_slice(n), cutoff=cutoff, gain=gain, inv=inv) for n in range(0, zsh)]

    im = SpatialImage(np.array(cs), **attr)
    # add2md(im)
    return im


@multichannel_wrapper
def rescale_intensity(image, in_range='image', out_range='dtype'):
    """Return image after stretching or shrinking its intensity levels.

    The desired intensity range of the input and output, `in_range` and `out_range` respectively, are used to stretch
    or shrink the intensity range of the input image.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        Input intensity image, can be 2D or 3D.
    in_range, out_range : str or 2-tuple, optional
        Min and max intensity values of input and output image.
        The possible string values for these parameters are 'image' or 'dtype'.

    Other Parameters
    ----------------
    channel : str
        If a ``MultiChannelImage`` is used as input image, the algorithm will be applied only to this channel.
        Else it will be applied to all channels of the multichannel image.

    Returns
    -------
    timagetk.SpatialImage
        Image after intensity rescaling. This image has the same ``dtype`` as the input image.

    See Also
    --------
    equalize_adapthist

    Notes
    -----
    Details about `in_range` & `out_range` values:

    - **image**: use image min/max as the intensity range.
    - **dtype**: use min/max of the image's dtype as the intensity range.
    - **2-tuple**: explicit values for min & max intensities.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.exposure import rescale_intensity
    >>> from timagetk.visu.mplt import image_n_hist
    >>> image = shared_data("flower_confocal", 0)
    >>> print(f"Original image range: {image.min()}, {image.max()}")
    Original image range: 10, 255
    >>> # Example #1: Rescale min/max of image to dtype (here 0-255):
    >>> out_img = rescale_intensity(image, in_range='image')
    >>> print(f"Rescaled image range: {out_img.min()}, {out_img.max()}")
    Rescaled image range: 0, 255
    >>> zsl = 15
    >>> image_n_hist([image.get_slice(zsl), out_img.get_slice(zsl)], img_title=["Original", "Rescaled"], title="Intensity rescaling")
    >>> # Example #2: Rescale image to dtype (here 0-255):
    >>> out_img = rescale_intensity(image, in_range=(10, 200))
    >>> print(f"Rescaled image range: {out_img.min()}, {out_img.max()}")
    >>> zsl = 15
    >>> image_n_hist([image.get_slice(zsl), out_img.get_slice(zsl)], img_title=["Original", "Rescaled"], title="Intensity rescaling")

    """
    assert_spatial_image(image, obj_name='image')
    attr = get_image_attributes(image, extra=['filename'])

    zsh = image.get_shape('z')
    cs = [exposure.rescale_intensity(image.get_slice(n), in_range=in_range, out_range=out_range) for n in range(0, zsh)]

    im = SpatialImage(np.array(cs), **attr)
    # add2md(im)
    return im
