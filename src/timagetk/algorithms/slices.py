#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Slices Manipulation Module

This module provides tools for manipulating and analyzing slices in multi-dimensional arrays.
It is particularly useful for tasks involving bounding-box operations, slice translations, and managing spatial regions.

Key Features

- **Dilation Methods**: Expand slices (bounding-boxes) by one or multiple voxels in all dimensions.
- **Bounding-Box Translation**: Translate slices to a new origin based on spatial coordinates.
- **Real-World Coordinates**: Convert voxel-based bounding-boxes into real-world units using specified resolutions.
- **Label-Based Operations**: Compare bounding-box sizes for labelled regions, compute minimal bounding-boxes, and more.
- **Slice Combination**: Merge multiple slices into a single encompassing slice with the widest range.
- **Overlap Testing**: Check if two given slices overlap in space.

Usage Examples

1. **Dilating Slices**:
    ```python
    from slices import dilation, dilation_by

    slices = (slice(2, 5), slice(3, 7))
    dilated_slices = dilation(slices)  # Expands by 1 voxel in all directions
    dilated_slices_by_3 = dilation_by(slices, amount=3)  # Expands by 3 voxels in all directions
    ```

2. **Testing Overlapping Slices**:
    ```python
    from slices import overlapping_slices

    slice_a = [slice(2, 5), slice(1, 3)]
    slice_b = [slice(2, 4), slice(0, 2)]
    print(overlapping_slices(slice_a, slice_b))  # Output: True
    ```

3. **Combining Slices**:
    ```python
    from slices import combine_slices

    slices = [[slice(2, 5), slice(1, 3)], [slice(3, 8), slice(0, 6)]]
    combined = combine_slices(slices)
    print(combined)  # Output: [slice(2, 8), slice(0, 6)]
    ```

"""

import numpy as np
import scipy.ndimage as nd


def dilation(slices):
    """Expand bounding-boxes, tuple of 'slice' by one voxel in every direction.

    Hence, each slice starts one voxel before and ends one voxel after.

    Parameters
    ----------
    slices : tuple
        A tuple of slices to dilate.

    Returns
    -------
    tuple of slice
        Tuple of dilated slice
    """
    return tuple(slice(max(0, s.start - 1), s.stop + 1) for s in slices)


def dilation_by(slices, amount=2):
    """Expand bounding-boxes, tuple of 'slice' by a given number of voxels ('amount') in every direction.

    Hence, each slice starts 'amount'-voxel before and ends 'amount'-voxel after.

    Parameters
    ----------
    slices : tuple
        A tuple of slices to dilate.
    amount : int
        The amount of expansion to apply to each slice start and stop

    Returns
    -------
    tuple of slice
        Tuple of dilated slice
    """
    return tuple(slice(max(0, s.start - amount), s.stop + amount) for s in slices)


def change_boundingbox_origins(bboxes, xyz_origin):
    """Translate the bounding-box to a new origin.

    Remove x, y and z origins to the start and stop of their respective dimension.

    Parameters
    ----------
    bboxes : tuple
        Length-3 tuple of slices to translate
    xyz_origin : list
        Length-3 list of x, y & z values used to translate the bounding-box origin

    Returns
    -------
    list of slice
        The XYZ list of translated slices.
    """
    if isinstance(bboxes, dict):
        return {k: change_boundingbox_origins(bbox, xyz_origin) for k, bbox in
                bboxes.items()}
    elif isinstance(bboxes, tuple):
        ori = xyz_origin
        return [slice(b.start - ori[n], b.stop - ori[n]) for n, b in
                enumerate(bboxes)]
    else:
        raise TypeError("Unknown type for 'bboxes', should be dict or tuple!")


def real_indices(slices, resolutions):
    """Transform the voxels coordinates of the bounding-box into real units.

    Parameters
    ----------
    slices : list
        List of slices or bounding-boxes found using scipy.ndimage.find_objects
    resolutions : list
        Length-2 (2D) or length-3 (3D) vector of float indicating the size of a voxel in real-world units

    Returns
    -------
    list of slice
        List of slice objects
    """
    return [slice(s.start * r, s.stop * r) for s, r in zip(slices, resolutions)]


def sort_labels_by_bbox(boundingbox, label_1, label_2):
    """Determine which provided label as the smallest bounding-box.

    Parameters
    ----------
    boundingbox : dict
        Label-based dictionary of slices tuples.
    label_1 : int
        A label to be found within the image.
    label_2 : int
        A label to be found within the image.

    Returns
    -------
    tuple
        The two labels, the first one has the smallest bounding-box.
    """
    assert isinstance(boundingbox, dict)
    if label_1 not in boundingbox:
        boundingbox[label_1] = None
    if label_2 not in boundingbox:
        boundingbox[label_2] = None

    bbox_1 = boundingbox[label_1]
    bbox_2 = boundingbox[label_2]
    # if bbox only for 'label_1' return 'label_2' first
    if bbox_1 is None and bbox_2:
        return label_2, label_1
    # if bbox only for 'label_2' return 'label_1' first
    if bbox_1 and bbox_2 is None:
        return label_1, label_2
    # if no bbox for 'label_1' and 'label_2' return (None, None)
    if bbox_1 is None and bbox_2 is None:
        return None, None
    # if both found in 'bounding-box', compute the smallest one use its volume:
    vol_bbox_1 = (bbox_1[0].stop - bbox_1[0].start) * (
            bbox_1[1].stop - bbox_1[1].start) * (
                         bbox_1[2].stop - bbox_1[2].start)
    vol_bbox_2 = (bbox_2[0].stop - bbox_2[0].start) * (
            bbox_2[1].stop - bbox_2[1].start) * (
                         bbox_2[2].stop - bbox_2[2].start)

    if vol_bbox_1 < vol_bbox_2:
        return label_1, label_2
    else:
        return label_2, label_1


def smallest_boundingbox(image, label_1, label_2):
    """Compute bounding-box for labels `label_1` and `label_2` and return the smallest in `image`.

    Parameters
    ----------
    image : timagetk.SpatialImage
        Labelled image containing labels `label_1` and `label_2`.
    label_1 : int
        A label to be found within the `image`.
    label_2 : int
        A label to be found within the `image`.

    Returns
    -------
    tuple of slice
        The smallest bounding-box object between `label_1` and `label_2`.
    """
    bbox = nd.find_objects(image, max_label=max([label_1, label_2]))
    # we do 'label_x - 1' since 'nd.find_objects' start at '1' (and not '0') !
    bbox = {label_1: bbox[label_1 - 1], label_2: bbox[label_2 - 1]}
    label_1, label_2 = sort_labels_by_bbox(bbox, label_1, label_2)
    return bbox[label_1]


def combine_slices(slices):
    """Return the slice containing all input slices.

    Parameters
    ----------
    slices : list
        List of slice to combine.

    Returns
    -------
    slice
        A slice with the min start and the max stop (step is set to ``None``)

    Example
    -------
    >>> from timagetk.algorithms.slices import combine_slices
    >>> slice_a = [slice(2, 5), slice(1, 3)]
    >>> slice_b = [slice(2, 4), slice(0, 2)]
    >>> slice_c = [slice(3, 8), slice(1, 6)]
    >>> combine_slices([slice_a, slice_b, slice_c])
    [slice(2, 8), slice(0, 6)]
    """
    region = []
    dim_slice = len(slices[0])
    for dim in range(dim_slice):
        start = min([s[dim].start for s in slices])
        stop = max([s[dim].stop for s in slices])
        region.append(slice(start, stop))

    return region


def overlapping_slices(slice_a, slice_b):
    """Test if two tuple or list of slices are overlapping or not.

    Parameters
    ----------
    slice_a : tuple or list of slice
        Slice for object A
    slice_b : tuple or list of slice
        Slice for object B

    Returns
    -------
    bool
        ``True`` if the slices are overlapping, else ``False``.

    Examples
    --------
    >>> slice_a = [slice(2, 5), slice(1, 3)]
    >>> slice_b = [slice(2, 4), slice(0, 2)]
    >>> overlapping_slices(slice_a, slice_b)
    True
    >>> slice_a = [slice(2, 5), slice(2, 3)]
    >>> slice_b = [slice(2, 4), slice(0, 2)]
    >>> overlapping_slices(slice_a, slice_b)
    False

    """
    n_a, n_b = len(slice_a), len(slice_b)
    try:
        assert n_a == n_b
    except AssertionError:
        raise ValueError("Both slices should have the same dimensionality!")

    overlap = [False] * n_a
    for n in range(n_a):
        s_a, s_b = slice_a[n], slice_b[n]
        if min([s_a.stop, s_b.stop]) > max([s_a.start, s_b.start]):
            overlap[n] = True

    if np.alltrue(overlap):
        return True
    else:
        return False
