#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Topological Elements Extraction Module

This module provides algorithms to extract and classify topological elements (labels, surfels, linels, and pointels)
from 2D and 3D labelled arrays. These operations are essential for analyzing multi-dimensional segmented data,
allowing users to identify interfaces, edges, and vertices between labels.

Key Features

- Extraction of topological elements for 3D labelled arrays:

  - Labels: 3D elements representing segmented data.
  - Surfels: 2D elements representing interfaces between labels.
  - Linels: 1D elements representing edges between multiple labels.
  - Pointels: 0D elements representing vertices formed by multiple labels.

- Extraction of topological elements for 2D labelled arrays:

  - Labels: 2D regions in the segmented image.
  - Linels: 1D edges between labels in a 2D neighborhood.
  - Pointels: 0D vertices formed from a 2D neighborhood.

- Handles large datasets efficiently, offering memory-friendly methods.
- Option to restrict dimensionality of elements for specific use cases.
- Configurable output through parameters like simplex filtering and verbose logging.

Usage Examples

Example 1 - Extracting topological elements from a 3D labelled array:

```python
import numpy as np
from timagetk.array_util import dummy_labelled_image_3D
from timagetk.algorithms.topological_elements import topological_elements_extraction3D
img = dummy_labelled_image_3D()
elements = topological_elements_extraction3D(
    img, elem_order=[2, 1, 0], simplex=True, use_vq=False, verbose=True
)
print(elements)  # Outputs dictionary of topological elements
```

Example 2 - Extracting topological elements from a 2D labelled array:

```python
from timagetk.algorithms.topological_elements import topological_elements_extraction2D
img_2D = np.array([
    [1, 1, 0],
    [1, 2, 2],
    [0, 2, 2]
])
elements_2D = topological_elements_extraction2D(
    img_2D, elem_order=[1, 0], simplex=True
)
print(elements_2D)  # Outputs dictionary of linels and pointels
```
"""

import bisect
import time

import numpy as np
import tqdm
from scipy.cluster.vq import vq

from timagetk.bin.logger import get_logger
from timagetk.util import elapsed_time
from timagetk.util import stuple

log = get_logger(__name__)


def _check_element_order(elem_order):
    all_orders = range(3)
    if elem_order is None:
        elem_order = all_orders
    else:
        # In an integer, make it a list:
        if isinstance(elem_order, int):
            elem_order = [elem_order]
        # Now check given values for `elem_order`:
        try:
            # Check we have at least one valid value:
            assert any(eo in all_orders for eo in elem_order)
        except AssertionError:
            # No value is right...
            log.error(f"Element orders should be in `{list(all_orders)}`, got `{elem_order}`!")
            return None
        else:
            # Keep only valid values:
            elem_order = list(set(elem_order) & set(all_orders))
    return elem_order


def topological_elements_extraction3D(img, elem_order=None, simplex=True, use_vq=False, verbose=True):
    """Extract the topological elements coordinates of a 3D labelled image.

    Extract the topological elements of order 2 (*i.e.* cell-wall), 1 (*i.e.* cell-edge)
    and 0 (*i.e.* cell-vertex) by returning their coordinates grouped by pair,
    triplet and quadruplet of labels.

    Parameters
    ----------
    img : timagetk.LabelledImage
        The labelled image to extract topological elements for.
    elem_order : list, optional
        List of dimensional order of the elements to return, should be in [2, 1, 0].
        By default, returns a dictionary with every order of topological elements.
    simplex : bool, optional
        Whether to return only simplicial topological elements (default) or to allow
        elements expressed by longer tuples of labels.
    use_vq : bool, optional
        If ``False`` (default),
        Else, use ``scipy.cluster.vq.vq``

    Returns
    -------
    dict
        Dictionary with topological elements order as key, each containing dictionaries of n-uplets as keys and
        coordinates array as values.

    Example
    -------
    >>> import numpy as np
    >>> from timagetk import TissueImage3D
    >>> from timagetk.array_util import dummy_labelled_image_3D
    >>> from timagetk.algorithms.topological_elements import topological_elements_extraction3D
    >>> img = dummy_labelled_image_3D()
    >>> elem = topological_elements_extraction3D(img)
    """
    import psutil
    mem = psutil.virtual_memory()
    avail_mem = mem.available * 0.8
    req_mem_pointel = _predict_pointel_3d_size(img)
    req_mem_linel = _predict_linel_3d_size(img)
    req_mem_surfel = _predict_surfel_3d_size(img)
    enought_mem_pointel = sum(req_mem_pointel) < avail_mem
    enought_mem_linel = req_mem_pointel[0] + sum(req_mem_linel) < avail_mem
    enought_mem_surfel = req_mem_pointel[0] + req_mem_linel[0] + sum(req_mem_surfel) < avail_mem
    enought_mem_neig_img = req_mem_pointel[0] + req_mem_linel[0] + req_mem_surfel[0] < avail_mem
    if enought_mem_pointel and enought_mem_linel and enought_mem_surfel and enought_mem_neig_img:
        log.info("Using memory hungry algorithm to extract topological elements...")
        return topological_elements_extraction3D_brute(img, elem_order=elem_order, simplex=simplex, use_vq=use_vq,
                                                       quiet=~verbose)
    else:
        log.info("Using memory friendly algorithm to extract topological elements...")
        return topological_elements_extraction3D_bbox(img, elem_order=elem_order, simplex=simplex, use_vq=use_vq)


def topological_elements_extraction3D_bbox(img, elem_order=None, simplex=True, use_vq=False, exclude_labels=None):
    """Extract the topological elements coordinates of a 3D labelled image.

    Extract the topological elements of order 2 (*i.e.* cell-wall), 1 (*i.e.* cell-edge)
    and 0 (*i.e.* cell-vertex) by returning their coordinates grouped by pair,
    triplet and quadruplet of labels.

    Parameters
    ----------
    img : timagetk.LabelledImage
        The labelled image to extract topological elements for.
    elem_order : list, optional
        List of dimensional order of the elements to return, should be in [2, 1, 0].
        By default, returns a dictionary with every order of topological elements.
    simplex : bool, optional
        Whether to return only simplicial topological elements (default) or to allow
        elements expressed by longer tuples of labels.
    use_vq : bool, optional
        If ``False`` (default),
        Else, use ``scipy.cluster.vq.vq``

    Returns
    -------
    dict
        Dictionary with topological elements order as key, each containing dictionaries of n-uplets as keys and
        coordinates array as values.

    Example
    -------
    >>> import numpy as np
    >>> from timagetk import TissueImage3D
    >>> from timagetk.array_util import dummy_labelled_image_3D
    >>> from timagetk.algorithms.topological_elements import topological_elements_extraction3D_brute
    >>> from timagetk.algorithms.topological_elements import topological_elements_extraction3D_bbox
    >>> img = dummy_labelled_image_3D()
    >>> elem_bbox = topological_elements_extraction3D_bbox(img)
    >>> elem = topological_elements_extraction3D_brute(img)

    """
    from tqdm.autonotebook import tqdm
    from timagetk import TissueImage3D
    from timagetk.algorithms.slices import dilation_by

    # Get the list of labels to exclude and make a set:
    if exclude_labels is not None:
        if isinstance(exclude_labels, int):
            exclude_labels = {exclude_labels}
        else:
            exclude_labels = set(exclude_labels)
    else:
        exclude_labels = set([])
    # Add the background to the list of labels to exclude:
    if isinstance(img, TissueImage3D):
        exclude_labels |= {img.background}

    labels = set(img.labels()) - exclude_labels
    bboxes = img.boundingbox(labels)

    elem_order = _check_element_order(elem_order)
    topological_elements = {elt_dim: {} for elt_dim in elem_order}
    for label in tqdm(labels, unit="label"):
        # - Get the slice for given label:
        label_slice = bboxes[label]
        if label_slice is None:
            continue  # skip this label if missing
        label_slice = dilation_by(label_slice, 2)
        topo_elt = topological_elements_extraction3D_brute(img[label_slice].get_array(), elem_order, simplex, use_vq,
                                                           quiet=True)
        # - Add label slicing offsets to coordinates:
        offset = np.array([label_slice[0].start, label_slice[1].start, label_slice[2].start])
        for order in topo_elt:
            if topo_elt[order] is None:
                continue
            # Add offset to current label element for current order:
            for elt_key, elt_coords in topo_elt[order].items():
                topo_elt[order][elt_key] = topo_elt[order][elt_key] + offset
            # Update global dict of element order
            topological_elements[order].update(topo_elt[order])

    return topological_elements


def topological_elements_extraction3D_brute(img, elem_order=None, simplex=True, use_vq=False, quiet=False):
    """Extract the topological elements coordinates of a 3D labelled image.

    Extract the topological elements of order 2 (*i.e.* cell-wall), 1 (*i.e.* cell-edge)
    and 0 (*i.e.* cell-vertex) by returning their coordinates grouped by pair,
    triplet and quadruplet of labels.

    Parameters
    ----------
    img : numpy.ndarray
        Array representing a labelled image.
    elem_order : list, optional
        List of dimensional order of the elements to return, should be in [2, 1, 0].
        By default, returns a dictionary with every order of topological elements.
    simplex : bool, optional
        Whether to return only simplicial topological elements (default) or to allow
        elements expressed by longer tuples of labels.
    use_vq : bool, optional
        If ``False`` (default),
        Else, use ``scipy.cluster.vq.vq``

    Returns
    -------
    dict
        Dictionary with topological elements order as key, each containing dictionaries of n-uplets as keys and
        coordinates array as values.

    Notes
    -----
    A "surfel" is a dimension 2 element with a neighborhood size equal to 2.
    A "linel" is a dimension 1 element with a neighborhood size equal to 3.
    A "pointel" is a dimension 0 element with a neighborhood size equal to 4.

    See Also
    --------
    scipy.cluster.vq.vq

    Example
    -------
    >>> import numpy as np
    >>> from timagetk.array_util import dummy_labelled_image_3D
    >>> from timagetk.algorithms.topological_elements import topological_elements_extraction3D
    >>> img = dummy_labelled_image_3D()
    >>> img.shape
    (5, 11, 11)
    >>> # Extract topological elements coordinates:
    >>> elem = topological_elements_extraction3D(img)
    >>> # Get the cell-vertex coordinates between labels 1, 2, 3 and 4
    >>> elem[0]
    {(1, 2, 3, 4): array([[0.5, 3.5, 3.5]]),
     (1, 2, 3, 7): array([[0.5, 7.5, 3.5]]),
     (1, 3, 4, 5): array([[0.5, 3.5, 8.5]]),
     (1, 3, 5, 6): array([[0.5, 7.5, 9.5]]),
     (1, 3, 6, 7): array([[ 0.5, 10.5,  6.5]])}
    >>> # Get the linel voxel coordinates between labels 1, 2 and 3:
    >>> elem[1][(1, 2, 3)]
    array([[0.5, 4. , 3.5],
           [0.5, 5. , 3.5],
           [0.5, 6. , 3.5],
           [0.5, 7. , 3.5]])
    >>> # Get the surfel voxel coordinates between labels 6 and 7:
    >>> elem[2][(6, 7)]
    array([[ 1. , 11. ,  6.5],
           [ 1. , 12. ,  6.5],
           [ 2. , 11. ,  6.5],
           [ 2. , 12. ,  6.5],
           [ 3. , 11. ,  6.5],
           [ 3. , 12. ,  6.5],
           [ 4. , 11. ,  6.5],
           [ 4. , 12. ,  6.5]])

    """
    try:
        assert img.ndim == 3
    except AssertionError:
        log.error(f"This method work with a 3D image, got a {img.ndim}D image!")
        return None

    elem_order = _check_element_order(elem_order)
    str_order = ', '.join(map(str, elem_order))
    if not quiet:
        log.info(f"Detecting topological elements of order{'s' if len(elem_order) > 1 else ''}: {str_order}.")

    neighborhood_images = {}
    element_coords = {}
    if 0 in elem_order:
        if not quiet:
            log.info(f"Computing 3D pointel coordinates...")
        neighborhood_images[0], element_coords[0] = _pointel_3d(img)

    if 1 in elem_order:
        if not quiet:
            log.info(f"Computing 3D linel coordinates...")
        neighborhood_images[1], element_coords[1] = _linel_3d(img)

    if 2 in elem_order:
        if not quiet:
            log.info(f"Computing 3D surfel coordinates...")
        neighborhood_images[2], element_coords[2] = _surfel_3d(img)

    # - Separate the voxels depending on the size of their neighborhood:
    #   a "surfel" is a dimension 2 element with a neighborhood size == 2;
    #   a "linel" is a dimension 1 element with a neighborhood size == 3;
    #   a "pointel" is a dimension 0 element with a neighborhood size == 4+;
    topological_elements = {}
    element_types = {0: 'pointels', 1: 'linels', 2: 'surfels'}
    for dimension in elem_order:
        neighborhood_img = neighborhood_images[dimension]
        elem_type = element_types[dimension]
        msg = "  - Creating dictionary of {} coordinates detected as topological elements (n={})..."
        log.debug(msg.format(elem_type, len(neighborhood_img)))

        if dimension < 2:
            log.debug(f"  -- Computing the {elem_type} neighborhood size...")
            start_time = time.time()
            # - Keep the "unique values" in each neighborhood:
            # neighborhoods = Pool().map(np.unique, neighborhood_img)
            neighborhoods = list(map(stuple, map(set, neighborhood_img)))
            neighborhoods = np.array(neighborhoods, dtype=object)
            # - Compute the neighborhood size of each voxel:
            # neighborhood_size = Pool().map(len, neighborhoods)
            neighborhood_size = list(map(len, neighborhoods))
            neighborhood_size = np.array(neighborhood_size)

            # - Make a mask indexing desired 'neighborhood_size':
            if simplex:
                dim_mask = neighborhood_size == (4 - dimension)
            else:
                dim_mask = neighborhood_size >= (4 - dimension)
            log.debug(f"    Computed {elem_type} neighborhood sizes: {elapsed_time(start_time)}")
        else:
            # - No need to compute for (non-flat) surfels: always 2 cells
            neighborhoods = np.sort(neighborhood_img)
            neighborhood_size = 2 * np.ones(len(neighborhood_img), int)
            dim_mask = np.ones(len(neighborhood_img)).astype(bool)

        # - Get all coordinates corresponding to selected 'neighborhood_size':
        element_points = element_coords[dimension][dim_mask]

        if simplex:
            # - Get labels list for this given 'neighborhood_size':
            element_cells = np.array(list(neighborhoods[dim_mask]), int)
        else:
            element_cells = neighborhoods[dim_mask]
            neighborhood_size = neighborhood_size[dim_mask]

        if simplex:
            # - In case of "cell-vertex" try to find 5-neighborhood:
            if (dimension == 0) & ((neighborhood_size >= 5).sum() > 0):
                start_time = time.time()

                def _neighborhood_size_5():
                    # - Make a mask indexing 'neighborhood_size == 5':
                    mask_5 = neighborhood_size == 5
                    # - Get all coordinates for 'neighborhood_size == 5':
                    try:
                        clique_pointel_points = np.concatenate([(p, p, p, p, p) for p in element_coords[dimension][mask_5]])
                    except ValueError as e:
                        log.error(e)
                        return None, None
                    # - Get labels list for 'neighborhood_size == 5':
                    try:
                        clique_pointel_cells = np.concatenate(
                            [[np.concatenate([p[:i], p[i + 1:]]) for i in range(5)] for p in
                             neighborhoods[mask_5]]).astype(int)
                    except ValueError as e:
                        log.error(e)
                        return None, None
                    return clique_pointel_points, clique_pointel_cells

                clique_pointel_points, clique_pointel_cells = _neighborhood_size_5()
                # - Add them to the 4-neighborhood arrays of coordinates and labels:
                if clique_pointel_cells is not None:
                    element_points = np.concatenate([element_points, clique_pointel_points])
                    element_cells = np.concatenate([element_cells, clique_pointel_cells])
                    log.debug(
                        f"    Added {len(clique_pointel_cells)} over-connected {elem_type}: {elapsed_time(start_time)}")

            if (dimension == 1) & ((neighborhood_size == 4).sum() > 0):
                start_time = time.time()
                # - Make a mask indexing 'neighborhood_size == 4':
                mask_4 = neighborhood_size == 4
                # - Get all coordinates for 'neighborhood_size == 4':
                clique_linel_points = np.concatenate(
                    [(p, p, p, p) for p in element_coords[dimension][mask_4]])
                # - Get labels list for 'neighborhood_size == 4':
                clique_linel_cells = np.concatenate(
                    [[np.concatenate([p[:i], p[i + 1:]]) for i in range(4)] for p in
                     neighborhoods[mask_4]]).astype(int)
                # - Add them to the 3-neighborhood arrays of coordinates and labels:
                element_points = np.concatenate(
                    [element_points, clique_linel_points])
                element_cells = np.concatenate([element_cells, clique_linel_cells])
                log.debug(f"    Added {len(clique_linel_cells)} over-connected {elem_type}: {elapsed_time(start_time)}")

        msg = "  -- Sorting {} {} as topological elements of order {}..."
        log.debug(msg.format(len(element_cells), elem_type, dimension))
        start_time = time.time()
        if len(element_cells) > 0:
            if simplex:
                # - Remove duplicate of labels n-uplets, with 'n = 4 - dim':
                unique_cell_elements = np.unique(element_cells, axis=0)
                # - ??
                if use_vq:
                    match_start_time = time.time()
                    element_matching = vq(element_cells, unique_cell_elements)[0]
                    # - Make a dictionary of all {(n-uplet) : np.array(coordinates)}:

                    match_start_time = time.time()
                    topological_elements[dimension] = dict(
                        zip([tuple(e) for e in unique_cell_elements],
                            [element_points[element_matching == e] for e, _ in
                             enumerate(unique_cell_elements)]))
                else:
                    match_start_time = time.time()
                    element_sorting = np.lexsort(element_cells.T[::-1])
                    sorted_element_cells = element_cells[element_sorting]
                    sorted_element_points = element_points[element_sorting]

                    match_start_time = time.time()
                    topological_elements[dimension] = {}
                    for element in unique_cell_elements:
                        i_left = 0
                        i_right = len(sorted_element_cells)
                        for k in range(4 - dimension):
                            i_left_k = bisect.bisect_left(sorted_element_cells[i_left:i_right, k], element[k])
                            i_right_k = bisect.bisect_right(sorted_element_cells[i_left:i_right, k], element[k])
                            i_right = i_left + i_right_k
                            i_left = i_left + i_left_k
                        topological_elements[dimension][tuple(element)] = sorted_element_points[i_left:i_right]
            else:
                topological_elements[dimension] = {}

                element_neighborhood_sizes = np.unique(neighborhood_size)[::-1]
                # if dimension != 0:
                #    element_neighborhood_sizes = [4-dimension]

                for s in element_neighborhood_sizes:
                    size_element_cells = np.array(list(element_cells[neighborhood_size == s]), int)
                    size_element_points = element_points[neighborhood_size == s]

                    unique_cell_elements = np.unique(size_element_cells, axis=0)

                    match_start_time = time.time()
                    element_sorting = np.lexsort(size_element_cells.T[::-1])
                    sorted_element_cells = size_element_cells[element_sorting]
                    sorted_element_points = size_element_points[element_sorting]

                    match_start_time = time.time()
                    for element in unique_cell_elements:
                        i_left = 0
                        i_right = len(sorted_element_cells)
                        for k in range(s):
                            i_left_k = bisect.bisect_left(sorted_element_cells[i_left:i_right, k], element[k])
                            i_right_k = bisect.bisect_right(sorted_element_cells[i_left:i_right, k], element[k])
                            i_right = i_left + i_right_k
                            i_left = i_left + i_left_k

                        if dimension == 0:
                            overlapping_elements = [e for e in topological_elements[dimension].keys()
                                                    if np.all([c in e for c in element])]
                            for e in overlapping_elements:
                                topological_elements[dimension][e] = np.concatenate(
                                    [topological_elements[dimension][e], sorted_element_points[i_left:i_right]])
                        topological_elements[dimension][tuple(element)] = sorted_element_points[i_left:i_right]

        else:
            log.warning(f"Could not find topological elements of order {dimension}!")
            topological_elements[dimension] = None
        log.debug(f"    Sorted topological elements of order {dimension} ({elem_type}): {elapsed_time(start_time)}")

    return topological_elements


def _predict_pointel_3d_size(img):
    "Rough prediction of the required memory to call ``_pointel_3d``."
    sh = np.array(img.shape)
    n_nei_vox = 8
    n_elements = (sh[0] - 1) * (sh[1] - 1) * (sh[2] - 1)

    neighborhood_img_size = n_elements * n_nei_vox * np.ones((1), dtype=img.dtype).nbytes
    pointel_coords_size = 3 * n_elements * np.ones((1), dtype='int64').nbytes
    return neighborhood_img_size, pointel_coords_size


def _pointel_3d(img):
    """Compute 3D pointel coordinates.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.array_util import dummy_labelled_image_3D
    >>> from timagetk.algorithms.topological_elements import _pointel_3d
    >>> img = dummy_labelled_image_3D()
    >>> _pointel_3d(img)
    >>>
    >>>

    """
    sh = np.array(img.shape)
    n_nei_vox = 8
    n_elements = (sh[0] - 1) * (sh[1] - 1) * (sh[2] - 1)

    log.debug(f"  - Computing the cell neighborhood of image pointels (n={n_elements})...")
    start_time = time.time()
    neighborhood_img = []
    for x in np.arange(-1, 1):
        for y in np.arange(-1, 1):
            for z in np.arange(-1, 1):
                neighborhood_img.append(img[1 + x:sh[0] + x, 1 + y:sh[1] + y, 1 + z:sh[2] + z])
    # - Reshape the neighborhood matrix in an ``N_voxel x n_nei_vox`` matrix:
    neighborhood_img = np.sort(np.transpose(neighborhood_img, (1, 2, 3, 0))).reshape((sh - 1).prod(), n_nei_vox)
    log.debug(f"    Computed pointel neighborhoods: {elapsed_time(start_time)}")

    log.debug("  - Removing pointels surrounded only by 1 cell...")
    start_time = time.time()
    # - Detect the voxels that are not alone (only neighbors to themselves, or same label around):
    non_flat = np.sum(neighborhood_img == np.tile(neighborhood_img[:, :1], (1, n_nei_vox)), axis=1) != n_nei_vox
    # - Keep only these "non-flat" neighborhood:
    neighborhood_img = neighborhood_img[non_flat]
    n_pointels = neighborhood_img.shape[0]
    pc = float(n_elements - n_pointels) / n_elements * 100
    log.debug(f"    Removed {round(pc, 3)}% of the initial pointels: {elapsed_time(start_time)}")

    log.debug("  - Creating the associated coordinate array...")
    start_time = time.time()
    # - Create the coordinate matrix associated to each voxel of the neighborhood matrix:
    pointel_coords = np.transpose(
        np.mgrid[0:sh[0] - 1, 0:sh[1] - 1, 0:sh[2] - 1], (1, 2, 3, 0)).reshape((sh - 1).prod(), 3) + 0.5
    # - Keep only these "non-flat" coordinates:
    pointel_coords = pointel_coords[non_flat]
    log.debug(f"    Computed {len(pointel_coords)} pointel coordinates: {elapsed_time(start_time)}")

    return neighborhood_img, pointel_coords


def _predict_linel_3d_size(img):
    "Rough prediction of the required memory to call ``_linel_3d``."
    sh = np.array(img.shape)
    n_nei_vox = 4
    x_elements = sh[0] * (sh[1] - 1) * (sh[2] - 1)
    y_elements = (sh[0] - 1) * sh[1] * (sh[2] - 1)
    z_elements = (sh[0] - 1) * (sh[1] - 1) * sh[2]

    x_nei_size = x_elements * n_nei_vox
    y_nei_size = y_elements * n_nei_vox
    z_nei_size = z_elements * n_nei_vox
    bs = np.ones((1), dtype=img.dtype).nbytes
    neighborhood_img_size = x_nei_size * bs + y_nei_size * bs + z_nei_size * bs

    coords_bs = np.ones((1), dtype='int64').nbytes
    pointel_coords_size = 3 * x_elements * coords_bs + 3 * y_elements * coords_bs + 3 * z_elements * coords_bs

    return neighborhood_img_size, pointel_coords_size


def _linel_3d(img):
    sh = np.array(img.shape)
    n_nei_vox = 4
    n_elements = 0
    n_elements += sh[0] * (sh[1] - 1) * (sh[2] - 1)
    n_elements += (sh[0] - 1) * sh[1] * (sh[2] - 1)
    n_elements += (sh[0] - 1) * (sh[1] - 1) * sh[2]
    total = 0
    log.debug(f"  - Computing the cell neighborhood of image linels (n={n_elements})...")
    start_time = time.time()
    # - Extract x-oriented linels
    x_neighborhood_img = []
    for y in np.arange(-1, 1):
        for z in np.arange(-1, 1):
            x_neighborhood_img.append(img[:, 1 + y:sh[1] + y, 1 + z:sh[2] + z])
    x_neighborhood_shape = (sh[0] * (sh[1] - 1) * (sh[2] - 1), n_nei_vox)
    x_neighborhood_img = np.sort(np.transpose(x_neighborhood_img, (1, 2, 3, 0))).reshape(x_neighborhood_shape)
    total += len(x_neighborhood_img)

    non_flat = np.sum(x_neighborhood_img == np.tile(x_neighborhood_img[:, :1], (1, n_nei_vox)), axis=1) != n_nei_vox
    # - Keep only these "non-flat" neighborhood:
    x_neighborhood_img = x_neighborhood_img[non_flat]

    x_linel_coords = np.transpose(
        np.mgrid[0:sh[0], 0:sh[1] - 1, 0:sh[2] - 1], (1, 2, 3, 0)).reshape(
        sh[0] * (sh[1] - 1) * (sh[2] - 1), 3) + np.array([0., 0.5, 0.5])
    # - Keep only these "non-flat" coordinates:
    x_linel_coords = x_linel_coords[non_flat]

    # - Extract y-oriented linels
    y_neighborhood_img = []
    for x in np.arange(-1, 1):
        for z in np.arange(-1, 1):
            y_neighborhood_img.append(img[1 + x:sh[0] + x, :, 1 + z:sh[2] + z])
    y_neighborhood_shape = ((sh[0] - 1) * sh[1] * (sh[2] - 1), n_nei_vox)
    y_neighborhood_img = np.sort(np.transpose(y_neighborhood_img, (1, 2, 3, 0))).reshape(y_neighborhood_shape)
    total += len(y_neighborhood_img)

    non_flat = np.sum(y_neighborhood_img == np.tile(y_neighborhood_img[:, :1], (1, n_nei_vox)), axis=1) != n_nei_vox
    # - Keep only these "non-flat" neighborhood:
    y_neighborhood_img = y_neighborhood_img[non_flat]

    y_linel_coords = np.transpose(
        np.mgrid[0:sh[0] - 1, 0:sh[1], 0:sh[2] - 1], (1, 2, 3, 0)).reshape(
        (sh[0] - 1) * sh[1] * (sh[2] - 1), 3) + np.array([0.5, 0., 0.5])
    # - Keep only these "non-flat" coordinates:
    y_linel_coords = y_linel_coords[non_flat]

    # - Extract z-oriented linels
    z_neighborhood_img = []
    for x in np.arange(-1, 1):
        for y in np.arange(-1, 1):
            z_neighborhood_img.append(img[1 + x:sh[0] + x, 1 + y:sh[1] + y, :])
    z_neighborhood_shape = ((sh[0] - 1) * (sh[1] - 1) * sh[2], n_nei_vox)
    z_neighborhood_img = np.sort(np.transpose(z_neighborhood_img, (1, 2, 3, 0))).reshape(z_neighborhood_shape)
    total += len(z_neighborhood_img)

    non_flat = np.sum(
        z_neighborhood_img == np.tile(z_neighborhood_img[:, :1], (1, n_nei_vox)),
        axis=1) != n_nei_vox
    # - Keep only these "non-flat" neighborhood:
    z_neighborhood_img = z_neighborhood_img[non_flat]

    z_linel_coords = np.transpose(
        np.mgrid[0:sh[0] - 1, 0:sh[1] - 1, 0:sh[2]], (1, 2, 3, 0)).reshape(
        (sh[0] - 1) * (sh[1] - 1) * sh[2], 3) + np.array([0.5, 0.5, 0.])
    # - Keep only these "non-flat" coordinates:
    z_linel_coords = z_linel_coords[non_flat]
    log.debug(f"    Computed linel neighborhoods: {elapsed_time(start_time)}")

    start_time = time.time()
    neighborhood_img = []
    neighborhood_img += list(x_neighborhood_img)
    neighborhood_img += list(y_neighborhood_img)
    neighborhood_img += list(z_neighborhood_img)
    neighborhood_img = np.array(neighborhood_img)

    n_linels = neighborhood_img.shape[0]
    pc = float(n_elements - n_linels) / n_elements * 100
    log.debug(f"    Removed {round(pc, 3)}% of the initial linels: {elapsed_time(start_time)}")

    log.debug("  - Creating the associated coordinate array...")
    start_time = time.time()
    linel_coords = []
    linel_coords += list(x_linel_coords)
    linel_coords += list(y_linel_coords)
    linel_coords += list(z_linel_coords)
    linel_coords = np.array(linel_coords)
    log.debug(f"    Computed {len(linel_coords)} linel coordinates: {elapsed_time(start_time)}")

    return neighborhood_img, linel_coords


def _predict_surfel_3d_size(img):
    "Rough prediction of the required memory to call ``_surfel_3d``."
    sh = np.array(img.shape)
    n_nei_vox = 4
    x_elements = sh[0] * sh[1] * (sh[2] - 1)
    y_elements = sh[0] * (sh[1] - 1) * sh[2]
    z_elements = (sh[0] - 1) * sh[1] * sh[2]

    x_nei_size = x_elements * n_nei_vox
    y_nei_size = y_elements * n_nei_vox
    z_nei_size = z_elements * n_nei_vox
    bs = np.ones((1), dtype=img.dtype).nbytes
    neighborhood_img_size = x_nei_size * bs + y_nei_size * bs + z_nei_size * bs

    coords_bs = np.ones((1), dtype='int64').nbytes
    pointel_coords_size = 3 * x_elements * coords_bs + 3 * y_elements * coords_bs + 3 * z_elements * coords_bs

    return neighborhood_img_size, pointel_coords_size


def _surfel_3d(img):
    sh = np.array(img.shape)
    n_nei_vox = 2
    n_elements = 0
    n_elements += sh[0] * sh[1] * (sh[2] - 1)
    n_elements += sh[0] * (sh[1] - 1) * sh[2]
    n_elements += (sh[0] - 1) * sh[1] * sh[2]
    total = 0
    log.debug(f"  - Computing the cell neighborhood of image surfels (n={n_elements})...")
    start_time = time.time()
    # - Extract xy-oriented surfels
    xy_neighborhood_img = []
    for z in np.arange(-1, 1):
        xy_neighborhood_img.append(img[:, :, 1 + z:sh[2] + z])
    xy_neighborhood_img = np.sort(np.transpose(xy_neighborhood_img, (1, 2, 3, 0))).reshape(sh[0] * sh[1] * (sh[2] - 1),
                                                                                           n_nei_vox)
    total += len(xy_neighborhood_img)

    non_flat = np.sum(
        xy_neighborhood_img == np.tile(xy_neighborhood_img[:, :1], (1, n_nei_vox)),
        axis=1) != n_nei_vox
    # - Keep only these "non-flat" neighborhood:
    xy_neighborhood_img = xy_neighborhood_img[non_flat]

    xy_surfel_coords = np.transpose(
        np.mgrid[0:sh[0], 0:sh[1], 0:sh[2] - 1], (1, 2, 3, 0)).reshape(
        sh[0] * sh[1] * (sh[2] - 1), 3) + np.array([0., 0., 0.5])
    # - Keep only these "non-flat" coordinates:
    xy_surfel_coords = xy_surfel_coords[non_flat]

    # - Extract xz-oriented surfels
    xz_neighborhood_img = []
    for y in np.arange(-1, 1):
        xz_neighborhood_img.append(img[:, 1 + y:sh[1] + y, :])
    xz_neighborhood_img = np.sort(np.transpose(xz_neighborhood_img, (1, 2, 3, 0))).reshape(sh[0] * (sh[1] - 1) * sh[2],
                                                                                           n_nei_vox)
    total += len(xz_neighborhood_img)

    non_flat = np.sum(xz_neighborhood_img == np.tile(xz_neighborhood_img[:, :1], (1, n_nei_vox)),
                      axis=1) != n_nei_vox
    # - Keep only these "non-flat" neighborhood:
    xz_neighborhood_img = xz_neighborhood_img[non_flat]

    xz_surfel_coords = np.transpose(
        np.mgrid[0:sh[0], 0:sh[1] - 1, 0:sh[2]], (1, 2, 3, 0)).reshape(
        sh[0] * (sh[1] - 1) * sh[2], 3) + np.array([0., 0.5, 0.])
    # - Keep only these "non-flat" coordinates:
    xz_surfel_coords = xz_surfel_coords[non_flat]

    # - Extract yz-oriented surfels
    yz_neighborhood_img = []
    for x in np.arange(-1, 1):
        yz_neighborhood_img.append(img[1 + x:sh[0] + x, :, :])
    yz_neighborhood_img = np.sort(np.transpose(yz_neighborhood_img, (1, 2, 3, 0))).reshape((sh[0] - 1) * sh[1] * sh[2],
                                                                                           n_nei_vox)
    total += len(yz_neighborhood_img)

    non_flat = np.sum(
        yz_neighborhood_img == np.tile(yz_neighborhood_img[:, :1], (1, n_nei_vox)),
        axis=1) != n_nei_vox
    # - Keep only these "non-flat" neighborhood:
    yz_neighborhood_img = yz_neighborhood_img[non_flat]

    yz_surfel_coords = np.transpose(
        np.mgrid[0:sh[0] - 1, 0:sh[1], 0:sh[2]], (1, 2, 3, 0)).reshape(
        (sh[0] - 1) * sh[1] * sh[2], 3) + np.array([0.5, 0., 0.])
    # - Keep only these "non-flat" coordinates:
    yz_surfel_coords = yz_surfel_coords[non_flat]
    log.debug(f"    Computed surfel neighborhoods: {elapsed_time(start_time)}")

    start_time = time.time()
    neighborhood_img = []
    neighborhood_img += list(xy_neighborhood_img)
    neighborhood_img += list(xz_neighborhood_img)
    neighborhood_img += list(yz_neighborhood_img)
    neighborhood_img = np.array(neighborhood_img)

    n_surfels = neighborhood_img.shape[0]
    pc = float(n_elements - n_surfels) / n_elements * 100
    log.debug(f"    Removed {round(pc, 3)}% of the initial surfels: {elapsed_time(start_time)}")

    log.debug("  - Creating the associated coordinate array...")
    start_time = time.time()
    surfel_coords = []
    surfel_coords += list(xy_surfel_coords)
    surfel_coords += list(xz_surfel_coords)
    surfel_coords += list(yz_surfel_coords)
    surfel_coords = np.array(surfel_coords)
    log.debug(f"    Computed {len(surfel_coords)} surfel coordinates: {elapsed_time(start_time)}")

    return neighborhood_img, surfel_coords


def topological_elements_extraction2D(img, elem_order=None, z_coords=None, simplex=True, use_vq=False, verbose=True):
    """Extract the topological elements coordinates of a 3D labelled image.

    Extract the topological elements of order 1 (ie. wall/interface) and 0
    (ie. cell vertex) by returning their coordinates grouped by pair and
    triplet of labels.

    Parameters
    ----------
    img : numpy.ndarray
        Array representing a labelled image.
    elem_order : list, optional
        List of dimensional order of the elements to return, should be in [2, 1, 0].
        By default, return a dictionary with every order of topological elements.
    z_coords : numpy.ndarray, optional
        Array representing the z coordinate of the 2D image.
    simplex : bool, optional
        Whether to return only simplicial topological elements (default) or to allow
        elements expressed by longer tuples of labels.
    use_vq : bool, optional
        If ``False`` (default),
        Else, use ``scipy.cluster.vq.vq``

    Returns
    -------
    dict
        Dictionary with topological elements order as key, each containing dictionaries of n-uplets as keys and
        coordinates array as values.

    Notes
    -----
    A "pixel" is a dimension 2 element with a neighborhood size equal to 1.
    A "linel" is a dimension 1 element with a neighborhood size equal to 2.
    A "pointel" is a dimension 0 element with a neighborhood size equal to 3.

    See Also
    --------
    scipy.cluster.vq.vq
    """
    try:
        assert img.ndim == 2
    except AssertionError:
        log.error(f"This method work with a 2D image, got a {img.ndim}D image!")
        return None

    elem_order = _check_element_order(elem_order)
    str_order = ', '.join(map(str, elem_order))
    if verbose:
        log.info(f"Detecting topological elements of order{'s' if len(elem_order) > 1 else ''}: {str_order}.")

    if z_coords is not None:
        assert z_coords.shape == img.shape

    neighborhood_images = {}
    element_coords = {}
    if 0 in elem_order:
        neighborhood_images[0], element_coords[0] = _pointel_2d(img, z_coords)

    if 1 in elem_order:
        neighborhood_images[1], element_coords[1] = _linel_2d(img, z_coords)

    if 2 in elem_order:
        neighborhood_images[2], element_coords[2] = _pixel_2d(img, z_coords)

    # - Separate the pixels depending on the size of their neighborhood:
    #   "cell" is a dimension 2 element with a neighborhood size == 1;
    #   "wall" is a dimension 1 element with a neighborhood size == 2;
    #   "cell-vertex" is a dimension 0 element with a neighborhood size == 3+;
    topological_elements = {}
    element_types = {0: 'pointels', 1: 'linels', 2: 'pixels'}
    for dimension in elem_order:
        neighborhood_img = neighborhood_images[dimension]
        elem_type = element_types[dimension]
        msg = "  - Creating dictionary of {} coordinates detected as topological elements (n={})..."
        log.debug(msg.format(elem_type, len(neighborhood_img)))

        if dimension < 1:
            log.debug("  -- Computing the {} neighborhood size...".format(elem_type))
            start_time = time.time()
            # - Keep the "unique values" in each neighborhood:
            # neighborhoods = Pool().map(np.unique, neighborhood_img)
            neighborhoods = list(map(stuple, map(set, neighborhood_img)))
            neighborhoods = np.array(neighborhoods, dtype=object)
            # - Compute the neighborhood size of each pixel:
            # neighborhood_size = Pool().map(len, neighborhoods)
            neighborhood_size = list(map(len, neighborhoods))
            neighborhood_size = np.array(neighborhood_size)
            log.debug(np.unique(neighborhood_size))

            # - Make a mask indexing desired 'neighborhood_size':
            if simplex:
                dim_mask = neighborhood_size == (3 - dimension)
            else:
                dim_mask = neighborhood_size >= (3 - dimension)
            log.debug(f"    Computed {elem_type} neighborhood sizes: {elapsed_time(start_time)}")
        else:
            # - No need to compute for (non-flat) surfels: always 2 cells
            neighborhoods = np.sort(neighborhood_img)
            neighborhood_size = (3 - dimension) * np.ones(len(neighborhood_img), int)
            dim_mask = np.ones(len(neighborhood_img)).astype(bool)

        # - Get all coordinates corresponding to selected 'neighborhood_size':
        element_points = element_coords[dimension][dim_mask]

        if simplex:
            # - Get labels list for this given 'neighborhood_size':
            element_cells = np.array(list(neighborhoods[dim_mask]), int)
        else:
            element_cells = neighborhoods[dim_mask]
            neighborhood_size = neighborhood_size[dim_mask]

        if simplex:
            # - In case of "cell-vertex" try to find 4-neighborhood:
            if (dimension == 0) & ((neighborhood_size == 4).sum() > 0):
                start_time = time.time()
                # - Make a mask indexing 'neighborhood_size == ':
                mask_4 = neighborhood_size == 4
                # - Get all coordinates for 'neighborhood_size == 4':
                clique_pointel_points = np.concatenate(
                    [(p, p, p, p) for p in element_coords[dimension][mask_4]])
                # - Get labels list for 'neighborhood_size == 4':
                clique_pointel_cells = np.concatenate(
                    [[np.concatenate([p[:i], p[i + 1:]]) for i in range(4)] for p in
                     neighborhoods[mask_4]]).astype(int)
                # - Add them to the 4-neighborhood arrays of coordinates and labels:
                element_points = np.concatenate(
                    [element_points, clique_pointel_points])
                element_cells = np.concatenate([element_cells, clique_pointel_cells])
                msg = "\tAdded {} overconnected {}: {}"
                log.debug(msg.format(len(clique_pointel_cells), elem_type, elapsed_time(start_time)))

        msg = "  -- Sorting {} {} as topological elements of order {}..."
        log.debug(msg.format(len(element_cells), elem_type, dimension))
        start_time = time.time()
        if len(element_cells) > 0:
            if simplex:
                # - Remove duplicate of labels n-uplets, with 'n = 4 - dim':
                unique_cell_elements = np.unique(element_cells, axis=0)
                # - ??
                if use_vq:
                    match_start_time = time.time()
                    element_matching = vq(element_cells, unique_cell_elements)[0]
                    # - Make a dictionary of all {(n-uplet) : np.array(coordinates)}:

                    match_start_time = time.time()
                    topological_elements[dimension] = dict(
                        zip([tuple(e) for e in unique_cell_elements],
                            [element_points[element_matching == e] for e, _ in
                             enumerate(unique_cell_elements)]))
                else:
                    match_start_time = time.time()
                    element_sorting = np.lexsort(element_cells.T[::-1])
                    sorted_element_cells = element_cells[element_sorting]
                    sorted_element_points = element_points[element_sorting]

                    match_start_time = time.time()
                    topological_elements[dimension] = {}
                    for element in unique_cell_elements:
                        i_left = 0
                        i_right = len(sorted_element_cells)
                        for k in range(3 - dimension):
                            i_left_k = bisect.bisect_left(sorted_element_cells[i_left:i_right, k], element[k])
                            i_right_k = bisect.bisect_right(sorted_element_cells[i_left:i_right, k], element[k])
                            i_right = i_left + i_right_k
                            i_left = i_left + i_left_k
                        topological_elements[dimension][tuple(element)] = sorted_element_points[i_left:i_right]
            else:
                topological_elements[dimension] = {}

                element_neighborhood_sizes = np.unique(neighborhood_size)[::-1]
                # if dimension != 0:
                #    element_neighborhood_sizes = [4-dimension]
                log.debug(element_neighborhood_sizes)

                for s in element_neighborhood_sizes:
                    size_element_cells = np.array(list(element_cells[neighborhood_size == s]), int)
                    size_element_points = element_points[neighborhood_size == s]

                    unique_cell_elements = np.unique(size_element_cells, axis=0)

                    match_start_time = time.time()
                    element_sorting = np.lexsort(size_element_cells.T[::-1])
                    sorted_element_cells = size_element_cells[element_sorting]
                    sorted_element_points = size_element_points[element_sorting]

                    match_start_time = time.time()
                    for element in unique_cell_elements:
                        i_left = 0
                        i_right = len(sorted_element_cells)
                        for k in range(s):
                            i_left_k = bisect.bisect_left(sorted_element_cells[i_left:i_right, k], element[k])
                            i_right_k = bisect.bisect_right(sorted_element_cells[i_left:i_right, k], element[k])
                            i_right = i_left + i_right_k
                            i_left = i_left + i_left_k

                        # if False:
                        if dimension == 0:
                            overlapping_elements = [e for e in topological_elements[dimension].keys()
                                                    if np.all([c in e for c in element])]
                            for e in overlapping_elements:
                                topological_elements[dimension][e] = np.concatenate(
                                    [topological_elements[dimension][e], sorted_element_points[i_left:i_right]])
                        topological_elements[dimension][tuple(element)] = sorted_element_points[i_left:i_right]

        else:
            log.warning(f"Could not find topological elements of order {dimension}!")
            topological_elements[dimension] = None
        log.debug(f"    Sorted topological elements of order {dimension} ({elem_type}): {elapsed_time(start_time)}")

    return topological_elements


def _pointel_2d(img, z_coords):
    sh = np.array(img.shape)
    n_nei_pix = 4
    n_elements = (sh[0] - 1) * (sh[1] - 1)
    log.debug(f"  - Computing the cell neighborhood of image pointels (n={n_elements})...")
    start_time = time.time()
    neighborhood_img = []
    for x in np.arange(-1, 1):
        for y in np.arange(-1, 1):
            neighborhood_img.append(img[1 + x:sh[0] + x, 1 + y:sh[1] + y])

    # - Reshape the neighborhood matrix in a N_pixel x n_nei_pix:
    neighborhood_img = np.sort(np.transpose(neighborhood_img, (1, 2, 0))).reshape((sh - 1).prod(), n_nei_pix)
    log.debug(f"    Computed pointel neighborhoods: {elapsed_time(start_time)}")

    log.debug("  - Removing pointels surrounded only by 1 cell...")
    start_time = time.time()
    # - Detect the pixels that are not alone (only neighbors to themselves, or same label around):
    non_flat = np.sum(neighborhood_img == np.tile(neighborhood_img[:, :1], (1, n_nei_pix)), axis=1) != n_nei_pix
    # - Keep only these "non-flat" neighborhood:
    neighborhood_img = neighborhood_img[non_flat]

    n_pointels = neighborhood_img.shape[0]
    pc = float(n_elements - n_pointels) / n_elements * 100
    log.debug(f"    Removed {round(pc, 3)}% of the initial pointels: {elapsed_time(start_time)}")

    log.debug("  - Creating the associated coordinate array...")
    start_time = time.time()
    # - Create the coordinate matrix associated to each pixels of the neighborhood matrix:
    pointel_coords = np.mgrid[0:sh[0] - 1, 0:sh[1] - 1]
    pointel_coords = np.transpose(pointel_coords, (1, 2, 0))
    pointel_coords = pointel_coords.reshape((sh - 1).prod(), 2) + 0.5
    if z_coords is not None:
        pointel_z = []
        for x in np.arange(-1, 1):
            for y in np.arange(-1, 1):
                pointel_z.append(z_coords[1 + x:sh[0] + x, 1 + y:sh[1] + y])
        pointel_z = np.mean(pointel_z, axis=0).reshape((sh - 1).prod(), 1)
        pointel_coords = np.concatenate([pointel_coords, pointel_z], axis=-1)

    # - Keep only these "non-flat" coordinates:
    pointel_coords = pointel_coords[non_flat]
    log.debug(f"    Computed {len(pointel_coords)} pointel coordinates: {elapsed_time(start_time)}")

    return neighborhood_img, pointel_coords


def _linel_2d(img, z_coords):
    sh = np.array(img.shape)
    n_nei_pix = 2
    n_elements = 0
    n_elements += sh[0] * (sh[1] - 1)
    n_elements += (sh[0] - 1) * sh[1]
    total = 0
    log.debug(f"  - Computing the cell neighborhood of image linels (n={n_elements})...")
    start_time = time.time()
    # - Extract x-oriented linels
    x_neighborhood_img = []
    for y in np.arange(-1, 1):
        x_neighborhood_img.append(img[:, 1 + y:sh[1] + y])
    x_neighborhood_img = np.sort(np.transpose(x_neighborhood_img, (1, 2, 0))).reshape(sh[0] * (sh[1] - 1), n_nei_pix)
    total += len(x_neighborhood_img)

    non_flat = np.sum(
        x_neighborhood_img == np.tile(x_neighborhood_img[:, :1], (1, n_nei_pix)),
        axis=1) != n_nei_pix
    # - Keep only these "non-flat" neighborhood:
    x_neighborhood_img = x_neighborhood_img[non_flat]

    x_linel_coords = np.mgrid[0:sh[0], 0:sh[1] - 1]
    x_linel_coords = np.transpose(x_linel_coords, (1, 2, 0))
    x_linel_coords = x_linel_coords.reshape(sh[0] * (sh[1] - 1), 2) + np.array([0., 0.5])
    if z_coords is not None:
        x_linel_z = []
        for y in np.arange(-1, 1):
            x_linel_z.append(z_coords[:, 1 + y:sh[1] + y])
        x_linel_z = np.mean(x_linel_z, axis=0).reshape(sh[0] * (sh[1] - 1), 1)
        x_linel_coords = np.concatenate([x_linel_coords, x_linel_z], axis=-1)

    # - Keep only these "non-flat" coordinates:
    x_linel_coords = x_linel_coords[non_flat]

    # - Extract y-oriented linels
    y_neighborhood_img = []
    for x in np.arange(-1, 1):
        y_neighborhood_img.append(img[1 + x:sh[0] + x, :])
    y_neighborhood_img = np.sort(np.transpose(y_neighborhood_img, (1, 2, 0))).reshape((sh[0] - 1) * sh[1], n_nei_pix)
    total += len(y_neighborhood_img)

    non_flat = np.sum(
        y_neighborhood_img == np.tile(y_neighborhood_img[:, :1], (1, n_nei_pix)),
        axis=1) != n_nei_pix
    # - Keep only these "non-flat" neighborhood:
    y_neighborhood_img = y_neighborhood_img[non_flat]

    y_linel_coords = np.mgrid[0:sh[0] - 1, 0:sh[1]]
    y_linel_coords = np.transpose(y_linel_coords, (1, 2, 0))
    y_linel_coords = y_linel_coords.reshape((sh[0] - 1) * sh[1], 2) + np.array([0.5, 0.])
    if z_coords is not None:
        y_linel_z = []
        for x in np.arange(-1, 1):
            y_linel_z.append(z_coords[1 + x:sh[0] + x, :])
        y_linel_z = np.mean(y_linel_z, axis=0).reshape((sh[0] - 1) * sh[1], 1)
        y_linel_coords = np.concatenate([y_linel_coords, y_linel_z], axis=-1)

    # - Keep only these "non-flat" coordinates:
    y_linel_coords = y_linel_coords[non_flat]
    log.debug(f"    Computed linel neighborhoods: {elapsed_time(start_time)}")

    start_time = time.time()
    neighborhood_img = []
    neighborhood_img += list(x_neighborhood_img)
    neighborhood_img += list(y_neighborhood_img)
    neighborhood_img = np.array(neighborhood_img)

    n_linels = neighborhood_img.shape[0]
    pc = float(n_elements - n_linels) / n_elements * 100
    log.debug(f"    Removed {round(pc, 3)}% of the initial linels: {elapsed_time(start_time)}")

    log.debug("  - Creating the associated coordinate array...")
    start_time = time.time()
    linel_coords = []
    linel_coords += list(x_linel_coords)
    linel_coords += list(y_linel_coords)
    linel_coords = np.array(linel_coords)
    log.debug(f"    Computed {len(linel_coords)} linel coordinates: {elapsed_time(start_time)}")

    return neighborhood_img, linel_coords


def _pixel_2d(img, z_coords):
    sh = np.array(img.shape)
    n_elements = 0
    n_elements += sh[0] * sh[1]

    log.debug(f"  - Computing the cell neighborhood of image pixels (n={n_elements})...")
    start_time = time.time()

    neighborhood_img = img[:, :].reshape((n_elements, 1))
    log.debug(f"    Computed pixel neighborhoods: {elapsed_time(start_time)}")

    log.debug("  - Creating the associated coordinate array...")
    start_time = time.time()
    pixel_coords = np.transpose(np.mgrid[0:sh[0], 0:sh[1]], (1, 2, 0)).reshape((n_elements, 2))
    if z_coords is not None:
        pixel_z = z_coords.reshape((n_elements, 1))
        pixel_coords = np.concatenate([pixel_coords, pixel_z], axis=-1)
    pixel_coords = pixel_coords.astype(float)

    log.debug(f"    Computed {len(pixel_coords)} pixel coordinates: {elapsed_time(start_time)}")
    return neighborhood_img, pixel_coords
