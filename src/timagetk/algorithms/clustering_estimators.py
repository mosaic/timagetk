#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Clustering Estimators Module

This module contains a comprehensive set of functions for evaluating and measuring the effectiveness of clustering
algorithms. It provides utilities for computing core clustering metrics that aid in cluster analysis, validation, and comparison.

Purpose
The module is designed for researchers and developers working on clustering-based machine learning and data processing
tasks. Its functionality enables quantifiable evaluation of clustering results to improve accuracy, detect patterns, and
refine cluster identification methods.

Key Features

- **Distance Metrics**: Compute distances between elements and clusters to measure closeness or separation.
- **Cluster Statistics**: Evaluate key clustering properties such as diameters, separation, and within/between cluster distances.
- **Validation Indices**: Calculate statistical scores like the Calinski-Harabasz index and Hartigan's index to validate cluster quality.
- **Global Metrics**: Aggregate cluster-level metrics to generate global statistics for comparative analysis.


This module helps users efficiently analyze clustering results by implementing robust mathematical techniques and
standard approaches, ensuring accurate and meaningful evaluations.
"""

import numpy as np

import timagetk
from timagetk.algorithms.clustering import _check_clustering
from timagetk.algorithms.clustering import _check_clustering
from timagetk.algorithms.clustering import _check_clustering
from timagetk.algorithms.clustering import _check_clustering
from timagetk.algorithms.clustering import _check_clustering
from timagetk.algorithms.clustering import _check_distance_matrix_clustering
from timagetk.algorithms.clustering import _check_distance_matrix_clustering
from timagetk.algorithms.clustering import _check_distance_matrix_clustering
from timagetk.algorithms.clustering import _check_distance_matrix_clustering
from timagetk.algorithms.clustering import _check_distance_matrix_clustering
from timagetk.algorithms.clustering import cluster_ids
from timagetk.algorithms.clustering import cluster_ids
from timagetk.algorithms.clustering import cluster_ids
from timagetk.algorithms.clustering import cluster_ids
from timagetk.algorithms.clustering import cluster_ids
from timagetk.algorithms.clustering import cluster_ids
from timagetk.algorithms.clustering import n_elements_by_cluster
from timagetk.algorithms.clustering import n_elements_by_cluster
from timagetk.algorithms.clustering import n_elements_by_cluster
from timagetk.algorithms.clustering import n_elements_by_cluster
from timagetk.components.clustering import example_clusterer
from timagetk.components.clustering import example_clusterer
from timagetk.components.clustering import example_clusterer
from timagetk.components.clustering import example_clusterer
from timagetk.components.clustering import example_clusterer
from timagetk.components.clustering import example_clusterer


def _element_distance_to_cluster(distance_matrix, clustering, element_index, cluster, **kwargs):
    """Compute the distances of an element to a cluster.

    Parameters
    ----------
    distance_matrix : numpy.ndarray
        Distance matrix used to create the clustering.
    clustering : list
        Resulting list of cluster ids.
    element_index : int
        Index of the element to consider.
    cluster : int or str
        Id of the cluster to consider.

    Other Parameters
    ----------------
    n_elt_in_cluster : int
        The number of element in the cluster.

    Returns
    -------
    float
        The distance of the element to the cluster.

    """
    # Get the index of all elements in cluster `q`:
    index_q = np.where(np.array(clustering) == cluster)[0].tolist()
    # Get the number of elements in the cluster:
    n_elt_in_cluster = kwargs.get('n_elt_in_cluster', None)
    if n_elt_in_cluster is None:
        n_elt_in_cluster = len(index_q)

    if clustering[element_index] == cluster:
        # If the element belong to cluster:
        # remove the element index from the list of element indexes
        index_q.remove(element_index)
        return np.sum(distance_matrix[element_index, index_q]) / (n_elt_in_cluster - 1)
    else:
        return np.sum(distance_matrix[element_index, index_q]) / n_elt_in_cluster


def element_distance_to_cluster(distance_matrix, clustering, outliers_cluster_id=None):
    """Compute the distances of all elements to each cluster.

    Parameters
    ----------
    distance_matrix : numpy.ndarray
        Distance matrix used to create the clustering.
    clustering : list
        Resulting list of cluster ids.
    outliers_cluster_id : None or int or str, optional
        The id of the cluster representing the outliers. It will be ignored.

    Returns
    -------
    dict of float
        The dictionary of elements distances to each cluster.
        Indexed by a tuple of element index and cluster id ``{(i, q): D(i, q)}``.

    Examples
    --------
    >>> from timagetk.algorithms.clustering_estimators import element_distance_to_cluster
    >>> from timagetk.components.clustering import example_clusterer
    >>> clust = example_clusterer()
    >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5])
    >>> clust.compute_clustering(method='ward', n_clusters=4)
    >>> Diq = element_distance_to_cluster(clust._global_pw_dist_mat, list(clust._clustering.values()))
    >>> D_tcid_q = {clust.ids[i]: {q: Diq[(i, q)] for q in range(clust._nb_clusters)} for i in range(len(clust.ids))}
    >>> D_tcid_q[(2, 129)]
    {0: 1.123208240009025,
     1: 0.6857721942484629,
     2: 1.3660433957015157,
     3: 1.993538515433321}

    """
    _check_distance_matrix_clustering(distance_matrix, clustering)
    _check_clustering(clustering, outliers_cluster_id)
    clusters_ids = cluster_ids(clustering, outliers_cluster_id)
    n_elt_by_cluster = n_elements_by_cluster(clustering, outliers_cluster_id)

    N_elt = len(clustering)
    return {(element_index, cluster): _element_distance_to_cluster(distance_matrix, clustering, element_index,
                                                                   cluster, n_elt_by_cluster=n_elt_by_cluster[cluster])
            for cluster in clusters_ids for element_index in range(N_elt)}


def cluster_diameters(distance_matrix, clustering, outliers_cluster_id=None):
    """Compute the *cluster diameter* metrics, that is the max distance between two vertex from the same cluster.

    Parameters
    ----------
    distance_matrix : numpy.ndarray
        Distance matrix used to create the clustering.
    clustering : list
        Resulting list of cluster ids.
    outliers_cluster_id : None or int or str, optional
        The id of the cluster representing the outliers. It will be ignored.

    Returns
    -------
    dict of float
        Dictionary of *cluster diameter* indexed by cluster id.

    Raises
    ------
    ValueError
        If the distance matrix is not a 2D matrix.
        If the distance matrix is not a square matrix.
        If the distance matrix and list of cluster ids are not of same size.
        If there is at least one cluster with a single element.
        If there is only one cluster.

    Notes
    -----
    The *diameter* of a cluster :math:`d(q)` is defined as follows:
    :math:`d(q) = \text{max}_{i,j \in q} D(i,j)`, where
    :math:`D(i,j)` is the distance between elements :math:`i` and :math:`j`,
    and :math:`q` a cluster.

    Examples
    --------
    >>> from timagetk.algorithms.clustering_estimators import cluster_diameters
    >>> from timagetk.components.clustering import example_clusterer
    >>> clust = example_clusterer()
    >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5])
    >>> clust.compute_clustering(method='ward', n_clusters=4)
    >>> diameters = cluster_diameters(clust._global_pw_dist_mat, list(clust._clustering.values()))
    >>> diameters
    {0: 5.388555219022697,
     2: 3.156664256657462,
     1: 1.8189028239633693,
     3: 1.2958967302391322}

    """
    _check_distance_matrix_clustering(distance_matrix, clustering)
    _check_clustering(clustering, outliers_cluster_id)
    clusters_ids = cluster_ids(clustering, outliers_cluster_id)

    diameters = {}
    for q in clusters_ids:
        # Get the index of all elements in cluster `q`:
        index_q = np.where(np.array(clustering) == q)[0].tolist()
        diameters[q] = max([distance_matrix[i, j] for i in index_q for j in index_q])

    return diameters


def cluster_separation(distance_matrix, clustering, outliers_cluster_id=None):
    """Compute the *cluster separation* metrics, that is the min distance between two vertex from two different clusters.

    Parameters
    ----------
    distance_matrix : numpy.ndarray
        Distance matrix used to create the clustering.
    clustering : list
        Resulting list of cluster ids.
    outliers_cluster_id : None or int or str, optional
        The id of the cluster representing the outliers. It will be ignored.

    Returns
    -------
    dict of float
        Dictionary of *cluster separation* by cluster id.

    Raises
    ------
    ValueError
        If the distance matrix is not a 2D matrix.
        If the distance matrix is not a square matrix.
        If the distance matrix and list of cluster ids are not of same size.
        If there is at least one cluster with a single element.
        If there is only one cluster.

    Notes
    -----
    The *diameter* of a cluster :math:`d(q)` is defined as follows:
    :math:`\text{min}_{i \in q, j \not\in q} D(j,i)`, where
    :math:`D(i,j)` is the distance between elements :math:`i` and :math:`j`,
    and :math:`q` a cluster.

    Examples
    --------
    >>> from timagetk.algorithms.clustering_estimators import cluster_separation
    >>> from timagetk.components.clustering import example_clusterer
    >>> clust = example_clusterer()
    >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5])
    >>> clust.compute_clustering(method='ward', n_clusters=4)
    >>> separation = cluster_separation(clust._global_pw_dist_mat, list(clust._clustering.values()))
    >>> separation
    {0: 0.00017341050852925628, 2: 0.0, 1: 0.00017341050852925628, 3: 0.0}

    """
    _check_distance_matrix_clustering(distance_matrix, clustering)
    _check_clustering(clustering, outliers_cluster_id)
    clusters_ids = cluster_ids(clustering, outliers_cluster_id)

    separation = {}
    for q in clusters_ids:
        # Get the index of all elements in cluster `q`:
        index_q = np.where(np.array(clustering) == q)[0].tolist()
        # Get the index of all elements not in cluster `q`:
        index_not_q = np.where(np.array(clustering) != q)[0].tolist()
        separation[q] = min([distance_matrix[i, j] for i in index_q for j in index_not_q])

    return separation


def within_cluster_distances(distance_matrix, clustering, outliers_cluster_id=None):
    """Compute the *within cluster distance* metrics.

    Parameters
    ----------
    distance_matrix : numpy.ndarray
        Distance matrix used to create the clustering.
    clustering : list
        Resulting list of cluster ids.
    outliers_cluster_id : None or int or str, optional
        The id of the cluster representing the outliers. It will be ignored.

    Returns
    -------
    dict of float
        Dictionary of *within cluster distances* by cluster id.

    Raises
    ------
    ValueError
        If the distance matrix is not a 2D matrix.
        If the distance matrix is not a square matrix.
        If the distance matrix and list of cluster ids are not of same size.
        If there is at least one cluster with a single element.
        If there is only one cluster.

    Notes
    -----
    The *within cluster distance* is defined as follows:
    :math:`D(q) = \dfrac{ \sum_{i,j \in q; i \neq j} D(i,j) }{(N_{q}-1)N_{q}}`, where
    :math:`D(i,j)` is the distance between elements :math:`i` and :math:`j`,
    and :math:`N_{q}` is the number of elements found in cluster :math:`q`.

    Examples
    --------
    >>> from timagetk.algorithms.clustering_estimators import within_cluster_distances
    >>> from timagetk.components.clustering import example_clusterer
    >>> clust = example_clusterer()
    >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5])
    >>> clust.compute_clustering(method='ward', n_clusters=4)
    >>> D_within = within_cluster_distances(clust._global_pw_dist_mat, list(clust._clustering.values()))
    >>> D_within
    {0: 1.7086544265796209,
     2: 1.0594677425279004,
     1: 0.5740079988497645,
     3: 0.4645248657536146}

    """
    _check_distance_matrix_clustering(distance_matrix, clustering)
    _check_clustering(clustering, outliers_cluster_id)
    clusters_ids = cluster_ids(clustering, outliers_cluster_id)
    n_elt_by_cluster = n_elements_by_cluster(clustering, outliers_cluster_id)

    D_within = {}
    for q in clusters_ids:
        # Get the index of all elements in cluster `q`:
        index_q = np.where(np.array(clustering) == q)[0].tolist()
        denominator = n_elt_by_cluster[q] * (n_elt_by_cluster[q] - 1)
        # Compute the within cluster distance for cluster `q`:
        D_within[q] = sum([distance_matrix[i, j] for i in index_q for j in index_q if j != i]) / denominator

    return D_within


def between_cluster_distances(distance_matrix, clustering, outliers_cluster_id=None):
    """Compute the *between cluster distance* metrics.

    Parameters
    ----------
    distance_matrix : numpy.ndarray
        Distance matrix used to create the clustering.
    clustering : list
        Resulting list of cluster ids.
    outliers_cluster_id : None or int or str, optional
        The id of the cluster representing the outliers. It will be ignored.

    Returns
    -------
    dict of float
        Dictionary of *between cluster distances* by cluster id.

    Raises
    ------
    ValueError
        If the distance matrix is not a 2D matrix.
        If the distance matrix is not a square matrix.
        If the distance matrix and list of cluster ids are not of same size.
        If there is at least one cluster with a single element.
        If there is only one cluster.

    Notes
    -----
    The *between cluster distance* is defined as:
    :math:`D(q) = \dfrac{ \sum_{i \in q} \sum_{j \not\in q} D(i,j) }{ (N - N_q) N_q }`, where
    :math:`D(i,j)` is the distance between elements :math:`i` and :math:`j`,
    :math:`N` is the total number of elements
    and :math:`N_{q}` is the number of elements found in cluster :math:`q`.

    Examples
    --------
    >>> from timagetk.algorithms.clustering_estimators import between_cluster_distances
    >>> from timagetk.components.clustering import example_clusterer
    >>> clust = example_clusterer()
    >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5])
    >>> clust.compute_clustering(method='ward', n_clusters=4)
    >>> D_between = between_cluster_distances(clust._global_pw_dist_mat, list(clust._clustering.values()))
    >>> D_between
    {0: 1.5797489182193483,
     2: 1.891976669477402,
     1: 1.0330348680885626,
     3: 1.1898281168837876}

    """
    _check_distance_matrix_clustering(distance_matrix, clustering)
    _check_clustering(clustering, outliers_cluster_id)
    clusters_ids = cluster_ids(clustering, outliers_cluster_id)
    n_elt_by_cluster = n_elements_by_cluster(clustering, outliers_cluster_id)
    N = len(clustering)

    D_between = {}
    for q in clusters_ids:
        # Get the index of all elements in cluster `q`:
        index_q = np.where(np.array(clustering) == q)[0].tolist()
        # Get the index of all elements not in cluster `q`:
        index_not_q = np.where(np.array(clustering) != q)[0].tolist()
        denominator = (N - n_elt_by_cluster[q]) * n_elt_by_cluster[q]
        D_between[q] = sum([distance_matrix[i, j] for i in index_q for j in index_not_q]) / denominator

    return D_between


def global_cluster_distances(distance_matrix, clustering, outliers_cluster_id=None):
    """Compute the *global cluster distance* metrics.

    Parameters
    ----------
    distance_matrix : numpy.ndarray
        Distance matrix used to create the clustering.
    clustering : list
        Resulting list of cluster ids.
    outliers_cluster_id : None or int or str, optional
        The id of the cluster representing the outliers. It will be ignored.

    Returns
    -------
    float
        The sum of *within cluster distance*.
    float
        The sum of *between cluster distance*.

    Examples
    --------
    >>> from timagetk.algorithms.clustering_estimators import global_cluster_distances
    >>> from timagetk.components.clustering import example_clusterer
    >>> clust = example_clusterer()
    >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5])
    >>> clust.compute_clustering(method='ward', n_clusters=4)
    >>> global_dist = global_cluster_distances(clust._global_pw_dist_mat, list(clust._clustering.values()))
    >>> global_dist
    (0.6682854884827563, 1.693079236837533)

    """
    w = within_cluster_distances(distance_matrix, clustering)
    b = between_cluster_distances(distance_matrix, clustering)

    # Remove outliers from distance matrix and clustering list:
    if outliers_cluster_id in clustering:
        clustering = np.array(clustering)
        not_outlier_index = np.where(clustering != outliers_cluster_id)
        clustering = list(clustering[not_outlier_index])

    N = len(clustering)
    clusters_ids = cluster_ids(clustering, outliers_cluster_id)
    n_elt_by_cluster = n_elements_by_cluster(clustering, outliers_cluster_id)

    gcd_w = sum([(n_elt_by_cluster[q] * (n_elt_by_cluster[q] - 1)) / float(
        sum([n_elt_by_cluster[l] * (n_elt_by_cluster[l] - 1) for l in clusters_ids if l != q])) * w[q] for q in
                 clusters_ids])
    gcd_b = sum([(N - n_elt_by_cluster[q]) * n_elt_by_cluster[q] / float(
        sum([(N - n_elt_by_cluster[l]) * n_elt_by_cluster[l] for l in clusters_ids if l != q])) * b[q] for q in
                 clusters_ids])
    return gcd_w, gcd_b


def calinski_harabasz_estimator(k, w, b, N):
    """The Calinski and Harabasz index.

    Parameters
    ----------
    k : int
        The number of clusters.
    w : float
        The global WITHIN cluster distances.
    b : float
        The global BETWEEN cluster distances.
    N : int
        The population size.

    Returns
    -------
    float
        The Calinski and Harabasz index.

    Notes
    -----
    In [Calinski_Harabasz]_, the authors proposed the following statistic:
    :math:`CH(k) = \dfrac{B(k) / (k-1)}{W(k) / (n-k)}`,
    where :math:`B(k)` and :math:`W(k)` are the between- and within-cluster sums of squares, with :math:`k` clusters.
    The idea is to maximize :math:`CH(k)` over the number of clusters :math:`k`.
    Note that :math:`CH(1)` is not defined.

    References
    ----------
    .. [Calinski_Harabasz] T. Caliński & J Harabasz (1974) A dendrite method for cluster analysis, Communications in Statistics, 3:1, 1-27, `DOI <https://doi.org/10.1080/03610927408827101>`
    """
    return (b / (k - 1)) / (w / (N - k))


def hartigan_estimator(k, w_k, N):
    """The Hartigan index.

    Parameters
    ----------
    k : int
        The number of clusters.
    w_k : dict
        Dictionary of WITHIN cluster distances (must contain `k` and `k+1`).
    N : int
        The population size.

    Returns
    -------
    float
        The Hartigan index.

    Notes
    -----
    In [Hartigan]_, the author proposed the following statistic:
    :math:`H(k) = \left\{ \dfrac{W(k)}{W(k+1)} - 1 \right\} / (n-k-1)`,
    The idea is to start with :math:`k=1` and to add a cluster as long as :math:`H(k)` is sufficiently large.
    One can use an approximate *F*-distribution cut-off.
    Instead, the author suggested that a cluster can be added if :math:`H(k) > 10`.
    Hence, the estimated number of clusters is the smallest :math:`k \geqslant 1` such that :math:`H(k) \leqslant 10`.
    This estimate is defined for :math:`k=1` and can potentially discriminate between one *versus* more than one cluster.

    References
    ----------
    .. [Hartigan] Hartigan JA (1975). Clustering Algorithms. John Wiley & Sons, Inc., New York, NY, USA. ISBN 047135645X

    """
    if w_k[k] is None or w_k[k + 1] is None:
        return None
    else:
        return (w_k[k] / w_k[k + 1] - 1) / (N - k - 1)
