#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Tissue Graph Module

This module implements the `TissueGraph` data structure, which extends the capabilities of
the `networkx` library by providing specialized graph representations for biological and
topological structures.
It supports handling nodes and edges with additional flexibility,
such as using tuples as identifiers to represent diffent geometrical structures as well as the
temporal relationships.

Key Features
------------
- **Unsorted Dictionary (`udict`)**:
  Provides a dictionary-like structure that treats tuples as keys, with flexibilities in
  key ordering, making it highly adaptable for graph data.

- **Enhanced Graph**:
  Extends the `networkx.Graph` class with additional functionalities, including:
  - Support for tuples as node identifiers, useful to represent different geometrical structures
    such as cells and cell-vertices.
  - Support relationships between nodes, such as topological relationships between
    cells, and temporal relationships between cells.
  - Easy assignment and retrieval of node and edge attributes.
  - Robust methods for adding nodes and edges, ensuring duplicated/redundant data is avoided.
  - Improved handling of graph data, such as `add_nodes_from`, `add_edges_from`, and
    property-assignment capabilities.

- **Convenient Data Structures**:
  Provides methods for accessing and modifying the properties of nodes and edges
  through intuitive APIs like `get_node_data` and `get_edge_data`.

Usage Examples
--------------
>>> # Example - Example of using the `Graph` class:
>>> from timagetk.graphs.nx_graph import Graph
>>> g = Graph()
>>> g.add_nodes_from([1, 2])  # Adding nodes
>>> g.add_edges_from([(1, 2)])  # Adding an edge connecting the nodes
>>> print(g.nodes())
[1, 2]
>>> print(g.edges())
[(1, 2)]
>>> g.nodes[1]["label"] = "cell"  # Assigning a label to a node
>>> print(g.get_node_data(1))  # Access node attributes
{'label': 'cell'}

>>> # Example - Using the `udict` class:
>>> from timagetk.graphs.nx_graph import udict
>>> u_edges = udict({(2, 1): {"area": 25.3}})
>>> print(u_edges)
udict({(1, 2): {'area': 25.3}})
>>> print(u_edges[2, 1])
{'area': 25.3}
"""

import networkx as nx

from timagetk.bin.logger import get_logger
from timagetk.util import stuple

log = get_logger(__name__)


class udict(dict):
    """Unsorted dictionary class.

    Extent the dictionary class to use tuple as keys, with irrelevant order.

    Examples
    --------
    >>> from timagetk.graphs.nx_graph import udict
    >>> u_edges = udict({(2, 1): {"area": 25.3}})
    >>> print(u_edges)
    udict({(1, 2): {'area': 25.3}})
    >>> print(u_edges[2, 1])
    {'area': 25.3}
    >>> print(u_edges[1, 2])
    {'area': 25.3}
    """

    def __init__(self, *args, **kwargs):
        super().__init__()
        self.update(*args, **kwargs)

    def __getitem__(self, key):
        if isinstance(key, int):
            val = dict.__getitem__(self, key)
        else:
            val = dict.__getitem__(self, stuple(key))
        return val

    def __setitem__(self, key, val):
        if isinstance(key, int):
            dict.__setitem__(self, key, val)
        else:
            dict.__setitem__(self, stuple(key), val)

    def __repr__(self):
        dictrepr = dict.__repr__(self)
        return '%s(%s)' % (type(self).__name__, dictrepr)

    def update(self, *args, **kwargs):
        for k, v in dict(*args, **kwargs).items():
            self[k] = v


class Graph(nx.Graph):
    """A basic graph structure.

    It extends the ``networkx.Graph`` to create undirected graphs from topological information.
    It also accepts tuples as node ids, useful to create the dual graph.

    Examples
    --------
    >>> from timagetk.graphs.nx_graph import Graph
    >>> # Example #1 - Neighborhood graph
    >>> g = Graph()
    >>> g.add_node(1)  # add a node, nodes id can be integers
    >>> g.add_node((1, 2))  # add a node, nodes id can be tuples of integers
    >>> print(g.nodes())
    [1, (1, 2)]
    >>> g.add_nodes_from([(2, 1), (4, 2), (3, 4)])  # add nodes from a list
    >>> print(g.nodes())
    [1, (1, 2), (2, 4), (3, 4)]
    >>> g.add_edges_from([((2, 1), (4, 2)), ((1, 2), (3, 4))])  # add nodes from a list
    >>> print(g.edges())
    [((1, 2), (2, 4)), ((1, 2), (3, 4))]

    >>> g = Graph()
    >>> g.add_nodes_from([1, 2])  # add two nodes with ids '1' & '2'
    >>> g.add_edges_from([(1, 2)])  # add an edge between nodes '1' & '2'
    >>> print(g.nodes())
    [1, 2]
    >>> print(g.edges())
    [(1, 2)]
    >>> g.nodes[1]["test"] = 'foo'  # add a 'test' property to node id '1'
    >>> print(g.nodes.data())  # print all nodes properties
    [(1, {'test': 'foo'}), (2, {})]
    >>> g.edges[(1, 2)]["test"] = 0  # add a 'test' property to the edge between nodes '1' & '2'
    >>> print(g.edges.data())  # print all edges properties
    [(1, 2, {'test': 0})]
    >>> g.get_node_data(1)
    {'test': 'foo'}
    >>> g.get_node_data([1, 2])

    """

    def __init__(self, incoming_graph_data=None, **kwargs):
        """Graph constructor.

        Parameters
        ----------
        incoming_graph_data : networkx.Graph
            A networkx Graph object.
        kwargs : keyword arguments, optional
            Attributes to add to graph as key=value pairs.
        """
        super().__init__(incoming_graph_data=incoming_graph_data, **kwargs)

    def __getitem__(self, item):
        if isinstance(item, int):
            val = nx.Graph.__getitem__(self, item)
        else:
            val = nx.Graph.__getitem__(self, stuple(item))
        return val

    def add_node(self, node, **kwargs):
        """Add a single node to the graph object.

        Parameters
        ----------
        node : int or tuple
            The node id to add to the graph.
        kwargs : keyword arguments, optional
            Node data can be assigned using keyword arguments.

        Examples
        --------
        >>> from timagetk.graphs.nx_graph import Graph
        >>> g = Graph()
        >>> g.add_node(5)  # use an integer as node id
        >>> g.add_node((1, 2))  # use a tuple as node id
        >>> print(g.nodes())
        [5, (1, 2)]
        >>> g.add_node((2, 1))
        >>> print(g.nodes())  # node (2, 1) is the same as (1, 2)
        [5, (1, 2)]
        >>> g.add_node(6, key1="hello", key2="world")  # assign properties to nodes
        >>> print(g.get_node_data(6))  # Access those properties
        {'key1': 'hello', 'key2': 'world'}
        """
        if isinstance(node, tuple):
            node = stuple(node)
        nx.Graph.add_node(self, node, **kwargs)

    def add_nodes_from(self, nodes, **kwargs):
        """Add a node to the graph object.

        Parameters
        ----------
        node : list or dict
            The node id to add to the graph.

        Examples
        --------
        >>> from timagetk.graphs.nx_graph import Graph
        >>> g = Graph()
        >>> g.add_node((1, 2))  # use a tuple as node id
        >>> print(g.nodes())
        [(1, 2)]
        >>> g.add_nodes_from([(2, 1), (4, 2), (3, 4)])  # use a list of tuples as node ids
        >>> print(g.nodes())  # node (2, 1) is the same as (1, 2)
        [(1, 2), (2, 4), (3, 4)]
        >>> g.add_nodes_from([(2, 1), (4, 2), (3, 4)], key1="hello", key2="world")  # assign properties to those nodes
        >>> print(g.get_node_data((2, 1)))  # Access those properties
        {'key1': 'hello', 'key2': 'world'}
        >>> print(g.get_node_data((2, 4)))  # Access those properties
        {'key1': 'hello', 'key2': 'world'}
        """
        if isinstance(nodes, list) and isinstance(nodes[0], tuple):
            nodes = [stuple(n) for n in nodes]
        nx.Graph.add_nodes_from(self, nodes, **kwargs)

    def add_edge(self, u, v, **kwargs):
        """Add a single edge between u and v.

        Parameters
        ----------
        u, v : nodes
            Nodes ids. Nodes must be hashable (and not None) Python objects.
        kwargs : keyword arguments, optional
            Edge data can be assigned using keyword arguments.

        Examples
        --------
        >>> from timagetk.graphs.nx_graph import Graph
        >>> g = Graph()
        >>> g.add_edge(5, 3)  # use an integer as node id, they don't have to exists!
        >>> print(g.edges())
        [(3, 5)]
        >>> print(g.nodes())
        [3, 5]
        >>> g.add_edge((1, 2), (5, 3))  # use a tuple as node ids
        >>> print(g.edges())
        [(3, 5), ((1, 2), (3, 5))]
        >>> print(g.nodes())
        [3, 5, (1, 2), (3, 5)]
        >>> g.add_edge(5, 4, key1="hello", key2="world")  # use an integer as node id, they don't have to exists!
        >>> g.get_edge_data(5, 4)
        {'key1': 'hello', 'key2': 'world'}
        """
        if isinstance(u, tuple):
            u = stuple(u)
        if isinstance(v, tuple):
            v = stuple(v)

        edge = stuple([u, v])
        nx.Graph.add_edge(self, edge[0], edge[1], **kwargs)

    def add_edges_from(self, ebunch_to_add, **kwargs):
        """Add a list of edges.

        Parameters
        ----------
        ebunch_to_add : list
            List of length-2 tuples as node ids.
        kwargs : keyword arguments, optional
            Edge data can be assigned using keyword arguments.

        Examples
        --------
        >>> from timagetk.graphs.nx_graph import Graph
        >>> g = Graph()
        >>> g.add_edges_from([(5, 3), (7, 8)])  # use an integer as node id, they don't have to exists!
        >>> print(g.edges())
        [(3, 5), (7, 8)]
        >>> print(g.nodes())None
        [3, 5, 7, 8]
        >>> g.add_edges_from([(1, 2), (1, 5), (1, 3), (2, 3)])  # use a integers as node ids
        >>> g.add_edges_from([((1, 2), (5, 3)), ((1, 2), (7, 8))])  # use a tuple as node ids
        >>> print(g.edges())
        [(3, 5), (7, 8), ((1, 2), (3, 5)), ((1, 2), (7, 8))]
        >>> print(g.nodes())
        [3, 5, 7, 8, (1, 2), (3, 5), (7, 8)]
        >>> g.add_edges_from([(7, 9)], key1="hello", key2="world")  # use an integer as node id, they don't
        >>> g.get_edge_data(7, 9)
        {'key1': 'hello', 'key2': 'world'}
        >>> g.copy() # fix bug relate to generator
        """
        # TODO: Rewrite the add_edges_from() method from networkx
        # Fix bug related to generator
        if not isinstance(ebunch_to_add, list):
            ebunch_to_add = list(ebunch_to_add) # force conversion from generator to list

        processed_edges = []
        for i, e in enumerate(ebunch_to_add):
            if len(e) == 2:
                u, v = e
                if isinstance(u, tuple):
                    u = stuple(u)
                if isinstance(v, tuple):
                    v = stuple(v)
                processed_edges.append((u, v))
            elif len(e) == 3:
                u, v, d = e
                if isinstance(u, tuple):
                    u = stuple(u)
                if isinstance(v, tuple):
                    v = stuple(v)
                processed_edges.append((u, v, d))
            else:
                raise ValueError(f"Invalid edge tuple length: {len(e)}. Expected length 2 or 3.")

        log.debug(f"List of edges to add: {ebunch_to_add}")
        nx.Graph.add_edges_from(self, ebunch_to_add, **kwargs)

    def get_node_data(self, node, default=None):
        """Get all data from the node.
None
        Parameters
        ----------
        node : int, tuple
            A node identifier. Nodes must be hashable (and not ``None``) Python objects.
        default : Any, optional
            Value to return if the `node` is not found, default is ``None``.

        Examples
        --------
        >>> from timagetk.graphs.nx_graph import Graph
        >>> g = Graph()
        >>> g.add_node(6, key1="hello", key2="world")  # assign properties to nodes
        >>> print(g.get_node_data(6))  # Access those properties
        {'key1': 'hello', 'key2': 'world'}
        >>> print(g.get_node_data(6)['key1'])  # Access those properties
        hello
        """
        if isinstance(node, tuple):
            node = stuple(node)
        try:
            val = self._node[node]
        except:
            val = default
        return val

    def get_edge_data(self, u, v, default=None):
        """Get all data from the edge between u and v.

        Parameters
        ----------
        u, v : int
            Nodes ids. Nodes must be hashable (and not None) Python objects.
        default : Any, optional
            Value to return if the edge `(u, v)` is not found, default is ``None``.

        Examples
        --------
        >>> from timagetk.graphs.nx_graph import Graph
        >>> g = Graph()
        >>> g.add_edge(5, 4, key1="hello", key2="world")
        >>> g.get_edge_data(5, 4)
        {'key1': 'hello', 'key2': 'world'}
        >>> g.add_edge((1, 2), (5, 3), key1="hello", key2="world")
        >>> g.get_edge_data((2, 1), (3, 5))
        {'key1': 'hello', 'key2': 'world'}
        >>> print(g.get_edge_data((2, 1), (3, 5))['key1'])  # Access those properties
        hello
        """
        if isinstance(u, tuple):
            u = stuple(u)
        if isinstance(v, tuple):
            v = stuple(v)

        return nx.Graph.get_edge_data(self, u, v, default)
