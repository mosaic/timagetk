#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Graph structure dedicated to representation of static multicellular dense tissues."""

import copy as cp

import networkx as nx

from timagetk.bin.logger import get_logger
from timagetk.graphs.nx_graph import Graph
from timagetk.util import not_default_test
from timagetk.util import stuple

log = get_logger(__name__)


class TissueGraph(object):
    """A graph structure dedicated to static dense multicellular tissues.

    Attributes
    ----------
    primal : networkx.Graph
        The primal graph contains the cells (nodes) and the cell-walls defines their topological relationships (edges).
        The nodes ids are defined as integers that are also the tissue cell ids.
        The edges ids are defined as len-2 tuples of integers (pairs of cell ids) that are also the tissue cell-wall ids.
    dual : networkx.Graph
        The dual graph contains the cell-vertices (nodes) and the cell-edges defines their topological relationships (edges).
        The nodes ids are defined as len-4 tuples (quadruplet of cell ids) that are also the tissue cell-vertex ids.
        The edges ids are defined as len-2 tuples of len-4 tuples (pairs of cell-vertex ids) that are also the tissue cell-edge ids.
    _cell_properties : dict
        Cell property based dictionary with their unit.
    _wall_properties : dict
        Cell-wall property based dictionary with their unit.
    _edge_properties : dict
        Cell-edge property based dictionary with their unit.
    _vertex_properties : dict
        Cell-vertex property based dictionary with their unit.
    _background : int
        The id referring to the tissue background, if any.

    Parameters
    ----------
    tissue : timagetk.TissueImage2D or timagetk.TissueImage3D
        The segmented tissue to transform.

    Other Parameters
    ----------------
    background : int
        The id to associate to the background position, this is useful to be able to defines the cell-walls.
    cells : list
        If given, allow to filter the labels used within the `tissue`. By default, all labels are used.
    coordinates : bool
        If ``True`` add the topological element coordinates to the graph at construction time.
        See notes for more information.
    features : list
        A list of spatial cell features to extract from the segmented tissue.
    wall_features : list
        A list of spatial wall features to extract from the segmented tissue.
    real : bool
        If ``True`` (default), extract the features in real units, else in voxels.
    extract_dual : bool
        If ``True`` (default ``False``) construct the dual graph with cell-edges and vertices.
        Else, construct only primal graph, that is the neighborhood graph.
    epidermis_area_threshold : float
        The minimum real contact area with the background necessary to be defined as epidermal cell.
        No minimum by default.

    Examples
    --------
    >>> from timagetk.graphs.tissue_graph import TissueGraph
    >>> from timagetk import TissueImage3D
    >>> from timagetk.io.dataset import shared_data
    >>> tissue = TissueImage3D(shared_data('flower_labelled', 0), background=1, not_a_label=0)
    >>> tg = TissueGraph(tissue, features=["volume"], wall_features=["area"])
    >>> tg.list_cell_properties()
    >>> tg.list_cell_wall_properties()

    >>> tg = TissueGraph()
    >>> tg.add_cells(tissue.cell_ids())  # add the list of cells
    >>> tg.cell_ids()[:10]  # access the first 10
    [2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
    >>> tg.add_cell_walls(tissue.walls.ids())  # add the list of cell-walls
    >>> tg.cell_neighbors(2)
    [565, 533, 475, 988, 430, 818, 1010]
    >>> tg.cell_neighbors_dict([2, 3, 4, 5])

    >>> tg.add_cell_property('volume',tissue.cells.volume())  # add the cell volume property
    >>> tg.cell_property_dict('volume',[2, 3, 4])  # access the cell volume property for some cell ids
    {2: 20.964309221671844, 3: 2027.1715324992686, 4: 196.3313989379268}
    >>> tg.add_cell_wall_property('area',tissue.walls.area())  # add the cell-wall area property
    >>> tg.cell_wall_property_dict('area',[(3, 903), (342, 450)])  # access the cell-wall area property for some cell-wall ids
    {(3, 903): 63.42856784380443, (342, 450): 15.465219404480086}
    """

    def __init__(self, tissue=None, **kwargs):
        self.primal = Graph()
        self.dual = Graph()
        self._cell_properties = {}
        self._cell_identities = []  # boolean properties related to cell identity
        self._wall_properties = {}
        self._edge_properties = {}
        self._vertex_properties = {}
        self._background = kwargs.get("background", None)
        self.verbose = kwargs.get("verbose", True)

        if tissue is not None:
            from timagetk.tasks.tissue_graph import tissue_graph_from_image
            tissue_graph_from_image(tissue, graph=self, **kwargs)

    def __len__(self):
        return self.nb_cells()

    @property
    def background(self):
        """Get the node id acting as background, can be ``None``.

        Returns
        -------
        int or None
            Graph node id defining the background.

        Examples
        --------
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> tg.background = 1
        >>> tg.background
        1
        """
        if self._background is None:
            log.warning("No value defined for the background id!")
        return cp.copy(self._background)

    @background.setter
    def background(self, value):
        """Defines the node id acting as background.

        Parameters
        ----------
        value : int
            Graph node id defining the background.

        Examples
        --------
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> tg.background = 1
        >>> tg.background
        1
        """
        if not isinstance(value, int) and value is not None:
            log.error(f"Provided node id '{value}' is not an integer!")
            return
        if value not in self.primal.nodes():
            log.warning(f"Provided node id '{value}' is not a node of the primal graph!")
            return
        self._background = value
        return

    def add_cells(self, cids):
        """Add cell(s) as node(s) of the primal graph.

        Parameters
        ----------
        cids : int or list of int
            A single or a list of cell id(s), to add as node(s) to the primal graph.

        Examples
        --------
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> tg.add_cells(2)
        >>> print(tg.cell_ids())
        [2]
        >>> tg.add_cells([3, 4, 5, 6, 7])
        >>> print(tg.cell_ids())
        [2, 3, 4, 5, 6, 7]
        """
        if isinstance(cids, int):
            self.primal.add_node(cids)
        else:
            self.primal.add_nodes_from(cids)
            if self.verbose:
                log.info(f"Added {len(cids)} cells to the TissueGraph!")
        return

    def add_cell_walls(self, wids):
        """Add cell-wall(s) as edge(s) of the primal graph.

        Parameters
        ----------
        wids : tuple of int or list of tuple of int
            A single or a list of cell-wall id(s), to add as edge(s) to the primal graph.

        Notes
        -----
        A cell-wall id ``wid`` is defined as a pair of cell ids ``cid`` , *i.e.* a len-2 tuple ``(cid, cid)``.
        The order of the cell ids in the len-2 tuple defining the cell-wall id is irrelevant, they are sorted.

        Examples
        --------
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> tg.add_cell_walls((2, 3))  # add a single cell-wall
        >>> print(tg.cell_wall_ids())  # note that it add the cell-vertices if they don't exist
        [(2, 3)]
        >>> tg.add_cell_walls((3, 2))
        >>> # Note that the order of the cell ids in the tuple defining the wall id is irrelevant:
        >>> print(tg.cell_wall_ids())
        [(2, 3)]
        >>> # Add a list of cell-walls:
        >>> walls = [(3, 2), (3, 4), (3, 5), (3, 6), (3, 7), (2, 4), (4, 5), (5, 6), (6, 7), (2, 7)]
        >>> tg.add_cell_walls(walls)
        >>> print(tg.cell_wall_ids())
        [(2, 3), (2, 4), (2, 7), (3, 4), (3, 5), (3, 6), (3, 7), (4, 5), (5, 6), (6, 7)]
        """
        if isinstance(wids, tuple) and len(wids) == 2:
            self.primal.add_edge(wids[0], wids[1])
        else:
            self.primal.add_edges_from(wids)
            if self.verbose:
                log.info(f"Added {len(wids)} cell-walls to the TissueGraph!")
        return

    def add_cell_vertices(self, vertices):
        """Add cell-vertice(s) as node(s) of the dual graph.

        Parameters
        ----------
        vertices : tuple of int or list of tuple of int
            A single or a list of cell-vertex id(s), to add as node(s) to the dual graph.

        Notes
        -----
        A cell-vertex id ``vid`` is defined as a triplet (2D) or quadruplet (3D) of cell ids, *i.e.* a len-3 or a len-4 tuple.
        The order of the cell ids in the len-3|4 tuple defining the vertex id is irrelevant, they are sorted.

        Examples
        --------
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> # Add a single 3D cell-vertex:
        >>> tg.add_cell_vertices((1, 2, 3, 4))
        >>> print(tg.cell_vertex_ids())
        [(1, 2, 3, 4)]
        >>> # Add a list of 3D cell-vertices:
        >>> tg.add_cell_vertices([(1, 3, 4, 5), (1, 3, 5, 6), (1, 3, 6, 7), (1, 2, 3, 7)])
        >>> print(tg.cell_vertex_ids())
        [(1, 2, 3, 4), (1, 3, 4, 5), (1, 3, 5, 6), (1, 3, 6, 7), (1, 2, 3, 7)]
        """
        if isinstance(vertices, tuple):
            self.dual.add_node(vertices)
        else:
            self.dual.add_nodes_from(vertices)
            if self.verbose:
                log.info(f"Added {len(vertices)} cell-vertices to the TissueGraph!")
        return

    def add_cell_edges(self, edges):
        """Add cell-edge(s) as edge(s) of the dual graph.

        Parameters
        ----------
        edges : tuple of int or list of tuple of int
            A single or a list of cell-edges, to add as edge(s) to the dual graph.

        Notes
        -----
        A cell-vertex id ``vid`` is defined as a triplet (2D) or quadruplet (3D) of cell ids, *i.e.* a len-3 or a len-4 tuple.
        A cell-edge id ``eid`` is defined as a pair of cell-vertex ids, *i.e.* a len-2 tuple ``(vid, vid)``.
        The order of the cell ids in the len-4 tuple defining the cell-vertex ids is irrelevant, they are sorted.

        Examples
        --------
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> # Add a single 3D cell-edge without prior creation of 3D cell-vertices:
        >>> tg.add_cell_edges(((1, 2, 3, 4), (1, 3, 4, 5)))
        >>> # Note it add the 3D cell-vertices at the same time:
        >>> print(tg.cell_vertex_ids())
        [(1, 2, 3, 4), (1, 3, 4, 5)]
        >>> # Add a list of 3D cell-edges:
        >>> edges = [((1, 2, 3, 4), (1, 3, 4, 5)), ((1, 3, 4, 5), (1, 3, 5, 6)), ((1, 3, 5, 6), (1, 3, 6, 7)), ((1, 3, 6, 7), (1, 2, 3, 7)), ((1, 2, 3, 7), (1, 2, 3, 4))]
        >>> tg.add_cell_edges(edges)
        >>> print(tg.cell_edge_ids())
        [((1, 2, 3, 4), (1, 3, 4, 5)), ((1, 2, 3, 4), (1, 2, 3, 7)), ((1, 3, 4, 5), (1, 3, 5, 6)), ((1, 3, 5, 6), (1, 3, 6, 7)), ((1, 3, 6, 7), (1, 2, 3, 7))]
        """
        if isinstance(edges, tuple) and len(edges) == 2:
            self.dual.add_edge(edges[0], edges[1])
        else:
            self.dual.add_edges_from(edges)
            if self.verbose:
                log.info(f"Added {len(edges)} cell-edges to the TissueGraph!")
        return

    def _identity_filtering(self, identity):
        """Get set of cell ids based on their identity.

        Parameters
        ----------
        identity : str
            Identity names to find in the graph to restrict the list of cell ids.
            This can also include 'layer_*' to include only cells belonging to the specified layers.
            For example 'layer_1_2' will include only cells that are in the first two layers.
        """
        if identity.lower().startswith('layer'):
            layers = list(map(int, identity.split("_")[1:]))  # layers are separated by underscores
            from timagetk.features.graph import list_cells_in_layer
            cell_ids = list_cells_in_layer(self, layer=layers)
            log.debug(f"Selected {len(cell_ids)} cell ids in layers {layers}!")
            ids = set(cell_ids)
        elif identity not in self.list_identities():
            log.critical(f"Could not find the cell identity '{identity}' in the graph!")
            ids = set([])
        else:
            ids = set(self.list_cells_with_identity(identity))
        return ids

    def cell_ids(self, identity=None) -> set:
        """List the cell ids.

        The cell ids are the nodes of the primal graph, excluding the ``background``, if any.

        Parameters
        ----------
        identity : list, optional
            Identity names to find in the graph to restrict the list of cell ids.
            This can also include 'layer_*' to include only cells belonging to the specified layers.
            For example 'layer_1_2' will include only cells that are in the first two layers.

        Examples
        --------
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> tg.add_cells([1, 2, 3, 4])
        >>> tg.cell_ids()
        [1, 2, 3, 4]
        """
        ids = list(self.primal.nodes())
        if self._background is not None:
            ids = set(ids) - {self._background}

        if identity is not None:
            if isinstance(identity, str):
                identity = [identity]
            # Restrict to given list of identities:
            for iden in identity:
                ids &= self._identity_filtering(iden)

        return ids

    def cell_wall_ids(self, cell_id=None):
        """Return the list of cell-wall ids, possibly restricted to the given cell id.

        The cell-wall ids are the edges of the primal graph.

        Parameters
        ----------
        cell_id : int, optional
            The cell id for which to return the list of cell-wall ids.
            If ``None``, returns all edges of the primal graph.

        Returns
        -------
        set
            The set of cell-wall ids, possibly restricted to the given cell id.

        Examples
        --------
        >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
        >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
        >>> wids = tg.cell_wall_ids()  # list all cell-wall ids
        >>> print(f"The graph contains {len(wids)} cell-walls!")
        The graph contains 356 cell-walls!
        >>> cell_wids = tg.cell_wall_ids(4)  # list of cell-wall ids associated to cell `4`
        >>> print(f"The graph contains {len(cell_wids)} cell-walls associated to cell 4!")
        The graph contains 15 cell-walls associated to cell 4!
        >>> print(tg.cell_wall_ids(16))  # list of cell-wall ids associated to epidermal cell `16`
        {(16, 20), (16, 61), (16, 58), (16, 19), (13, 16), (8, 16), (16, 38), (10, 16), (16, 21), (16, 34), (1, 16)}
        """
        if cell_id is not None:
            return {stuple((cell_id, neigh)) for neigh in self.primal.neighbors(cell_id)}
        else:
            return set(map(stuple, self.primal.edges()))

    def cell_edge_ids(self, cell_id=None, wall_id=None):
        """Return the list of cell-edge ids, possibly restricted to the given cell id.

        The cell-edge ids are the edges of the dual graph.

        Parameters
        ----------
        cell_id : int, optional
            The cell id for which to return the list of cell-edge ids.
            If ``None``, returns all edges of the dual graph.
        wall_id : (int, int), optional
            The wall id for which to return the list of cell-vertex ids.
            If ``None``, returns all nodes of the dual graph.
            If defined, override the `cell_id` parameter.

        Returns
        -------
        set
            The set of cell-edge ids, possibly restricted to the given cell id.

        Examples
        --------
        >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
        >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
        >>> cwids = tg.cell_edge_ids()  # list all cell-edge ids
        >>> print(f"The graph contains {len(cwids)} cell-edges!")
        The graph contains 537 cell-edges!
        >>> cell_eids = tg.cell_edge_ids(4)  # list of cell-edge ids associated to cell `4`
        >>> print(f"The graph contains {len(cell_eids)} cell-edges associated to cell 4!")
        The graph contains 39 cell-edges associated to cell 4!
        >>> epidermis_eids = tg.cell_edge_ids(wall_id=(1, 16))  # list of cell-edge ids associated to epidermal cell-wall `(1, 16)`
        >>> print(len(epidermis_eids))
        5
        >>> print(epidermis_eids)
        {((1, 16, 21, 38), (1, 16, 38, 61)), ((1, 16, 19, 21), (1, 16, 21, 38)), ((1, 16, 34, 61), (1, 16, 38, 61)), ((1, 16, 19, 34), (1, 16, 34, 61)), ((1, 16, 19, 21), (1, 16, 19, 34))}
        """
        cid = None
        if wall_id is not None:
            cid, cell_id = wall_id
        if cell_id is not None:
            # Search for all edges in the dual graph with the given cell id:
            cell_edges = {stuple(ce) for ce in self.dual.edges if cell_id in ce[0] and cell_id in ce[1]}
            if wall_id is None:
                return cell_edges
            else:
                # Also 'filter' by the other cell id `cid`
                return {stuple(ce) for ce in map(list, cell_edges) if cid in ce[0] and cid in ce[1]}
        else:
            return set(map(stuple, self.dual.edges()))

    def cell_vertex_ids(self, cell_id=None, wall_id=None):
        """Return the list of cell-vertex ids, possibly restricted to the given cell id or cell-wall id.

        The cell-vertex ids are the nodes of the dual graph.

        Parameters
        ----------
        cell_id : int, optional
            The cell id for which to return the list of cell-vertex ids.
            If ``None``, returns all nodes of the dual graph.
        wall_id : (int, int), optional
            The wall id for which to return the list of cell-vertex ids.
            If ``None``, returns all nodes of the dual graph.
            If defined, override the `cell_id` parameter.

        Returns
        -------
        set
            The set of cell-vertex ids, possibly restricted to the given cell id or cell-wall id.

        Examples
        --------
        >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
        >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
        >>> cvids = tg.cell_vertex_ids()  # list all cell-vertex ids
        >>> print(f"The graph contains {len(cvids)} cell-vertices!")
        The graph contains 303 cell-vertices!
        >>> cell_cvids = tg.cell_vertex_ids(4)  # list of cell-vertex ids associated to cell `4`
        >>> print(f"The graph contains {len(cell_cvids)} cell-vertices associated to cell 4!")
        The graph contains 26 cell-vertices associated to cell 4!
        >>> epidermis_cvids = tg.cell_vertex_ids(wall_id=(1, 16))  # list of cell-vertex ids associated to epidermal cell-wall `(1, 16)`
        >>> print(len(epidermis_cvids))
        5
        >>> print(epidermis_cvids)
        {(1, 16, 21, 38), (1, 16, 38, 61), (1, 16, 19, 34), (1, 16, 19, 21), (1, 16, 34, 61)}
        """
        cid = None
        if wall_id is not None:
            cid, cell_id = wall_id
        if cell_id is not None:
            # Search for all nodes in the dual graph with the given cell id:
            cell_vertices = {cv for cv in self.dual.nodes if cell_id in cv}
            if wall_id is None:
                return cell_vertices
            else:
                # Also 'filter' by the other cell id `cid`
                return {cv for cv in cell_vertices if cid in cv}
        else:
            return set(self.dual.nodes())

    def nb_cells(self) -> int:
        """Return the number of cells in the graph.

        Examples
        --------
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> tg.add_cells([1, 2, 3, 4])
        >>> tg.nb_cells()
        4
        """
        return len(self.cell_ids())

    def nb_cell_walls(self) -> int:
        """Return the number of cell-walls in the graph.

        Examples
        --------
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> # Add a list of cell-walls:
        >>> walls = [(3, 2), (3, 4), (3, 5), (3, 6), (3, 7), (2, 4), (4, 5), (5, 6), (6, 7), (2, 7)]
        >>> tg.add_cell_walls(walls)
        >>> tg.nb_cell_walls() == len(walls)
        True
        """
        return len(self.cell_wall_ids())

    def draw_primal_graph(self):
        """Draw the primal graph.

        Examples
        --------
        >>> import matplotlib.pyplot as plt
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> # Add a list of cell-walls:
        >>> walls = [(3, 2), (3, 4), (3, 5), (3, 6), (3, 7), (2, 4), (4, 5), (5, 6), (6, 7), (2, 7)]
        >>> tg.add_cell_walls(walls)
        >>> tg.draw_primal_graph()
        >>> plt.show()
        """
        if 'coordinates' in self.list_cell_properties():
            pos = self.cell_property_dict('coordinates')
        else:
            pos = None
        nx.draw(self.primal, pos=pos, with_labels=True, font_weight='bold')
        return

    def draw_dual_graph(self):
        """Draw the dual graph.

        Examples
        --------
        >>> import matplotlib.pyplot as plt
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> # Add a list of cell-edges:
        >>> edges = [((1, 2, 3, 4), (1, 3, 4, 5)), ((1, 3, 4, 5), (1, 3, 5, 6)), ((1, 3, 5, 6), (1, 3, 6, 7)), ((1, 3, 6, 7), (1, 2, 3, 7)), ((1, 2, 3, 7), (1, 2, 3, 4))]
        >>> tg.add_cell_edges(edges)
        >>> tg.draw_dual_graph()
        >>> plt.show()
        """
        if 'coordinates' in self.list_cell_vertex_properties():
            pos = self.cell_vertex_property_dict('coordinates')
        else:
            pos = None
        nx.draw(self.dual, pos=pos, with_labels=False, node_color='red', node_shape='s', style='dashed')
        return

    def draw_graph(self):
        """Draw the primal and dual graphs.

        Examples
        --------
        >>> import matplotlib.pyplot as plt
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> # Add a list of cell-walls:
        >>> walls = [(3, 2), (3, 4), (3, 5), (3, 6), (3, 7), (2, 4), (4, 5), (5, 6), (6, 7), (2, 7)]
        >>> tg.add_cell_walls(walls)
        >>> # Add a list of cell-edges:
        >>> edges = [((1, 2, 3, 4), (1, 3, 4, 5)), ((1, 3, 4, 5), (1, 3, 5, 6)), ((1, 3, 5, 6), (1, 3, 6, 7)), ((1, 3, 6, 7), (1, 2, 3, 7)), ((1, 2, 3, 7), (1, 2, 3, 4))]
        >>> tg.add_cell_edges(edges)
        >>> tg.draw_graph()
        >>> plt.show()
        """
        self.draw_primal_graph()
        self.draw_dual_graph()
        return

    def cell_neighbors(self, cell_id):
        """Return the neighbors of a cell.

        Parameters
        ----------
        cell_id : int
            The cell id for which to return neighbors.

        Returns
        -------
        list
            The list of neighbors for the given cell id.

        Notes
        -----
        The cell declared as "background" is not a neighbor and will not be returned here.

        Examples
        --------
        >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
        >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
        >>> print(tg.cell_neighbors(16))
        [8, 10, 13, 19, 20, 21, 34, 38, 58, 61]
        >>> print(len(tg.cell_neighbors(16)))
        10
        """
        return sorted(set(self.primal.neighbors(cell_id)) - {self._background})

    def cell_vertex_neighbors(self, cv_id):
        """Return the neighbors of a cell-vertex.

        Parameters
        ----------
        cv_id : int
            The cell-vertex id for which to return neighbors.

        Returns
        -------
        list
            The list of neighbors for the given cell-vertex id.

        Examples
        --------
        >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
        >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
        >>> cv_id = list(tg.cell_vertex_ids(16))[0]  # get the first cell-vertex id from cell 16
        >>> print(cv_id)
        (1, 16, 21, 38)
        >>> print(tg.cell_vertex_neighbors(cv_id))  # get the neighbors of the obtained cell-vertex id
        [(1, 16, 19, 21), (1, 16, 38, 61), (1, 21, 38, 58)]
        """
        return sorted(set(self.dual.neighbors(cv_id)))

    def cell_neighbors_dict(self, cell_ids=None):
        """Return the neighborhood dictionary of a list of cells.

        Parameters
        ----------
        cell_ids : list of int, optional
            The list of cell ids for which to return the neighborhood dictionary.
            If `None` (default), all cells neighborhood are returned.

        Returns
        -------
        dict
            The cell-indexed neighborhood dictionary for the given cell ids.

        Examples
        --------
        >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
        >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
        >>> tg.cell_neighbors_dict([2, 3, 4, 5])
        {2: [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
         3: [2, 5, 9, 10, 11, 14, 15, 23, 25, 26, 41, 48, 52, 54, 56, 63],
         4: [2, 6, 7, 8, 11, 12, 18, 29, 37, 39, 43, 46, 50, 51, 53],
         5: [2, 3, 7, 11, 13, 15, 22, 23, 27, 41, 44, 49, 56, 57, 62]}
        """
        if cell_ids is None:
            cell_ids = self.cell_ids()
        return {cid: self.cell_neighbors(cid) for cid in cell_ids}

    def epidermal_cell_ids(self):
        """List the epidermal cell ids.

        Returns
        -------
        list
            The list of epidermal cell ids.

        Notes
        -----
        This requires prior definition of an 'epidermis' identity (boolean property) on cells with ``self.add_cell_identity()``.
        This can be achieved using the tissue image (with a defined background id) during graph initialization.

        Examples
        --------
        >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
        >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
        >>> epidermal_cids = tg.epidermal_cell_ids()
        >>> print(epidermal_cids)
        [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64]
        >>> print(len(epidermal_cids))
        49
        """
        try:
            assert 'epidermis' in self._cell_properties
        except AssertionError:
            log.error("Missing 'epidermis' cell identity! Add it first!")
            return []
        return self.list_cells_with_identity('epidermis')

    def epidermal_cell_wall_ids(self):
        """List the epidermal cell-wall ids.

        Returns
        -------
        list
            The list of epidermal cell-wall ids (edges of the primal graph).

        Notes
        -----
        This requires prior definition of a background id with ``self.background()``.
        This requires prior definition of an 'epidermis' identity (boolean property) on cells with ``self.add_cell_identity()``.
        Both can be achieved using the tissue image (with a defined background id) during graph initialization.

        Examples
        --------
        >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
        >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
        >>> epidermal_cwids = tg.epidermal_cell_wall_ids()
        >>> print(epidermal_cwids[:5])
        [(1, 16), (1, 17), (1, 18), (1, 19), (1, 20)]
        >>> print(len(epidermal_cwids))
        49
        """
        ep_cids = self.epidermal_cell_ids()
        return [stuple((self.background, cid)) for cid in ep_cids]

    def epidermal_cell_edge_ids(self, cell_id):
        """Return the list of cell-edge ids associated to the given epidermal cell.

        Parameters
        ----------
        cell_id : int
            The cell id for which to return the list of cell-edge ids.

        Returns
        -------
        list
            The list of cell-edge ids (edges of the dual graph) associated to the cell.

        Notes
        -----
        This requires prior definition of a background id with ``self.background()``.
        This requires prior definition of an 'epidermis' identity (boolean property) on cells with ``self.add_cell_identity()``.
        Both can be achieved using the tissue image (with a defined background id) during graph initialization.

        Examples
        --------
        >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
        >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
        >>> ce_ids = tg.epidermal_cell_edge_ids(16)  # list of cell-edge ids associated to cell `16`
        >>> print(len(ce_ids))
        5
        >>> print(ce_ids)
        [((1, 16, 19, 34), (1, 16, 19, 21)), ((1, 16, 19, 34), (1, 16, 34, 61)), ((1, 16, 19, 21), (1, 16, 21, 38)), ((1, 16, 21, 38), (1, 16, 38, 61)), ((1, 16, 34, 61), (1, 16, 38, 61))]
        """
        bid = self.background
        try:
            assert 'epidermis' in self._cell_properties
        except AssertionError:
            raise ValueError("Missing 'epidermis' cell property! Add it first!")

        if self.cell_property(cell_id, 'epidermis', False):
            return [ce for ce in self.dual.edges if
                    cell_id in ce[0] and cell_id in ce[1] and bid in ce[0] and bid in ce[1]]
        else:
            log.warning(f"Cell {cell_id} does not belong to the epidermis!")
            return []

    def epidermal_cell_vertex_ids(self, cell_id):
        """Return the list of cell-vertex ids associated to the given epidermal cell.

        Parameters
        ----------
        cell_id : int
            The cell id for which to return the list of cell-vertex ids.

        Returns
        -------
        list
            The list of cell-vertex ids (nodes of the dual graph) associated to the cell.

        Notes
        -----
        This requires prior definition of a background id with ``self.background()``.
        This requires prior definition of an 'epidermis' identity (boolean property) on cells with ``self.add_cell_identity()``.
        Both can be achieved using the tissue image (with a defined background id) during graph initialization.

        Examples
        --------
        >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
        >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
        >>> cv_ids = tg.epidermal_cell_vertex_ids(16)  # list of cell-vertex ids associated to cell `16`
        >>> print(len(cv_ids))
        5
        >>> print(cv_ids)
        [(1, 16, 19, 34), (1, 16, 19, 21), (1, 16, 21, 38), (1, 16, 34, 61), (1, 16, 38, 61)]
        """
        bid = self.background
        try:
            assert 'epidermis' in self._cell_properties
        except AssertionError:
            raise ValueError("Missing 'epidermis' cell property! Add it first!")
        if self.cell_property(cell_id, 'epidermis', False):
            return [cv for cv in self.dual.nodes if cell_id in cv and bid in cv]
        else:
            log.warning(f"Cell {cell_id} does not belong to the epidermis!")
            return []

    def _update_ppty(self, graph, elem, ppty_name, ppty_values):
        g = getattr(self, graph)
        g_elem = getattr(g, elem)
        for k, v in ppty_values.items():
            g_elem[k][ppty_name] = v
        return

    def _add_ppty(self, graph, elem, ppty_name, ppty_values):
        """Hidden method to add a dictionary of property to the graph.

        Parameters
        ----------
        graph : {'primal', 'dual'}
            The graph receiving the property.
        elem : {'nodes', 'edges'}
            The elements of the graph receiving the values.
        ppty_name : str
            Name of the property, *e.g.* 'volume'.
        ppty_values : dict
            Dictionary of property, *e.g.* ``{cid: ppty_value}`` with ``graph='primal'` and ``elem='nodes'``.

        Returns
        -------
        int
            The number of added property values. Used to avoid listing a property if no element where found.
        """
        g = getattr(self, graph)
        g_elem = getattr(g, elem)
        elem_ids = list(g_elem())
        unknown_keys = []
        for k, v in ppty_values.items():
            if isinstance(k, tuple):
                if (graph == 'primal' and elem == 'edges') or (graph == 'dual' and elem == 'nodes'):
                    k = stuple(k)
                else:
                    k = (stuple(k[0]), stuple(k[1]))
            if k not in elem_ids:
                if elem == 'edges':
                    try:
                        assert (k[1], k[0]) in elem_ids
                    except AssertionError:
                        unknown_keys.append(k)
                    else:
                        g_elem[(k[1], k[0])][ppty_name] = v
                else:
                    unknown_keys.append(k)
            else:
                g_elem[k][ppty_name] = v

        if unknown_keys:
            log.warning(
                f"The '{ppty_name}' dictionary has {len(unknown_keys)} keys unknown to the {graph} graph {elem}.")
            log.warning(
                f"This represent {round(len(unknown_keys) / len(ppty_values) * 100, 1)}% of the input dictionary.")

        return len(ppty_values) - len(unknown_keys)

    def add_cell_property(self, name, values, unit=""):
        """Add a cell property dictionary to the graph.

        Parameters
        ----------
        name : str
            Name of the cell property to add.
        values : dict
            Cell id indexed dictionary ``{cid: ppty_value}``.
        unit : str, optional
            The unit in which the property should be expressed.

        Examples
        --------
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> tg.add_cells([2, 3, 4, 5, 6])
        >>> # Creates and add an example "volume" property to the cells:
        >>> volume = {2: 10., 3:20., 4:30., 5:40., 6:50.}
        >>> tg.add_cell_property('volume', volume, 'A.U.')
        >>> print(tg.list_cell_properties())  # list the known cell properties
        ['volume']
        >>> print(tg.cell_property_dict("volume"))
        {2: 10.0, 3: 20.0, 4: 30.0, 5: 40.0, 6: 50.0}
        >>> # Try to add a value to undefined cell-id:
        >>> tg.add_cell_property('test', {12: 112}, 'A.U.')
        INFO - Got a dictionary with 1 keys unknown to the primal graph nodes.
        INFO - [12]
        """
        n_added = self._add_ppty('primal', 'nodes', name, values)
        if n_added > 0:
            self._cell_properties.update({name: unit})
            if n_added > 1 and self.verbose:
                log.info(f"Added {n_added} cell values to property '{name}'!")
        return

    def add_cell_wall_property(self, name, values, unit=""):
        """Add a cell-wall property dictionary to the graph.

        Parameters
        ----------
        name : str
            Name of the cell-wall property to add.
        values : dict
            Cell-wall id indexed dictionary ``{wid: ppty_value}``.
        unit : str, optional
            The unit in which the property should be expressed.

        Notes
        -----
        A cell-wall id ``wid`` is defined as a pair of cell ids, *i.e.* a len-2 tuple.

        Examples
        --------
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> walls = [(3, 2), (3, 4), (3, 5), (3, 6), (3, 7), (2, 4), (4, 5), (5, 6), (6, 7), (2, 7)]
        >>> tg.add_cell_walls(walls)
        >>> # Creates and add an example "area" property to the cell-walls:
        >>> wall_area = {(2, 3): 32, (2, 4): 34, (2, 7): 18, (3, 4): 8, (3, 5): 25, (3, 6): 1, (3, 7): 36, (4, 5): 20, (5, 6): 11, (6, 7): 40}
        >>> tg.add_cell_wall_property("area", wall_area, "µm²")
        >>> print(tg.list_cell_wall_properties())  # list the known cell-wall properties
        ['area']
        >>> print(tg.cell_wall_property_dict("area"))
        {(2, 3): 32, (2, 4): 34, (2, 7): 18, (3, 4): 8, (3, 5): 25, (3, 6): 1, (3, 7): 36, (4, 5): 20, (5, 6): 11, (6, 7): 40}
        """
        n_added = self._add_ppty('primal', 'edges', name, values)
        if n_added > 0:
            self._wall_properties.update({name: unit})
            if n_added > 1 and self.verbose:
                log.info(f"Added {n_added} cell-wall values to property '{name}'!")
        return

    def add_cell_edge_property(self, name, values, unit=""):
        """Add a cell-edge property dictionary to the graph.

        Parameters
        ----------
        name : str
            Name of the cell-edge property to add.
        values : dict
            Cell-edge id indexed dictionary ``{eid: ppty_value}``.
        unit : str, optional
            The unit in which the property should be expressed.

        Notes
        -----
        A cell-vertex id ``vid`` is defined as a triplet (2D) or quadruplet (3D) of cell ids ``cid``, *i.e.* a len-3 or a len-4 tuple.
        A cell-edge id ``eid`` is defined as a pair of cell-vertex ids ``vid``, *i.e.* a len-2 tuple ``(vid, vid)``.
        The order of the cell ids in the tuple defining the cell-vertex ids is irrelevant, they are sorted.

        Examples
        --------
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> edges = [((1, 2, 3, 4), (1, 3, 4, 5)), ((1, 3, 4, 5), (1, 3, 5, 6)), ((1, 3, 5, 6), (1, 3, 6, 7)), ((1, 3, 6, 7), (1, 2, 3, 7)), ((1, 2, 3, 7), (1, 2, 3, 4))]
        >>> tg.add_cell_edges(edges)
        >>> # Creates and add an example "length" property to the cell-edges:
        >>> e_coords = {((1, 2, 3, 4), (1, 3, 4, 5)): 14, ((1, 2, 3, 4), (1, 2, 3, 7)): 81, ((1, 3, 4, 5), (1, 3, 5, 6)): 30, ((1, 3, 5, 6), (1, 3, 6, 7)): 69, ((1, 3, 6, 7), (1, 2, 3, 7)): 1}
        >>> tg.add_cell_edge_property("length", e_coords, "µm")
        >>> print(tg.list_cell_edge_properties())  # list the known cell-edge properties
        ['length']
        >>> print(tg.cell_edge_property_dict("length"))
        {((1, 2, 3, 4), (1, 3, 4, 5)): 14, ((1, 2, 3, 4), (1, 2, 3, 7)): 81, ((1, 3, 4, 5), (1, 3, 5, 6)): 30, ((1, 3, 5, 6), (1, 3, 6, 7)): 69, ((1, 3, 6, 7), (1, 2, 3, 7)): 1}
        """
        n_added = self._add_ppty('dual', 'edges', name, values)
        if n_added > 0:
            self._edge_properties.update({name: unit})
            if n_added > 1 and self.verbose:
                log.info(f"Added {n_added} cell-edge values to property '{name}'!")
        return

    def add_cell_vertex_property(self, name, values, unit=""):
        """Add a cell-vertex property dictionary to the graph.

        Parameters
        ----------
        name : str
            Name of the cell-vertex property to add.
        values : dict
            Cell-vertex id indexed dictionary ``{vid: ppty_value}``.
        unit : str, optional
            The unit in which the property should be expressed.

        Notes
        -----
        A cell-vertex id is defined as a triplet (2D) or quadruplet (3D) of cell ids ``cid``, *i.e.* a len-3 or a len-4 tuple.
        The order of the cell ids in the tuple defining the cell-vertex ids is irrelevant, they are sorted.

        Examples
        --------
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> vertices = [(1, 2, 3, 4), (1, 3, 4, 5), (1, 3, 5, 6)]
        >>> tg.add_cell_vertices(vertices)
        >>> # Creates and add an example 3D "coordinates" property to the cell-vertices:
        >>> v_coords = {(1, 2, 3, 4): (19, 45, 42), (1, 3, 4, 5): (18, 25, 45), (1, 3, 5, 6): (14, 21, 29)}
        >>> tg.add_cell_vertex_property("coordinate", v_coords, "voxels")
        >>> print(tg.list_cell_vertex_properties())  # list the known cell-vertex properties
        ['coordinate']
        >>> print(tg.cell_vertex_property_dict("coordinate"))
        {(1, 2, 3, 4): (19, 45, 42), (1, 3, 4, 5): (18, 25, 45), (1, 3, 5, 6): (14, 21, 29)}
        """
        n_added = self._add_ppty('dual', 'nodes', name, values)
        if n_added > 0:
            self._vertex_properties.update({name: unit})
            if n_added > 1 and self.verbose:
                log.info(f"Added {n_added} cell-vertex values to property '{name}'!")
        return

    def list_cell_properties(self) -> list:
        """List known cell properties to the graph."""
        return list(self._cell_properties.keys())

    def list_cell_wall_properties(self) -> list:
        """List known cell-wall properties to the graph."""
        return list(self._wall_properties.keys())

    def list_cell_edge_properties(self) -> list:
        """List known cell-edge properties to the graph."""
        return list(self._edge_properties.keys())

    def list_cell_vertex_properties(self) -> list:
        """List known cell-vertex properties to the graph."""
        return list(self._vertex_properties.keys())

    def cell_property_unit(self, ppty) -> str:
        """Get the unit of a cell property."""
        return cp.copy(self._cell_properties[ppty])

    def cell_wall_property_unit(self, ppty) -> str:
        """Get the unit of a cell-wall property."""
        return cp.copy(self._wall_properties[ppty])

    def cell_edge_property_unit(self, ppty) -> str:
        """Get the unit of a cell-edge property."""
        return cp.copy(self._edge_properties[ppty])

    def cell_vertex_property_unit(self, ppty) -> str:
        """Get the unit of a cell-vertex property."""
        return cp.copy(self._vertex_properties[ppty])

    def cell_property(self, cid, ppty, default=None):
        """Return the property value of a cell.

        Parameters
        ----------
        cid : int
            A cell id.
        ppty : str
            Name of the cell property to returns.
        default : Any, optional
            Value to return if the property is not defined for this cell, default is ``None``.

        Returns
        -------
        Any
            Cell property of given id.

        Examples
        --------
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> tg.add_cells([2, 3, 4, 5, 6])
        >>> tg.cell_ids()
        [2, 3, 4, 5, 6]
        >>> # Add example 'test' values to the cells
        >>> tg.add_cell_property('test', {2: 10, 3:20, 4:30, 5:40, 6:50}, "A.U.")
        >>> # Get the 'test' property value for cell `2`
        >>> tg.cell_property(2, 'test', 0)
        10
        >>> # The 'foo' property is undefined, so the default value is returned
        >>> tg.cell_property(2, 'foo', 0)
        0
        >>> # Cell id `10` is undefined, so the default value is returned
        >>> tg.cell_property(10, 'test', 0)
        0
        """
        data = self.primal.get_node_data(cid, default)
        if isinstance(data, dict):
            return data.get(ppty, default)
        else:
            return default

    def cell_wall_property(self, wid, ppty, default=None):
        """Return the property value of a cell-wall.

        Parameters
        ----------
        wid : (int, int)
            A cell-wall id, as a len-2 tuple of cell ids.
        ppty : str
            Name of the cell-wall property to returns.
        default : Any, optional
            Value to return if the property is not defined for this cell-wall, default is ``None``.

        Returns
        -------
        Any
            Cell-wall property of given id.

        Examples
        --------
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> walls = [(3, 2), (3, 4), (3, 5), (3, 6)]
        >>> tg.add_cell_walls(walls)
        >>> tg.cell_wall_ids()
        [(2, 3), (3, 4), (3, 5), (3, 6)]
        >>> tg.cell_ids()
        [2, 3, 4, 5, 6]
        >>> # Add example 'area' values to the cell-walls
        >>> wall_area = {(3, 2): 3.5, (3, 4): 4.5, (3, 5): 5.5, (3, 6): 6.5}
        >>> tg.add_cell_wall_property('area', wall_area, "µm²")
        >>> # Get the 'area' property value for cell-wall `(3, 2)`
        >>> tg.cell_wall_property((3, 2), 'area', 0)
        3.5
        >>> # The 'foo' property is undefined, so the default value is returned
        >>> tg.cell_wall_property((3, 2), 'foo', 0)
        0
        >>> # Cell-wall id `(3, 10)` is undefined, so the default value is returned
        >>> tg.cell_wall_property((3, 10), 'area', 0)
        0
        """
        data = self.primal.get_edge_data(*wid, default=default)
        if isinstance(data, dict):
            return data.get(ppty, default)
        else:
            return default

    def cell_edge_property(self, eid, ppty, default=None):
        """Return the property value of a cell-edge.

        Parameters
        ----------
        eid : tuple of tuple of int
            A cell-edge id, as a len-2 tuple of cell-vertex ids.
        ppty : str
            Name of the cell-edge property to returns.
        default : Any, optional
            Value to return if the property is not defined for this cell-edge, default is ``None``.

        Returns
        -------
        Any
            Cell-edge property of given id.

        Examples
        --------
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> edges = [((1, 2, 3, 4), (1, 3, 4, 5)), ((1, 2, 3, 4), (1, 2, 3, 6))]
        >>> tg.add_cell_edges(edges)
        >>> tg.cell_edge_ids()
        [((1, 2, 3, 4), (1, 3, 4, 5)), ((1, 2, 3, 4), (1, 2, 3, 6))]
        >>> tg.cell_vertex_ids()
        [(1, 2, 3, 4), (1, 3, 4, 5), (1, 2, 3, 6)]
        >>> # Add example 'length' values to the cell-edges
        >>> edge_length = {((1, 2, 3, 4), (1, 3, 4, 5)): 25., ((1, 2, 3, 4), (1, 2, 3, 6)): 26.}
        >>> tg.add_cell_edge_property('length', edge_length, "µm")
        >>> # Get the 'length' property value for cell-edge `((1, 2, 3, 4), (1, 3, 4, 5))`
        >>> tg.cell_edge_property(((1, 2, 3, 4), (1, 3, 4, 5)), 'length', 0)
        25.0
        >>> # The 'foo' property is undefined, so the default value is returned
        >>> tg.cell_edge_property(((1, 2, 3, 4), (1, 3, 4, 5)), 'foo', 0)
        0
        >>> # Cell-edge `((1, 2, 3, 4), (1, 2, 3, 7))` is undefined, so the default value is returned
        >>> tg.cell_edge_property(((1, 2, 3, 4), (1, 2, 3, 7)), 'length', 0)
        0
        """
        data = self.dual.get_edge_data(*eid, default=default)
        if isinstance(data, dict):
            return data.get(ppty, default)
        else:
            return default

    def cell_vertex_property(self, vid, ppty, default=None):
        """Return the property value of a cell-vertex.

        Parameters
        ----------
        vid : tuple of tuple of int
            A cell-vertex id, as a len-4 tuple (3D) or len-3 tuple (2D) of integers.
        ppty : str
            Name of the cell-vertex property to returns.
        default : Any, optional
            Value to return if the property is not defined for this cell-vertex, default is ``None``.

        Returns
        -------
        Any
            Cell-vertex property of given id.

        Examples
        --------
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> vertices = [(1, 2, 3, 4), (1, 3, 4, 5), (1, 2, 3, 6)]
        >>> tg.add_cell_vertices(vertices)
        >>> tg.cell_vertex_ids()
        [(1, 2, 3, 4), (1, 3, 4, 5), (1, 2, 3, 6)]
        >>> # Add example 'coordinate' values to the cell-vertices
        >>> vertex_coordinate = {(1, 2, 3, 4): [0., 0. ,0.], (1, 3, 4, 5): [0., 0. ,1.], (1, 2, 3, 6): [0., 1. ,0.]}
        >>> tg.add_cell_vertex_property('coordinate', vertex_coordinate, "voxels")
        >>> # Get the 'coordinate' property value for cell-vertex `(1, 3, 4, 5)`
        >>> tg.cell_vertex_property((1, 3, 4, 5), 'coordinate', 0)
        [0., 0. ,1.]
        >>> # The 'foo' property is undefined, so the default value is returned
        >>> tg.cell_vertex_property((1, 2, 3, 4), 'foo', [])
        []
        >>> # Cell-vertex` '(1, 2, 3, 9)'` is undefined, so the default value is returned
        >>> tg.cell_vertex_property((1, 2, 3, 9), 'coordinate', [])
        []
        """
        data = self.dual.get_node_data(vid, default=default)
        if isinstance(data, dict):
            return data.get(ppty, default)
        else:
            return default

    def cell_properties(self, cid, ppties=None, default=None):
        """Return the properties and associated values of a cell.

        Parameters
        ----------
        cid : int
            A cell id.
        ppties : list of str, optional
            If defined, a list of cell properties to returns.
        default : Any, optional
            Value to return if the property is not defined for this cell, default is ``None``.

        Returns
        -------
        dict
            Cell properties of given id, indexed by property name(s).

        Examples
        --------
        >>> import numpy as np
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> cells = [2, 3, 4, 5, 6]
        >>> tg.add_cells(cells)
        >>> # Create example 'test' and 'foo' cell properties with random values:
        >>> rng = np.random.default_rng(9)
        >>> cell_size = {cid: rng.integers(10, 50) for cid in cells}
        >>> cell_something = {cid: rng.integers(10, 50) for cid in cells}
        >>> # Add them as cell properties with some unit:
        >>> tg.add_cell_property('size', cell_size, 'voxels')
        >>> tg.add_cell_property('something', cell_something, 'A.U.')
        >>> # Get all cell properties associated to the first cell:
        >>> tg.cell_properties(cells[0])
        {'size': 26, 'something': 34}
        >>> # Get the 'size' cell property associated to the first cell:
        >>> tg.cell_properties(cells[0], 'size')
        {'size': 26}
        >>> # Get the 'something' and 'foo' cell properties associated to the first cell:
        >>> tg.cell_properties(cells[0], ['something', 'foo'], default=np.nan)
        {'something': 34, 'foo': nan}
        >>> # If the cell id do not exist, we return the default value:
        >>> tg.cell_properties(7, default=np.nan)
        nan
        """
        if isinstance(ppties, str):
            ppties = [ppties]
        data = self.primal.get_node_data(cid, default)
        if isinstance(data, dict):
            if ppties is None:
                return data
            else:
                return {ppty: data.get(ppty, default) for ppty in ppties}
        else:
            return default

    def cell_wall_properties(self, wid, ppties=None, default=None):
        """Return the properties and associated values of a cell-wall.

        Parameters
        ----------
        wid : (int, int)
            A cell-wall id, as a len-2 tuple of cell ids.
        ppties : list of str, optional
            If defined, a list of cell-wall properties to returns.
        default : Any, optional
            Value to return if the property is not defined for this cell-wall, default is ``None``.

        Returns
        -------
        dict
            Cell-wall properties of given cell-wall id, indexed by property name(s).

        Examples
        --------
        >>> import numpy as np
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> walls = [(3, 2), (3, 4), (3, 5), (3, 6)]
        >>> tg.add_cell_walls(walls)
        >>> # Create example 'area' and 'something' cell-wall properties with random values:
        >>> rng = np.random.default_rng(9)
        >>> wall_area = {wid: rng.integers(10, 50) for wid in walls}
        >>> wall_something = {wid: rng.integers(10, 50) for wid in walls}
        >>> # Add them as cell-wall properties with some unit:
        >>> tg.add_cell_wall_property("area", wall_area, "voxels")
        >>> tg.add_cell_wall_property("something", wall_something, "A.U.")
        >>> # Get all cell-wall properties associated to the first cell-wall:
        >>> tg.cell_wall_properties(walls[0])
        {'area': 26, 'something': 14}
        >>> # Get the 'length' cell-wall property associated to the first cell-wall:
        >>> tg.cell_wall_properties(walls[0], 'area')
        {'area': 26}
        >>> # Get the 'something' and 'foo' cell-wall properties associated to the first cell-wall:
        >>> tg.cell_wall_properties(walls[0], ['something', 'foo'], default=np.nan)  # 'foo' property is undefined so default value is returned
        {'something': 14, 'foo': nan}
        >>> # If the cell-wall id do not exist, we return the default value:
        >>> tg.cell_wall_properties((3, 10), 'area', default=np.nan)
        nan
        """
        if isinstance(ppties, str):
            ppties = [ppties]
        data = self.primal.get_edge_data(*wid, default=default)
        if isinstance(data, dict):
            if ppties is None:
                return data
            else:
                return {ppty: data.get(ppty, default) for ppty in ppties}
        else:
            return default

    def cell_edge_properties(self, eid, ppties=None, default=None):
        """Return the properties and associated values of a cell-edge.

        Parameters
        ----------
        eid : tuple of tuple of int
            A cell-edge id, as a len-2 tuple of cell-vertex ids.
        ppties : list of str, optional
            If defined, a list of cell-edge properties to returns.
        default : Any, optional
            Value to return if the property is not defined for this cell-edge, default is ``None``.

        Returns
        -------
        dict
            Cell-edge properties of given cell-edge id, indexed by property name(s).

        Examples
        --------
        >>> import numpy as np
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> edges = [((1, 2, 3, 4), (1, 3, 4, 5)), ((1, 3, 4, 5), (1, 3, 5, 6)), ((1, 3, 5, 6), (1, 3, 6, 7)), ((1, 3, 6, 7), (1, 2, 3, 7)), ((1, 2, 3, 7), (1, 2, 3, 4))]
        >>> tg.add_cell_edges(edges)
        >>> # Create example 'length' and 'something' cell-edge properties with random values:
        >>> rng = np.random.default_rng(9)
        >>> edge_length = {eid: rng.integers(10, 50) for eid in edges}
        >>> edge_something = {eid: rng.integers(10, 50) for eid in edges}
        >>> # Add them as cell-edge properties with some unit:
        >>> tg.add_cell_edge_property("length", edge_length, unit="voxels")
        >>> tg.add_cell_edge_property("something", edge_something, unit="A.U.")
        >>> # Get all cell-edge properties associated to the first cell-edge:
        >>> tg.cell_edge_properties(edges[0])
        {'length': 26, 'something': 34}
        >>> # Get the 'length' cell-edge property associated to the first cell-edge:
        >>> tg.cell_edge_properties(edges[0], 'length')
        {'length': 26}
        >>> # Get the 'something' and 'foo' cell-edge properties associated to the first cell-edge:
        >>> tg.cell_edge_properties(edges[0], ['something', 'foo'], default=np.nan)  # 'foo' property is undefined so default value is returned
        {'something': 34, 'foo': nan}
        >>> # If the cell-edge id do not exist, we return the default value:
        >>> tg.cell_edge_properties(((1, 2, 3, 4), (1, 2, 3, 5)), default=np.nan)
        nan
        """
        if isinstance(ppties, str):
            ppties = [ppties]
        data = self.dual.get_edge_data(*eid, default=default)
        if isinstance(data, dict):
            if ppties is None:
                return data
            else:
                return {ppty: data.get(ppty, default) for ppty in ppties}
        else:
            return default

    def cell_vertex_properties(self, vid, ppties=None, default=None):
        """Return the properties and associated values of a cell-vertex.

        Parameters
        ----------
        vid : tuple of int
            A cell-vertex id, as a len-4 tuple of cell ids.
        ppties : list of str, optional
            If defined, a list of cell-vertex properties to returns.
        default : Any, optional
            Value to return if the property is not defined for this cell-vertex, default is ``None``.

        Returns
        -------
        dict
            Cell-vertex properties of given cell-vertex id, indexed by property name(s).

        Examples
        --------
        >>> import numpy as np
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> vertices = [(1, 2, 3, 4), (1, 3, 4, 5), (1, 3, 5, 6), (1, 3, 6, 7), (1, 2, 3, 7), (1, 2, 3, 4)]
        >>> tg.add_cell_vertices(vertices)
        >>> # Create example 'bar' and 'something' cell-vertex properties with random values:
        >>> rng = np.random.default_rng(9)
        >>> vertex_bar = {vid: rng.integers(10, 50) for vid in vertices}
        >>> vertex_something = {vid: rng.integers(10, 50) for vid in vertices}
        >>> # Add them as cell-vertex properties with some unit:
        >>> tg.add_cell_vertex_property("bar", vertex_bar, unit="voxels")
        >>> tg.add_cell_vertex_property("something", vertex_something, unit="A.U.")
        >>> # Get all cell-vertex properties associated to the first cell-vertex:
        >>> tg.cell_vertex_properties(vertices[0])
        {'bar': 34, 'something': 46}
        >>> # Get the 'bar' cell-vertex property associated to the first cell-vertex:
        >>> tg.cell_vertex_properties(vertices[0], 'bar')
        {'bar': 34}
        >>> # Get the 'something' and 'foo' cell-vertex properties associated to the first cell-vertex:
        >>> tg.cell_vertex_properties(vertices[0], ['something', 'foo'],default=np.nan)  # 'foo' property is undefined so default value is returned
        {'something': 46, 'foo': nan}
        >>> # If the cell-vertex id do not exist, we return the default value:
        >>> tg.cell_vertex_properties((1, 2, 3, 5), default=np.nan)
        nan
        """
        if isinstance(ppties, str):
            ppties = [ppties]
        data = self.dual.get_node_data(vid, default=default)
        if isinstance(data, dict):
            if ppties is None:
                return data
            else:
                return {ppty: data.get(ppty, default) for ppty in ppties}
        else:
            return default

    def _add_cell_identity(self, identity, cids, strict=False):
        """Hidden method to add cell identity without the checks."""
        id_ppty = {cid: True for cid in cids}
        if strict:
            other_cids = list(set(self.cell_ids()) - set(cids))
            id_ppty.update({cid: False for cid in other_cids})
        self.add_cell_property(identity, id_ppty, unit="id")
        return

    def _remove_cell_identity(self, identity, cids):
        """Hidden method to remove cell identity without the checks."""
        self.add_cell_property(identity, {cid: False for cid in cids}, unit="id")
        return

    def add_cell_identity(self, identity, cids, strict=False):
        """Add an identity to a list of cells.

        Parameters
        ----------
        identity : str
            Name of the identity.
        cids : int or list, optional
            List of cell ids to add to the identity.
        strict : bool, optional
            If ``True`` (default is ``False``), make sure to exclude all other cell-ids from this identity.
            By default, only update the list of given cell as belonging to this identity.

        Examples
        --------
        >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
        >>> from timagetk.features.graph import list_cells_in_layer
        >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
        >>> layer2 = list_cells_in_layer(tg,layer=2)  # get the list of cells from the tissue second layer
        >>> print(len(layer2))
        13
        >>> tg.add_cell_identity('Layer2', layer2)
        >>> tg.list_identities()
        ['epidermis', 'Layer2']
        >>> tg.list_cells_with_identity('Layer2')
        [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        >>> tg.add_cell_identity('Layer2', 16)
        >>> tg.list_cells_with_identity('Layer2')
        [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]
        >>> tg.add_cell_identity('Layer2', [3, 4, 5], strict=True)
        >>> tg.list_cells_with_identity('Layer2')
        [3, 4, 5]
        """
        try:
            assert isinstance(identity, str)
        except AssertionError:
            raise TypeError("Given identity is not a string!")
        # Add this "identity" name if unknown to the graph:
        if identity not in self._cell_identities:
            self._cell_identities.append(identity)
        # Ensure `cids` is iterable:
        if isinstance(cids, int):
            cids = [cids]
        # Call the hidden method to add an identity to cells:
        self._add_cell_identity(identity, cids, strict)
        return

    def remove_cell_identity(self, identity, cids=None):
        """Remove a list of cells from given identity.

        Parameters
        ----------
        identity : str
            Name of the identity.
        cids : int or list, optional
            List of cell ids to remove from the given identity.
            By default, ``None``, remove all cells with this identity.

        Examples
        --------
        >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
        >>> from timagetk.features.graph import list_cells_in_layer
        >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
        >>> layer2 = list_cells_in_layer(tg,layer=2)  # get the list of cells from the tissue second layer
        >>> tg.add_cell_identity('Layer2', layer2)  # add this list of cell-ids as 'Layer2'
        >>> tg.list_identities()
        ['epidermis', 'Layer2']
        >>> tg.list_cells_with_identity('Layer2')
        [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        >>> tg.remove_cell_identity('Layer2', 15)
        >>> tg.list_cells_with_identity('Layer2')
        [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
        >>> tg.remove_cell_identity('Layer2')
        >>> tg.list_identities()
        """
        try:
            assert isinstance(identity, str)
        except AssertionError:
            raise TypeError("Given identity is not a string!")
        # Check this "identity" name exists:
        if identity not in self._cell_identities:
            log.error(f"Identity '{identity}' is not defined in the object!")
            return

        # Get all cells with this identity:
        if cids is None:
            cids = self.list_cells_with_identity(identity)
        # Ensure `cids` is iterable:
        if isinstance(cids, int):
            cids = [cids]

        # Call the hidden method to add an identity to cells:
        self._remove_cell_identity(identity, cids)

        # Check if the list of cell with this identity is empty, if so, clear it:
        cids = self.list_cells_with_identity(identity)
        if cids == []:
            # Remove the attribute from all nodes:
            [self.primal._node[n].pop(identity, None) for n in self.primal.nodes]
            # Remove it from the list of cell identities:
            self._cell_identities.remove(identity)
        return

    def list_identities(self) -> list:
        """List all the cell identities known to the tissue graph."""
        return self._cell_identities

    def cell_identities(self, cid):
        """List all identities attached to this cell.

        Parameters
        ----------
        cid : int or list or None, optional
            If ``None`` (default) return the dictionary for all cells.
            If an integer or a list return the dictionary for selected cells.

        Examples
        --------
        >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
        >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
        >>> tg.add_cell_identity('test', [16])
        >>> tg.cell_identities(16)
        ['epidermis', 'test']
        """
        return [idty for idty in self.list_identities() if self.cell_has_identity(cid, idty)]

    def cell_identities_dict(self, cids=None):
        """Cell-based dictionary of cell identities.

        Parameters
        ----------
        cids : int or list or None, optional
            If ``None`` (default) return the dictionary for all cells.
            If an integer or a list return the dictionary for selected cells.

        Returns
        -------
        dict
            Cell-based dictionary of cell identities.

        Examples
        --------
        >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
        >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
        >>> tg.cell_identities_dict()
        False
        """
        if cids is None:
            cids = self.cell_ids()
        return {cid: self.cell_identities(cid) for cid in cids}

    def cell_has_identity(self, cid, identity):
        """Test if the cell has the given identity.

        Parameters
        ----------
        cid : int
            A valid cell id at given time-point.
        identity : str
            Name of a valid cell identity, *i.e.* a boolean property.

        Returns
        -------
        bool
            Boolean describing if the cell has the tested `identity`.

        Examples
        --------
        >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
        >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
        >>> tg.cell_has_identity(5, 'epidermis')  # cell 5 from t0 does not belong to the epidermis!
        False
        >>> tg.cell_has_identity(16, 'epidermis')  # cell 5 from t0 does not belong to the epidermis!
        True
        """
        return self.cell_property(cid, identity, default=False)

    def list_cells_with_identity(self, identity):
        """List all cells presenting the selected identity.

        Parameters
        ----------
        identity : str
            Name of a valid cell identity, *i.e.* a boolean property.

        Returns
        -------
        list
            List of cells that present the defined `identity`.

        Examples
        --------
        >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
        >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
        >>> # List cells with the 'epidermis' identity
        >>> epidermal_cell_ids = tg.list_cells_with_identity('epidermis')
        >>> len(epidermal_cell_ids)  # should be 49
        49
        """
        return list(self.cell_property_dict(identity, default=False, only_defined=True).keys())

    def _get_property(self, graph, elem, ppty, ids=None, default=None, only_defined=False):
        """Return a dictionary of property.

        Parameters
        ----------
        graph : {'primal', 'dual'}
            The graph receiving the property.
        elem : {'nodes', 'edges'}
            The elements of the graph receiving the values.
        ppty : str
            Name of the property, *e.g.* volume.
        ids : list, optional
            A list of ids used to filter keys of returned dictionary.
            By default, all ids are returned.
        default : Any, optional
            Value to return if the node or edge id is not found, default is ``None``.
            We strongly advise using ``None`` or ``numpy.nan`` with ``only_defined=True``.
        only_defined : bool, optional
            Returns the dictionary only for ids with "valid" values, *i.e.* a non-default value.

        Returns
        -------
        dict
            Ids indexed property dictionary, ``{i: ppty(i)} for i in ids}``.
        """
        g = getattr(self, graph)  # primal or dual graph
        g_elem = getattr(g, elem)  # nodes or edges of primal or dual graph

        if elem == 'edges':
            all_ids = list(map(stuple, g_elem()))
            if ids is not None:
                ids = list(map(stuple, ids))
        else:
            all_ids = g_elem()

        if ids is None:
            ids = all_ids

        if elem == 'nodes':
            nd = g.get_node_data
            ppty_dict = {i: default if i not in all_ids or ppty not in nd(i) else nd(i)[ppty] for i in ids}
        else:
            ed = g.get_edge_data
            ppty_dict = {(u, v): default if (u, v) not in all_ids or ppty not in ed(u, v) else ed(u, v)[ppty] for u, v
                         in ids}

        if only_defined:
            is_not_default = not_default_test(default)
            return {k: v for k, v in ppty_dict.items() if is_not_default(v)}
        else:
            return ppty_dict

    def cell_property_dict(self, ppty, ids=None, default=None, only_defined=False):
        """Return a dictionary of cell property.

        Parameters
        ----------
        ppty : str
            Name of the cell property to return.
        ids : list, optional
            A list of cell used to filter keys of returned dictionary.
            By default, all ids are returned.
        default : Any, optional
            Value to return if the cell id is not found, default is ``None``.
            We strongly advise using ``None`` or ``numpy.nan`` with ``only_defined=True``.
        only_defined : bool, optional
            Returns the dictionary only for ids with "valid" values, *i.e.* a non-default value.

        Returns
        -------
        dict
            Cell ids indexed property dictionary, ``{i: cell_property(i)} for i in ids}``.

        Examples
        --------
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> tg.add_cells([2, 3, 4, 5, 6])
        >>> # Add a 'test' cell property with random values:
        >>> tg.add_cell_property('test', {2: 10, 3:20, 4:30, 5:40}, 'A.U.')
        >>> # Get 'test' cell property for all cells:
        >>> tg.cell_property_dict('test')
        {2: 10, 3: 20, 4: 30, 5: 40, 6: None}
        >>> # Get 'test' cell property for a subset of cells:
        >>> tg.cell_property_dict('test', [3, 5, 7])  # note that if the id does not exist you get the default value
        {3: 20, 5: 40, 7: None}
        >>> tg.cell_property_dict('test')  # access property only if defined
        {2: 10, 3: 20, 4: 30, 5: 40}
        """
        if ppty not in self.list_cell_properties():
            log.error(f"Property '{ppty}' is not defined for cells!")
            return {}
        return self._get_property('primal', 'nodes', ppty, ids, default, only_defined)

    def cell_wall_property_dict(self, ppty, ids=None, default=None, only_defined=False):
        """Return a dictionary of cell-wall property.

        Parameters
        ----------
        ppty : str
            Name of the cell-wall property to return.
        ids : list, optional
            A list of cell-wall used to filter keys of returned dictionary.
            By default, all ids are returned.
        default : Any, optional
            Value to return if the cell-wall id is not found, default is ``None``.
            We strongly advise using ``None`` or ``numpy.nan`` with ``only_defined=True``.
        only_defined : bool, optional
            Returns the dictionary only for ids with "valid" values, *i.e.* a non-default value.

        Returns
        -------
        dict
            Cell-wall ids indexed property dictionary, ``{i: cell_wall_property(i)}`` for ``i`` in `ids`.

        Examples
        --------
        >>> from timagetk.graphs.tissue_graph import TissueGraph
        >>> tg = TissueGraph()
        >>> walls = [(3, 2), (3, 4), (3, 5), (3, 6), (3, 7), (2, 4), (4, 5), (5, 6), (6, 7), (2, 7)]
        >>> tg.add_cell_walls(walls)
        >>> # Add a random "area" value to the walls:
        >>> from numpy.random import randint
        >>> wall_area = {w: randint(0, 50) for w in walls}
        >>> tg.add_cell_wall_property("area",wall_area,"voxels")
        >>> # Get 'area' cell-wall property for all cells:
        >>> wall_areas = tg.cell_wall_property_dict('area')
        >>> print(wall_areas)
        {(2, 3): 11, (2, 4): 46, (2, 7): 2, (3, 4): 2, (3, 5): 18, (3, 6): 7, (3, 7): 49, (4, 5): 4, (5, 6): 19, (6, 7): 49}
        >>> # Get 'area' cell-wall property for a subset of cells:
        >>> tg.cell_wall_property_dict('area',[(2, 3), (3, 5)])
        {(2, 3): 11, (3, 5): 18}
        >>> tg.cell_wall_property_dict('area',[(3, 2), (3, 5)])
        {(3, 2): 11, (3, 5): 18}
        >>> tg.cell_wall_property_dict('area',[(25, 12)])
        {(25, 12): None}
        >>> tg.cell_wall_property_dict('area', only_defined=True)  # access property only if defined
        """
        if ppty not in self.list_cell_wall_properties():
            log.error(f"Property '{ppty}' is not defined for cell-walls!")
            return {}
        return self._get_property('primal', 'edges', ppty, ids, default, only_defined)

    def cell_edge_property_dict(self, ppty, ids=None, default=None, only_defined=False):
        """Return a dictionary of cell-edge property.

        Parameters
        ----------
        ppty : str
            Name of the cell-edge property to return.
        ids : list, optional
            A list of cell-edge used to filter keys of returned dictionary.
            By default, all ids are returned.
        default : Any, optional
            Value to return if the cell-edge id is not found, default is ``None``.
            We strongly advise using ``None`` or ``numpy.nan`` with ``only_defined=True``.
        only_defined : bool, optional
            Returns the dictionary only for ids with "valid" values, *i.e.* a non-default value.

        Returns
        -------
        dict
            Cell-edge ids indexed property dictionary, ``{i: cell_edge_property(i)}`` for ``i`` in `ids`.
        """
        if ppty not in self.list_cell_edge_properties():
            log.error(f"Property '{ppty}' is not defined for cell-edges!")
            return {}
        return self._get_property('dual', 'edges', ppty, ids, default, only_defined)

    def cell_vertex_property_dict(self, ppty, ids=None, default=None, only_defined=False):
        """Return a dictionary of cell-vertex property.

        Parameters
        ----------
        ppty : str
            Name of the cell-vertex property to return.
        ids : list, optional
            A list of cell-vertex used to filter keys of returned dictionary.
            By default, all ids are returned.
        default : Any, optional
            Value to return if the cell-vertex id is not found, default is ``None``.
            We strongly advise using ``None`` or ``numpy.nan`` with ``only_defined=True``.
        only_defined : bool, optional
            Returns the dictionary only for ids with "valid" values, *i.e.* a non-default value.

        Returns
        -------
        dict
            Cell-vertex ids indexed property dictionary, ``{i: cell_vertex_property(i)}`` for ``i`` in `ids`.
        """
        if ppty not in self.list_cell_vertex_properties():
            log.error(f"Property '{ppty}' is not defined for cell-vertices!")
            return {}
        return self._get_property('dual', 'nodes', ppty, ids, default, only_defined)

    def topological_distance(self, cell_id, max_depth=None):
        """Return the topological distance of all cells to the selected one.

        Parameters
        ----------
        cell_id : int
            The source cell id to use.
        max_depth : int, optional
            The maximum search distance. No limit by default.

        Returns
        -------
        dict
            Topological distance dictionary

        Examples
        --------
        >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
        >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
        >>> topo_dist = tg.topological_distance(1)  # distance from the background == cell layers!
        >>> topo_dist

        >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
        >>> tg = example_tissue_graph_from_csv('p58', time_point=0)
        >>> # Limit search to L1 layer with ``max_depth=1`` (rest is set to `inf` by default)
        >>> topo_dist = tg.topological_distance(1, max_depth=1)  # distance from the background == cell layers!
        >>> topo_dist
        """
        from timagetk.features.graph import topological_distance
        return topological_distance(self, cell_id, max_depth)

    def list_isolated_nodes(self):
        """List the nodes isolated from the largest graph at given time-point.

        See Also
        --------
        timagetk.features.graph.search_isolated_nodes
        """
        from timagetk.features.graph import search_isolated_nodes
        return search_isolated_nodes(self, background_id=self._background)

    def get_cell_property_unit(self, ppty):
        """Return the unit of the selected property.

        Parameters
        ----------
        ppty : str
            The name of the property.

        Returns
        -------
        str
            The property unit.
        """
        return self._cell_properties[ppty]
