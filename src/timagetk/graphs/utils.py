#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Graph structure dedicated to representation of dynamic multicellular dense tissues."""

import numpy as np
import pandas as pd

from timagetk.bin.logger import get_logger

log = get_logger(__name__)


def cell_feature_df(graph, names, ids=None, default=np.nan):
    """Return a table made of the selected features taken from a (temporal) tissue graph.

    Parameters
    ----------
    graph : timagetk.graphs.TissueGraph or timagetk.graphs.TemporalTissueGraph
        The (temporal) tissue graph to extract features from.
    names : list of str
        List of feature names to use to create the DataFrame.
    ids : list, optional
        If ``None`` or ``"all"``, use all ids available in the graph.
        If a list of integers, restrict the list of cell ids found in the graph by interseting it with this list.
        If a list of string, identity names to find in the graph to restrict the list of cell ids.
        This can also include ``'forward_lineaged'`` to include only cells that have at least one ancestor.
        This can also include ``'layer_*'`` to include only cells belonging to the specified layers.
        For example ``'layer_1_2'`` will include only cells that are in the first two layers.
        Defaults to ``None``.
    default : any, optional
        The default value to use if the cell has no value for this property.
        Defaults to ``np.nan``.

    Returns
    -------
    pandas.DataFrame
        The table with the cell features extracted from the graph.

    Examples
    --------
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
    >>> from timagetk.graphs.utils import cell_feature_df
    >>> ttg = example_temporal_tissue_graph_from_csv()
    >>> cell_df = cell_feature_df(ttg, ['volume', 'area', 'foo'])
    >>> cell_df.describe()
                     area      volume
    count  279.000000  279.000000
    mean   302.544464  371.462422
    std     39.194551   78.471530
    min    153.580704  155.578125
    25%    275.048691  310.945312
    50%    303.463074  372.265625
    75%    332.843124  430.546875
    max    373.218994  522.046875

    """
    from timagetk.graphs.tissue_graph import TissueGraph
    # Get the list of cell ids to use as table index:
    if isinstance(ids, (list, set, tuple, np.array)) and all([isinstance(i, int) for i in ids]):
        # Intersect cell ids from the graph with given list of cell ids:
        cids = graph.cell_ids() & set(ids)
    else:
        # Filter cell ids by keeping those with the given (list of) identity:
        cids = graph.cell_ids(identity=ids)

    # Check for unknown features to the graph:
    unknown_features = set(names) - set(graph.list_cell_properties())
    if unknown_features:
        log.warning(f"Some features could not be found in the graph: {unknown_features}")
    # Keep only known features to the graph:
    names = set(names) & set(graph.list_cell_properties())
    # Get a feature indexed dictionary of cell features:
    features_dict = {feat: graph.cell_property_dict(feat, cids, default=default) for feat in names}
    # Create the table index using the list of cell ids:
    if isinstance(graph, TissueGraph):
        index = pd.Index(list(cids), name="cell_id")
    else:
        index = pd.MultiIndex.from_tuples(list(cids), names=["time-index", "cell_id"])
    return pd.DataFrame(features_dict, columns=list(names), index=index)[sorted(names)]


# -----------------------------------------------------------------------------
# Spatial differentiation functions
# -----------------------------------------------------------------------------
def laplacian(value, neighbor_values):
    """Compute the Laplacian of a `value` based on a list of surrounding values.

    For any property :math:`X_a` of a given vertex :math:`u` surrounded by :math:`K(u)` neighbors,
     the Laplacian `\mathbf{L}_a(u)` is defined as follows:

    .. math::

        \mathbf{L}_a(u) = \left( \frac{1}{K(u)} \sum_{v, v \sim u} X_a(v) \right) - X_a(u),

    where :math:`v \sim u` indicate the vertices :math:`v` neighbor of :math:`u`.

    Parameters
    ----------
    value : float
        The value to consider.
    neighbor_values : list of float
        The list of values surrounding the value to consider.

    Returns
    -------
    float
        The Laplacian for the given values.

    Notes
    -----
    Any ``np.nan`` in the list of `neighbor_values` is not considered a "neighbor" of :math:`u`,
     _i.e._ not counted in :math:`K(u)`.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.graphs.utils import laplacian
    >>> laplacian(1, [0.5, 0.5])
    -0.5
    >>> laplacian(1, [0.5, 0.5, np.nan])
    -0.5

    """
    return np.nansum(neighbor_values) / float(sum(~np.isnan(neighbor_values))) - value


def mean_absolute_deviation(value, neighbor_values):
    """Compute the mean absolute deviation of a `value` based on a list of surrounding values.

    For any property :math:`X_a` of a given vertex :math:`u` surrounded by :math:`K(u)` neighbors,
     the mean absolute deviation `\mathbf{MAD}_a(u)` is defined as follows:

    .. math::

        \mathbf{MAD}_a(u) = \frac{1}{K(u)} \sum_{v, v \sim u} |X_a(u) - X_a(v)|,

    where :math:`v \sim u` indicate the vertices :math:`v` neighbor of :math:`u`.

    Parameters
    ----------
    value : float
        The value to consider.
    neighbor_values : list of float
        The list of values surrounding the value to consider.

    Returns
    -------
    float
        The mean absolute deviation for the given values.

    Notes
    -----
    Any ``np.nan`` in the list of `neighbor_values` is not considered a "neighbor" of :math:`u`,
     _i.e._ not counted in :math:`K(u)`.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.graphs.utils import mean_absolute_deviation
    >>> mean_absolute_deviation(1, [0.5, 0.5])
    0.5
    >>> mean_absolute_deviation(1, [0.5, 0.5, np.nan])
    0.5

    """
    return 1 / float(sum(~np.isnan(neighbor_values))) * np.nansum(np.abs([n_val - value for n_val in neighbor_values]))


# -----------------------------------------------------------------------------
# Temporal differentiation functions
# -----------------------------------------------------------------------------
def temporal_rate_of_change(anc_value, desc_values, interval):
    """Compute the temporal rate of change between descendant(s) and their ancestor.

    For each vertex :math:`u \in V_i` (at time :math:`i`),
    with lineage :math:`L_j(u)=\{v_1,\dots,v_{K_j(u)}\}` at time :math:`j`,
    the relative value :math:`\mathbf{RC}_j(u)` is computed as follows:

    .. math::

       \mathbf{RC}_j(u) = \frac{1}{t_j - t_i} \sum_{v \in L_j(u)} \left( X_a(v) - X_a(u) \right),

    where :math:`X_a(u)` is a property of vertex :math:`u` and for :math:`j > i`,
    and :math:`|X_a(u)|` is either the absolute value or the norm of the property depending on whether :math:`X_a` is
    a scalar, a vector or a tensor.

    Parameters
    ----------
    anc_value : int or iterable
        The ancestor value, can be a scalar, a vector or a tensor.
    desc_values : int or iterable
        The descendant value(s), can be a list of scalar, a vector or a tensor.
    interval : float
        The time interval between the decendants and the ancestors.

    Returns
    -------
    float
        The temporal rate of change.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.graphs.utils import temporal_rate_of_change
    >>> temporal_rate_of_change(100, [50, 50], interval=1)
    -100.0
    >>> temporal_rate_of_change(100, [100, 100, np.nan], interval=1)
    0.0
    >>> temporal_rate_of_change(np.array([2, 2, 2]), np.array([[2, 2, 2], [2, 2, 2]]),interval=1)
    array([0., 0., 0.])
    >>> temporal_rate_of_change(np.array([2, 2, 2]), np.array([[2, 2, 2], [2, 2, 2], [np.nan, np.nan, np.nan]]),interval=1)
    array([0., 0., 0.])
    """
    return 1 / interval * np.nansum([desc_value - anc_value for desc_value in desc_values], axis=0)


def temporal_relative_rate_of_change(anc_value, desc_values, interval):
    """Compute the temporal relative rate of change between descendant(s) and their ancestor.

    For each vertex :math:`u \in V_i` (at time :math:`i`),
    with lineage :math:`L_j(u)=\{v_1,\dots,v_{K_j(u)}\}` at time :math:`j`,
    the relative value :math:`\mathbf{RRC}_j(u)` is computed as follows:

    .. math::

       \mathbf{RRC}_j(u) = \frac{1}{t_j - t_i} \sum_{v \in L_j(u)} \frac{X_a(v) - X_a(u) }{|X_a(u)|},

    where :math:`X_a(u)` is a property of vertex :math:`u` and for :math:`j > i`,
    and :math:`|X_a(u)|` is either the absolute value or the norm of the property depending on whether :math:`X_a` is
    a scalar, a vector or a tensor.

    Parameters
    ----------
    anc_value : int or iterable
        The ancestor value, can be a scalar, a vector or a tensor.
    desc_values : int or iterable
        The descendant value(s), can be a list of scalar, a vector or a tensor.
    interval : float
        The time interval between the decendants and the ancestors.

    Returns
    -------
    float
        The temporal relative rate of change.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.graphs.utils import temporal_relative_rate_of_change
    >>> temporal_relative_rate_of_change(100, [50, 50], interval=1)
    -1.0
    >>> temporal_relative_rate_of_change(100, [100, 100, np.nan], interval=1)
    0.0
    >>> temporal_relative_rate_of_change(np.array([1, 1, 1]), np.array([[2, 2, 2], [2, 2, 2]]),interval=1)
    array([1.15470054, 1.15470054, 1.15470054])
    >>> temporal_relative_rate_of_change(np.array([2, 2, 2]), np.array([[2, 2, 2], [2, 2, 2], [np.nan, np.nan, np.nan]]),interval=1)
    array([0., 0., 0.])
    """
    return 1 / interval * np.nansum(
        [(desc_value - anc_value) / np.linalg.norm(anc_value) for desc_value in desc_values], axis=0)


def temporal_relative_value(anc_value, desc_values, interval):
    """Compute the temporal relative value between descendant(s) and their ancestor.

    For each vertex :math:`u \in V_i` (at time :math:`i`),
    with lineage :math:`L_j(u)=\{v_1,\dots,v_{K_j(u)}\}` at time :math:`j`,
    the relative value :math:`\mathbf{RV}_j(u)` is computed as follows:

    .. math::

       \mathbf{RV}_j(u) = \frac{1}{t_j - t_i} \sum_{v \in L_j(u)} \frac{X_a(v)}{|X_a(u)|},

    where :math:`X_a(u)` is a property of vertex :math:`u` and for :math:`j > i`,
    and :math:`|X_a(u)|` is either the absolute value or the norm of the property depending on whether :math:`X_a` is
    a scalar, a vector or a tensor.

    Parameters
    ----------
    anc_value : int or iterable
        The ancestor value, can be a scalar, a vector or a tensor.
    desc_values : int or iterable
        The descendant value(s), can be a list of scalar, a vector or a tensor.
    interval : float
        The time interval between the decendants and the ancestors.

    Returns
    -------
    float
        The temporal relative value.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.graphs.utils import temporal_relative_value
    >>> temporal_relative_value(100,[50, 50],interval=1)
    1.0
    >>> temporal_relative_value(100,[100, 100, np.nan],interval=1)
    2.0
    >>> temporal_relative_value(np.array([2, 2, 2]),np.array([[2, 2, 2], [2, 2, 2]]),interval=1)
    array([1.15470054, 1.15470054, 1.15470054])
    >>> temporal_relative_value(np.array([2, 2, 2]),np.array([[2, 2, 2], [2, 2, 2], [np.nan, np.nan, np.nan]]),interval=1)
    array([1.15470054, 1.15470054, 1.15470054])
    """
    return 1 / interval * np.nansum(desc_values, axis=0) / np.linalg.norm(anc_value)


def temporal_log_relative_value(anc_value, desc_values, interval):
    """Compute the temporal log-2 relative value between descendant(s) and their ancestor.

    For each vertex :math:`u \in V_i` (at time :math:`i`),
    with lineage :math:`L_j(u)=\{v_1,\dots,v_{K_j(u)}\}` at time :math:`j`,
    the log-2 relative value :math:`\mathbf{LRV}_j(u)` is computed as follows:

    .. math::

       \mathbf{LRV}_j(u) = \frac{1}{t_j - t_i} ln \left( \sum_{v \in L_j(u)} \frac{X_a(v)}{|X_a(u)|} \right),

    where :math:`X_a(u)` is a property of vertex :math:`u` and for :math:`j > i`,
    and :math:`|X_a(u)|` is either the absolute value or the norm of the property depending on whether :math:`X_a` is
    a scalar, a vector or a tensor.
    Note that :math:`ln` is the base-2 logarithm.

    Parameters
    ----------
    anc_value : int or iterable
        The ancestor value, can be a scalar, a vector or a tensor.
    desc_values : int or iterable
        The descendant value(s), can be a list of scalar, a vector or a tensor.
    interval : float
        The time interval between the decendants and the ancestors.

    Returns
    -------
    float
        The log-2 relative value.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.graphs.utils import temporal_log_relative_value
    >>> temporal_log_relative_value(100,[50, 50],interval=1)
    0.0
    >>> temporal_log_relative_value(100,[100, 100, np.nan],interval=1)
    1.0
    >>> temporal_log_relative_value(np.array([2, 2, 2]),np.array([[2, 2, 2], [2, 2, 2]]),interval=1)
    array([0.14384104, 0.14384104, 0.14384104])
    >>> temporal_log_relative_value(np.array([2, 2, 2]),np.array([[2, 2, 2], [2, 2, 2], [np.nan, np.nan, np.nan]]),interval=1)
    array([0.14384104, 0.14384104, 0.14384104])
    """
    return 1 / interval * np.log2(np.nansum(desc_values, axis=0) / np.linalg.norm(anc_value))


def division_rate(n_descendants, interval):
    """Compute the division rate.

    Parameters
    ----------
    n_descendants : int
        The number of descendant for a given cell.
    interval : float
        The time interval between the decendants and their ancestor.

    Returns
    -------
    float
        The division rate, can be ``np.nan`` if ``n_descendants < 1``.

    Notes
    -----
    For each vertex :math:`u \in V_i`, having :math:`K_j(u) \neq 0` descendants, the division rate is defined by:

    .. math::

        \mathbf{DR}_j(u) = \frac{K_j(u)-1}{t_j - t_i}

    for :math:`j > i`.

    Examples
    --------
    >>> from timagetk.graphs.utils import division_rate
    >>> division_rate(2, 1)
    1.0

    """
    if n_descendants < 1:
        return np.nan
    else:
        return (n_descendants - 1) / interval
