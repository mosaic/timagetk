#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Graph structure dedicated to representation of dynamic multicellular dense tissues."""

import copy as cp

import networkx as nx
import numpy as np
from networkx import NetworkXError

from timagetk.bin.logger import get_logger
from timagetk.features.utils import flatten
from timagetk.graphs.utils import division_rate
from timagetk.graphs.utils import mean_absolute_deviation
from timagetk.graphs.utils import temporal_log_relative_value
from timagetk.graphs.utils import temporal_rate_of_change
from timagetk.graphs.utils import temporal_relative_rate_of_change
from timagetk.graphs.utils import temporal_relative_value

log = get_logger(__name__)


def example_temporal_tissue_graph(dataset='sphere', **kwargs):
    """Create a TemporalTissueGraph from a shared dataset.

    Parameters
    ----------
    dataset : {'sphere', 'p58'}
        A valid dataset name.

    Other Parameters
    ----------------
    coordinates : bool, optional
        If ``True`` add the topologocal element coordinates to the graph at construction time.
        See notes for more information.
    features : list, optional
        List of spatial cell features to extract from the segmented tissue.
    wall_features : list, optional
        List of spatial wall features to extract from the segmented tissue.
    extract_dual : bool, optional
        If ``True`` construct the dual graph with cell edges and vertices.
        Else, construct only primal graph, that is the neighborhood graph.

    Returns
    -------
    TemporalTissueGraph
        The temporal tissue graph built from the corresponding shared dataset.

    Examples
    --------
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph
    >>> ttg = example_temporal_tissue_graph(extract_dual=False)
    >>> print(ttg.background)
    [(0, 1), (1, 1), (2, 1)]
    >>> print(ttg.list_cell_properties())
    ['barycenter', 'epidermis', 'epidermis_median', 'layers', 'time-point']
    >>> print(ttg.list_cell_wall_properties())
    ['median']
    >>> # EXAMPLE 2: Get everything you can:
    >>> ttg = example_temporal_tissue_graph(features='all', wall_features='all', temp_features='all')
    >>> print(ttg.list_cell_properties())
    >>> print(ttg.list_cell_wall_properties())
    >>> ttg.cell_wall_property_dict('area')
    >>> # Overview of the features by types, names and time-index:
    >>> {cf: {t: len(tg.cell_property_dict(cf, only_defined=True)) for t, tg in ttg._tissue_graph.items()} for cf in ttg.list_cell_properties()}
    >>> {wf: {t: len(tg.cell_wall_property_dict(wf, only_defined=True)) for t, tg in ttg._tissue_graph.items()} for wf in ttg.list_cell_wall_properties()}
    >>> {ef: {t: len(tg.cell_edge_property_dict(ef, only_defined=True)) for t, tg in ttg._tissue_graph.items()} for ef in ttg.list_cell_edge_properties()}
    >>> {vf: {t: len(tg.cell_vertex_property_dict(vf, only_defined=True)) for t, tg in ttg._tissue_graph.items()} for vf in ttg.list_cell_vertex_properties()}

    """
    from timagetk.io.dataset import shared_data
    from timagetk.tasks.temporal_tissue_graph import temporal_tissue_graph_from_images
    tissues = shared_dataset(dataset, dtype='segmented')
    # Load example lineages:
    lineages = shared_dataset(dataset, dtype='lineage')
    # Get corresponding time-points
    tp = shared_dataset(dataset, dtype='time-points')
    # Creates the TemporalTissueGraph:
    ttg = temporal_tissue_graph_from_images(tissues, lineages, tp, **kwargs)
    return ttg


def example_temporal_tissue_graph_from_csv(dataset='sphere', **kwargs):
    """Create a TemporalTissueGraph from a shared CSV dataset.

    Parameters
    ----------
    dataset : {'sphere', 'p58'}, optional
        A valid dataset name, "sphere" by default.

    Returns
    -------
    TemporalTissueGraph
        The temporal tissue graph built from the corresponding shared CSV dataset.

    Examples
    --------
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
    >>> ttg = example_temporal_tissue_graph_from_csv()
    >>> print(ttg.list_cell_properties())
    >>> print(ttg.list_cell_wall_properties())
    >>> # Overview of the features by types, names and time-index:
    >>> {cf: {t: len(tg.cell_property_dict(cf, only_defined=True)) for t, tg in ttg._tissue_graph.items()} for cf in ttg.list_cell_properties()}
    >>> {wf: {t: len(tg.cell_wall_property_dict(wf, only_defined=True)) for t, tg in ttg._tissue_graph.items()} for wf in ttg.list_cell_wall_properties()}
    >>> {ef: {t: len(tg.cell_edge_property_dict(ef, only_defined=True)) for t, tg in ttg._tissue_graph.items()} for ef in ttg.list_cell_edge_properties()}
    >>> {vf: {t: len(tg.cell_vertex_property_dict(vf, only_defined=True)) for t, tg in ttg._tissue_graph.items()} for vf in ttg.list_cell_vertex_properties()}

    """
    from timagetk.io.dataset import shared_data
    from timagetk.io.graph import temporal_tissue_graph_from_csv
    csv_files = shared_dataset(dataset, dtype='temporal features')
    ttg = temporal_tissue_graph_from_csv(csv_files, **kwargs)
    return ttg


class TemporalTissueGraph(object):
    """A graph structure dedicated to time-series of dense multicellular tissues.

    Attributes
    ----------
    _tissue_graph : dict
        Time indexed dictionary of ``TissueGraph`` objects.
    _temporal_graph : networkx.DiGraph
        Directed graph containing the lineage information.
    _elapsed_time : dict
        Time indexed dictionary of the elapsed time since origin of time-series.
    _time_unit : str
        The unit associated to the elapsed time.
    nb_time_points : int
        Number of time-points defined in the temporal graph.

    Other Parameters
    ----------------
    verbose : bool
        Control the verbosity of the object.

    Examples
    --------
    >>> from timagetk.graphs.temporal_tissue_graph import TemporalTissueGraph
    >>> from timagetk.tasks.tissue_graph import tissue_graph_from_image
    >>> from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image
    >>> from timagetk import TissueImage3D
    >>> # Create synthetic spherical labelled images as TissueImage3D:
    >>> tissue0 = TissueImage3D(example_layered_sphere_labelled_image(extent=30), background=1, not_a_label=0)
    >>> tissue1 = TissueImage3D(example_layered_sphere_labelled_image(extent=60), background=1, not_a_label=0)
    >>> tissue2 = TissueImage3D(example_layered_sphere_labelled_image(extent=90), background=1, not_a_label=0)
    >>> # Lineages are simples as there is no division:
    >>> lineage01 = {i: [i] for i in tissue0.cells.ids()}
    >>> lineage12 = {i: [i] for i in tissue1.cells.ids()}
    >>> # Create TissueGraph instances from TissueImage instances and compute the volumes:
    >>> tg0 = tissue_graph_from_image(tissue0, coordinates=False, extract_dual=False)
    >>> tg0.add_cell_property('volume', tissue0.cells.volume(), unit='µm³')
    >>> tg1 = tissue_graph_from_image(tissue1,coordinates=False,extract_dual=False)
    >>> tg1.add_cell_property('volume', tissue1.cells.volume(), unit='µm³')
    >>> tg2 = tissue_graph_from_image(tissue2,coordinates=False,extract_dual=False)
    >>> tg2.add_cell_property('volume', tissue2.cells.volume(), unit='µm³')
    >>> # Initialize an empty TemporalTissueGraph:
    >>> ttg = TemporalTissueGraph()
    >>> ttg.add_tissue_graph(tg0, elapsed_time=0)
    >>> ttg.add_tissue_graph(tg1, elapsed_time=5)  # add the graph with a +5h interval
    >>> ttg.add_tissue_graph(tg2, elapsed_time=10)  # add the graph with a +5h interval
    >>> missing_anc_node01, missing_des_node01 = ttg.add_lineage(0, lineage01)  # add cell lineage between, t0 & t1
    >>> missing_anc_node12, missing_des_node12 = ttg.add_lineage(1, lineage12)  # add cell lineage between, t1 & t2
    >>> ttg.child(0, 2)  # simple lineage, same id at later time-point
    [(1, 2)]
    >>> ttg.parent(1, 10)  # simple lineage, same id at early time-point
    (0, 10)
    >>> ttg.siblings(1, 2)  # remember there is no division here
    [(1, 2)]
    >>> ttg.cell_property(0, 2, 'volume')
    220.536
    >>> ttg.time_interval(0, 2)
    10.0
    >>> ttg.cell_relative_value(0, 2, 'volume')  # in µm³/h
    1.609598432908913
    >>> ttg.cell_log_relative_value(0, 2, 'volume')  # in µm³/h
    0.6017257801097466
    >>> ttg.cell_rate_of_change(0, 2, 'volume')  # in µm³/h
    310.8672
    >>> ttg.cell_relative_rate_of_change(0, 2, 'volume')  # in µm³/h
    1.409598432908913
    >>> ttg.cell_division_rate(0, 2)  # in division per hour
    0.0

    """

    def __init__(self, **kwargs):
        self._tissue_graph = {}  # {time-index: TissueGraph}
        self._temporal_graph = nx.DiGraph()  # directed graph of cell lineage
        self._vertex_temporal_graph = nx.DiGraph()  # directed graph of cell-vertex lineage
        self._elapsed_time = []  # elapsed time since origin of acquisition
        self._time_unit = None  # str
        self.nb_time_points = 0
        self._background = kwargs.get("background", None)
        self.verbose = kwargs.get("verbose", True)

    def add_tissue_graph(self, tissue, elapsed_time, unit='h'):
        """Add a TissueGraph to the TemporalTissueGraph.

        Parameters
        ----------
        tissue : TissueGraph
            A TissueGraph to add, time-point is automatically defined as the last one + 1.
        elapsed_time : int or float
            Elapsed time since first time point.
        unit : str
            Time unit, "hours" by default.
        """
        # Defines the time-index:
        if self._tissue_graph == {}:
            ti = 0
        else:
            ti = max(self._tissue_graph.keys()) + 1

        self._tissue_graph.update({ti: tissue})  # {time-index: TissueGraph}
        self._elapsed_time.append(elapsed_time)
        self._time_unit = unit
        self.nb_time_points += 1

    def add_lineage(self, ancestor_tp, lineage):
        """Add cell lineages from ancestor time-point to the next time-point.

        Parameters
        ----------
        ancestor_tp : int
            The time-point of the ancestor.
        lineage : dict or ctrl.lineage.Lineage
            The lineage dictionary or object to use.

        Returns
        -------
        list
            The list of ancestor cell ids unknown to the ancestor ``TissueGraph`` but present in the lineage.
        list
            The list of descendant cell ids unknown to the descendant ``TissueGraph`` but present in the lineage.

        Notes
        -----
        Both ancestor and descendant time-points have to exist.
        Both ancestor and descendant cells should exist to add the lineage.

        Raises
        ------
        ValueError
            If the ancestor time-point is not defined.
            If the descendant time-point is not defined.
        """
        descendant_tp = ancestor_tp + 1
        self._check_time_point_exists(ancestor_tp, descendant_tp)

        missing_ancestors = []
        missing_descendants = []
        for ancestor_node, descendant_node in lineage.items():
            # Check existence of ancestor cell in ancestor TissueGraph:
            if ancestor_node not in self._tissue_graph[ancestor_tp].cell_ids():
                missing_ancestors.append(ancestor_node)
                continue  # skip the rest, nothing will be added!
            # Check existence of ancestor cell in ancestor TissueGraph:
            if not all(dn in self._tissue_graph[descendant_tp].cell_ids() for dn in descendant_node):
                missing_descendants.extend(descendant_node)
                continue  # skip the rest, nothing will be added!
            self._temporal_graph.add_edges_from(
                [((ancestor_tp, ancestor_node), (descendant_tp, dn)) for dn in descendant_node])

        if missing_ancestors != []:
            n_miss = len(missing_ancestors)
            log.warning(
                f"Got {n_miss} ancestor nodes missing while adding cell lineage between t{ancestor_tp} and t{descendant_tp}!")
        if missing_descendants != []:
            n_miss = len(missing_descendants)
            log.warning(
                f"Got {n_miss} descendant nodes missing while adding cell lineage between t{ancestor_tp} and t{descendant_tp}!")
        return missing_ancestors, missing_descendants

    def add_vertex_lineage(self, ancestor_tp, lineage):
        """Add cell-vertex lineages from ancestor time-point to the next time-point.

        Parameters
        ----------
        ancestor_tp : int
            The time-point of the ancestor.
        lineage : dict
            The cell-vertex lineage dictionary or object to use.

        Returns
        -------
        list
            The list of ancestor cell-vertex ids unknown to the ancestor ``TissueGraph`` but present in the lineage.
        list
            The list of descendant cell-vertex ids unknown to the descendant ``TissueGraph`` but present in the lineage.

        Notes
        -----
        Both ancestor and descendant time-points have to exist.
        Both ancestor and descendant cell-vertices should exist to add the lineage.

        Raises
        ------
        ValueError
            If the ancestor time-point is not defined.
            If the descendant time-point is not defined.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
        >>> ttg.add_vertex_lineage(0, {(1, 16, 19, 21): (1, 16, 19, 21)})
        >>> ttg.parent_vertex(1, (1, 16, 19, 21))
        """
        descendant_tp = ancestor_tp + 1
        self._check_time_point_exists(ancestor_tp, descendant_tp)

        missing_ancestors = []
        missing_descendants = []
        for ancestor_node, descendant_node in lineage.items():
            # Check existence of ancestor cell-vertex in ancestor TissueGraph:
            if ancestor_node not in self._tissue_graph[ancestor_tp].cell_vertex_ids():
                missing_ancestors.append(ancestor_node)
                continue  # skip the rest, nothing will be added!
            # Check existence of descendant cell-vertex in descendant TissueGraph:
            if descendant_node not in self._tissue_graph[descendant_tp].cell_vertex_ids():
                missing_descendants.append(descendant_node)
                continue  # skip the rest, nothing will be added!
            self._vertex_temporal_graph.add_edge((ancestor_tp, ancestor_node), (descendant_tp, descendant_node))

        if missing_ancestors != []:
            n_miss = len(missing_ancestors)
            log.warning(
                f"Got {n_miss} ancestor nodes missing while adding cell-vertex lineage between t{ancestor_tp} and t{descendant_tp}!")
        if missing_descendants != []:
            n_miss = len(missing_descendants)
            log.warning(
                f"Got {n_miss} descendant nodes missing while adding cell-vertex lineage between t{ancestor_tp} and t{descendant_tp}!")
        return missing_ancestors, missing_descendants

    def _check_time_point_exists(self, ancestor_tp, descendant_tp=None):
        """Hidden method to check whether a given time-point is defined in the temporal graph or not."""
        try:
            assert ancestor_tp in self._tissue_graph
        except AssertionError:
            raise ValueError(f"Could not find ancestor time-point {ancestor_tp} in the TemporalTissueGraph!")
        if descendant_tp is not None:
            try:
                assert descendant_tp in self._tissue_graph
            except AssertionError:
                raise ValueError(f"Could not find descendant time-point {descendant_tp} in the TemporalTissueGraph!")

    def _check_ppty_scalar(self, tp, cell_id, ppty):
        """Hidden method to check whether a cell property value is a scalar or not."""
        values = self._tissue_graph[tp].cell_property(cell_id, ppty)
        try:
            assert isinstance(values, (float, int))
        except AssertionError:
            raise TypeError(f"Property '{ppty}' is not a scalar, but a {type(values)}!")

    def _filter_time_points(self, time_points=None, rank=0):
        """Filter given time-points with those defined in the object.

        Parameters
        ----------
        time_points : int or list, optional
            If ``None`` (default) compute the temporal differentiation function for all time-point.
            Else, should be a list of valid time-points to select for computation.
        rank : int, optional
            A temporal rank to use to restrict returned time-points.
            Defaults to ``0`` to consider all time-points.

        Returns
        -------
        list of int
            The (filtered) list of time-points.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg._filter_time_points()
        [0, 1, 2]
        >>> ttg._filter_time_points(2)
        [2]
        >>> ttg._filter_time_points(rank=1)
        [0, 1]
        """
        # List all time-points:
        all_time_points = self.list_time_points()
        # Filter all time-point for temporal rank:
        n_tp = len(all_time_points)
        tp_idx = slice(0, n_tp - rank, 1)
        all_time_points = set(all_time_points[tp_idx])

        if time_points is None:
            time_points = list(all_time_points)
        elif isinstance(time_points, int):
            time_points = list({time_points} & all_time_points)
        else:
            time_points = list(set(time_points) & all_time_points)

        return time_points

    def list_time_points(self) -> list:
        """Get the list of all time-points."""
        return list(range(0, len(self._elapsed_time)))

    def time_interval(self, ancestor_tp, descendant_tp):
        """Get the time interval between two time points.

        Parameters
        ----------
        ancestor_tp : int
            Index of the ancestor time-point.
        descendant_tp : int
            Index of the descendant time-point.

        Returns
        -------
        float or None
            The time interval or delta, between the two time points, if it exists.
            ``None`` if ``ancestor_tp`` or ``descendant_tp`` do not exist.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> print(ttg.time_interval(0, 1))
        5.0
        >>> print(ttg.time_interval(0, 5))
        None
        """
        try:
            self._check_time_point_exists(ancestor_tp, descendant_tp)
        except ValueError:
            return None
        else:
            return float(self._elapsed_time[descendant_tp] - self._elapsed_time[ancestor_tp])

    @property
    def background(self):
        """Get the node id acting as background, can be ``None``.

        Returns
        -------
        int or None
            Graph node id defining the background.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.background
        [(0, 1), (1, 1), (2, 1)]
        """
        return cp.copy([(tp, tissue_graph._background) for tp, tissue_graph in self._tissue_graph.items()])

    def child(self, tp, cell_id):
        """Return the child or children of the given cell at given time-point.

        Parameters
        ----------
        tp : int
            Time-point of the cell.
        cell_id : int
            ID of the cell.

        Returns
        -------
        list
            The list of child or children IDs.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.child(0, 3)
        [(1, 3), (1, 74)]
        """
        try:
            child = list(self._temporal_graph.neighbors((tp, cell_id)))
        except NetworkXError:
            child = []
        return child

    def parent(self, tp, cell_id):
        """Return the parent of the given cell at given time-point.

        Parameters
        ----------
        tp : int
            Time-point of the cell.
        cell_id : int
            ID of the cell.

        Returns
        -------
        tuple
            The time-point and cell ID of the parent cell.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.parent(1, 3)
        (0, 3)
        """
        try:
            return list(self._temporal_graph.predecessors((tp, cell_id)))[0]
        except IndexError:
            log.warning(f"No predecessor of cell-id {cell_id} at time {tp} could be found!")
            return []
        except NetworkXError as e:
            log.warning(e)
            return []

    def siblings(self, tp, cell_id):
        """Return the siblings of the given cell at given time-point.

        Parameters
        ----------
        tp : int
            Time-point of the cell
        cell_id : int
            ID of the cell

        Returns
        -------
        list of tuple
            The list of siblings, including the selected cell.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.siblings(1, 3)
        [(1, 3), (1, 74)]
        """
        parent_tp, parent = self.parent(tp, cell_id)
        return self.child(parent_tp, parent)

    def ancestor(self, tp, cell_id, rank="max"):
        """Return the ranked dictionary of ancestors genealogy tree.

        Parameters
        ----------
        tp : int
            Time-point of the cell
        cell_id : int
            ID of the cell
        rank : int or "max"
            If ``"max"`` (default), return the whole genealogy starting from given `tp`.
            Else, an integer specifying the depth of the genealogy to return.
            Note that ``rank=0`` is the cell itself.
            Note that using ``ancestor(tp, cell_id, rank=1)`` is similar to ``parent(tp, cell_id)``.

        Returns
        -------
        dict
            The rank-indexed dictionary of the cell genealogy

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.ancestor(2, 16)
        {-2: [(0, 16)], -1: [(1, 16)], 0: [(2, 16)]}
        >>> ttg.ancestor(2, 4)
        {-2: [(0, 4)], -1: [(1, 4)], 0: [(2, 4)]}
        >>> ttg.ancestor(2, 128)
        {-2: [(0, 4)], -1: [(1, 4)], 0: [(2, 128)]}
        """
        max_rank = tp  # max depth of the ancestors' genealogy starting from given `tp`
        if rank == "max":
            rank = max_rank
        else:
            rank = min(rank, max_rank)

        ancestors_at_rank = {0: [(tp, cell_id)]}
        for r in range(rank):
            if len(ancestors_at_rank[-r][0]) != 0:
                ancestors_at_rank[-(r + 1)] = [self.parent(*ancestors_at_rank[-r][0])]
            else:
                ancestors_at_rank.pop(-r)
                break
        return dict(sorted(ancestors_at_rank.items()))

    def descendant(self, tp, cell_id, rank="max"):
        """Return the ranked dictionary of descendants genealogy tree.

        Parameters
        ----------
        tp : int
            Time-point of the cell
        cell_id : int
            ID of the cell
        rank : int or "max"
            If ``"max"`` (default), return the descendants starting from given `tp`.
            Else, an integer specifying the depth of the genealogy to return.
            Note that ``rank=0`` is the cell itself.
            Note that using ``descendant(tp, cell_id, rank=1)`` is similar to ``child(tp, cell_id)``.

        Returns
        -------
        dict
            The rank-indexed dictionary of the cell descendants

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.descendant(0, 16)
        {0: [(0, 16)], 1: [(1, 16)], 2: [(2, 16)]}
        >>> ttg.descendant(0, 4)
        {0: [(0, 4)], 1: [(1, 4)], 2: [(2, 4), (2, 128)]}
        """
        max_rank = self.nb_time_points - tp - 1  # max depth of the genealogy starting from given `tp`
        if rank == "max":
            rank = max_rank
        else:
            rank = min(rank, max_rank)

        desc_at_rank = {0: [(tp, cell_id)]}
        for r in range(rank):
            desc_at_rank[r + 1] = []
            for t, cid in desc_at_rank[r]:
                desc_at_rank[r + 1].extend(self.child(t, cid))

        return desc_at_rank

    def cell_lineage(self, tp, cell_id, rank_range=None):
        """Get the lineage of the cell within an ancestor and descendant range of ranks.

        Parameters
        ----------
        tp : int
            Time-point of the cell
        cell_id : int
            ID of the cell
        rank_range : len-2 list, optional
            List of values to use as ancestor and descendant ranks. Can be an integer or ``"max"``.

        Returns
        -------
        dict
            The rank-indexed dictionary of the cell genealogy

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.descendant(0, 16)
        {0: [(0, 16)], 1: [(1, 16)], 2: [(2, 16)]}
        >>> ttg.cell_lineage(1,16)
        {-1: [(0, 16)], 0: [(1, 16)], 1: [(2, 16)]}
        >>> ttg.cell_lineage(0,16)
        {0: [(0, 16)], 1: [(1, 16)], 2: [(2, 16)]}
        """
        if rank_range is None:
            ancestor_rank, descendant_rank = "max", "max"
        else:
            ancestor_rank, descendant_rank = rank_range
        lineage = self.ancestor(tp, cell_id, ancestor_rank)
        lineage.update(self.descendant(tp, cell_id, descendant_rank))
        return lineage

    def lineage(self, ancestor_tp, descendant_tp, time_indexed=False):
        """Get the lineage of all cells within an ancestor and descendant time-index.

        Parameters
        ----------
        ancestor_tp : int
            Index of the ancestor time-point.
        descendant_tp : int
            Index of the descendant time-point.
        time_indexed : bool, optional
            Wheter to add the time-index to the lineage dictionary.
            Defaults to ``False``.

        Returns
        -------
        dict
            The lineage dictionary.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> # Example #1 - Non time-indexed t0-t1 (rank-1) lineage dictionary
        >>> lineage = ttg.lineage(0, 1)
        >>> print(lineage[30])
        [30, 81]
        >>> # Example #2 - Time-indexed t0-t1 (rank-1) lineage dictionary
        >>> t_lineage = ttg.lineage(0, 1, time_indexed=True)
        >>> print(t_lineage[(0, 30)])
        [(1, 30), (1, 81)]
        >>> # Example #3 - Non time-indexed t0-t2 (rank-2) lineage dictionary
        >>> lineage = ttg.lineage(0, 2)
        >>> print(lineage[30])
        [30, 81]
        """
        rank = descendant_tp - ancestor_tp
        cell_ids = self.cell_ids(ancestor_tp)
        if time_indexed:
            lineage = {(t, cid): self.descendant(t, cid, rank)[rank] for t, cid in cell_ids}
        else:
            lineage = {cid: [d for _, d in self.descendant(t, cid, rank)[rank]] for t, cid in cell_ids}
        return {cid: lin for cid, lin in lineage.items() if len(lin) != 0}

    def cell_genealogy(self, tp, cell_id, rank_range=None):
        """Get the whole genealogy of the cell within an ancestor and descendant range of ranks.

        Parameters
        ----------
        tp : int
            Time-point of the cell
        cell_id : int
            ID of the cell
        rank_range : len-2 list, optional
            List of values to use as ancestor and descendant ranks. Can be an integer or ``"max"``.

        Returns
        -------
        dict
            The rank-indexed dictionary of the cell genealogy

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.descendant(0, 57)
        {0: [(0, 57)], 1: [(1, 57), (1, 89)], 2: [(2, 57), (2, 89)]}
        >>> ttg.cell_genealogy(1,57)
        {-1: [(0, 57)], 0: [(1, 57), (1, 89)], 1: [(2, 57), (2, 89)]}
        >>> ttg.cell_lineage(1,57)
        {-1: [(0, 57)], 0: [(1, 57)], 1: [(2, 57)]}
        """
        if rank_range is None:
            ancestor_rank, descendant_rank = "max", "max"
        else:
            ancestor_rank, descendant_rank = rank_range

        if ancestor_rank == "max":
            ancestor_rank = tp
        if descendant_rank == "max":
            descendant_rank = self.nb_time_points - tp - 1

        ancestors = self.ancestor(tp, cell_id, ancestor_rank)
        first_ancestor_rank = min(ancestors.keys())
        while len(ancestors[first_ancestor_rank]) == 0:
            first_ancestor_rank += 1

        desc = self.descendant(*ancestors[first_ancestor_rank][0], descendant_rank - first_ancestor_rank)
        desc = {k + first_ancestor_rank: v for k, v in desc.items()}

        return desc

    def has_ancestor(self, tp, cell_id, rank=1):
        """Test if the given cell, from the given time-point, has an ancestor at given `rank`.

        Parameters
        ----------
        tp : int
            Time-point of the cell
        cell_id : int
            ID of the cell
        rank : int
            An integer specifying the depth of the cell lineage to test.
            Note that `rank=0` is the cell itself, so it should be strictly greater than ``1``.

        Returns
        -------
        bool
            ``True`` if the cell has an ancestor at given `rank`, else ``False``.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.ancestor(2, 16)
        {0: [(2, 16)], -1: [(1, 16)], -2: [(0, 16)]}
        >>> ttg.has_ancestor(2, 16, rank=2)
        True
        >>> ttg.has_ancestor(2, 16, rank=3)
        False
        """
        try:
            assert rank >= 1
        except AssertionError:
            log.error(f"A rank value ({rank}) strictly inferior to 1 is not valid!")
            return False
        try:
            assert rank <= tp
        except AssertionError:
            log.error(f"A rank value ({rank}) greater than prior time-points ({tp}) is not valid!")
            return False
        if rank == 1:
            return len(self.parent(tp, cell_id)) >= 1
        else:
            return len(self.ancestor(tp, cell_id, rank=rank)[-rank]) >= 1

    def has_descendant(self, tp, cell_id, rank=1):
        """Test if the given cell, from the given time-point, has at least a descendant at given `rank`.

        Parameters
        ----------
        tp : int
            Time-point of the cell
        cell_id : int
            ID of the cell
        rank : int
            An integer specifying the depth of the cell lineage to test.
            Note that ``rank=0`` is the cell itself, so it should be strictly greater than ``1``.

        Returns
        -------
        bool
            ``True`` if the cell has at least a descendant at given `rank`, else ``False``.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.descendant(0, 16)
        {0: [(0, 16)], 1: [(1, 16)], 2: [(2, 16)]}
        >>> ttg.has_descendant(0,16)
        True
        >>> ttg.has_descendant(0,16,rank=2)
        True
        >>> ttg.has_descendant(1,16,rank=2)
        False
        """
        try:
            assert rank >= 1
        except AssertionError:
            log.error(f"A rank value ({rank}) strictly inferior to 1 is not valid!")
            return False
        try:
            remain_tp = self.nb_time_points - tp - 1
            assert rank <= remain_tp
        except AssertionError:
            log.error(f"A rank value ({rank}) greater than remaining time-points ({remain_tp}) is not valid!")
            return False
        if rank == 1:
            return len(self.child(tp, cell_id)) >= 1
        else:
            return len(self.descendant(tp, cell_id, rank=rank)[rank]) >= 1

    def child_vertex(self, tp, cv_id):
        """Return the child or children of the given cell-vertex at given time-point.

        Parameters
        ----------
        tp : int
            Time-point of the cell-vertex.
        cv_id : tuple
            ID of the cell-vertex, as a 4-tuple of cell ids.

        Returns
        -------
        list
            The child cell-vertex id.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> from timagetk.graphs.temporal_tissue_graph import compute_vertex_lineage
        >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
        >>> ttg = compute_vertex_lineage(ttg)
        >>> ttg.cell_ids(tp=0, identity='layer_1')
        >>> ttg._tissue_graph[0].cell_vertex_ids(16)
        >>> ttg.child_vertex(0, (1, 16, 19, 21))
        (1, (1, 16, 19, 21))
        """
        try:
            child = tuple(list(self._vertex_temporal_graph.neighbors((tp, cv_id)))[0])
        except NetworkXError:
            child = ()
        except IndexError:
            child = ()
        return child

    def parent_vertex(self, tp, cv_id):
        """Return the parent of the given cell-vertex at given time-point.

        Parameters
        ----------
        tp : int
            Time-point of the cell-vertex.
        cv_id : tuple
            ID of the cell-vertex, as a 4-tuple of cell ids.

        Returns
        -------
        tuple
            The time-point and cell-vertex ID of the parent cell-vertex.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> from timagetk.graphs.temporal_tissue_graph import compute_vertex_lineage
        >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
        >>> ttg = compute_vertex_lineage(ttg)
        >>> ttg._tissue_graph[1].cell_vertex_ids(16)
        >>> ttg.parent_vertex(1, (1, 16, 19, 21))
        (0, (1, 16, 19, 21))
        """
        try:
            return tuple(list(self._vertex_temporal_graph.predecessors((tp, cv_id)))[0])
        except IndexError:
            log.warning(f"No predecessor of cell-id {cv_id} at time {tp} could be found!")
            return ()
        except NetworkXError as e:
            log.warning(e)
            return ()

    def ancestor_vertex(self, tp, cv_id, rank="max"):
        """Return the ranked dictionary of ancestors genealogy tree.

        Parameters
        ----------
        tp : int
            Time-point of the cell-vertex.
        cv_id : tuple
            ID of the cell-vertex, as a 4-tuple of cell ids.
        rank : int or "max"
            If ``"max"`` (default), return the whole genealogy starting from given `tp`.
            Else, an integer specifying the depth of the genealogy to return.
            Note that ``rank=0`` is the cell-vertex itself.
            Note that using ``ancestor_vertex(tp, cv_id, rank=1)`` is similar to ``parent_vertex(tp, cv_id)``.

        Returns
        -------
        dict
            The rank-indexed dictionary of the cell-vertex genealogy.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> from timagetk.graphs.temporal_tissue_graph import compute_vertex_lineage
        >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
        >>> ttg = compute_vertex_lineage(ttg)
        >>> ttg.ancestor_vertex(1, (1, 16, 19, 21))
        {-1: (0, (1, 16, 19, 21)), 0: (1, (1, 16, 19, 21))}
        >>> ttg.ancestor_vertex(2, (1, 16, 19, 21))
        {-2: (0, (1, 16, 19, 21)), -1: (1, (1, 16, 19, 21)), 0: (2, (1, 16, 19, 21))}
        """
        max_rank = tp  # max depth of the ancestors' genealogy starting from given `tp`
        if rank == "max":
            rank = max_rank
        else:
            rank = min(rank, max_rank)

        ancestors_at_rank = {0: (tp, cv_id)}
        for r in range(rank):
            if len(ancestors_at_rank[-r]) != 0:
                ancestors_at_rank[-(r + 1)] = self.parent_vertex(*ancestors_at_rank[-r])
            else:
                ancestors_at_rank.pop(-r)
                break
        return dict(sorted(ancestors_at_rank.items()))

    def descendant_vertex(self, tp, cv_id, rank="max"):
        """Return the ranked dictionary of descendants genealogy tree.

        Parameters
        ----------
        tp : int
            Time-point of the cell-vertex.
        cv_id : tuple
            ID of the cell-vertex, as a 4-tuple of cell ids.
        rank : int or "max"
            If ``"max"`` (default), return the descendants starting from given `tp`.
            Else, an integer specifying the depth of the genealogy to return.
            Note that ``rank=0`` is the cell-vertex itself.
            Note that using ``descendant_vertex(tp, cell_id, rank=1)`` is similar to ``child_vertex(tp, cell_id)``.

        Returns
        -------
        dict
            The rank-indexed dictionary of the cell-vertex descendants.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> from timagetk.graphs.temporal_tissue_graph import compute_vertex_lineage
        >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
        >>> ttg = compute_vertex_lineage(ttg)
        >>> ttg.descendant_vertex(0, (1, 16, 19, 21), rank=1)
        {0: (0, (1, 16, 19, 21)), 1: (1, (1, 16, 19, 21))}
        >>> ttg.descendant_vertex(0, (1, 16, 19, 21))
        {0: (0, (1, 16, 19, 21)), 1: (1, (1, 16, 19, 21)), 2: (2, (1, 16, 19, 21))}
        """
        max_rank = self.nb_time_points - tp - 1  # max depth of the genealogy starting from given `tp`
        if rank == "max":
            rank = max_rank
        else:
            rank = min(rank, max_rank)

        desc_at_rank = {0: (tp, cv_id)}
        for r in range(rank):
            try:
                desc_at_rank[r + 1] = self.child_vertex(*desc_at_rank[r])
            except:
                desc_at_rank[r + 1] = ()

        return desc_at_rank

    def has_ancestor_vertex(self, tp, cv_id, rank=1):
        """Test if the given cell-vertex, from the given time-point, has an ancestor at given `rank`.

        Parameters
        ----------
        tp : int
            Time-point of the cell-vertex.
        cv_id : int
            ID of the cell-vertex, as a 4-tuple of cell ids.
        rank : int
            An integer specifying the depth of the cell-vertex lineage to test.
            Note that `rank=0` is the cell itself, so it should be strictly greater than ``1``.

        Returns
        -------
        bool
            ``True`` if the cell-vertex has an ancestor at given `rank`, else ``False``.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> from timagetk.graphs.temporal_tissue_graph import compute_vertex_lineage
        >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
        >>> ttg = compute_vertex_lineage(ttg)
        >>> ttg.ancestor_vertex(2, (1, 16, 19, 21))
        {-2: (0, (1, 16, 19, 21)), -1: (1, (1, 16, 19, 21)), 0: (2, (1, 16, 19, 21))}
        >>> ttg.has_ancestor_vertex(0, (1, 16, 19, 21))
        False
        >>> ttg.has_ancestor_vertex(2, (1, 16, 19, 21))
        True
        >>> ttg.has_ancestor_vertex(2, (1, 16, 19, 21), rank=2)
        True
        >>> ttg.has_ancestor_vertex(1, (1, 16, 19, 21), rank=2)
        False
        """
        try:
            assert rank >= 1
        except AssertionError:
            log.error(f"A rank value ({rank}) strictly inferior to 1 is not valid!")
            return False
        try:
            assert rank <= tp
        except AssertionError:
            log.error(f"A rank value ({rank}) greater than prior time-points ({tp}) is not valid!")
            return False
        if rank == 1:
            return self.parent_vertex(tp, cv_id) != ()
        else:
            return self.ancestor_vertex(tp, cv_id, rank=rank)[-rank] != ()

    def has_descendant_vertex(self, tp, cv_id, rank=1):
        """Test if the given cell-vertex, from the given time-point, has at least a descendant at given `rank`.

        Parameters
        ----------
        tp : int
            Time-point of the cell-vertex.
        cv_id : tuple
            ID of the cell-vertex, as a 4-tuple of cell ids.
        rank : int
            An integer specifying the depth of the cell-vertex lineage to test.
            Note that ``rank=0`` is the cell-vertex itself, so it should be strictly greater than ``1``.

        Returns
        -------
        bool
            ``True`` if the cell-vertex has at least a descendant at given `rank`, else ``False``.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> from timagetk.graphs.temporal_tissue_graph import compute_vertex_lineage
        >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
        >>> ttg = compute_vertex_lineage(ttg)
        >>> ttg.descendant_vertex(0, (1, 16, 19, 21))
        {0: (0, (1, 16, 19, 21)), 1: (1, (1, 16, 19, 21)), 2: (2, (1, 16, 19, 21))}
        >>> ttg.has_descendant_vertex(2,(1, 16, 19, 21))
        False
        >>> ttg.has_descendant_vertex(0,(1, 16, 19, 21))
        True
        >>> ttg.has_descendant_vertex(0,(1, 16, 19, 21),rank=2)
        True
        >>> ttg.has_descendant_vertex(1,(1, 16, 19, 21),rank=2)
        False
        """
        try:
            assert rank >= 1
        except AssertionError:
            log.error(f"A rank value ({rank}) strictly inferior to 1 is not valid!")
            return False
        try:
            remain_tp = self.nb_time_points - tp - 1
            assert rank <= remain_tp
        except AssertionError:
            log.error(f"A rank value ({rank}) greater than remaining time-points ({remain_tp}) is not valid!")
            return False
        if rank == 1:
            return self.child_vertex(tp, cv_id) != ()
        else:
            return self.descendant_vertex(tp, cv_id, rank=rank)[rank] != ()

    def cell_ids(self, tp=None, identity=None):
        """List cell ids of the temporal tissue graph.

        Parameters
        ----------
        tp : int or list of int
            Selected time-point(s), if ``None`` (default) return for all defined time-points of the graph.
        identity : list, optional
            Identity names to find in the graph to restrict the list of cell ids.
            This can also include 'forward_lineaged' to include only cells that have at least one ancestor.
            This can also include 'layer_*' to include only cells belonging to the specified layers.
            For example 'layer_1_2' will include only cells that are in the first two layers.

        Returns
        -------
        set
            A set of time-indexed cell ids ``tcid = (tp, cid)``, where ``cid`` is a unique integer.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> t0_cids = ttg.cell_ids(0)  # Get cell ids at time 0
        >>> print(len(t0_cids))
        63
        >>> t_cids = ttg.cell_ids()  # Get cell ids for all time-points
        >>> print(len(t_cids))
        279
        >>> t_cids = ttg.cell_ids(identity="forward_lineaged")  # Get lineaged cell ids for all time-points
        >>> print(len(t_cids))
        151
        >>> t_cids = ttg.cell_ids(identity="epidermis")  # Get epidermal cell ids for all time-points
        >>> print(len(t_cids))
        217
        """
        if tp is None:
            tp = range(self.nb_time_points)
            cids = {(t, cid) for t in tp for cid in self._tissue_graph[t].cell_ids()}
        elif isinstance(tp, int):
            self._check_time_point_exists(tp)
            cids = {(tp, cid) for cid in self._tissue_graph[tp].cell_ids()}
            tp = [tp]
        elif isinstance(tp, (list, tuple)):
            [self._check_time_point_exists(t) for t in tp]
            cids = {(t, cid) for t in tp for cid in self._tissue_graph[t].cell_ids()}
        else:
            raise ValueError(f"Could not make sense of parameter `tp`: {tp}")

        log.debug(f"Selected {len(cids)} cell-ids from {len(tp)} time-points.")

        if identity is not None:
            if isinstance(identity, str):
                identity = [identity]
            # Restrict to given list of identities:
            for iden in identity:
                if iden == 'forward_lineaged':
                    ids = set(self.lineaged_cell_ids(forward=True, backward=False))
                    log.debug(f"Selected {len(cids)} cell ids that have at least one descendant!")
                elif iden not in self.list_identities() and not iden.startswith('layer'):
                    log.error(f"Could not find the cell identity '{iden}' in the graph!")
                    continue
                else:
                    ids = set([])
                    for t in tp:
                        # Gather identities from each time-point
                        ids |= {(t, i) for i in self._tissue_graph[t]._identity_filtering(iden)}
                log.debug(f"Intersecting {len(cids)} cell-ids with {len(ids)} cell-ids with {iden} identity.")
                cids &= ids

        return cids

    def cell_wall_ids(self, tp=None, cell_id=None, tcid_indexed=False):
        """List cell-wall ids of the temporal tissue graph.

        Parameters
        ----------
        tp : int or list of int
            Selected time-point(s), if ``None`` (default) return for all defined time-points of the graph.
        cell_id : int, optional
            The cell id for which to return the list of cell-wall ids.
            If ``None``, returns all eges of the primal graph.
            Only work if `tp` is an integer.
        tcid_indexed : bool, optional
            If ``True``, defaults to ``False``, returns index returned dictionary with tuples of time-indexed cell ids,
            Else by tuples of time-indexed wall ids.

        Returns
        -------
        list
            By default, a set of time-indexed wall ids ``twid = (tp, wid)``, where ``wid=(cid_i, cid_j)``.
            If ``tcid_indexed`` is ``True``, a list of 2-tuples as time-indexed cell ids ``twid = (tcid_i, tcid_j) = ((tp, cid_i), (tp, cid_j))``.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> wids = ttg.cell_wall_ids(0, cell_id=16)  # Get cell-wall ids at time 0 for cell 16
        >>> print(len(wids))
        11
        >>> print(wids[0])  # print the first one, note the `(t, (cid1, cid2))` format
        (0, (16, 20))
        >>> t0_wids = ttg.cell_wall_ids(0)  # Get cell-wall ids at time 0
        >>> print(len(t0_wids))
        356
        >>> print(t0_wids[0])  # example of a twid as time-indexed wall id
        (0, (20, 17))
        >>> t0_wids_i = ttg.cell_wall_ids(0, tcid_indexed=True)  # Get cell-wall ids at time 0, as time-indexed cell ids
        >>> print(t0_wids_i[0])  # example of a twid as time-indexed cell id
        ((0, 20), (0, 17))
        >>> all_wids = ttg.cell_wall_ids()  # Get all known cell-wall ids
        >>> len(all_wids)
        1638
        """
        if tp is None:
            tp = list(range(self.nb_time_points))
        else:
            self._check_time_point_exists(tp)

        if isinstance(tp, int):
            if tcid_indexed:
                return [tuple(zip([tp] * 2, cwid)) for cwid in self._tissue_graph[tp].cell_wall_ids(cell_id)]
            else:
                return [(tp, cwid) for cwid in self._tissue_graph[tp].cell_wall_ids(cell_id)]
        elif isinstance(tp, (list, tuple)):
            if tcid_indexed:
                return [tuple(zip([t] * 2, cwid)) for t in tp for cwid in self._tissue_graph[t].cell_wall_ids()]
            else:
                return [(t, cwid) for t in tp for cwid in self._tissue_graph[t].cell_wall_ids()]
        else:
            raise ValueError(f"Could not make sense of parameter `tp`: {tp}")

    def cell_edge_ids(self, tp=None, cell_id=None, wall_id=None, tcid_indexed=False):
        """List cell-edge ids of the temporal tissue graph.

        Parameters
        ----------
        tp : int or list of int
            Selected time-point(s), if ``None`` (default) return for all defined time-points of the graph.
        cell_id : int, optional
            The cell id for which to return the list of cell-eges ids.
            If ``None``, returns all eges of the dual graph.
            Only work if `tp` is an integer.
        wall_id : (int, int), optional
            The wall id for which to return the list of cell-vertex ids.
            If ``None``, returns all nodes of the dual graph.
            If defined, override the `cell_id` parameter.
            Only work if `tp` is an integer.
        tcid_indexed : bool, optional
            If ``True``, defaults to ``False``, returns index returned dictionary with tuples of time-indexed cell ids,
            Else by tuples of time-indexed edge ids.

        Returns
        -------
        list
            By default, a list of time-indexed edge ids ``teid = (tp, eid)``, where ``eid=(vid_1, vid_2)`` and ``vid1=(cid_i1, cid_j1, cid_k1, cid_l1)``.
            If ``tcid_indexed`` is ``True``, a list of 2-tuples as time-indexed cell ids ``teid = ((tcid_i1, tcid_j1, tcid_k1, tcid_l1), (tcid_i2, tcid_j2, tcid_k2, tcid_l2))``.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> eids = ttg.cell_edge_ids(0, cell_id=16)  # Get cell-edge ids at time 0 for cell 16
        >>> print(len(eids))
        19
        >>> print(eids[0])  # print the first one, note the `(t, (vid1, vid2))` format
        (0, ((8, 10, 13, 16), (8, 10, 16, 21)))
        >>> ep_eids = ttg.cell_edge_ids(0, wall_id=(1, 16))  # Get cell-vertex ids at time 0 for epidermal cell-wall `(1, 16)`
        >>> print(len(ep_eids))
        5
        >>> t0_eids = ttg.cell_edge_ids(0)  # Get cell-edge ids at time 0
        >>> print(len(t0_eids))
        567
        >>> print(t0_eids[0])  # example of a teid as time-indexed wall id
        (0, ((1, 32, 47, 55), (14, 32, 47, 55)))
        >>> t0_eids_i = ttg.cell_edge_ids(0, tcid_indexed=True)  # Get cell-edge ids at time 0, as time-indexed cell ids
        >>> print(t0_eids_i[0])  # example of a teid as time-indexed cell id
        (((0, 1), (0, 32), (0, 47), (0, 55)), ((0, 14), (0, 32), (0, 47), (0, 55)))
        """
        if tp is None:
            tp = list(range(self.nb_time_points))
        else:
            self._check_time_point_exists(tp)

        if isinstance(tp, int):
            if tcid_indexed:
                return [(tuple(zip([tp] * 4, vid_i)), tuple(zip([tp] * 4, vid_j))) for vid_i, vid_j in
                        self._tissue_graph[tp].cell_edge_ids(cell_id, wall_id)]
            else:
                return [(tp, ceid) for ceid in self._tissue_graph[tp].cell_edge_ids(cell_id, wall_id)]
        elif isinstance(tp, (list, tuple)):
            if tcid_indexed:
                return [(tuple(zip([t] * 4, vid_i)), tuple(zip([t] * 4, vid_j))) for t in tp for vid_i, vid_j in
                        self._tissue_graph[t].cell_edge_ids()]
            else:
                return [(t, ceid) for t in tp for ceid in self._tissue_graph[t].cell_edge_ids()]
        else:
            raise ValueError(f"Could not make sense of parameter `tp`: {tp}")

    def cell_vertex_ids(self, tp=None, cell_id=None, wall_id=None, tcid_indexed=False):
        """List cell-vertex ids of the temporal tissue graph.

        Parameters
        ----------
        tp : int or list of int, optional
            Selected time-point(s), if ``None`` (default) return for all defined time-points of the graph.
        cell_id : int, optional
            The cell id for which to return the list of cell-vertex ids.
            If ``None``, returns all nodes of the dual graph.
            Only work if `tp` is an integer.
        wall_id : (int, int), optional
            The wall id for which to return the list of cell-vertex ids.
            If ``None``, returns all nodes of the dual graph.
            If defined, override the `cell_id` parameter.
            Only work if `tp` is an integer.
        tcid_indexed : bool, optional
            If ``True``, defaults to ``False``, returns index returned dictionary with tuples of time-indexed cell ids,
            Else by tuples of time-indexed vertex ids.

        Returns
        -------
        list
            By default, a list of time-indexed vertex ids ``tvid = (tp, vid)``, where ``vid=(cid_i, cid_j, cid_k, cid_l)``.
            If ``tcid_indexed`` is ``True``, a list of 4-tuples as time-indexed cell ids ``tvid = (tcid_i, tcid_j, tcid_k, tcid_l) = ((tp, cid_i), (tp, cid_j), (tp, cid_k), (tp, cid_l))``.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> vids = ttg.cell_vertex_ids(0, cell_id=16)  # Get cell-vertex ids at time 0 for cell 16
        >>> print(len(vids))
        16
        >>> print(vids[0])  # print the first one, note the `(t, (cid_i, cid_j, cid_k, cid_l))` format
        (0, (8, 10, 16, 21))
        >>> ep_vids = ttg.cell_vertex_ids(0, wall_id=(1, 16))  # Get cell-vertex ids at time 0 for epidermal cell-wall `(1, 16)`
        >>> print(len(ep_vids))
        5
        >>> t0_vids = ttg.cell_vertex_ids(0)  # Get cell-vertex ids at time 0
        >>> print(len(t0_vids))
        303
        >>> print(t0_vids[0])  # example of a tvid as time-indexed vertex id
        (0, (1, 32, 47, 55))
        >>> t0_vids_i = ttg.cell_vertex_ids(0, tcid_indexed=True)  # Get cell-vertex ids at time 0, as time-indexed cell ids
        >>> print(t0_vids_i[0])  # example of a tvid as time-indexed cell id
        ((0, 1), (0, 32), (0, 47), (0, 55))
        """
        if tp is None:
            tp = list(range(self.nb_time_points))
        else:
            self._check_time_point_exists(tp)

        if isinstance(tp, int):
            if tcid_indexed:
                return [tuple(zip([tp] * 4, cvid)) for cvid in self._tissue_graph[tp].cell_vertex_ids(cell_id, wall_id)]
            else:
                return [(tp, cvid) for cvid in self._tissue_graph[tp].cell_vertex_ids(cell_id, wall_id)]
        elif isinstance(tp, (list, tuple)):
            if tcid_indexed:
                return [tuple(zip([t] * 4, cvid)) for t in tp for cvid in self._tissue_graph[t].cell_vertex_ids()]
            else:
                return [(t, cvid) for t in tp for cvid in self._tissue_graph[t].cell_vertex_ids()]
        else:
            raise ValueError(f"Could not make sense of parameter `tp`: {tp}")

    def n_cell_ids(self, tp=None):
        """Return the number of cell ids per time point or for a given one.

        Parameters
        ----------
        tp : int
            An existing time-index.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.n_cell_ids(0)
        63
        >>> ttg.n_cell_ids()
        {0: 63, 1: 88, 2: 128}
        """
        if tp is None:
            return {tp: len(self.cell_ids(tp)) for tp in range(self.nb_time_points)}
        else:
            return len(self.cell_ids(tp))

    def add_cell_property(self, name, values, unit=""):
        """Add a cell property dictionary to the temporal graph.

        Parameters
        ----------
        name : str
            Name of the cell property to add.
        values : dict
            Cell indexed dictionary ``{(t, cell): ppty_value}``.
        unit : str, optional
            The unit in which the property should be expressed.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
        >>> 'random' in ttg.list_cell_properties()
        False
        >>> ttg.add_cell_property('random', {(0, 2): 0.5, (0, 3): 0.5}, 'NA')
        >>> 'random' in ttg.list_cell_properties()
        True
        """
        for (t, cid), value in values.items():
            self._tissue_graph[t].add_cell_property(name, {cid: value}, unit=unit)
        n_added = len(values)
        if self.verbose:
            log.info(f"Added {n_added} cell values to property '{name}'!")
        return

    def add_cell_wall_property(self, name, values, unit=""):
        """Add a cell-wall property dictionary to the temporal graph.

        Parameters
        ----------
        name : str
            Name of the cell-wall property to add.
        values : dict
            Cell-wall indexed dictionary ``{(t, (cell, cell)): ppty_value}``.
        unit : str, optional
            The unit in which the property should be expressed.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
        >>> 'random' in ttg.list_cell_wall_properties()
        False
        >>> ttg.add_cell_wall_property('random', {(0, (2, 3)): 0.5, (0, (3, 4)): 0.5}, 'NA')
        >>> 'random' in ttg.list_cell_wall_properties()
        True
        """
        for (t, wid), value in values.items():
            self._tissue_graph[t].add_cell_wall_property(name, {wid: value}, unit=unit)
        n_added = len(values)
        if self.verbose:
            log.info(f"Added {n_added} cell-wall values to property '{name}'!")
        return

    def add_cell_edge_property(self, name, values, unit=""):
        """Add a cell-edge property dictionary to the temporal graph.

        Parameters
        ----------
        name : str
            Name of the cell-edge property to add.
        values : dict
            Cell-edge indexed dictionary ``{(t, (tuple(cell*4), tuple(cell*4))): ppty_value}``.
        unit : str, optional
            The unit in which the property should be expressed.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
        >>> 'random' in ttg.list_cell_edge_properties()
        False
        >>> ttg.add_cell_edge_property('random', {(0, ((4, 29, 50, 53), (4, 8, 50, 53))): 0.5, (0, ((2, 3, 5, 11), (2, 5, 7, 11))): 0.5}, 'NA')
        >>> 'random' in ttg.list_cell_edge_properties()
        True
        """
        for (t, eid), value in values.items():
            self._tissue_graph[t].add_cell_edge_property(name, {eid: value}, unit=unit)
        n_added = len(values)
        if self.verbose:
            log.info(f"Added {n_added} cell-edge values to property '{name}'!")
        return

    def add_cell_vertex_property(self, name, values, unit=""):
        """Add a cell-vertex property dictionary to the temporal graph.

        Parameters
        ----------
        name : str
            Name of the cell-vertex property to add.
        values : dict
            Cell-vertex indexed dictionary ``{(t, tuple(cell*4)): ppty_value}``.
        unit : str, optional
            The unit in which the property should be expressed.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
        >>> 'random' in ttg.list_cell_vertex_properties()
        False
        >>> ttg.add_cell_vertex_property('random', {(0, (4, 29, 37, 53)): 0.5, (0, (4, 18, 29, 43)): 0.5}, 'NA')
        >>> 'random' in ttg.list_cell_vertex_properties()
        True
        >>> ttg.cell_vertex_property_dict('random')
        """
        for (t, vid), value in values.items():
            self._tissue_graph[t].add_cell_vertex_property(name, {vid: value}, unit=unit)
        n_added = len(values)
        if self.verbose:
            log.info(f"Added {n_added} cell-vertex values to property '{name}'!")
        return

    def list_cell_properties(self):
        """List the cell properties known to the temporal graph."""
        n_tp = self.nb_time_points
        ppties = list(set(flatten([self._tissue_graph[tp].list_cell_properties() for tp in range(n_tp)])))
        return sorted(ppties + ["time-point", "time-index"])

    def list_cell_wall_properties(self):
        """List the cell-wall properties known to the temporal graph."""
        n_tp = self.nb_time_points
        return sorted(set(flatten([self._tissue_graph[tp].list_cell_wall_properties() for tp in range(n_tp)])))

    def list_cell_edge_properties(self):
        """List the cell-edge properties known to the temporal graph."""
        n_tp = self.nb_time_points
        return sorted(set(flatten([self._tissue_graph[tp].list_cell_edge_properties() for tp in range(n_tp)])))

    def list_cell_vertex_properties(self):
        """List the cell-vertex properties known to the temporal graph."""
        n_tp = self.nb_time_points
        return sorted(set(flatten([self._tissue_graph[tp].list_cell_vertex_properties() for tp in range(n_tp)])))

    def cell_property_unit(self, ppty) -> str:
        """Get the unit of a cell property."""
        # FIXME: this assumes that each property as the same unit for all time-points
        return self._tissue_graph[0]._cell_properties[ppty]

    def cell_wall_property_unit(self, ppty) -> str:
        """Get the unit of a cell-wall property."""
        # FIXME: this assumes that each property as the same unit for all time-points
        return self._tissue_graph[0]._wall_properties[ppty]

    def cell_edge_property_unit(self, ppty) -> str:
        """Get the unit of a cell-edge property."""
        # FIXME: this assumes that each property as the same unit for all time-points
        return self._tissue_graph[0]._edge_properties[ppty]

    def cell_vertex_property_unit(self, ppty) -> str:
        """Get the unit of a cell-vertex property."""
        # FIXME: this assumes that each property as the same unit for all time-points
        return self._tissue_graph[0]._vertex_properties[ppty]

    def cell_neighbors(self, tp, cell_id):
        """Return the neighbors of cell-id at given time-point.

        Parameters
        ----------
        tp : int
            An existing time-index.
        cell_id : int
            A valid cell id at given time-point.

        Returns
        -------
        list
            The list of neighbors for the cell-id at given time-point.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.cell_neighbors(0, 16)
        [8, 10, 13, 19, 20, 21, 34, 38, 58, 61]
        >>> print(len(ttg.cell_neighbors(0, 16)))
        10
        """
        return self._tissue_graph[tp].cell_neighbors(cell_id)

    def cell_vertex_neighbors(self, tp, cv_id):
        """Return the neighbors of cell-id at given time-point.

        Parameters
        ----------
        tp : int
            An existing time-index.
        cell_id : int
            A valid cell-vertex id at given time-point.

        Returns
        -------
        list
            The list of neighbors for the cell-id at given time-point.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> cv_id = list(ttg.cell_vertex_ids(0, 16))[0]  # get the first cell-vertex id from cell 16
        >>> print(cv_id)
        (0, (8, 10, 16, 21))
        >>> print(ttg.cell_vertex_neighbors(*cv_id))
        [(8, 10, 12, 21), (8, 10, 13, 16), (8, 16, 19, 21), (10, 16, 21, 38)]
        """
        return [(tp, nei_cv_id) for nei_cv_id in self._tissue_graph[tp].cell_vertex_neighbors(cv_id)]

    def cell_neighbors_dict(self, tcids=None):
        """Return the cell neighbors dictionary.

        Parameters
        ----------
        tcids : list of tuples, optional
            A list of time-indexed cell ids used to filter keys of returned dictionary.
            If `None` (default), all cells neighborhood are returned.

        Returns
        -------
        dict
            ``{(t, i): neighbors(t, i)}`` for ``(t, i)`` in temporal cell ids `tcids`.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> len(ttg.cell_neighbors_dict())
        279
        >>> ttg.cell_neighbors_dict([(0, 2), (0, 3)])
        {(0, 2): [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
         (0, 3): [2, 5, 9, 10, 11, 14, 15, 23, 25, 26, 41, 48, 52, 54, 56, 57, 63]}
        >>> bkgd_nei = ttg.cell_neighbors_dict(ttg.background)
        >>> print({cid: len(nei) for cid, nei in bkgd_nei.items()})
        {(0, 1): 49, (1, 1): 69, (2, 1): 99}
        """
        if tcids is None:
            tcids = self.cell_ids()
        else:
            tcids = list(set(tcids) & (set(self.cell_ids()) | set(self.background)))
        return {(t, cid): self._tissue_graph[t].cell_neighbors(cid) for t, cid in tcids}

    def cell_property(self, tp, cid, ppty, default=None):
        """Return the value of a temporal cell property.

        Parameters
        ----------
        tp : int
            An existing time-index.
        cid : int
            A valid cell id at given time-point.
        ppty : str
            Name of a valid cell property.
        default : Any, optional
            Value to return if the property is not defined for the temporal cell id, default is ``None``.

        Returns
        -------
        any
            The value associated to the temporal cell property.

        Examples
        --------
        >>> import numpy as np
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.cell_property(2,5,'volumetric_strain_rates')
        >>> ttg._elapsed_time
        [0, 5, 10]
        >>> ttg.cell_property(2,5,'time-point')
        10
        """
        if ppty == 'time-point':
            return self._elapsed_time[tp]
        else:
            return self._tissue_graph[tp].cell_property(cid, ppty, default)

    @staticmethod
    def _only_defined_values(ppty_dict, default):
        """Filter a dictionary, keeping only non-default value.

        Parameters
        ----------
        ppty_dict : dict
            The dictionary to filter.
        default : any
            The default value to filter-out.

        Returns
        -------
        dict
            The dictionary without keys showing a `default` values.
        """
        if default is None:
            return {k: v for k, v in ppty_dict.items() if v is not None}
        elif np.isnan(default):
            return {k: v for k, v in ppty_dict.items() if not np.isnan(v)}
        else:
            return {k: v for k, v in ppty_dict.items() if v != default}

    def cell_property_dict(self, ppty, tcids=None, default=None, only_defined=False):
        """Return the cell property dictionary.

        Parameters
        ----------
        ppty : str
            Name of the cell property to returns.
        tcids : list, optional
            A list of time-indexed cell ids used to filter keys of returned dictionary, all by default.
        default : Any, optional
            Value to return if the property is not defined for a temporal cell id, default is ``None``.
        only_defined : bool, optional
            Returns the dictionary only for ids with "valid" values, *i.e.* a non-default value.

        Returns
        -------
        dict
            ``{(t, i): cell_property(t, i, ppty)}`` for ``(t, i)`` in temporal cell ids `tcids`.

        Examples
        --------
        >>> import numpy as np
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> tcids = ttg.cell_ids(2)
        >>> ttg.cell_property_dict('volumetric_strain_rates', tcids, default=np.nan, only_defined=False)
        >>> ttg.cell_property_dict('time-index', default=None, only_defined=False)
        """
        if tcids is None:
            tcids = self.cell_ids()
        else:
            tcids = list(self.cell_ids() & set(tcids))

        if ppty == 'time-point':
            ppty_dict = {(t, cid): self._elapsed_time[t] for t, cid in tcids}
        elif ppty == 'time-index':
            ppty_dict = {(t, cid): t for t, cid in tcids}
        else:
            ppty_dict = {(t, cid): self.cell_property(t, cid, ppty, default) for t, cid in tcids}

        if only_defined:
            return self._only_defined_values(ppty_dict, default)
        else:
            return ppty_dict

    def cell_property_genealogy(self, tp, cid, ppty, default=None, rank="max"):
        """Return the value of a temporal cell property.

        Parameters
        ----------
        tp : int
            An existing time-index.
        cid : int
            A valid cell id at given time-point.
        ppty : str
            Name of a valid cell property.
        default : Any, optional
            Value to return if the property is not defined for the temporal cell id, default is ``None``.
        rank : int or "max"
            If `"max"` (default), return the whole genealogy starting from given `tp`.
            Else, an integer specifying the depth of the genealogy to return.
            Note that `rank=0` is the cell itself.

        Returns
        -------
        dict
            A rank-indexed dictionary of cell property.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.cell_property_genealogy(0, 5, 'epidermis', default=False, rank=2)
        {0: [False], 1: [False, False], 2: [False, False]}
        >>> ttg.cell_property_genealogy(0, 5, 'volume', default=None, rank=2)
        {0: [448.5], 1: [330.609375, 299.78125], 2: [474.296875, 419.109375]}
        """
        gen = self.descendant(tp, cid, rank)
        return {r: [self._tissue_graph[tp].cell_property(cid, ppty, default) for tp, cid in lin] for r, lin in
                gen.items()}

    def cell_wall_property(self, tp, wid, ppty, default=None):
        """Return the value of a temporal cell-wall property.

        Parameters
        ----------
        tp : int
            An existing time-index.
        wid : int
            A valid cell-wall id at given time-point.
        ppty : str
            Name of a valid cell-wall property.
        default : Any, optional
            Value to return if the property is not defined for the temporal cell-wall id, default is ``None``.

        Returns
        -------
        any
            The value associated to the temporal cell-wall property.
        """
        return self._tissue_graph[tp].cell_wall_property(wid, ppty, default)

    def cell_wall_property_dict(self, ppty, twids=None, default=None, only_defined=False):
        """Return the cell-wall property dictionary.

        Parameters
        ----------
        ppty : str
            Name of the cell property to returns.
        twids : list, optional
            A list of time-indexed cell-wall ids used to filter keys of returned dictionary, all by default.
        default : Any, optional
            Value to return if the property is not defined for a temporal cell-wall id, default is ``None``.
        only_defined : bool, optional
            Returns the dictionary only for ids with "valid" values, *i.e.* a non-default value.

        Returns
        -------
        dict
            ``{(t, wid): cell_wall_property(t, wid, ppty)}`` for ``(t, wid)`` in temporal cell-wall ids `tcids`.
            Remember that ``wid`` is a len-2 tuple of cell ids.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
        >>> ttg.list_cell_wall_properties()
        ['area', 'fused_median', 'median', 'time-point']
        >>> ttg.cell_wall_property_dict('area', twids=[(0, (2, 3)), (0, (3, 4))])
        {(0, (2, 3)): 14.44421672821045}
        >>> cell_wall_area = ttg.cell_wall_property_dict('area', only_defined=True)
        >>> len(cell_wall_area)
        1380
        """
        if twids is None:
            twids = self.cell_wall_ids(tcid_indexed=False)
        else:
            twids = list(set(self.cell_wall_ids(tcid_indexed=False)) & set(twids))

        if ppty == 'time-point':
            ppty_dict = {(t, wid): self._elapsed_time[t] for t, wid in twids}
        elif ppty == 'time-index':
            ppty_dict = {(t, wid): t for t, wid in twids}
        else:
            ppty_dict = {(t, wid): self.cell_wall_property(t, wid, ppty, default) for t, wid in twids}

        if only_defined:
            return self._only_defined_values(ppty_dict, default)
        else:
            return ppty_dict

    def cell_edge_property(self, tp, eid, ppty, default=None):
        """Return the value of a temporal cell-edge property.

        Parameters
        ----------
        tp : int
            An existing time-index.
        eid : int
            A valid cell-edge id at given time-point.
        ppty : str
            Name of a valid cell-edge property.
        default : Any, optional
            Value to return if the property is not defined for the temporal cell-edge id, default is ``None``.

        Returns
        -------
        any
            The value associated to the temporal cell-edge property.
        """
        return self._tissue_graph[tp].cell_edge_property(eid, ppty, default)

    def cell_edge_property_dict(self, ppty, teids=None, default=None, only_defined=False):
        """Return the cell-edge property dictionary.

        Parameters
        ----------
        ppty : str
            Name of the cell property to returns.
        teids : list, optional
            A list of time-indexed cell-edge ids used to filter keys of returned dictionary, all by default.
        default : Any, optional
            Value to return if the property is not defined for a temporal cell-edge id, default is ``None``.
        only_defined : bool, optional
            Returns the dictionary only for ids with "valid" values, *i.e.* a non-default value.

        Returns
        -------
        dict
            ``{(t, eid): cell_vertex_property(t, eid, ppty)}`` for ``(t, eid)`` in temporal cell-edge ids `teids`.
            Remember that ``eid`` is a len-2 tuple of vertex ids.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
        >>> ttg.list_cell_edge_properties()
        ['fused_median', 'linel_id', 'median', 'time-point']
        >>> ttg.cell_edge_property_dict('median', teids=[(0, ((4, 29, 50, 53), (4, 8, 50, 53))), (0, ((2, 3, 5, 11), (2, 5, 7, 11)))])
        {(0, ((4, 29, 50, 53), (4, 8, 50, 53))): [27.0, 29.875, 17.625],
         (0, ((2, 3, 5, 11), (2, 5, 7, 11))): [27.0, 24.625, 28.375]}
        >>> cell_edge_median = ttg.cell_edge_property_dict('median', only_defined=True)
        >>> len(cell_edge_median)
        2440
        """
        if teids is None:
            teids = self.cell_edge_ids(tcid_indexed=False)
        else:
            teids = list(set(self.cell_edge_ids(tcid_indexed=False)) & set(teids))

        if ppty == 'time-point':
            ppty_dict = {(t, eid): self._elapsed_time[t] for t, eid in teids}
        elif ppty == 'time-index':
            ppty_dict = {(t, eid): t for t, eid in teids}
        else:
            ppty_dict = {(t, eid): self.cell_edge_property(t, eid, ppty, default) for t, eid in teids}

        if only_defined:
            return self._only_defined_values(ppty_dict, default)
        else:
            return ppty_dict

    def cell_vertex_property(self, tp, vid, ppty, default=None):
        """Return the value of a temporal cell-vertex property.

        Parameters
        ----------
        tp : int
            An existing time-index.
        vid : int
            A valid cell-vertex id at given time-point.
        ppty : str
            Name of a valid cell-vertex property.
        default : Any, optional
            Value to return if the property is not defined for the temporal cell-vertex id, default is ``None``.

        Returns
        -------
        any
            The value associated to the temporal cell-vertex property.
        """
        return self._tissue_graph[tp].cell_vertex_property(vid, ppty, default)

    def cell_vertex_property_dict(self, ppty, tvids=None, default=None, only_defined=False):
        """Return the cell-vertex property dictionary.

        Parameters
        ----------
        ppty : str
            Name of the cell property to returns.
        tvids : list, optional
            A list of time-indexed cell-vertex ids used to filter keys of returned dictionary, all by default.
        default : Any, optional
            Value to return if the property is not defined for a temporal cell-vertex id, default is ``None``.
        only_defined : bool, optional
            Returns the dictionary only for ids with "valid" values, *i.e.* a non-default value.

        Returns
        -------
        dict
            ``{(t, vid): cell_vertex_property(t, vid, ppty)}`` for ``(t, vid)`` in temporal cell-vertex ids `tvids`.
            Remember that ``vid`` is a len-4 tuple of cell ids.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
        >>> ttg.list_cell_vertex_properties()
        ['coordinate', 'fused_coordinate', 'time-point']
        >>> ttg.cell_vertex_property_dict('coordinate', tvids=[(0, (4, 29, 50, 53)), (0, (4, 8, 50, 53))])
        {(0, (4, 8, 50, 53)): [25.875, 17.875, 30.125],
         (0, (4, 29, 50, 53)): [28.625, 17.625, 29.625]}
        >>> cell_vertex_coordinate = ttg.cell_vertex_property_dict('coordinate', only_defined=True)
        >>> len(cell_vertex_coordinate)
        1420
        """
        if tvids is None:
            tvids = self.cell_vertex_ids(tcid_indexed=False)
        else:
            tvids = list(set(self.cell_vertex_ids(tcid_indexed=False)) & set(tvids))

        if ppty == 'time-point':
            ppty_dict = {(t, vid): self._elapsed_time[t] for t, vid in tvids}
        elif ppty == 'time-index':
            ppty_dict = {(t, vid): t for t, vid in tvids}
        else:
            ppty_dict = {(t, vid): self.cell_vertex_property(t, vid, ppty, default) for t, vid in tvids}

        if only_defined:
            return self._only_defined_values(ppty_dict, default)
        else:
            return ppty_dict

    def lineaged_cell_ids(self, forward=True, backward=False, time_point=None):
        """Get the list of lineaged cells.

        Parameters
        ----------
        forward : bool, optional
            If ``True`` (default), requires the cell to have at least a descendant.
        backward: bool, optional
            If ``True`` (default ``False``), requires the cell to have an ancestor.
        time_point : int or list, optional
            Filter returned cell that belong to selected time-point(s).

        Returns
        -------
        list
            List of time-indexed cell ids.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
        >>> len(ttg.lineaged_cell_ids())
        151
        >>> len(ttg.lineaged_cell_ids(time_point=0))
        63
        """
        tcids = list(self._temporal_graph.nodes())
        if isinstance(time_point, int):
            tcids = [(t, cid) for (t, cid) in tcids if t == time_point]
        elif isinstance(time_point, (tuple, list)):
            tcids = [(t, cid) for (t, cid) in tcids if t in time_point]

        if forward:
            tcids = [(t, cid) for (t, cid) in tcids if self.has_descendant(t, cid)]
        if backward:
            tcids = [(t, cid) for (t, cid) in tcids if self.has_ancestor(t, cid)]

        return tcids

    def add_cell_identity(self, identity, tcids, strict=False, by_time_point=False):
        """Defines a list of time-index cell ids as belonging to given identity to the temporal graph.

        Parameters
        ----------
        identity : str
            Name of the identity to add.
        tcids : list of len-2 tuples, optional
            List of time-indexed cell ids to add to the identity.
        strict : bool, optional
            If ``True`` (default is ``False``), make sure to exclude all other cell-ids from this identity.
            By default, only update the list of given cell as belonging to this identity.
        by_time_point : bool, optional
            Control the behaviour of `strict`.
            If ``True`` (default is ``False``), work by time-point (exclude other cell from same time-point).
            By default, exclude all other cell (from all time-points).

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
        >>> ttg.add_cell_identity('test', [(0, 16)])
        >>> ttg.list_identities()
        ['epidermis', 'test']
        >>> ttg.list_cells_with_identity('test')
        [(0, 16)]
        >>> ttg.add_cell_identity('test', [(1, 16)])
        >>> ttg.list_cells_with_identity('test')
        [(0, 16), (1, 16)]
        >>> ttg.add_cell_identity('test', [(2, 16)], strict=True)
        >>> ttg.list_cells_with_identity('test')
        [(2, 16)]
        >>> ttg.add_cell_identity('test', [(1, 16)], strict=True, by_time_point=True)
        >>> ttg.list_cells_with_identity('test')
        [(1, 16), (2, 16)]
        """
        # Reference this "identity" name to all tissue graphs:
        for t in range(self.nb_time_points):
            if identity not in self._tissue_graph[t]._cell_identities:
                self._tissue_graph[t]._cell_identities.append(identity)
        # Ensure `tcids` is iterable:
        if isinstance(tcids, tuple):
            tcids = [tcids]
        # Gather cids by time-points:
        cids_by_tp = {t: [] for t in range(self.nb_time_points)}
        for (t, cid) in tcids:
            cids_by_tp[t] += [cid]
        # Call the hidden method to add an identity to cells:
        for t, cids in cids_by_tp.items():
            if by_time_point and len(cids) == 0:
                continue
            self._tissue_graph[t]._add_cell_identity(identity, cids, strict)
        return

    def remove_cell_identity(self, identity, tcids):
        """Remove the list of cells from the identity set.

        Parameters
        ----------
        identity : str
            Name of the identity to add.
        tcids : list of len-2 tuples, optional
            List of time-indexed cell ids to which identity should be added.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
        >>> ttg.list_identities()
        ['epidermis']
        >>> epidermis_tcids = ttg.list_cells_with_identity('epidermis')
        >>> excluded_tcid = epidermis_tcids[0]
        >>> print(excluded_tcid)
        (0, 16)
        >>> ttg.cell_has_identity(*excluded_tcid, identity='epidermis')
        True
        >>> ttg.remove_cell_identity('epidermis', excluded_tcid)
        >>> ttg.cell_has_identity(*excluded_tcid, identity='epidermis')
        False
        """
        # Ensure `tcids` is iterable:
        if isinstance(tcids, tuple):
            tcids = [tcids]
        for (t, cid) in tcids:
            self._tissue_graph[t]._remove_cell_identity(identity, [cid])

    def list_identities(self) -> list:
        """List all the cell identities known to the temporal tissue graph."""
        idties = []
        for t in range(self.nb_time_points):
            idties.extend(self._tissue_graph[t].list_identities())
        return list(set(idties))

    def cell_identities(self, tcid):
        """List all identities attached to this cell.

        Parameters
        ----------
        cids : int or list or None, optional
            If ``None`` (default) return the dictionary for all cells.
            If an integer or a list return the dictionary for selected cells.
        identity : str
            Name of the cell identity to add.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
        >>> ttg.add_cell_identity([(0, 16)],'test')
        >>> ttg.cell_identities((0, 16))
        ['epidermis', 'test']
        """
        return [idty for idty in self.list_identities() if self.cell_has_identity(*tcid, identity=idty)]

    def cell_identities_dict(self, tcids=None):
        """Cell-based dictionary of cell identities.

        Parameters
        ----------
        tcids : list of len-2 tuple or None, optional
            If ``None`` (default) return the dictionary for all cells from all time-points.
            If a list return the dictionary for selected time-indexed cells.

        Returns
        -------
        dict
            Time-indexed cell-based dictionary of cell identities.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
        >>> ttg.add_cell_identity([(0, 16)],'test')
        """
        if tcids is None:
            tcids = self.cell_ids()
        return {(t, cid): self._tissue_graph[t].cell_identities(cid) for t, cid in tcids}

    def cell_has_identity(self, tp, cid, identity, rank=0):
        """Test if the cell has the given identity.

        Parameters
        ----------
        tp : int
            An existing time-index.
        cid : int
            A valid cell id at given time-point.
        identity : str
            Name of a valid cell identity, *i.e.* a boolean property.
        rank : int or "max"
            If ``rank=0``, test it the cell has the given identity.
            Else, test if the descendants (up to given `rank`) of the cell also have the same identity.
            If ``rank="max"``, the rank is the maximum rank.

        Returns
        -------
        bool
            Boolean describing if the cell has the tested `identity`.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.cell_has_identity(0,5,'epidermis')  # cell 5 from t0 does not belong to the epidermis!
        False
        >>> ttg.cell_has_identity(0,16,'epidermis')  # cell 16 from t0 does belong to the epidermis!
        True
        >>> ttg.coherent_cell_identity(0,16,'epidermis',rank=1)  # rank 1 descendants of cell 16 from t0 do belong to the epidermis!
        True
        >>> ttg.cell_has_identity(0,16,'epidermis',rank=1)  # hence, cell 16 from t0 and its descendant at rank 1 all belong to the epidermis!
        True
        """
        cell_identity = self._tissue_graph[tp].cell_property(cid, identity, default=False)
        if rank > 0:
            return cell_identity and self.coherent_cell_identity(tp, cid, identity, rank)
        else:
            return cell_identity

    def coherent_cell_identity(self, tp, cid, identity, rank=1):
        """Test if the cell has a coherent (stable) identity over time (here specified as rank).

        Parameters
        ----------
        tp : int
            An existing time-index.
        cid : int
            A valid cell id at given time-point.
        identity : str
            Name of a valid cell property referring to identity, *i.e.* a boolean property.
        rank : int or "max"
            If `"max"` (default is  ``1``), return the whole genealogy starting from given `tp`.
            Else, an integer specifying the depth of the genealogy to test.
            Note that `rank=0` is the cell itself, so it should be strictly greater than ``1``.

        Returns
        -------
        bool
            ``True`` if the cell has a coherent (stable) identity over time, ``False`` otherwise.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.cell_property_genealogy(0, 16, 'epidermis', default=False, rank=2)
        {0: [True], 1: [True], 2: [True]}
        >>> ttg.coherent_cell_identity(0, 16, 'epidermis', rank=2)
        True
        >>> ttg.cell_property_genealogy(0, 5, 'epidermis', default=False, rank=2)
        {0: [False], 1: [False, False], 2: [False, False]}
        >>> ttg.coherent_cell_identity(0, 5, 'epidermis', rank=2)
        True
        """
        # Cells from last time-point cannot be tested over next time-point!
        if tp == self.nb_time_points - 1:
            return False

        max_rank = self.nb_time_points - tp - 1  # max depth of the genealogy starting from given `tp`
        if rank == "max":
            rank = max_rank
        else:
            rank = min(rank, max_rank)

        try:
            assert rank >= 1
        except AssertionError:
            log.critical(f"Rank values strictly inferior to 1 are not possible, got `{rank}`!")
            return False

        cell_identities = self.cell_property_genealogy(tp, cid, identity, default=False, rank=rank)
        init_id = cell_identities[0][0]  # cell identity at rank 0, that is the identity of the `cid`
        all_ids = flatten(list(cell_identities.values()))
        return all(i == init_id for i in all_ids)

    def list_cells_with_identity(self, identity, rank=0, tp=None):
        """List all cell that present the defined `identity`.

        Parameters
        ----------
        identity : str
            Name of a valid cell identity, *i.e.* a boolean property.
        rank : int or "max"
            If ``rank=0``, test if the cell has the given identity.
            Else, test if the descendants (up to given `rank`) of the cell also have the same identity.
            If ``rank="max"``, the rank is the maximum rank.
        tp : None or int or list of int
            Selected time-point(s), if ``None`` (default) return for all defined time-points of the graph.

        Returns
        -------
        list
            List of cells that present the defined `identity`.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> # Number of cells with the 'epidermis' identity
        >>> epidermal_cell_ids = ttg.list_cells_with_identity('epidermis')
        >>> len(epidermal_cell_ids)
        217
        >>> # Number of cells with the 'epidermis' identity from time-point 0, that keep this identity at the next time-point
        >>> epidermal_cell_ids = ttg.list_cells_with_identity('epidermis', rank=1, tp=0)
        >>> len(epidermal_cell_ids)  # should be 49
        49
        """
        cids = self.cell_ids(tp)
        return [(t, cid) for t, cid in cids if self.cell_has_identity(t, cid, identity, rank=rank)]

    # ------------------------------------------------------------------------------------------------------------------
    # SPATIAL DIFFERENTIATION FUNCTIONS
    # ------------------------------------------------------------------------------------------------------------------

    def _check_missing_spatial_values(self, tcid, neighbor_values, ppty, max_missing_percent):
        """Check the proportion of missing values for neighbors is acceptable."""
        t, cid = tcid
        n_nei = len(neighbor_values)
        n_miss = sum([np.isnan(v) for v in neighbor_values])
        miss_pc = n_miss / n_nei * 100
        if n_miss != 0:
            if n_miss == n_nei:
                log.critical(
                    f"Cell {cid} from time {t} has {n_nei} neighbors, all without values for property '{ppty}'!")
            elif miss_pc >= max_missing_percent:
                log.critical(
                    f"Cell {cid} from time {t} has {miss_pc}% ({n_miss}/{n_nei}) neighbors without values for property '{ppty}'!")
            else:
                log.warning(
                    f"Cell {cid} from time {t} has {miss_pc}% ({n_miss}/{n_nei}) neighbors without values for property '{ppty}'!")
        return miss_pc

    def _get_spatial_values(self, tp, cid, ppty, max_missing_percent):
        """Get the values required to perform spatial differentiation of a scalar."""
        neighbors = self.cell_neighbors(tp, cid)
        # If no neighbors was found, return the default value
        if neighbors == []:
            return None, None

        cell_value = self.cell_property(tp, cid, ppty, np.nan)
        # If no value is found for the current cell id (tp, cid), return the default value
        if cell_value is None or np.isnan(cell_value):
            return None, None

        neighbor_values = [self.cell_property(tp, nei, ppty, np.nan) for nei in neighbors]
        miss_pc = self._check_missing_spatial_values((tp, cid), neighbor_values, ppty, max_missing_percent)
        # If the percentage of descendants without value is greater than the given limit, return the default value
        if miss_pc >= max_missing_percent:
            return None, None

        return cell_value, neighbor_values

    def cell_mean_absolute_deviation(self, tp, cell_id, ppty, default=None, max_missing_percent=25):
        """Compute the mean absolute deviation for a given cell at a given time on a given property.

        Parameters
        ----------
        tp : int
            Time-point of the cell.
        cell_id : int
            ID of the cell.
        ppty : str
            Spatial property to spatially differentiate.
        time_points : int or list, optional
            If ``None`` (default) compute the spatial differentiation function for all time-point.
            Else, should be a list of valid time-points to select for computation.
        default : Any, optional
            The default value to return when no value is found for neighbors cell property or too many neighbors do not have a value
            Defaults to ``None``.
        max_missing_percent : int, optional
            The acceptable percentage of missing values among neighbors to return a value.
            Else returns the `default` value. ``25`` by default.

        Returns
        -------
        float
            The mean absolute deviation.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.cell_mean_absolute_deviation(0, 2, 'volume')
        179.66466346153845
        """
        cell_value, neighbor_values = self._get_spatial_values(tp, cell_id, ppty, max_missing_percent)
        if None in (cell_value, neighbor_values):
            return default
        else:
            return mean_absolute_deviation(cell_value, neighbor_values)

    def mean_absolute_deviation(self, ppty, time_points=None, default=np.nan, max_missing_percent=25):
        """Compute the mean absolute deviation for all cells on a given property.

        Parameters
        ----------
        ppty : str
            Spatial property to spatially differentiate
        time_points : int or list, optional
            If ``None`` (default) compute the spatial differentiation function for all time-point.
            Else, should be a list of valid time-points to select for computation.
        default : Any, optional
            The default value to return when no value is found for neighbors cell property or too many neighbors do not have a value
            Defaults to ``None``.
        max_missing_percent : int, optional
            The acceptable percentage of missing values among neighbors to return a value.
            Else returns the `default` value. ``25`` by default.

        Returns
        -------
        dict
            The mean absolute deviation dictionary, ``{(tp, cid) : LTR(tp, cid, ppty)}``

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> mad_volume = ttg.mean_absolute_deviation('volume')
        """
        time_points = self._diff_tp(ppty, time_points)
        ppty_dict = {(tp, cid): self.cell_mean_absolute_deviation(tp, cid, ppty, default, max_missing_percent) for
                     tp, cid in self.cell_ids(time_points)}
        self.add_cell_property(f"mean_absolute_deviation('{ppty}')", ppty_dict, unit=self.get_cell_property_unit(ppty))
        return ppty_dict

    # ------------------------------------------------------------------------------------------------------------------
    # TEMPORAL DIFFERENTIATION FUNCTIONS
    # ------------------------------------------------------------------------------------------------------------------
    @staticmethod
    def _check_missing_temporal_values(tcid, descendant_values, ppty, max_missing_percent):
        """Check the proportion of missing values for descendants is acceptable.

        Parameters
        ----------
        tcid : (int, int)
            Time idexed identity of the parent cell.
        descendant_values : list of float
            The list of values obtained from descendants.
        ppty : str
            Property to temporally differentiate.
        max_missing_percent : int, optional
            The acceptable percentage of missing values among descendants to return a value.

        Returns
        -------
        float or None
            The precentage of missing values.
        """
        t, cid = tcid
        n_desc = len(descendant_values)
        n_miss = sum([np.isnan(v) for v in descendant_values])
        miss_pc = n_miss / n_desc * 100
        if n_miss != 0:
            if n_miss == n_desc:
                log.warning(
                    f"Cell {cid} from time {t} has {n_desc} descendants, all without values for property '{ppty}'!")
            elif miss_pc >= max_missing_percent:
                log.warning(
                    f"Cell {cid} from time {t} has {miss_pc}% ({n_miss}/{n_desc}) descendants without values for property '{ppty}'!")
            else:
                log.warning(
                    f"Cell {cid} from time {t} has {miss_pc}% ({n_miss}/{n_desc}) descendants without values for property '{ppty}'!")
        return miss_pc

    def _get_temporal_values(self, tp, cid, ppty, max_missing_percent, rank):
        """Get the values required to perform temporal differentiation of a scalar.

        Parameters
        ----------
        tp : int
            Time-point of the parent cell.
        cid : int
            Identity of the parent cell.
        ppty : str
            Property to temporally differentiate.
        max_missing_percent : int, optional
            The acceptable percentage of missing values among descendants to return a value.
        rank : int
            The temporal ranked distance from the parent cell to search for descendants.

        Returns
        -------
        float or None
            The value of the parent cell ``cid`` at time ``tp``.
        """
        interval = self.time_interval(tp, tp + rank)
        if interval is None:
            return None, None, None

        try:
            descendants = self.descendant(tp, cid, rank)[rank]
        except KeyError:
            descendants = []
        # If no descendant was found, return the default value
        if descendants == []:
            return None, None, interval

        parent_value = self.cell_property(tp, cid, ppty, np.nan)
        # If no parent value is found, return the default value
        if parent_value is None or np.isnan(parent_value) or parent_value == 0:
            return None, None, interval

        descendant_values = [self.cell_property(tpd, desc, ppty, np.nan) for (tpd, desc) in descendants]
        miss_pc = self._check_missing_temporal_values((tp, cid), descendant_values, ppty, max_missing_percent)
        # If the percentage of descendants without value is greater than the given limit, return the default value
        if miss_pc >= max_missing_percent:
            return None, None, interval

        return parent_value, descendant_values, interval

    def _relative_value(self, tp, cid, ppty, rank, default, max_missing_percent, log):
        """Compute the relative value or log-2 relative value.

        Parameters
        ----------
        tp : int
            Time-point of the parent cell.
        cid : int
            Identity of the parent cell.
        ppty : str
            Property to temporally differentiate.
        rank : int
            The temporal ranked distance from the parent cell to search for descendants.
        default : any
            The default value to returns if ``tp + rank`` does not exist.
        max_missing_percent : int
            The acceptable percentage of missing values among descendants to return a value.
        log : bool
            Whether to compute ``temporal_log_relative_value`` if ``True`` else ``temporal_relative_value``.

        Returns
        -------
        float
            The computed temporal (log-2) relative value between ``cid`` from time ``tp`` and its descendants, if any.

        See Also
        --------
        timagetk.graphs.utils.relative_value
        timagetk.graphs.utils.log_relative_value
        """
        anc_value, desc_values, interval = self._get_temporal_values(tp, cid, ppty, max_missing_percent, rank)
        if None in (anc_value, desc_values):
            return default
        if log:
            return temporal_log_relative_value(anc_value, desc_values, interval)
        else:
            return temporal_relative_value(anc_value, desc_values, interval)

    def cell_relative_value(self, tp, cell_id, ppty, rank=1, default=None, max_missing_percent=25):
        """Compute a temporal relative value for a cell on a given property.

        Parameters
        ----------
        tp : int
            Time-point of the parent cell.
        cell_id : int
            Identity of the parent cell.
        ppty : str
            Property to temporally differentiate.
        rank : int, optional
            The temporal ranked distance from the parent cell to search for descendants.
            Defaults to ``1`` to get the child at the next time-point.
        default : any, optional
            The default value to return when no value is found for ancestor cell property or too many descendants do not have a value
            Defaults to ``None``.
        max_missing_percent : int, optional
            The acceptable percentage of missing values among descendants to return a value.
            Else returns the `default` value. ``25`` by default.

        Returns
        -------
        float
            The temporal relative value of a cell property.

        Raises
        ------
        TypeError
            If the property is not an integer or a float.

        See Also
        --------
        timagetk.graphs.utils.relative_value
        timagetk.graphs.utils.log_relative_value

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.cell_relative_value(0, 2, 'volume')
        0.2979492102277686
        """
        self._check_ppty_scalar(tp, cell_id, ppty)
        return self._relative_value(tp, cell_id, ppty, rank=rank, default=default,
                                    max_missing_percent=max_missing_percent, log=False)

    def cell_log_relative_value(self, tp, cell_id, ppty, rank=1, default=None, max_missing_percent=25):
        """Compute the temporal log relative value for a cell on a given property.

        Parameters
        ----------
        tp : int
            Time-point of the parent cell.
        cell_id : int
            Identity of the parent cell.
        ppty : str
            Property to temporally differentiate.
        rank : int, optional
            The temporal ranked distance from the parent cell to search for descendants.
            Defaults to ``1`` to get the child at the next time-point.
        default : any, optional
            The default value to return when no value is found for ancestor cell property or too many descendants do not have a value
            Defaults to ``None``.
        max_missing_percent : int, optional
            The acceptable percentage of missing values among descendants to return a value.
            Else returns the `default` value. ``25`` by default.

        Returns
        -------
        float
            The temporal log relative value of a cell property.

        Raises
        ------
        TypeError
            If the property is not an integer or a float.

        Notes
        -----
        For each parent cell :math:`c` having :math:`j=\{1,...,D\}` descendants, the temporal log relative value (LTR) is defined as:
        :math:`LTR(c) = \dfrac{1}{\Delta t} \ln \left( \dfrac{\sum_{j=1}^{D} x_j}{x_c} \right)`,
        where :math:`x_i` is the value of cell :math:`i` for any scalar feature attached to a cell.

        Values in :math:`]0, +\infty[` correspond to growth.
        Values in :math:`]-\infty, 0[` correspond to shrink.
        And :math:`0` is no change.

        Compared to the *temporal change*, the *log transformation* symmetries the values' distribution for increase and
        decrease around 0.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.cell_log_relative_value(0,2,'volume')
        0.07972113399086092
        """
        self._check_ppty_scalar(tp, cell_id, ppty)
        return self._relative_value(tp, cell_id, ppty, rank=rank, default=default,
                                    max_missing_percent=max_missing_percent, log=False)

    def _rate_of_change(self, tp, cid, ppty, rank, default, max_missing_percent, relative):
        """Compute the temporal relative value or temporal log relative value.

        Parameters
        ----------
        tp : int
            Time-point of the parent cell.
        cid : int
            Identity of the parent cell.
        ppty : str
            Property to temporally differentiate.
        rank : int
            The temporal ranked distance from the parent cell to search for descendants.
        default : any
            The default value to return when no value is found for ancestor cell property or too many descendants do not have a value
        max_missing_percent : int
            The acceptable percentage of missing values among descendants to return a value.

        Returns
        -------
        float
            The computed temporal (relative) rate of change between ``cid`` from time ``tp`` and its descendants, if any.
        """
        anc_value, desc_values, interval = self._get_temporal_values(tp, cid, ppty, max_missing_percent, rank)
        if None in (anc_value, desc_values):
            return default
        if relative:
            return temporal_relative_rate_of_change(anc_value, desc_values, interval)
        else:
            return temporal_rate_of_change(anc_value, desc_values, interval)

    def cell_rate_of_change(self, tp, cell_id, ppty, rank=1, default=None, max_missing_percent=25):
        """Compute the temporal rate of change for a cell on a given property.

        Parameters
        ----------
        tp : int
            Time-point of the parent cell.
        cell_id : int
            Identity of the parent cell.
        ppty : str
            Property to temporally differentiate.
        rank : int, optional
            The temporal ranked distance from the parent cell to search for descendants.
            Defaults to ``1`` to get the child at the next time-point.
        default : any, optional
            The default value to return when no value is found for ancestor cell property or too many descendants do not have a value
            Defaults to ``None``.
        max_missing_percent : int, optional
            The acceptable percentage of missing values among descendants to return a value.
            Else returns the `default` value. ``25`` by default.

        Returns
        -------
        float
            The temporal rate of change of a cell property.

        Raises
        ------
        TypeError
            If the property is not an integer or a float.

        Notes
        -----
        For each parent cell :math:`c` having :math:`j=\{1,...,D\}` descendants, the temporal change (ATC) is defined as:
        :math:`ATC(c) = \dfrac{1}{\Delta t} \left( \sum_{j=1}^{D} x_j - x_c \right)`,
        where :math:`x_i` is the value of cell :math:`i` for any scalar feature attached to a cell.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.cell_rate_of_change(0,2,'volume')
        17.5375
        """
        self._check_ppty_scalar(tp, cell_id, ppty)
        return self._rate_of_change(tp, cell_id, ppty, rank=rank, default=default,
                                    max_missing_percent=max_missing_percent, relative=False)

    def cell_relative_rate_of_change(self, tp, cell_id, ppty, rank=1, default=None, max_missing_percent=25):
        """Compute the temporal rate of change for a cell on a given property.

        Parameters
        ----------
        tp : int
            Time-point of the parent cell.
        cell_id : int
            Identity of the parent cell.
        ppty : str
            Property to temporally differentiate.
        rank : int, optional
            The temporal ranked distance from the parent cell to search for descendants.
            Defaults to ``1`` to get the child at the next time-point.
        default : any, optional
            The default value to return when no value is found for ancestor cell property or too many descendants do not have a value
            Defaults to ``None``.
        max_missing_percent : int, optional
            The acceptable percentage of missing values among descendants to return a value.
            Else returns the `default` value. ``25`` by default.

        Returns
        -------
        float
            The relative temporal change of the cell property

        Raises
        ------
        TypeError
            If the property is not an integer or a float.

        Notes
        -----
        For each parent cell :math:`c` having :math:`j=\{1,...,D\}` descendants, the temporal change (ATC) is defined as:
        :math:`RTC(c) = \dfrac{1}{\Delta t} \left( \dfrac{\sum_{j=1}^D x_j - x_c }{x_c} \right)`,
        where :math:`x_i` is the value of cell :math:`i` for any scalar feature attached to a cell.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.cell_relative_rate_of_change(0,2,'volume')
        0.09794921022776858
        """
        self._check_ppty_scalar(tp, cell_id, ppty)
        return self._rate_of_change(tp, cell_id, ppty, rank=rank, default=default,
                                    max_missing_percent=max_missing_percent, relative=True)

    def _diff_tp(self, ppty, time_points, rank=1):
        """Pre-processing for temporal differentiation function working on whole temporal graph or time-points.

        Parameters
        ----------
        ppty : str
            Property to temporally differentiate
        time_points : int or list, optional
            If ``None`` (default) compute the temporal differentiation function for all time-point.
            Else, should be a list of valid time-points to select for computation.
        rank : int, optional
            The temporal ranked distance from the parent cell to search for descendants.
            Defaults to ``1`` to get the child at the next time-point.

        Returns
        -------
        list
            The list of time-points to temporally differentiate.

        Notes
        -----
        Check the time-points exists & the property is a scalar.
        """
        time_points = self._filter_time_points(time_points, rank=rank)
        [self._check_ppty_scalar(tp, list(self._tissue_graph[tp].cell_property_dict(ppty, only_defined=True).keys())[0],
                                 ppty) for tp in time_points]
        return time_points

    def relative_value(self, ppty, rank=1, time_points=None, default=np.nan, max_missing_percent=25):
        """Compute a temporal relative value for all cells on a given property.

        Parameters
        ----------
        ppty : str
            Property to temporally differentiate
        rank : int, optional
            The temporal ranked distance from the parent cell to search for descendants.
            Defaults to ``1`` to get the child at the next time-point.
        time_points : int or list, optional
            If ``None`` (default) compute the temporal differentiation function for all time-point.
            Else, should be a list of valid time-points to select for computation.
        default : any, optional
            The default value to return when no value is found for ancestor cell property or too many descendants do not have a value
            Defaults to ``None``.
        max_missing_percent : int, optional
            The acceptable percentage of missing values among descendants to return a value.
            Else returns the `default` value. ``25`` by default.

        Returns
        -------
        dict
            The temporal relative value dictionary, ``{(tp, cid) : TR(tp, cid, ppty)}``

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> "relative_value('volume',1)" in ttg.list_cell_properties()
        False
        >>> tr_volume = ttg.relative_value('volume')
        >>> "relative_value('volume',1)" in ttg.list_cell_properties()
        True
        >>> tr_volume_r2 = ttg.relative_value('volume',rank=2)
        """
        time_points = self._diff_tp(ppty, time_points, rank=rank)
        ppty_dict = {(tp, cid): self._relative_value(tp, cid, ppty, rank=rank, default=default,
                                                     max_missing_percent=max_missing_percent, log=False)
                     for tp, cid in self.cell_ids(time_points)}
        unit = f"{self.get_cell_property_unit(ppty)}/{self._time_unit}"
        self.add_cell_property(f"relative_value('{ppty}',{rank})", ppty_dict, unit=unit)
        return ppty_dict

    def log_relative_value(self, ppty, rank=1, time_points=None, default=np.nan, max_missing_percent=25):
        """Compute a temporal log relative value for all cells on a given property.

        Parameters
        ----------
        ppty : str
            Property to temporally differentiate
        rank : int, optional
            The temporal ranked distance from the parent cell to search for descendants.
            Defaults to ``1`` to get the child at the next time-point.
        time_points : int or list, optional
            If ``None`` (default) compute the temporal differentiation function for all time-point.
            Else, should be a list of valid time-points to select for computation.
        default : any, optional
            The default value to return when no value is found for ancestor cell property or too many descendants do not have a value
            Defaults to ``None``.
        max_missing_percent : int, optional
            The acceptable percentage of missing values among descendants to return a value.
            Else returns the `default` value. ``25`` by default.

        Returns
        -------
        dict
            The temporal log relative value dictionary, ``{(tp, cid) : LTR(tp, cid, ppty)}``

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> "log_relative_value('volume',1)" in ttg.list_cell_properties()
        False
        >>> ltr_volume = ttg.log_relative_value('volume'),
        >>> "log_relative_value('volume',1)" in ttg.list_cell_properties()
        True
        """
        time_points = self._diff_tp(ppty, time_points, rank)
        ppty_dict = {(tp, cid): self._relative_value(tp, cid, ppty, rank=rank, default=default,
                                                     max_missing_percent=max_missing_percent, log=True)
                     for tp, cid in self.cell_ids(time_points)}
        unit = f"{self._time_unit}-1"
        self.add_cell_property(f"log_relative_value('{ppty}',{rank})", ppty_dict, unit=unit)
        return ppty_dict

    def rate_of_change(self, ppty, rank=1, time_points=None, default=np.nan, max_missing_percent=25):
        """Compute the temporal rate of change for all cells on a given property.

        Parameters
        ----------
        ppty : str
            Property to temporally differentiate.
        rank : int, optional
            The temporal ranked distance from the parent cell to search for descendants.
            Defaults to ``1`` to get the child at the next time-point.
        time_points : int or list, optional
            If ``None`` (default) compute the temporal differentiation function for all time-point.
            Else, should be a list of valid time-points to select for computation.
        default : any, optional
            The default value to return when no value is found for ancestor cell property or too many descendants do not have a value
            Defaults to ``None``.
        max_missing_percent : int, optional
            The acceptable percentage of missing values among descendants to return a value.
            Else returns the `default` value. ``25`` by default.

        Returns
        -------
        dict
            The temporal rate of change dictionary, ``{(tp, cid) : RC(tp, cid, ppty)}``

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> "rate_of_change('volume',1)" in ttg.list_cell_properties()
        False
        >>> rc_volume = ttg.rate_of_change('volume')
        >>> "rate_of_change('volume',1)" in ttg.list_cell_properties()
        True
        """
        time_points = self._diff_tp(ppty, time_points, rank)
        ppty_dict = {
            (tp, cid): self._rate_of_change(tp, cid, ppty, rank=rank, default=default,
                                            max_missing_percent=max_missing_percent, relative=False) for
            tp, cid in self.cell_ids(time_points)}
        unit = f"{self.get_cell_property_unit(ppty)}/{self._time_unit}-1"
        self.add_cell_property(f"rate_of_change('{ppty}',{rank})", ppty_dict, unit=unit)
        return ppty_dict

    def relative_rate_of_change(self, ppty, rank=1, time_points=None, default=np.nan, max_missing_percent=25):
        """Compute the temporal relative rate of change for all cells on a given property.

        Parameters
        ----------
        ppty : str
            Property to temporally differentiate.
        rank : int, optional
            The temporal ranked distance from the parent cell to search for descendants.
            Defaults to ``1`` to get the child at the next time-point.
        time_points : int or list, optional
            If ``None`` (default) compute the temporal differentiation function for all time-point.
            Else, should be a list of valid time-points to select for computation.
        default : any, optional
            The default value to return when no value is found for ancestor cell property or too many descendants do not have a value
            Defaults to ``None``.
        max_missing_percent : int, optional
            The acceptable percentage of missing values among descendants to return a value.
            Else returns the `default` value. ``25`` by default.

        Returns
        -------
        dict
            The temporal relative rate of change dictionary, ``{(tp, cid) : RTC(tp, cid, ppty)}``

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> "relative_rate_of_change('volume',1)" in ttg.list_cell_properties()
        False
        >>> rtc_volume = ttg.relative_rate_of_change('volume')
        >>> "relative_rate_of_change('volume',1)" in ttg.list_cell_properties()
        True
        """
        time_points = self._diff_tp(ppty, time_points, rank)
        ppty_dict = {
            (tp, cid): self._rate_of_change(tp, cid, ppty, rank=rank, default=default,
                                            max_missing_percent=max_missing_percent, relative=True) for
            tp, cid in self.cell_ids(time_points)}
        unit = f"{self._time_unit}-1"
        self.add_cell_property(f"relative_rate_of_change('{ppty}',{rank})", ppty_dict, unit=unit)
        return ppty_dict

    def cell_division_rate(self, tp, cell_id, rank=1, default=None):
        """Compute the cell division rate.

        Parameters
        ----------
        tp : int
            Time-point of the parent cell.
        cell_id : int
            ID of the cell.
        rank : int, optional
            The temporal ranked distance from the parent cell to search for descendants.
            Defaults to ``1`` to get the child at the next time-point.
        rank : int, optional
            The temporal ranked distance from the parent cell to search for descendants.
            Defaults to ``1`` to get the child at the next time-point.
        default : any, optional
            The default value to returns if ``tp + rank`` does not exist.
            Defaults to ``None``.

        Returns
        -------
        float
            The cell division rate.

        See Also
        --------
        timagetk.graphs.utils.division_rate

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> print(ttg.time_interval(0, 1))
        5.0
        >>> print(len(ttg.child(0, 5)))
        2
        >>> print(ttg.cell_division_rate(0, 5))
        0.2
        >>> print(ttg.cell_division_rate(0, 5, rank=2))
        0.1
        >>> print(ttg.cell_division_rate(1, 5, rank=2))  # only 3 time-points, so no rank-2 descendants from time-point 1
        None
        """
        interval = self.time_interval(tp, tp + rank)
        if interval is None:
            return default

        if rank == 1:
            descendants = self.child(tp, cell_id)
        else:
            descendants = self.descendant(tp, cell_id, rank=rank)[rank]
        return division_rate(len(descendants), interval)

    def division_rate(self, rank=1, time_points=None, default=None):
        """Compute the division rate.

        Parameters
        ----------
        rank : int, optional
            The temporal ranked distance from the parent cell to search for descendants.
            Defaults to ``1`` to get the child at the next time-point.
        time_points : int or list, optional
            If ``None`` (default) return the list of all time-points.
            Else, filter the given list against of all valid time-points.
        default : any, optional
            The default value to returns if ``tp + rank`` does not exist.
            Defaults to ``None``.

        Returns
        -------
        dict
            The  division rate dictionary, ``{(tp, cid) : DR(tp, cid)}``.

        See Also
        --------
        timagetk.graphs.utils.division_rate

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> div_rate_r1 = ttg.division_rate()
        >>> "division_rate(1)" in ttg.list_cell_properties()
        True
        >>> div_rate_r1[(0, 5)]
        0.2
        >>> div_rate_r2 = ttg.division_rate(rank=2)
        >>> div_rate_r2[(0, 5)]
        0.1
        >>> "division_rate(2)" in ttg.list_cell_properties()
        True
        """
        time_points = self._filter_time_points(time_points, rank)
        ppty_dict = {(tp, cid): self.cell_division_rate(tp, cid, rank=rank, default=default) for tp, cid in
                     self.cell_ids(time_points)}
        unit = f"{self._time_unit}^-1"
        self.add_cell_property(f"division_rate({rank})", ppty_dict, unit=unit)
        return ppty_dict

    def list_isolated_nodes(self, time_point):
        """List the nodes isolated from the largest graph at given time-point.

        Parameters
        ----------
        time_point : int
            Select the time-point where to look for isolated nodes.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
        >>> ttg = example_temporal_tissue_graph_from_csv()
        >>> ttg.list_isolated_nodes(0)
        []
        >>> ttg._tissue_graph[0].add_cells([500])  # add an unconnected cell
        >>> ttg.list_isolated_nodes(0)
        [500]
        """
        from timagetk.features.graph import search_isolated_nodes
        return search_isolated_nodes(self._tissue_graph[time_point],
                                     background_id=self._tissue_graph[time_point]._background_id)

    def get_cell_property_unit(self, ppty):
        """Return the unit of the selected cell property.

        Parameters
        ----------
        ppty : str
            The name of the cell property.

        Returns
        -------
        str
            The cell property unit.

        Examples
        --------
        >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph
        >>> ttg = example_temporal_tissue_graph(features=['area', 'volume'], extract_dual=False)
        >>> ttg.get_cell_property_unit('volume')
        'µm³'
        >>> ttg.get_cell_property_unit('area')
        'µm²'
        >>> ttg.get_cell_property_unit('time-point')
        'h'
        """
        if ppty == "time-point":
            unit = self._time_unit
        else:
            try:
                unit = self._tissue_graph[0]._cell_properties[ppty]
            except KeyError:
                unit = ""
                log.warning(f"No property named '{ppty}' in the cell property units dictionary!")
            except ValueError:
                unit = ""
                log.warning(f"Could not obtain unit for '{ppty}' property from the cell property units dictionary!")
        return unit


def propagate_cell_identity(temporal_graph, identity, tp=0, rank="max", method="descendant"):
    """Propagate the identity, already defined for a group of cells from a given time-point, to theirs relatives according to a method.

    Parameters
    ----------
    temporal_graph : TemporalTissueGraph
        The temporal graph with the required cell `identity` and lineage.
    identity : str
        Name of a valid cell identity, *i.e.* a boolean property.
    tp : int or None
        The time-index used to get the initial list of cell with the selected `identity`.
    rank : int or "max"
        If ``rank="max"`` (default), the rank is the maximum rank, *i.e.* predict up to the last time-point.
        Else, test if the descendants (up to given `rank`) of the cell also have the same identity.
    method : {"ancestor", "descendant", "lineage", "genealogy"}
        Temporal propagation method to use.
        Beware of adapting `rank` to the selected method as "lineage" and "genealogy" takes two values.

    Returns
    -------
    list
        List of temporal cell ids `tcids` to which the ientity has been propagated to.

    Notes
    -----
    If the starting time-point is ``0``, methods "descendant", "lineage" & "genealogy" are equivalent and "ancestor" makes no sense!
    If the starting time-point is the last one, method "descendant" makes no sense!

    Examples
    --------
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
    >>> from timagetk.graphs.temporal_tissue_graph import propagate_cell_identity
    >>> ttg = example_temporal_tissue_graph_from_csv()
    >>> pred_epidermal_cids = propagate_cell_identity(ttg, 'epidermis', tp=0, method="descendant")
    >>> len(pred_epidermal_cids)
    217
    >>> pred_epidermal_cids = propagate_cell_identity(ttg, 'epidermis', tp=1, rank=["max", "max"], method="genealogy")
    >>> len(pred_epidermal_cids)
    217
    >>> from timagetk.third_party.ctrl.algorithm.metrics import sets_metrics
    >>> epidermal_cids = set(ttg.list_cells_with_identity('epidermis'))
    >>> sc = sets_metrics(epidermal_cids, pred_epidermal_cids, metric='jaccard coefficient')
    >>> print(sc)
    1.0

    """
    if method == "descendant":
        try:
            assert tp < temporal_graph.nb_time_points - 1
        except AssertionError:
            log.critical(f"Can not 'forward propagate' cell identity starting from the last time-point (t{tp}) !")
            return None
    if method == "ancestor":
        try:
            assert tp != 0
        except AssertionError:
            log.critical(f"Can not 'backward propagate' cell identity starting from the first time-point (t{tp}) !")
            return None

    init_cell_ids = temporal_graph.list_cells_with_identity(identity, rank=0, tp=tp)

    propagate_method = eval(f"temporal_graph.{method}")
    pred_cell_identity = set()
    for t_init, cid_init in init_cell_ids:
        genealogy = propagate_method(t_init, cid_init, rank)
        for _r, tcids in genealogy.items():
            if tcids == [[]]:
                continue  # skip if the genealogy is empty!
            pred_cell_identity |= set(tcids)

    return pred_cell_identity


def list_cells_in_layer(temporal_graph, layer, time_points=None):
    """Return the list of cells belonging to the selected layer(s).

    Parameters
    ----------
    temporal_graph : timagetk.graphs.TemporalTissueGraph
        The tissue graph with the cells topology.
    layer : int or list
        The cell layer to return. Can be a len-2 list indicating a range of layers.
    time_points : int or list or None, optional
        If ``None`` (default), return the list of cell layer(s) for all time-points.
        If an integer, return the list of cell layer(s) for this time-point.
        If an list, return the list of cell layer(s) for selected time-point.

    Returns
    -------
    list
        The list of cells belonging to the selected cell layer(s).

    Examples
    --------
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
    >>> from timagetk.graphs.temporal_tissue_graph import list_cells_in_layer
    >>> # Use the 'sphere' synthetic data at t0 as it is made of 3 layers of cells:
    >>> #  - one (#2) round in the center (layer 3);
    >>> #  - 13 (#3 - #15) in the layer 2;
    >>> #  - 49 (#16 - #64) in the layer 1;
    >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
    >>> layer1_t0 = list_cells_in_layer(ttg,1,0)
    >>> print(len(layer1_t0))
    49
    >>> layer1 = list_cells_in_layer(ttg, 1)
    >>> print(len(layer1))
    217
    >>> layer3 = list_cells_in_layer(ttg, 3)
    >>> print(layer3)
    [(0, 2), (1, 2), (2, 2)]
    >>> layer1_2 = list_cells_in_layer(ttg, [1, 2])
    >>> print(len(layer1_2))
    276

    """
    from timagetk.features.graph import list_cells_in_layer as static_cells_in_layer

    # Make sure specified time-points are valid:
    valid_time_points = list(range(temporal_graph.nb_time_points))
    if time_points is None:
        time_points = valid_time_points
    elif isinstance(time_points, int):
        time_points = [time_points]
    elif isinstance(time_points, list):
        time_points = list(set(time_points) & set(valid_time_points))
    else:
        log.critical(f"Could not make sense of given `time_points`: {time_points}!")

    cells = []
    for t in time_points:
        cells.extend([(t, cid) for cid in static_cells_in_layer(temporal_graph._tissue_graph[t], layer)])

    return cells


def compute_vertex_lineage(temporal_tissue_graph):
    """Compute vertex lineage using cell lineage.

    Parameters
    ----------
    temporal_tissue_graph : timagetk.graph.TemporalTissueGraph
        Temporal tissue graph with the cell lineage.

    Returns
    -------
    timagetk.graph.TemporalTissueGraph
        Temporal tissue graph augmented with the cell-vertex lineage.

    Examples
    --------
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
    >>> from timagetk.graphs.temporal_tissue_graph import compute_vertex_lineage
    >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
    >>> ttg = compute_vertex_lineage(ttg)
    >>> import networkx as nx
    >>> import matplotlib.pyplot as plt
    >>> nx.draw(ttg._vertex_temporal_graph)
    >>> plt.show()

    """
    from timagetk.util import stuple

    for t in range(1, temporal_tissue_graph.nb_time_points)[::-1]:
        vtx_mapping = {}
        # Get list of ancestor cell-vertex ids:
        ancestor_cv_ids = temporal_tissue_graph.cell_vertex_ids(tp=t - 1, tcid_indexed=False)
        # Get list of current cell-vertex ids (to relabel):
        cv_ids = temporal_tissue_graph.cell_vertex_ids(tp=t, tcid_indexed=False)
        # Get lineage dictionary
        mapping = temporal_tissue_graph.lineage(t - 1, t, time_indexed=False)
        mapping = {descendant: ancestor for ancestor, descendants in mapping.items() for descendant in descendants}
        mapping.update(
            {temporal_tissue_graph._tissue_graph[t].background: temporal_tissue_graph._tissue_graph[t - 1].background})
        for _, cv_id in cv_ids:
            relabelled_cv_id = [mapping[c_id] if c_id in mapping else None for c_id in cv_id]
            if None in relabelled_cv_id:
                continue
            relabelled_cv_id = stuple(relabelled_cv_id)
            if (t - 1, relabelled_cv_id) in ancestor_cv_ids:
                vtx_mapping.update({relabelled_cv_id: cv_id})

        temporal_tissue_graph.add_vertex_lineage(t - 1, vtx_mapping)

    return temporal_tissue_graph
