# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Graph-Based Cell Property Analysis Module

This module provides utility functions to analyze and retrieve cell property
values grouped by various criteria from tissue graphs.

Key Features
------------
- **Retrieve Cell Properties by Groups**:
  Extract cell property values grouped by any defined group in `TissueGraph`
  or `TemporalTissueGraph`.
- **Support for Subgroup Analysis**:
  Provides additional capabilities to fetch data grouped by both primary and
  secondary subgroups (e.g., groups and subgroups).
- **Customizable Defaults**:
  Offers filtering options, custom default property values, and subset selections
  for more precise data retrieval.
- **Flexible Parameterization**:
  Easily specify groups, subgroups, or cell properties with options to handle
  undefined or missing properties gracefully.
- **Integration with Clustering**:
  Compatible with clustering results for advanced group-based cell analysis.

Usage Examples
--------------
>>> # Example - Retrieve and group cell properties by layers in a tissue graph:
>>> from timagetk.graphs.graph_analysis import cell_property_by_group
>>> from timagetk.graphs.tissue_graph import example_tissue_graph
>>> tg = example_tissue_graph('sphere', time_point=0, features=['layers', 'volume'])
>>> grouped_properties = cell_property_by_group(tg, 'layers', 'volume', group_id_subset=[1, 2])
>>> print(grouped_properties)

>>> # Example - Retrieve and group cell properties by time-points and clusters:
>>> from timagetk.graphs.graph_analysis import cell_property_by_subgroups
>>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph
>>> ttg = example_temporal_tissue_graph('sphere', features=['layers', 'volume'])
>>> subgrouped_properties = cell_property_by_subgroups(ttg, 'time-index', 'layers', 'volume', subgroup_id_subset=[1, 2])
>>> print(subgrouped_properties)

This module is ideal for applications in computational biology, tissue modeling,
and spatial graph-based data analysis, providing a systematic and efficient
approach to interacting with cell property data.
"""

import numpy as np

DEFAULT_PPTY_VAL = np.nan

def cell_property_by_group(graph, group_name, property_name, **kwargs):
    """Get a dictionary of cell property values by groups.

    Parameters
    ----------
    graph : timagetk.graphs.TissueGraph or timagetk.graphs.TemporalTissueGraph
        The (temporal) tissue graph with the `group_name` & `property_name` as "cell property".
    group_name : str
        The name of the cell group, defined as "cell property" in the `graph`, to use to group cell property values.
    property_name : str
        The name of the cell property, defined in the `graph`, to group.

    Other Parameters
    ----------------
    cids : list or set
        The list of cell ids to get the cell property values and groups for.
    group_id_subset : list or set
        A list or set of group ids to retain in the returned dictionary.
        If undefined, all the detected group ids are returned.
    default_group_id : any
        The default value for the cell group, ``None`` by default.
    default_ppty_val : any
        The default value for the cell property, ``np.nan`` by default.
    only_defined_ppty : bool
        If ``True``, return the value of a cell property if it is defined, _i.e._ not equal to `default_ppty_val`.
        Else, return the values of all cells defined in the `graph` and set it to `default_ppty_val` if not defined for selected `property_name`.

    Returns
    -------
    dict
        A group id indexed dictionary of cell property values.

    Examples
    --------
    >>> import matplotlib.pyplot as plt
    >>> from timagetk.graphs.graph_analysis import cell_property_by_group
    >>> from timagetk.graphs.tissue_graph import example_tissue_graph
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph
    >>> # Example 1 - Group cell volume by layers on a TissueGraph, for the first two layers:
    >>> tg = example_tissue_graph('sphere', time_point=0, features=['layers', 'volume'], extract_dual=False)
    >>> ppty_by_g = cell_property_by_group(tg, 'layers', 'volume', group_id_subset=[1, 2])
    >>> plt.boxplot(list(ppty_by_g.values()), labels=list(map(str, ppty_by_g.keys())))
    >>> # Example 2 - Group cell volume by layers on a TemporalTissueGraph:
    >>> ttg = example_temporal_tissue_graph('sphere', features=['layers', 'volume'], extract_dual=False)
    >>> ppty_by_g = cell_property_by_group(ttg, 'layers', 'volume')
    >>> plt.boxplot(list(ppty_by_g.values()), labels=list(map(str, ppty_by_g.keys())))
    >>> # Example 3 - Group cell volume by clusters:
    >>> from timagetk.components.clustering import example_clusterer
    >>> properties = ['volume', "log_relative_value('volume',1)"]  # selected cell properties
    >>> weights = [0.5, 0.5]  # weights vector
    >>> clusterer = example_clusterer()
    >>> clusterer.assemble_matrix(properties, weights)  # create the pairwise-distance matrix
    >>> clusterer.compute_clustering(method='ward', n_clusters=4)  # compute the cell clustering
    >>> graph = clusterer.graph  # get the associated TemporalTissueGraph
    >>> clustering_name = clusterer.clustering_name()
    >>> ppty_by_g = cell_property_by_group(graph, clustering_name, 'volume')
    >>> plt.boxplot(list(ppty_by_g.values()), labels=list(map(str, ppty_by_g.keys())))

    """
    cids = kwargs.get('cids', None)
    default_gid = kwargs.get('default_group_id', None)
    default_ppty_val = kwargs.get('default_ppty_val', DEFAULT_PPTY_VAL)
    only_defined_ppty = kwargs.get('only_defined_ppty', False)
    gid_set = kwargs.get('group_id_subset', None)

    # Get the cell ids indexed dictionary of cell property values:
    ppty_dict = graph.cell_property_dict(property_name, cids, default=default_ppty_val, only_defined=only_defined_ppty)
    if cids is None:
        cids = set(ppty_dict.keys())
    # Get the cell ids indexed dictionary of cell groups:
    group_dict = graph.cell_property_dict(group_name, cids, default=default_gid, only_defined=False)
    # Get the associated set of group ids:
    group_ids = set(group_dict.values())

    # Get the values by group ids:
    ppty_by_g = {gid: [] for gid in group_ids}
    for tcid, val in ppty_dict.items():
        gid = group_dict.get(tcid, default_gid)
        ppty_by_g[gid].append(val)

    # Filter the group indexed dictionary with the subset, if any:
    if gid_set is not None:
        common_gids = set(group_ids) & set(gid_set)
        ppty_by_g = {gid: ppty_by_g[gid] for gid in common_gids}

    return ppty_by_g


def cell_property_by_subgroups(graph, group_name, subgroup_name, property_name, **kwargs):
    """Get a dictionary of cell property values by groups & subgroups.

    Parameters
    ----------
    graph : timagetk.graphs.TemporalTissueGraph
        The (temporal) tissue graph with the `group_name`, `subgroup_name` & `property_name` as "cell property".
    group_name : str
        The name of the cell group, defined as "cell property" in the `graph`, to use to group cell property values.
    subgroup_name : str
        The name of the cell subgroup, defined as "cell property" in the `graph`, to use to group cell property values.
    property_name : str
        The name of the cell property, defined in the `graph`, to group.

    Other Parameters
    ----------------
    group_id_subset : list or set
        A list or set of group ids to retain in the returned dictionary.
        If undefined, all the detected group ids are returned.
    subgroup_id_subset : list or set
        A list or set of subgroup ids to retain in the returned dictionary.
        If undefined, all the detected subgroup ids are returned.
    default_group_id : any
        The default value for the cell group, ``None`` by default.
    default_subgroup_id : any
        The default value for the cell subgroup, ``None`` by default.
    default_ppty_val : any
        The default value for the cell property, ``np.nan`` by default.
    only_defined_ppty : bool
        If ``True`` (default), return the value of a cell property if it is defined, _i.e._ not equal to `default_ppty_val`.
        Else, return the values of all cells defined in the `graph` and set it to `default_ppty_val` if not defined for selected `property_name`.

    Returns
    -------
    dict
        A group id & subgroup id indexed dictionary of cell property values.

    Examples
    --------
    >>> import matplotlib.pyplot as plt
    >>> from timagetk.graphs.graph_analysis import cell_property_by_subgroups
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph
    >>> # Example 1 - Group cell volume by time-points (all) and layers (first two):
    >>> ttg = example_temporal_tissue_graph('sphere', features=['layers', 'volume'], extract_dual=False)
    >>> ppty_by_sg = cell_property_by_subgroups(ttg, 'time-index', 'layers', 'volume', subgroup_id_subset=[1, 2])
    >>> fig, ax = plt.subplots(ncols=ttg.nb_time_points)
    >>> [axi.boxplot(list(ppty_by_sg[i].values()), labels=list(map(str, ppty_by_sg[i].keys()))) for i, axi in enumerate(ax)]
    >>> # Example 2 - Group cell volume by time-points and clusters:
    >>> from timagetk.components.clustering import example_clusterer
    >>> properties = ['volume', "log_relative_value('volume',1)"]  # selected cell properties
    >>> weights = [0.5, 0.5]  # weights vector
    >>> clusterer = example_clusterer()
    >>> clusterer.assemble_matrix(properties, weights)  # create the pairwise-distance matrix
    >>> clusterer.compute_clustering(method='ward', n_clusters=4)  # compute the cell clustering
    >>> graph = clusterer.graph  # get the associated TemporalTissueGraph
    >>> clustering_name = clusterer.clustering_name()
    >>> ppty_by_sg = cell_property_by_subgroups(graph, 'time-index', clustering_name, 'volume')
    >>> fig, ax = plt.subplots(ncols=ttg.nb_time_points)
    >>> [axi.boxplot(list(ppty_by_sg[i].values())) for i, axi in enumerate(ax)]

    """
    default_gid = kwargs.get('default_group_id', None)
    default_sgid = kwargs.get('default_subgroup_id', None)
    default_ppty_val = kwargs.get('default_ppty_val', np.nan)
    only_defined_ppty = kwargs.get('only_defined_ppty', False)
    gid_set = kwargs.get('group_id_subset', None)
    sgid_set = kwargs.get('subgroup_id_subset', None)

    # Get the cell ids indexed dictionary of cell property values:
    ppty_dict = graph.cell_property_dict(property_name, default=default_ppty_val, only_defined=only_defined_ppty)
    cids = set(ppty_dict.keys())
    # Get the cell ids indexed dictionary of cell groups:
    group_dict = graph.cell_property_dict(group_name, cids, default=default_gid, only_defined=False)
    # Get the associated set of group ids:
    group_ids = set(group_dict.values())
    # Get the cell ids indexed dictionary of cell subgroups:
    subgroup_dict = graph.cell_property_dict(subgroup_name, cids, default=default_sgid, only_defined=False)
    # Get the associated set of subgroup ids:
    subgroup_ids = set(subgroup_dict.values())

    # Get the values by subgroups & group ids:
    ppty_by_g = {gid: {sgid: [] for sgid in subgroup_ids} for gid in group_ids}
    for tcid, val in ppty_dict.items():
        gid = group_dict.get(tcid, default_gid)
        sgid = subgroup_dict.get(tcid, default_sgid)
        ppty_by_g[gid][sgid].append(val)

    # Filter the group indexed dictionary with the subset, if any:
    if gid_set is not None:
        common_gids = set(group_ids) & set(gid_set)
        ppty_by_g = {gid: ppty_by_g[gid] for gid in common_gids}

    # Filter the group indexed dictionary with the subset, if any:
    if sgid_set is not None:
        common_sgids = set(subgroup_ids) & set(sgid_set)
        ppty_by_g = {gid: {sgid: ppty_g[sgid] for sgid in common_sgids} for gid, ppty_g in ppty_by_g.items()}

    return ppty_by_g
