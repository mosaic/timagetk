#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""This module regroups various array utilities."""

import sys

import numpy as np

from timagetk import MultiChannelImage
from timagetk.bin.logger import get_logger
from timagetk.components.labelled_image import LabelledImage
from timagetk.components.spatial_image import DEFAULT_SIZE_UNIT
from timagetk.components.spatial_image import DEFAULT_VXS_2D
from timagetk.components.spatial_image import DEFAULT_VXS_3D
from timagetk.components.spatial_image import SpatialImage
from timagetk.io.image import default_image_attributes
from timagetk.tasks.decorators import singlechannel_wrapper
from timagetk.util import now

log = get_logger(__name__)


def random_array(shape, dtype="uint8"):
    """Generate a random array.

    Parameters
    ----------
    shape : list[int]
        Specifies the shape of the array to create.
    dtype : {"uint8", "uint16", "float32", "float", "double"}, optional
        Specifies the numerical type of the values, with "uint8" as the default.

    Returns
    -------
    numpy.ndarray
        The generated random array.

    Notes
    -----
    The NumPy type `float32` corresponds to the `float` type in C.
    The NumPy type `float64` corresponds to the `double` type in C.
    The NumPy type `float` is equivalent to `float64`.

    See Also
    --------
    numpy.random.randint
    numpy.random.random_sample

    References
    ----------
    NumPy array types and conversions between types `documentation <https://docs.scipy.org/doc/numpy/user/basics.types.html#array-types-and-conversions-between-types>`_

    Examples
    --------
    >>> from timagetk.array_util import random_array
    >>> # Example 1 - Generates a random 2D array with 8 bits signed integers:
    >>> rand_image = random_array([7, 5], dtype='uint8')
    >>> print(rand_image.shape)
    (7, 5)
    >>> print(rand_image.dtype)
    uint8
    >>> print(rand_image.nbytes)  # memory consumed by the elements of the array
    35
    >>> # Example 2 - Generates a random 2D array with 64 bits floats:
    >>> rand_image = random_array([7, 5], dtype='float')
    >>> print(rand_image.shape)
    (7, 5)
    >>> print(rand_image.dtype)
    float64
    >>> print(rand_image.nbytes)  # memory consumed by the elements of the array
    280
    >>> # Example 3 - Generates a random 3D array with 8 bits signed integers:
    >>> rand_image = random_array([7, 5, 3], dtype='uint8')
    >>> print(rand_image.shape)
    (7, 5, 3)
    >>> print(rand_image.dtype)
    uint8
    >>> print(rand_image.nbytes)  # memory consumed by the elements of the array
    105
    >>> # Example 4 - Generates a random 3D array with 64 bits floats:
    >>> rand_image = random_array([7, 5, 3], dtype='double')
    >>> print(rand_image.shape)
    (7, 5, 3)
    >>> print(rand_image.dtype)
    float64
    >>> print(rand_image.nbytes)  # memory consumed by the elements of the array
    840

    """
    # Convert given str to NumPy type:
    if isinstance(dtype, str):
        dtype = np.dtype(dtype)

    # Create the array given the subtype: integer or float
    rng = np.random.default_rng(1337)
    if np.issubdtype(dtype, np.integer):
        mini = np.iinfo(dtype).min
        maxi = np.iinfo(dtype).max
        arr = rng.integers(low=mini, high=maxi, size=shape, dtype=dtype)
    else:
        arr = rng.random(shape).astype(dtype)

    return arr


def random_spatial_image(size, dtype='uint8', **kwargs):
    """Generate a random `SpatialImage`.

    Parameters
    ----------
    size : iterable
        The size of the image to generate. The order is ZYX if the image is 3D, and YX if it is 2D.
    dtype : str or numpy.dtype
        A valid data type for the array. This is checked against `AVAIL_TYPES`. The default is `'uint8'`.

    Other Parameters
    ----------------
    axes_order : str
        Specifies the physical axes order for the array. The default is `default_axes_order(array)`.
    origin : list[int]
        The coordinates of the origin for the array. The default is `default_origin(array)`.
    unit : float
        The unit associated with the voxel sizes in the array. The default is `DEFAULT_SIZE_UNIT`.
    voxelsize : list[float]
        The voxel sizes to be used with the array. The default is `default_voxelsize(array)`.
    metadata : dict
        A dictionary containing metadata for the array. The default is an empty dictionary `{}`.

    Returns
    -------
    timagetk.SpatialImage
        A randomly generated image object.

    See Also
    --------
    timagetk.array_util.random_array
    timagetk.io.image.default_image_attributes

    Examples
    --------
    >>> from timagetk.array_util import random_spatial_image
    >>> # Example 1 - Generates a random 3D SpatialImage:
    >>> img = random_spatial_image((3, 5, 5), dtype='uint8')
    >>> print(img.shape)  # ZYX sorted
    (3, 5, 5)
    >>> # Example 2 - Generates a random 2D SpatialImage:
    >>> img = random_spatial_image((3, 5), dtype='uint8')
    >>> print(img.shape)  # YX sorted
    (3, 5)
    >>> print(img)
    SpatialImage object with following metadata:
       - shape: (3, 5)
       - ndim: 2
       - dtype: uint8
       - axes_order: YX
       - voxelsize: [1.0, 1.0]
       - unit: 1e-06
       - origin: [0, 0]
       - extent: [2.0, 4.0]
       - acquisition_date: ...

    """
    img = random_array(size, dtype)
    img_attr = default_image_attributes(img, log_undef=log.debug, **kwargs)
    img_attr['acquisition_date'] = now()
    return SpatialImage(img, **img_attr)


def handmade_2d():
    """Creates a 2D NumPy array with structured intensity values.

    Returns
    -------
    numpy.ndarray
        The handmade 2D array.

    Notes
    -----
    Returns a ``uint8`` YX-sorted array of shape ``(11, 11)``, with:
       - ``255`` on the diagonal
       - ``200`` in the middle column (X)
       - ``100`` in the middle row (Y)

    Example
    -------
    >>> from timagetk.array_util import handmade_2d
    >>> arr = handmade_2d()
    >>> print(arr.shape)
    (11, 11)
    >>> # Graphical representation of the 2D array:
    >>> import matplotlib.pyplot as plt
    >>> fig, ax = plt.subplots()
    >>> im_ax = ax.imshow(handmade_2d(), cmap='viridis')
    >>> _ = ax.set_title('Dummy 2D array')
    >>> _ = ax.set_xlabel('X-axis')
    >>> _ = ax.set_ylabel('Y-axis')
    >>> cbar_ax = plt.colorbar(im_ax)

    """
    xy_shape = 11
    mid_xy = xy_shape // 2
    # - Create a 2D array with a diagonal at max value (255), and 0 elsewhere:
    arr_2d = np.diag(np.repeat(255, xy_shape))
    # - For all rows (Y) at middle column (X), replace by 200:
    arr_2d[:, mid_xy] = 200
    # - For all columns (X) at middle row (Y), replace by 100:
    arr_2d[mid_xy, :] = 100
    return arr_2d.astype('uint8')


def handmade_3d():
    """Creates a 3D NumPy array with structured intensity values.

    Returns
    -------
    numpy.ndarray
        The handmade 3D array.

    Notes
    -----
    Returns an ``uint8`` ZYX-sorted array with the shape of `(5, 11, 11)`,
    created by z-stacking the 2D array from `handmade_2d()`.

    See Also
    --------
    timagetk.array_util.handmade_2d

    Example
    -------
    >>> from timagetk.array_util import handmade_3d
    >>> arr = handmade_3d()
    >>> print(arr.shape)
    (5, 11, 11)
    >>> # Graphical representation of the 2nd slice of the 3D array:
    >>> import matplotlib.pyplot as plt
    >>> fig, ax = plt.subplots()
    >>> im_ax = ax.imshow(handmade_3d()[2, :, :], cmap='viridis')
    >>> _ = ax.set_title('Dummy 3D array - slice 2/5')
    >>> _ = ax.set_xlabel('X-axis')
    >>> _ = ax.set_ylabel('Y-axis')
    >>> cbar_ax = plt.colorbar(im_ax)

    """
    arr_2d = handmade_2d()
    z_shape = 5
    # - Repeat this 2D array `z_shape` times along new dimension to get a 3D array:
    arr_3d = np.repeat(arr_2d[np.newaxis, :, :], z_shape, axis=0)
    return arr_3d


def dummy_spatial_image_2D(voxelsize=DEFAULT_VXS_2D, unit=DEFAULT_SIZE_UNIT):
    """Create a dummy 2D SpatialImage.

    Parameters
    ----------
    voxelsize : tuple of float, optional
        A tuple of two floats representing the voxel size, sorted in YX order.
        Defaults to ``DEFAULT_VXS_2D``.
    unit : int or float, optional
        The size unit, in meters, following the International System of Units.
        Defaults to ``DEFAULT_SIZE_UNIT``.

    Returns
    -------
    timagetk.SpatialImage
        A dummy 2D intensity image.

    See Also
    --------
    timagetk.array_util.handmade_2d
    timagetk.components.spatial_image.DEFAULT_VXS_2D
    timagetk.components.spatial_image.DEFAULT_SIZE_UNIT

    Example
    -------
    >>> from timagetk.array_util import dummy_spatial_image_2D
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> im = dummy_spatial_image_2D()
    >>> print(im)
    SpatialImage object with following metadata:
       - shape: (11, 11)
       - ndim: 2
       - dtype: uint8
       - axes_order: YX
       - voxelsize: [1.0, 1.0]
       - unit: 1e-06
       - origin: [0, 0]
       - extent: [10.0, 10.0]
       - acquisition_date: ...
    >>> # Graphical representation of the 2D intensity image:
    >>> v = fig = grayscale_imshow(im, title="Dummy 2D intensity image")

    """
    img = handmade_2d()
    img_attr = default_image_attributes(img, log_undef=log.debug, voxelsize=voxelsize, unit=unit)
    img_attr['acquisition_date'] = now()
    return SpatialImage(img, filename='dummy_intensity_2d', **img_attr)


def dummy_spatial_image_3D(voxelsize=DEFAULT_VXS_3D, unit=DEFAULT_SIZE_UNIT):
    """Create a dummy 3D SpatialImage.

    Parameters
    ----------
    voxelsize : tuple of float, optional
        A tuple of three floats representing the voxel size, sorted in ZYX order.
        Defaults to ``DEFAULT_VXS_3D``.
    unit : int or float, optional
        The size unit, in meters, following the International System of Units.
        Defaults to ``DEFAULT_SIZE_UNIT``.

    Returns
    -------
    timagetk.SpatialImage
        A dummy 3D intensity image.

    See Also
    --------
    timagetk.array_util.handmade_3d
    timagetk.components.spatial_image.DEFAULT_VXS_2D
    timagetk.components.spatial_image.DEFAULT_SIZE_UNIT

    Example
    -------
    >>> from timagetk.array_util import dummy_spatial_image_3D
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> from timagetk.visu.stack import stack_browser
    >>> im = dummy_spatial_image_3D()
    >>> v = fig = grayscale_imshow(im, slice_id=2, axis='z', title="Dummy 3D intensity image")

    """
    img = handmade_3d()
    img_attr = default_image_attributes(img, log_undef=log.debug, voxelsize=voxelsize, unit=unit)
    img_attr['acquisition_date'] = now()
    return SpatialImage(img, filename='dummy_intensity_3d', **img_attr)


#: Handmade labelled 2D array mimicking a dense cellular tissue.
DUMMY_SEG_2D = np.array([
    [2, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5],
    [2, 2, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5],
    [2, 2, 2, 4, 4, 4, 4, 4, 4, 4, 5, 5],
    [2, 2, 2, 2, 4, 4, 4, 4, 4, 5, 5, 5],
    [2, 2, 2, 2, 3, 3, 3, 3, 3, 5, 5, 5],
    [2, 2, 2, 2, 3, 3, 3, 3, 3, 5, 5, 5],
    [2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 5, 5],
    [2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 5, 5],
    [2, 2, 2, 7, 7, 3, 3, 3, 3, 3, 6, 6],
    [2, 2, 7, 7, 7, 3, 3, 3, 3, 6, 6, 6],
    [2, 7, 7, 7, 7, 7, 3, 3, 6, 6, 6, 6],
    [7, 7, 7, 7, 7, 7, 7, 6, 6, 6, 6, 6],
    [7, 7, 7, 7, 7, 7, 7, 6, 6, 6, 6, 6],
], dtype='uint8')  # YX shape: (13, 12) (rows, columns)

#: Handmade labelled 3D array mimicking a dense cellular tissue with a background (1).
DUMMY_SEG_3D = np.array([
    np.ones_like(DUMMY_SEG_2D),
    DUMMY_SEG_2D,
    DUMMY_SEG_2D,
    DUMMY_SEG_2D,
    DUMMY_SEG_2D,
])


def dummy_labelled_image_2D(voxelsize=DEFAULT_VXS_2D, unit=DEFAULT_SIZE_UNIT):
    """Create a dummy 2D LabelledImage with a YX shape of 13x12 (rows, columns).

    Parameters
    ----------
    voxelsize : tuple of float, optional
        A tuple of two floats representing the voxel size, sorted in YX order.
        Defaults to ``DEFAULT_VXS_2D``.
    unit : int or float, optional
        The size unit, in meters, following the International System of Units.
        Defaults to ``DEFAULT_SIZE_UNIT``.

    Returns
    -------
    timagetk.LabelledImage
        A dummy 2D labelled image.

    See Also
    --------
    timagetk.array_util.DUMMY_SEG_2D
    timagetk.components.spatial_image.DEFAULT_VXS_2D
    timagetk.components.spatial_image.DEFAULT_SIZE_UNIT

    Example
    -------
    >>> from timagetk.array_util import dummy_labelled_image_2D
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> from timagetk.visu.util import greedy_colormap
    >>> im = dummy_labelled_image_2D((0.5, 0.5))  # YX sorted voxel-size values
    >>> print(im)
    LabelledImage object with following metadata:
       - shape: (13, 12)
       - ndim: 2
       - dtype: uint8
       - axes_order: YX
       - voxelsize: [0.5, 0.5]
       - unit: 1e-06
       - origin: [0, 0]
       - extent: [6.0, 5.5]
       - acquisition_date: ...
       - not_a_label: 0
    >>> v = fig = grayscale_imshow(im, title="Dummy 2D labelled image", cmap='glasbey', val_range="auto")

    """
    img = DUMMY_SEG_2D
    img_attr = default_image_attributes(img, log_undef=log.debug, voxelsize=voxelsize, unit=unit)
    img_attr['acquisition_date'] = now()
    return LabelledImage(img, not_a_label=0, filename='dummy_labelled_2d', **img_attr)


def dummy_labelled_image_3D(voxelsize=DEFAULT_VXS_3D, unit=DEFAULT_SIZE_UNIT):
    """Create a dummy 3D LabelledImage with a ZYX shape of 5x13x12 (planes, rows, columns).

    Parameters
    ----------
    voxelsize : tuple of float, optional
        A tuple of three floats representing the voxel size, sorted in ZYX order.
        Defaults to ``DEFAULT_VXS_2D``.
    unit : int or float, optional
        The size unit, in meters, following the International System of Units.
        Defaults to ``DEFAULT_SIZE_UNIT``.

    Returns
    -------
    timagetk.LabelledImage
        A dummy 3D labelled image.

    Notes
    -----
    Concatenate along the z-axis: a first layer of ``1``, then 4 repeats of ``DUMMY_SEG_2D``.

    See Also
    --------
    timagetk.array_util.DUMMY_SEG_3D
    timagetk.components.spatial_image.DEFAULT_VXS_3D
    timagetk.components.spatial_image.DEFAULT_SIZE_UNIT

    Example
    -------
    >>> from timagetk.array_util import dummy_labelled_image_3D
    >>> from timagetk.visu.stack import stack_browser
    >>> im = dummy_labelled_image_3D((0.2, 0.5, 0.5))  # ZYX sorted voxel-sizes
    >>> print(im)
    LabelledImage object with following metadata:
       - shape: (5, 13, 12)
       - ndim: 3
       - dtype: uint8
       - axes_order: ZYX
       - voxelsize: [0.2, 0.5, 0.5]
       - unit: 1e-06
       - origin: [0, 0, 0]
       - extent: [0.8, 6.0, 5.5]
       - acquisition_date: ...
       - not_a_label: 0
    >>> b = stack_browser(im, cmap='viridis', val_range='auto')  # all slices, except the first one, should be the same

    """
    img = DUMMY_SEG_3D
    img_attr = default_image_attributes(img, log_undef=log.debug, voxelsize=voxelsize, unit=unit)
    img_attr['acquisition_date'] = now()
    return LabelledImage(img, not_a_label=0, filename='dummy_labelled_3d', **img_attr)


def dummy_multichannel_image(voxelsize=DEFAULT_VXS_3D, unit=DEFAULT_SIZE_UNIT):
    """Create a dummy 3D MultiChannelImage.

    Parameters
    ----------
    voxelsize : tuple of float, optional
        A tuple of three floats representing the voxel size, sorted in ZYX order.
        Defaults to ``DEFAULT_VXS_2D``.
    unit : int or float, optional
        The size unit, in meters, following the International System of Units.
        Defaults to ``DEFAULT_SIZE_UNIT``.

    Returns
    -------
    timagetk.MultiChannelImage
        A dummy multichannel image.

    See Also
    --------
    timagetk.array_util.handmade_3d
    timagetk.array_util.random_spatial_image
    timagetk.components.spatial_image.DEFAULT_VXS_3D
    timagetk.components.spatial_image.DEFAULT_SIZE_UNIT

    Example
    -------
    >>> from timagetk.array_util import dummy_multichannel_image
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> from timagetk.visu.stack import stack_browser
    >>> im = dummy_multichannel_image()
    >>> print(im)
    MultiChannelImage 'dummy_multichannel' has 2 channels: CH1, CH2
     * CH1: SpatialImage object with following metadata:
       - shape: (5, 11, 11)
       - ndim: 3
       - dtype: uint8
       - axes_order: ZYX
       - voxelsize: [1.0, 1.0, 1.0]
       - unit: 1e-06
       - origin: [0, 0, 0]
       - extent: [4.0, 10.0, 10.0]
       - acquisition_date: ...
     * CH2: SpatialImage object with following metadata:
       - shape: (5, 11, 11)
       - ndim: 3
       - dtype: uint8
       - axes_order: ZYX
       - voxelsize: [1.0, 1.0, 1.0]
       - unit: 1e-06
       - origin: [0, 0, 0]
       - extent: [4.0, 10.0, 10.0]
       - acquisition_date: ...
    >>> v = fig = grayscale_imshow(im, slice_id=2, axis='z', title="Dummy 3D multichannel image")

    """
    ch1 = dummy_spatial_image_3D(voxelsize=voxelsize, unit=unit)
    ch2 = random_spatial_image(ch1.shape, voxelsize=voxelsize, unit=unit)
    return MultiChannelImage([ch1, ch2], channel_names=["CH1", "CH2"], filename="dummy_multichannel")


@singlechannel_wrapper
def intensity_threshold_bimodal(image):
    """Estimate the intensity threshold assuming a bimodal distribution.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        The intensity image to be thresholded.

    Other Parameters
    ----------------
    channel : str
        If a `MultiChannelImage` is used as the input `image`, specify the channel that this algorithm should use.

    Returns
    -------
    int
        The estimated intensity threshold.

    See Also
    --------
    skimage.filters.threshold_minimum

    Examples
    --------
    >>> from timagetk.array_util import intensity_threshold_bimodal
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> # - Get 'p58' shared intensity image:
    >>> img = shared_data('flower_confocal', 0)
    >>> th_img = intensity_threshold_bimodal(img)
    >>> print(f"Intensity threshold based on bi-modal distribution search: {th_img}")
    Intensity threshold based on bi-modal distribution search: 27

    """
    from skimage.filters import threshold_minimum
    from timagetk.components.spatial_image import SpatialImage
    if isinstance(image, SpatialImage):
        return threshold_minimum(image.get_array())
    else:
        return threshold_minimum(image)


@singlechannel_wrapper
def intensity_threshold_otsu(image):
    """Estimate the intensity threshold based on Otsu's method.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        The intensity image to be thresholded.

    Other Parameters
    ----------------
    channel : str
        If a `MultiChannelImage` is used as the input `image`, specify the channel that this algorithm should use.

    Returns
    -------
    int
        The estimated intensity threshold.

    See Also
    --------
    skimage.filters.threshold_otsu

    Examples
    --------
    >>> from timagetk.array_util import intensity_threshold_otsu
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> # - Get 'p58' shared intensity image:
    >>> img = shared_data('flower_confocal', 0)
    >>> th_img = intensity_threshold_otsu(img)
    >>> print(f"Intensity threshold based on Otsu's method: {th_img}")
    Intensity threshold based on Otsu's method: 59

    """
    from skimage.filters import threshold_otsu
    from timagetk.components.spatial_image import SpatialImage
    if isinstance(image, SpatialImage):
        return threshold_otsu(image.get_array())
    else:
        return threshold_otsu(image)


@singlechannel_wrapper
def intensity_threshold_li(image):
    """Estimate the intensity threshold by Li's iterative Minimum Cross Entropy method.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        The intensity image to be thresholded.

    Other Parameters
    ----------------
    channel : str
        If a `MultiChannelImage` is used as the input `image`, specify the channel that this algorithm should use.

    Returns
    -------
    float
        The estimated intensity threshold.

    See Also
    --------
    skimage.filters.threshold_li

    Example
    -------
    >>> from timagetk.array_util import intensity_threshold_li
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> # - Get 'p58' shared intensity image:
    >>> img = shared_data('flower_confocal', 0)
    >>> th_img = intensity_threshold_li(img)
    >>> print(f"Intensity threshold based on Li's method: {th_img}")
    Intensity threshold based on Li's method: 34.336600328173354

    """
    from skimage.filters import threshold_li
    from timagetk.components.spatial_image import SpatialImage
    if isinstance(image, SpatialImage):
        return threshold_li(image.get_array())
    else:
        return threshold_li(image)


#: The default value for the percentile used in ``intensity_threshold_percentile``.
DEFAULT_PERCENTILE = 75


@singlechannel_wrapper
def intensity_threshold_percentile(image, q=DEFAULT_PERCENTILE):
    """Estimate the intensity threshold by percentile method.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        The intensity image to be thresholded.
    q : int or float, optional
        The percentile to compute, which must be between 0 and 100 inclusive.

    Other Parameters
    ----------------
    channel : str
        If a `MultiChannelImage` is used as the input `image`, specify the channel that this algorithm should use.

    Returns
    -------
    float
        The estimated intensity threshold.

    See Also
    --------
    numpy.percentile
    timagetk.array_util.DEFAULT_PERCENTILE

    Example
    -------
    >>> from timagetk.array_util import intensity_threshold_percentile
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> # - Get 'p58' shared intensity image:
    >>> img = shared_data('flower_confocal', 0)
    >>> q = 75  # value for the percentile
    >>> th_img = intensity_threshold_percentile(img, q=q)
    >>> print(f"Intensity threshold based on the {q}-th percentile: {th_img}")
    Intensity threshold based on the 75-th percentile: 65.0

    """
    from timagetk.components.spatial_image import SpatialImage
    if isinstance(image, SpatialImage):
        return np.percentile((image.get_array()), q)
    else:
        return np.percentile((image), q)


@singlechannel_wrapper
def guess_intensity_threshold(image, **kwargs):
    """Attempt to automatically determine the appropriate threshold to distinguish between signal and noise.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        The intensity image used for guessing the threshold.

    Other Parameters
    ----------------
    channel : str
        If a `MultiChannelImage` is provided as the `image` input, specify the channel to use with this algorithm.

    Returns
    -------
    float
        The estimated intensity threshold.

    Examples
    --------
    >>> from timagetk.array_util import guess_intensity_threshold
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> # - Get 'p58' shared intensity image:
    >>> img = shared_data("flower_confocal", 0)
    >>> th_img = guess_intensity_threshold(img)
    >>> print(f"Detected intensity threshold: {th_img}")
    Detected intensity threshold: 35.1

    """
    from timagetk.bin.logger import get_dummy_logger
    logger = kwargs.pop('logger', get_dummy_logger(sys._getframe().f_code.co_name))

    q_th = intensity_threshold_percentile(image, **kwargs)
    mid_zsl = image.get_shape('z') // 2
    try:
        threshold = 1.3 * intensity_threshold_bimodal(image.get_slice(mid_zsl, 'z'), **kwargs)
        assert threshold < image.max() and threshold < 2 * q_th
    except:
        logger.warning("Could not detect intensity threshold with 'bimodal search'...")
        threshold = q_th
        logger.info(f"Using value obtained from 'percentile search' ({threshold})...")

    logger.debug(f"Automatically estimated intensity threshold value: {threshold}")
    return threshold


@singlechannel_wrapper
def guess_image_orientation(image, **kwargs):
    """Determine the orientation of an image based on the distribution of intensity levels.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.MultiChannelImage
        The intensity image used to determine the orientation.

    Other Parameters
    ----------------
    channel : str
        If a `MultiChannelImage` is provided as the input `image`, specify the channel to use with this algorithm.

    Returns
    -------
    {-1, 1}
        Returns `-1` if the z-axis begins at the bottom of the stack and extends upward.
        Returns `1` if the z-axis begins at the top of the stack and extends downward.

    Examples
    --------
    >>> from timagetk.array_util import guess_image_orientation
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> img = shared_data('flower_confocal', 0)
    >>> guess_image_orientation(img)
    1

    """
    from timagetk.bin.logger import get_dummy_logger
    logger = kwargs.pop('logger', get_dummy_logger(sys._getframe().f_code.co_name))

    if image.is2D():
        logger.error("Image orientation is intended to be used on 3D images!")
        return None

    orientation = int(1 - 2 * (np.mean(image.get_slice(0, 'z')) > np.mean(image.get_slice(-1, 'z'))))

    logger.debug(f"Automatically estimated image orientation: {orientation}")
    return orientation


@singlechannel_wrapper
def to_uint8(image, **kwargs):
    """Converts an image to 8-bit unsigned integer format.

    Parameters
    ----------
    image :
        Input image object that needs to be converted. The image should be
        compatible with the functions from `timagetk` and be able to provide its
        data array when `get_array` is called.

    Returns
    -------
    Image
        A new image object with pixel data converted to 8-bit unsigned integers
        (`uint8`) preserving the original image's attributes, excluding data type.

    Notes
    -----
    This function uses skimage utilities to perform the conversion to `uint8`,
    maintaining image attributes except for `dtype`.
    """
    from skimage.util import img_as_ubyte
    from timagetk.components.image import get_image_class
    from timagetk.components.image import get_image_attributes
    Image = get_image_class(image)
    image_attr = get_image_attributes(image, exclude=['dtype'])
    return Image(img_as_ubyte(image.get_array()), dtype='uint8', **image_attr)


def int16_to_uint16(array):
    """Converts an array of type 'int16' to 'uint16'.

    Parameters
    ----------
    array : numpy.ndarray
        The array to be converted.

    Returns
    -------
    numpy.ndarray
        The array converted to 'uint16'.
    int
        The offset value used in the conversion.

    Examples
    --------
    >>> from timagetk.array_util import int16_to_uint16
    >>> from timagetk.array_util import random_spatial_image
    >>> img_int16 = random_spatial_image([15, 40, 40], voxelsize=[0.5, 0.21, 0.21], dtype="int16")
    >>> print(img_int16.get_array().min(), img_int16.get_array().max())
    -32761 32763
    >>> img_uint16, offset = int16_to_uint16(img_int16)
    >>> print(offset, int(img_int16.get_array().max()) + offset)
    32761
    >>> print(int(img_int16.get_array().max()) + offset)
    65524
    >>> print(img_uint16.min(), img_uint16.max())
    0 65524

    """
    # Ensure the input is a numpy array of int16
    int16_array = np.asarray(array, dtype=np.int16)
    # Find the minimum value
    min_value = np.min(int16_array)
    # Calculate the shift needed to make all values non-negative
    if min_value < 0:
        shift = -min_value
    else:
        shift = 0
    # Shift the int32 converted array (to prevent overflow during sum), clip to uint16 range and convert
    uint16_array = np.clip(int16_array.astype(np.int32) + shift, 0, 65535).astype(np.uint16)
    return uint16_array, shift


def uint16_to_int16(array, offset):
    """Convert an array of type 'uint16' to 'int16' using a shift offset.

    Parameters
    ----------
    array: numpy.ndarray
        The array to convert.
    offset: int
        The shift offset value.

    Returns
    -------
    numpy.ndarray
        The converted 'int16' array.

    Examples
    --------
    >>> from timagetk.array_util import uint16_to_int16
    >>> from timagetk.array_util import random_spatial_image
    >>> img_uint16 = random_spatial_image([15, 40, 40], voxelsize=[0.5, 0.21, 0.21], dtype="uint16")
    >>> print(img_uint16.get_array().min(), img_uint16.get_array().max())
    7 65531
    >>> img_int16 = uint16_to_int16(img_uint16, offset=32761)
    >>> print(img_int16.get_array().min(), img_int16.get_array().max())
    -32754 32767

    """
    # Convert to int32 (to prevent overflow during subtraction), subtract the offset, clip to int16 range and convert
    return np.clip(array.astype(np.int32) - offset, -32768, 32767).astype(np.int16)


def test_array_n_percent_range(arr: np.ndarray, pc_range_threshold: (int, float) = 75) -> bool:
    """Checks if the range of the array is equal to or greater than a specified percentage of the dtype's range.

    Parameters
    ----------
    arr : numpy.ndarray
        The input array.
    pc_range_threshold : int or float, optional
        The percentage difference allowed between the dtype's range and the array's range.
        The default is `75%`.

    Returns
    -------
    bool
        ``True`` if the array's range is equal to or greater than the specified percentage of the dtype's range,
        ``False`` otherwise.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.array_util import test_array_n_percent_range
    >>> arr = np.array([1, 2, 3, 4, 250], dtype=np.uint8)
    >>> print(test_array_n_percent_range(arr, pc_range_threshold=75))
    True
    >>> print(test_array_n_percent_range(arr, pc_range_threshold=99))
    False
    >>> arr = np.array([1, 2, 3, 4, 250], dtype=np.uint16)
    >>> print(test_array_n_percent_range(arr, pc_range_threshold=5))
    False
    """
    # Get the dtype's range
    dtype_range = np.iinfo(arr.dtype).max - np.iinfo(arr.dtype).min
    # Get the array's range
    array_range = arr.max() - arr.min()
    # Express the array's range as a percentage of the dtype range:
    array_pc_range = 100 * array_range / dtype_range
    return array_pc_range >= pc_range_threshold


def check_patch_size(image, patch_size):
    """Check the given patch size.

    Parameters
    ----------
    image : numpy.ndarray
        The image to cut into patches.
    patch_size : tuple or list
        The patch size to apply.

    Returns
    -------
    tuple
        The updated patch size.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.array_util import check_patch_size
    >>> image = np.zeros((10, 10, 10))
    >>> check_patch_size(image, (80, 160, 160))
    (10, 10, 10)

    """
    patch_size = list(patch_size)
    # Define axis names for meaningful warning messages
    ax_names = ('z', 'y', 'x')
    # Compare image dimensions with patch size for each axis
    for ax_idx, (im_sh, p_sh) in enumerate(zip(image.shape, patch_size)):
        # If patch size is larger than image dimension, adjust it
        if im_sh < p_sh:
            log.warning(f"Image shape ({im_sh}) is smaller than patch size ({p_sh}) for {ax_names[ax_idx]}-axis.")
            patch_size[ax_idx] = im_sh

    return tuple(patch_size)


def make_patches(image, patch_size=(80, 160, 160)):
    """Create overlapping patches of a given size in an array.

    Parameters
    ----------
    image : numpy.ndarray
        The image to cut into patches.
    patch_size : tuple or list
        The patch size to apply.

    Returns
    -------
    list
        The patches as tuples of slices

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.array_util import make_patches
    >>> image = np.zeros((50, 100, 100))
    >>> make_patches(image, (20, 40, 40))
    [(slice(0, 20, 1), slice(0, 40, 1), slice(0, 40, 1)),
    (slice(0, 20, 1), slice(0, 40, 1), slice(20, 60, 1)),
    ...
    (slice(30, 50, 1), slice(60, 100, 1), slice(40, 80, 1)),
    (slice(30, 50, 1), slice(60, 100, 1), slice(60, 100, 1))]
    """
    # Validate patch size against image dimensions
    patch_size = check_patch_size(image, patch_size)

    # Calculate centers of patches for each dimension
    # For each dimension, create evenly spaced points considering overlap
    patch_centers = [
        np.floor(np.linspace(
            p // 2,  # Start from half patch size
            s - p // 2,  # End at image size minus half patch
            int(np.ceil(s / (p // 2))) - 1  # Number of patches needed
        )).astype(int)
        for s, p in zip(image.shape, patch_size)
    ]

    # Create a grid of all patch center coordinates
    # Reshape to get a list of (z,y,x) coordinates
    patch_centers = np.transpose(np.meshgrid(*patch_centers), (1, 2, 3, 0)).reshape((-1, 3))

    # Convert center coordinates to slice objects for each patch
    # Each slice extracts a region of size patch_size centered on patch_centers
    patch_slices = [tuple(slice(c - p // 2, c + p // 2, 1)
                          for c, p in zip(p_c, patch_size))
                    for p_c in patch_centers]

    return patch_slices
