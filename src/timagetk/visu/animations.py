#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""The ``visu.animation`` module regroups functions to generate animated figures or movies.

Making "movies" requires the ``ffmpeg`` & ``x264`` libraries.
They are accessible via conda on the conda-forge channel.
"""

import os
import subprocess
import sys
from subprocess import CalledProcessError
from subprocess import PIPE
from subprocess import STDOUT

import matplotlib.pyplot as plt
import numpy as np
from tqdm.autonotebook import tqdm

from timagetk import SpatialImage
from timagetk.bin.logger import get_logger
from timagetk.components.multi_channel import combine_channels
from timagetk.components.multi_channel import label_blending
from timagetk.io import imread
from timagetk.io import read_trsf

log = get_logger(__name__)


def _check_ffmpeg():
    try:
        subprocess.run("ffmpeg -version", check=True, shell=True, stdout=PIPE, stderr=STDOUT)
    except CalledProcessError:
        msg = "Missing `ffmpeg` and most likely `x264` too.\n"
        msg += "To install them, in a conda environment, do:\n"
        msg += "conda install ffmpeg x264 -c conda-forge"
        raise ImportError(msg)


def _check_libx264():
    out = subprocess.run("ffmpeg -codecs", check=True, shell=True, stdout=PIPE, stderr=STDOUT)
    try:
        assert '--enable-libx264' in out.stdout.decode()
    except AssertionError:
        msg = "Missing `x264` but not `ffmpeg`!.\n"
        msg += "The easiest way to install them in a conda environment is to first remove ffmpeg then reinstall them both:\n"
        msg += "conda remove ffmpeg\n"
        msg += "conda install ffmpeg x264 -c conda-forge"
        raise ImportError(msg)


try:
    _check_ffmpeg()
except ImportError as e:
    raise e
else:
    _check_libx264()


def _slices2movie(slice_list, fname, **kwargs):
    """Make an .mp' a movie from a sequence of 2D images.

    Parameters
    ----------
    slice_list : list
        Sequence of 2D RGB images to iterate over in order to generate the movie.
    fname : str
        File name of the movie.

    Other Parameters
    ----------------
    fps : int
        Movie frame rate (frame per second).
    metadata : dict
        Dictionary of metadata to add to the movie
    bitrate : int
        The bitrate for the saved movie file, which is one way to control the output file size and quality.
        A value of ``-1`` implies that the bitrate should be determined automatically by the underlying utility.
    dpi : number
        Controls the dots per inch for the movie frames.
    extent : list of float
        If provided, set the extent of the displayed image.
        By default, try to use the real extent of the SpatialImage instance or set to ``image.shape`` (voxel unit).
    voxelsize : list of float
        If provided, set the voxelsize of the displayed image.
        By default, try to use the real voxelsize of the SpatialImage instance or set to ``[1., 1.]`` (voxel unit).
    unit : str
        If provided, set the unit oo the displayed image axes.
        By default, try to use the real unit of the SpatialImage instance or set to ``'voxels'`` (voxel unit).
    val_range : str or list of int
        Define the range of values used by the colormap, by default `'type'`.
    norm : matplotlib.colors.BoundaryNorm
        The `.Normalize` instance used to scale scalar data to the [0, 1] range before mapping to colors using *cmap*.
        By default, a linear scaling mapping the lowest value to 0 and the highest to 1 is used.
        This parameter is ignored for RGB(A) data.
    cmap : Any
        The Colormap instance or registered colormap name used to map scalar data to colors.
        This parameter is ignored for RGB(A) data.

    See Also
    --------
    timagetk.visu.mplt.image_plot

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.io import imread
    >>> from timagetk.visu.animations import _slices2movie
    >>> # - Get 'p58' shared intensity image:
    >>> image = shared_data('flower_confocal', 0)
    >>> axis = 'z'  # move along the z-axis
    >>> slice_list = [image.get_slice(i, axis) for i in range(image.get_shape(axis))]
    >>> extent = image.get_extent()
    >>> extent.pop(image.axes_order_dict[axis.upper()])  # remove the value at `axis` index
    >>> voxelsize = image.get_voxelsize()
    >>> voxelsize.pop(image.axes_order_dict[axis.upper()])  # remove the value at `axis` index
    >>> unit = image.get_unit()
    >>> _slices2movie(slice_list, shared_data('movie_test.mp4'), cmap='gray', extent=extent, voxelsize=voxelsize, unit=unit)

    """
    md = kwargs.pop('metadata', {})
    dpi = kwargs.pop('dpi', 200)
    fps = int(kwargs.pop('fps', 15))
    bitrate = kwargs.pop('bitrate', -1)
    from matplotlib.animation import FFMpegWriter
    writer = FFMpegWriter(fps=fps, metadata=md, codec='h264', bitrate=bitrate)

    fig = plt.figure()
    ax_img = plt.subplot()
    first_sl = slice_list.pop(0)
    from timagetk.visu.mplt import image_plot
    ax_img, fig_img = image_plot(first_sl, ax_img, cmap=kwargs.pop('cmap', None), **kwargs)
    ax_img.set_title(os.path.split(fname)[-1])

    with writer.saving(fig, fname, dpi=dpi):
        for sl in slice_list:
            fig_img.set_data(sl)
            writer.grab_frame()

    plt.close()


def animate_stack(image, filename, axis='z', cmap='gray', duration=5., plane_shape=None, invert=False, **kwargs):
    """Create a gif animation of the 3D stack along given plane.

    Parameters
    ----------
    images : list[timagetk.SpatialImage] or timagetk.MultiChannelImage
        List of intensity image stacks or a 3D multichannel image to use to create the animation.
    filename : str
        The file path and name to use to save the animation.
    axis : str, optional
        Axis to move along for animation. Animation will show the plane normal to this axis.
    cmap : str, optional
        A valid matplotlib colormap.
    duration : float, optional
        Duration of the animation, in seconds. 5s by default.
    plane_shape : list, optional
        The 2D shape (size) of the animation.
    invert : bool, optional
        If ``True``, revert the `axis` before generating the animation.

    Other Parameters
    ----------------
    background : int
        The background of the labelled or tissue image.

    Notes
    -----
    Pay attention to the order of the axes (ZYX, XYZ, ...) when providing a `plane_shape`.

    Raises
    ------
    ValueError
        If the specified `plane_shape` is not a len-2 list.
        If the file extension is not ``gif`` or ``mp4``.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk import LabelledImage
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.animations import animate_stack
    >>> from timagetk.visu.util import greedy_colormap

    >>> # -- Example #1: Make a GIF animation of an intensity image
    >>> # - Get shared INTENSITY image:
    >>> img = shared_data('flower_labelled', 0)
    >>> out_fname = "p58-z_slices{}"
    >>> # - Defines the GIF file name and path:
    >>> gif_path = shared_data(out_fname.format('.gif'), "p58")  # Export it to the shared dataset
    >>> # - Generates and export the GIF animation:
    >>> animate_stack(img, gif_path)

    >>> # -- Example #2: Make a GIF animation of a labelled image
    >>> # - Get shared LABELLED image:
    >>> img = imread(shared_dataset("p58", "segmented")[0], LabelledImage, not_a_label=0)
    >>> # - Defines the GIF file name and path:
    >>> gif_path = shared_data(out_fname.format('.gif'), "p58")  # Export it to the shared dataset
    >>> # - Generates and export the GIF animation:
    >>> animate_stack(img, gif_path, cmap='glasbey')

    >>> # -- Example #3: Make an MP4 animation of a labelled image
    >>> # - Get shared LABELLED image:
    >>> img = imread(shared_dataset("p58", "segmented")[0], LabelledImage, not_a_label=0)
    >>> # - Generates and export then MP4 animation:
    >>> movie_path = shared_data(out_fname.format('.mp4'), "p58")  # Export it to the shared dataset
    >>> # - Defines the MP4 file name and path:
    >>> animate_stack(img, movie_path, cmap=greedy_colormap(img))

    """
    import matplotlib.cm as cm
    import matplotlib.colors as colors
    from matplotlib.pyplot import get_cmap
    from timagetk.algorithms.resample import resample
    from timagetk.components.labelled_image import LabelledImage
    from skimage import img_as_ubyte

    extent = image.get_extent()
    extent.pop(image.axes_order_dict[axis.upper()])  # remove the value at `axis` index
    voxelsize = image.get_voxelsize()
    voxelsize.pop(image.axes_order_dict[axis.upper()])  # remove the value at `axis` index
    unit = image.get_unit()

    if plane_shape is not None:
        # Make sure its correctly defined:
        try:
            assert len(plane_shape) == 2
        except AssertionError:
            raise ValueError("The `plane_shape` should be a len-2 list!")
        # Insert the original shape of the selected `axis` at its position w.r.t. axes ordering:
        plane_shape.insert(image.axes_order_dict[axis.upper()], image.get_shape(axis))
        # Resample labelled and intensity image to blend:
        if isinstance(image, LabelledImage):
            image = resample(image, shape=plane_shape, interpolation='cellbased')
        else:
            image = resample(image, shape=plane_shape, interpolation='linear')

    max_slice = image.get_shape(axis)
    fps = max_slice / float(duration)
    # Invert ordering if required:
    if invert:
        image.invert_axis(axis)

    mappable = cm.ScalarMappable(norm=colors.Normalize(vmin=0, vmax=255), cmap=get_cmap(cmap))

    if isinstance(image, LabelledImage):
        slice_list = [mappable.to_rgba(image.get_slice(sl).get_array() % 256) for sl in range(max_slice)]
        # TODO: change the RGBA values where background is located to 'white':
        # bkgd = kwargs.get('background', 1)
        # slice_list = [np.where(image.get_slice(sl_id) != bkgd, sl, [1., 1., 1., 0.]) for sl_id, sl in enumerate(slice_list)]
    else:
        slice_list = [mappable.to_rgba(image.get_slice(sl)) for sl in range(max_slice)]

    if filename.endswith('.gif'):
        from imageio import mimsave as gif_writer
        # Transform to uint8 array (required by imageio):
        slice_list = [img_as_ubyte(im) for im in slice_list]
        gif_writer(filename, slice_list, format='gif', fps=fps)
    elif filename.endswith('.mp4'):
        _slices2movie(slice_list, filename, fps=fps, extent=extent, voxelsize=voxelsize, unit=unit)
    else:
        raise ValueError("Unknown extension '{}', accepted are: 'gif' & 'mp4'.")

    log.info(f"Saved animation under: '{filename}'")

    return


def animate_time_series(images, fname, cmap='gray', duration=5., plane_shape=None, trsfs=None, **kwargs):
    """Create a dynamic figure of a time-series, GIF or MP4.

    Parameters
    ----------
    images : dict of str or dict of timagetk.SpatialImage or dict of timagetk.LabelledImage
        Time-indexed dictionary of 3D intensity or segmented image to project and animate.
        Can be a dictionary of filenames, in that case you should specify `rtype` to load the correct type of image.
    fname : str
        The file path and name to use to save the animation.
    cmap : str, optional
        A valid matplotlib colormap.
    duration : float, optional
        Duration of the animation, in seconds. 5s by default.
    plane_shape : list, optional
        The 2D shape (size) of the animation.
    trsfs : dict of Trsf, optional
        If not ``None``, use this time interval indexed dictionary of rigid transformations to register images.

    Other Parameters
    ----------------
    axis : {'x', 'y', 'z'}
        Axis to use for projection.
    method : str, optional
        Method to use for projection, see ``PROJECTION_METHODS`` for available methods.
        Valid for intensity images, segmented image have only one (default) method.
    threshold : int
        Threshold to use with the 'contour' method.
    orientation: {-1, 1}
        Defines the orientation of the projection, try to guess it by default.
        If `-1` we consider the z-axis starts at the bottom of the object and goes upward.
        If `1` we consider the z-axis starts at the top of the object and goes downward.
    iso_upsampling : bool
        If ``True`` (default), up-sample the image to the minimum voxelsize.
        This is usefull if you have an axis with a lower resolution, typically the z-axis.
    altimap_smoothing : bool
        If ``True``, default is ``False``, smooth the altitude map obtained from the mask.
        Valid for intensity images.
    gaussian_sigma : float
        Sigma value of the Gaussian filter applied to the image, it is divided by the image voxelsize.
        Valid for intensity images.
    height : float
        Height parameter.
    rtype : {SpatialImage, LabelledImage}
        The class of image to use when loading images.
    logger : loggging.Logger
        The logger to use. Creates one by default.

    See Also
    --------
    timagetk.visu.projection.PROJECTION_METHODS

    Notes
    -----
    Pay attention to the order of the axes (ZYX, XYZ, ...) and the selected `axis` when providing a `plane_shape`.

    Raises
    ------
    ValueError
        If the specified `plane_shape` is not a len-2 list.
        If the file extension is not ``gif`` or ``mp4``.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk import LabelledImage
    >>> from timagetk.io import imread
    >>> from timagetk.tasks.registration import sequence_registration
    >>> from timagetk.visu.animations import animate_time_series
    >>> # EXAMPLE #1 - Project intensity time-series
    >>> time_points = shared_dataset("p58",'time-points')
    >>> list_fnames = shared_dataset("p58",'intensity')
    >>> # - Defines the GIF file name and path:
    >>> gif_fname = "p58-intensity_time_series{}".format('.gif')
    >>> gif_path = shared_data(gif_fname, "p58")  # Export it to the shared dataset
    >>> # - Generates and export the GIF animation:
    >>> proj_list = animate_time_series(list_fnames, gif_path, method='contour')
    >>> # EXAMPLE #2 - Project segmented time-series
    >>> list_fnames = shared_dataset("p58",'segmented')
    >>> # - Defines the GIF file name and path:
    >>> gif_fname = "p58-segmented_time_series{}".format('.gif')
    >>> gif_path = shared_data(gif_fname, "p58")  # Export it to the shared dataset
    >>> # - Generates and export the GIF animation:
    >>> proj_list = animate_time_series(list_fnames, gif_path, cmap='glasbey', rtype=LabelledImage)

    """
    from timagetk.algorithms.resample import resample
    from timagetk.components.spatial_image import SpatialImage
    from timagetk.components.labelled_image import LabelledImage
    from skimage import img_as_ubyte
    from timagetk.visu.projection import projection
    import matplotlib.cm as cm
    import matplotlib.colors as colors
    from matplotlib.pyplot import get_cmap
    from timagetk.visu.projection import DEF_PROJ_METHODS
    from timagetk.algorithms.trsf import apply_trsf

    from timagetk.bin.logger import get_dummy_logger
    logger = kwargs.get('logger', get_dummy_logger(sys._getframe().f_code.co_name))

    axis = kwargs.pop('axis', "z")  # default projection axis is "z"
    proj_method = kwargs.pop('method', DEF_PROJ_METHODS)  # default projection method
    rtype = kwargs.pop('rtype', LabelledImage if isinstance(images[0],
                                                            LabelledImage) else SpatialImage)  # default type of projected image is `SpatialImage`
    not_a_label = kwargs.pop('not_a_label', 0)  # default `not_a_label` is `0`
    background = kwargs.pop('background', 1)  # default `background` is `1`
    interpolation = kwargs.pop('interpolation', 'cellbased' if rtype == LabelledImage else 'linear')
    cell_based_sigma = kwargs.pop('cell_based_sigma', 1 if rtype == LabelledImage else None)

    # If not `plane_shape` was specified, we have to determine it
    if plane_shape is None:
        plane_shape = _last_plane_shape(images, axis, rtype=rtype, not_a_label=not_a_label, background=background)
    else:
        # Make sure its correctly defined:
        try:
            assert len(plane_shape) == 2
        except AssertionError:
            raise ValueError("The `plane_shape` should be a len-2 list!")

    img_type = 'segmented' if rtype == LabelledImage else 'intensity'
    # Todo: check all same type of instances!

    n_imgs = len(images)
    tmax = max(list(images.keys()))  # max time index
    template = None

    if trsfs is not None:
        # Check we have the required transformations for temporal registration of the sequence (t_i, t_max)
        required_regs = [(t, tmax) for t in range(n_imgs - 1)]
        try:
            assert all(reg in trsfs for reg in required_regs)
        except AssertionError:
            logger.critical("Could not find all the required rigid registered time-series!")
            logger.critical(f"Missing rigid trsfs for intervals: {[reg not in trsfs for reg in required_regs]}")
            logger.info("Run `sequence_registration.py` prior to using `temporal_projection.py`!")
            trsfs = None

    log.info(f"Starting {proj_method} projection of {img_type} images...")
    proj_list = []
    for t, image in tqdm(images.items(), unit='image'):
        if isinstance(image, str):
            image = imread(image, rtype=rtype, not_a_label=not_a_label, background=background)
        if trsfs is not None and t != tmax:
            # Load the template if not done yet:
            if template is None:
                template = images[tmax]  # the template is the last image of the time-series
                if isinstance(template, str):
                    template = imread(template, rtype=rtype, not_a_label=not_a_label, background=background)
            # Load the rigid trsf to apply:
            if isinstance(trsfs[(t, tmax)], str):
                trsf = read_trsf(trsfs[(t, tmax)])
            else:
                trsf = trsfs[(t, tmax)]
            # Apply the transformation:
            image = apply_trsf(image, trsf, template_img=template,
                               interpolation=interpolation, cell_based_sigma=cell_based_sigma)
        # Project the image:
        proj_im = projection(image, method=proj_method, **kwargs)
        # Resample projected image if plane_shape do not match:
        if proj_im.shape != plane_shape:
            proj_im = resample(proj_im, shape=plane_shape,
                               interpolation=interpolation, cell_based_sigma=cell_based_sigma)
        proj_list.append(proj_im)

    if isinstance(image, SpatialImage):
        from timagetk.util import type_to_range
        vmin, vmax = type_to_range(image)
        log.debug(vmin, vmax)
        mappable = cm.ScalarMappable(norm=colors.Normalize(vmin=vmin, vmax=vmax), cmap=get_cmap(cmap))
    else:
        mappable = cm.ScalarMappable(norm=colors.Normalize(vmin=0, vmax=255), cmap=get_cmap(cmap))

    if rtype == LabelledImage:
        proj_list = [mappable.to_rgba(im.get_array() % 256) for im in proj_list]
        # TODO: change the RGBA values where background is located to 'white':
        # bkgd = kwargs.get('background', 1)
        # proj_im = [np.where(image.get_slice(sl_id) != bkgd, sl, [1., 1., 1., 0.]) for sl_id, sl in enumerate(slice_list)]
    else:
        proj_list = [mappable.to_rgba(im) for im in proj_list]

    # TODO: use time-intervals for a time-realistic animation
    # TODO: use time-intervals to performs motion interpolations

    fps = n_imgs / float(duration)
    if fname.endswith('.gif'):
        from imageio import mimsave as gif_writer
        # Transform to uint8 array (required by imageio):
        proj_list = [img_as_ubyte(im) for im in proj_list]
        try:
            gif_writer(fname, proj_list, format='gif', fps=fps)
        except TypeError:
            gif_writer(fname, proj_list, format='gif', duration=1000*duration)
    elif fname.endswith('.mp4'):
        pass
    else:
        _slices2movie(proj_list, fname, fps=fps)
        raise ValueError("Unknown extension '{}', accepted are: 'gif' & 'mp4'.")

    log.info(f"Saved animation under: '{fname}'")

    return proj_list


def _largest_plane_shape(images, axis, **kwargs):
    """Get the largest plane shape from the list of `images`.

    Parameters
    ----------
    images : list of str or list of SpatialImage or list[timagetk.LabelledImage]
        Time indexed dictionary of images to project.
    axis : {'x', 'y', 'z'}
        Axis to use for projection.

    Returns
    -------
    list
        a len-2 list given the largest plane shape

    """
    rtype = kwargs.pop('rtype', SpatialImage)  # default type of projected image is `SpatialImage`
    not_a_label = kwargs.pop('not_a_label', 0)  # default `not_a_label` is `0`
    background = kwargs.pop('background', 1)  # default `background` is `1`

    plane_shape = [0, 0]
    for image in images:
        if isinstance(image, str):
            image = imread(image, rtype=rtype, not_a_label=not_a_label, background=background)
        new_shape = image.get_shape()
        new_shape.pop(image.axes_order_dict[axis.upper()])  # remove the value at `axis` index
        if np.prod(new_shape) >= np.prod(plane_shape):
            plane_shape = new_shape
    log.info(f"Automatically determined final shape: {plane_shape}")

    return plane_shape


def _last_plane_shape(images, axis, **kwargs):
    """Get the plane shape from the last element of the list of `images`.

    Parameters
    ----------
    images : list of str or list of SpatialImage or list[timagetk.LabelledImage]
        Time indexed dictionary of images to project.
    axis : {'x', 'y', 'z'}
        Axis to use for projection.

    Returns
    -------
    list
        a len-2 list given the largest plane shape

    """
    rtype = kwargs.pop('rtype', SpatialImage)  # default type of projected image is `SpatialImage`
    not_a_label = kwargs.pop('not_a_label', 0)  # default `not_a_label` is `0`
    background = kwargs.pop('background', 1)  # default `background` is `1`

    if isinstance(images, (list, tuple)):
        image = images[-1]
    elif isinstance(images, dict):
        max_idx = max(list(images.keys()))
        image = images[max_idx]
    else:
        raise TypeError("Wrong type for parameter `images`!")

    if isinstance(image, str):
        image = imread(image, rtype=rtype, not_a_label=not_a_label, background=background)

    plane_shape = image.get_shape()
    plane_shape.pop(image.axes_order_dict[axis.upper()])  # remove the value at `axis` index
    log.info(f"Automatically determined final shape: {plane_shape}")

    return plane_shape


def animate_channel_blending(images, filename, axis='z', duration=5., plane_shape=None, invert=False, **kwargs):
    """Create a gif animation of the blended channels along given plane.

    Parameters
    ----------
    images : list[timagetk.SpatialImage] or timagetk.MultiChannelImage
        List of intensity image stacks or a 3D multichannel image to use to create the animation.
    filename : str
        The file path and name to use to save the animation.
    axis : str, optional
        Axis to move along for animation. Animation will show the plane normal to this axis.
    duration : float, optional
        Duration of the animation, in seconds. 5s by default.
    plane_shape : list, optional
        The 2D shape (size) of the animation.
    invert : bool, optional
        If ``True``, revert the `axis` before generating the animation.

    Other Parameters
    ----------------
    colors : list or tuple, optional
        Coloring to apply to each image or channel.

    Notes
    -----
    Pay attention to the order of the axes (ZYX, XYZ, ...) when providing a `plane_shape`.

    Raises
    ------
    ValueError
        If the specified `plane_shape` is not a len-2 list.
        If the file extension is not ``gif`` or ``mp4``.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.io import imread
    >>> from timagetk.algorithms.blockmatching import blockmatching
    >>> from timagetk.algorithms.pointmatching import pointmatching
    >>> from timagetk.algorithms.trsf import apply_trsf
    >>> from timagetk.visu.animations import animate_channel_blending
    >>> # - Get two shared multi-angle images:
    >>> ref_img = shared_data('flower_multiangle', 0)
    >>> float_img = shared_data('flower_multiangle', 1)
    >>> # - Load shared multi-angle landmarks for the first time point (t0) of 'p58' shared time-series
    >>> ref_pts_01 = np.loadtxt(shared_data('p58_t0_reference_ldmk-01.txt', 'p58'))
    >>> flo_pts_01 = np.loadtxt(shared_data('p58_t0_floating_ldmk-01.txt', 'p58'))
    >>> # - Creates manual initialization transformations with `pointmatching` algorithm:
    >>> init_trsf = pointmatching(flo_pts_01, ref_pts_01, template_img=ref_img, method='rigid')
    >>> # - Rigid registration of the "a1" image onto the "a0" image:
    >>> rigid_trsf = blockmatching(float_img, ref_img, method='rigid', init_trsf=init_trsf, pyramid_lowest_level=2)
    >>> res_img = apply_trsf(float_img, rigid_trsf, interpolation='linear', template_img=ref_img)
    >>> # - Defines the GIF file name and path:
    >>> gif_fname = "p58-t0-rigid-a1_on_a0{}".format('.gif')
    >>> gif_path = shared_data(gif_fname, "p58")  # Export it to the shared dataset
    >>> # - Generates and export the GIF animation:
    >>> animate_channel_blending([res_img, ref_img], gif_path, plane_shape=[512, 512])

    >>> fname = "t0-on-t1{}".format('.mp4')
    >>> animate_channel_blending([res_def, ref_img],shared_data(fname),duration=5.,plane_shape=[512, 512])

    """
    from skimage import img_as_ubyte
    from timagetk.algorithms.resample import resample
    from timagetk.components.spatial_image import SpatialImage
    from timagetk.components.multi_channel import MultiChannelImage

    if isinstance(images, list) and all(isinstance(img, SpatialImage) for img in images):
        images = MultiChannelImage(images)

    if plane_shape is not None:
        # Make sure its correctly defined:
        try:
            plane_shape = list(plane_shape)
            assert len(plane_shape) == 2
        except AssertionError:
            raise ValueError("The `plane_shape` should be a len-2 list!")
        # Insert the original shape of the selected `axis` at its position w.r.t. axes ordering:
        plane_shape.insert(images.axes_order_dict[axis.upper()], images.get_shape(axis))
        # Resample labelled and intensity image to blend:
        images = resample(images, shape=plane_shape, interpolation='linear')

    blend = combine_channels(images, kwargs.get('colors', None))
    max_slice = images.get_shape(axis)
    fps = max_slice / float(duration)
    # Extract the slices
    slice_list = [blend.get_slice(sl, axis=axis) for sl in range(max_slice)]
    # Invert ordering if required:
    if invert:
        slice_list = slice_list[::-1]
    # Transform to array:
    slice_list = [img_as_ubyte(im) for im in slice_list]

    if filename.endswith('.gif'):
        from imageio import mimsave as gif_writer
        gif_writer(filename, slice_list, format='gif', fps=fps)
    elif filename.endswith('.mp4'):
        _slices2movie(slice_list, filename, fps=fps)
    else:
        raise ValueError("Unknown extension '{}', accepted are: 'gif' & 'mp4'.")

    log.info(f"Saved animation under: '{filename}'")

    return


def animate_labelled_intensity_blending(label, image, filename, axis='z', duration=5., plane_shape=None, invert=False,
                                        **kwargs):
    """Create a gif animation of the blended stack (`label`+`image`) by moving along the selected axis.

    Parameters
    ----------
    label : timagetk.LabelledImage
        Labelled image stack, should have the same shape as `image`.
    image : timagetk.SpatialImage
        Intensity image stack, should have the same shape as `label`.
    filename : str
        The file path and name to use to save the animation.
    axis : str, optional
        Axis to move along for animation. Animation will show the plane normal to this axis.
    duration : float, optional
        Duration of the animation, in seconds. 5s by default.
    plane_shape : list, optional
        The 2D shape (size) of the animation.
    invert : bool, optional
        If ``True``, revert the `axis` before generating the animation.

    Other Parameters
    ----------------
    alpha : float [0, 1], optional
        Opacity of colorized labels. Ignored if image is ``None``.
    bg_label : int, optional
        Label that's treated as the background.
    bg_color : str or array, optional
        Background color. Must be a name in `color_dict` or RGB float values between [0, 1].

    Notes
    -----
    Pay attention to the order of the axes (ZYX, XYZ, ...) when providing a `plane_shape`.

    Raises
    ------
    ValueError
        If the specified `plane_shape` is not a len-2 list.
        If the file extension is not ``gif`` or ``mp4``.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.io import imread
    >>> from timagetk.algorithms.exposure import slice_contrast_stretch
    >>> from timagetk.algorithms.resample import isometric_resampling
    >>> from timagetk.algorithms.linearfilter import gaussian_filter
    >>> from timagetk.tasks.segmentation import watershed_segmentation
    >>> from timagetk.visu.animations import animate_labelled_intensity_blending
    >>> int_img = shared_data('flower_confocal', 0)
    >>> int_img = isometric_resampling(int_img, value='min', option='cspline')
    >>> # - Performs seeded watershed segmentation:
    >>> h_min = 8
    >>> seg_img, _, _ = watershed_segmentation(int_img, h_min)
    >>> # - Defines the GIF file name and path:
    >>> gif_fname = "p58-t0-a0-seg_hmin{}-reshaped{}".format(h_min, '.gif')
    >>> gif_path = shared_data(gif_fname, "p58")  # Export it to the shared dataset
    >>> # - Generates and export the GIF animation:
    >>> animate_labelled_intensity_blending(seg_img, int_img, gif_path, plane_shape=[256, 256])

    """
    from skimage import img_as_ubyte
    from timagetk.algorithms.resample import resample

    try:
        assert np.array_equal(label.shape, image.shape)
    except AssertionError:
        msg = "Segmented {} and intensity {} images do not have the same shape!"
        raise ValueError(msg.format(label.shape, image.shape))

    if plane_shape is not None:
        # Make sure its correctly defined:
        try:
            plane_shape = list(plane_shape)
            assert len(plane_shape) == 2
        except AssertionError:
            raise ValueError("The `plane_shape` should be a len-2 list!")
        # Insert the original shape of the selected `axis` at its position w.r.t. axes ordering:
        plane_shape.insert(image.axes_order_dict[axis.upper()], image.get_shape(axis))
        # Resample labelled and intensity image to blend:
        label = resample(label, shape=plane_shape, interpolation='cellbased')
        image = resample(image, shape=plane_shape, interpolation='linear')

    # - Blend the labelled and intensity image
    blend = label_blending(label, image, **kwargs)
    max_slice = image.get_shape(axis)
    fps = max_slice / float(duration)
    # Extract the slices
    slice_list = [blend.get_slice(sl, axis=axis) for sl in range(max_slice)]
    # Invert ordering if required:
    if invert:
        slice_list = slice_list[::-1]

    # Transform to uint8 array (required by imageio):
    slice_list = [img_as_ubyte(im) for im in slice_list]

    if filename.endswith('.gif'):
        from imageio import mimsave as gif_writer
        gif_writer(filename, slice_list, format='gif', fps=fps)
    elif filename.endswith('.mp4'):
        _slices2movie(slice_list, filename, fps=fps)
    else:
        raise ValueError("Unknown extension '{}', accepted are: 'gif' & 'mp4'.")

    log.info(f"Saved animation under: '{filename}'")

    return
