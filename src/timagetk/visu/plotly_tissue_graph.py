#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Plotly based graph methods.

See `timagetk/notebooks/plotly_examples.ipynb` for examples.
"""

import numpy as np
import plotly.express as px
import plotly.graph_objects as go
from ipywidgets import widgets

from timagetk.bin.logger import get_logger

log = get_logger(__name__)


def get_cids_by_layers(tg, layer_range, time_point=None):
    """Get the list of cell ids within the layers range.

    Parameters
    ----------
    tg : timagetk.graphs.TissueGraph
        The tissue graph object
    layer_range : list
        The range of layer to accept.

    Notes
    -----
    ``[1, 5]`` is equivalent to `range(1, 6)`.
    ``[1, 1]`` is equivalent to `range(1, 2)`.

    Returns
    -------
    list
        The list of cell ids within the layer range.

    """
    from timagetk.graphs.temporal_tissue_graph import TemporalTissueGraph
    if isinstance(tg, TemporalTissueGraph):
        idx = tg.cell_ids(time_point)
    else:
        idx = tg.cell_ids()
    layers = tg.cell_property_dict('layers', idx, only_defined=True)
    layer_range[1] += 1
    idx2exclude = [cid for cid, layer in layers.items() if layer not in range(*layer_range)]
    # for cid, layer in layers.items():
    #     if layer not in range(*layer_range):
    #         idx2exclude.append(cid)
    return list(set(idx) - set(idx2exclude))


def get_axes_range(x, y, z):
    """Return the axes ranges to use from the set of barycenter coordinates."""
    mini_x, maxi_x = round(np.nanmin(x) - 2.1, 2), round(np.nanmax(x) + 2.1, 2)
    mini_y, maxi_y = round(np.nanmin(y) - 2.1, 2), round(np.nanmax(y) + 2.1, 2)
    mini_z, maxi_z = round(np.nanmin(z) - 2.1, 2), round(np.nanmax(z) + 2.1, 2)
    return mini_x, maxi_x, mini_y, maxi_y, mini_z, maxi_z


def tissue_graph_gui(tg, **kwargs):
    """A GUI for exploring TissueGraph instances in a notebook.

    Parameters
    ----------
    tg : timagetk.graphs.TissueGraph
        The tissue graph object to open in the GUI.

    Other Parameters
    ----------------
    width : int
        Width of the GUI, note that notebook usually have a max width of 1110px. Defaults to 950px.
    height : int
        Height of the GUI. Defaults to 800px.

    Returns
    -------
    plotly.graph_objects.FigureWidget
        The figure widget.
    ipywidgets.widgets
        The widget controls.

    """
    width = kwargs.get('width', 950)  # figure width
    height = kwargs.get('height', 800)  # figure height

    feature_list = sorted(tg.list_cell_properties())
    # Creates the 'layers' property in the graph if missing:
    if "layers" not in feature_list:
        from timagetk.features.graph import add_cell_layers
        tg = add_cell_layers(tg)

    feature_list = _select_valid_cell_features(tg, kwargs.get('exclude_identities', False))
    # -- CELL widget:
    node_w, node_filter_w, ppty_w, layer_w, cmap_w, nodesize_w = _cell_widgets(tg, feature_list)
    # -- CELL-WALL widget:
    edge_w, edge_thick_w = _wall_widgets(tg, feature_list)

    global x_cid2exclude, y_cid2exclude, z_cid2exclude
    x_cid2exclude, y_cid2exclude, z_cid2exclude = set(), set(), set()

    def _filter_ids(layer, node_filtering=False):
        idx = tg.cell_ids()
        if node_filtering:
            idx = set(idx) & set(get_cids_by_layers(tg, list(layer)))
        return list(set(idx) - x_cid2exclude - y_cid2exclude - z_cid2exclude)

    global idx
    idx = _filter_ids(layer_w.value, node_filter_w.value)
    global barycenter
    barycenter = tg.cell_property_dict('barycenter', tg.cell_ids(), default=[np.nan, np.nan, np.nan])
    global cwids
    cwids = tg.cell_wall_ids()

    # Get x, y & z coordinates:
    z, y, x = get_node_coordinates(barycenter, idx)
    mini_z, maxi_z, mini_y, maxi_y, mini_x, maxi_x = get_axes_range(z, y, x)
    global x_range_w, y_range_w, z_range_w
    x_range_w = widgets.FloatRangeSlider(description='x-range:   ', min=mini_x, max=maxi_x, step=2.0,
                                         value=[mini_x, maxi_x], continuous_update=True)
    y_range_w = widgets.FloatRangeSlider(description='y-range:   ', min=mini_y, max=maxi_y, step=2.0,
                                         value=[mini_y, maxi_y], continuous_update=True)
    z_range_w = widgets.FloatRangeSlider(description='z-range:   ', min=mini_z, max=maxi_z, step=2.0,
                                         value=[mini_z, maxi_z], continuous_update=True)

    def _gui_get_ppty_dict(sel_ppty):
        if sel_ppty in tg.list_identities():
            ppty_values = tg.cell_property_dict(sel_ppty, default=False)
            ppty_values = {tcid: int(i) for tcid, i in ppty_values.items()}
        else:
            ppty_values = tg.cell_property_dict(sel_ppty, default=np.nan)
        return ppty_values

    def _gui_get_ppty_range(sel_ppty):
        if sel_ppty in tg.list_identities():
            return 0, 1
        else:
            all_ppty_values_dict = tg.cell_property_dict(sel_ppty, default=np.nan)
            return np.nanmin(list(all_ppty_values_dict.values())), np.nanmax(list(all_ppty_values_dict.values()))

    # Widget to highlight a list of cell ids
    highlight_cids_w = widgets.Text(
        value='',
        placeholder='List of cell ids, comma separated',
        description='Highlight cell ids:',
        disabled=False,
    )
    update_high_cids_w = widgets.Button(
        description='Update',
        disabled=False,
        button_style='',  # 'success', 'info', 'warning', 'danger' or ''
        tooltip='Update the list of cells to highlight',
        icon='check',  # (FontAwesome names without the `fa-` prefix)
    )

    # Get the cell property values to color the scatter plot:
    global ppty_values_dict, ppty_values, min_val, max_val
    ppty_values_dict = _gui_get_ppty_dict(ppty_w.value)
    ppty_values = [ppty_values_dict[tcid] for tcid in idx]
    min_val, max_val = _gui_get_ppty_range(ppty_w.value)

    def scatter_hover(ppty_dict, idx):
        """Create the template to display cell information on hover."""
        hovertemplate = [f"cell-id: {cid}" + "<br>" +
                         f"{ppty_w.value}: {round(ppty_dict[cid], 3) if isinstance(ppty_dict[cid], float) else ppty_dict[cid]}" +
                         f"{tg.cell_property_unit(ppty_w.value)}"
                         for cid in idx]
        return hovertemplate

    marker_kwargs = {'size': nodesize_w.value, 'color': ppty_values, 'colorscale': cmap_w.value,
                     'colorbar': {'title': ppty_w.value}, 'cmin': min_val, 'cmax': max_val}
    node_trace = go.Scatter3d(x=x, y=y, z=z, ids=idx, mode='markers', name='cells', marker=marker_kwargs,
                              hovertemplate=scatter_hover(ppty_values_dict, idx), hoverinfo='text', showlegend=False)

    edge_x, edge_y, edge_z = get_edge_coordinates(cwids, barycenter, idx)
    edge_trace = go.Scatter3d(x=edge_z, y=edge_y, z=edge_x, mode='lines', name='edges',
                              line={'width': edge_thick_w.value, 'color': '#888'},
                              hoverinfo='none', showlegend=False)

    # -- Figure LAYOUT:
    layout_kwargs = {}
    if width is not None:
        layout_kwargs["width"] = width
    if height is not None:
        layout_kwargs["height"] = height

    camera = {'eye': {'x': 0, 'y': 0, 'z': -2.5}}  # Top-view

    def _aspect_ratio(mini_x, maxi_x, mini_y, maxi_y, mini_z, maxi_z):
        xr, yr, zr = maxi_x - mini_x, maxi_y - mini_y, maxi_z - mini_z  # to size the axes depending on their extent
        rmax = float(max(xr, yr, zr))  # get the max axes extent to use for ratio computation
        return {'x': xr / rmax, 'y': yr / rmax, 'z': zr / rmax}

    layout = go.Layout(autosize=False, hovermode="closest",
                       scene={'dragmode': 'orbit',
                              "aspectratio": _aspect_ratio(mini_x, maxi_x, mini_y, maxi_y, mini_z, maxi_z),
                              'xaxis': {'autorange': False},
                              'yaxis': {'autorange': False},
                              'zaxis': {'autorange': False}},
                       scene_camera=camera, **layout_kwargs)
    fig_w = go.FigureWidget(data=[node_trace, edge_trace], layout=layout)
    # fig_w.update_layout(scene_aspectmode='data')
    fig_w.layout.scene.xaxis.range = [mini_x, maxi_x]
    fig_w.layout.scene.yaxis.range = [mini_y, maxi_y]
    fig_w.layout.scene.zaxis.range = [mini_z, maxi_z]
    fig_w.layout.title = ppty_w.value

    def highlight_cids(change):
        if highlight_cids_w.value != '':
            high_cids = highlight_cids_w.value.replace(' ', '')  # remove whitespaces
            if ',' in high_cids:
                try:
                    high_cids = list(map(int, high_cids.split(',')))
                except:
                    log.error("There is a mistake in the list of cell ids to highlight!")
            else:
                high_cids = [int(high_cids)]
            sizes = [1.5 * nodesize_w.value if k in high_cids else 0.5 * nodesize_w.value for k in idx]
        else:
            sizes = [nodesize_w.value] * len(idx)

        with fig_w.batch_update():
            fig_w.data[0].marker.size = sizes

    def update_ids(change):
        filter_ids(change)  # reset list of cell ids
        show_hide_node(change)

    def change_ppty(change):
        global ppty_values_dict, ppty_values, min_val, max_val
        ppty_values_dict = _gui_get_ppty_dict(ppty_w.value)
        ppty_values = [ppty_values_dict[tcid] for tcid in idx]
        min_val, max_val = _gui_get_ppty_range(ppty_w.value)
        update_ppty_data(change)

    def update_ppty_data(change):
        ppty_values = [ppty_values_dict[k] for k in idx]
        with fig_w.batch_update():
            fig_w.data[0].marker.size = nodesize_w.value
            fig_w.data[0].marker.color = ppty_values
            fig_w.data[0].marker.cmin = min_val
            fig_w.data[0].marker.cmax = max_val
            fig_w.data[0].marker.colorscale = cmap_w.value
            # fig_w.data[0].marker.colorbar.title = ppty_w.value
            fig_w.layout.title = ppty_w.value
            fig_w.data[0].hovertemplate = scatter_hover(ppty_values_dict, idx)

    def update_cmap(change):
        with fig_w.batch_update():
            fig_w.data[0].marker.colorscale = cmap_w.value

    def show_hide_node(change):
        with fig_w.batch_update():
            if node_w.value:
                fig_w.data[0].z, fig_w.data[0].y, fig_w.data[0].x = get_node_coordinates(barycenter, idx)
                update_ppty_data(change)
                show_hide_edge(change)
            else:
                fig_w.data[0].x = [None]
                fig_w.data[0].y = [None]
                fig_w.data[0].z = [None]
                fig_w.data[0].marker = {}

    def show_hide_edge(change):
        with fig_w.batch_update():
            if edge_w.value:
                fig_w.data[1].z, fig_w.data[1].y, fig_w.data[1].x = get_edge_coordinates(cwids, barycenter, idx)
            else:
                fig_w.data[1].x = [None]
                fig_w.data[1].y = [None]
                fig_w.data[1].z = [None]

    def update_node_marker_size(change):
        with fig_w.batch_update():
            fig_w.data[0].marker.size = nodesize_w.value

    def update_edge_thickness(change):
        with fig_w.batch_update():
            fig_w.data[1].line.width = edge_thick_w.value

    def filter_ids(change):
        global idx
        idx = _filter_ids(layer_w.value, node_filter_w.value)
        show_hide_node(change)

    def update_xaxes_range(change):
        global x_cid2exclude
        min_x, max_x = list(x_range_w.value)

        def _exclude(cid):
            z, y, x = barycenter[cid]
            if not min_x <= x <= max_x:
                return cid
            else:
                return None

        x_cid2exclude = {_exclude(cid) for cid in tg.cell_ids()}
        filter_ids(change)

    def update_yaxes_range(change):
        global y_cid2exclude
        min_y, max_y = list(y_range_w.value)

        def _exclude(cid):
            z, y, x = barycenter[cid]
            if not min_y <= y <= max_y:
                return cid
            else:
                return None

        y_cid2exclude = {_exclude(cid) for cid in tg.cell_ids()}
        filter_ids(change)

    def update_zaxes_range(change):
        global z_cid2exclude
        min_z, max_z = list(z_range_w.value)

        def _exclude(cid):
            z, y, x = barycenter[cid]
            if not min_z <= z <= max_z:
                return cid
            else:
                return None

        z_cid2exclude = {_exclude(cid) for cid in tg.cell_ids()}
        filter_ids(change)

    # -- Set the CHANGE observers:
    # Cell observers:
    node_w.observe(show_hide_node, names="value")
    ppty_w.observe(change_ppty, names="value")
    cmap_w.observe(update_cmap, names="value")
    nodesize_w.observe(update_node_marker_size, names="value")
    node_filter_w.observe(filter_ids, names="value")
    layer_w.observe(update_ids, names="value")

    # Cell-wall observers:
    edge_w.observe(show_hide_edge, names="value")
    edge_thick_w.observe(update_edge_thickness, names="value")

    # Range observers:
    x_range_w.observe(update_xaxes_range, names="value")
    y_range_w.observe(update_yaxes_range, names="value")
    z_range_w.observe(update_zaxes_range, names="value")

    # Highlight observers:
    update_high_cids_w.on_click(highlight_cids)

    cells_container = widgets.VBox([widgets.HBox([node_w, ppty_w, cmap_w, nodesize_w]),
                                    widgets.HBox([node_filter_w, layer_w])])
    walls_container = widgets.HBox([edge_w, edge_thick_w])  # widgets block
    range_container = widgets.HBox([x_range_w, y_range_w, z_range_w])  # widgets block
    highlight_container = widgets.HBox([highlight_cids_w, update_high_cids_w])  # widgets block

    w = widgets.VBox([cells_container, walls_container, highlight_container, fig_w, range_container])
    return fig_w, w


def temporal_tissue_graph_gui(ttg, **kwargs):
    """A GUI for exploring TemporalTissueGraph instances in a notebook.

    Parameters
    ----------
    ttg : timagetk.graphs.TemporalTissueGraph
        The temporal tissue graph to open in the GUI.

    Other Parameters
    ----------------
    width : int
        Width of the GUI, note that notebook usually have a max width of 1110px. Defaults to 950px.
    height : int
        Height of the GUI. Defaults to 800px.
    init_time_point : int
        The initial time-point to display. Defaults to ``0``.
    init_feature : str
        The default cell feature to display. Defaults to the first one on the list.
    exclude_identities : bool
        If ``True`` default, remove the cell identities from the dropdown list of cell properties. Else, keep them.

    Returns
    -------
    plotly.graph_objects.FigureWidget
        The figure widget.
    ipywidgets.widgets
        The widget controls.

    """
    width = kwargs.get('width', 950)  # figure width
    height = kwargs.get('height', 800)  # figure height

    # -- TIME widget:
    init_tp = kwargs.get('init_time_point', 0)
    max_tp = ttg.nb_time_points - 1
    time_w = widgets.IntSlider(description='Time-point:   ', value=init_tp, min=0, max=max_tp, step=1,
                               continuous_update=False)

    # Creates the 'layers' property in the graph if missing:
    if "layers" not in ttg.list_cell_properties():
        from timagetk.features.graph import add_cell_layers
        ttg = add_cell_layers(ttg)

    feature_list = _select_valid_cell_features(ttg._tissue_graph[time_w.value], kwargs.get('exclude_identities', True))
    # -- CELL widget:
    node_w, node_filter_w, ppty_w, layer_w, cmap_w, nodesize_w = _cell_widgets(ttg, feature_list, **kwargs)
    # -- CELL-WALL widget:
    edge_w, edge_thick_w = _wall_widgets(ttg, feature_list)

    global x_cid2exclude, y_cid2exclude, z_cid2exclude
    x_cid2exclude, y_cid2exclude, z_cid2exclude = set(), set(), set()

    def _filter_ids(time, layer, node_filtering=False):
        idx = ttg.cell_ids(time)
        if node_filtering:
            idx = set(idx) & set(get_cids_by_layers(ttg, list(layer), time))
        return list(set(idx) - x_cid2exclude - y_cid2exclude - z_cid2exclude)

    global idx
    idx = _filter_ids(time_w.value, layer_w.value, node_filter_w.value)
    global barycenter
    barycenter = ttg.cell_property_dict('barycenter', ttg.cell_ids(), default=[np.nan, np.nan, np.nan])
    global cwids
    cwids = ttg.cell_wall_ids(time_w.value, tcid_indexed=True)

    # Get global bounding-box, i.e. the min/max coordinates value for each axis for all time-points
    min_x, max_x, min_y, max_y, min_z, max_z = get_axes_range(*get_node_coordinates(barycenter))
    # Get x, y & z coordinates:
    x, y, z = get_node_coordinates(barycenter, idx)
    mini_z, maxi_z, mini_y, maxi_y, mini_x, maxi_x = get_axes_range(z, y, x)
    global x_range_w, y_range_w, z_range_w
    x_range_w = widgets.FloatRangeSlider(description='x-range:   ', min=min_x, max=max_x, step=2.0,
                                         value=[mini_x, maxi_x], continuous_update=True)
    y_range_w = widgets.FloatRangeSlider(description='y-range:   ', min=min_y, max=max_y, step=2.0,
                                         value=[mini_y, maxi_y], continuous_update=True)
    z_range_w = widgets.FloatRangeSlider(description='z-range:   ', min=min_z, max=max_z, step=2.0,
                                         value=[mini_z, maxi_z], continuous_update=True)

    def _gui_get_ppty_dict(sel_ppty):
        if sel_ppty in ttg.list_identities():
            ppty_values = ttg.cell_property_dict(sel_ppty, ttg.cell_ids(time_w.value), default=False)
            ppty_values = {tcid: int(i) for tcid, i in ppty_values.items()}
        else:
            ppty_values = ttg.cell_property_dict(sel_ppty, ttg.cell_ids(time_w.value), default=np.nan)
        return ppty_values

    def _gui_get_ppty_range(sel_ppty):
        if sel_ppty in ttg.list_identities():
            return 0, 1
        else:
            all_ppty_values_dict = ttg.cell_property_dict(sel_ppty, default=np.nan)
            return np.nanmin(list(all_ppty_values_dict.values())), np.nanmax(list(all_ppty_values_dict.values()))

    # Get the cell property values to color the scatter plot:
    global ppty_values_dict, ppty_values, min_val, max_val, value_range_w
    ppty_values_dict = _gui_get_ppty_dict(ppty_w.value)
    ppty_values = [ppty_values_dict[tcid] for tcid in idx]
    min_val, max_val = _gui_get_ppty_range(ppty_w.value)
    # min_val_w = widgets.BoundedFloatText(value=min_val, min=min_val*1.1, max=max_val, step=0.1, description='Range min:', disabled=False)
    # max_val_w = widgets.BoundedFloatText(value=max_val, min=min_val_w.value, max=max_val*1.1, step=0.1, description='Range max:', disabled=False)
    value_range_w = widgets.FloatRangeSlider(description='value-range:   ', min=min_val, max=max_val,
                                             step=round((max_val - min_val) / 500, 2),
                                             value=[min_val, max_val], continuous_update=True)
    refresh_range_w = widgets.Button(
        description='Refresh',
        disabled=False,
        button_style='',  # 'success', 'info', 'warning', 'danger' or ''
        tooltip='Refresh range',
        icon="rotate-right"
    )

    def scatter_hover(ppty_dict, idx):
        hovertemplate = [f"time-point: {tcid[0]}" + "<br>" +
                         f"cell-id: {tcid[1]}" + "<br>" +
                         f"{ppty_w.value}: {round(ppty_dict[tcid], 3) if isinstance(ppty_dict[tcid], float) else ppty_dict[tcid]}" +
                         f"{ttg.cell_property_unit(ppty_w.value)}"
                         for tcid in idx]
        return hovertemplate

    marker_kwargs = {'size': nodesize_w.value, 'color': ppty_values, 'colorscale': cmap_w.value,
                     'colorbar': {'title': "Property"}}
    node_trace = go.Scatter3d(x=x, y=y, z=z, ids=idx, mode='markers', name='cells', marker=marker_kwargs,
                              hovertemplate=scatter_hover(ppty_values_dict, idx),
                              hoverinfo='text', showlegend=False)

    edge_x, edge_y, edge_z = get_edge_coordinates(cwids, barycenter, idx)
    edge_trace = go.Scatter3d(x=edge_x, y=edge_y, z=edge_z, mode='lines', name='edges',
                              line={'width': edge_thick_w.value, 'color': '#888'},
                              hoverinfo='none', showlegend=False)

    # -- Figure LAYOUT:
    layout_kwargs = {}
    if width is not None:
        layout_kwargs["width"] = width
    if height is not None:
        layout_kwargs["height"] = height

    camera = {'eye': {'x': 0, 'y': 0, 'z': -2.5}}  # Top-view

    def _aspect_ratio(mini_x, maxi_x, mini_y, maxi_y, mini_z, maxi_z):
        xr, yr, zr = maxi_x - mini_x, maxi_y - mini_y, maxi_z - mini_z  # to size the axes depending on their extent
        rmax = float(max(xr, yr, zr))  # get the max axes extent to use for ratio computation
        return {'x': xr / rmax, 'y': yr / rmax, 'z': zr / rmax}

    layout = go.Layout(autosize=False, hovermode="closest",
                       scene={'dragmode': 'orbit',
                              "aspectratio": _aspect_ratio(mini_x, maxi_x, mini_y, maxi_y, mini_z, maxi_z),
                              'xaxis': {'autorange': False},
                              'yaxis': {'autorange': False},
                              'zaxis': {'autorange': False}},
                       scene_camera=camera, **layout_kwargs)
    fig_w = go.FigureWidget(data=[node_trace, edge_trace], layout=layout)
    # fig_w.update_layout(scene_aspectmode='data')
    fig_w.layout.scene.xaxis.range = [mini_x, maxi_x]
    fig_w.layout.scene.yaxis.range = [mini_y, maxi_y]
    fig_w.layout.scene.zaxis.range = [mini_z, maxi_z]
    fig_w.layout.title = ppty_w.value

    def update_ids(change):
        filter_ids(change)
        show_hide_node(change)

    def change_ppty(change):
        global value_range_w, ppty_values_dict, ppty_values, min_val, max_val
        ppty_values_dict = _gui_get_ppty_dict(ppty_w.value)
        ppty_values = [ppty_values_dict[tcid] for tcid in idx]
        min_val, max_val = _gui_get_ppty_range(ppty_w.value)
        # Update the value range slider:
        value_range_w.min = min_val
        value_range_w.max = max_val
        value_range_w.value = [min_val, max_val]
        value_range_w.readout = False
        value_range_w.readout = True
        value_range_w.step = round((max_val - min_val) / 500, 2)
        update_ppty_data(change)

    def update_ppty_data(change):
        ppty_values = [ppty_values_dict[tcid] for tcid in idx]
        with fig_w.batch_update():
            fig_w.data[0].marker.size = nodesize_w.value
            fig_w.data[0].marker.color = ppty_values
            fig_w.data[0].marker.cmin = min_val
            fig_w.data[0].marker.cmax = max_val
            fig_w.data[0].marker.colorscale = cmap_w.value
            # fig_w.data[0].marker.colorbar.title = ppty_w.value
            fig_w.layout.title = ppty_w.value
            fig_w.data[0].hovertemplate = scatter_hover(ppty_values_dict, idx)

    def update_ppty_range(change):
        global min_val, max_val
        min_val, max_val = list(value_range_w.value)
        with fig_w.batch_update():
            fig_w.data[0].marker.cmin = min_val
            fig_w.data[0].marker.cmax = max_val

    def refresh_ppty_range(change):
        global min_val, max_val
        min_val, max_val = _gui_get_ppty_range(ppty_w.value)
        value_range_w.min = min_val
        value_range_w.max = max_val
        value_range_w.value = [min_val, max_val]
        with fig_w.batch_update():
            fig_w.data[0].marker.cmin = min_val
            fig_w.data[0].marker.cmax = max_val

    def update_cmap(change):
        with fig_w.batch_update():
            fig_w.data[0].marker.colorscale = cmap_w.value

    def show_hide_node(change):
        with fig_w.batch_update():
            if node_w.value:
                fig_w.data[0].x, fig_w.data[0].y, fig_w.data[0].z = get_node_coordinates(barycenter, idx)
                update_ppty_data(change)
                show_hide_edge(change)
            else:
                fig_w.data[0].x = [None]
                fig_w.data[0].y = [None]
                fig_w.data[0].z = [None]
                fig_w.data[0].marker = {}

    def show_hide_edge(change):
        with fig_w.batch_update():
            if edge_w.value:
                fig_w.data[1].x, fig_w.data[1].y, fig_w.data[1].z = get_edge_coordinates(cwids, barycenter, idx)
            else:
                fig_w.data[1].x = [None]
                fig_w.data[1].y = [None]
                fig_w.data[1].z = [None]

    def update_node_marker_size(change):
        with fig_w.batch_update():
            fig_w.data[0].marker.size = nodesize_w.value

    def update_edge_thickness(change):
        with fig_w.batch_update():
            fig_w.data[1].line.width = edge_thick_w.value

    def filter_ids(change):
        global idx
        idx = _filter_ids(time_w.value, layer_w.value, node_filter_w.value)
        show_hide_node(change)

    def update_xaxes_range(change):
        global x_cid2exclude
        min_x, max_x = list(x_range_w.value)

        def _exclude(cid):
            x, _, _ = barycenter[cid]
            if not min_x <= x <= max_x:
                return cid
            else:
                return None

        x_cid2exclude = {_exclude(cid) for cid in ttg.cell_ids(time_w.value)}
        filter_ids(change)

    def update_yaxes_range(change):
        global y_cid2exclude
        min_y, max_y = list(y_range_w.value)

        def _exclude(cid):
            _, y, _ = barycenter[cid]
            if not min_y <= y <= max_y:
                return cid
            else:
                return None

        y_cid2exclude = {_exclude(cid) for cid in ttg.cell_ids(time_w.value)}
        filter_ids(change)

    def update_zaxes_range(change):
        global z_cid2exclude
        min_z, max_z = list(z_range_w.value)

        def _exclude(cid):
            _, _, z = barycenter[cid]
            if not min_z <= z <= max_z:
                return cid
            else:
                return None

        z_cid2exclude = {_exclude(cid) for cid in ttg.cell_ids(time_w.value)}
        filter_ids(change)

    def update_tissue_graph(change):
        global idx, x_range_w, y_range_w, z_range_w
        idx = _filter_ids(time_w.value, layer_w.value, node_filter_w.value)
        global cid2exclude, x_cid2exclude, y_cid2exclude, z_cid2exclude
        cid2exclude, x_cid2exclude, y_cid2exclude, z_cid2exclude = set(), set(), set(), set()
        global cwids
        cwids = ttg.cell_wall_ids(time_w.value, tcid_indexed=True)
        x, y, z = get_node_coordinates(barycenter, idx)
        # Reset axes range to new values:
        mini_z, maxi_z, mini_y, maxi_y, mini_x, maxi_x = get_axes_range(z, y, x)
        # Update aspect ratio
        aspect_ratio = _aspect_ratio(mini_x, maxi_x, mini_y, maxi_y, mini_z, maxi_z)
        with fig_w.batch_update():
            # update axes ranges:
            fig_w.layout.scene.xaxis.range = [mini_x, maxi_x]
            fig_w.layout.scene.yaxis.range = [mini_y, maxi_y]
            fig_w.layout.scene.zaxis.range = [mini_z, maxi_z]
            # update axes ratio
            fig_w.layout.scene.aspectratio = aspect_ratio
        change_ppty(change)
        show_hide_node(change)
        x_range_w.value = [mini_x, maxi_x]
        y_range_w.value = [mini_y, maxi_y]
        z_range_w.value = [mini_z, maxi_z]

    # -- Set the CHANGE observers:
    # Cell observers:
    node_w.observe(show_hide_node, names="value")
    ppty_w.observe(change_ppty, names="value")
    cmap_w.observe(update_cmap, names="value")
    nodesize_w.observe(update_node_marker_size, names="value")
    node_filter_w.observe(filter_ids, names="value")
    layer_w.observe(update_ids, names="value")
    value_range_w.observe(update_ppty_range, names="value")
    refresh_range_w.on_click(refresh_ppty_range)

    # Cell-wall observers:
    edge_w.observe(show_hide_edge, names="value")
    edge_thick_w.observe(update_edge_thickness, names="value")

    # Range observers:
    x_range_w.observe(update_xaxes_range, names="value")
    y_range_w.observe(update_yaxes_range, names="value")
    z_range_w.observe(update_zaxes_range, names="value")

    # Time-point observer
    time_w.observe(update_tissue_graph, names="value")

    cells_container = widgets.VBox([widgets.HBox([node_w, ppty_w, cmap_w, nodesize_w]),
                                    widgets.HBox([node_filter_w, layer_w, value_range_w, refresh_range_w])])
    walls_container = widgets.HBox([edge_w, edge_thick_w])
    range_container = widgets.HBox([x_range_w, y_range_w, z_range_w])  # widgets block
    time_container = widgets.HBox([time_w])

    w = widgets.VBox([cells_container, walls_container, time_container, fig_w, range_container])
    return fig_w, w


def _cell_widgets(tissue_graph, feature_list, **kwargs):
    # Show/Hide cell nodes:
    node_w = widgets.Checkbox(description="Show/hide cells", value=True)
    node_filter_w = widgets.Checkbox(description="Filter cells", value=False)
    # Cell feature widget:
    ppty_w = widgets.Dropdown(description='Property:   ', value=kwargs.get('init_feature', feature_list[0]),
                              options=feature_list)
    # Layer selection widget:
    layer_dict = tissue_graph.cell_property_dict('layers', default=None, only_defined=True)
    max_layer = max(list(layer_dict.values()))
    layer_w = widgets.IntRangeSlider(description='Layers:   ', min=1, max=max_layer, value=[1, max_layer])
    # Colormap widget:
    named_colorscales = sorted(px.colors.named_colorscales())
    cmap_w = widgets.Dropdown(description='Color map:   ', value='viridis', options=named_colorscales)
    # Markersize widget:
    nodesize_w = widgets.BoundedIntText(value=12, min=0, max=20, step=1, description='Node size:   ', disabled=False)
    return node_w, node_filter_w, ppty_w, layer_w, cmap_w, nodesize_w


def _wall_widgets(tissue_graph, feature_list):
    # Show/Hide cell-wall edges:
    edge_w = widgets.Checkbox(description="Show/hide edges", value=True)
    # Edge thickness widget:
    edge_thick_w = widgets.FloatSlider(description='Thickness:   ', value=2.0, min=0.5, max=8, step=0.2)
    return edge_w, edge_thick_w


def get_node_coordinates(barycenter, idx=None):
    """Get an array of points from a dictionary of cell barycenter coordinates.

    Parameters
    ----------
    barycenter : dict
        The dictionary of cell barycenter coordinates.
    idx : list, optional
        List of cell ids to use during node coordinates extraction.
        Defaults to use all keys of the `barycenter` dictionary.

    Returns
    -------
    numpy.ndarray
        The XYZ nodes (cells) coordinates.

    """
    if idx is None:
        idx = barycenter.keys()
    return np.array([barycenter[cid] for cid in idx]).T


def get_edge_coordinates(eids, barycenter, idx=None):
    """Get a 3-tuple of arrays giving the edges coordinates between cell barycenter coordinates.

    Parameters
    ----------
    eids : list of 2-tuple
        The list of pair of cells to 'link'.
    barycenter : dict
        The dictionary of cell barycenter coordinates.
     idx : list, optional
        List of cell ids to use during node coordinates extraction.
        Defaults to use all keys of the `barycenter` dictionary.

    Returns
    -------
    numpy.ndarray
        The nodes (cells) coordinates for the x-axis.
    numpy.ndarray
        The nodes (cells) coordinates for the y-axis.
    numpy.ndarray
        The nodes (cells) coordinates for the z-axis.
    """
    if idx is None:
        idx = barycenter.keys()
    edge_x = []
    edge_y = []
    edge_z = []
    for eid0, eid1 in eids:
        if eid0 not in idx or eid1 not in idx:
            continue
        try:
            x0, y0, z0 = barycenter[eid0]
        except:
            continue
        try:
            x1, y1, z1 = barycenter[eid1]
        except:
            continue
        edge_x.extend([x0, x1, None])  # None break the line
        edge_y.extend([y0, y1, None])  # None break the line
        edge_z.extend([z0, z1, None])  # None break the line
    return edge_x, edge_y, edge_z


def _select_valid_cell_features(tissue_graph, exclude_identities=True):
    """Return a list of scalar cell features.

    Parameters
    ----------
    tissue_graph : timagetk.graphs.TissueGraph, timagetk.graphs.TemporalTissueGraph
        The tissue graph from which to select cell features.
    exclude_identities : bool, optional
        If ``True`` (default), exclude the cell identities from the returned cell properties.
        Else, keep them in the list.

    Returns
    -------
    list
        List of cell features that are either scalar, bolean or string.

    Examples
    --------
    >>> from timagetk.visu.plotly_tissue_graph import _select_valid_cell_features
    >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
    >>> tg = example_tissue_graph_from_csv('p58', time_point=0)
    >>> _select_valid_cell_features(tg)
    ['area', 'epidermis_area', 'layers', 'number_of_neighbors', 'shape_anisotropy', 'volume']

    >>> from timagetk.visu.plotly_tissue_graph import _select_valid_cell_features
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
    >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
    >>> _select_valid_cell_features(ttg)
    ['affine_deformation_r2', 'area', 'division_rate', 'epidermis_affine_deformation_r2', 'epidermis_area', 'epidermis_areal_strain_rates', 'epidermis_strain_anisotropy', 'layers', 'number_of_neighbors', 'shape_anisotropy', 'volume', 'volumetric_strain_rates']

    """
    feature_list = sorted(tissue_graph.list_cell_properties())
    # Search for properties impossible to display (empty, list or array):
    non_scalar_ppty = []
    for ppty in feature_list:
        ppty_dict = tissue_graph.cell_property_dict(ppty, default=None, only_defined=True)
        if len(ppty_dict) == 0:
            # print(f"Excluding empty property '{ppty}'!")
            non_scalar_ppty.append(ppty)
            continue
        if not isinstance(list(ppty_dict.values())[0], (int, float, bool, str, np.int32, np.int64)):
            # print(f"Excluding non-scalar property '{ppty}'!")
            non_scalar_ppty.append(ppty)

    feature_list = sorted(tissue_graph.list_cell_properties())
    feature_list = sorted(set(feature_list) - set(non_scalar_ppty))
    if exclude_identities:
        # Remove cell identities from the list:
        identity_list = sorted(tissue_graph.list_identities())
        feature_list = list(set(feature_list) - set(identity_list))
    return sorted(feature_list)


def manual_selection(ttg, **kwargs):
    """A GUI for interactive annotation of TemporalTissueGraph instances in a notebook.

    Parameters
    ----------
    ttg : timagetk.graphs.TemporalTissueGraph
        The temporal tissue graph to open in the GUI.

    Other Parameters
    ----------------
    width : int
        Width of the GUI, note that notebook usually have a max width of 1110px. Defaults to 950px.
    height : int
        Height of the GUI. Defaults to 800px.
    init_time_point : int
        The initial time-point to display. Defaults to ``0``.

    Returns
    -------
    plotly.graph_objects.FigureWidget
        The figure widget.
    ipywidgets.widgets
        The widget controls.

    """
    width = kwargs.get('width', 950)  # figure width
    height = kwargs.get('height', 800)  # figure height

    # TIME widget:
    init_tp = kwargs.get('init_time_point', 0)
    max_tp = ttg.nb_time_points - 1
    time_w = widgets.IntSlider(description='Time-point:   ', value=init_tp, min=0, max=max_tp, step=1,
                               continuous_update=False)

    # Creates the 'layers' property in the graph if missing:
    if "layers" not in ttg.list_cell_properties():
        from timagetk.features.graph import add_cell_layers
        ttg = add_cell_layers(ttg)

    identity_list = ttg.list_identities()
    node_w, node_filter_w, ppty_w, layer_w, cmap_w, nodesize_w = _cell_widgets(ttg, identity_list)
    edge_w, edge_thick_w = _wall_widgets(ttg, None)

    global x_cid2exclude, y_cid2exclude, z_cid2exclude
    x_cid2exclude, y_cid2exclude, z_cid2exclude = set(), set(), set()

    def _filter_ids(time, layer, node_filtering=False):
        idx = ttg.cell_ids(time)
        if node_filtering:
            idx = set(idx) & set(get_cids_by_layers(ttg, list(layer), time))
        return list(set(idx) - x_cid2exclude - y_cid2exclude - z_cid2exclude)

    global idx
    idx = _filter_ids(time_w.value, layer_w.value, node_filter_w.value)
    global barycenter
    barycenter = ttg.cell_property_dict('barycenter', ttg.cell_ids(), default=[np.nan, np.nan, np.nan])
    global cwids
    cwids = ttg.cell_wall_ids(time_w.value, tcid_indexed=True)

    # Get global bounding-box, i.e. the min/max coordinates value for each axis for all time-points
    min_z, max_z, min_y, max_y, min_x, max_x = get_axes_range(*get_node_coordinates(barycenter))
    # Get x, y & z coordinates:
    z, y, x = get_node_coordinates(barycenter, idx)
    mini_z, maxi_z, mini_y, maxi_y, mini_x, maxi_x = get_axes_range(z, y, x)
    global x_range_w, y_range_w, z_range_w
    x_range_w = widgets.FloatRangeSlider(description='x-range:   ', min=min_x, max=max_x, step=2.0,
                                         value=[mini_x, maxi_x], continuous_update=True)
    y_range_w = widgets.FloatRangeSlider(description='y-range:   ', min=min_y, max=max_y, step=2.0,
                                         value=[mini_y, maxi_y], continuous_update=True)
    z_range_w = widgets.FloatRangeSlider(description='z-range:   ', min=min_z, max=max_z, step=2.0,
                                         value=[mini_z, maxi_z], continuous_update=True)

    # Get the list of cell with the identity to color the scatter plot:
    global selected_cids
    identity_dict = ttg.cell_property_dict(ppty_w.value, ttg.cell_ids(time_w.value), default=False)
    selected_cids = [tcid for tcid, identity in identity_dict.items() if identity]

    def scatter_hover(selected_cids):
        hovertemplate = [f"time-point: {tcid[0]}" + "<br>" + f"cell-id: {tcid[1]}" for tcid in selected_cids]
        return hovertemplate

    marker_kwargs = {'size': nodesize_w.value, 'color': ppty_values, 'colorscale': cmap_w.value,
                     'colorbar': {'title': "Property"}}
    node_trace = go.Scatter3d(x=x, y=y, z=z, ids=idx, mode='markers', name='cells', marker=marker_kwargs,
                              hovertemplate=scatter_hover(identity_dict),
                              hoverinfo='text', showlegend=False)

    edge_z, edge_y, edge_x = get_edge_coordinates(cwids, barycenter, idx)
    edge_trace = go.Scatter3d(x=edge_x, y=edge_y, z=edge_z, mode='lines', name='edges',
                              line={'width': edge_thick_w.value, 'color': '#888'},
                              hoverinfo='skip', showlegend=False)

    # -- Figure LAYOUT:
    layout_kwargs = {}
    if width is not None:
        layout_kwargs["width"] = width
    if height is not None:
        layout_kwargs["height"] = height

    camera = {'eye': {'x': 0, 'y': 0, 'z': -2.5}}  # Top-view

    def _aspect_ratio(mini_x, maxi_x, mini_y, maxi_y, mini_z, maxi_z):
        xr, yr, zr = maxi_x - mini_x, maxi_y - mini_y, maxi_z - mini_z  # to size the axes depending on their extent
        rmax = float(max(xr, yr, zr))  # get the max axes extent to use for ratio computation
        return {'x': xr / rmax, 'y': yr / rmax, 'z': zr / rmax}

    layout = go.Layout(autosize=False, hovermode="closest",
                       scene={'dragmode': 'orbit',
                              "aspectratio": _aspect_ratio(mini_x, maxi_x, mini_y, maxi_y, mini_z, maxi_z),
                              'xaxis': {'autorange': False},
                              'yaxis': {'autorange': False},
                              'zaxis': {'autorange': False}},
                       scene_camera=camera, **layout_kwargs)
    fig_w = go.FigureWidget(data=[node_trace, edge_trace], layout=layout)
    # fig_w.update_layout(scene_aspectmode='data')
    fig_w.layout.scene.xaxis.range = [mini_x, maxi_x]
    fig_w.layout.scene.yaxis.range = [mini_y, maxi_y]
    fig_w.layout.scene.zaxis.range = [mini_z, maxi_z]
    fig_w.layout.title = ppty_w.value

    global sel_color, sel_size, unsel_color, unsel_size
    sel_color, sel_size = '#ebba34', 20
    unsel_color, unsel_size = '#5c5c5c', 15

    # Initialize the "selection state":
    def _fig_update_selection():
        with fig_w.batch_update():
            fig_w.data[0].marker.color = [unsel_color if i not in selected_cids else sel_color for i in idx]
            fig_w.data[0].marker.size = [unsel_size if i not in selected_cids else sel_size for i in idx]

    # create our `on_click` callback function
    def update_selected_points(trace, points, state):
        global selected_cids
        # On click, you get the id of the selected point as a list in `points.point_inds`
        for i in points.point_inds:
            if i in selected_cids:
                # If it was in "selected state", unselect it:
                selected_cids.remove(i)
            else:
                # Else, select it
                selected_cids.append(i)
            _fig_update_selection()

    # def update_selected_points(trace, points, state):
    #     global selected_cids
    #     c = list(fig_w.data[0].marker.color)
    #     s = list(fig_w.data[0].marker.size)
    #     # On click, you get the id of the selected point as a list in `points.point_inds`
    #     for i in points.point_inds:
    #         if c[i] == sel_color:
    #             # If it was in "selected state", unselect it:
    #             c[i] = unsel_color
    #             s[i] = unsel_size
    #             selected_cids.remove(i)
    #         else:
    #             # Else, select it
    #             c[i] = sel_color
    #             s[i] = sel_size
    #             selected_cids.append(i)
    #         with fig_w.batch_update():
    #             fig_w.data[0].marker.color = c
    #             fig_w.data[0].marker.size = s

    fig_w.data[0].on_click(update_selected_points)

    def update_ids(change):
        filter_ids(change)
        show_hide_node(change)

    def change_identity(change):
        global selected_cids
        identity_dict = ttg.cell_property_dict(ppty_w.value, ttg.cell_ids(time_w.value), default=False)
        selected_cids = [tcid for tcid, identity in identity_dict.items() if identity]
        update_ppty_data(change)

    def update_ppty_data(change):
        _fig_update_selection()
        with fig_w.batch_update():
            fig_w.data[0].marker.colorscale = cmap_w.value
            fig_w.layout.title = ppty_w.value
            fig_w.data[0].hovertemplate = scatter_hover(selected_cids)

    def update_cmap(change):
        with fig_w.batch_update():
            fig_w.data[0].marker.colorscale = cmap_w.value

    def show_hide_node(change):
        with fig_w.batch_update():
            if node_w.value:
                fig_w.data[0].z, fig_w.data[0].y, fig_w.data[0].x = get_node_coordinates(barycenter, idx)
                update_ppty_data(change)
                show_hide_edge(change)
            else:
                fig_w.data[0].x = [None]
                fig_w.data[0].y = [None]
                fig_w.data[0].z = [None]
                fig_w.data[0].marker = {}

    def show_hide_edge(change):
        with fig_w.batch_update():
            if edge_w.value:
                fig_w.data[1].z, fig_w.data[1].y, fig_w.data[1].x = get_edge_coordinates(cwids, barycenter, idx)
            else:
                fig_w.data[1].x = [None]
                fig_w.data[1].y = [None]
                fig_w.data[1].z = [None]

    def update_node_marker_size(change):
        with fig_w.batch_update():
            fig_w.data[0].marker.size = nodesize_w.value

    def update_edge_thickness(change):
        with fig_w.batch_update():
            fig_w.data[1].line.width = edge_thick_w.value

    def filter_ids(change):
        global idx
        idx = _filter_ids(time_w.value, layer_w.value, node_filter_w.value)
        show_hide_node(change)

    def update_xaxes_range(change):
        global x_cid2exclude
        min_x, max_x = list(x_range_w.value)

        def _exclude(cid):
            z, y, x = barycenter[cid]
            if not min_x <= x <= max_x:
                return cid
            else:
                return None

        x_cid2exclude = {_exclude(cid) for cid in ttg.cell_ids(time_w.value)}
        filter_ids(change)

    def update_yaxes_range(change):
        global y_cid2exclude
        min_y, max_y = list(y_range_w.value)

        def _exclude(cid):
            z, y, x = barycenter[cid]
            if not min_y <= y <= max_y:
                return cid
            else:
                return None

        y_cid2exclude = {_exclude(cid) for cid in ttg.cell_ids(time_w.value)}
        filter_ids(change)

    def update_zaxes_range(change):
        global z_cid2exclude
        min_z, max_z = list(z_range_w.value)

        def _exclude(cid):
            z, y, x = barycenter[cid]
            if not min_z <= z <= max_z:
                return cid
            else:
                return None

        z_cid2exclude = {_exclude(cid) for cid in ttg.cell_ids(time_w.value)}
        filter_ids(change)

    def update_tissue_graph(change):
        global idx, x_range_w, y_range_w, z_range_w
        idx = _filter_ids(time_w.value, layer_w.value, node_filter_w.value)
        global cid2exclude, x_cid2exclude, y_cid2exclude, z_cid2exclude
        cid2exclude, x_cid2exclude, y_cid2exclude, z_cid2exclude = set(), set(), set(), set()
        global cwids
        cwids = ttg.cell_wall_ids(time_w.value, tcid_indexed=True)
        z, y, x = get_node_coordinates(barycenter, idx)
        # Reset axes range to new values:
        mini_z, maxi_z, mini_y, maxi_y, mini_x, maxi_x = get_axes_range(z, y, x)
        # Update aspect ratio
        aspect_ratio = _aspect_ratio(mini_x, maxi_x, mini_y, maxi_y, mini_z, maxi_z)
        with fig_w.batch_update():
            # update axes ranges:
            fig_w.layout.scene.xaxis.range = [mini_x, maxi_x]
            fig_w.layout.scene.yaxis.range = [mini_y, maxi_y]
            fig_w.layout.scene.zaxis.range = [mini_z, maxi_z]
            # update axes ratio
            fig_w.layout.scene.aspectratio = aspect_ratio
        change_identity(change)
        show_hide_node(change)
        x_range_w.value = [mini_x, maxi_x]
        y_range_w.value = [mini_y, maxi_y]
        z_range_w.value = [mini_z, maxi_z]

    # ------------------------------------------------------------------------------------------------------------------
    def export_to_ttg():
        """Add the `identity` to the `selection` of cell ids to the graph."""
        global ttg
        identity = ppty_w.value  # The name of the identity to modify.

        ttg.add_cell_identity(identity, selected_cids, strict=True, by_time_point=True)
        return

    def back_propagate():
        """Temporal propagation of cell identity to the ancestors."""
        global ttg
        time_point = time_w.value  # The starting time point to backward propagate.
        identity = ppty_w.value  # The name of the identity to modify.

        from timagetk.graphs.temporal_tissue_graph import propagate_cell_identity
        # Use the TemporalTissueGraph to propagate the cell identity to the ancestors:
        pred_id = propagate_cell_identity(ttg, identity, tp=time_point, method="ancestor")
        # Update the cell identity:
        ttg.add_cell_identity(identity, pred_id)  # add this cell identity dictionary to the TemporalTissueGraph
        return

    def expand_identity():
        """Expand identity to cells surrounded by it."""
        global ttg
        identity = ppty_w.value  # The name of the identity to modify.
        max_n_missing = 1  # max number of neighboring cell WITHOUT identity to declare it as missing identity

        expand_identity = {}
        n_round = 0
        while n_round == 0 or len(expand_identity[n_round]) != 0:
            n_round += 1
            expand_identity[n_round] = []
            for tcid in ttg.cell_ids():
                t, cid = tcid
                if not ttg.cell_has_identity(t, cid, identity):
                    neighbors = ttg.cell_neighbors(t, cid)
                    try:
                        neighbors.remove(1)
                    except:
                        pass
                    identities = [ttg.cell_has_identity(t, cid, identity) for cid in neighbors]
                    if sum(identities) >= len(neighbors) - max_n_missing:
                        expand_identity[n_round].append(tcid)
            log.info(f"Found {len(expand_identity[n_round])} surrounded cells at round #{n_round}!")
            ttg.add_cell_identity(identity, expand_identity[n_round])
        return

    def remove_identity():
        """Remove identity from isolated cells."""
        global ttg
        identity = ppty_w.value  # The name of the identity to modify.
        max_n_around = 0  # max number of neighboring cell WITH identity to declare it as isolated

        remove_identity = {}
        n_round = 0
        while n_round == 0 or len(remove_identity[n_round]) != 0:
            n_round += 1
            remove_identity[n_round] = []
            for tcid in ttg.cell_ids():
                t, cid = tcid
                if ttg.cell_has_identity(t, cid, identity):
                    neighbors = ttg.cell_neighbors(t, cid)
                    try:
                        neighbors.remove(1)
                    except:
                        pass
                    identities = [ttg.cell_has_identity(t, cid, identity) for cid in neighbors]
                    if sum(identities) <= max_n_around:
                        remove_identity[n_round].append(tcid)
            log.info(f"Found {len(remove_identity[n_round])} isolated cells at round #{n_round}!")
            ttg.remove_cell_identity(identity, remove_identity[n_round])
        return

    export_to_ttg_button = widgets.Button(description="Export to graph",
                                          tooltip="Export (replace) the current selection to the Tissue Graph.")
    export_to_ttg_button.on_click(export_to_ttg)
    back_propagate_button = widgets.Button(description="Back propagate",
                                           tooltip="Propagate the current identity to ancestors (starting from this time-point).")
    back_propagate_button.on_click(back_propagate)
    expand_identity_button = widgets.Button(description="Expand identity",
                                            tooltip="Expand the cell identity to cells surrounded by neighbors with this identity.")
    expand_identity_button.on_click(expand_identity)
    remove_identity_button = widgets.Button(description="Remove identity",
                                            tooltip="Remove the cell identity to cells surrounded by neighbors without this identity.")
    remove_identity_button.on_click(remove_identity)
    # ------------------------------------------------------------------------------------------------------------------

    # -- Set the CHANGE observers:
    # Cell observers:
    node_w.observe(show_hide_node, names="value")
    ppty_w.observe(change_identity, names="value")
    cmap_w.observe(update_cmap, names="value")
    nodesize_w.observe(update_node_marker_size, names="value")
    node_filter_w.observe(filter_ids, names="value")
    layer_w.observe(update_ids, names="value")

    # Cell-wall observers:
    edge_w.observe(show_hide_edge, names="value")
    edge_thick_w.observe(update_edge_thickness, names="value")

    # Range observers:
    x_range_w.observe(update_xaxes_range, names="value")
    y_range_w.observe(update_yaxes_range, names="value")
    z_range_w.observe(update_zaxes_range, names="value")

    # Time-point observer
    time_w.observe(update_tissue_graph, names="value")

    cells_container = widgets.VBox([widgets.HBox([node_w, ppty_w, cmap_w, nodesize_w]),
                                    widgets.HBox([node_filter_w, layer_w])])
    walls_container = widgets.HBox([edge_w, edge_thick_w])
    range_container = widgets.HBox([x_range_w, y_range_w, z_range_w])  # widgets block
    time_container = widgets.HBox([time_w])
    selection_container = widgets.HBox([export_to_ttg_button, back_propagate_button,
                                        expand_identity_button, remove_identity_button])

    w = widgets.VBox([cells_container, walls_container, time_container, fig_w, range_container, selection_container])
    return fig_w, w
