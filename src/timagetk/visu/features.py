#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import numpy as np
from matplotlib import pyplot as plt

from timagetk.bin.logger import get_logger
from timagetk.graphs.graph_analysis import DEFAULT_PPTY_VAL
from timagetk.graphs.graph_analysis import cell_property_by_group
from timagetk.graphs.graph_analysis import cell_property_by_subgroups
from timagetk.util import not_default_test

log = get_logger(__name__)


def boxplot_property_by_group(graph, group_name, property_name, cmap='Pastel1', figname=None, **kwargs):
    """Create a boxplot showing the cell property values by groups.

    Parameters
    ----------
    graph : timagetk.graphs.TissueGraph or timagetk.graphs.TemporalTissueGraph
        The (temporal) tissue graph with the `group_name` & `property_name` as "cell property".
    group_name : str
        The name of the cell group, defined as "cell property" in the `graph`, to use to group cell property values.
    property_name : str
        The name of the cell property, defined in the `graph`, to group.
    cmap : str, optional
        The name of the matplotlib colormap to use.
    figname : str, optional
        Full path (with file name and extension) to use to save the generated figure.

    Other Parameters
    ----------------
    group_id_subset : list or set
        A list or set of group ids to retain in the returned dictionary.
        If undefined, all the detected group ids are returned.
    group_fmt : str
        A string formatter for the group level.
    default_group_id : any
        The default value for the cell group, ``None`` by default.
    default_ppty_val : any
        The default value for the cell property, ``np.nan`` by default.
    only_defined_values : bool
        If ``True`` (default), return the value of a cell property if it is defined, _i.e._ not equal to `default_ppty_val`.
        Else, return the value of all cells defined in `group_name` and set it to `default_ppty_val` is non-existant in the `graph`.
    y_lims : (float, float)
        The lower and upper limits of the y-axis.

    Returns
    -------
    matplotlib.figure.Figure
        The `Figure` object.
    matplotlib.axes.Axes
        The `Axes` object.

    See Also
    --------
    timagetk.graphs.graph_analysis.cell_property_by_group

    Examples
    --------
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph
    >>> from timagetk.visu.features import boxplot_property_by_group
    >>> ttg = example_temporal_tissue_graph('sphere', features=['layers', 'volume'], extract_dual=False)
    >>> # Example - Plot cell volume by layers:
    >>> fig, ax = boxplot_property_by_group(ttg, 'layers', 'volume')

    """
    default_gid = kwargs.get('default_group_id', None)
    # Get the cell ids indexed dictionary of cell groups:
    group_dict = graph.cell_property_dict(group_name, default=default_gid, only_defined=False)
    # Get the associated set of group ids:
    group_ids = set(group_dict.values())
    n_groups = len(group_ids)

    if isinstance(property_name, str):
        fig, axes = plt.subplots(figsize=(n_groups, 6))
        axes = _boxplot_property_by_group(axes, graph, group_name, property_name, cmap, **kwargs)
    else:
        n_ppty = len(property_name)
        y_lims = kwargs.get('y_lims', [None]*n_ppty)
        fig, axes = plt.subplots(ncols=n_ppty, figsize=(n_ppty * 1.1 * n_groups, 6))
        for i, (ax, ppty) in enumerate(zip(axes, property_name)):
            kwargs['y_lims'] = y_lims[i]
            axes[i] = _boxplot_property_by_group(ax, graph, group_name, ppty, cmap, **kwargs)

    if figname is not None:
        # Save the figure:
        plt.savefig(figname)
        plt.close()

    return fig, axes


def _boxplot_property_by_group(ax, graph, group_name, property_name, cmap, **kwargs):
    """Create a boxplot showing the cell property values by groups.

    Parameters
    ----------
    ax : matplotlib.axes.Axes
        The `Axes` object to plot to.
    graph : timagetk.graphs.TissueGraph or timagetk.graphs.TemporalTissueGraph
        The (temporal) tissue graph with the `group_name` & `property_name` as "cell property".
    group_name : str
        The name of the cell group, defined as "cell property" in the `graph`, to use to group cell property values.
    property_name : str
        The name of the cell property, defined in the `graph`, to group.
    cmap : str or dict or list or numpy.ndarray
        If a string, should be the name of a matplotlib colormap to use.
        If a dictionary, should be a group-id indexed dictionary of RGB(A) colors to use.
        If a list or array, should be a group-id ordered sequence of RGB(A) colors to use.
    figname : str
        Full path (with file name and extension) to use to save the generated figure.

    Other Parameters
    ----------------
    cids : list or set
        The list of cell ids to get the cell property values and groups for.
    group_fmt : str
        A string formatter for the group level.
    group_id_subset : list or set
        A list or set of group ids to retain in the returned dictionary.
        If undefined, all the detected group ids are returned.
    default_group_id : any
        The default value for the cell group, ``None`` by default.
    default_ppty_val : any
        The default value for the cell property, ``np.nan`` by default.
    only_defined_ppty : bool
        If ``True``, return the value of a cell property if it is defined, _i.e._ not equal to `default_ppty_val`.
        Else, return the values of all cells defined in the `graph` and set it to `default_ppty_val` if not defined for selected `property_name`.

    Returns
    -------
    matplotlib.axes.Axes
        The updated `Axes` object.

    See Also
    --------
    timagetk.graphs.graph_analysis.cell_property_by_group

    """
    from typing import Iterable
    from matplotlib import colormaps as cm

    # Get the dictionary of cell property values by groups.
    ppty_by_g = cell_property_by_group(graph, group_name, property_name, **kwargs)
    group_ids = set(ppty_by_g.keys())
    n_groups = len(group_ids)
    default_ppty_val = kwargs.get('default_ppty_val', DEFAULT_PPTY_VAL)
    is_not_default = not_default_test(default_ppty_val)

    y_lims = kwargs.get('y_lims', None)
    if y_lims is not None:
        ax.set_ylim(*y_lims)

    # - Get the list of colors to apply to the boxes, if any:
    patch_artist = False
    if isinstance(cmap, str):
        cmap = cm.get_cmap(cmap).colors[:n_groups]
        patch_artist = True
    elif isinstance(cmap, dict):
        cmap = [cmap[gid] for gid in group_ids]
        patch_artist = True
    elif isinstance(cmap, Iterable):
        patch_artist = True

    # - Create the list of labels to use
    group_fmt = kwargs.get('group_fmt', "{}")
    group_labels = [group_fmt.format(gid) for gid in group_ids]

    # - Get the data to plot and the labels:
    data = [ppty_by_g[gid] for gid in group_ids]
    # Remove default values as the boxplot method may fail
    data = [[val for val in g_data if is_not_default(val)] for g_data in data]

    # - Add the number of VALID observations (not default) per group:
    n_data = [len(g_data) for g_data in data]
    group_labels = [label + f"\n(n={n_data[n]})" for n, label in enumerate(group_labels)]

    # - Create the boxplot:
    bplot = ax.boxplot(data, widths=0.4, showbox=True, labels=group_labels,
                       flierprops={"marker": '_', 'color': 'r'}, patch_artist=patch_artist)

    # - Add color to the boxplot, if any:
    if cmap is not None:
        for patch, color in zip(bplot['boxes'], cmap):
            patch.set_facecolor(color)

    # - Add a grid:
    ax.grid(linestyle='-.')

    # - Add the feature name as Y-axis label with unit:
    ppt_unit = graph.get_cell_property_unit(property_name)
    ax.set_xlabel(f"{group_name}", family='freesans', fontsize=13)
    ax.set_ylabel(f"{property_name} ({ppt_unit})", family='freesans', fontsize=13)
    # plt.tight_layout()

    return ax


def boxplot_property_by_subgroups(graph, group_name, subgroup_name, property_name, cmap='Pastel1', figname=None,
                                  **kwargs):
    """Create a boxplot showing the cell property values by groups & subgroups.

    Parameters
    ----------
    graph : timagetk.graphs.TemporalTissueGraph
        The (temporal) tissue graph with the `group_name`, `subgroup_name` & `property_name` as "cell property".
    group_name : str
        The name of the cell group, defined as "cell property" in the `graph`, to use to group cell property values.
    subgroup_name : str
        The name of the cell subgroup, defined as "cell property" in the `graph`, to use to group cell property values.
    property_name : str
        The name of the cell property, defined in the `graph`, to group.
    cmap : str, optional
        The name of the matplotlib colormap to use.
        Defaults to 'Pastel1'.
    figname : str, optional
        If set, defaults to ``None``, full path (with file name and extension) to use to save the generated figure.

    Other Parameters
    ----------------
    sharey : bool
        If ``True`` (default), share the y-axis range between all groups.
        Else, individualy resize the y-axis range of each group.
    group_id_subset : list or set
        A list or set of group ids to retain in the returned dictionary.
        If undefined, all the detected group ids are returned.
    subgroup_id_subset : list or set
        A list or set of subgroup ids to retain in the returned dictionary.
        If undefined, all the detected subgroup ids are returned.
    group_fmt : str
        A string formatter for the group level.
    subgroup_fmt : str
        A string formatter for the subgroup level.
    default_group_id : any
        The default value for the cell group, ``None`` by default.
    default_ppty_val : any
        The default value for the cell property, ``np.nan`` by default.
    only_defined_values : bool
        If ``True`` (default), return the value of a cell property if it is defined, _i.e._ not equal to `default_ppty_val`.
        Else, return the value of all cells defined in `group_name` and set it to `default_ppty_val` is non-existant in the `graph`.

    Returns
    -------
    matplotlib.figure.Figure
        The `Figure` object.
    matplotlib.axes.Axes
        The `Axes` object.

    See Also
    --------
    timagetk.graphs.graph_analysis.cell_property_by_subgroups

    Examples
    --------
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph
    >>> from timagetk.visu.features import boxplot_property_by_subgroups
    >>> ttg = example_temporal_tissue_graph('sphere', features=['layers', 'volume'], extract_dual=False)
    >>> # Example - Plot cell volume by layers:
    >>> from timagetk.visu.features import boxplot_property_by_subgroups
    >>> fig, ax = boxplot_property_by_subgroups(ttg, 'time-index', 'layers', 'volume', group_fmt="t{}", subgroup_fmt="L{}")

    """
    # Get the dictionary of cell property values by groups.
    ppty_by_sg = cell_property_by_subgroups(graph, group_name, subgroup_name, property_name, **kwargs)
    group_ids = list(set(ppty_by_sg.keys()))
    subgroup_ids = list(set(ppty_by_sg[group_ids[0]].keys()))
    n_groups = len(group_ids)
    n_subgroups = len(subgroup_ids)
    default_ppty_val = kwargs.get('default_ppty_val', DEFAULT_PPTY_VAL)
    is_not_default = not_default_test(default_ppty_val)

    # - Get the list of colors to apply to the boxes, if any:
    patch_artist = False
    if cmap is not None:
        from matplotlib import colormaps as cm
        cmap = cm.get_cmap(cmap).colors[:n_subgroups]
        patch_artist = True

    # - Create the list of labels to use
    subgroup_fmt = kwargs.get('subgroup_fmt', "{}")
    subgroup_labels = [subgroup_fmt.format(sgid) for sgid in subgroup_ids]

    # - Instantiate the figure:
    fig, axes = plt.subplots(ncols=n_groups, figsize=(n_groups * n_subgroups, 6), sharey=kwargs.get('sharey', True))
    for i, ax in enumerate(axes):
        gid = group_ids[i]
        # - Get the data to plot from the subgroups:
        data = [ppty_by_sg[gid][sgid] for sgid in subgroup_ids]
        # Remove default values as the boxplot method may fail
        data = [[val for val in g_data if is_not_default(val)] for g_data in data]

        # - Add the number of VALID observations (not default) per group:
        n_data = [len(g_data) for g_data in data]
        subgroup_labels_i = [label + f"\n(n={n_data[n]})" for n, label in enumerate(subgroup_labels)]

        # Create the boxplot:
        bplot = ax.boxplot(data, widths=0.4, showbox=True, labels=subgroup_labels_i, patch_artist=patch_artist,
                           flierprops={"marker": 'x', 'markerfacecolor': 'r', 'markeredgecolor': 'r'})

        # - Add color to the boxplot, if any:
        if cmap is not None:
            for patch, color in zip(bplot['boxes'], cmap):
                patch.set_facecolor(color)

        # - Add a grid:
        ax.grid(linestyle='-.')

        ppt_unit = graph.get_cell_property_unit(property_name)
        group_fmt = kwargs.get('group_fmt', "{}")
        ax.set_title(group_fmt.format(gid), fontsize=14)
        ax.set_xlabel(subgroup_name, family='freesans', fontsize=13)
        if i == 0:
            # Add the feature name as Y-axis label with unit:
            ax.set_ylabel(f"{property_name} ({ppt_unit})", family='freesans', fontsize=13)

    plt.tight_layout()

    if figname is not None:
        # Save the figure:
        plt.savefig(figname)
        plt.close()

    return fig, axes


def pca_explained_variance(pca, figname=None):
    """Plot the total explained variance by components for a PCA.

    Parameters
    ----------
    pca : sklearn.decomposition.PCA
        A principal component analysis instance.
    figname : str, optional
        If set, defaults to ``None``, full path (with file name and extension) to use to save the generated figure.

    Other Parameters
    ----------------
    figsize : tuple
        A 2-tuple specifying the figure size.

    Returns
    -------
    matplotlib.figure.Figure
        The `Figure` object.
    matplotlib.axes.Axes
        The `Axes` object.

    """
    n_components = pca.n_components_
    ind = np.arange(1, n_components + 1)
    tot_exp_var = np.cumsum(pca.explained_variance_ratio_) * 100

    fig, ax = plt.subplots(figsize=(8, 4))
    ax.grid(linestyle='dashed')
    ax.plot(ind, tot_exp_var, marker='o')
    for n, i in enumerate(ind[:-1]):
        ax.text(i, tot_exp_var[n] - 2, f"{np.round(tot_exp_var[n], 1)}%", ha='left', va='top',
                fontweight='bold', fontfamily="monospace")
    ax.set_title('Cumulative explained variance')
    ax.fill_between(ind, 0, tot_exp_var, alpha=0.5)
    ax.set_xticks(ind)
    ax.set_xticklabels(ind)
    ax.set_xlabel('Number of Principal Component')
    ax.set_ylabel('Total Explained Variance (%)')
    ax.set_xlim([1, n_components])
    ax.set_ylim([0, 105])

    if figname is not None:
        plt.savefig(figname)
        plt.close()

    return fig, ax


def _pca_correlation_circle(ax, pca, feature_names, pc=None, step=0.25, arrow_size=0.05):
    from matplotlib.patches import Circle

    if pc is None:
        pc = (1, 2)
    comp1, comp2 = pc

    # Create the frame, set axes limits & title:
    ax.set_frame_on(False)
    ax.tick_params(top=False, bottom=False, left=False, right=False, labelbottom=False, labelleft=False)
    ax.axis('equal')
    ax.set_xlim([-1.1, 1.1])
    ax.set_ylim([-1.1, 1.1])
    ax.set_title(f'Principal components {comp1}&{comp2}')

    # Add principal components axes & arrow heads
    ax.axhline(y=0, xmin=0, xmax=1, color='gray')
    ax.axvline(x=0, ymin=0, ymax=1, color='gray')
    ax.arrow(0, 0.9, 0, 0.1, head_width=2/30., head_length=0.1, color='gray')
    ax.arrow(0.9, 0, 0.1, 0, head_width=2/30., head_length=0.1, color='gray')
    ax.set_xlabel(f'PC {comp2}')
    ax.set_ylabel(f'PC {comp1}', rotation='horizontal')

    # Add circles & ticks
    if step > 1 or step is None:
        step = 1
    radiuses = np.arange(step, 1. + step, step)
    circles = [Circle((0, 0), radius=r, fill=False,
                      edgecolor='darkgray' if r != 1 else 'gray',
                      linestyle='--' if r != 1 else '-') for r in radiuses]
    [ax.add_artist(artist) for artist in circles]
    [ax.text(x, 0., x, ha='right', va='top') for x in np.arange(step, 1. + step, step)]
    # ax.text(0, 0., 0, ha='right', va='top')
    [ax.text(x, 0., x, ha='left', va='top') for x in np.arange(-1, 0, step)]
    # [ax.text(0, y, y, ha='right', va='top') for y in np.arange(step, 1.+step, step)]
    # [ax.text(0, y, y, ha='right', va='bottom') for y in np.arange(-1, 0, step)]

    # Add arrows with feature names:
    for i, feat in enumerate(feature_names):
        ax.arrow(0, 0,  # Start the arrow at the origin
                 pca.components_[comp1 - 1, i],
                 pca.components_[comp2 - 1, i],
                 head_width=arrow_size * 2/3., head_length=arrow_size, color='black')
        offset = 0.05
        x_offset = offset if pca.components_[comp1 - 1, i] > 0 else -offset
        y_offset = offset if pca.components_[comp2 - 1, i] > 0 else -offset
        ax.text(pca.components_[comp1 - 1, i] + x_offset,
                pca.components_[comp2 - 1, i] + y_offset,
                feat)

    return ax


def pca_correlation_circle(pca, feature_names, pc_range=(1, 2), figname=None, step=0.25):
    """Plot the PCA correlation circles for the features space.

    Parameters
    ----------
    pca : sklearn.decomposition.PCA
        A principal component analysis instance.
    feature_names : list of str
        The list of feature names used to build the PCA.
    pc_range : tuple, optional
        A len-2 tuple giveing the range of principal components you want to plot the correlation circles for.
        Defaults to ``(1,2)``, to plot the correlation circle for the first two principal components.
    figname : str, optional
        If set, defaults to ``None``, full path (with file name and extension) to use to save the generated figure.
    step : float, optional
        The step to use to plot intermediary circles.
        Defaults to ``0.25``.

    Returns
    -------
    matplotlib.figure.Figure
        The `Figure` object.
    matplotlib.axes.Axes
        The `Axes` object.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.components.clustering import example_clusterer
    >>> from sklearn.decomposition import PCA
    >>> from sklearn.preprocessing import RobustScaler
    >>> from timagetk.visu.features import pca_correlation_circle
    >>> clust = example_clusterer("p58")
    >>> clust.add_spatial_variable('shape_anisotropy', 'numeric')
    >>> clust.add_spatial_variable('volume', 'numeric')
    >>> clust.add_temporal_variable("log_relative_value('volume',1)",'numeric')
    >>> features = ["shape_anisotropy", "volume", "log_relative_value('volume',1)"]
    >>> arr, ids, features = clust.get_features_array(features, exclude_outliers=False)
    >>> arr = arr[~np.any(np.isnan(arr), axis=1), :]  # remova any line (individual) with a NaN value
    >>> lq, hq = 1/10., 9/10.  # 80% of the data
    >>> scaled_arr = RobustScaler(quantile_range=(lq*100, hq*100)).fit_transform(arr)
    >>> pca = PCA().fit(scaled_arr)
    >>> fig, axes = pca_correlation_circle(pca, features, (1,3))

    """
    from typing import Iterable

    pc_pairs = list(zip(range(pc_range[0], pc_range[1]), range(pc_range[0] + 1, pc_range[1] + 1)))
    n_subplots = len(pc_pairs)
    fig, axes = plt.subplots(ncols=n_subplots, figsize=(6 * n_subplots, 5.5))
    plt.suptitle(f"Features correlation circle{'s' if n_subplots > 1 else ''}")

    if not isinstance(axes, Iterable):
        axes=[axes]

    for n, pcs in enumerate(pc_pairs):
        axes[n] = _pca_correlation_circle(axes[n], pca, feature_names, pcs, step)

    if figname is not None:
        plt.savefig(figname)
        plt.close()
    else:
        plt.show()

    return fig, axes
