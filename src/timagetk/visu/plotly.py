#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Plotly based graph methods.

See `timagetk/notebooks/plotly_examples.ipynb` for examples.
"""

import numpy as np
import plotly.express as px
import plotly.graph_objects as go
from ipywidgets import widgets
from vt import vtImage
from vt.image import Image

import timagetk
from timagetk import LabelledImage
from timagetk.bin.logger import get_logger

log = get_logger(__name__)


def simple_z_slider(img, live=False, mini=0, maxi=255, colorscale='gray', start_slice=0):
    """A simple image z-slider.

    Parameters
    ----------
    img : numpy.ndarray or vt.vtImage or timagetk.SpatialImage
        The image to plot.
    live : bool, optional
        If ``True``, default ``False``, do a continuous update of the figure as the slider moves.

    """
    if isinstance(img, (Image, vtImage)):
        arr = img.copy_to_array()
    else:
        assert isinstance(img, np.ndarray)
        arr = img

    hovertemplate = 'x: %{x}' + "<br>" + 'y: %{y}' + "<br>"

    if isinstance(img, LabelledImage):
        labels = img.labels()
        mini = min(labels)
        maxi = max(labels)
        hovertemplate += "label id: %{z}"
        labels = {'x': "X (voxels)", 'y': "Y (voxels)", 'color': "Label ID"}
    else:
        hovertemplate += "value: %{z}"
        labels = {'x': "X (voxels)", 'y': "Y (voxels)", 'color': "Intensity"}

    # Slider
    z_sh = arr.shape[0]
    slicer = widgets.IntSlider(
        value=start_slice, min=0., max=z_sh - 1, step=1.0,
        description='Slice:', continuous_update=live,
    )

    im = go.Heatmap(z=arr[start_slice, :, :], colorscale=colorscale, zmin=mini, zmax=maxi,
                    hovertemplate=hovertemplate, name="")

    layout = go.Layout(width=600, height=600, title='3D image slider')
    fig = go.FigureWidget(data=[im], layout=layout)

    def z_slicer(change):
        z_sl = slicer.value
        fig.data[0].z = arr[z_sl, :, :]

    slicer.observe(z_slicer, names="value")

    return fig, widgets.VBox([fig, slicer])


def blend_z_slider(img, seg_img, live=False, start_slice=0):
    """A simple image z-slider.

    Parameters
    ----------
    img : numpy.ndarray or vt.vtImage or timagetk.SpatialImage
        The image to plot.
    live : bool, optional
        If ``True``, default ``False``, do a continuous update of the figure as the slider moves.

    """
    if isinstance(img, (Image, vtImage)):
        arr = img.copy_to_array()
    else:
        assert isinstance(img, np.ndarray)
        arr = img
    maxi = seg_img.max()
    seg_img[seg_img == 0] = np.nan
    seg_img[seg_img == 1] = np.nan

    hovertemplate = 'x: %{x}' + "<br>" + 'y: %{y}' + "<br>"

    # Slider
    z_sh = arr.shape[0]
    slicer = widgets.IntSlider(
        value=start_slice, min=0., max=z_sh - 1, step=1.0,
        description='Slice:', continuous_update=live,
    )

    im = go.Heatmap(z=arr[start_slice, :, :], colorscale='gray', zmin=0, zmax=255,
                    hovertemplate=hovertemplate, name="intensity")

    sim = go.Heatmap(z=seg_img[start_slice, :, :], colorscale='rainbow', zmin=0, zmax=maxi,
                     hovertemplate=hovertemplate, name="label")

    layout = go.Layout(width=600, height=600, title='3D image slider')
    fig = go.FigureWidget(data=[im, sim], layout=layout)

    def z_slicer(change):
        z_sl = slicer.value
        fig.data[0].z = arr[z_sl, :, :]
        fig.data[1].z = seg_img[z_sl, :, :]

    slicer.observe(z_slicer, names="value")

    return fig, widgets.VBox([fig, slicer])


def stack_browser(img, slide_axis='z', cmap='greys', **kwargs):
    """Plotly stack browser.

    Parameters
    ----------
    img : timagetk.SpatialImage
        The image data. Supported array shapes are:
          - ``(M, N)``: an image with scalar data. The data is visualized using a colormap.
          - ``(M, N, 3)``: an image with RGB values.
          - ``(M, N, 4)``: an image with RGBA values, *i.e.* including transparency.
    slide_axis : {'x', 'y', 'z'}, optional
        Axis along which the image array is sliced to create an animation plot.
        Defaults to `'z'` axis.
    cmap : str, optional
        Name of the colormap to use.

    Other Parameters
    ----------------
    range_color : list of two numbers
        If provided, overrides auto-scaling on the continuous color scale, including overriding color_continuous_midpoint.
        Also overrides zmin and zmax. Used only for single-channel images.
    title : str
        The figure title.
    width : int
        The figure width in pixels.
    height : int
        The figure height in pixels.
    """
    try:
        assert cmap in list_colormap()
    except AssertionError:
        log.error(f"Unknown colormap '{cmap}'!")
        log.info(f"Valid colormap names are: {list_colormap()}")
    slide_axis = img.axes_order_dict[slide_axis.upper()]

    fig = px.imshow(img.get_array(), animation_frame=slide_axis, aspect='equal', binary_string=False,
                    color_continuous_scale=cmap, labels={'animation_frame': "slice"}, **kwargs)
    fig.update(data=[{'hovertemplate': "x: %{x}<br>y: %{y}<br>label: %{z}"}])
    return fig


def list_colormap():
    return px.colors.named_colorscales()


def plot_tissue_graph(tissue_graph, ppty=None, cmap='viridis', layers=None, filter_cids=None, exclude_cids=None,
                      layout_kwargs=None, **kwargs):
    """Display a tissue graph with a plotly scatter plot.

    Parameters
    ----------
    tissue_graph : timagetk.graphs.TissueGraph
        The tissue graph to represent as 3D scatter plot.
    ppty : str, optioanl
        A cell property to display as colormap on the markers.
    cmap : str, optional
        The colormap to use with the property values.
    layers : int or list, optional
        If defined, only cells in selected layers will be used.
    filter_cids : list, optional
        List of cell ids to keep in property display (the rest is set to NaN).
        All of them by default.
    exclude_cids : list, optional
        list of cell ids to exclude from representation.
        None by default.

    Other Parameters
    ----------------
    width : int
        Width of the figure, default to ``700``, passed to plotly layout.
    height : int
        Height of the figure, default to ``700``, passed to plotly layout.
    ppty_on_hover : bool
        If ``True`` (default), cell property will be displayed on hover.
        Else try to keep it to the minimum.
    marker_size : int or str
        If an integer, set the size of markers. (Spoiler: does not seem to work!).
        If a string, should correspond to a cell property that will be used to scale the marker sizes.
    colorbar_title : str
        Use this to replace the `ppty` in the colorbar name. The `ppty` str will be in the title.
    default : Any
        The default value to use for cell id without any associate value for the selected property.
        Defaults to `np.nan`.

    Examples
    --------
    >>> from timagetk.visu.plotly import plot_tissue_graph
    >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
    >>> tg = example_tissue_graph_from_csv('p58', time_point=0)
    >>> plot_tissue_graph(tg, ppty='volume', layers=1)

    >>> f_cids=tg.list_cells_with_identity('FloralMeristem')
    >>> e_cids=tg.list_cells_with_identity('stack_margin'))
    >>> plot_tissue_graph(tg, ppty='volume', layers=[1, 2], filter_cids=f_cids, exclude_cids=e_cids)

    """
    import plotly.express as px
    from timagetk.io.graph import to_dataframe
    from timagetk.features.graph import list_cells_in_layer

    msize = kwargs.get("marker_size", None)
    cb_title = kwargs.get('colorbar_title', None)
    width = kwargs.get('width', None)  # figure width
    height = kwargs.get('height', None)  # figure height
    discrete_cmap = kwargs.get('discrete_cmap', False)
    default = kwargs.get("default", np.nan)

    # -  List the properties to export on dataframe (contains the displayed data)
    if kwargs.get('ppty_on_hover', True):
        # -- Get all available cell properties to display them on hover
        # Exclude landmarks, median and axis related properties...
        features = [feat for feat in tissue_graph.list_cell_properties() if
                    'landmarks' not in feat and 'median' not in feat and 'axis' not in feat]
    else:
        # -- Get the required properties...
        # Starting with the barycenter, obviously...
        features = ["barycenter"]
        # Then the selected property:
        if ppty is not None:
            features += [ppty]
        # And the property controlling the marker size (if any)
        if isinstance(msize, str):
            features += [msize]

    cell_ids = tissue_graph.cell_ids()
    if layers is not None:
        cell_ids = list(set(cell_ids) & set(list_cells_in_layer(tissue_graph, layers)))
    if exclude_cids is not None:
        cell_ids = list(set(cell_ids) - set(exclude_cids))

    # - Get the cell related DataFrame
    cell_df, _, _, _ = to_dataframe(tissue_graph, cell_ids, cell_features=features, wall_features=[], default=default)
    cell_df.pop('neighbors')

    # - For cells not in `filter_cids`, set a default value
    if filter_cids is not None:
        filter_out_cids = list(set(cell_df.index) - set(filter_cids))
        cell_df.loc[filter_out_cids, ppty] = kwargs.get("default", np.nan)

    try:
        cell_df[ppty]
    except KeyError:
        unit = tissue_graph.get_cell_property_unit(ppty)
        ppty = f"{ppty} -- {unit}"

    # - Plotly express keyword argument for scatter plot:
    px_kwargs = {}
    # -- Defines the type of colormap to use (discrete/continuous):
    if cell_df[ppty].dtype in ['bool', 'str']:
        px_kwargs["color_discrete_map"] = cmap
    elif discrete_cmap and cell_df[ppty].dtype not in ['bool', 'str']:
        cell_df[ppty] = cell_df[ppty].astype(str)
        px_kwargs["color_discrete_map"] = cmap
    else:
        px_kwargs["color_continuous_scale"] = cmap
    # -- Defines how marker size is controlled:
    if isinstance(msize, str):
        px_kwargs["size"] = msize
        px_kwargs["size_max"] = 30
    elif isinstance(msize, int):
        px_kwargs["size"] = np.array([msize] * len(cell_df))
    # -- Add a colorbar title if any:
    if cb_title is not None:
        px_kwargs['title'] = ppty
        cell_df = cell_df.rename(columns={ppty: cb_title})
        ppty = cb_title
    # -- Get the cell id from the dataframe index
    idx = cell_df.index

    # - Create the scatter plot:
    fig = px.scatter_3d(cell_df, x="barycenter_x", y="barycenter_y", z="barycenter_z", color=ppty,
                        hover_name=idx, hover_data=cell_df.columns, **px_kwargs)

    # - Control the figure layout
    if layout_kwargs is None:
        layout_kwargs = {}
    # -- Figure width control:
    if width is not None:
        layout_kwargs["width"] = width
    # -- Figure height control:
    if height is not None:
        layout_kwargs["height"] = height
    # -- Update the figure layout
    camera = {'eye': {'x': 0, 'y': 0, 'z': -2.5}}  # Top-view
    fig.update_layout(scene={'dragmode': 'orbit'}, scene_camera=camera, **layout_kwargs)
    fig.show(config={'displayModeBar': True})


def clustering_cmap(plotly_discrete_cmap='Plotly'):
    """Return a dictionary mapping a Plotly qualitative color map."""
    pl_cm = eval(f"px.colors.qualitative.{plotly_discrete_cmap}")

    cmap_dict = {
        'nan': '#575757',  # DARK GRAY
        '-1.0': '#000000',  # BLACK
    }
    cmap_dict.update({str(float(n)): color for n, color in enumerate(pl_cm)})
    return cmap_dict


def pca_correlation_circle(pca, feature_names, pc_range=(1, 2), step=0.25):
    """Plot the PCA loading circle.

    Parameters
    ----------
    pca : sklearn.decomposition.PCA
        The PCA object to use to draw loading circle.
    feature_names : list of str
        List of feature names used to compute the PCA.
    pc_range : tuple, optional
        A len-2 tuple giveing the range of principal components you want to plot the correlation circles for.
        Defaults to ``(1,2)``, to plot the correlation circle for the first two principal components.

    Returns
    -------
    matplotlib.figure.Figure
        The `Figure` object.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.components.clustering import example_clusterer
    >>> from sklearn.decomposition import PCA
    >>> from sklearn.preprocessing import RobustScaler
    >>> from timagetk.visu.plotly import pca_correlation_circle
    >>> clust = example_clusterer("p58")
    >>> clust.add_spatial_variable('shape_anisotropy', 'numeric')
    >>> clust.add_spatial_variable('volume', 'numeric')
    >>> clust.add_temporal_variable("log_relative_value('volume',1)",'numeric')
    >>> features = ["shape_anisotropy", "volume", "log_relative_value('volume',1)"]
    >>> arr, ids, features = clust.get_features_array(features, exclude_outliers=False)
    >>> arr = arr[~np.any(np.isnan(arr), axis=1), :]  # remove any line (individual) with a NaN value
    >>> lq, hq = 1/10., 9/10.  # 80% of the data
    >>> scaled_arr = RobustScaler(quantile_range=(lq*100, hq*100)).fit_transform(arr)
    >>> pca = PCA().fit(scaled_arr)
    >>> pca_correlation_circle(pca, features)

    """
    from plotly.subplots import make_subplots
    labels = {i: f"PC {i + 1} ({var:.1f}%)" for i, var in enumerate(pca.explained_variance_ratio_ * 100)}

    pc_pairs = list(zip(range(pc_range[0], pc_range[1]), range(pc_range[0] + 1, pc_range[1] + 1)))
    n_subplots = len(pc_pairs)
    fig = make_subplots(rows=1, cols=n_subplots, subplot_titles=[f"PC {pc_i}&{pc_j}" for pc_i, pc_j in pc_pairs])

    if step > 1 or step is None:
        step = 1

    for i_fig, (pc_i, pc_j) in enumerate(pc_pairs):
        # Add unit circle:
        fig.add_shape(type="circle", xref="x", yref="y", x0=-1, y0=-1, x1=1, y1=1, line_color="LightSeaGreen",
                      row=1, col=i_fig + 1, name="")
        # Add step circles:
        radiuses = np.arange(step, 1., step)
        if len(radiuses) >= 1:
            for radius in radiuses:
                fig.add_shape(type="circle", xref="x", yref="y",
                              x0=-radius, y0=-radius, x1=radius, y1=radius,
                              line_color="LightSeaGreen", line_width=1, row=1, col=i_fig + 1, name="")

        text_positions = []
        for i, feature in enumerate(feature_names):
            fig.add_shape(type='line', x0=0, y0=0,
                          x1=pca.components_[pc_i - 1, i],
                          y1=pca.components_[pc_j - 1, i],
                          name=feature, row=1, col=i_fig + 1)
            xanchor = "left" if pca.components_[pc_i - 1, i] > 0 else "right"
            yanchor = "bottom" if pca.components_[pc_j - 1, i] < 0 else "top"
            text_positions.append(yanchor + " " + xanchor)
        fig.add_trace(go.Scatter(x=pca.components_[pc_i - 1, :], y=pca.components_[pc_j - 1, :],
                                 mode='text', text=feature_names, textposition=text_positions,
                                 hovertemplate="%{x:.3f}<br>%{y:.3f}<br>%{text}", name=""),
                      row=1, col=i_fig + 1)

        # Set axes properties
        fig.update_xaxes(range=[-1.1, 1.1], title=labels[i_fig], row=1, col=i_fig + 1, showgrid=False)
        fig.update_yaxes(range=[-1.1, 1.1], title=labels[i_fig + 1], row=1, scaleanchor="x", scaleratio=1,
                         col=i_fig + 1, showgrid=False)

    fig.update_layout(title="PCA correlation circles", width=500 * n_subplots, height=600,
                      xaxis={"dtick": step}, yaxis={"dtick": step}, legend={"visible": False})

    return fig


def bound(value, coef):
    return value * coef if value > 0 else value * (2 - coef)


KERNELS = ['gaussian', 'tophat', 'epanechnikov', 'exponential', 'linear', 'cosine']


def xy_density(data, bandwidth=0.2, kernel="gaussian", grid_resolution=100):
    """Compute a density estimation on a 2D scatter plot.

    Parameters
    ----------
    data : numpy.ndarray
        The Nx2 data to use to estimate density.
    bandwidth : float, optional
        The bandwidth of the kernel.
    kernel : {'gaussian', 'tophat', 'epanechnikov', 'exponential', 'linear', 'cosine'}
        The kernel to use. Defaults to `gaussian`.
    grid_resolution : int, optional
        The size of the XY grid to construct to estimate density. Defaults to `100`.

    Returns
    -------
    numpy.ndarray
        The x-coordinates at which the density was estimated.
    numpy.ndarray
        The y-coordinates at which the density was estimated.
    numpy.ndarray
        The `NxN` density matrix with `N = grid_resolution`.

    """
    import numpy as np
    from sklearn.neighbors import KernelDensity

    kde = KernelDensity(bandwidth=bandwidth, kernel=kernel)
    kde.fit(data)
    x, y = data.T
    x_bins = np.linspace(bound(x.min(), 0.9), bound(x.max(), 1.1), grid_resolution)
    y_bins = np.linspace(bound(y.min(), 0.9), bound(y.max(), 1.1), grid_resolution)

    X, Y = np.meshgrid(x_bins, y_bins)
    xy = np.vstack([Y.ravel(), X.ravel()]).T

    Z = np.exp(kde.score_samples(xy))
    Z = Z.reshape(X.shape)
    return x_bins, y_bins, Z


REG_MODELS = ["ols", "ransac"]


def regression(X, Y, method='ransac', order=1, resolution=1000):
    """Compute a regression, linear or polynomial.

    Parameters
    ----------
    X : numpy.ndarray
        The coordinates in the x-axis.
    Y : numpy.ndarray
        The coordinates in the y-axis.
    method : {"ols", "ransac"}, optional
        The estimation method to use. Defaults to `'ransac'`.
    order : int, optional
        The polynomial order, `1` (default) is linear.
    resolution : int, optional
        The number of points to use in the returned predicted coordinates.

    Returns
    -------
    numpy.ndarray
        The x-coordinates of the predicted regression model.
    numpy.ndarray
        The y-coordinates of the predicted regression model.
    str
        A string description of the predicted regression model.
    float
        The score of the predicted regression model.

    """
    from sklearn import linear_model
    from sklearn.pipeline import make_pipeline
    from sklearn.preprocessing import PolynomialFeatures

    if method.lower() == "ols":
        estimator = linear_model.LinearRegression()
    elif method.lower() == "ransac":
        estimator = linear_model.RANSACRegressor()
    else:
        raise ValueError(f"Unknown estimation method '{method}', should be in {REG_MODELS}!")

    model = make_pipeline(PolynomialFeatures(order), estimator)
    # -- Fit the model
    model.fit(X.reshape(-1, 1), Y.reshape(-1, 1))
    # -- Apply the predicted model on a range of values:
    x_pred = np.linspace(bound(X.min(), 0.9), bound(X.max(), 1.1), resolution)
    y_pred = model.predict(x_pred.reshape(-1, 1)).reshape(resolution)

    if method.lower() == "ols":
        coef = model[-1].coef_[0][1:]
        intercept = model[-1].intercept_[0]
    elif method.lower() == "ransac":
        coef = model[-1].estimator_.coef_[0][1:]
        intercept = model[-1].estimator_.intercept_[0]

    reg_str = "y = "
    for n, c in enumerate(coef):
        if n == 0:
            reg_str += f"{np.round(c, 3)}*x"
        else:
            reg_str += ' + ' if c > 0 else ' - '
            reg_str += f"{np.round(abs(c), 3)}*x^{n + 1}"
    reg_str += f"{' +' if intercept > 0 else ' -'} {np.round(abs(intercept), 3)}"

    return x_pred, y_pred, reg_str, model.score(Y.reshape(-1, 1), model.predict(X.reshape(-1, 1)))


def interactive_scatter_plot(df, cell_data, cell_groups):
    # -- DropDown widgets for data selection (X & Y axis):
    x_axis_w = widgets.Dropdown(description='X-axis:   ', value=cell_data[0], options=cell_data)
    y_axis_w = widgets.Dropdown(description='Y-axis:   ', value=cell_data[1], options=cell_data)
    points_color_w = widgets.Dropdown(description='Coloring:   ', value=cell_groups[0], options=cell_groups)
    # -- DropDown widgets for density contour map parameters:
    do_contour_w = widgets.Checkbox(description="Show contour", value=True)
    bandwith_w = widgets.BoundedFloatText(description='Bandwidth:   ', value=0.2, min=0.05, max=1, step=0.05,
                                          disabled=False)
    kernel_list = ['gaussian', 'tophat', 'epanechnikov', 'exponential', 'linear', 'cosine']
    kernel_w = widgets.Dropdown(description='Kernel:   ', value=kernel_list[0], options=kernel_list)
    # -- DropDown widgets for regression model selection:
    do_reg_w = widgets.Checkbox(description="Show regression", value=True)
    model_w = widgets.Dropdown(description='Model:   ', value=REG_MODELS[0], options=REG_MODELS)
    poly_w = widgets.BoundedIntText(description='Polynomial:   ', value=1, min=1, max=5, step=1, disabled=False)

    # Get x & y data:
    x = df[x_axis_w.value].to_numpy()
    y = df[y_axis_w.value].to_numpy()

    # -- Scatter plot:
    def scatter_hover():
        idx = [f"{x_axis_w.value}" + ": %{x:.2f}" + "<br>" +
               f"{y_axis_w.value}" + ": %{y:.2f}" + "<br>" +
               f"time-index: {t}<br>cell-id: {cid}" for t, cid in list(df.index)]  # for hovertext
        return idx

    def style_marker(df, column):
        if column == "None":
            points_color = {'color': 'rgba(0,0,0,0.4)', 'size': 3, 'showscale': False}
        else:
            if df[column].dtype == 'bool':
                color_data = df[column].astype(int).to_numpy()
            else:
                color_data = df[column].to_numpy()
            points_color = {'color': color_data, 'colorscale': 'Viridis', 'size': 3, 'showscale': True}
        return points_color

    scatter = go.Scatter(x=x, y=y, name='scatter plot', xaxis='x', yaxis='y',
                         mode='markers', marker=style_marker(df, points_color_w.value),
                         hovertemplate=scatter_hover(), hoverinfo='text')

    # -- Side histograms:
    histo_x = go.Histogram(y=y, histnorm='percent', xaxis='x2',
                           name=y_axis_w.value, marker={'color': 'rgba(0,0,0,1)'},
                           hovertemplate="%{y}<br>%{x:.2f}%", hoverinfo='name+text')
    histo_y = go.Histogram(x=x, histnorm='percent', yaxis='y2',
                           name=x_axis_w.value, marker={'color': 'rgba(0,0,0,1)'},
                           hovertemplate="%{x}<br>%{y:.2f}%", hoverinfo='name+text')
    # -- Density contour map:
    xb, yb, z = xy_density(df[[x_axis_w.value, y_axis_w.value]].to_numpy(), bandwith_w.value, kernel_w.value)
    contour = go.Contour(x=xb, y=yb, z=z, colorscale='Viridis', contours_coloring='heatmap',
                         reversescale=True, xaxis='x', yaxis='y', opacity=0.95, hoverinfo='none')
    # -- Linear Regression Models:
    xr, yr, reg_str, score = regression(x, y, method=model_w.value, order=poly_w.value)
    reg = go.Scatter(x=xr, y=yr, xaxis='x', yaxis='y', mode='lines',
                     hovertext=f"<b>{reg_str}</b> (R²={np.round(score, 3)})", hoverinfo='text')

    # -- Figure layout:
    layout = go.Layout(
        autosize=False, hovermode="closest",
        xaxis={'title': cell_data[0], 'zeroline': False, 'domain': [0, 0.85], 'showgrid': True},
        yaxis={'title': cell_data[1], 'zeroline': False, 'domain': [0, 0.85], 'showgrid': True},
        xaxis2={'title': '%', 'zeroline': True, 'domain': [0.85, 1], 'showgrid': False},
        yaxis2={'title': '%', 'zeroline': True, 'domain': [0.85, 1], 'showgrid': False},
        height=900, width=900, bargap=0, showlegend=False,
    )

    # Initialise the FigureWidget with all traces:
    fig_w = go.FigureWidget(data=[contour, reg, histo_x, histo_y, scatter], layout=layout)
    fig_w.layout.xaxis.range = [bound(x.min(), 0.9), bound(x.max(), 1.1)]
    fig_w.layout.yaxis.range = [bound(y.min(), 0.9), bound(y.max(), 1.1)]

    # Add equation of the fitted model to the graph as an annotation:
    # reg_annotation = dict(xref="paper", yref="paper", x=0., y=0.85,
    #                  text=f"<b>{reg_str}</b> (R²={np.round(score,3)})", align="center", showarrow=False,
    #                  font={'size': 15, 'color': '#EF553B', 'family':"Courier New, monospace"})
    # fig_w.add_annotation(reg_annotation)

    def response(change):
        """Update the graph (data, axes names, ranges, hovertext & equation.)."""
        x = df[x_axis_w.value].to_numpy()
        y = df[y_axis_w.value].to_numpy()
        xb, yb, z = xy_density(df[[x_axis_w.value, y_axis_w.value]].to_numpy(), bandwith_w.value, kernel_w.value)
        xr, yr, reg_str, score = regression(x, y, method=model_w.value, order=poly_w.value)
        with fig_w.batch_update():
            fig_w.data[4].x = x  # scatter plot
            fig_w.data[4].y = y  # scatter plot
            fig_w.data[4].marker = style_marker(df, points_color_w.value)  # scatter plot
            if points_color_w.value != 'None':
                tickvals = df[points_color_w.value].unique()
                fig_w.layout.coloraxis = {'colorbar': {'title': points_color_w.value, 'tickvals': tickvals}}
            # -- Update the data within the figure widget:
            fig_w.data[2].y = y  # y-axis histogram
            fig_w.data[2].name = y_axis_w.value  # update y-axis histogram hover text
            fig_w.data[3].x = x  # x-axis histogram
            fig_w.data[3].name = x_axis_w.value  # update x-axis histogram hover text
            fig_w.data[0].x = xb  # density-based contour map
            fig_w.data[0].y = yb  # density-based contour map
            fig_w.data[0].z = z  # density-based contour map
            fig_w.data[0].visible = do_contour_w.value
            fig_w.data[1].x = xr  # regression plot
            fig_w.data[1].y = yr  # regression plot
            fig_w.data[1].hovertext = f"<b>{reg_str}</b> (R²={np.round(score, 3)})"
            fig_w.data[1].visible = do_reg_w.value
            # Set the scatter plot axis labels:
            fig_w.layout.xaxis.title = x_axis_w.value
            fig_w.layout.yaxis.title = y_axis_w.value
            # Manually set the range to the same extent as the contour map:
            fig_w.layout.xaxis.range = [bound(x.min(), 0.9), bound(x.max(), 1.1)]
            fig_w.layout.yaxis.range = [bound(y.min(), 0.9), bound(y.max(), 1.1)]
            # Update equation plotly annotation:
        #         reg_annotation['text'] = f"<b>{reg_str}</b> (R²={np.round(score,3)})"
        #         fig_w.layout.update(annotations=[reg_annotation])
        #         fig_w.layout.barmode = 'overlay'
        return

    def update_point_color_w(change):
        """Deactivate contour plot if scatter points should be colored."""
        if points_color_w.value != 'None':
            do_contour_w.value = False
        response(change)
        return

    def update_contour_w(change):
        """Deactivate point coloring if contour plot is required."""
        if do_contour_w.value:
            points_color_w.value = 'None'
        response(change)
        return

    # -- Set the CHANGE observers:
    points_color_w.observe(update_point_color_w, names="value")
    x_axis_w.observe(response, names="value")
    y_axis_w.observe(response, names="value")
    do_contour_w.observe(update_contour_w, names="value")
    bandwith_w.observe(response, names="value")
    kernel_w.observe(response, names="value")
    do_reg_w.observe(response, names="value")
    model_w.observe(response, names="value")
    poly_w.observe(response, names="value")

    axis_container = widgets.HBox([x_axis_w, y_axis_w, points_color_w])  # AXIS widgets block
    density_container = widgets.HBox(children=[kernel_w, bandwith_w, do_contour_w])  # density map widget block
    regression_container = widgets.HBox(children=[model_w, poly_w, do_reg_w])  # regression model block

    w = widgets.VBox([widgets.Label("Scatter plot: "), axis_container,
                      widgets.Label("Contour plot: "), density_container,
                      widgets.Label("Regression model: "), regression_container,
                      fig_w])
    return fig_w, w


def _plotly_rgb_fmt(color):
    """Convert the rgb plotly string to [0., 1.] floating values."""
    color = color.lstrip('rgb(')
    color = color.replace(')', '')
    color = color.replace(' ', '')
    return [c / 255. for c in list(map(int, color.split(',')))]


# %%
def plotly_colormap(cmap: str) -> list[list[float]]:
    """Returns a list of RGB or RGBA colors as [0., 1.] floating values corresponding to a Plotly colormap."""
    from plotly.express import colors
    from matplotlib.colors import to_rgba
    cmap = getattr(colors.qualitative, cmap)
    if cmap[0].startswith('rgb'):
        cmap = [_plotly_rgb_fmt(color) for color in cmap]
    else:
        cmap = [list(to_rgba(color)) for color in cmap]
    return cmap


def plotly_mesh_data(image, label_mapping=None, cmap='Plotly', mesh_kwargs=None, **kwargs):
    """Create a list of plotly 3D meh data from a segmented image.

    Parameters
    ----------
    image : timagetk.TissueImage3D
        The segmented image to represent as a mesh.
    label_mapping : dict, optional
        A label indexed dictionary mapping the label to a value to display instead of the label.
    cmap : list or str or dict, optional
        A list of color of same size as the number of labels.
        If a string, must be a valid qualitative `plotly` colormap.
        If a dict, must map each label to a valid plotly color (hex value or 'rgb(x, x, x)' string).

    Other Parameters
    ----------------
    labels : list
        The list of labels to process.
    resampling_voxelsize : float
        The voxelsize into which to resample the image to extract cell meshes.
    smoothing_iterations : int
        The number of smoothing iterations to apply to each cell mesh.
    target_reduction: float
        The percentage of reduction to achieve by the decimation step.

    See Also
    --------
    timagetk.visu.pyvista.tissue_image_cell_polydatas : the pyvista based meshing method.
    plotly.express.colors.qualitative: the list of Plotly colormaps.

    Returns
    -------
    list[plotly.graph_objects.Mesh3d]
        The list of plotly 3D mesh to display.
        Each mesh is a dict with a `'name'` entry that can either the label or the corresponding mapped label if `label_mapping` is not `None`.
        As the unicity of the 'name' is not garanteed (if `label_mapping` is not `None`), the label is stored as the first element of the list in `'customdata'`.

    Examples
    --------
    >>> from timagetk.visu.plotly import plotly_mesh_data
    >>>
    """
    from copy import deepcopy
    from timagetk.visu.pyvista import tissue_image_cell_polydatas
    # Create the PolyData dictionary:
    cpd = tissue_image_cell_polydatas(image, **kwargs)
    # Get the list of labels:
    labels = list(cpd.keys())

    # Get the label indexed color dictionary:
    if isinstance(cmap, str):
        cmap = getattr(px.colors.qualitative, cmap, px.colors.qualitative.Plotly)
        if len(cmap) < len(labels):
            cmap = cmap * int(np.ceil(len(labels) / len(cmap)))
        cmap = {label: cmap[idx] for idx, label in enumerate(labels)}

    # Create the dictionary of 'names' associated to each mesh in the legend.
    if label_mapping is not None:
        names = {label: f"{label} ({label_mapping.get(label, 'NA')})" for label in labels}
    else:
        names = {label: f"{label}" for label in labels}

    # Default global styles (applied to all label):
    mesh_style = {'showlegend': True}
    # Label indexed dictionary of styles:
    mesh_styles = {label: deepcopy(mesh_style) for label in labels}
    # Update with mesh_kwarg dictionary:
    if isinstance(mesh_kwargs, dict):
        for mkw_k, mkw_v in mesh_kwargs.items():
            if isinstance(mkw_k, str) and isinstance(mkw_v, dict) and isinstance(list(mkw_v.keys())[0], int):
                # If it is a label indexed styling dictionary, e.g. `{'opacity': {2: 0.6, 3: 1.}}`
                # Add "styling" specifically by declared label:
                [mesh_styles[label].update({mkw_k: mkw_v[label]}) for label in mkw_v.keys()]
            else:
                # Add this "styling" to all labels:
                [mesh_styles[label].update({mkw_k: mkw_v}) for label in labels]

    mesh = []
    for label, pd in cpd.items():
        # Get the point coordinates:
        x, y, z = np.array(pd.points).T
        # Get the face coordinates:
        j, i, k = pd.faces.reshape((-1, 4))[:, 1:].T
        # Create the 3D mesh with Plotly:
        mesh.append(go.Mesh3d(x=x, y=y, z=z, i=i, j=j, k=k, name=names[label], customdata=[label],
                              color=cmap[label], **mesh_styles[label]))

    return mesh


def temporal_mesh_viewer(images, label_mapping=None, cmap='Plotly', **kwargs):
    """Create an interactive plotly 3D mesh data from a segmented image with a temporal slider.

    Parameters
    ----------
    image : dict of timagetk.TissueImage3D or dict of str
        A time-indexed dictionary of segmented image to represent as cell-meshes.
    label_mapping : dict, optional
        A time-indexed dictionary of label indexed dictionary mapping the label to a value to display instead of the label.
    cmap : str or dict, optional
        If a string, must be a valid qualitative `plotly` colormap.

    Other Parameters
    ----------------
    not_a_label : int
        The value defining the absence of label in the labelled images. Defaults to `0`.
    background : int
        The value defining the background label in the labelled images. Defaults to `1`.
    labels : list
        The list of labels to process. Defaults to `0`.
    resampling_voxelsize : float
        The voxelsize into which to resample the image to extract cell meshes. Defaults to `None`.
    smoothing_iterations : int
        The number of smoothing iterations to apply to each cell mesh. Defaults to `10`.
    target_reduction: float
        The percentage of reduction to achieve by the decimation step. Defaults to `0.5`.
    scene : dict
        The scene plotly dictionary.
        Defaults to `{"aspectmode": 'data', "dragmode": 'orbit'}`.
    margin: dict
        The margins plotly dictionary.
        Defaults to `{'l': 2, 'r': 2, 't': 30, 'b': 5}`.
    layout
        The layout plotly dictionary.
        Defaults to `{"width": 900, "height": 700, "barmode": 'overlay'}`.

    See Also
    --------
    timagetk.visu.plotly : the meshing method.
    plotly.express.colors.qualitative: the list of Plotly colormaps.

    Returns
    -------
    plotly.graph_objects.FigureWidget
        The figure widget.
    ipywidgets.widgets
        The widget controls.
    """
    from pathlib import Path
    from timagetk import TissueImage3D
    from timagetk.io import imread
    from timagetk.visu.util import greedy_colormap
    from matplotlib.colors import to_hex

    # Defines the figure layout to use:
    scene = {"aspectmode": 'data', "dragmode": 'orbit'} | kwargs.pop('scene', {})
    margin = {'l': 2, 'r': 2, 't': 30, 'b': 5} | kwargs.pop('margin', {})
    layout = {"width": 900, "height": 700, "barmode": 'overlay'} | kwargs.pop('layout', {})
    layout = layout | {"scene": scene, "margin": margin}

    # Get the Plotly colormap:
    plotly_cmap = plotly_colormap(cmap)
    # Create empty label mapping dictionary:
    if label_mapping is None:
        label_mapping = {tp: None for tp in images.keys()}

    meshes = {}
    fig_titles = {}
    for tp, image in images.items():
        if isinstance(image, (str, Path)):
            fig_titles[tp] = Path(image).name
            # Load the image:
            image = imread(image, TissueImage3D,
                           not_a_label=kwargs.get('not_a_label', 0),
                           background=kwargs.get('background', 1))
        elif not isinstance(image, timagetk.TissueImage3D):
            fig_titles[tp] = image.filename
            image = TissueImage3D(image,
                                  not_a_label=kwargs.get('not_a_label', 0),
                                  background=kwargs.get('background', 1))
        else:
            fig_titles[tp] = image.filename
        # Check the title is not empty or creates one:
        if fig_titles[tp] == "":
            fig_titles[tp] = f"Labelled image t{tp}"
        # Use a greedy colormap to clearly differentiate cell meshes:
        greedy_cmap = greedy_colormap(image, plotly_cmap)
        greedy_cmap = [to_hex(rgba) for rgba in greedy_cmap.colors]
        # Create the cell meshes for current image
        meshes[tp] = plotly_mesh_data(image, label_mapping[tp], cmap=greedy_cmap,
                                      resampling_voxelsize=kwargs.get('resampling_voxelsize', None),
                                      smoothing_iterations=kwargs.get('smoothing_iterations', 10),
                                      target_reduction=kwargs.get('target_reduction', 0.5))

    # We create a dictionary with (time-index, label) as key and cell-mesh as value:
    meshes = {(tp, mesh['customdata'][0]): mesh for tp, tmesh in meshes.items() for mesh in tmesh}
    # Get the min and max value of the time-slider:
    t_start, t_stop = min(images.keys()), max(images.keys())
    # Create the time-slider:
    time_w = widgets.IntSlider(value=0, min=t_start, max=t_stop, step=1, description="Time index:",
                               continuous_update=False)
    # Instantiate the figure widget:
    fig_w = go.FigureWidget(data=list(meshes.values()),
                            layout=go.Layout(layout | {"title": fig_titles[time_w.value]}))

    def mesh_visible(sel_time, mesh_keys):
        """Returns a boolean indicating if a mesh should visible depending on selected time-index and cell-mesh keys.

        Parameters
        ----------
        sel_time : int
            The selected time-index.
        mesh_keys : list
            The list of mesh-keys.

        Returns
        -------
        list of bool
            A list of boolean indicating if a mesh should visible.
        """
        return [mk[0] == sel_time for mk in mesh_keys]

    # Initialize the mesh visibility:
    visible = mesh_visible(time_w.value, meshes.keys())
    for data_idx, f_data in enumerate(fig_w.data):
        f_data.visible = visible[data_idx]

    def response(change):
        """Method to update the visible cell meshes depending on the selected time-index with the slider."""
        # Defines the list of visible mesh for currently selected time-index:
        visible = mesh_visible(time_w.value, meshes.keys())
        # Update the figure:
        with fig_w.batch_update():
            # Update the title to indicate the selected time-index:
            fig_w.layout.title = fig_titles[time_w.value]
            # Update the visible mesh data:
            for data_idx, f_data in enumerate(fig_w.data):
                f_data.visible = visible[data_idx]

    time_w.observe(response, names="value")
    # Create the widget by stack vertically the time-slider and the figure:
    w = widgets.VBox([time_w, fig_w])

    return fig_w, w
