from typing import Dict
from typing import List
from typing import Optional

import numpy as np
import pyvista as pv
import vtk
from joblib import Parallel
from joblib import delayed
from tqdm import tqdm

import timagetk
from timagetk import TissueImage3D
from timagetk.algorithms.resample import isometric_resampling
from timagetk.bin.logger import get_logger

log = get_logger(__name__)


def tissue_image_cell_polydatas(
    seg_img: TissueImage3D, labels: Optional[List[int]]=None,
    resampling_voxelsize: Optional[float]=None, use_resample: bool=True,
    smoothing_iterations: int=10, target_reduction: float=0.5, **kwargs
) -> Dict[int, pv.PolyData]:
    """Generate a PolyData object for each cell in the tissue

    The function computes for each cell in the label list a triangular mesh
    using the Marching Cubes algorithm and returns a dictionary mapping the
    cell label and the corresponding 3D polydata.

    The meshes can be decimated and smoothed according to the target reduction
    and smoothing iterations parameters respectively.

    Parameters
    ----------
    seg_img
        The 3D tissue image to visualize as cell meshes
    labels
        The list of cell labels to process
    resampling_voxelsize
        The voxelsize into which to resample the image to extract cell meshes
    use_resample
        Whether to use a resampling function instead of naively subsampling
    smoothing_iterations
        The number of smoothing iterations to apply to each cell mesh
    target_reduction
        The percentage of reduction to achieve by the decimation step

    Returns
    -------
    Dict[int, pyvista.PolyData]
        A label-indexed dictionary of the cell meshes.

    Examples
    --------
    >>> from timagetk.visu.pyvista import tissue_image_cell_polydatas
    >>> from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image
    >>> from timagetk import TissueImage3D
    >>> # Create synthetic spherical labelled images as TissueImage3D:
    >>> tissue = TissueImage3D(example_layered_sphere_labelled_image(extent=30), background=1, not_a_label=0)
    >>> cpd = tissue_image_cell_polydatas(tissue)
    """
    if resampling_voxelsize is not None:
        if use_resample:
            resampled_img = isometric_resampling(seg_img, value=float(resampling_voxelsize), interpolation='cellbased', cell_based_sigma=1)
            img_array = resampled_img.get_array(float)
            voxelsize = np.array(resampled_img.voxelsize)
            subsampling = np.array(voxelsize) / np.array(seg_img.voxelsize)
        else:
            subsampling = np.round([resampling_voxelsize / v for v in seg_img.voxelsize]).astype(int)
            img_array = seg_img.get_array(float)[::subsampling[0],::subsampling[1],::subsampling[2]]
            voxelsize = np.array(seg_img.voxelsize)*subsampling
    else:
        img_array = np.array(seg_img).astype(float)
        voxelsize = np.array(seg_img.voxelsize)
        subsampling = np.array([1., 1., 1.])

    if labels is None:
        labels = seg_img.cell_ids()
    else:
        labels = list(set(labels) & set(seg_img.cell_ids()))

    if len(labels) < 0:
        log.warning("Could not obtain cell labels to mesh!")
        return {}

    log.debug(f"  --> Computing {len(labels)} cell meshes with Marching Cubes")

    image_bboxes = seg_img.boundingbox(labels=labels)
    cell_bboxes = {}
    for c in tqdm(labels, unit='label', desc="Applying bounding boxes on image cells"):
        if c in image_bboxes and image_bboxes[c] is not None:
            cell_bboxes[c] = tuple(
                [slice(np.maximum(int(np.floor(image_bboxes[c][i].start / subsampling[i])) - 1, 0),
                       np.minimum(int(np.ceil(image_bboxes[c][i].stop / subsampling[i])) + 1, img_array.shape[i]),
                       None)
                 for i in range(3)])
        else:
            cell_bboxes[c] = tuple([slice(0, 0, None) for i in range(3)])
    cell_images = dict(zip(labels, [img_array[cell_bboxes[l]] == l for l in labels]))

    cell_polydatas = {}
    for c in tqdm(labels, unit='label', desc="Marching Cubes + Smoothing on cells"):
        if (cell_images[c].sum() != 0) and (np.logical_not(cell_images[c]).sum() != 0):
            image_data = pv.ImageData(dimensions=cell_images[c].shape, spacing=voxelsize)
            image_data.point_data['mask'] = cell_images[c].flatten(order="F")

            cell_polydatas[c] = image_data.contour(isosurfaces=[0.5], scalars='mask', method='marching_cubes')
            cell_polydatas[c].points += np.array([cell_bboxes[c][i].start * voxelsize[i] for i in range(3)])
            if smoothing_iterations > 0:
                cell_polydatas[c] = cell_polydatas[c].smooth_taubin(
                    feature_smoothing=False,
                    feature_angle=120.0,
                    pass_band=0.01,
                    boundary_smoothing=True,
                    n_iter=smoothing_iterations,
                    normalize_coordinates=True,
                )
            if target_reduction > 0:
                cell_polydatas[c] = cell_polydatas[c].decimate(
                    target_reduction=target_reduction,
                    volume_preservation=False
                )
        else:
            log.debug(f"      --> Cell {c} is missing!")
            cell_polydatas[c] = pv.PolyData()

    return cell_polydatas


def tissue_image_unstructured_grid(seg_img, labels: Optional[List[int]] = None,
                                   resampling_voxelsize: Optional[float] = None,
                                   cell_polydatas: Optional[Dict[int, pv.PolyData]] = None) -> pv.UnstructuredGrid:
    """Generate a UnstructuredGrid object representing the cells in the tissue.

    The function creates for each cell in the label list a triangular mesh and
    stores it as a polyhedral cell in a 3D complex object called unstructured
    grid. The original features computed on the cells are passed on as cell
    data fields on the unstructured grid.

    Parameters
    ----------
    seg_img
        The 3D tissue image to visualize as cell meshes
    labels
        The list of cell labels to display
    resampling_voxelsize
        The voxelsize into which to resample the image to extract cell meshes
    cell_polydatas
        A dictionary of the cell meshes

    Returns
    -------
    pyvista.UnstructuredGrid
        The mesh structure representing the cells in the image

    Examples
    --------
    >>> from timagetk.visu.pyvista import tissue_image_unstructured_grid
    >>> from timagetk import TissueImage3D
    >>> from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image
    >>> # Create synthetic spherical labelled images as TissueImage3D:
    >>> seg_img = TissueImage3D(example_layered_sphere_labelled_image(n_layers=2), not_a_label=0, background=1)
    >>> # Pre-compute the 'volume' cell feature:
    >>> volumes = seg_img.cells.volume()
    >>> # Create the unstructured grid representation of the cell meshes:
    >>> grid = tissue_image_unstructured_grid(seg_img, resampling_voxelsize=1)
    >>> # Plot the cell meshes:
    >>> import pyvista as pv
    >>> plotter = pv.Plotter()
    >>> plotter.add_mesh(grid, scalars='volume', cmap='inferno')
    >>> plotter.show()
    """
    # Start by creating the cell indexed dict of cell polydatas (cell-based mesh) if not defined:
    if cell_polydatas is None:
        cell_polydatas = tissue_image_cell_polydatas(seg_img, labels=labels, resampling_voxelsize=resampling_voxelsize)
    # If not defined, get the list of labels from the keys of the previous dict:
    if labels is None:
        labels = list(cell_polydatas.keys())
    # Make sure all values are `pv.PolyData` instances:
    cell_polydatas = {c: pv.PolyData(cpd) if not isinstance(cpd, pv.PolyData) else cpd for c, cpd in
                      cell_polydatas.items()}

    n_points = sum(c_pd.n_points for c_pd in cell_polydatas.values() if c_pd.n_faces_strict>0)
    points = np.zeros((n_points, 3))
    point_offset = 0
    cell_offset = 0
    cells = np.zeros(sum(c_pd.n_faces_strict * 4 + 2 for c_pd in cell_polydatas.values() if c_pd.n_faces_strict>0), dtype=int)
    cell_types = [pv.CellType.POLYHEDRON]*len([c_pd for c_pd in cell_polydatas.values() if c_pd.n_faces_strict>0])
    meshed_labels = []
    # tqdm might not be needed anymore here as it is became really fast (<1s)
    for label in tqdm(labels, unit="labels", desc="Gathering cell meshes"):
        c_pd = cell_polydatas[label]
        if c_pd.n_faces_strict > 0:
            points[point_offset:point_offset+c_pd.n_points, :] = c_pd.points
            cells[cell_offset] = c_pd.n_faces_strict * 4 + 1
            cells[cell_offset+1] = c_pd.n_faces_strict
            cells[cell_offset+2:cell_offset+2+c_pd.n_faces_strict*4] = c_pd.faces + point_offset*np.tile([0, 1, 1, 1], c_pd.n_faces_strict)
            point_offset += c_pd.n_points
            cell_offset += 2 + c_pd.n_faces_strict*4
            meshed_labels.append(label)

    grid = pv.UnstructuredGrid(cells, cell_types, points)

    grid.cell_data['label'] = meshed_labels
    grid.cell_data['display_label'] = [c % 256 for c in meshed_labels]
    for feature_name in seg_img.cells.feature_names():
        cell_feature = seg_img.cells.feature(feature_name)

        if len(cell_feature) > 0:
            if np.array(list(cell_feature.values())).dtype != np.dtype('O'):
                nan_value = np.nan * np.array(list(cell_feature.values())[0])
                cell_feature = np.array([cell_feature[c] if c in cell_feature else nan_value for c in meshed_labels])
                if cell_feature.ndim == 1 and cell_feature.dtype != np.dtype(object):  # scalar
                    grid.cell_data[feature_name] = list(cell_feature)
                    log.debug(f"  --> Adding scalar cell feature {feature_name}")
                elif cell_feature.ndim == 2 and cell_feature.shape[1] == 3:  # vector
                    grid.cell_data[feature_name] = list(cell_feature)
                    log.debug(f"  --> Adding vector cell feature {feature_name}")
                elif cell_feature.ndim == 3 and cell_feature.shape[1] == 3 and cell_feature.shape[2] == 3:  # tensor
                    grid.cell_data[feature_name] = list(cell_feature)
                    log.debug(f"  --> Adding tensor cell feature {feature_name}")

    return grid


def greedy_lookup_table(labelled_image, cmap='tab20', background=1):
    """Create a greedy lookup table for the given labelled image.

    Attempts to color a graph using as few colors as possible, where no neighbours of a node can have same color as the
    node itself.

    Parameters
    ----------
    labelled_image : timagetk.LabelledImage
        The segmented image to color.
    cmap : str
        Name of a matplotlib colormap to use for greedy coloring.
    background : int, optional
        Used only if the image is a LabelledImage instance.

    Returns
    -------
    pyvista.LookupTable
        A greedy colormap following the image labelling.

    Examples
    --------
    >>> from timagetk.visu.pyvista import greedy_lookup_table
    >>> from timagetk import TissueImage3D
    >>> from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image
    >>> from timagetk.visu.pyvista import tissue_image_unstructured_grid
    >>> # Create synthetic spherical labelled images as TissueImage3D:
    >>> seg_img = TissueImage3D(example_layered_sphere_labelled_image(n_layers=2), not_a_label=0, background=1)
    >>> # Create the unstructured grid representation of the cell meshes:
    >>> grid = tissue_image_unstructured_grid(seg_img, resampling_voxelsize=1)
    >>> greedy_lut = greedy_lookup_table(seg_img, cmap='tab10', background=1)
    >>> # Plot the cell meshes:
    >>> import pyvista as pv
    >>> plotter = pv.Plotter()
    >>> plotter.add_mesh(grid, scalars='label', cmap=greedy_lut)
    >>> plotter.show()
    """
    from matplotlib import colormaps as cm
    from timagetk.visu.util import greedy_labelling
    # Compute the greedy labelling color code dictionary:
    color_code = greedy_labelling(labelled_image, background=background)
    # Defines how many colors we need and get them from matplotlib:
    n_colors = len(np.unique(list(color_code.values())))
    color_list = list(map(list, cm.get_cmap(cmap).colors[0:n_colors]))
    # Check the colormap is big enough to represent all required unique colors:
    try:
        assert n_colors <= len(color_list)
    except AssertionError:
        n = len(color_list)
        raise ValueError(f"The colormap is too small ({n}) for the required number of greedy colors ({n_colors}).")

    # Defines the min and max label:
    mini, maxi = int(labelled_image.min()), int(labelled_image.max())

    lut_table = []
    for label in range(mini, maxi + 1):
        try:
            code = color_code[label]
        except KeyError:
            # rgba = [0., 0., 0., 1.]  # black
            rgba = [1., 1., 1., 1.]  # white
        else:
            rgba = color_list[code] + [1.]
        lut_table.append(rgba)

    lut_table = np.array(lut_table) * 255
    return pv.LookupTable(values=lut_table, scalar_range=[mini, maxi])


def opacity_func(low_threshold=100, high_threshold=255):
    """Linear opacity function on an 8-bit range.

    Parameters
    ----------
    low_threshold : int, optional
        The low threshold, clipping the lower part of the linear opacity function.
    high_threshold : int, optional
        The high threshold, clipping the upper part of the  linear opacity function.

    Returns
    -------
    list
        A list of 255 values, in `[0., 1.]`, to use as opacities.

    Examples
    --------
    >>> from timagetk.visu.pyvista import opacity_func
    >>> op = opacity_func(low_threshold=100)
    >>> len(op)
    255
    """
    step = 1 / float(high_threshold - low_threshold)
    return (np.repeat(0, low_threshold - 1).tolist() +
            np.arange(0., 1., step=step).tolist() +
            np.repeat(1., 255 - high_threshold + 1).tolist())


def intensity_image_volume(image: timagetk.SpatialImage, voxelsize=None):
    """Generate an ImageData representation of an intensity image.

    Parameters
    ----------
    image : timagetk.SpatialImage
        An intensity image to visualize.
    voxelsize : float or list of float, optional


    Returns
    -------
    pyvista.ImageData
        The PyVista structure to respresent an intensity image object.

    Examples
    --------
    >>> from timagetk.visu.pyvista import intensity_image_volume
    >>> from timagetk.visu.pyvista import opacity_func
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> image = imread(shared_data("p58-t0-INT_down_interp_2x.inr.gz", "p58_down_interp_2x", "fusion"))
    >>> img_grid = intensity_image_volume(image)
    >>> # Plot the intensity image:
    >>> import pyvista as pv
    >>> plotter = pv.Plotter()
    >>> plotter.add_volume(img_grid, cmap='gray_r', opacity=opacity_func(low_threshold=50))
    >>> plotter.show()
    >>> # Plot the intensity image with a widget to control the opacity lower threshold:
    >>> import pyvista as pv
    >>> plotter = pv.Plotter()
    >>> def low_opacity_threshold(value):
    >>>     plotter.add_volume(img_grid, cmap='gray_r', opacity=opacity_func(low_threshold=int(value)))
    >>>     return
    >>> plotter.add_slider_widget(low_opacity_threshold, [0, 100], title='Low opacity threshold')
    >>> plotter.show()

    """
    if voxelsize is not None:
        from timagetk.algorithms.resample import resample
        image = resample(image, voxelsize=voxelsize)

    sh = np.array(image.get_shape()) + 1
    vxs = image.get_voxelsize()
    ori = image.origin
    image_data = pv.ImageData(dimensions=sh, spacing=vxs, origin=ori)
    image_data.cell_data["values"] = image.get_array().flatten(order="F")
    return image_data
