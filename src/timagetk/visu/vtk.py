#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import numpy as np
import vtk
from visu_core.vtk.utils.image_tools import image_to_vtk_cell_polydatas
from visu_core.vtk.utils.matplotlib_tools import vtk_lookuptable_from_mpl_cmap
from visu_core.vtk.utils.polydata_tools import vtk_combine_polydatas
from vtk.util.numpy_support import vtk_to_numpy

from timagetk.bin.logger import get_logger
from timagetk.visu.util import convert_str_range

log = get_logger(__name__)


def intensity_image_viewer(image, colormap='gray', value_range='type', opacity=1., **kwargs):
    """3D rendering of an intensity image.

    Parameters
    ----------
    image : timagetk.SpatialImage
        3D intensity image to view.
    colormap : str, optional
        Colormap to use, see the notes for advised colormaps.
        Defaults to `'gray'`.
    value_range : (int, int) or {'type', 'auto'}, optional
        Minimum and maximum values to use for the colormap, can be given as a list of length 2 values.
        If None (default), set to 'auto' for a ``LabelledImage``, set to 'type' for a ``SpatialImage``.
        See the "Notes" section for detailled explanations.
    opacity : float, optional
        Opacity to give to the maximum value of the range, should be in ``[0., 1.]``.

    Other Parameters
    ----------------
    viewer_background : (float, float, float)
        The RGB color to use in the viewer for the background, values should be in ``[0., 1.]``.
        Defaults to 'black' (0., 0., 0.).
    get_actor : bool
        If ``True``, do not open the viewer but return the actor instead.

    Notes
    -----
    We advise to use the **sequential** colormaps, such as:

     - *Sequential (2)* [mplt_cmap_sequential]_: `'binary', 'gist_yarg', 'gist_gray', 'gray', 'bone', 'pink', 'summer', 'Wistia'`.
     - *Perceptually Uniform Sequential* [mplt_cmap_sequential2]_: `'viridis', 'plasma', 'inferno', 'magma', 'cividis'`.

    To understand the differences in "perception" induced by the different colormaps, see: [mplt_cmap_perception]_

    Accepted str for `val_range` can be:

     - 'auto': get the min and max value of the image;
     - 'type': get the maximum range from the `image.dtype`, *e.g.* 'uint8'=[0, 255];

    Examples
    --------
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.stack import stack_panel
    >>> from timagetk.visu.vtk import intensity_image_viewer
    >>> img = shared_data('flower_labelled', 0)
    >>> # Example 1: Use a 'gray' colormap:
    >>> intensity_image_viewer(img, 'gray')
    >>> # Example 2: Use a 'viridis' colormap, crop out the low intensity values and use a white background:
    >>> intensity_image_viewer(img, 'viridis', value_range=[20, 255], viewer_background=[1., 1., 1.])

    """
    from visu_core.vtk.utils.image_tools import vtk_image_data_from_image
    from visu_core.vtk.volume import vtk_image_volume
    from visu_core.vtk.display import vtk_display_actors
    viewer_background = kwargs.pop('viewer_background', 'k')

    # Check/convert the given `value_range`:
    if value_range is None:
        value_range = 'type'
    if isinstance(value_range, str):
        value_range = convert_str_range(image, value_range)

    image_data = vtk_image_data_from_image(image.get_array(), voxelsize=image.voxelsize)
    image_volume = vtk_image_volume(image_data, alpha_mode='intensity', background_label=1, colormap=colormap,
                                    value_range=value_range, opacity=opacity)

    if kwargs.get('get_actor', False):
        return [image_volume]
    else:
        return vtk_display_actors([image_volume], background=viewer_background)


def labelled_image_viewer(image, erode=None, **kwargs):
    """3D rendering of a labelled image.

    Parameters
    ----------
    image : timagetk.LabelledImage
        3D labelled image to view.
    erode : int, optional
        Erosion radius to use prior to rendering the volume.

    Other Parameters
    ----------------
    viewer_background : (float, float, float)
        The RGB color to use in the viewer for the background, values should be in ``[0., 1.]``.
        Defaults to 'black' (0., 0., 0.).
    get_actor : bool
        If ``True``, do not open the viewer but return the actor instead.

    Notes
    -----
    We advise to use the **sequential** colormaps, such as:

     - *Sequential (2)* [mplt_cmap_sequential]_: `'binary', 'gist_yarg', 'gist_gray', 'gray', 'bone', 'pink', 'summer', 'Wistia'`.
     - *Perceptually Uniform Sequential* [mplt_cmap_sequential2]_: `'viridis', 'plasma', 'inferno', 'magma', 'cividis'`.

    To understand the differences in "perception" induced by the different colormaps, see: [mplt_cmap_perception]_

    Accepted str for `val_range` can be:

     - 'auto': get the min and max value of the image;
     - 'type': get the maximum range from the `image.dtype`, *e.g.* 'uint8'=[0, 255];

    Examples
    --------
    >>> from timagetk import LabelledImage
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.stack import stack_panel
    >>> from timagetk.visu.vtk import labelled_image_viewer
    >>> img = imread(shared_dataset("p58", "segmented")[0], LabelledImage)
    >>> labelled_image_viewer(img, viewer_background=[1., 1., 1.])

    """
    from visu_core.vtk.utils.image_tools import vtk_image_data_from_image
    from visu_core.vtk.volume import vtk_image_volume
    from visu_core.vtk.display import vtk_display_actors
    viewer_background = kwargs.pop('viewer_background', 'k')

    if erode is not None and erode >= 1:
        from timagetk.algorithms.morphology import label_filtering
        image = label_filtering(image, "erosion", radius=erode)

    label_data = vtk_image_data_from_image(image.get_array() % 256 + (image.get_array() == 0).astype(image.dtype),
                                           voxelsize=image.voxelsize)
    label_volume = vtk_image_volume(label_data, colormap='glasbey', value_range=(0, 255), alpha_mode='label',
                                    background_label=1, opacity=kwargs.get('opacity', 1.))

    if kwargs.get('get_actor', False):
        return [label_volume]
    else:
        return vtk_display_actors([label_volume], background=viewer_background)


def meshed_image_viewer(image, erode=None, **kwargs):
    """3D rendering of a labelled image as a mesh.

    Parameters
    ----------
    image : timagetk.LabelledImage
        3D labelled image to view as a mesh.
    erode : int, optional
        Erosion radius to use prior to rendering the volume.

    Other Parameters
    ----------------
    viewer_background : (float, float, float)
        The RGB color to use in the viewer for the background, values should be in ``[0., 1.]``.
        Defaults to 'black' (0., 0., 0.).
    get_actor : bool
        If ``True``, do not open the viewer but return the actor instead.

    See Also
    --------
    visu_core.vtk.utils.image_tools.image_to_vtk_cell_polydatas

    Notes
    -----
    The keyword arguments are passed to ``visu_core.vtk.utils.image_tools.image_to_vtk_cell_polydatas``.

    We advise to use the **sequential** colormaps, such as:

     - *Sequential (2)* [mplt_cmap_sequential]_: `'binary', 'gist_yarg', 'gist_gray', 'gray', 'bone', 'pink', 'summer', 'Wistia'`.
     - *Perceptually Uniform Sequential* [mplt_cmap_sequential2]_: `'viridis', 'plasma', 'inferno', 'magma', 'cividis'`.

    To understand the differences in "perception" induced by the different colormaps, see: [mplt_cmap_perception]_

    Accepted str for `val_range` can be:

     - 'auto': get the min and max value of the image;
     - 'type': get the maximum range from the `image.dtype`, *e.g.* 'uint8'=[0, 255];

    Examples
    --------
    >>> from timagetk import LabelledImage
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.stack import stack_panel
    >>> from timagetk.visu.vtk import meshed_image_viewer
    >>> img = imread(shared_dataset("p58", "segmented")[0], LabelledImage)
    >>> meshed_image_viewer(img, viewer_background=[1., 1., 1.])

    """
    from visu_core.vtk.utils.image_tools import image_to_vtk_cell_polydatas
    from visu_core.vtk.utils.polydata_tools import vtk_combine_polydatas
    from visu_core.vtk.actor import vtk_actor
    from visu_core.vtk.display import vtk_display_actors
    viewer_background = kwargs.pop('viewer_background', 'k')

    if erode is not None and erode >= 1:
        from timagetk.algorithms.morphology import label_filtering
        image = label_filtering(image, "erosion", radius=erode)

    label_polydatas = image_to_vtk_cell_polydatas(image, labels=[c for c in image.labels() if c != 0], **kwargs)
    label_polydata = vtk_combine_polydatas([label_polydatas[c] for c in label_polydatas if c not in [0, 1]])
    label_actor = vtk_actor(label_polydata, colormap='glasbey', value_range=(0, 255))

    if kwargs.get('get_actor', False):
        return [label_actor]
    else:
        return vtk_display_actors([label_actor], background=viewer_background)


def blend_viewer(intensity_img, labelled_img, meshing=True, **kwargs):
    """3D rendering of an intensity and segmented images.

    Parameters
    ----------
    intensity_img : timagetk.SpatialImage
        3D intensity image to view.
    labelled_img : timagetk.LabelledImage
        3D labelled image to view.
    meshing : bool, optional
        If ``True``, use the meshed actor instead of the volume actor for the labelled image.

    Notes
    -----
    See ``intensity_image_viewer``, ``labelled_image_viewer`` & ``meshed_image_viewer`` for the possible keyword arguments.

    See Also
    --------
    timagetk.visu.vtk.intensity_image_viewer
    timagetk.visu.vtk.labelled_image_viewer
    timagetk.visu.vtk.meshed_image_viewer

    Examples
    --------
    >>> from timagetk import LabelledImage
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.stack import stack_panel
    >>> from timagetk.visu.vtk import blend_viewer
    >>> int_img = shared_data('flower_labelled', 0)
    >>> seg_img = imread(shared_dataset("p58", "segmented")[0], LabelledImage)
    >>> blend_viewer(int_img, seg_img, erode=1, value_range=[20, 255])
    >>> blend_viewer(int_img, seg_img, meshing=False, erode=1, colormap='viridis', value_range=[20, 255])

    """
    from visu_core.vtk.display import vtk_display_actors

    try:
        kwargs.pop('get_actor')
    except:
        pass

    actors = []
    if meshing:
        opacity = 1.
        actors += meshed_image_viewer(labelled_img, get_actor=True, **kwargs)
    else:
        opacity = 0.5
        actors += labelled_image_viewer(labelled_img, opacity=opacity, get_actor=True, **kwargs)

    actors += intensity_image_viewer(intensity_img, opacity=opacity, get_actor=True, **kwargs)

    return vtk_display_actors(actors, background=kwargs.get('viewer_background', 'k'))


class LabelledImageActor(vtk.vtkActor):
    """Class to represent labelled image with VTK.

    Attributes
    ----------
    background : int
        The value to treat as background, *i.e.* not a cell.
    seg_img : timagetk.LabelledImage
        The labelled image to mesh.
    cell_labels : list
        The list of labels found in the image.
    cell_polydatas : dict of vtk.vtkPolyData
        The dictionary of ``vtkPolyData`` associated to each label.

    Examples
    --------
    >>> from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image
    >>> from timagetk.features.cells import volume
    >>> from timagetk.visu.vtk import LabelledImageActor
    >>> from visu_core.vtk.display import vtk_display_actors
    >>> # Load an example image:
    >>> seg_img = example_layered_sphere_labelled_image()
    >>> # Compute the cell volumes:
    >>> cell_volumes = volume(seg_img)
    >>> # Create the VTK actor:
    >>> actor = LabelledImageActor(seg_img)
    >>> actor.update()
    >>> vtk_display_actors(actor, background='w')  # display
    >>> # Add the volume as a cell property:
    >>> actor.update(cell_property=cell_volumes, colormap='viridis')
    >>> vtk_display_actors(actor, background='w')  # display
    """

    def __init__(self, seg_img, background=1):
        """Constructor for the LabelledImageActor.

        Parameters
        ----------
        seg_img : timagetk.LabelledImage
            The labelled image to mesh.
        background : int, optional
            The value to treat as background, *i.e.* not a cell.
            Defaults to ``1``.
        """
        super().__init__()

        self.background = background

        self.seg_img = None
        self.cell_labels = None
        self.cell_polydatas = None

        self.set_labelled_image(seg_img)

    def set_labelled_image(self, seg_img):
        """Set the labelled image to use.

        Parameters
        ----------
        seg_img : timagetk.LabelledImage
            The labelled image to mesh.
        """
        self.seg_img = seg_img

        self.cell_labels = np.array([l for l in self.seg_img.labels() if l not in [self.background]])
        self.cell_polydatas = {}

    def update_polydatas(self, labels=None, subsampling_voxelsize=0.8, smoothing=10):
        """Update the cell polydata.

        Parameters
        ----------
        labels : list of int, optional
            The list of labels to update cell polydata.
            Defaults to ``None`` to update all cells.
        subsampling_voxelsize : float, optional
            The subsampling factor to apply.
            Default to ``0.8``.
        smoothing : int, optional
            The smoothing factor to apply.
            Defaults to ``10``.
        """
        if labels is None:
            labels = self.cell_labels
        subsampling = [int(np.ceil(subsampling_voxelsize / v)) for v in self.seg_img.voxelsize]
        self.cell_polydatas.update(image_to_vtk_cell_polydatas(self.seg_img,
                                                               labels=labels,
                                                               subsampling=subsampling,
                                                               cell_property=None,
                                                               smoothing=smoothing))

    def update(self, labels=None, cell_property=None, colormap='glasbey', value_range=None, opacity=1, linewidth=1,
               wireframe=False):
        """Update the vtk.vtkActor.

        Parameters
        ----------
        labels : list of int, optional
            The list of labels to update cell polydata.
            Defaults to ``None`` to update all cells.
        cell_property : dict, optional
            A dictionary of cell property to map on the actor.
            Used with the colormap instead of cell id if specified.
            Defaults to ``None``.
        colormap : str, optional
            The name of the colormap to use.
            Defaults to `'glasbley'`.
        value_range : [float, float], optional
            The value range to use as min & max for the colormap.
            Default to ``None`` to use the min and max of the cell property or label.
        opacity : float, optional
            Opacity of the actor in the display.
            Defaults to ``1``.
        linewidth : float, optional
            Width of the line.
            Defaults to ``1``.
        wireframe : bool, optional
            Render the actor in wireframe mode.
            Defaults to ``False``.
        """
        if labels is None:
            labels = self.cell_labels
        missing_labels = [l for l in labels if l not in self.cell_polydatas.keys()]
        if len(missing_labels) > 0:
            self.update_polydatas(labels=missing_labels)

        if cell_property is None:
            if value_range is None:
                value_range = (0, 255)

        if value_range is None:
            value_range = (np.nanmin([cell_property[l] for l in labels]),
                           np.nanmax([cell_property[l] for l in labels]))
        log.debug(value_range)

        display_polydatas = image_to_vtk_cell_polydatas(self.seg_img,
                                                        labels=labels,
                                                        cell_property=cell_property,
                                                        cell_polydatas=self.cell_polydatas)
        display_polydata = vtk_combine_polydatas([display_polydatas[l] for l in labels])

        scalars = vtk_to_numpy(display_polydata.GetCellData().GetArray(0))
        log.debug(scalars)

        lut = vtk_lookuptable_from_mpl_cmap(colormap, value_range)

        mapper = vtk.vtkPolyDataMapper()
        mapper.SetScalarModeToUseCellData()

        mapper.SetInputData(display_polydata)
        mapper.SetLookupTable(lut)

        self.SetMapper(mapper)

        self.GetProperty().SetOpacity(opacity)
        self.GetProperty().SetLineWidth(linewidth)
        if wireframe:
            self.GetProperty().SetRepresentationToWireframe()
