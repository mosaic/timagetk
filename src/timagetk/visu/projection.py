#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Projection methods to transform 3D image into 2D."""

import numpy as np

from timagetk.algorithms.reconstruction import image_surface_projection
from timagetk.algorithms.reconstruction import project_segmentation
from timagetk.bin.logger import get_logger
from timagetk.components.labelled_image import LabelledImage
from timagetk.components.spatial_image import SpatialImage
from timagetk.visu.mplt import plot_img_and_hist

log = get_logger(__name__)

#: List of valid intensity image projection methods
PROJECTION_METHODS = ['contour', 'background', 'maximum', 'average', 'minimum', 'max', 'mean', 'min']
#: Default intensity image projection method
DEF_PROJ_METHODS = PROJECTION_METHODS[0]


def projection(image, method='contour', axis='z', **kwargs):
    """Create a projection of an intensity or labelled 3D `image` to 2D along selected `axis`.

    Available intensity image projection `methods` are:

     - **contour**, detect the object contour and use voxel intensities along given `axis`;
     - **background**, detect the object contour using the background from segmentation along given `axis`;
     - **maximum** or **max**, only maximum values along given `axis` are kept;
     - **average** or **mean**, mean values along given `axis` are kept;
     - **minimum** or **min**, only minimum values along given `axis` are kept.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.LabelledImage
        Intensity or labelled image to project.
    method : str, optional
        Method to use for intensity image projection, see ``PROJECTION_METHODS`` for available methods.
    axis : str in {'x', 'y', 'z'}, optional
        Axis to use for projection. Defaults to `'z'`.

    Other Parameters
    ----------------
    threshold : int or float
        Consider only voxels with intensities superior to given threshold for object detection.
        By default, try to estimate it automatically.
    orientation: {-1, 1}
        Defines the orientation of the projection, try to guess it by default.
        If `-1` we consider the z-axis starts at the bottom of the object and goes upward.
        If `1` we consider the z-axis starts at the top of the object and goes downward.
    iso_upsampling : bool
        If ``True`` (default), up-sample the image to the minimum voxelsize.
        This is usefull if you have an axis with a lower resolution, typically the z-axis.
    altimap_smoothing : bool
        If ``True``, default is ``False``, smooth the altitude map obtained from the mask.
    gaussian_sigma : float
        Sigma value, in real units, of the Gaussian filter applied to the image.
        Skip this step by setting the value to ``0`` or ``None``. Defaults to ``1``.
        This value is divided by the image voxelsize.

    Returns
    -------
    timagetk.SpatialImage or timagetk.LabelledImage
        2D projection of the 3D stack.

    See Also
    --------
    timagetk.visu.projection.PROJECTION_METHODS
    timagetk.algorithms.reconstruction.image_surface_projection

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk import TissueImage3D
    >>> from timagetk.io import imread
    >>> from timagetk.visu.projection import projection
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> # - Get 'p58' shared intensity image:
    >>> img = imread(shared_dataset("p58", 'intensity', 0)[0])
    >>> projection_methods = ['contour', 'max', 'mean', 'min']
    >>> proj_list = [projection(img, method=meth, orientation=1) for meth in projection_methods]
    >>> fig = grayscale_imshow(proj_list, suptitle="Projection along z-axis", title=projection_methods)
    >>> proj_list = [projection(img, method=meth, axis='x') for meth in projection_methods]
    >>> fig = grayscale_imshow(proj_list,suptitle="Projection along x-axis", title=projection_methods)
    >>> proj_list = [projection(img, method=meth, axis='x', invert_axis=True) for meth in projection_methods]
    >>> fig = grayscale_imshow(proj_list,suptitle="Projection along inverted x-axis", title=projection_methods)
    >>> proj_list = [projection(img, method=meth, axis='y') for meth in projection_methods]
    >>> fig = grayscale_imshow(proj_list,suptitle="Projection along y-axis", title=projection_methods)
    >>> # - Get 'p58' shared intensity image:
    >>> img = imread(shared_dataset("p58", 'segmented', 0)[0], TissueImage3D, background=1, not_a_label=0)
    >>> proj_list = [projection(img, axis=ax) for ax in ['z', 'y', 'x']]
    >>> fig = grayscale_imshow(proj_list, title=[f"+{ax.upper()}-projection" for ax in ['z', 'y', 'x']], cmap='glasbey', suptitle="Segmentation projection")

    """
    if isinstance(image, LabelledImage):
        return _labelled_projection(image, method, axis=axis, **kwargs)
    else:
        return _intensity_projection(image, method, axis=axis, **kwargs)


def _labelled_projection(seg_image, method, axis, **kwargs):
    # TODO: move to `timagetk/algorithms/reconstruction.py` ?
    if 'contour' in method:
        proj_img = project_segmentation(seg_image, **kwargs)
        return proj_img
    elif method in ('background'):
        from timagetk.algorithms.reconstruction import altimap_from_segmentation
        from timagetk.algorithms.reconstruction import altimap_surface_projection
        alti = altimap_from_segmentation(seg_image, axis=axis, **kwargs)
        proj_img = altimap_surface_projection(seg_image, alti, axis=axis, **kwargs)
        return proj_img
    else:
        raise ValueError("Unknown `method` {}!".format(method))


def _intensity_projection(image, method, axis, **kwargs):
    # TODO: move to `timagetk/algorithms/reconstruction.py` ?
    ax_idx = image.axes_order_dict[axis.upper()]  # projected axis index
    img_slice = image.get_slice(0, axis=axis)  # easy way to obtain origin, voxelsize & metadata attributes
    ax, ori, vxs, md = img_slice.axes_order, img_slice.origin, img_slice.voxelsize, img_slice.metadata

    if 'contour' in method:
        proj_img, _ = image_surface_projection(image, **kwargs)
        return proj_img
    elif method in ('background'):
        seg_image = kwargs.pop('segmentation', None)
        try:
            assert seg_image is not None
        except AssertionError:
            raise ValueError("Missing 'segmentation' entry in keyword arguments!")
        from timagetk.algorithms.reconstruction import altimap_from_segmentation
        from timagetk.algorithms.reconstruction import altimap_surface_projection
        alti = altimap_from_segmentation(seg_image, axis=axis, **kwargs)
        proj_img = altimap_surface_projection(image, alti, axis=axis, **kwargs)
        return proj_img
    elif method in ('maximum', 'max'):
        return SpatialImage(np.max(image.get_array(), axis=ax_idx).astype(image.dtype),
                            axes_order=ax, voxelsize=vxs, origin=ori, metadata=md)
    elif method in ('average', 'mean'):
        return SpatialImage(np.mean(image.get_array(), axis=ax_idx).astype(image.dtype),
                            axes_order=ax, voxelsize=vxs, origin=ori, metadata=md)
    elif method in ('minimum', 'min'):
        return SpatialImage(np.min(image.get_array(), axis=ax_idx).astype(image.dtype),
                            axes_order=ax, voxelsize=vxs, origin=ori, metadata=md)
    else:
        raise ValueError("Unknown `method` {}!".format(method))


def projection_and_histogram(image, method='contour', axis='z', **kwargs):
    """Create a projection of an intensity or labelled 3D `image` to 2D along selected `axis`.

    Available intensity image projection `methods` are:

     - **contour**, detect the object contour and use voxel intensities along given `axis`;
     - **background**, detect the object contour using the background from segmentation along given `axis`;
     - **maximum** or **max**, only maximum values along given `axis` are kept;
     - **average** or **mean**, mean values along given `axis` are kept;
     - **minimum** or **min**, only minimum values along given `axis` are kept.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.LabelledImage
        Intensity or labelled image to project.
    method : str, optional
        Method to use for intensity image projection, see ``PROJECTION_METHODS`` for available methods.
    axis : str in {'x', 'y', 'z'}, optional
        Axis to use for projection. Defaults to `'z'`.

    Other Parameters
    ----------------
    invert_axis : bool
        If ``True``, default ``False``, revert the selected axis before 2D projection.
    threshold : int or float
        Consider only voxels with intensities superior to given threshold for object detection.
        By default, try to estimate it automatically.
    orientation: {-1, 1}
        Defines the orientation of the projection, try to guess it by default.
        If `-1` we consider the z-axis starts at the bottom of the object and goes upward.
        If `1` we consider the z-axis starts at the top of the object and goes downward.
    iso_upsampling : bool
        If ``True`` (default), up-sample the image to the minimum voxelsize.
        This is usefull if you have an axis with a lower resolution, typically the z-axis.
    altimap_smoothing : bool
        If ``True``, default is ``False``, smooth the altitude map obtained from the mask.
    gaussian_sigma : float
        Sigma value of the Gaussian filter applied to the image, it is divided by the image voxelsize.

    Returns
    -------
    timagetk.SpatialImage or timagetk.LabelledImage
        2D projection of the 3D stack.

    See Also
    --------
    timagetk.visu.projection.PROJECTION_METHODS
    timagetk.algorithms.reconstruction.image_surface_projection

    Examples
    --------
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.projection import projection_and_histogram
    >>> fname = 'p58-t0-a0.lsm'
    >>> image_path = shared_data(fname, "p58")
    >>> img = imread(image_path)
    >>> projection_and_histogram(img, orientation=1)
    >>> from timagetk.algorithms.exposure import global_contrast_stretch
    >>> s_img = global_contrast_stretch(img)
    >>> projection_and_histogram(s_img, orientation=1)

    """
    try:
        assert isinstance(image, SpatialImage) and not isinstance(image, LabelledImage)
    except AssertionError:
        log.error('The provided image is not a SpatialImage!')
        return
    try:
        assert not isinstance(image, LabelledImage)
    except AssertionError:
        log.error('The provided image is a LabelledImage!')
        return

    # -- Try to guess the intensity threshold value if none given:
    threshold = kwargs.pop('threshold', None)
    if threshold is None:
        from timagetk.array_util import guess_intensity_threshold
        threshold = guess_intensity_threshold(image)

    proj = _intensity_projection(image, method, axis, threshold=threshold, **kwargs)

    data = image.get_array()
    exclude = kwargs.pop('exclude', None)
    if exclude is not None:
        data = data[np.where(data) != exclude]
    data = data.ravel()

    ax_img, ax_hist, ax_cdf = plot_img_and_hist(proj, data=data)
    ax_hist.axvline(threshold, color='r')
