#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import numpy as np
from matplotlib import pyplot as plt
from sklearn.manifold import MDS
from sklearn.manifold import TSNE

from timagetk.bin.logger import get_logger

log = get_logger(__name__)


# -----------------------------------------------------------------------------
# PRE-CLUSTERING visualisations
# -----------------------------------------------------------------------------

def _plot_observations_pca(ax, proj_array, pca, components):
    """Plot the projected observation array.

    Parameters
    ----------
    ax : matplotlib.axes.Axes
        The ``Axes`` instance to use to plot the 2D projected observation array.
    proj_array : numpy.ndarray
        The projected array of shape ``[n_observations, n_components]``.
    pca : sklearn.decomposition.PCA
        The ``PCA`` instance use to perform the Principal Components Analysis.
    components : (int, int)
        The components to use to project the observation array.
        Index starts at ``1``.

    Returns
    -------
    matplotlib.axes.Axes
        The updated ``Axes`` instance.

    """
    # Get explained variance and create axes label:
    exp_var = [round(evar, 3) for evar in pca.explained_variance_ratio_]
    labels = [f"PC {comp} ({exp_var[comp - 1] * 100:.1f}%)" for comp in components]

    ax.grid(linestyle='-.')
    x = proj_array[:, components[0] - 1].T
    y = proj_array[:, components[1] - 1].T
    ax.scatter(x, y, alpha=0.3, label="samples")
    ax.set(aspect='equal', title="Observations 2D projection.", xlabel=labels[0], ylabel=labels[1])
    return ax


def _plot_variables_pca(ax, features, pca, components):
    """Plot the variable space circle with feature names for selected component axes.

    Parameters
    ----------
    ax : matplotlib.axes.Axes
        The ``Axes`` instance to use to plot the 2D projected observation array.
    features : list of str
        List of feature names to plot.
    pca : sklearn.decomposition.PCA
        The ``PCA`` instance use to perform the Principal Components Analysis.
    components : (int, int)
        The components to use to project the observation array.
        Index starts at ``1``.

    Returns
    -------
    matplotlib.axes.Axes
        The updated ``Axes`` instance.

    """
    import matplotlib.patches as mpatches
    import matplotlib.ticker as ticker

    # Get explained variance and create axes label:
    exp_var = [round(evar, 3) for evar in pca.explained_variance_ratio_]
    labels = [f"PC {comp} ({exp_var[comp - 1] * 100:.1f}%)" for comp in components]
    # Get variables loadings:
    loadings_x = pca.components_[components[0] - 1, :].T
    loadings_y = pca.components_[components[1] - 1, :].T

    # Plot the loading circle:
    circle = mpatches.Circle([0, 0], radius=1, edgecolor="black", fill=False)
    ax.add_patch(circle)
    for r in np.arange(0.25, 1, 0.25):
        circle = mpatches.Circle([0, 0], radius=r, edgecolor="gray", fill=False, linestyle=':')
        ax.add_patch(circle)
    ax.vlines(0, -1, 1, linestyle=':', colors='gray')
    ax.hlines(0, -1, 1, linestyle=':', colors='gray')

    # Move the bottom spine (x-axis) to y = 0
    ax.spines["bottom"].set_position(("data", 0))
    # Hide the spines:
    ax.spines[["top", "right", "left", "bottom"]].set_visible(False)
    # Hide the yaxis:
    ax.yaxis.set_major_locator(ticker.NullLocator())
    # Set ticks locations:
    ax.xaxis.set_major_locator(ticker.AutoLocator())
    [tick.label1.set_horizontalalignment('left') for tick in ax.xaxis.get_major_ticks()]
    ax.xaxis.set_label_coords(1, 0)

    # Add the variables loading:
    for i, feature in enumerate(features):
        feature = feature.replace('_', ' ')
        ax.quiver(0, 0, loadings_x[i], loadings_y[i], angles='xy', scale_units='xy', scale=1.)
        ax.text(loadings_x[i], loadings_y[i], feature)
        ax.set_xlabel(labels[0])
        ax.set_ylabel(labels[1])

    ax.set(aspect='equal', title="Variables 2D projection.", xlabel=labels[0], ylabel=labels[1])
    return ax


def pca_figures(clusterer, features='all', components=None, **kwargs):
    """Plot PCA observations & features subspace projection figures.

    Parameters
    ----------
    clusterer : timagetk.components.clustering.FeatureClusterer
        The clusterer instance to use as source for cell features.
    features : list of str
        List of feature names to plot, should be defined in `clust`.
    components : (int, int), optional
        The components to use, index starting at ``1``.
        Use ``components=[2, 3]`` to plot the figures for the second and third components.
        Defaults to first and second principal components.

    Other Parameters
    ----------------
    figname : str or pathlib.Path
        The file name to use to save the figure.

    Notes
    -----
    The ``[observation, features]`` array is built using the selected `features`.
    It is then stripped of observations with a NaN value in any of their `feature`.
    It is then scaled using the ``RobustScaler`` class from scikit-learn.
    Finally, we perfom a principal component analysis using the ``PCA`` class from scikit-learn.

    See Also
    --------
    sklearn.decomposition.PCA
    sklearn.preprocessing.RobustScaler

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.components.clustering import example_clusterer
    >>> clust = example_clusterer("p58", pre_compute=False)
    >>> # - Select cells with 'FloralMeristem' identity and from the first two layers:
    >>> clust.ids = ['FloralMeristem', 'layer_1_2']
    >>> # - Add cell features to the `Clusterer` instance:
    >>> clust.add_spatial_variable('area')
    >>> clust.add_spatial_variable('volume')
    >>> clust.add_spatial_variable('number_of_neighbors')
    >>> clust.add_spatial_variable('shape_anisotropy')
    >>> clust.add_temporal_variable("division_rate")
    >>> clust.add_temporal_variable("log_relative_value('volume',1)")
    >>> clust.add_temporal_variable("volumetric_strain_rates")
    >>> from timagetk.visu.clustering import pca_figures
    >>> pca_figures(clust, features=["volume", "log_relative_value('volume',1)"])
    >>> pca_figures(clust)

    """
    from sklearn.decomposition import PCA

    all_features = np.array(list(clusterer.variable_vector_info.keys()))
    if features == "all":
        features = list(all_features)
        array = np.array(list(clusterer._variable_vector_dict.values())).T
    else:
        feature_exists = np.array([feat in clusterer.variable_vector_info for feat in features])
        try:
            assert all(feature_exists)
        except AssertionError:
            missing_feature = all_features[~feature_exists]
            raise ValueError(f"Missing {len(missing_feature)} features: {', '.join(missing_feature)}")
        else:
            array = np.array([clusterer._variable_vector_dict[feat] for feat in features]).T

    n_obs = array.shape[0]  # initial number of observations
    # Remove NANs from features array:
    nan_idx = np.isnan(array).any(axis=1)
    array = np.array(array)[~nan_idx]
    if n_obs != array.shape[0]:
        pc_lost = round((n_obs - array.shape[0]) / n_obs, 3) * 100
        log.warning(f"Lost {n_obs - array.shape[0]} observations ({pc_lost}%) due to missing data during scaling!")

    # Apply robust scaler (data centering & scaling)
    from sklearn.preprocessing import RobustScaler
    transformer = RobustScaler().fit(array)
    std_array = transformer.transform(array)

    pca = PCA()
    proj_array = pca.fit_transform(std_array)

    if components is None:
        components = [1, 2]

    fig, ax = plt.subplots(nrows=1, ncols=2, gridspec_kw={'width_ratios': [2, 1]})
    fig.set_size_inches(w=12, h=5)
    _ = _plot_observations_pca(ax[0], proj_array, pca, components)
    _ = _plot_variables_pca(ax[1], features, pca, components)

    plt.tight_layout()
    figname = kwargs.get("figname", "")
    if figname != "":
        plt.savefig(figname)
        plt.close()
    else:
        plt.show()
    return


def plot_mds_dimensionality_reduction(distance_matrix, clustering=None, **kwargs):
    """2D projection of the distance matrix by Multi-Dimensional Scaling.

    Parameters
    ----------
    distance_matrix numpy.ndarray
        The pairwise distance matrix to reduce.
    clustering : dict, optional
        The clustering dictionary to project (color the points), if any.

    Other Parameters
    ----------------
    alpha : float
        Control the alpha transparency of the markers. Defaults to ``0.65``.
    cmap : str
        Colormap to use to represent the clusters, if any.
    figname : str or pathlib.Path
        File name to save the figure.

    See Also
    --------
    sklearn.manifold.MDS

    References
    ----------
    https://scikit-learn.org/stable/modules/manifold.html#multi-dimensional-scaling-mds

    Examples
    --------
    >>> from timagetk.visu.clustering import plot_mds_dimensionality_reduction
    >>> from timagetk.components.clustering import example_clusterer
    >>> # - Get an example from shared dataset:
    >>> clust = example_clusterer("p58", pre_compute=False)
    >>> # - Select cells with 'FloralMeristem' identity and from the first two layers:
    >>> clust.ids = ['FloralMeristem', 'layer_1_2']
    >>> # - Compute and add vector variables for "volume" and "log_relative_value('volume',1)" cell properties:
    >>> clust.add_spatial_variable("volume")
    >>> clust.add_temporal_variable("log_relative_value('volume',1)")
    >>> # - Compute the global pairwise distance matrix combine these two cell properties on a 50/50% basis:
    >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5])
    >>> # - Dimensionality reduction into two dimensions with t-SNE:
    >>> plot_mds_dimensionality_reduction(clust._global_pw_dist_mat)
    >>> # - Cluster it with a hierarchical agglomerative clustering method with "Ward's" linkage:
    >>> clust.compute_clustering(n_clusters=4, method="ward")
    >>> clust.n_cells_by_cluster()
    {1: 58, 2: 219, 3: 199, 4: 155}
    >>> # - Dimensionality reduction into two dimensions with t-SNE:
    >>> plot_mds_dimensionality_reduction(clust._global_pw_dist_mat, clust.get_clustering_dict(True))

    """
    figname = kwargs.pop('figname', "")
    # 2D projection of distance matrix with MDS:
    projection = MDS(n_components=2, dissimilarity='precomputed', normalized_stress='auto', n_jobs=-1).fit_transform(distance_matrix)

    fig, ax = plt.subplots()
    fig.set_size_inches(h=8, w=8)
    plt.grid(linestyle='-.')

    if clustering is None:
        ax.scatter(*projection.T, alpha=kwargs.get('alpha', 0.65), linewidth=0)
    else:
        data = list(clustering.values())
        ax.scatter(*projection.T, c=data, alpha=kwargs.get('alpha', 0.65), cmap=kwargs.get('cmap', 'viridis'),
                   linewidth=0)

    ax.axis('equal')
    ax.set_title("2D projection by MDS")
    ax.set_xlabel("Component 1")
    ax.set_ylabel("Component 2")
    plt.tight_layout()

    if figname != "":
        plt.savefig(figname)
        plt.close()

    return


def plot_tsne_dimensionality_reduction(distance_matrix, clustering=None, **kwargs):
    """2D projection of the distance matrix by t-distributed Stochastic Neighbor Embedding.

    Parameters
    ----------
    distance_matrix numpy.ndarray
        The pairwise distance matrix to reduce.
    clustering : dict, optional
        The clustering dictionary to project (color the points), if any.

    Other Parameters
    ----------------
    alpha : float
        Control the alpha transparency of the markers. Defaults to ``0.65``.
    cmap : str
        Colormap to use to represent the clusters, if any.
    figname : str or pathlib.Path
        File name to save the figure.

    See Also
    --------
    sklearn.manifold.TSNE

    References
    ----------
    https://scikit-learn.org/stable/modules/manifold.html#t-distributed-stochastic-neighbor-embedding-t-sne

    Examples
    --------
    >>> from timagetk.visu.clustering import plot_tsne_dimensionality_reduction
    >>> from timagetk.components.clustering import example_clusterer
    >>> # - Get an example from shared dataset:
    >>> clust = example_clusterer("p58", pre_compute=False)
    >>> # - Select cells with 'FloralMeristem' identity and from the first two layers:
    >>> clust.ids = ['FloralMeristem', 'layer_1_2']
    >>> # - - EXAMPLE 1 - Compute and add vector variable for "volume":
    >>> clust.add_spatial_variable("volume")
    >>> # - Compute the global pairwise distance matrix combine these two cell properties on a 50/50% basis:
    >>> clust.assemble_matrix(['volume'], [1.])
    >>> # - Dimensionality reduction into two dimensions with t-SNE:
    >>> plot_tsne_dimensionality_reduction(clust._global_pw_dist_mat)
    >>> # - Cluster it with a hierarchical agglomerative clustering method with "Ward's" linkage:
    >>> clust.compute_clustering(n_clusters=5, method="ward")
    >>> clust.n_cells_by_cluster()
    {1: 58, 2: 219, 3: 199, 4: 155}
    >>> # - Dimensionality reduction into two dimensions with t-SNE:
    >>> plot_tsne_dimensionality_reduction(clust._global_pw_dist_mat, clust.get_clustering_dict(True))

    >>> # - EXAMPLE 2 - Compute and add vector variables for "volume" and "log_relative_value('volume',1)" cell properties:
    >>> clust.add_spatial_variable("volume")
    >>> clust.add_temporal_variable("log_relative_value('volume',1)")
    >>> # - Compute the global pairwise distance matrix combine these two cell properties on a 50/50% basis:
    >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5])
    >>> # - Dimensionality reduction into two dimensions with t-SNE:
    >>> plot_tsne_dimensionality_reduction(clust._global_pw_dist_mat)
    >>> # - Cluster it with a hierarchical agglomerative clustering method with "Ward's" linkage:
    >>> clust.compute_clustering(n_clusters=4, method="ward")
    >>> clust.n_cells_by_cluster()
    {1: 58, 2: 219, 3: 199, 4: 155}
    >>> # - Dimensionality reduction into two dimensions with t-SNE:
    >>> plot_tsne_dimensionality_reduction(clust._global_pw_dist_mat, clust.get_clustering_dict(True))

    """
    figname = kwargs.pop('figname', "")
    # 2D projection of distance matrix with t-SNE:
    projection = TSNE(n_components=2, metric='precomputed', learning_rate='auto', init='random',
                      n_jobs=-1).fit_transform(distance_matrix)

    fig, ax = plt.subplots()
    fig.set_size_inches(h=8, w=8)
    plt.grid(linestyle='-.')

    if clustering is None:
        ax.scatter(*projection.T, alpha=kwargs.get('alpha', 0.65), linewidth=0)
    else:
        data = list(clustering.values())
        ax.scatter(*projection.T, c=data, alpha=kwargs.get('alpha', 0.65), cmap=kwargs.get('cmap', 'viridis'),
                   linewidth=0)

    ax.axis('equal')
    ax.set_title("2D projection by t-SNE")
    ax.set_xlabel("Component 1")
    ax.set_ylabel("Component 2")
    plt.tight_layout()

    if figname != "":
        plt.savefig(figname)
        plt.close()

    return


# -----------------------------------------------------------------------------
# POST-CLUSTERING visualisations
# -----------------------------------------------------------------------------

def plot_silhouette_estimator(silhouette_metrics, **kwargs):
    """Represent the silhouette metrics.

    Parameters
    ----------
    silhouette_metrics : dict
        Dictionary of silhouette scores by number of clusters

    Other Parameters
    ----------------
    figname : str or pathlib.Path
        File name to save the figure.

    Examples
    --------
    >>> from timagetk.visu.clustering import plot_silhouette_estimator
    >>> from timagetk.components.clustering import example_clusterer
    >>> # - Get an example from shared dataset:
    >>> clust = example_clusterer("p58", pre_compute=False)
    >>> # - Select cells with 'FloralMeristem' identity and from the first two layers:
    >>> clust.ids = ['FloralMeristem', 'layer_1_2']
    >>> # - Compute and add vector variables for "volume" and "log_relative_value('volume',1)" cell properties:
    >>> clust.add_spatial_variable('volume')
    >>> clust.add_temporal_variable("log_relative_value('volume',1)")
    >>> # - Compute the global pairwise distance matrix combine these two cell properties on a 50/50% basis:
    >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5])
    >>> # Performs several clustering and compute their silhouette metrics:
    >>> silhouette, nb_cluster = clust.silhouette_estimator("ward", k_max=7)
    >>> plot_silhouette_estimator(silhouette)

    """
    figname = kwargs.pop('figname', "")
    k_min = min(list(silhouette_metrics.keys()))
    k_max = max(list(silhouette_metrics.keys()))

    fig, ax = plt.subplots()
    fig.set_size_inches(h=5, w=0.8 * (k_max - k_min + 1))
    ax.plot(range(k_min, k_max + 1), silhouette_metrics.values(), color='red')
    ax.scatter(range(k_min, k_max + 1), silhouette_metrics.values(), color='black', marker='+')
    ax.set_title("Silhouette estimator")
    ax.set_xlabel('Number of clusters')
    ax.set_ylabel('Score')
    plt.grid(linestyle='-.')
    plt.tight_layout()

    if figname != "":
        plt.savefig(figname)
        plt.close()

    return


def clustering_figures(graph, clustering_name, seg_imgs, layers=[1, 2], trsfs=None, **kwargs):
    """Project the clustering on the segmented image for given layers.

    Parameters
    ----------
    graph : timagetk.graphs.TemporalTissueGraph
        The TemporalTissueGraph instance to use.
    seg_imgs : dict of str or dict of pathlib.Path
        Time-indexed dictionary of segmented image files.
    layers : list of int, optional
        List of cell layers to project clustering.
    trsfs : dict of str or dict of pathlib.Path, optional
        Time-interval indexed dictionary of transformation files.

    Other Parameters
    ----------------
    figname : str
        A file path with a placeholder (`{}`) to indicate the time-point.
    orientation : {-1, 1}
        Image stack orientation, use `-1` with an inverted microscope, `1` by default.
    colormap : str or matplotlib.colors.LinearSegmentedColormap
        The colormap used to represent data. Scaled by `value_range`. Defaults to 'viridis'.
    outliers_cluster_id : int
        The id associated to the outliers cluster.
    logger : logging.Logger
        A logger

    Returns
    -------
    list of str
        List of figure paths.

    Examples
    --------
    >>> from timagetk.components.clustering import example_clusterer
    >>> # - Get an example from shared dataset:
    >>> clust = example_clusterer("p58", pre_compute=False)
    >>> # - Select cells with 'FloralMeristem' identity and from the first two layers:
    >>> clust.ids = ['FloralMeristem', 'layer_1_2']
    >>> # - Compute and add vector variables for "volume" and "log_relative_value('volume',1)" cell properties:
    >>> clust.add_spatial_variable("volume")
    >>> clust.add_temporal_variable("log_relative_value('volume',1)")
    >>> # - Compute the global pairwise distance matrix combine these two cell properties on a 50/50% basis:
    >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5])
    >>> clust.n_gpdm_ids()
    631
    >>> # - Cluster it with a hierarchical agglomerative clustering method with "Ward's" linkage:
    >>> clust.compute_clustering(n_clusters=4, method="ward")
    >>> clust.n_cells_by_cluster()
    {1: 58, 2: 219, 3: 199, 4: 155}
    >>> # - Spatial projection of the obtained clustering on segmented tissues:
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.clustering import clustering_figures
    >>> tissues = shared_dataset("p58",'segmented')
    >>> tissues = dict(zip(range(len(tissues)), tissues))
    >>> clustering_figures(clust.graph, clust.clustering_name(), tissues)  # interactive viewer

    """
    import sys
    import numpy as np

    from timagetk import TissueImage3D
    from timagetk.algorithms.resample import resample
    from timagetk.algorithms.trsf import apply_trsf
    from timagetk.io import imread
    from timagetk.io import read_trsf
    from timagetk.gui.vtk_views import vtk_viewer
    from timagetk.features.utils import flatten

    from timagetk.bin.logger import get_dummy_logger
    logger = kwargs.get('logger', get_dummy_logger(sys._getframe().f_code.co_name))
    orientation = kwargs.get('orientation', 1)
    figname = kwargs.get('figname', None)

    if 'layers' not in graph.list_cell_properties():
        from timagetk.features.graph import add_cell_layers
        logger.warning("Could not find cell 'layers' property in the provided graph!")
        add_cell_layers(graph)

    # -- Get clustering related variables:
    outliers_cluster_id = kwargs.get('outliers_cluster_id', -1)
    clustering = graph.cell_property_dict(clustering_name, default=outliers_cluster_id, only_defined=True)
    nb_clusters = len(set(clustering.values()))
    value_range = kwargs.get('value_range', [0.5, nb_clusters + 0.5])
    n_labels = kwargs.get('n_labels', nb_clusters)

    # - Creates a categorical colormap:
    from visu_core.matplotlib.colormap import multicolor_categorical_colormap
    from timagetk.visu.util import colors_array
    vtk_cmap = multicolor_categorical_colormap(colors_array('viridis', nb_clusters), "clustering")
    colormap = kwargs.get('colormap', vtk_cmap)

    t_idx_max = graph.nb_time_points - 1
    template_img = None
    fnames = []
    for t_idx in range(graph.nb_time_points - 1):
        # Load the associated segmented images:
        logger.info(f"Loading t{t_idx} segmented image '{seg_imgs[t_idx]}'...")
        seg_img = imread(seg_imgs[t_idx], TissueImage3D, not_a_label=0, background=1)
        # - Try to load & apply the t_i -> t_N trsf
        if trsfs is not None:
            try:
                assert (t_idx, t_idx_max) in trsfs.keys()
            except (AssertionError, KeyError):
                logger.warning(f"Could not find 'rigid' transformation for {t_idx}->{t_idx_max} interval!")
            else:
                logger.info(f"Found 'rigid' transformation for {t_idx}->{t_idx_max} interval.")
                if isinstance(trsfs[(t_idx, t_idx_max)], str):
                    trsf = read_trsf(trsfs[(t_idx, t_idx_max)])
                else:
                    trsf = trsfs[(t_idx, t_idx_max)]
                logger.info(f"Applying 'rigid' transformation to t{t_idx} segmented image.")
                if template_img is None:
                    template_img = imread(seg_imgs[t_idx_max], TissueImage3D, not_a_label=0, background=1)
                seg_img = apply_trsf(seg_img, trsf, interpolation="cellbased", cell_based_sigma=1,
                                     template_img=template_img)

        # -- Down-sample the image to speed up the meshing:
        seg_img = resample(seg_img, voxelsize=[1., 1., 1.], interpolation='cellbased', cell_based_sigma=1.)

        # -- Get the cell layer dictionary:
        layers_cid = graph._tissue_graph[t_idx].cell_property_dict('layers', seg_img.cells.ids(), default=None,
                                                                   only_defined=True)
        layers_cid = {cid: int(layer) for cid, layer in layers_cid.items() if int(layer) in layers}
        # - Make a layer indexed list of cids:
        cell_layers = {layer: [] for layer in layers}
        for cid, layer in layers_cid.items():
            cell_layers[layer].append(cid)

        # -- Create the list of ids based on the selected layers
        cell_ids = list(flatten(list(cell_layers.values())))
        logger.debug(f"Found {len(cell_ids)} cell ids in layers: {list(cell_layers.keys())}.")

        # -- Get the clustering dictionary:
        data = graph._tissue_graph[t_idx].cell_property_dict(clustering_name, cell_ids, default=np.nan,
                                                             only_defined=False)

        # - Define the clustering projection absolute file path:
        if figname is not None:
            out_fname = figname.format(t_idx)
            logger.info(f"Saving '{out_fname}'...")
        else:
            out_fname = ''

        # - Create the tissue mesh and project the clustering:
        vtk_viewer(seg_img, data, 'Clustering', layers=cell_layers, value_range=value_range, n_labels=n_labels,
                   filename=out_fname, orientation=orientation, labels=cell_ids, colormap=colormap,
                   categorical=True)
        fnames.append(out_fname)

    return fnames
