#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import numpy as np
from PIL import ImageColor
from matplotlib.colors import ListedColormap
from matplotlib.pyplot import colormaps
from matplotlib.pyplot import get_cmap
from skimage.util.dtype import dtype_range
from timagetk.bin.logger import get_logger
from timagetk.components.image import get_image_attributes
from timagetk.visu.cmap import GLASBEY

log = get_logger(__name__)


def list_qualitative_colormaps():
    """List all qualitative colormaps from the currently registered colormaps.

    This function retrieves the names of all colormaps that are registered and classified as qualitative.
    These colormaps are generally suited for representing categorical data due to their discrete color mappings.

    Returns
    -------
    list of str
        A list containing the names of all registered qualitative colormaps.

    Examples
    --------
    >>> from timagetk.visu.util import list_qualitative_colormaps
    >>> print(list_qualitative_colormaps())
    ['tab10', 'tab20', 'tab20b', 'tab20c', 'Pastel1', 'Pastel2', 'Paired', 'Accent', 'Dark2', 'Set1', 'Set2', 'Set3']
    """
    import matplotlib as mpl
    return list(dict(mpl.color_sequences).keys())


def colors_array(cm_name, n_colors, alpha=True):
    """Return an array of colors from a knwon matplotlib colormap.

    Parameters
    ----------
    cm_name : str
        A colormap name, known to matplotlib.
    n_colors : int
        The number of colors to get from the colormap.
    alpha : bool
        If ``False``

    Returns
    -------
    numpy.array
        An array of `n_colors` RGBA colors taken in the colormap.

    See Also
    --------
    matplotlib.cm.get_cmap : the method used to get the colors.

    Examples
    --------
    >>> from timagetk.visu.util import colors_array
    >>> cm = colors_array('viridis', 5)
    >>> print(cm)
    >>> cm = colors_array('viridis', 5, alpha=False)
    >>> print(cm)
    >>> from timagetk.visu.util import register_glasbey
    >>> cm = colors_array('glasbey', 5, alpha=False)
    >>> print(cm)

    """
    from matplotlib import cm
    from matplotlib import colors
    cmap = get_cmap(cm_name)
    norm = colors.Normalize(vmin=0, vmax=n_colors - 1)
    scalarmap = cm.ScalarMappable(norm=norm, cmap=cmap)
    color_array = np.array([scalarmap.to_rgba(i) for i in range(n_colors)])
    if not alpha:
        color_array = color_array[:, :3]
    return color_array


def get_n_colors(cm_name, n_colors):
    """Generates a list of n_colors colors from a specified colormap.

    This function retrieves a specified number of colors from a provided matplotlib colormap
    name. It checks if the colormap is qualitative (discrete) or continuous, processes the colors
    accordingly, and ensures the compatibility of the requested number of colors with the
    available options in the colormap.

    For qualitative colormaps, it validates that there are enough colors to extract, slices the relevant
    number of colors, and ensures their RGBA compatibility. For non-qualitative colormaps, it
    generates the colors programmatically.

    Parameters
    ----------
    cm_name : str
        The name of the colormap. It can refer to a qualitative colormap or other
        available matplotlib colormap representations.
    n_colors : int
        The number of colors to retrieve from the colormap. If a qualitative
        colormap is provided, ensures that it has at least this number of colors.

    Returns
    -------
    list of tuple
        A list of RGBA color tuples. Each tuple in the list contains four floating-point
        values representing red, green, blue, and alpha channels.

    Raises
    ------
    ValueError
        If the colormap is qualitative and does not have at least `n_colors`
        available for extraction.

    Notes
    -----
    - Qualitative colormaps refer to discrete colormaps with a fixed set of defined colors.
    - This function handles both qualitative and programmatically defined continuous
      colormaps.
    - An alpha channel (transparency) of 1.0 is automatically added to RGB colors from qualitative
      colormaps if missing.
    """
    # Generate a colormap based on the specified matplotlib colormap:
    if cm_name in list_qualitative_colormaps():  # Conditional check for qualitative colormaps
        import matplotlib as mpl
        color_list = mpl.color_sequences[cm_name]  # Retrieve the list of colors from the colormap
        try:
            # Confirm that the colormap has sufficient colors for the graph:
            assert len(color_list) >= n_colors
        except AssertionError:
            raise ValueError(f"Given `cmap` has only {n_colors} colors, but {len(color_list)} are required for greedy coloring!")
        else:
            # Slice the list to use only the first `n_colors` colors:
            color_list = color_list[:n_colors]
            # Add an alpha channel to RGB colors if missing (convert to RGBA):
            color_list = [tuple(color) + (1.,) if len(color) == 3 else color for color in color_list]
    else:
        # If colormap is not qualitative, generate a color array programmatically:
        color_list = colors_array(cm_name, n_colors, alpha=True)

    return color_list


def color_code(color):
    """Return the color array corresponding to the given color name.

    Example of available `colors` are: 'red', 'green', 'blue', 'yellow',
    'cyan', 'magenta'.

    Parameters
    ----------
    color : str
        The name of a color, it can be one of the 140 standard HTML colors or an
        hexadecimal value like '#ff0000' for 'red'.

    Returns
    -------
    numpy.ndarray, shape (3,)
        Color array in the RGB space, 64bit float values in [0, 1]

    See Also
    --------
    PIL.ImageColor.getrgb : the method converting given `color` to 8bit RGB
    value.

    Examples
    --------
    >>> from timagetk.visu.util import color_code
    >>> color_code('red')
    array([1., 0., 0.])
    >>> color_code('#ff0000')
    array([1., 0., 0.])
    >>> color_code('green')
    array([0.        , 0.50196078, 0.        ])

    """
    cm = ImageColor.getrgb(color)  # 8bit RGB list
    return np.array(cm) / np.array([255., 255., 255.])


def sum_no_overflow(a, b):
    """Sum two numpy arrays without overflow and by preserving ``dtype``.

    Parameters
    ----------
    a : numpy.ndarray
        Array to add values to, its type will be used to set maximum value.
    b : numpy.ndarray
        Array to add to ``a``, its ``dtype`` attribute should be the same as ``a`` or you may be looking for troubles!

    Returns
    -------
    numpy.ndarray
        The sum array without overflow and of same type.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.visu.util import sum_no_overflow
    >>> a = np.array([0, 0, 5, 150, 250], dtype='uint8')
    >>> b = np.array([0, 5, 20, 20, 20], dtype='uint8')
    >>> c = sum_no_overflow(a, b)
    >>> print(c)
    [  0   5  25 170 255]
    >>> print(a)
    [  0   0   5 150 250]

    """
    import copy as cp
    _, max_val = dtype_range[a.dtype.type]
    c = max_val - b  # a temp array here
    d = cp.deepcopy(a)
    np.putmask(d, c < d, c)  # a temp bool array here
    d += b
    return d


def _force_aspect_ratio(ax, xy_ratio):
    """Use this to force the image aspect ratio with ``matplotlib.imshow()``."""
    im = ax.get_images()[0]
    ext = im.get_extent()
    ax.set_aspect(abs((ext[1] - ext[0]) / (ext[3] - ext[2])) / xy_ratio)


def convert_str_range(image, val_range):
    """Convert range string to a min & max value.

    Accepted ``val_range`` can be:

     - **auto**: get the min and max value of the image;
     - **type**: get the maximum range from the `image.dtype`, *e.g.* 'uint8'=[0, 255];
     - **pc-X**: get the X% percentile of the image, where X is an integer between 0 and 100;

    Parameters
    ----------
    image : timagetk.SpatialImage
        Reference image to use to convert range string to min and max values
    val_range : str, in ['auto', 'type', 'pc-X']
        A string to convert into a min and max value

    Returns
    -------
    list
        Min and max values corresponding to given range
    """
    if val_range == 'type':
        from skimage.util.dtype import dtype_range
        mini, maxi = dtype_range[image.dtype.type]
        return [mini, maxi]
    elif val_range.startswith('pc'):
        pc = float(val_range.split('-')[1])
        mini, maxi = np.percentile(image, [pc, 100 - pc])
        return [mini, maxi]
    else:  # auto case
        mini, maxi = image.min(), image.max()
        if image.dtype in (np.uint8, np.uint16):
            return [int(mini), int(maxi)]
        else:
            return [float(mini), float(maxi)]


def register_cmap(array, name, force=False):
    """Register an array as a colormap in matplotlib.

    Parameters
    ----------
    array : numpy.ndarray
        The RGB(A) array to use to register the colormap.
    name : str
        The name to give to the colormap.
    force : bool, optional
        Wheter to override an existing colormap with the selected `name`.

    Examples
    --------
    >>> from timagetk.visu.util import get_glasbey
    >>> from timagetk.visu.util import register_cmap
    >>> glasbey = get_glasbey(range(15))
    >>> glasbey.N
    15
    >>> register_cmap(glasbey.colors, 'glasbey', force=True)

    """
    from matplotlib import colormaps as cm
    from matplotlib import colormaps
    from matplotlib import colors

    try:
        cm.get_cmap(name)
    except ValueError:
        pass  # this is what we expect!
    else:
        if not force:
            log.info(f"Custom colormap '{name}' is already registered, use `force=True` to register again!")
            return
        else:
            colormaps.unregister(name)

    n_colors = len(array)
    color_dict = {'red': [], 'green': [], 'blue': []}
    for i_p, rgb in enumerate(array):
        for k, c in enumerate(['red', 'green', 'blue']):
            color_dict[c] += [(i_p / float(n_colors - 1), rgb[k], rgb[k])]
    for c in ['red', 'green', 'blue']:
        color_dict[c] = tuple(color_dict[c])

    lin_cmap = colors.LinearSegmentedColormap(name, color_dict)
    colormaps.register(cmap=lin_cmap, name=name, force=force)

    log.debug(f"Done registering the '{name}' colormap to matplotlib.")

    return


def register_glasbey(force=False):
    """Register the previously defined GLASBEY colormap in matplotlib.

    Call this method to be able to use the 'glasbey' colormap with matplotlib.

    Parameters
    ----------
    force : bool, optional
        Wheter to override an existing colormap with the selected `name`.

    Notes
    -----
    This colormap is made of 256 values, so to display images with more than 256
    unique values, you need to transform it prior to plotting or close labels
    will have similar colors.

    Examples
    --------
    >>> from timagetk.visu.util import register_glasbey
    >>> from timagetk.array_util import random_spatial_image
    >>> import matplotlib.pyplot as plt
    >>> # EXAMPLE #1 - random 8-bits 2D image:
    >>> img = random_spatial_image([20, 20])
    >>> plt.imshow(img, cmap='glasbey')
    >>> plt.colorbar()
    >>> plt.show()
    >>> # EXAMPLE #2 - random 16-bits 2D image:
    >>> img = random_spatial_image([20, 20], dtype="uint16")
    >>> plt.imshow(img%255, cmap='glasbey')
    >>> plt.colorbar()
    >>> plt.show()

    """
    register_cmap(GLASBEY, "glasbey", force=force)
    log.debug("Done registering the 'glasbey' colormap to matplotlib.")
    return


def get_glasbey(labels, not_a_label=0, background=1):
    """Create a glasbey-like colormap scaled to the list of labels.

    Parameters
    ----------
    labels : list
        List of labels
    not_a_label : int, optional
        Label with a specific coloring behavior, set to [0., 0., 0.] (black).
    background : int, optional
        Label with a specific coloring behavior, set to [1., 1., 1.] (white).

    Returns
    -------
    matplotlib.colors.ListedColormap
        A matplotlib colormap.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.visu.util import get_glasbey
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.io.image import imread
    >>> from timagetk import LabelledImage
    >>> img = imread(shared_dataset("p58", "segmented")[0], LabelledImage, not_a_label=0)
    >>> all_labels = np.unique(img)  # get all labels in the image
    >>> print(len(all_labels))
    1040
    >>> from timagetk.visu.cmap import GLASBEY
    >>> print(len(GLASBEY))  # Glasbey colormap has 256 RGB values
    256
    >>> cm = get_glasbey(all_labels)
    >>> print(cm.N)  # get the number of colors in the colormap.
    1040
    >>> print(cm.colors[:5])
    """
    from matplotlib import colors
    from timagetk.visu.cmap import GLASBEY

    glasbey = GLASBEY.tolist()
    # Clear the colormap of the specific color we want to use later
    try:
        glasbey.remove([0., 0., 0.])  # black
    except:
        pass
    try:
        glasbey.remove([1., 1., 1.])  # white
    except:
        pass

    # Make sure we have a list
    if isinstance(labels, np.ndarray):
        labels = labels.tolist()
    # Get the true set of labels (exclude `not_a_label` & `background`)
    true_labels = set(labels) - {not_a_label, background}
    # Defines how many colors we need:
    mini, maxi = min(true_labels), max(true_labels)
    n_colors = int(maxi - mini + 1)
    # Now create a list of RGB values big enough for the number of labels/colors
    big_glasbey = glasbey.copy()
    while len(big_glasbey) < n_colors:
        big_glasbey += glasbey

    cmap = []
    if not_a_label in labels:
        cmap += [[0., 0., 0.]]  # black
    if background in labels:
        cmap += [[1., 1., 1.]]  # white
    cmap += [big_glasbey[l] for l in true_labels]
    return colors.ListedColormap(cmap, 'glasbey')


def randomize_cmap(labels, cmap, not_a_label=0, background=1):
    """Randomize the colors of a matplotlib colormap.

    Parameters
    ----------
    labels : list of int
        The list of labels to get a colormap for.
    cmap : str
        The name of the matplotlib colormap to randomize.
    not_a_label : int, optional
        A label to exclude from the colormap.
    background : int, optional
        A label to exclude from the colormap.

    Returns
    -------
    matplotlib.colors.ListedColormap
        The randomized colormap.

    Examples
    --------
    >>> from timagetk.visu.util import randomize_cmap
    >>> cmap = randomize_cmap(range(3), 'viridis')
    >>> print(cmap.N)
    2
    >>> print(cmap.colors)
    """
    from matplotlib import colormaps as cm
    from matplotlib.colors import ListedColormap
    if not_a_label not in labels:
        labels.append(not_a_label)
    if background not in labels:
        labels.append(background)

    n_colors = max(labels) - min(labels)
    cmap = cm.get_cmap(cmap).colors[0:n_colors]
    newcmap = cmap(np.linspace(0, 1, n_colors))
    rng = np.random.default_rng()
    rng.shuffle(np.array(newcmap))
    newcmap = newcmap.tolist()
    return ListedColormap(newcmap)


def relabel_8bits(seg_image):
    """Relabel a segmented image to 8bits.

    Parameters
    ----------
    seg_image : timagetk.LabelledImage or timagetk.TissueImage
        The segmented image to relabel to 8bits.

    Returns
    -------
    timagetk.LabelledImage or timagetk.TissueImage
        The 8bits segmented image.

    Notes
    -----
    This function is a bit rough as it takes no precautions and simply returns the original image modulo `255`.
    It should be used for visualisation purposes only but still with great care!
    """
    from timagetk.components.image import get_image_class
    Image = get_image_class(seg_image)
    attr = get_image_attributes(seg_image, exclude=['dtype'], extra=['filename'])
    return Image(seg_image % 255, dtype='uint8', **attr)


def topological_graph(tissue_image, background=1, exclude=None):
    """Create a topological graph, that is the neighbors of the labels in `labelled_image`.

    Parameters
    ----------
    tissue_image : timagetk.LabelledImage or timagetk.TissueImage2D or timagetk.TissueImage3D
        The segmented image to color.
    background : int, optional
        Used only if the image is a LabelledImage instance.
    exclude : Iterable
        An iterable of labels to exclude from the topological graph.

    Returns
    -------
    networkx.Graph
        The topological graph of the labelled image.

    Examples
    --------
    >>> from timagetk import TissueImage3D
    >>> from timagetk.visu.util import topological_graph
    >>> from timagetk.io.dataset import shared_data
    >>> tissue = TissueImage3D(shared_data('synthetic', 'labelled'))
    >>> G = topological_graph(tissue, exclude={tissue.background, tissue.not_a_label})
    >>> print(list(G.nodes()))
    [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
    >>> print(tissue.cell_ids())
    [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
    """
    import networkx as nx
    from timagetk.components.labelled_image import LabelledImage
    from timagetk import TissueImage3D

    if isinstance(tissue_image, LabelledImage):
        attr = get_image_attributes(tissue_image, extra=['filename'])
        if 'background' in attr:
            background = attr.pop('background')
        tissue_image = TissueImage3D(tissue_image, background=background, **attr)

    if exclude is None:
        exclude = {}

    G = nx.Graph()
    G.add_nodes_from(tissue_image.cell_ids())
    nei_dict = tissue_image.neighbors(tissue_image.cell_ids())
    [G.add_edges_from([(l, n) for n in nei if n not in exclude]) for l, nei in nei_dict.items()]
    return G


def greedy_labelling(tissue_image, background=1):
    """Segmented image coloring.

    Attempts to color a graph using as few colors as possible, where no neighbours of a node can have same color as the
    node itself.

    Parameters
    ----------
    tissue_image : timagetk.LabelledImage or timagetk.TissueImage2D or timagetk.TissueImage3D
        The segmented image to color.
    background : int, optional
        Used only if the image is a LabelledImage instance.

    Returns
    -------
    dict
        A label (cell id) based dictionary with colors ids to use.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.visu.util import greedy_labelling
    >>> from timagetk.io.dataset import shared_data
    >>> seg_img = shared_data('synthetic', 'labelled')
    >>> print(len(np.unique(seg_img)))
    >>> len(seg_img.labels())
    >>> greedy_labels = greedy_labelling(seg_img, background=1)

    >>> from timagetk.components.labelled_image import relabel_from_mapping
    >>> from timagetk.visu.stack import stack_browser
    >>> coded_img = relabel_from_mapping(seg_img, greedy_labels)
    >>> b = stack_browser(coded_img, cmap='viridis', val_range='auto')

    """
    from timagetk.components.labelled_image import LabelledImage
    from timagetk import TissueImage3D
    from networkx.algorithms import coloring

    if isinstance(tissue_image, LabelledImage):
        attr = get_image_attributes(tissue_image, extra=['filename'])
        if 'background' in attr:
            background = attr.pop('background')
        tissue_image = TissueImage3D(tissue_image, background=background, **attr)

    G = topological_graph(tissue_image, exclude={tissue_image.background, tissue_image.not_a_label})
    return coloring.greedy_color(G)


def greedy_color_dict(tissue_image, cmap='tab20', background=1):
    """Create a dictionary of colors for a segmented image following a greedy algorithm.

    Attempts to color a graph using as few colors as possible, where no neighbours of a node can have same color as the
    node itself.

    Parameters
    ----------
    tissue_image : timagetk.LabelledImage or timagetk.TissueImage2D or timagetk.TissueImage3D
        The segmented image to color.
    cmap : str or matplotlib.colors.ListedColormap or Iterable
        Name of a matplotlib colormap to use for greedy coloring.
        Can also be a `ListedColormap` from matplotlib.
        Else, can be any iterable of RGB or RGBA colors as [0., 1.] floating values.
    background : int, optional
        Used only if the image is a LabelledImage instance.

    Returns
    -------
    dict
        A label-indexed dictionary of color obtained from greedy labelling.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.visu.util import greedy_color_dict
    >>> from timagetk.io.dataset import shared_data
    >>> seg_img = shared_data('synthetic', 'labelled')
    >>> print(len(np.unique(seg_img)))
    14
    >>> color_dict = greedy_color_dict(seg_img, 'tab20', background=1)
    >>> print(len(color_dict))
    14
    >>> print({label: color_dict[label] for label in list((color_dict.keys()))[:3]})

    """
    from typing import Iterable
    from matplotlib.colors import ListedColormap
    # Compute the greedy labelling color code dictionary:
    greedy_labels = greedy_labelling(tissue_image, background=background)
    # Defines how many colors we need and get them from matplotlib:
    n_colors = len(np.unique(list(greedy_labels.values())))
    # Get the list of colors
    if isinstance(cmap, str):
        color_list = list(map(list, colors_array(cmap, n_colors, alpha=True)))
    elif isinstance(cmap, ListedColormap):
        color_list = list(map(list, cmap.colors))
    elif isinstance(cmap, Iterable):
        color_list = list(map(list, cmap))
    else:
        from timagetk.util import clean_type
        log.critical(f"Got an invalid `cmap` type: `{clean_type(cmap)}`.")
        raise ValueError("Parameter `cmap` must be either a string referencing a matplotlib colormap or an iterable!")
    # Make sure the list of colors has enough values:
    try:
        assert len(color_list) >= n_colors
    except AssertionError:
        raise ValueError(
            f"Given `cmap` has only {n_colors} colors, but {len(color_list)} are required for greedy coloring!")

    # Defines the min and max label:
    min_label, max_label = int(tissue_image.min()), int(tissue_image.max())
    # Make sure all colors are RGBA:
    color_list = [color + [1.] if len(color) == 3 else color for color in color_list]

    colormap = {}
    for label in range(min_label, max_label + 1):
        try:
            code = greedy_labels[label]
        except KeyError:
            # rgba = [0., 0., 0., 1.]  # black
            rgba = [1., 1., 1., 1.]  # white
        else:
            rgba = color_list[code]
        colormap[label] = rgba

    return colormap


def greedy_color_from_topology(topo_graph, cmap='tab20', undef_color=(1., 1., 1., 1.)):
    """Create a dictionary of colors for topological graph following a greedy algorithm.

    Attempts to color a graph using as few colors as possible, where no neighbours of a node can have same color as the
    node itself.

    Parameters
    ----------
    topo_graph : networkx.Graph
        A topological graph to color.
    cmap : str
        A matplotlib colormap to use for greedy coloring.
    undef_color : tuple, optional
        An RGBA color to use for undefined nodes. Defaults to white ``(1., 1., 1., 1.)``.

    Returns
    -------
    dict[int, (float, float, float, float)]
        A node-indexed dictionary of RGBA colors obtained from greedy labelling.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.visu.util import greedy_color_from_topology
    >>> from timagetk.visu.util import greedy_color_dict
    >>> from timagetk.visu.util import topological_graph
    >>> from timagetk.io.dataset import shared_data
    >>> seg_img = shared_data('synthetic', 'labelled')
    >>> print(len(np.unique(seg_img)))
    14
    >>> topo_graph = topological_graph(seg_img)
    >>> color_dict = greedy_color_from_topology(topo_graph, 'Set3')
    >>> print(len(color_dict))
    14
    >>> print(color_dict[2])
    (0.5529411764705883, 0.8274509803921568, 0.7803921568627451, 1.0)

    """
    from networkx.algorithms.coloring.greedy_coloring import greedy_color

    # Computes greedy color labels for each node (ensures neighboring nodes have different labels):
    greedy_labels = greedy_color(topo_graph)

    # Determine the total number of unique colors required for the graph:
    n_colors = len(np.unique(list(greedy_labels.values())))

    # Generate a colormap based on the specified matplotlib colormap:
    color_list = get_n_colors(cmap, n_colors)

    colormap = {}  # Initialize the color mapping dictionary:
    for node in topo_graph.nodes():
        try:
            # Retrieve the greedy color label for the current node:
            greedy_label = greedy_labels[node]
        except KeyError:
            # If no label is found, use the undefined color as a fallback:
            rgba = undef_color
        else:
            # Get the RGBA value corresponding to the greedy label:
            rgba = color_list[greedy_label]
        # Assign the RGBA color to the node in the color mapping dictionary:
        colormap[node] = tuple(rgba)

    return colormap


def greedy_colormap(tissue_image, cmap='tab20', background=1):
    """Create a colormap for a segmented image following a greedy algorithm.

    Attempts to color a graph using as few colors as possible, where no neighbours of a node can have same color as the
    node itself.

    Parameters
    ----------
    tissue_image : timagetk.LabelledImage or timagetk.TissueImage2D or timagetk.TissueImage3D
        The segmented image to color.
    cmap : str or matplotlib.colors.ListedColormap or Iterable
        Name of a matplotlib colormap to use for greedy coloring.
        Can also be a `ListedColormap` from matplotlib.
        Else, can be any iterable of RGB or RGBA colors as [0., 1.] floating values.
    background : int, optional
        Used only if the image is a LabelledImage instance.

    Returns
    -------
    matplotlib.colors.ListedColormap
        A matplotlib greedy colormap following the image labelling.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.visu.util import greedy_colormap
    >>> from timagetk.io.dataset import shared_data
    >>> seg_img = shared_data('synthetic', 'labelled')
    >>> print(len(np.unique(seg_img)))
    14
    >>> from timagetk.visu.util import greedy_labelling
    >>> greedy_labels = greedy_labelling(seg_img, background=1)
    >>> print(len(np.unique(list(greedy_labels.values()))))  # number of unique greedy labels
    6
    >>> cmap = greedy_colormap(seg_img, 'tab20')
    >>> print(cmap.N)  # get the number of colors in the greedy colormap.
    14
    >>> from timagetk.visu.stack import stack_browser
    >>> stack_browser(seg_img, cmap=cmap)

    """
    from matplotlib.colors import ListedColormap
    colormap = greedy_color_dict(tissue_image, cmap, background=background)
    return ListedColormap(list(colormap.values()))


def property_colormap(label_property, labelled_image=None, colormap_name='viridis', undefined_color=(0, 0, 0, 1)):
    """Create a colormap of labels property values.

    Parameters
    ----------
    label_property : dict
        A dictionary where keys are labels and values are associated
        numerical intensities for those labels.
    labelled_image : numpy.ndarray or timagetk.LabelledImage, optional
        A labelled image to extract the labels from.
        If not provided (default), the colormap might miss some undefined labels.
    colormap_name : str, optional
        A name of a colormap from matplotlib. Defaults to 'viridis'.
    undefined_color : tuple, optional
        A tuple of RGBA values for undefined labels. Defaults to black ``(0, 0, 0, 1)``.

    Returns
    -------
    matplotlib.colors.ListedColormap
        The colormap providing a mapping from label values to colors.
    matplotlib.colors.BoundaryNorm
        The norm object ensures that the colormap is applied correctly to the
        input data by normalizing the labels.
    matplotlib.colors.BoundaryNorm
        The norm object ensures that the colormap is applied correctly to the
        input data by normalizing the values.

    Examples
    --------
    >>> import numpy as np
    >>> import matplotlib.pyplot as plt
    >>> from timagetk.visu.util import property_colormap
    >>> label_to_value_map = {0: 1.5, 1: 0.5, 2: 3.5, 5: 0.75}
    >>> cmap, norm_labels, norm_values = property_colormap(label_to_value_map)
    >>> print(cmap.N)
    3
    >>> print(norm_labels.boundaries)
    [-0.5  0.5  1.5  2.5]
    >>> labelled_image = np.array([[1, 2, 4, 5], [4, 1, 0, 2], [0, 1, 2, 5]])
    >>> cmap, norm_labels, norm_values = property_colormap(label_to_value_map, labelled_image)
    >>> print(cmap.N)
    4
    >>> print(norm_labels.boundaries)
    [-0.5  0.5  1.5  2.5  3.5  4.5  5.5]

    >>> plt.imshow(labelled_image, cmap=cmap, norm=norm_labels)
    >>> cbar_val = plt.colorbar()
    >>> cbar_val.set_label('Mapped Labels')
    >>> plt.title('Image with Custom Colormap from Labels Values')
    >>> plt.axis('off')
    >>> plt.show()

    >>> plt.imshow(labelled_image, cmap=cmap, norm=norm_values)
    >>> cbar_val = plt.colorbar()
    >>> cbar_val.set_label('Mapped Values')
    >>> plt.title('Image with Custom Colormap from Labels Values')
    >>> plt.axis('off')
    >>> plt.show()

    """
    import matplotlib.pyplot as plt
    import matplotlib.colors as mcolors
    # Extract labels and values from the map
    defined_labels, values = zip(*label_property.items())

    if labelled_image is not None:
        # Extract labels and values from the image
        labels = list(np.unique(labelled_image))
    else:
        labels = defined_labels

    min_value, max_value = min(values), max(values)
    min_label, max_label = min(labels), max(labels)

    # Create a colormap using the specified colormap name
    base_cmap = plt.get_cmap(colormap_name)
    # Create a colormap by mapping labels and the corresponding values
    label_colors = {label: base_cmap(value / max_value) for label, value in label_property.items()}

    # Create mapping, defaulting undefined labels to the undefined color
    color_list = [label_colors.get(label, undefined_color) for label in range(min_label, max_label + 1)]

    # Normalize the custom colormap
    cmap = mcolors.ListedColormap(color_list)
    # Adjusting the boundaries for normalization
    norm_labels = mcolors.BoundaryNorm(boundaries=np.arange(min_label - 0.5, max_label + 0.5 + 1),
                                       ncolors=min(max_label - min_label + 1, 255))
    # Adjusting the boundaries for normalization
    norm_values = mcolors.BoundaryNorm(boundaries=np.arange(min_value, max_value + 1),
                                       ncolors=min(max_value - min_value + 1, 255))

    return cmap, norm_labels, norm_values
