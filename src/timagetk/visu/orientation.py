#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import sys

import numpy as np
import scipy.ndimage as nd

from timagetk.algorithms.connexe import connected_components
from timagetk.algorithms.linearfilter import gaussian_filter
from timagetk.algorithms.resample import isometric_resampling
from timagetk.components.labelled_image import LabelledImage
from timagetk.components.spatial_image import SpatialImage


################################################################################
## From `Tissue_Analysis` array_tools.py
################################################################################


def coordinates_centering3D(coordinates, mean=None):
    """Center a set of 'coordinates' around their 'mean'.

    Parameters
    ----------
    coordinates : numpy.ndarray
        A set of coordinates to center.
    mean : list or numpy.ndarray, optional
        If ``None`` (default) the mean is computed before centering.
        Else should be a length-3 list or array.

    Returns
    -------
    numpy.ndarray
        The centered set of coordinates.
    """
    try:
        x, y, z = coordinates
    except ValueError:
        x, y, z = coordinates.T
    if mean is None:
        mean = np.mean(np.array([x, y, z]), 1)
    # Now perform centering operation:
    x = x - mean[0]
    y = y - mean[1]
    z = z - mean[2]
    return np.array([x, y, z])


def compute_covariance_matrix(coordinates):
    """Function computing the covariance matrix of a given set of 'coordinates'.

    The covariance matrix of X, a set of coordinate is computed as follows: :math:`cov(X) = 1/N \time X \dot X^T,`
    where:
        - :math:`N` is the number of coordinate;
        - :math:`\dot` is the dot product;
        - :math:`^T` is the matrix transposition.

    Parameters
    ----------
    coordinates : numpy.ndarray
        Array of coordinates as a ``[d, N]`` matrix with ``d`` the dimensionality

    Returns
    -------
    numpy.ndarray
        The ``[d, d]`` covariance matrix.

    Notes
    -----
    Maximum value for ``d`` is 3.

    """
    # Make sure we have an array:
    if not isinstance(coordinates, np.ndarray):
        coordinates = np.array(coordinates)

    n_points = max(coordinates.shape)
    try:
        assert n_points > 3
    except:
        raise ValueError('Too small number of coordinates!')

    # Make sure it is a [d, N] array and not an [N, d] array:
    if coordinates.shape[0] > 3:
        coordinates = coordinates.T

    return 1. / n_points * np.dot(coordinates, coordinates.T)


def eigen_values_vectors(cov_matrix):
    """Extract the eigen vectors and associated values from a covariance matrix.

    Parameters
    ----------
    cov_matrix : numpy.ndarray
        The covariance matrix of a set of coordinates.
        Typically from ``.

    Returns
    -------
    eig_val : numpy.ndarray
        Length-3 list of sorted eigen values associated to the eigen vectors
    eig_vec : numpy.ndarray
        3x3 array of eigen vectors --by rows-- associated to sorted eigen values
    """
    assert max(cov_matrix.shape) <= 3
    # np.linalg.eig return eigenvectors by column !!
    eig_val, eig_vec = np.linalg.eig(cov_matrix)
    decreasing_index = eig_val.argsort()[::-1]
    eig_val, eig_vec = eig_val[decreasing_index], eig_vec[:, decreasing_index]
    eig_vec = np.array(eig_vec).T  # ... our standard is by rows !
    return eig_val, eig_vec


################################################################################
def object_detection_threshold(image, threshold=None):
    """Detect the object within a 3D intensity image from thresholding.

    Parameters
    ----------
    image : timagetk.SpatialImage
        Intensity image containing an object to detect.
    threshold : int or float, optional
        Intensity threshold value to use.
        By default, it will be automatically computed as the 90^th percentile.

    Returns
    -------
    timagetk.LabelledImage
        A binary labelled image, where 1 indicate the main object, 0 the background.

    Examples
    --------
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.mplt import intensity_histogram
    >>> from timagetk.visu.stack import stack_browser
    >>> from timagetk.visu.orientation import object_detection_threshold
    >>> # Using shared data as example:
    >>> img = shared_data('flower_confocal', 0)
    >>> intensity_histogram(image)
    >>> mask = object_detection_threshold(img)
    >>> stack_browser(mask, val_range='auto')

    """
    from timagetk.components.image import get_image_attributes
    attr = get_image_attributes(image, exclude=["dtype"], extra=['filename'])

    smooth_img = gaussian_filter(image, sigma=1., real=False)

    if threshold is None:
        threshold = np.percentile(image.get_array(), 90)

    # Detect the main object:
    mask_img = np.where(smooth_img <= threshold, 0, 1)
    mask_img = nd.binary_fill_holes(mask_img)
    mask_img = LabelledImage(mask_img, not_a_label=0, **attr)

    return mask_img


def object_detection_labelling(image, threshold=None):
    """Detect the object within a 3D intensity image.

    Parameters
    ----------
    image : timagetk.SpatialImage
        intensity image containing an object to detect
    threshold : int or float, optional
        Threshold value to use.
        If the `threshold` is left to ``None`` (default), it will be automatically computed as the 90^th percentile.

    Returns
    -------
    timagetk.LabelledImage
        A binary labelled image, where 1 indicate the main object, 0 the background

    Examples
    --------
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.mplt import intensity_histogram
    >>> from timagetk.visu.stack import stack_browser
    >>> from timagetk.visu.orientation import object_detection_labelling
    >>> # Using shared data as example:
    >>> img = shared_data('flower_confocal', 0)
    >>> intensity_histogram(image)
    >>> mask = main_object_detection(image)
    >>> stack_browser(mask, val_range='auto')

    """
    from timagetk.components.image import get_image_attributes
    attr = get_image_attributes(image, exclude=["dtype"], extra=['filename'])

    sigma = 1.5 * max(image.get_voxelsize())
    smooth_img = gaussian_filter(image, sigma=sigma, real=True)

    if threshold is None:
        threshold = np.percentile(image.get_array(), 90)

    # Detect the main object:
    mask_img = connected_components(smooth_img, method="connected_components", low_threshold=threshold, max=True)

    if len(mask_img.labels()) == 1:
        mask_img = np.where(smooth_img <= threshold, 0, 1)
        mask_img = nd.binary_fill_holes(mask_img)

    mask_img = LabelledImage(mask_img, not_a_label=0, **attr)

    return mask_img


def _object_main_axes(mask_img):
    """Compute the main axes of inertia of an object in a binary labelled image.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Binary labelled image containing a unique component with value 1

    Returns
    -------
    numpy.ndarray
        Array of main axes norms
    numpy.ndarray
        Array of main axes unit vectors, ordered by rows, *i.e.* ``vectors[0]`` correspond to ``norms[0]``

    """
    vxs = np.array(mask_img.voxelsize).reshape((mask_img.ndim, 1))
    coord = mask_img.nonzero()
    center_vox = np.array(nd.center_of_mass(mask_img, mask_img, index=1))

    coord = coordinates_centering3D(coord, center_vox)
    coord = vxs * coord
    # compute the variance-covariance matrix (1/n_vox * P . P^T):
    cov = compute_covariance_matrix(coord)

    # Find & returns the eigen values and vectors.
    # np.linalg.eig return eigenvectors by column !!
    eig_val, eig_vec = np.linalg.eig(cov)

    return eig_val, eig_vec


def object_main_axes(image):
    """Compute the main axes of an object.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.LabelledImage
        Intensity image containing an object for which to compute the main axes or a labelled image wit only one label

    Returns
    -------
    numpy.ndarray
        Array of main axes norms.
    numpy.ndarray
        Array of main axes unit vectors, *i.e.* ``vectors[0]`` correspond to ``norms[0]``

    Examples
    --------
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.orientation import object_main_axes
    >>> # Using shared data as example:
    >>> fname = 'p58-t0-a0.lsm'
    >>> img = imread(shared_data(fname), "p58")

    >>> norms, vect = object_main_axes(img)

    """
    if isinstance(image, LabelledImage):
        # Only one labels should be defined:
        assert image.labels() == [1]
        axes = _object_main_axes(image)
    else:
        image = object_detection_threshold(image)
        axes = _object_main_axes(image)

    return axes


DETECT_METHODS = ['threshold', 'labelling']


def main_axes_trsf(image, method="threshold"):
    """Transformation to orientation an image according to the signal inertia axis.

    Parameters
    ----------
    image : timagetk.SpatialImage
        An intensity image containing an object to lay "flat" within its view
    method : {'threshold', 'labelling'}
        Method to use to detect the object.

    Returns
    -------
    Trsf
        Transformation to orient the intensity image containing the object along its inertia axis.
    timagetk.LabelledImage
        The isometric mask image obtained from "object detection".

    Notes
    -----
    The ``threshold`` method is faster but may detect some noise.
    The ``labelling`` method is slower but detect the object more accurately since it try to label the region with the object.
    However, it may fail due to the lack of robustness by watershed.

    The intensity image is resampled to isometric voxel size prior to "object detection".

    See Also
    --------
    timagetk.visu.orientation.object_detection_threshold
    timagetk.visu.orientation.object_detection_labelling

    Example
    -------
    >>> import numpy as np
    >>> from math import sqrt
    >>> from timagetk import SpatialImage
    >>> from timagetk import Trsf
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.orientation import main_axes_trsf
    >>> from timagetk.algorithms.quaternion import centered_rotation_trsf
    >>> from timagetk.algorithms.trsf import apply_trsf
    >>> from timagetk.algorithms.trsf import compose_trsf
    >>> from timagetk.algorithms.trsf import inv_trsf
    >>> from timagetk.visu.stack import orthogonal_view
    >>> # Load the sphere image:
    >>> img_path = shared_dataset("sphere")[0]
    >>> image = imread(img_path)
    >>> # Deform it: stretch it by 2 in the x-axis and center-rotate it by 45° in the xy-plane
    >>> stretch_trsf = np.identity(4, dtype=float)
    >>> stretch_trsf[0,0] = 0.5  # stretch x-axis by a factor two!
    >>> stretch_trsf = Trsf(stretch_trsf)
    >>> rotate_trsf = centered_rotation_trsf(image, 45, axis="z")
    >>> template = SpatialImage(np.zeros(list(map(int, [image.get_shape('z'), image.get_shape('y')*sqrt(2), image.get_shape('x')*sqrt(2)]))), voxelsize=image.voxelsize)
    >>> image = apply_trsf(image, compose_trsf([stretch_trsf, rotate_trsf]), template_img=template)
    >>> orthogonal_view(image, val_range="auto")
    >>> # Detect object inertia axes and get rigid transformation to "align it" in XYZ:
    >>> trsf, mask_img = main_axes_trsf(image)

    """
    from timagetk.algorithms.quaternion import quaternion_translation_matrix
    from timagetk.algorithms.trsf import compose_trsf
    from timagetk.algorithms.trsf import inv_trsf
    if method == "threshold":
        mask_img = object_detection_threshold(isometric_resampling(image, value='max'))
    elif method == "labelling":
        mask_img = object_detection_labelling(isometric_resampling(image, value='max'))
    else:
        msg = "Unknown method {}, choose among {}."
        raise ValueError(msg.format(method, DETECT_METHODS))

    mask_img = mask_img.transpose("xyz")  # needs an XYZ array!
    vxs = np.array(mask_img.get_voxelsize())

    # Get object barycenter in REAL coordinates:
    center_vox = nd.center_of_mass(mask_img, mask_img, index=1)
    center_vox = vxs * np.array(center_vox)

    # Coordinates centering:
    coord = vxs.reshape((mask_img.ndim, 1)) * np.array(mask_img.nonzero())
    coord = coordinates_centering3D(coord, center_vox)

    # compute the variance-covariance matrix (1/n_vox * P . P^T):
    cov = compute_covariance_matrix(coord)

    # Singular value decomposition returns eigen values (diagonal of D) and eigen vectors:
    r_bef, eig_val, r_aft_t = np.linalg.svd(cov)

    # Translation quaternion (centered rotation):
    trans = np.array(mask_img.get_extent()) / 2.
    trans_q = quaternion_translation_matrix(*trans)

    # Rotation quaternion:
    rotat_q = np.zeros((4, 4), dtype=float)
    rotat_q[:3, :3] = r_bef
    rotat_q[3, 3] = 1.

    # Create ``Trsf`` objects from quaternions and compose them:
    from timagetk import Trsf
    trans_trsf = Trsf(trans_q, trsf_unit='real')
    rotat_trsf = Trsf(rotat_q, trsf_unit='real')
    trsf = compose_trsf([trans_trsf, rotat_trsf, inv_trsf(trans_trsf)])

    return trsf, mask_img.transpose(image.axes_order)


def main_axes_orientation(image, method="threshold", use_bbox=True, margin_pc=10, **kwargs):
    """Automatic orientation of an image according to the signal inertia axis.

    Parameters
    ----------
    image : timagetk.SpatialImage
        An intensity image containing an object to lay "flat" within its view
    method : {'threshold', 'labelling'}
        Method to use to detect the object

    Returns
    -------
    timagetk.SpatialImage
        Intensity image containing the object, oriented along its inertia axis.

    Notes
    -----
    The ``threshold`` method is faster but may detect some noise.
    The ``labelling`` method is slower but detect the object more accurately since it try to label the region with the object.
    However, it may fail due to the lack of robustness by watershed.

    Example
    -------
    >>> import numpy as np
    >>> from math import sqrt
    >>> from timagetk import SpatialImage
    >>> from timagetk import Trsf
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.orientation import main_axes_orientation
    >>> from timagetk.algorithms.quaternion import centered_rotation_trsf
    >>> from timagetk.algorithms.trsf import apply_trsf
    >>> from timagetk.algorithms.trsf import compose_trsf
    >>> from timagetk.visu.stack import orthogonal_view
    >>> # Load the sphere image:
    >>> img_path = shared_dataset("sphere")[0]
    >>> image = imread(img_path)
    >>> # Deform it: stretch it by 2 in the x-axis and center-rotate it by 45° in the xy-plane
    >>> stretch_trsf = np.identity(4, dtype=float)
    >>> stretch_trsf[0,0] = 0.5  # stretch x-axis by a factor two!
    >>> stretch_trsf = Trsf(stretch_trsf)
    >>> rotate_trsf = centered_rotation_trsf(image, 45, axis="z")
    >>> template = SpatialImage(np.zeros(list(map(int, [image.get_shape('z'), image.get_shape('y')*sqrt(2), image.get_shape('x')*sqrt(2)]))), voxelsize=image.voxelsize)
    >>> image = apply_trsf(image, compose_trsf([stretch_trsf, rotate_trsf]), template_img=template)
    >>> orthogonal_view(image, val_range="auto")
    >>> print(image.get_voxelsize())
    [0.25, 0.25, 0.25]
    >>> print(image.get_shape())
    [225, 318, 318]
    >>> # Align the image along its inertia axis using an intensity threshold to detect the object:
    >>> auto_rotated_img = main_axes_orientation(image, use_bbox=False)
    >>> print(auto_rotated_img.get_voxelsize())
    [0.25, 0.25, 0.25]
    >>> print(auto_rotated_img.get_shape())  # image shape is unchanged
    [225, 318, 318]
    >>> orthogonal_view(auto_rotated_img, val_range="auto", suptitle="Inertia axis rotated image")
    >>> # Align the image along its inertia axis using an intensity threshold and a bounding-box to increase margins:
    >>> auto_rotated_img = main_axes_orientation(image, use_bbox=True)
    >>> print(auto_rotated_img.get_voxelsize())
    [0.25, 0.25, 0.25]
    >>> print(auto_rotated_img.get_shape())  # image shape is fitted to the detected object
    [159, 161, 320]
    >>> orthogonal_view(auto_rotated_img, val_range="auto", suptitle="Inertia axis rotated image with bounding box")

    # Create Max Intensity Projections and display them:
    >>> from timagetk.visu.projection import projection
    >>> proj_img = [projection(im, method="maximum") for im in [image, auto_rotated_img]]

    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> img_names = ["Original image", "Main axis rotated image"]
    >>> fig = grayscale_imshow(proj_img, val_range=[0, 255], suptitle="Main axis automatic rotation", title=img_names)
    """
    from timagetk.bin.logger import get_dummy_logger
    logger = get_dummy_logger(sys._getframe().f_code.co_name)

    from timagetk.components.image import get_image_attributes
    attr = get_image_attributes(image, exclude=["dtype", "voxelsize"], extra=['filename'])

    from timagetk.algorithms.trsf import apply_trsf
    trsf, mask_img = main_axes_trsf(image, method=method)
    if use_bbox:
        from timagetk import Trsf
        from timagetk.algorithms.pointmatching import apply_trsf_to_points
        from timagetk.algorithms.quaternion import quaternion_translation_matrix
        from timagetk.algorithms.trsf import compose_trsf
        from timagetk.algorithms.trsf import inv_trsf
        mask_img = mask_img.transpose("xyz")  # needs an XYZ array!
        mask_pts = np.array(mask_img.get_voxelsize()) * np.array(mask_img.nonzero()).T  # real coordinates, XYZ sorted!
        res_pts = apply_trsf_to_points(mask_pts, inv_trsf(trsf))  # res_pts is XYZ sorted!
        mini = np.min(res_pts, axis=0)
        maxi = np.max(res_pts, axis=0)
        new_extent = (maxi - mini)[::-1]  # new image extent, ZYX sorted!
        logger.debug(f"Found ZYX extent: {new_extent}")
        extent_margin_pc = 1 + (margin_pc / 100)  # margin around detected mask
        arr_shape = list(map(int, np.ceil(new_extent / mask_img.get_voxelsize() * extent_margin_pc)))
        extent_translate = (new_extent - new_extent * extent_margin_pc)[::-1] / 2
        logger.debug(f"New image shape: {arr_shape}")
        translate = Trsf(quaternion_translation_matrix(*mini + extent_translate),
                         trsf_unit='real')  # to move the origin to (0, 0, 0)
        trsf = compose_trsf([trsf, translate])
        template = SpatialImage(np.zeros(arr_shape), voxelsize=mask_img.get_voxelsize(), dtype="uint8", **attr)
    else:
        template = image

    # Apply transformation:
    res_image = apply_trsf(image, trsf, template_img=template, **kwargs)
    return res_image
