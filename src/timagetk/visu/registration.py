#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

from matplotlib import pyplot as plt
import matplotlib.patches as patches

from timagetk.algorithms.trsf import apply_trsf
from timagetk.algorithms.blockmatching import compute_pyramid_infos
from timagetk.algorithms.resample import resample
from timagetk.components.multi_channel import combine_channels
from timagetk.visu.stack import orthogonal_view
from timagetk.visu.mplt import image_plot

def spimshow(ax, spim, title=""):
    """Plot a 2D image on the given axis.

    Parameters
    ----------
    ax : matplotlib.axes.Axes
        the``Axes`` element where to plot hte figure
    spim : timagetk.SpatialImages
        Image to show.
    title : str
        The title to give to the figure.
    """
    spim_extent = [-spim.get_voxelsize('x') / 2, spim.get_extent('x') + spim.get_voxelsize('x') / 2,
                   spim.get_extent('y') - spim.get_voxelsize('y') / 2, -spim.get_voxelsize('y') / 2]
    ax.imshow(spim, cmap='gray', extent=spim_extent, vmin=0, vmax=255)
    ax.xaxis.tick_top()
    if title != "":
        ax.set_title(title)
    else:
        try:
            ax.set_title(f"{spim.filename}")
        except:
            pass
    return


def registration_snapshot(images, trsfs, title, figname=""):
    """Create a snapshot for registrations.

    Parameters
    ----------
    images : list[timagetk.SpatialImage]
        List of intensity images, the first one is the reference and the other are floating image that a registered by the transformation in the list
        Thus ``len(images) = len(trsfs) + 1``.
    trsfs : list of Trsf
        List of transformation instances, the order should match the floating image list.
    title : str
        The title to give to the figure.
    figname : str
        The file name to use to save the snapshot.
        If set, the figure is closed upon saving.

    """
    ref = images[0]
    floats = images[1:]
    reg_floats = []
    for n, img in enumerate(floats):
        trsf = trsfs[n]
        if trsf.is_linear():
            reg_float = apply_trsf(img, trsf, template_img=ref)
        else:
            reg_float = apply_trsf(img, trsf)
        reg_floats.append(reg_float)

    images = [ref] + reg_floats
    blend = combine_channels(images)
    cuts = [1 / 5., 1 / 3., 1 / 2., 2 / 3.]
    z_sh = ref.get_shape('z')
    z_slices = [int(round(c * z_sh, 0)) for c in cuts]

    fig, axes = plt.subplots(1, 4)
    fig.set_size_inches(15, 5)
    fig.set_dpi(160)
    fig.suptitle(title)
    for n, zsl in enumerate(z_slices):
        spimshow(axes[n], blend[zsl, :, :], title=f"z-slice {zsl}/{z_sh}")

    fig.subplots_adjust(wspace=0, hspace=0)
    fig.tight_layout()
    if figname != "":
        fig.savefig(figname)
        plt.close()
    else:
        plt.show()

def visualize_pyramid_levels(image, min_img_size=16, block_size=5, max_squares=10):
    """Visualize orthogonal slices (XY, YZ, ZX) of each 3D image at each pyramid level,
    with each column corresponding to a pyramid level.

    Parameters
    ----------
    image : timagetk.SpatialImage
        Object containing image dimensions and voxel sizes.
    min_img_size : int
        Minimum size for any dimension of the image at a pyramid level.
    block_size : int
        Minimum block size for any dimension.
    max_squares : int
        Maximum number of squares representing the blockmatching "block" to display.

    Returns
    -------
    fig : matplotlib.figure.Figure
        A matplotlib figure object with the visualized pyramid levels.

    Examples
    --------
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> img = shared_data('flower_confocal', 0)

    >>> from timagetk.visu.registration import visualize_pyramid_levels
    >>> fig = visualize_pyramid_levels(img)
    """
    # Compute the pyramid levels for the input image
    pyramid_dict = compute_pyramid_infos(image, min_img_size=min_img_size, block_size=block_size)
    pyramid_shape = pyramid_dict['shape']

    n_levels = len(pyramid_shape)
    fig, axs = plt.subplots(4, n_levels, figsize=(4*n_levels, 12), gridspec_kw={'height_ratios': [1, 5, 5, 5]})

    # Ensure axs is a 2D array even when n_levels is 1
    if n_levels == 1:
        axs = [axs]

    for i_level, (level, size) in enumerate(pyramid_shape.items()):
        # Resample the image to get the desired shape & plot it
        image_resampled = resample(image, shape=size)

        # Extract middle slices
        z_slice = image_resampled.shape[0] // 2
        y_slice = image_resampled.shape[1] // 2
        x_slice = image_resampled.shape[2] // 2

        slices = [
            image_resampled.get_slice(z_slice, 'z'),  # XY slice
            image_resampled.get_slice(y_slice, 'y'),  # ZX slice
            image_resampled.get_slice(x_slice, 'x')   # YZ slice
        ]


        # Display slices
        for i_axis, slice_img in enumerate(slices):
            ax = axs[i_axis + 1, i_level]
            # Assuming image_plot is a function that can display a 2D slice in a given axis
            ax, xy_fig = image_plot(slice_img, axe=ax, cmap='gray', val_range='auto')  # Adjust parameters as needed

            # Adjust block size to real units
            real_block_size = [block_size * vx for vx in slice_img.voxelsize]

            # Determine the number of squares that can fit within the central area of the image
            num_squares_x = min(max_squares, int(slice_img.shape[1] / block_size))
            num_squares_y = min(max_squares, int(slice_img.shape[0] / block_size))

            # Compute the total space occupied by the squares
            total_square_space_x = num_squares_x * real_block_size[1]
            total_square_space_y = num_squares_y * real_block_size[0]

            # Compute the starting position to display the squares
            start_x = (slice_img.extent[1] - total_square_space_x) / 2
            start_y = (slice_img.extent[0] - total_square_space_y) / 2

            # Draw red squares
            for i in range(num_squares_x):
                for j in range(num_squares_y):
                    rect_x = start_x + i * real_block_size[1]
                    rect_y = start_y + j * real_block_size[0]
                    rect = patches.Rectangle((rect_x, rect_y),
                                             real_block_size[1], real_block_size[0],
                                             linewidth=1, edgecolor='r', facecolor='none')
                    ax.add_patch(rect)

            slice_name = ['XY Slice', 'YZ Slice', 'ZX Slice']
            ax.set_title(slice_name[i_axis])

        # Use the first column for level indication
        text_ax = axs[0, i_level]
        text_ax.axis('off')  # Turn off axis
        text_ax.text(0.5, 0.5, f'Level {i_level}', ha='center', va='center', fontsize=20)

    plt.tight_layout()
    plt.show()
    return fig
