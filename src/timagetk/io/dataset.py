#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Shared Data Module.

This module provides access to shared datasets and synthetic data, including functionality
to retrieve experimental and simulated images stored locally or hosted on Zenodo.
It supports multidimensional imaging data and landmarks for biological research visualisation.

The functionalities include:

 - Accessing experimental datasets from different sources using identifiers.
 - Generating synthetic 3D image stacks for biological simulations.
 - Listing available shared and synthetic datasets.
 - Reading and processing both labelled and unlabelled images.
 - Handling landmarks and associated file paths for specific datasets.

The Zenodo records accessible are:

  1. `flower_multiangle`
    - **Description**: Multi-angle LSM images of _Arabidopsis thaliana_ early floral meristem.
    - **URL**: `https://zenodo.org/records/7151866`
    - **Images**:
      1. `090223-p58-flo-top.lsm`
      2. `090223-p58-flo-tilt1.lsm`
      3. `090223-p58-flo-tilt2.lsm`
  2. `flower_confocal`
    - **Description**: Time-series intensity images of _Arabidopsis thaliana_ early floral meristem development.
    - **URL**: `https://zenodo.org/records/7151866`
    - **Images**:
      1. `p58-t0-imgFus.inr.gz`
      2. `p58-t1-imgFus.inr.gz`
      3. `p58-t2-imgFus.inr.gz`
  3. `flower_labelled`
    - **Description**: Time-series labelled images of _Arabidopsis thaliana_ early floral meristem development.
    - **URL**: `https://zenodo.org/records/7151866`
    - **Images**:
      1. `p58-t0-imgSeg.inr.gz`
      2. `p58-t1-imgSeg.inr.gz`
      3. `p58-t2-imgSeg.inr.gz`
  4. `sam_confocal`
    - **Description**: Time-series multichannel images of _Arabidopsis thaliana_ shoot apical meristem development with fluorescent reporters.
    - **URL**: `https://zenodo.org/records/3737795`
    - **Images**:
      1. `qDII-CLV3-DR5-E25-LD-SAM1.czi`
      2. `qDII-CLV3-DR5-E25-LD-SAM1-T5.czi`
      3. `qDII-CLV3-DR5-E25-LD-SAM1-T10.czi`

"""

from importlib.resources import files

import numpy as np

from timagetk import LabelledImage
from timagetk.io import imread
from timagetk.io.image import _image_from_url
from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image
from timagetk.synthetic_data.multichannel_image import example_layered_sphere_multichannel_image
from timagetk.synthetic_data.nuclei_image import example_nuclei_image
from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image

ZENODO_RECORDS = {
    # Multi-angle LSM images of *Arabidopsis thaliana* early floral meristem
    'flower_multiangle': {
        0: {
            'url': 'https://zenodo.org/records/7151866/files/090223-p58-flo-top.lsm',
            'hash': 'md5:5548917d3d1490200d0d56cbb31c0d35'
        },
        1: {
            'url': 'https://zenodo.org/records/7151866/files/090223-p58-flo-tilt1.lsm',
            'hash': 'md5:fa004a84100fcd5ff1d2f9d7c5cbda30'
        },
        2: {
            'url': 'https://zenodo.org/records/7151866/files/090223-p58-flo-tilt2.lsm',
            'hash': 'md5:006bf3bacb667eb72d0fb9a6419edcc1'
        }
    },
    # Time-series intensity images of *Arabidopsis thaliana* early floral meristem development
    'flower_confocal': {
        0: {
            'url': 'https://zenodo.org/records/7151866/files/p58-t0-imgFus.inr.gz',
            'hash': 'md5:48f6f9924289037c55ea785273c2fe72'
        },
        1: {
            'url': 'https://zenodo.org/records/7151866/files/p58-t1-imgFus.inr.gz',
            'hash': 'md5:377dfa1b2aaf2471e5f8ab5655ef7610'
        },
        2: {
            'url': 'https://zenodo.org/records/7151866/files/p58-t2-imgFus.inr.gz',
            'hash': 'md5:0c08a63c0966bd5655e4676893dd1068'
        }
    },
    # Time-series labelled images of *Arabidopsis thaliana* early floral meristem development
    'flower_labelled': {
        0: {
            'url': 'https://zenodo.org/records/7151866/files/p58-t0-imgSeg.inr.gz',
            'hash': 'md5:3e9259e801c39ec5f8f235deeb8e936c'
        },
        1: {
            'url': 'https://zenodo.org/records/7151866/files/p58-t1-imgSeg.inr.gz',
            'hash': 'md5:74f085176500e54caa7e8bf708ace12f'
        },
        2: {
            'url': 'https://zenodo.org/records/7151866/files/p58-t2-imgSeg.inr.gz',
            'hash': 'md5:a95448cf8325836d908b55feeb71879e'
        }
    },
    # Time-series multichannel images of *Arabidopsis thaliana* shoot apical meristem development with fluorescent reporters
    'sam_confocal': {
        0: {
            'url': 'https://zenodo.org/records/3737795/files/qDII-CLV3-DR5-E25-LD-SAM1.czi',
            'hash': 'md5:35fe1d1e64736d628d43ae7e2aa14dc6'
        },
        1: {
            'url': 'https://zenodo.org/records/3737795/files/qDII-CLV3-DR5-E25-LD-SAM1-T5.czi',
            'hash': 'md5:e7c229b057ff4fdf8775ef1e94937651'
        },
        2: {
            'url': 'https://zenodo.org/records/3737795/files/qDII-CLV3-DR5-E25-LD-SAM1-T10.czi',
            'hash': 'md5:6373815cd16afd2e24ded7dff6a986b8'
        }
    }

}

LOCAL_RECORDS = {
    'flower_multiangle_landmarks': {
        (0, 1): ["p58_t0_reference_ldmk-01.txt", "p58_t0_floating_ldmk-01.txt"],
        (0, 2): ["p58_t0_reference_ldmk-02.txt", "p58_t0_floating_ldmk-02.txt"],
    }
}

DATA_REGISTRY = ZENODO_RECORDS | LOCAL_RECORDS

SYNTHETIC_REGISTRY = ['wall', 'nuclei', 'labelled', 'multichannel']


def get_synthetic_data(type_name, **kwargs):
    """Generates synthetic data.

    Parameters
    ----------
    type_name : str
        Specifies the type of synthetic data to generate.
        Should be one of 'wall', 'nuclei', 'labelled', or 'multichannel'.

    Other Parameters
    ----------------
    extent : float, optional
        The extent of the output image (in µm).
        Defaults to ``60.``.
    voxelsize : list[float], optional
        Length-3 list of image voxelsize (in µm), ZYX sorted.
        Defaults to ``(0.6, 0.6, 0.6)``.
    n_points : int, optional
        The number of cells on innermost spherical layer.
        Defaults to ``12``.
    n_layers : int, optional
        The number of layer around the central cell.
        Defaults to ``1``.
    wall_sigma : float, optional
        Only valid if ``type_name == 'wall'``.
        Sigma value determining the wall width, in real units.
        Defaults to the image voxelsize ``seg_img.voxelsize``.
    intensity : float, optional
        Only valid if ``type_name == 'wall'`` or ``type_name == 'nuclei'``.
        Signal intensity of the wall interfaces or nuclei.
        Defaults to 2/3 of the maximum value based on `dtype`.
    dtype : str or numpy.dtype
        A valid numpy dtype for the image.
    """
    if type_name == 'wall':
        return example_layered_sphere_wall_image(**kwargs)
    elif type_name == 'nuclei':
        return example_nuclei_image(**kwargs)
    elif type_name == 'labelled':
        return example_layered_sphere_labelled_image(**kwargs)
    elif type_name == 'multichannel':
        return example_layered_sphere_multichannel_image(**kwargs)
    else:
        raise ValueError(f"Invalid synthetic data type: {type_name}")


def shared_data_path(name, img_id=None, **kwargs):
    """Access shared dataset path from Zenodo examples.

    Parameters
    ----------
    name : str
        The dataset name to retrieve the image from.
    img_id : str or int, optional
        The identifier for the image within the dataset.

    Returns
    -------
    pathlib.Path or dict[pathlib.Path]
        The path to the Zenodo shared image from the provided dataset name and image ID.

    Raises
    ------
    ValueError
        If the dataset name is unknown.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data_path
    >>> # Example - Get a 3D confocal image of an early flower of Arabidopsis thaliana
    >>> path = shared_data_path('flower_confocal', 0)
    >>> print(path)
    $HOME/.cache/timagetk/44b7f3d0bccef67a517cd55ad4e6a9ff-p58-t0-imgFus.inr.gz
    >>> path = shared_data_path('flower_confocal')
    >>> print(path)
    {0: '$HOME/.cache/timagetk/44b7f3d0bccef67a517cd55ad4e6a9ff-p58-t0-imgFus.inr.gz', 1: '$HOME/.cache/timagetk/d47e26514b326cb3464fe7fbcd95b7f4-p58-t1-imgFus.inr.gz', 2: '$HOME/.cache/timagetk/c381fb2c79e6b6cad13e14f480e6ea08-p58-t2-imgFus.inr.gz'}
    >>> # Example 3 - Get a list of landmarks for the fusion of 3D confocal image
    >>> landmarks_path = shared_data_path('flower_multiangle_landmarks')
    >>> landmarks_path = shared_data_path('flower_multiangle_landmarks', (0, 1))

    """
    root = files('timagetk')  # Get the root directory for the 'timagetk' package
    # Define the path to the landmarks data directory
    root_data = root.joinpath('../../data/p58/multiangle_landmarks/')

    # Check if the dataset name exists in the registry
    if name in DATA_REGISTRY:
        # If no specific image ID is provided
        if img_id is None:
            dataset = DATA_REGISTRY[name]  # Retrieve the dataset corresponding to the name
            if 'landmarks' in name:
                # Return a dictionary mapping dataset indices to file paths for each associated landmark
                return {idx: [root_data.joinpath(fname) for fname in data] for idx, data in dataset.items()}
            else:
                # Return a dictionary mapping dataset indices to images fetched from URLs
                return {idx: _image_from_url(data['url'], hash_value=data['hash']) for idx, data in dataset.items()}

        # If a specific image ID is provided and exists in the registry
        elif img_id in DATA_REGISTRY[name]:
            if 'landmarks' in name:
                file_names = DATA_REGISTRY[name][img_id]  # Get the file names associated with the image ID
                # Return the file paths for the landmarks associated with the given image ID
                return [root_data.joinpath(fname) for fname in file_names]
            else:
                # Fetch and return the image from the given URL, verifying its hash
                return _image_from_url(DATA_REGISTRY[name][img_id]['url'],
                                       hash_value=DATA_REGISTRY[name][img_id]['hash'])

        # Raise an error if the provided image ID does not exist in the dataset
        else:
            raise ValueError(f"Unknown identifier '{img_id}' for dataset name '{name}'!")

    # Raise an error if the dataset name does not exist in the registry
    else:
        raise ValueError(f"Unknown dataset name '{name}'!")


def shared_data(name, img_id, **kwargs):
    """Access shared dataset, either synthetic data or Zenodo examples.

    Parameters
    ----------
    name : str
        The dataset name to retrieve the image from.
    img_id : str or int
        The identifier for the image within the dataset.
    kwargs : dict, optional
        Additional arguments passed to the `get_synthetic_data` function.

    Returns
    -------
    timagetk.SpatialImage or timagetk.LabelledImage
        The image read from the provided dataset name and image ID.

    Raises
    ------
    ValueError
        If the dataset name is unknown.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.stack import orthogonal_view
    >>> # Example 1 - Get a 3D confocal image of an early flower of Arabidopsis thaliana
    >>> img = shared_data('flower_confocal', 0)
    >>> v = orthogonal_view(img, cmap='viridis', val_range='auto')
    >>> # Example 2 - Get a synthetic 3D stack of segmented cells
    >>> seg_img = shared_data('synthetic', 'labelled')
    >>> v = orthogonal_view(seg_img, cmap='glasbey', val_range='auto')
    >>> # Example 3 - Get a list of landmarks for the fusion of 3D confocal image
    >>> landmarks = shared_data('flower_multiangle_landmarks', (0, 1))
    >>> # Example 4 - Get a dictionary of 3 multi-angle images of early flowers of Arabidopsis thaliana
    >>> ma_images = {ma_im_idx: shared_data('flower_multiangle', ma_im_idx) for ma_im_idx in range(3)}

    """
    # Check if the dataset name is in the registry and if it relates to 'landmarks'.
    if name in DATA_REGISTRY and 'landmarks' in name:
        # Ensure that the given image identifier (img_id) is valid within the dataset.
        if img_id not in DATA_REGISTRY[name]:
            # Raise an error if the image identifier doesn't exist in the dataset registry.
            raise ValueError(f"Unknown identifier '{img_id}' for dataset name '{name}'!")
        # Generate file paths for shared data based on the dataset name and image ID.
        file_names = shared_data_path(name, img_id=img_id, **kwargs)
        # Load and return the content of the files as numpy arrays.
        return [np.loadtxt(fname) for fname in file_names]

    # Check if the dataset name is in the registry and the image ID exists for the given dataset.
    elif name in DATA_REGISTRY and img_id in DATA_REGISTRY[name]:
        # Handle cases where the dataset refer to 'labelled' image.
        if 'labelled' in name:
            # Read the image and return it as a LabelledImage object, marking 0 as "not_a_label".
            return imread(DATA_REGISTRY[name][img_id]['url'],
                          hash_value=DATA_REGISTRY[name][img_id]['hash'],
                          rtype=LabelledImage, not_a_label=0)
        else:
            # Read and return a standard image using the reference URL and checksum hash.
            return imread(DATA_REGISTRY[name][img_id]['url'],
                          hash_value=DATA_REGISTRY[name][img_id]['hash'])

    # Handle the special case of 'synthetic' dataset names.
    elif name == 'synthetic':
        # Generate and return synthetic data for the given image ID.
        return get_synthetic_data(img_id, **kwargs)

    # If the dataset name is unknown, raise an appropriate error.
    else:
        raise ValueError(f"Unknown dataset name '{name}'!")


def list_shared_data():
    """Lists the shared dataset names.

    Returns
    -------
    list[str]
        A list of dataset names available in the registry.
    """
    return list(DATA_REGISTRY.keys())


def list_synthetic_data():
    """Lists the shared synthetic dataset names.

    Returns
    -------
    list[str]
        A list of synthetic dataset names available in the registry.
    """
    return SYNTHETIC_REGISTRY
