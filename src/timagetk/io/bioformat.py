#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""This module implement an image reader/writer based on python-bioformat library.

It extends the I/O capabilities of timagetk beyond implemented readers & writers.

.. note:: Library `python-bioformat` depend on `python-javabridge`.
"""

from bioformats.omexml import get_float_attr
from bioformats.omexml import get_int_attr

from timagetk.components.spatial_image import SpatialImage
from timagetk.io.dataset import shared_data


def start_jvm():
    """

    """
    # - Starting the Java virtual machine with python-javabridge:
    javabridge.start_vm(class_path=bioformats.JARS)


def stop_jvm():
    """

    """
    javabridge.kill_vm()


def read_bioformat(fname, channel_names=None, verbose=True):
    """Image reader based on java library bioformat.

    Parameters
    ----------
    fname : str
        Path to the image
    channel_names : str, optional
        If given (default=None), name of a channel
    verbose : bool
        If ``True``, increase verbosity, else keep it minimal.

    Returns
    -------
    sp_image : timagetk.SpatialImage
        Image and metadata (such as voxelsize, extent, type, etc.)

    Example
    -------
    >>> from timagetk.io.dataset import shared_data_path
    >>> from timagetk.io.bioformat import read_bioformat
    >>> image_path = shared_data_path('flower_confocal', 0)
    >>> sp_image = read_bioformat(image_path)

    """
    # - Start the Java virtual machine:
    start_jvm()
    # - Create a reader:
    # Make or find an image reader appropriate for the given path:
    reader = bioformats.get_image_reader(0, path=fname)
    n_series = reader.rdr.getSeriesCount()

    # - Get metadata from image and convert
    metadata_xml = bioformats.get_omexml_metadata(path=fname).encode('utf-8')
    img_metadata = bioformats.OMEXML(metadata_xml)

    img_series = {}
    for s in range(n_series)[:1]:
        reader.rdr.setSeries(s)
        z_sh = reader.rdr.getSizeZ()
        t_sh = reader.rdr.getSizeT()
        planes = [[reader.read(c=None, z=z, t=t, rescale=False) for z in range(z_sh)] for t in
                  range(t_sh)]
        img = np.array(planes)

        if reader.rdr.getSizeC() == 1:
            img = img[:, :, :, :, np.newaxis]

        img = np.transpose(img, (1, 4, 3, 2, 0))

        series_name = img_metadata.image(s).Name

        # - Get 'voxelsize' from metadata:
        vx = get_float_attr(img_metadata.image(s).Pixels.node, "PhysicalSizeX")
        vy = get_float_attr(img_metadata.image(s).Pixels.node, "PhysicalSizeY")
        vz = get_float_attr(img_metadata.image(s).Pixels.node, "PhysicalSizeZ")
        vx = 1 if vx is None else vx
        vy = 1 if vy is None else vy
        vz = 1 if vz is None else vz
        if verbose:
            print("Found 'voxelsize' values:", (vx, vy, vz))

        # - Get 'bit-depth' & 'bit-type':
        bit_depth = get_int_attr(img_metadata.image(s).Pixels.node, "SignificantBits")
        dtype = img_metadata.image(s).Pixels.get_PixelType()

        if img.shape[0] > 1:
            for t in range(img.shape[0]):
                img_series[series_name + "_T" + str(t).zfill(2)] = {}

                for c in range(img.shape[1]):
                    image = img[t, c]
                    if image.max() <= 1.0:
                        image = image * (np.power(2, bit_depth) - 1)
                    image = SpatialImage(image, voxelsize=(vx, vy, vz),
                                         dtype=dtype)

                    if channel_names is not None and len(channel_names) == img.shape[1]:
                        channel_id = channel_names[c]
                    else:
                        channel_id = img_metadata.image(s).Pixels.Channel(c).ID
                    if channel_id is None:
                        channel_id = "C" + str(c)
                    img_series[series_name + "_T" + str(t).zfill(2)][
                        channel_id] = image

                if img.shape[1] == 1:
                    img_series[series_name + "_T" + str(t).zfill(2)] = \
                        img_series[series_name + "_T" + str(t).zfill(2)].values()[0]
        else:
            img_series[series_name] = {}
            for c in range(img.shape[1]):
                image = np.copy(img[0, c])
                if image.max() <= 1.0:
                    image = image * (np.power(2, bit_depth) - 1)
                image = SpatialImage(image, voxelsize=(vx, vy, vz), dtype=dtype)
                if channel_names is not None and len(channel_names) == img.shape[1]:
                    channel_id = channel_names[c]
                else:
                    channel_id = img_metadata.image(s).Pixels.Channel(c).Name
                if channel_id is None:
                    channel_id = img_metadata.image(s).Pixels.Channel(c).ID
                    if channel_id is None:
                        channel_id = "C" + str(c)
                img_series[series_name][channel_id] = image
            if img.shape[1] == 1:
                img_series[series_name] = img_series[series_name].values()[0]

    # - Tell the cache that it should flush the reference for the given key
    reader.release_image_reader(0)
    # - Stop the Java virtual machine:
    stop_jvm()

    if n_series == 1:
        return img_series.values()[0]
    else:
        return img_series


def write_bioformat(fname, obj):
    """

    Parameters
    ----------
    fname : str
        Path to the image to write
    obj : any
        Image object to write

    Returns
    -------

    """
    pass


import javabridge
import bioformats

javabridge.start_vm(class_path=bioformats.JARS)

fname = shared_data("flower_confocal", 0)

reader = bioformats.get_image_reader(0, path=fname)
n_series = reader.rdr.getSeriesCount()

metadata_xml = bioformats.get_omexml_metadata(path=fname)
img_metadata = bioformats.OMEXML(metadata_xml)

s = 0
reader.rdr.setSeries(s)
z_sh = reader.rdr.getSizeZ()
t_sh = reader.rdr.getSizeT()
planes = [[reader.read(c=None, z=z, t=t, rescale=False) for z in range(z_sh)] for t in range(t_sh)]

import numpy as np

img = np.array(planes)

javabridge.kill_vm()
