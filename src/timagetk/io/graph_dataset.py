#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
from timagetk.graphs import TissueGraph


def example_tissue_graph(dataset, time_point=0, features='all', wall_features='all', **kwargs):
    """Create a TissueGraph from a shared dataset.

    Parameters
    ----------
    dataset : {'synthetic', 'flower_confocal'}
        A valid dataset name.
    time_point : int, optional
        Time point to select from the dataset, the first one by default (0).
    features : list, optional
        List of spatial cell features to extract from the segmented tissue.
    wall_features : list, optional
        List of spatial wall features to extract from the segmented tissue.

    Other Parameters
    ----------------
    coordinates : bool, optional
        If ``True`` add the topological element coordinates to the graph at construction time.
        See notes for more information.
    extract_dual : bool, optional
        If ``True`` construct the dual graph with cell edges and vertices.
        Else, construct only primal graph, that is the neighborhood graph.

    Returns
    -------
    TissueGraph
        The tissue graph built from the corresponding shared dataset.

    Examples
    --------
    >>> from timagetk.graphs.tissue_graph import example_tissue_graph
    >>> tg = example_tissue_graph('synthetic', extract_dual=False)
    >>> print(tg.list_cell_properties())
    ['barycenter', 'epidermis', 'epidermis_median', 'area', 'inertia_axis', 'number_of_neighbors', 'shape_anisotropy', 'volume', 'epidermis_area']
    >>> print(tg.list_cell_wall_properties())
    ['median', 'area']
    """
    from timagetk import TissueImage3D
    from timagetk.io.dataset import shared_data
    if dataset == "synthetic":
        img = shared_data(dataset, 'labelled')
    else:
        img = shared_data(dataset, time_point)
    img = TissueImage3D(img, background=1, not_a_label=0)
    kwargs['verbose'] = kwargs.get('verbose', False)
    tg = TissueGraph(img, features=features, wall_features=wall_features, **kwargs)
    return tg


def example_tissue_graph_from_csv(dataset='sphere', time_point=0, **kwargs):
    """Create a TissueGraph from a shared CSV dataset.

    Parameters
    ----------
    dataset : {'sphere', 'p58'}, optional
        A valid dataset name, "sphere" by default.
    time_point : int, optional
        Time point to select from the dataset, the first one by default (0).

    Returns
    -------
    TissueGraph
        The tissue graph built from the corresponding shared CSV dataset.

    Examples
    --------
    >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
    >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
    >>> print(tg.list_cell_properties())
    ['area', 'number_of_neighbors', 'volume', 'epidermis', 'epidermis_area', 'inertia_axis', 'shape_anisotropy', 'epidermis_median', 'barycenter']
    >>> print(tg.list_cell_wall_properties())
    ['area', 'median']
    >>> # Overview of the features by types, names and time-index:
    >>> {cf: len(tg.cell_property_dict(cf, only_defined=True)) for cf in tg.list_cell_properties()}
    {'area': 63,  'number_of_neighbors': 63,  'volume': 63,  'epidermis': 63,  'epidermis_area': 49,
     'inertia_axis': 63,  'shape_anisotropy': 63,  'epidermis_median': 49,  'barycenter': 63}
    >>> {wf: len(tg.cell_wall_property_dict(wf, only_defined=True)) for wf in tg.list_cell_wall_properties()}
    {'area': 299, 'median': 299}
    >>> {ef: len(tg.cell_edge_property_dict(ef, only_defined=True)) for ef in tg.list_cell_edge_properties()}
    {'linel_id': 537, 'median': 537}
    >>> {vf: len(tg.cell_vertex_property_dict(vf, only_defined=True)) for vf in tg.list_cell_vertex_properties()}
    {'coordinate': 303}

    """
    from timagetk.io.graph import tissue_graph_from_csv
    csv_files = shared_dataset(dataset, dtype='features', time_indexes=time_point)[0]
    tg = tissue_graph_from_csv(csv_files, background=1, **kwargs)
    return tg