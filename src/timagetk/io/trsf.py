#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Transformation Object Input/Output Module

This module provides functionality for saving and loading `Trsf` transformation objects.

Key Features
------------
- Save `Trsf` transformation objects to a specified file location.
- Load `Trsf` transformation objects from a file.

Usage Examples
--------------
>>> from timagetk.io.trsf import save_trsf, read_trsf
>>> from timagetk.components.trsf import Trsf
>>> # Example - Save a `Trsf` object to a file:
>>> trsf = Trsf()  # Create a transformation object
>>> save_trsf(trsf, "transform.trsf")
>>> # Example - Load a `Trsf` object from a file:
>>> loaded_trsf = read_trsf("transform.trsf")
>>> print(loaded_trsf)

"""

from timagetk.bin.logger import get_logger
from timagetk.components.trsf import Trsf

log = get_logger(__name__)


def save_trsf(trsf, filename):
    """Write a ``Trsf`` under given ``filename``.

    Parameters
    ----------
    trsf : Trsf
        Transformation object to save.
    filename : str
        Name of the file to write.

    """
    try:
        assert isinstance(trsf, Trsf)
    except AssertionError:
        raise TypeError("This is not a `Trsf` instance!")
    log.debug(f"Saving transformation file: {filename}")
    trsf.write(filename)
    return


def read_trsf(filename):
    """Read a transformation file.

    Parameters
    ----------
    filename : str
        Name of the file containing a ``Trsf`` object.

    Returns
    -------
    Trsf
        The loaded transformation object.

    """
    log.debug(f"Loading transformation file: {filename}")
    trsf = Trsf(filename)
    return trsf
