#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Input output module for TissueGraph & TemporalTissueGraph.

Based on export/import of CSV files.
"""
import sys
from pathlib import Path

import numpy as np
import pandas as pd
from tqdm import tqdm

import timagetk
from timagetk.bin.logger import get_logger
from timagetk.features.utils import flatten
from timagetk.graphs.temporal_tissue_graph import TemporalTissueGraph
from timagetk.graphs.tissue_graph import TissueGraph
from timagetk.util import clean_type
from timagetk.util import not_default_test

log = get_logger(__name__)
#: List of properties related to spatial coordinates and should be decomposed in 'X', 'Y' & 'Z' parts in DataFrame & CSV
COORD_TYPE = ['barycenter', 'median', 'coordinate']
#: List of CSV suffixes used when saving a dataframe.
CSV_EXPORT_SUFFIXES = ['cell', 'wall', 'cell_vertex', 'cell_edge']

CELL_INDEX = 'cell_id'
WALL_INDEX = [f'cell_id_{i + 1}' for i in range(2)]
EDGE_INDEX = [f'cell_vertex_id_{i + 1}' for i in range(2)]
VERTEX_INDEX = [f'cell_id_{i + 1}' for i in range(4)]


def get_df_index(index_type, is_temporal):
    """Return the index use for DataFrame.

    Parameters
    ----------
    index_type : {'cell', 'wall', 'vertex', 'edge'}
        The type of index to return.
    is_temporal : bool
        Indicate if the index are time-indexed.

    Returns
    -------
    str or list[str]
        The index to use for DataFrame.

    Examples
    --------
    >>> from timagetk.io.graph import get_df_index
    >>> get_df_index("cell", False)
    'cell_id'
    >>> get_df_index("cell", True)
    ['time-point', 'cell_id']
    >>> get_df_index("edge", True)
    ['time-point', 'cell_vertex_id_1', 'cell_vertex_id_2']
    """
    index = eval(f"{index_type.upper()}_INDEX")
    if is_temporal:
        index = ['time-point'] + [index] if isinstance(index, str) else ['time-point'] + index
    return index


def to_dataframe(graph, cell_ids=None, cell_features='all', wall_features='all', export_dual=False, **kwargs):
    """Export the tissue graph to a data frame.

    Parameters
    ----------
    graph : timagetk.graphs.TissueGraph or timagetk.graphs.TemporalTissueGraph
        The graph containing properties to export.
    cell_ids : list[int], optional
        If defined, used to filter the list of cell ids saved under cell related CSV.
    cell_features : str or list[str], optional
        By default, 'all' save all known cell properties to cell related CSV.
        Else should be a list of selected cell properties to export to cell related CSV.
    wall_features : str or list[str], optional
        By default, 'all' save all known cell-wall properties to cell-wall related CSV.
        Else should be a list of selected cell-wall properties to export to cell-wall related CSV.
    export_dual : bool, optional
        If ``True``, default is ``False``, also export cell-vertex and cell-edge related CSVs.

    Other Parameters
    ----------------
    decompose_xyz : list[str]
        List of features to decompose in 'X', 'Y' & 'Z' parts. ``COORD_TYPE`` by default.
    decompose_ijk : list[str]
        List of features to decompose in 'i', 'j' & 'k' parts. ``None`` by default.
        Usefull for vector (norms) or tensors.
    default : Any
        The default value to use for cell id without any associate value for the selected property.
        Defaults to `None`.

    Returns
    -------
    pandas.DataFrame
        Cell related data frame
    pandas.DataFrame
        Cell-wall related data frame
    pandas.DataFrame
        Cell-vertex related data frame
    pandas.DataFrame
        Cell-edge related data frame

    Examples
    --------
    >>> from timagetk import TissueImage3D
    >>> from timagetk.io.graph import to_dataframe
    >>> from timagetk.graphs.tissue_graph import TissueGraph
    >>> from timagetk.io.dataset import shared_data
    >>> tissue = TissueImage3D(shared_data('flower_labelled', 0), background=1, not_a_label=0)
    >>> tg = TissueGraph(tissue, features=["volume"], wall_features=["area"])
    >>> print(tg.list_cell_properties())
    >>> print(tg.list_cell_wall_properties())
    >>> print(tg.list_cell_edge_properties())
    >>> print(tg.list_cell_vertex_properties())
    >>> cell_df, wall_df, cell_vertex_df, cell_edge_df = to_dataframe(tg)

    >>> # Example #2 - Export a TemporalTissueGraph as a dataframe:
    >>> from timagetk.io.graph import to_dataframe
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph
    >>> ttg = example_temporal_tissue_graph(extract_dual=False)
    >>> print(ttg.list_cell_properties())
    >>> print(ttg.list_cell_wall_properties())
    >>> cell_df, wall_df, _, _  = to_dataframe(ttg)

    >>> from timagetk.io.graph import to_csv
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph
    >>> ttg = example_temporal_tissue_graph("p58")
    >>> print(ttg.list_cell_properties())
    >>> print(ttg.list_cell_wall_properties())
    >>> cell_df, wall_df, _, _  = to_dataframe(ttg)

    """
    try:
        assert isinstance(graph, (TissueGraph, TemporalTissueGraph))
    except AssertionError:
        raise TypeError("This method can only export TissueGraph or TemporalTissueGraph to DataFrame.")

    is_ttg = isinstance(graph, TemporalTissueGraph)
    # List all defined cell and cell-wall features:
    all_cell_features = graph.list_cell_properties()
    all_cell_wall_features = graph.list_cell_wall_properties()
    # Filter or define cell and cell-wall features to export:
    if cell_features == 'all':
        cell_features = all_cell_features
    else:
        missing = set(cell_features) - set(all_cell_features)
        if len(missing) != 0:
            log.warning(f"Missing the following cell features in the {clean_type(graph)}: {', '.join(missing)}")
        cell_features = list(set(cell_features) & set(all_cell_features))
    if wall_features == 'all':
        wall_features = all_cell_wall_features
    else:
        missing = set(wall_features) - set(all_cell_wall_features)
        if len(missing) != 0:
            log.warning(f"Missing the following cell-wall features in the {clean_type(graph)}: {', '.join(missing)}")
        wall_features = list(set(wall_features) & set(all_cell_wall_features))

    # Defines list of cell ids as we will use it to iterate over some properties!
    all_ids = graph.cell_ids()
    if cell_ids is None:
        cell_ids = all_ids
    else:
        cell_ids = set(all_ids) & set(cell_ids)

    decompose_xyz = kwargs.pop('decompose_xyz', COORD_TYPE)
    decompose_ijk = kwargs.pop('decompose_ijk', [])
    # - Gather cell features in a dictionary:
    cell_dict = {}
    # Starts with neighbors list:
    neighbors_dict = graph.cell_neighbors_dict(cell_ids)
    if is_ttg:
        # Update with dictionary of neighbors to the background if defined:
        for bkgd_id in graph.background:
            if bkgd_id is not None:
                neighbors_dict.update(graph.cell_neighbors_dict([bkgd_id]))
    else:
        # Update with dictionary of neighbors to the background if defined:
        bkgd_id = graph.background
        if bkgd_id is not None:
            neighbors_dict.update(graph.cell_neighbors_dict([bkgd_id]))
    cell_dict.update({'neighbors': neighbors_dict})

    # Add selected cell features:
    for feat in cell_features:
        default = kwargs.pop("default", None)
        if any(d_feat in feat for d_feat in decompose_xyz + decompose_ijk):
            default = [np.nan] * 3
            is_not_default = not_default_test(default[0])
        else:
            is_not_default = not_default_test(default)

        if feat in graph.list_identities():
            log.info(f"Exporting '{feat}' cell identity...")
            cpd = graph.cell_property_dict(feat, cell_ids, default=False)
            unit_suffix = ""
        else:
            log.info(f"Exporting '{feat}' cell feature...")
            cpd = graph.cell_property_dict(feat, cell_ids, default=default, only_defined=False)
            unit = graph.get_cell_property_unit(feat)
            if unit != "":
                unit_suffix = f" -- {unit}"
            else:
                unit_suffix = ""
        if any(xyz_f in feat for xyz_f in decompose_xyz):
            cell_dict.update({feat + "_x": {cid: pval[0] for cid, pval in cpd.items() if is_not_default(pval[0])}})
            cell_dict.update({feat + "_y": {cid: pval[1] for cid, pval in cpd.items() if is_not_default(pval[1])}})
            cell_dict.update({feat + "_z": {cid: pval[2] for cid, pval in cpd.items() if is_not_default(pval[2])}})
        elif any(ijk_f in feat for ijk_f in decompose_ijk):
            cell_dict.update({feat + "_i": {cid: pval[0] for cid, pval in cpd.items() if is_not_default(pval[0])}})
            cell_dict.update({feat + "_j": {cid: pval[1] for cid, pval in cpd.items() if is_not_default(pval[1])}})
            cell_dict.update({feat + "_k": {cid: pval[2] for cid, pval in cpd.items() if is_not_default(pval[2])}})
        else:
            cell_dict.update({feat + unit_suffix: _dict_array_to_list(cpd)})

    # Also save lineage with 'descendants' and elapsed time if a temporal tissue graph:
    if is_ttg:
        cell_dict.update({'descendants': {(t, cid): [d for td, d in graph.child(t, cid)] for t, cid in cell_ids}})
        cell_dict.update({'elapsed_time': {(t, cid): graph._elapsed_time[t] for t, cid in cell_ids}})

    # Also export the 'background identity':
    background = dict(zip(cell_ids, [False] * len(cell_ids)))  # start with all cell ids, they are NOT background
    if is_ttg:
        background.update({bkgd_id: True for bkgd_id in graph.background})
    else:
        background.update({graph.background: True})
    cell_dict.update({'background': background})

    # - Gather cell-wall features in a dictionary:
    wall_dict = {}
    # Defines cell-walls list as pairs of cell ids from neighbors dictionary:
    if is_ttg:
        # Warning: need to reformat with time-point if a temporal tissue graph:
        wall_pairs = [(t, (l, n)) for (t, l), nei in neighbors_dict.items() for n in nei]
    else:
        wall_pairs = [(l, n) for l, nei in neighbors_dict.items() for n in nei]
    # Add selected cell-wall features:
    for feat in wall_features:
        log.info(f"Exporting '{feat}' cell-wall property...")
        wpd = graph.cell_wall_property_dict(feat, wall_pairs, default=None)
        if is_ttg:
            # Warning: need to reformat keys if a temporal tissue graph:
            wpd = {(t, *wid): v for (t, wid), v in wpd.items()}
        if any(xyz_f in feat for xyz_f in decompose_xyz):
            wall_dict.update({feat + "_x": {wid: p[0] for wid, p in wpd.items() if p is not None}})
            wall_dict.update({feat + "_y": {wid: p[1] for wid, p in wpd.items() if p is not None}})
            wall_dict.update({feat + "_z": {wid: p[2] for wid, p in wpd.items() if p is not None}})
        else:
            wall_dict.update({feat: _dict_array_to_list(wpd)})

    # Creates cell & cell-wall data frames from dictionaries:
    cell_df = pd.DataFrame().from_dict(cell_dict)
    wall_df = pd.DataFrame().from_dict(wall_dict)
    cell_df.index.rename(get_df_index('cell', is_ttg), inplace=True)
    wall_df.index.rename(get_df_index('wall', is_ttg), inplace=True)

    cell_vertex_df, cell_edge_df = None, None
    if export_dual:
        ## Cell-vertex dictionaries:
        cell_vertex_dict = {}
        for feat in graph.list_cell_vertex_properties():
            log.info(f"Exporting '{feat}' cell-vertex property...")
            cvpd = graph.cell_vertex_property_dict(feat, default=None)
            if is_ttg:
                # Warning: need to reformat keys if a temporal tissue graph:
                cvpd = {(t, *vid): v for (t, vid), v in cvpd.items()}
            if any(xyz_f in feat for xyz_f in decompose_xyz):
                cell_vertex_dict.update({feat + "_x": {cvid: p[0] for cvid, p in cvpd.items() if p is not None}})
                cell_vertex_dict.update({feat + "_y": {cvid: p[1] for cvid, p in cvpd.items() if p is not None}})
                cell_vertex_dict.update({feat + "_z": {cvid: p[2] for cvid, p in cvpd.items() if p is not None}})
            else:
                cell_vertex_dict.update({feat: _dict_array_to_list(cvpd)})

        ## Cell-edge dictionaries:
        cell_edge_dict = {}
        for feat in graph.list_cell_edge_properties():
            log.info(f"Exporting '{feat}' cell-edge property...")
            cepd = graph.cell_edge_property_dict(feat, default=None)
            if is_ttg:
                # Warning: need to reformat keys if a temporal tissue graph:
                cepd = {(t, *eid): v for (t, eid), v in cepd.items()}
            if any(xyz_f in feat for xyz_f in decompose_xyz):
                cell_edge_dict.update({feat + "_x": {ceid: p[0] for ceid, p in cepd.items() if p is not None}})
                cell_edge_dict.update({feat + "_y": {ceid: p[1] for ceid, p in cepd.items() if p is not None}})
                cell_edge_dict.update({feat + "_z": {ceid: p[2] for ceid, p in cepd.items() if p is not None}})
            else:
                cell_edge_dict.update({feat: _dict_array_to_list(cepd)})

        # Creates cell-vertex & cell-edge data frames from dictionaries:
        cell_vertex_df = pd.DataFrame().from_dict(cell_vertex_dict)
        cell_edge_df = pd.DataFrame().from_dict(cell_edge_dict)
        cell_vertex_df.index.rename(get_df_index('vertex', is_ttg), inplace=True)
        cell_edge_df.index.rename(get_df_index('edge', is_ttg), inplace=True)

    return cell_df, wall_df, cell_vertex_df, cell_edge_df


def to_csv(graph, name, cell_ids=None, cell_features='all', wall_features='all', export_dual=False, **kwargs):
    """Export a (temporal) tissue graph as CSV files.

    Parameters
    ----------
    graph : timagetk.graphs.TissueGraph or timagetk.graphs.TemporalTissueGraph
        The (temporal) tissue graph to export.
    name : str or list[str]
        The filename & filepath to use to save the (temporal) tissue graph as multiple CSVs.
        If a string is given, remove the extentsion and use it as base to export CSVs.
        Else should be a len-2 (cell & cell-wall) or len-4 list of CSV file names.
    cell_ids : list[int], optional
        If defined, used to filter the list of cell ids saved under cell related CSV.
    cell_features : str or list[str], optional
        By default, ``'all'`` save all known cell features to cell related CSV.
        Else should be a list of selected cell features to export to cell related CSV.
    wall_features : str or list of str, optional
        By default, ``'all'`` save all known cell-wall features to cell-wall related CSV.
        Else should be a list of selected cell-wall features to export to cell-wall related CSV.
    export_dual : bool, optional
        If ``True``, also export the dual graph (cell-vertex and cell-edge) and associated features to CSVs.
        Defaults to ``False``.

    Other Parameters
    ----------------
    decompose_xyz : list
        List of features to decompose in 'x', 'y' & 'z' parts.
        Defaults to ``COORD_TYPE``.
    decompose_ijk : list
        List of features to decompose in 'i', 'j' & 'k' parts. Usefull to 'split' vector or tensors (as lists).
        Defaults to ``None``.
    default : Any
        The default value to use for cell id without any associate value for the selected property.
        Defaults to ``None``.

    Returns
    -------
    list of str
        Path to the cell, cell-wall(, cell-vertex, cell-edge) related CSV files.

    See Also
    --------
    timagetk.io.graph.to_dataframe
    pandas.DataFrame.to_csv

    Notes
    -----
    Except for the listed 'other parameters' above, that are used by ``to_dataframe``, it is possible to specify others
     that will be used by ``to_csv``

    Examples
    --------
    >>> # EXAMPLE #1 - Export p58-t0 TissueGraph (with cell volume and cell-wall area) as CSVs:
    >>> from timagetk.io.graph import to_csv
    >>> from timagetk.graphs.tissue_graph import example_tissue_graph
    >>> tg = example_tissue_graph("p58", features=["volume"], wall_features=["area"], extract_dual=False)
    >>> csv_files = to_csv(tg, '~/p58-t0_SEG_down_interp_2x', export_dual=False)
    >>> print(csv_files)
    [PosixPath('~/p58-t0_SEG_down_interp_2x_cell.csv'), PosixPath('~/p58-t0_SEG_down_interp_2x_wall.csv')]
    >>> # EXAMPLE #2 - Export sphere time-series (with all cell, cell-wall & temporal features) as TPG CSVs:
    >>> from timagetk.io.graph import to_csv
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph
    >>> ttg = example_temporal_tissue_graph("sphere", features='all', wall_features='all', temp_features='all', extract_dual=True)
    >>> csv_files = to_csv(ttg, '~/sphere', export_dual=True)
    >>> print(csv_files)
    [PosixPath('~/sphere_cell.csv'), PosixPath('~/sphere_wall.csv'), PosixPath('~/sphere_cell_vertex.csv'), PosixPath('~/sphere_cell_edge.csv')]
    >>> # EXAMPLE #3 - Export p58 time-series (with all cell, cell-wall & temporal features) as TPG CSVs:
    >>> from timagetk.io.graph import to_csv
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph
    >>> ttg = example_temporal_tissue_graph("p58", features='all', wall_features='all', temp_features='all', extract_dual=True)
    >>> csv_files = to_csv(ttg, '~/p58_down_interp_2x', export_dual=True)
    >>> print(csv_files)
    [PosixPath('~/p58_down_interp_2x_cell.csv'), PosixPath('~/p58_down_interp_2x_wall.csv'), PosixPath('~/p58_down_interp_2x_cell_vertex.csv'), PosixPath('~/p58_down_interp_2x_cell_edge.csv')]
    """
    try:
        assert isinstance(graph, (TissueGraph, TemporalTissueGraph))
    except AssertionError:
        raise TypeError("This method can only export TissueGraph or TemporalTissueGraph to CSV.")

    # - Define or check the CSV filename to write:
    if isinstance(name, (str, Path)):
        # Get a file basename (stem) without any extension:
        name = Path(name)
        path, basename, ext = name.parent, name.stem, name.suffix
        while ext != '':
            basename, ext = Path(basename).stem, Path(basename).suffix
        # Create the CSV file names using the provided base file path & file name:
        cell_csv, wall_csv, cell_vertex_csv, cell_edge_csv = [Path(path) / f'{basename}_{csv}.csv' for csv in
                                                              CSV_EXPORT_SUFFIXES]
    else:
        if len(name) == 2:
            cell_csv, wall_csv = name
            cell_vertex_csv, cell_edge_csv = None, None
            export_dual = False
        elif len(name) == 4:
            cell_csv, wall_csv, cell_vertex_csv, cell_edge_csv = name
        else:
            log.critical(f"Could not make sense of `name` parameter: {name}")
            sys.exit(f"Wrong `name` parameter for `timagetk.io.graph.to_csv` function.")

    # - Convert the (temporal) tissue graph to pandas DataFrames:
    cell_df, wall_df, cell_vertex_df, cell_edge_df = to_dataframe(graph, cell_ids=cell_ids, cell_features=cell_features,
                                                                  wall_features=wall_features, export_dual=export_dual,
                                                                  **kwargs)

    # - Write the pandas DataFrames as CSVs:
    cell_df.to_csv(cell_csv, **kwargs)
    wall_df.to_csv(wall_csv, **kwargs)
    csv_files = [cell_csv, wall_csv]
    if export_dual:
        cell_vertex_df.to_csv(cell_vertex_csv, **kwargs)
        cell_edge_df.to_csv(cell_edge_csv, **kwargs)
        csv_files += [cell_vertex_csv, cell_edge_csv]

    return csv_files


def from_csv(csv_files, background=None, **kwargs):
    """Attempt to recreate a TissueGraph or a TemporalTissueGraph from a list of CSV files.

    Parameters
    ----------
    csv_files : list of str or list[pathlib.Path] or dict
        List of CSV files to use to reconstruct the TissueGraph or TemporalTissueGraph.
        The first one should be the cell related CSV and the second the cell-walls related CSV.
        If a dictionary, should be a time-indexed list of CSV files corresponding to tissue graphs.
    background : int, optional
        The label to use to override background, ``None`` by default.

    Returns
    -------
    timagetk.graphs.TissueGraph or timagetk.graphs.TemporalTissueGraph
        The reconstructed (temporal) tissue graph from the CSV files.

    Notes
    -----
    Keyword argument are passed to `pandas.read_csv` method.

    Examples
    --------
    >>> from timagetk.io.graph import from_csv
    >>> csv_tg = from_csv(['~/p58-t0_SEG_down_interp_2x_cell.csv', '~/p58-t0_SEG_down_interp_2x_wall.csv'])
    >>> csv_tg.list_cell_properties()
    >>> csv_tg.list_cell_wall_properties()
    >>> csv_tg.cell_wall_property_dict('median')
    >>> csv_tg.cell_wall_property_dict('area')

    >>> from timagetk.io.dataset import shared_data
    >>> csv_tg = from_csv(shared_dataset("sphere", 'features'))

    """
    if isinstance(csv_files, list):
        df = pd.read_csv(csv_files[0], **kwargs)
        if 'time-point' in df.columns:
            graph = temporal_tissue_graph_from_csv(csv_files, background, **kwargs)
        else:
            graph = tissue_graph_from_csv(csv_files, background, **kwargs)
    else:
        graph = TemporalTissueGraph()
        time_points = kwargs.pop('time_points')
        for t, csv_list in csv_files.items():
            log.info(f"Loading t{t} TissuGraph from CSVs...")
            graph.add_tissue_graph(tissue_graph_from_csv(csv_list, background, **kwargs), time_points[t])

    return graph


def tissue_graph_from_csv(csv_files, background=None, **kwargs):
    """Attempt to recreate a TissueGraph from a list of CSV files.

    Parameters
    ----------
    csv_files : list of str
        List of CSV files to use to reconstruct the TissueGraph.
        The first one should be the cell related CSV and the second the cell-walls related CSV.
    background : int, optional
        The label to use to override background, ``None`` by default.

    Returns
    -------
    TissueGraph
        The reconstructed tissue graph from the CSV files.

    Notes
    -----
    Keyword argument are passed to `pandas.read_csv` method.

    Examples
    --------
    >>> from timagetk.io.graph import tissue_graph_from_csv
    >>> csv_tg = tissue_graph_from_csv(['~/p58-t0_SEG_down_interp_2x_cell.csv', '~/p58-t0_SEG_down_interp_2x_wall.csv'])
    >>> print(csv_tg.list_cell_properties())
    >>> bary = csv_tg.cell_property_dict('barycenter')
    >>> print(csv_tg.list_cell_wall_properties())
    >>> wall_medians = csv_tg.cell_wall_property_dict('median', only_defined=True)
    >>> cell_areas = csv_tg.cell_wall_property_dict('area')

    >>> from timagetk.io.graph import tissue_graph_from_csv
    >>> tg = tissue_graph_from_csv(['/data/FPS_paper/Old_Data/p194/features/p194-t5-v2_SegXP_cell.csv', '/data/FPS_paper/Old_Data/p194/features/p194-t5-v2_SegXP_wall.csv'])

    """
    tg = TissueGraph()
    for csv in csv_files:
        df = pd.read_csv(csv, **kwargs)
        n_cell_id = sum([c.startswith('cell_id') for c in df.columns])
        n_cell_vertex_id = sum([c.startswith('cell_vertex_id') for c in df.columns])
        if n_cell_id == 1:
            # cell related CSV!
            tg = _cell_tissue_graph_from_dataframe(tg, df)
        if n_cell_id == 2:
            # cell-wall related CSV!
            tg = _cell_wall_tissue_graph_from_dataframe(tg, df)
        if n_cell_id == 4:
            # cell-vertex related CSV!
            tg = _cell_vertex_tissue_graph_from_dataframe(tg, df)
        if n_cell_vertex_id == 2:
            # cell-edge related CSV!
            tg = _cell_edge_tissue_graph_from_dataframe(tg, df)

    if background is not None:
        if tg.background is not None:
            log.warning(f"Overriding current background value `{tg.background}` with `{background}`!")
        tg.background = background

    return tg


def _cell_tissue_graph_from_dataframe(tg, df, tp=None, **kwargs):
    """Hidden method to add cells to a tissue graph from a data frame.

    Parameters
    ----------
    tg : timagetk.graphs.TissueGraph
        The tissue graph receiving the cell features loaded from data frame
    df : pandas.DataFrame
        The data frame containing the data to load in the tissue graph.
    tp : int, optional
        To use when creating a temporal tissue graph, used with printing!

    Returns
    -------
    timagetk.graphs.TissueGraph
        The tissue graph containing the loaded cell data.

    Examples
    --------
    >>> import pandas as pd
    >>> from timagetk.graphs import TissueGraph
    >>> from timagetk.io.graph import tissue_graph_from_csv
    >>> from timagetk.io.graph import _cell_tissue_graph_from_dataframe
    >>> from timagetk.io.dataset import shared_data
    >>> df = pd.read_csv(shared_dataset("p58", "features")[0][0])
    >>> csv_tg = TissueGraph()
    >>> csv_tg = _cell_tissue_graph_from_dataframe(csv_tg, df)
    >>> print(csv_tg.background)
    1
    >>> print(csv_tg.list_cell_properties())
    ['volume', 'number_of_neighbors', 'stack_margin', 'layers', 'shape_anisotropy', 'inertia_axis', 'epidermis_area', 'area', 'principal_direction_norms', 'epidermis', 'barycenter', 'epidermis_median']
    >>> print(csv_tg.list_identities())
    ['stack_margin', 'epidermis']
    """
    cell_index = get_df_index('cell', False)
    try:
        assert cell_index in df.columns
    except AssertionError:
        raise IOError(f"The '{cell_index}' index column is missing from the provided cell related CSV!")
    try:
        assert 'neighbors' in df.columns
    except AssertionError:
        raise IOError("The 'neighbors' column is missing from the provided cell related CSV!")

    if "background" in df.columns:
        # Add background id to TissueGraph:
        bkgd_id = int(df[cell_index][df.index[df['background'] == True][0]])
        tg.background = bkgd_id
        # Add the background's neighbors:
        bkgd_nei = eval(df['neighbors'][df.index[df[cell_index] == bkgd_id][0]])
        bkgd_wall_pairs = [(bkgd_id, nei) for nei in bkgd_nei]
        tg.add_cell_walls(bkgd_wall_pairs)
    else:
        log.warning("No column with the background info could be found in the CSV!")

    # Add primal nodes from the list of cell ids:
    tg.add_cells(list(df[cell_index]))

    # Create the neighborhood dictionary:
    neigh_dict = dict(zip(df[cell_index], df['neighbors'].apply(lambda x: _try_eval(x, []))))
    # Re-creates the pairs of walls from the neighborhood dictionary:
    wall_pairs = [(l, n) for l, nei in neigh_dict.items() for n in nei]
    # Add primal edges from the list of wall pairs:
    tg.add_cell_walls(wall_pairs)

    # Parse the DataFrame column names to get the cell feature names and units (if any):
    df_col_remap = {}  # DataFrame column remapping dictionary
    units = {}
    for col in df.columns:
        try:
            feat, unit = col.split(' -- ')
        except ValueError:
            pass
        else:
            units |= {feat: unit}
            df_col_remap |= {col: feat}
    # Rename the DataFrame columns:
    if df_col_remap != {}:
        df.rename(columns=df_col_remap, inplace=True)

    # Get the list of cell features:
    exclude = set(get_df_index('cell', True)) | {'background', 'neighbors', 'elapsed_time', 'descendants'}
    cell_features_list = list(set(df.columns) - exclude)
    # Search for "xyz decomposed" features:
    to_combine = _search_decomposed_features(cell_features_list, ['x', 'y', 'z'])
    # Search for "ijk decomposed" features:
    to_combine.update(_search_decomposed_features(cell_features_list, ['i', 'j', 'k']))
    cell_features_list = list(set(cell_features_list) - set(flatten(to_combine.values())))

    # Add cell features, if any:
    if len(cell_features_list) != 0:
        for feat in cell_features_list:
            # Re-create cell feature dict with cell id as keys:
            f_dict = dict(zip(df[cell_index], df[feat]))
            # Filter-out np.nan values:
            if any(dt in df[feat].dtype.name for dt in ('int', 'float')):
                f_dict = {k: v for k, v in f_dict.items() if v is not None and not np.isnan(v)}
            else:
                new_f_dict = {}
                for k, v in f_dict.items():
                    if isinstance(v, str):
                        new_f_dict[k] = _try_eval(v)
                    elif v is not None and not np.isnan(v):
                        new_f_dict[k] = v
                f_dict = new_f_dict
            if len(f_dict) != 0:
                if isinstance(list(f_dict.values())[0], bool):
                    cid_idty = [k for k, v in f_dict.items() if v]
                    log.debug(
                        f"Adding cell identity '{feat}' for {len(cid_idty)} cells{f' at time {tp}' if tp is not None else ''}!")
                    tg.add_cell_identity(feat, cid_idty)
                else:
                    log.debug(
                        f"Adding cell property '{feat}' with {len(f_dict)} values{f' at time {tp}' if tp is not None else ''}!")
                    tg.add_cell_property(feat, f_dict, unit=units[feat] if feat in units else '')
            else:
                if kwargs.get('verbose', True):
                    log.warning(
                        f"Loading cell property '{feat}' returned NO value{f' at time {tp}' if tp is not None else ''}!")
    # Add cell features to combine, if any:
    if len(to_combine) != 0:
        for feat, columns in to_combine.items():
            x, y, z = columns
            combined_df = "[" + df[x].astype(str) + ", " + df[y].astype(str) + ", " + df[z].astype(str) + "]"
            # convert str list into list of floats:
            combined_df = combined_df.apply(lambda x: _try_eval(x))
            # Re-create cell feature dict with cell id as keys:
            f_dict = dict(zip(df[cell_index], combined_df))
            # Filter-out np.nan values:
            f_dict = {k: v for k, v in f_dict.items() if v is not None and not np.isnan(v[0])}
            if len(f_dict) != 0:
                log.debug(
                    f"Adding combined cell property '{feat}' with {len(f_dict)} values{f' at time {tp}' if tp is not None else ''}!")
                tg.add_cell_property(feat, f_dict)
            else:
                if kwargs.get('verbose', True):
                    log.warning(
                        f"Loading combined cell property '{feat}' returned NO value{f' at time {tp}' if tp is not None else ''}!")

    return tg


def _cell_wall_tissue_graph_from_dataframe(tg, df, tp=None, **kwargs):
    """Hidden method to add cell-walls to a tissue graph from a data frame.

    Parameters
    ----------
    tg : timagetk.graphs.TissueGraph
        The tissue graph receiving the cell-wall features loaded from data frame
    df : pandas.DataFrame
        The data frame containing the data to load in the tissue graph.
    tp : int, optional
        To use when creating a temporal tissue graph, used with printing!

    Returns
    -------
    timagetk.graphs.TissueGraph
        The tissue graph containing the loaded cell-wall data.

    Examples
    --------
    >>> import pandas as pd
    >>> from timagetk.graphs import TissueGraph
    >>> from timagetk.io.graph import _cell_tissue_graph_from_dataframe
    >>> from timagetk.io.graph import _cell_wall_tissue_graph_from_dataframe
    >>> from timagetk.io.dataset import shared_data
    >>> csv_tg = TissueGraph()
    >>> # Load cell dataframe to create primal graph nodes:
    >>> c_df = pd.read_csv(shared_dataset("p58", "features")[0][0])
    >>> csv_tg = _cell_tissue_graph_from_dataframe(csv_tg, c_df)
    >>> # Load cell-wall dataframe to create primal graph edges:
    >>> cw_df = pd.read_csv(shared_dataset("p58", "features")[0][1])
    >>> csv_tg = _cell_wall_tissue_graph_from_dataframe(csv_tg, cw_df)
    >>> print(csv_tg.list_cell_wall_properties())
    ['area', 'median']
    """
    cell_wall_index = get_df_index('wall', False)
    for index in cell_wall_index:
        try:
            assert index in df.columns
        except AssertionError:
            raise IOError(f"The '{index}' index column is missing from the provided cell-wall related CSV!")

    cell_wall_ids = list(map(tuple, df[cell_wall_index].values))

    # Get the list of cell-wall features:
    wall_features_list = list(set(df.columns) - set(cell_wall_index))
    # Search for "xyz decomposed" features:
    to_combine = _search_decomposed_features(wall_features_list, ['x', 'y', 'z'])
    # Search for "ijk decomposed" features:
    to_combine.update(_search_decomposed_features(wall_features_list, ['i', 'j', 'k']))
    wall_features_list = list(set(wall_features_list) - set(flatten(to_combine.values())))

    # Add cell-wall features, if any:
    if len(wall_features_list) != 0:
        for feat in wall_features_list:
            # Re-create cell-wall feature dict with cell id pairs as keys:
            f_dict = dict(zip(cell_wall_ids, df[feat]))
            # Filter-out np.nan values:
            if any(dt in df[feat].dtype.name for dt in ('int', 'float')):
                f_dict = {k: v for k, v in f_dict.items() if v is not None and not np.isnan(v)}
            else:
                new_f_dict = {}
                for k, v in f_dict.items():
                    if isinstance(v, str):
                        new_f_dict[k] = _try_eval(v)
                    elif v is not None and not np.isnan(v):
                        new_f_dict[k] = v
                f_dict = new_f_dict
            if len(f_dict) != 0:
                log.debug(
                    f"Adding cell-wall property '{feat}' with {len(f_dict)} values{f' at time {tp}' if tp is not None else ''}!")
                tg.add_cell_wall_property(feat, f_dict)
            else:
                if kwargs.get('verbose', True):
                    log.warning(
                        f"Loading cell-wall property '{feat}' returned NO value{f' at time {tp}' if tp is not None else ''}!")
    # Add cell-wall features to combine, if any:
    if len(to_combine) != 0:
        for feat, columns in to_combine.items():
            x, y, z = columns
            combined_df = "[" + df[x].astype(str) + ", " + df[y].astype(str) + ", " + df[z].astype(str) + "]"
            # convert str list into list of floats:
            combined_df = combined_df.apply(lambda x: _try_eval(x))
            # Re-create cell-wall feature dict with cell id pairs as keys:
            f_dict = dict(zip(cell_wall_ids, combined_df))
            # Filter-out np.nan values:
            f_dict = {k: v for k, v in f_dict.items() if v is not None and not np.isnan(v[0])}
            if len(f_dict) != 0:
                log.debug(
                    f"Adding combined cell-wall property '{feat}' with {len(f_dict)} values{f' at time {tp}' if tp is not None else ''}!")
                tg.add_cell_wall_property(feat, f_dict)
            else:
                if kwargs.get('verbose', True):
                    log.warning(
                        f"Loading combined cell-wall property '{feat}' returned NO value{f' at time {tp}' if tp is not None else ''}!")

    return tg


def _cell_vertex_tissue_graph_from_dataframe(tg, df, tp=None):
    """Hidden method to add cell-vertex to a tissue graph from a data frame.

    Parameters
    ----------
    tg : timagetk.graphs.TissueGraph
        The tissue graph receiving the cell-vertex features loaded from data frame
    df : pandas.DataFrame
        The data frame containing the data to load in the tissue graph.
    tp : int, optional
        To use when creating a temporal tissue graph, used with printing!

    Returns
    -------
    timagetk.graphs.TissueGraph
        The tissue graph containing the loaded cell-vertex data.

    Examples
    --------
    >>> import pandas as pd
    >>> from timagetk.graphs import TissueGraph
    >>> from timagetk.io.graph import _cell_vertex_tissue_graph_from_dataframe
    >>> from timagetk.io.dataset import shared_data
    >>> csv_tg = TissueGraph()
    >>> cv_df = pd.read_csv(shared_dataset("p58", "features")[0][2])
    >>> csv_tg = _cell_vertex_tissue_graph_from_dataframe(csv_tg, cv_df)
    >>> print(csv_tg.list_cell_vertex_properties())
    ['coordinate']
    >>> print(len(csv_tg.cell_vertex_ids()))
    5793
    """
    cell_vertex_index = get_df_index('vertex', False)
    for index in cell_vertex_index:
        try:
            assert index in df.columns
        except AssertionError:
            raise IOError(f"The '{index}' index column is missing from the provided cell-vertex related CSV!")

    # Add the cell-vertex ids to the dual graph:
    cell_vertex_ids = list(map(tuple, df[cell_vertex_index].values))
    tg.add_cell_vertices(cell_vertex_ids)

    # Get the list of cell-vertex features:
    exclude = set(cell_vertex_index) - {'neighbors'}
    vertex_features_list = list(set(df.columns) - exclude)
    # Search for "xyz decomposed" features:
    to_combine = _search_decomposed_features(vertex_features_list, ['x', 'y', 'z'])
    # Search for "ijk decomposed" features:
    to_combine.update(_search_decomposed_features(vertex_features_list, ['i', 'j', 'k']))
    vertex_features_list = list(set(vertex_features_list) - set(flatten(to_combine.values())))

    # Add cell-vertex features, if any:
    if len(vertex_features_list) != 0:
        for feat in vertex_features_list:
            # Re-create cell-vertex feature dict with cell id pairs as keys:
            f_dict = dict(zip(cell_vertex_ids, df[feat]))
            # Filter-out np.nan values:
            if any(dt in df[feat].dtype.name for dt in ('int', 'float')):
                f_dict = {k: v for k, v in f_dict.items() if v is not None and not np.isnan(v)}
            else:
                new_f_dict = {}
                for k, v in f_dict.items():
                    if isinstance(v, str):
                        new_f_dict[k] = _try_eval(v)
                    elif v is not None and not np.isnan(v):
                        new_f_dict[k] = v
                f_dict = new_f_dict
            if len(f_dict) != 0:
                log.debug(
                    f"Adding cell-vertex property '{feat}' with {len(f_dict)} values{f' at time {tp}' if tp is not None else ''}!")
                tg.add_cell_vertex_property(feat, f_dict)
            else:
                log.warning(
                    f"Loading cell-vertex property '{feat}' returned NO value{f' at time {tp}' if tp is not None else ''}!")
    # Add cell-vertex features to combine, if any:
    if len(to_combine) != 0:
        for feat, columns in to_combine.items():
            x, y, z = columns
            combined_df = "[" + df[x].astype(str) + ", " + df[y].astype(str) + ", " + df[z].astype(str) + "]"
            # convert str list into list of floats:
            combined_df = combined_df.apply(lambda x: _try_eval(x))
            # Re-create cell-vertex feature dict with cell id pairs as keys:
            f_dict = dict(zip(cell_vertex_ids, combined_df))
            # Filter-out np.nan values:
            f_dict = {k: v for k, v in f_dict.items() if v is not None and not np.isnan(v[0])}
            if len(f_dict) != 0:
                log.debug(
                    f"Adding combined cell-vertex property '{feat}' with {len(f_dict)} values{f' at time {tp}' if tp is not None else ''}!")
                tg.add_cell_vertex_property(feat, f_dict)
            else:
                log.warning(
                    f"Loading combined cell-vertex property '{feat}' returned NO value{f' at time {tp}' if tp is not None else ''}!")

    return tg


def _cell_edge_tissue_graph_from_dataframe(tg, df, tp=None):
    """Hidden method to add cell-edge to a tissue graph from a data frame.

    Parameters
    ----------
    tg : timagetk.graphs.TissueGraph
        The tissue graph receiving the cell-edge features loaded from data frame
    df : pandas.DataFrame
        The data frame containing the data to load in the tissue graph.
    tp : int, optional
        To use when creating a temporal tissue graph, used with printing!

    Returns
    -------
    timagetk.graphs.TissueGraph
        The tissue graph containing the loaded cell-edge data.

    Examples
    --------
    >>> import pandas as pd
    >>> from timagetk.graphs import TissueGraph
    >>> from timagetk.io.graph import _cell_edge_tissue_graph_from_dataframe
    >>> from timagetk.io.dataset import shared_data
    >>> csv_tg = TissueGraph()
    >>> ce_df = pd.read_csv(shared_dataset("p58", "features")[0][3])
    >>> csv_tg = _cell_edge_tissue_graph_from_dataframe(csv_tg, ce_df)
    >>> print(csv_tg.list_cell_edge_properties())
    ['linel_id', 'median']
    >>> print(len(csv_tg.cell_vertex_ids()))  # cell-vertices have been added by cell-edges
    5476
    """
    cell_edge_index = get_df_index('edge', False)
    for index in cell_edge_index:
        try:
            assert index in df.columns
        except AssertionError:
            raise IOError(f"The '{index}' index column is missing from the provided cell-edge related CSV!")
        else:
            df[index] = df[index].apply(lambda x: eval(x))

    cell_edge_ids = list(map(tuple, df[cell_edge_index].values))

    # Add dual graph edges from the list of cell_vertex_id pairs:
    tg.add_cell_edges(cell_edge_ids)

    # Get the list of cell-edge features:
    edge_features_list = list(set(df.columns) - set(cell_edge_index))
    # Search for "xyz decomposed" features:
    to_combine = _search_decomposed_features(edge_features_list, ['x', 'y', 'z'])
    # Search for "ijk decomposed" features:
    to_combine.update(_search_decomposed_features(edge_features_list, ['i', 'j', 'k']))
    edge_features_list = list(set(edge_features_list) - set(flatten(to_combine.values())))

    # Add cell-edge features, if any:
    if len(edge_features_list) != 0:
        for feat in edge_features_list:
            # Re-create cell-edge feature dict with cell id pairs as keys:
            f_dict = dict(zip(cell_edge_ids, df[feat]))
            # Filter-out np.nan values:
            if any(dt in df[feat].dtype.name for dt in ('int', 'float')):
                f_dict = {k: v for k, v in f_dict.items() if v is not None and not np.isnan(v)}
            else:
                new_f_dict = {}
                for k, v in f_dict.items():
                    if isinstance(v, str):
                        new_f_dict[k] = _try_eval(v)
                    elif v is not None and not np.isnan(v):
                        new_f_dict[k] = v
                f_dict = new_f_dict
            if len(f_dict) != 0:
                log.debug(
                    f"Adding cell-edge property '{feat}' with {len(f_dict)} values{f' at time {tp}' if tp is not None else ''}!")
                tg.add_cell_edge_property(feat, f_dict)
            else:
                log.warning(
                    f"Loading cell-edge property '{feat}' returned NO value{f' at time {tp}' if tp is not None else ''}!")
    # Add cell-edge features to combine, if any:
    if len(to_combine) != 0:
        for feat, columns in to_combine.items():
            x, y, z = columns
            combined_df = "[" + df[x].astype(str) + ", " + df[y].astype(str) + ", " + df[z].astype(str) + "]"
            # convert str list into list of floats:
            combined_df = combined_df.apply(lambda x: _try_eval(x))
            # Re-create cell-edge feature dict with cell id pairs as keys:
            f_dict = dict(zip(cell_edge_ids, combined_df))
            # Filter-out np.nan values:
            f_dict = {k: v for k, v in f_dict.items() if v is not None and not np.isnan(v[0])}
            if len(f_dict) != 0:
                log.debug(
                    f"Adding combined cell-edge property '{feat}' with {len(f_dict)} values{f' at time {tp}' if tp is not None else ''}!")
                tg.add_cell_edge_property(feat, f_dict)
            else:
                log.warning(
                    f"Loading combined cell-edge property '{feat}' returned NO value{f' at time {tp}' if tp is not None else ''}!")

    return tg


def _search_decomposed_features(features_list, suffixes=['x', 'y', 'z']):
    """Hidden method to search for "decomposed" features, *i.e.* those split with suffixes.

    Parameters
    ----------
    features_list : list of str
        List of features.
    suffixes : list, optional
        The list of used suffixes to decompose features.
        Defaults to ``['x', 'y', 'z']``.

    Returns
    -------
    dict
        Dictionary of features to combine, with new name as key and list of features to combine as values.

    Examples
    --------
    >>> from timagetk.io.graph import _search_decomposed_features
    >>> _search_decomposed_features(['bary_x', 'bary_y', 'bary_z', 'area'])
    {'bary': ['bary_x', 'bary_y', 'bary_z']}
    >>> _search_decomposed_features(['bary_z', 'area', 'bary_x', 'bary_y'])  # returned order is suffixes order!
    {'bary': ['bary_x', 'bary_y', 'bary_z']}
    >>> _search_decomposed_features(['norm_i', 'norm_j', 'norm_k', 'volume'], ['i', 'j', 'k'])
    {'norm': ['norm_i', 'norm_j', 'norm_k']}

    """
    to_combine = {}
    for suffix in suffixes:
        for feat in features_list:
            if feat.endswith(f"_{suffix}"):
                tail = len(suffix) + 1
                feat_root = feat[:-tail]
                if feat_root not in to_combine:
                    to_combine[feat_root] = []
                to_combine[feat_root].append(feat)
    return to_combine


def temporal_tissue_graph_from_csv(csv_files, background=None, **kwargs):
    """Attempt to recreate a TemporalTissueGraph from a list of CSV files.

    Parameters
    ----------
    csv_files : list of str
        List of CSV files to use to reconstruct the TissueGraph.
        The first one should be the cell related CSV and the second the cell-walls related CSV.
    background : int, optional
        The label used as background, ``None`` by default.

    Returns
    -------
    timagetk.graphs.TemporalTissueGraph
        The reconstructed temporal tissue graph from the CSV files.

    Notes
    -----
    Keyword argument are passed to `pandas.read_csv` method.

    Examples
    --------
    >>> from timagetk.io.graph import temporal_tissue_graph_from_csv
    >>> from timagetk.io.dataset import shared_data
    >>> csv_files = shared_dataset("sphere", dtype='temporal features')
    >>> print([f.name for f in csv_files])
    ['sphere_cell.csv', 'sphere_wall.csv', 'sphere_cell_vertex.csv', 'sphere_cell_edge.csv']
    >>> csv_ttg = temporal_tissue_graph_from_csv(csv_files)
    >>> print(csv_ttg.list_cell_properties())
    ['affine_deformation_matrix', 'affine_deformation_r2', 'area', 'barycenter', 'division_rate', 'epidermis', 'epidermis_affine_deformation_matrix', 'epidermis_affine_deformation_r2', 'epidermis_area', 'epidermis_areal_strain_rates', 'epidermis_growth_rates', 'epidermis_growth_tensor_after', 'epidermis_growth_tensor_before', 'epidermis_landmarks', 'epidermis_median', 'epidermis_strain_anisotropy', 'fused_barycenter', 'fused_epidermis_median', 'growth_rates', 'growth_tensor_after', 'growth_tensor_before', 'inertia_axis', 'landmarks', 'number_of_neighbors', 'shape_anisotropy', 'time-index', 'time-point', 'volume', 'volumetric_strain_rates']
    >>> cell_volumes = csv_ttg.cell_property_dict('volume')
    >>> print(len(cell_volumes))
    279
    >>> cell_wall_areas = csv_ttg.cell_wall_property_dict('area')
    >>> print(len(cell_wall_areas))
    1638
    """
    verbose = kwargs.pop("verbose", False)
    ttg = TemporalTissueGraph(verbose=verbose)
    for csv in csv_files:
        log.info(f"Loading {csv}...")
        df = pd.read_csv(csv, **kwargs)
        try:
            assert 'time-point' in df.columns
        except AssertionError:
            raise IOError(f"The 'time-point' column is missing from the provided cell related CSV: {csv}!")
        time_point = sorted(df['time-point'].unique())
        n_cell_id = sum([c.startswith('cell_id') for c in df.columns])
        n_cell_vertex_id = sum([c.startswith('cell_vertex_id') for c in df.columns])

        # Re-construct the TissueGraph per time-point:
        for t in tqdm(time_point, unit="time-point"):
            tp_df = df.loc[df['time-point'] == t].copy()
            if n_cell_id == 1:
                elapsed_time = sorted(df['elapsed_time'].unique())
                if verbose:
                    log.info(f"Creating primal tissue graph and loading cell property at time {t}...")
                # cell related CSV!
                tg = _cell_tissue_graph_from_dataframe(TissueGraph(verbose=verbose), tp_df, t, verbose=verbose)
                if background is not None:
                    if tg.background is not None:
                        log.warning(f"Overriding current background value `{tg.background}` with `{background}`!")
                    tg.background = background
                ttg.add_tissue_graph(tg, elapsed_time[t])
            if n_cell_id == 2:
                if verbose:
                    log.info(f"Loading cell-wall property to tissue graph at time {t}...")
                # cell-wall related CSV!
                _ = _cell_wall_tissue_graph_from_dataframe(ttg._tissue_graph[t], tp_df, t, verbose=verbose)
            if n_cell_id == 4:
                if verbose:
                    log.info(f"Creating dual tissue graph and loading cell-vertex property at time {t}...")
                # cell-vertex related CSV!
                _ = _cell_vertex_tissue_graph_from_dataframe(ttg._tissue_graph[t], tp_df, t)
            if n_cell_vertex_id == 2:
                if verbose:
                    log.info(f"Loading cell-edge property to tissue graph at time {t}...")
                # cell-edge related CSV!
                _ = _cell_edge_tissue_graph_from_dataframe(ttg._tissue_graph[t], tp_df, t)

        # Add cell-lineage between time-points:
        if n_cell_id == 1:  # descendant info is in cell related CSV!
            for t in time_point:
                tp_df = df.loc[df['time-point'] == t]
                if t != time_point[-1]:
                    lin = dict(zip(tp_df['cell_id'], tp_df['descendants'].apply(lambda x: _try_eval(x))))
                    ttg.add_lineage(t, lin)

    return ttg


def _dict_array_to_list(d_arr):
    """Convert dictionary values to list if a numpy array.

    Parameters
    ----------
    d_arr : dict
        Dictionary to transform

    Returns
    -------
    dict
        The converted dictionary

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.io.graph import _dict_array_to_list
    >>> _dict_array_to_list({1: np.array([2, 3]), 2: 4, 3: None})
    {1: [2, 3], 2: 4, 3: None}

    """
    for k, v in d_arr.items():
        try:
            d_arr[k] = v.tolist()
        except:
            pass
    return d_arr


def _try_eval(expression, default=None):
    """Try to evaluate an expression or return a default value.

    Parameters
    ----------
    expression : str
        The expression to evaluate.
    default : any, optional
        The default value to return if the evaluation of the expression fails.
        Defaults to ``None``.

    Examples
    --------
    >>> from timagetk.io.graph import _try_eval
    >>> _try_eval('True')
    True
    >>> _try_eval('1.25')
    1.25
    >>> _try_eval('np.nan')
    nan
    >>> _try_eval('[1, 2, 3]')
    [1, 2, 3]
    >>> _try_eval('toto',default=0)
    0

    """
    try:
        expression = eval(expression)
    except:
        return default
    else:
        return expression


def export_identity(graph, identity, filepath):
    """Export an identity to a TXT file.

    Parameters
    ----------
    graph : timagetk.graphs.TissueGraph or timagetk.graphs.TemporalTissueGraph
        The (temporal) tissue graph containing the cell identity to export.
    identity : str
        The name of the identity to export.
    filepath : str or pathlib.Path
        The file path where to save the identity file.
        The file name (without extension) is the identity name!

    Examples
    --------
    >>> from timagetk.io.graph import export_identity
    >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
    >>> from timagetk.features.graph import cell_by_layers
    >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
    >>> graph = example_tissue_graph_from_csv("sphere")
    >>> graph.list_identities()
    ['epidermis']
    >>> export_identity(graph, 'epidermis', '/tmp')

    """
    try:
        assert identity in graph.list_identities()
    except AssertionError:
        log.critical(f"Could not find requested identity `{identity}` in the provided `graph`!")

    if isinstance(graph, TissueGraph):
        id_cell_list = graph.list_cells_with_identity(identity)
    elif isinstance(graph, TemporalTissueGraph):
        id_cell_list = graph.list_cells_with_identity(identity, rank=0, tp=None)
    else:
        raise TypeError(f"Expecting a TissueGraph or TemporalTissueGraph instance, got `{clean_type(graph)}`!")

    from pathlib import Path
    directory = Path(filepath)
    if not directory.exists():
        directory.mkdir()

    fpath = Path(filepath) / f"{identity}.txt"
    with open(fpath, "w") as f:
        f.writelines("\n".join(list(map(str, id_cell_list))))

    log.info(f"Saved identity '{identity}' under `{fpath}`!")
    return


def export_all_identities(graph, filepath):
    """Export all cell identities contained in the given `graph`.

    Parameters
    ----------
    graph : timagetk.graphs.TissueGraph or timagetk.graphs.TemporalTissueGraph
        The (temporal) tissue graph containing the cell identities to export.
    filepath : str or pathlib.Path
        The file path where to save the identity files.
        The file name (without extension) is the identity name!

    """
    cell_identities_list = graph.list_identities()
    for identity in cell_identities_list:
        export_identity(graph, identity, filepath)
    return


def import_identity(graph, filepath, identity=None, strict=False, **kwargs):
    """Import an identity TXT file and add it to the graph.

    Parameters
    ----------
    graph : timagetk.graphs.TissueGraph or timagetk.graphs.TemporalTissueGraph
        The (temporal) tissue graph to add the cell identity to.
    filepath : str or pathlib.Path
        The directory path where to get the identity file, requires to set `identity`.
        The file path to the identity file, its name (without extension) is the identity name!
    identity : str, optional
        The name of the identity to import.
        Use this to override the use of the file name as identity name.
    strict : bool, optional
        If ``True`` (default is ``False``), make sure to exclude all other cell-ids from this identity.
        By default, only update the list of given cell as belonging to this identity.

    Other Parameters
    ----------------
    by_time_point : bool
        Control the behaviour of `strict`.
        If ``True`` (default is ``False``), work by time-point (exclude other cell from same time-point).
        By default, exclude all other cell (from all time-points).
    time_index : list
        List of time-indexes to filter the loaded identity file.

    Returns
    -------
    timagetk.graphs.TissueGraph or timagetk.graphs.TemporalTissueGraph
        The (temporal) tissue graph with the cell identity.

    Examples
    --------
    >>> from timagetk.io.graph import export_identity
    >>> from timagetk.io.graph import import_identity
    >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
    >>> from timagetk.features.graph import cell_by_layers
    >>> graph = example_tissue_graph_from_csv("sphere", time_point=0)
    >>> graph.list_identities()
    ['epidermis']
    >>> cids_by_layers = cell_by_layers(graph, [1, 2])
    >>> print(f"Found {len(cids_by_layers[2])} cell ids layer 2!")
    Found 13 cell ids layer 2!
    >>> graph.add_cell_identity('Layer2', cids_by_layers[2])
    >>> graph.list_identities()
    ['epidermis', 'Layer2']
    >>> export_identity(graph, 'Layer2', '/tmp')
    >>> graph.remove_cell_identity('Layer2')
    >>> graph.list_identities()
    ['epidermis']
    >>> graph.remove_cell_identity('Layer2')
    >>> graph = import_identity(graph,'/tmp/Layer2.txt')
    >>> graph.remove_cell_identity('Layer2')
    >>> # OR:
    >>> graph = import_identity(graph,'/tmp','Layer2')
    >>> graph.list_identities()
    ['epidermis', 'Layer2']
    >>> print(f"Found {len(graph.list_cells_with_identity('Layer2'))} cell ids with identity 'Layer2'!")
    Found 13 cell ids with identity 'Layer2'!

    """
    try:
        assert identity not in graph.list_identities()
    except AssertionError:
        log.warning(f"Requested identity `{identity}` is already in the provided `graph`!")

    # Check the speci
    filepath = Path(filepath)
    if not filepath.suffix == '.txt':
        try:
            assert identity is not None
        except AssertionError:
            raise ValueError("You should provide an `identity` when providing a path to a directory!")
        filepath = filepath.joinpath(f"{identity}.txt")
    else:
        try:
            assert filepath.exists()
        except AssertionError:
            IOError(f"The given identity file path does not exists ({filepath})!")
        if identity is None:
            identity = filepath.stem

    log.info(f"Loading identity '{identity}' from `{filepath}`!")
    with open(filepath, "r") as f:
        lines = f.readlines()

    lines = [l.replace('\n', '') for l in lines]

    if "," in lines[0]:
        is_tg = False
        # Case where the list of cell-ids are indexed by time-point and should be added to a TemporalTissueGraph
        lines = [l.replace('(', '') for l in lines]
        lines = [l.replace(')', '') for l in lines]
        cids = [tuple(map(int, l.split(','))) for l in lines]
    else:
        is_tg = True
        # Case where the list of cell-ids are NOT indexed by time-point and should be added to a TissueGraph
        cids = list(map(int, lines))

    if isinstance(graph, TissueGraph):
        try:
            assert is_tg
        except AssertionError:
            raise TypeError("You are trying to add time indexed cell ids to a TissueGraph instance!")
        else:
            graph.add_cell_identity(identity, cids, strict=strict)
    elif isinstance(graph, TemporalTissueGraph):
        try:
            assert not is_tg
        except AssertionError:
            raise TypeError(
                "You are trying to add cell ids that are NOT time indexed to a TemporalTissueGraph instance!")
        else:
            time_index = kwargs.pop('time_index', range(graph.nb_time_points))
            # We consider here a temporal resampling case where time-index change in the graph but the identity files have all time-indexed cells:
            old2new_tidx = {t_idx: i for i, t_idx in enumerate(time_index)}
            if time_index is not None:
                cids = [(old2new_tidx[t_idx], cid) for (t_idx, cid) in cids if t_idx in time_index]
            graph.add_cell_identity(identity, cids, strict=strict, **kwargs)
    else:
        raise TypeError(f"Expecting a TissueGraph or TemporalTissueGraph instance, got `{clean_type(graph)}`!")

    return graph


def import_all_identities(graph, filepath, **kwargs):
    """Import all identity files and add them to the graph.

    Parameters
    ----------
    graph : timagetk.graphs.TissueGraph or timagetk.graphs.TemporalTissueGraph
        The (temporal) tissue graph to add the cell identities to.
    filepath : str or pathlib.Path
        The file path where to get the identity files.
        The file name (without extension) is the identity name!

    Other Parameters
    ----------------
    strict : bool, optional
        If ``True`` (default is ``False``), make sure to exclude all other cell-ids from this identity.
        By default, only update the list of given cell as belonging to this identity.
    by_time_point : bool, optional
        Control the behaviour of `strict`.
        If ``True`` (default is ``False``), work by time-point (exclude other cell from same time-point).
        By default, exclude all other cell (from all time-points).
    time_index : list
        List of time-indexes to filter the loaded identity files.

    Returns
    -------
    timagetk.graphs.TissueGraph or timagetk.graphs.TemporalTissueGraph
        The (temporal) tissue graph with the cell identities.

    """
    from os import listdir
    cell_identities_list = listdir(filepath)
    log.info(f"Found {len(cell_identities_list)} cell identity files under `filepath`.")

    for cell_identity_file in cell_identities_list:
        cell_identity, _ = cell_identity_file.split('.')
        graph = import_identity(graph, filepath, cell_identity, **kwargs)

    return graph


def to_json(graph, fname):
    """Export a (temporal) tissue graph to a JSON file.

    Parameters
    ----------
    graph : timagetk.graphs.TissueGraph or timagetk.graphs.TemporalTissueGraph
        The (temporal) tissue graph to export.
    fname : str or pathlib.Path
        The file path and name to use to save the JSON.

    Examples
    --------
    >>> from timagetk.io.graph import to_json
    >>> from timagetk.graphs.tissue_graph import example_tissue_graph
    >>> graph = example_tissue_graph("sphere")
    >>> to_json(graph, "~/sphere_t0.json")

    """
    import json
    from pathlib import Path
    try:
        assert isinstance(graph, (TissueGraph, TemporalTissueGraph))
    except AssertionError:
        raise TypeError("This method can only export TissueGraph or TemporalTissueGraph to DataFrame.")

    if isinstance(graph, TissueGraph):
        json_data = tissue_graph_to_json_dict(graph)
    elif isinstance(graph, TemporalTissueGraph):
        json_data = {"time-point":
                         {tp: tissue_graph_to_json_dict(graph._tissue_graph[tp]) for tp in range(graph.nb_time_points)},
                     "elapsed_time": graph._elapsed_time,
                     "time_unit": graph._time_unit,
                     }
    else:
        raise TypeError(f"Unknown graph type `{clean_type(graph)}`!")

    if isinstance(fname, Path):
        fname = fname.expanduser()
    else:
        fname = Path(fname).expanduser()

    with open(fname, 'w') as f:
        json.dump(json_data, f, indent=4, default=convert)

    return


def convert(o):
    if isinstance(o, np.generic):
        return o.item()
    raise TypeError


def tissue_graph_to_json_dict(graph):
    """Save the TissueGraph as a JSON file.

    Parameters
    ----------
    timagetk.graphs.TissueGraph
        The tissue graph.

    Returns
    -------
    dict
        A json serialized dictionary.

    Examples
    --------
    >>> from timagetk.io.graph import tissue_graph_to_json_dict
    >>> from timagetk.graphs.tissue_graph import example_tissue_graph
    >>> graph = example_tissue_graph("sphere")
    >>> json_graph = tissue_graph_to_json_dict(graph)

    """
    from networkx.readwrite import json_graph

    for feat in graph.list_cell_properties():
        ppty_dict = graph.cell_property_dict(feat, default=None)
        graph._update_ppty('primal', 'nodes', feat, _dict_array_to_list(ppty_dict))

    for feat in graph.list_cell_wall_properties():
        ppty_dict = graph.cell_wall_property_dict(feat, default=None)
        graph._update_ppty('primal', 'edges', feat, _dict_array_to_list(ppty_dict))

    for feat in graph.list_cell_edge_properties():
        ppty_dict = graph.cell_edge_property_dict(feat, default=None)
        graph._update_ppty('dual', 'edges', feat, _dict_array_to_list(ppty_dict))

    for feat in graph.list_cell_vertex_properties():
        ppty_dict = graph.cell_vertex_property_dict(feat, default=None)
        graph._update_ppty('dual', 'nodes', feat, _dict_array_to_list(ppty_dict))

    primal_json = json_graph.node_link_data(graph.primal)
    dual_json = json_graph.node_link_data(graph.dual)
    graph_json = {"background_id": graph.background,
                  "cell_properties": graph._cell_properties,
                  "wall_properties": graph._wall_properties,
                  "edge_properties": graph._edge_properties,
                  "vertex_properties": graph._vertex_properties,
                  }
    return {"primal": primal_json, "dual": dual_json, "graph": graph_json}


def tissue_graph_from_json(json_data):
    """Return a TissueGraph from JSON data.

    Parameters
    ----------
    json_data : str
        Path to the JSON file containing the tissue graph.

    Returns
    -------
    timagetk.graphs.TissueGraph
        The tissue graph.

    Examples
    --------
    >>> import json
    >>> from pathlib import Path
    >>> from timagetk.io.graph import tissue_graph_from_json
    >>> with open(Path("~/sphere_t0.json").expanduser(), 'r') as f: json_data = json.load(f)
    >>> graph = tissue_graph_from_json(json_data)
    >>> graph.list_cell_properties()
    >>> graph.list_cell_wall_properties()
    >>> graph.cell_property_dict("volume")

    """
    from networkx.readwrite import json_graph
    from timagetk.graphs.nx_graph import Graph

    primal_graph = json_graph.node_link_graph(json_data["primal"], directed=False, multigraph=False)
    dual_graph = json_graph.node_link_graph(json_data["dual"], directed=False, multigraph=False)
    graph = TissueGraph()
    graph.primal = Graph(primal_graph)
    graph.dual = Graph(dual_graph)
    graph.background = json_data["graph"]["background_id"]
    graph._cell_properties = json_data["graph"]["cell_properties"]
    graph._wall_properties = json_data["graph"]["wall_properties"]
    graph._edge_properties = json_data["graph"]["edge_properties"]
    graph._vertex_properties = json_data["graph"]["vertex_properties"]

    return graph


def from_json(json_file):
    import json
    with open(json_file, 'r') as f:
        json_data = json.load(f)

    if "time-point" in json_data:
        pass
