#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Image IO (input/output) module."""
import sys
import tempfile
from os.path import basename
from os.path import sep
from pathlib import Path

import numpy as np
import pooch
import scipy.ndimage as nd
from vt.image import Image

from timagetk import TissueImage2D
from timagetk import TissueImage3D
from timagetk.bin.logger import get_logger
from timagetk.components.labelled_image import LabelledImage
from timagetk.components.metadata import _check_class_def
from timagetk.components.metadata import _check_fname_def
from timagetk.components.metadata import _check_physical_ppty
from timagetk.components.multi_channel import MultiChannelImage
from timagetk.components.spatial_image import SpatialImage
from timagetk.io.util import LSM_EXT
from timagetk.io.util import TIF_EXT
from timagetk.io.util import get_pne
from timagetk.third_party.vt_converter import vt_to_spatial_image
from timagetk.util import check_type
from timagetk.util import clean_type
from timagetk.util import now

log = get_logger(__name__)


def _image_from_url(url, hash_value=None):
    """Fetches an image from the given URL using `pooch` for data management.

    Parameters
    ----------
    url : str
        A valid URL pointing toward an image.
    hash_value : str, optional
        The hash value to use for comparison.

    Returns
    -------
    str
        Path to the temporary file containing the retreived image.

    Notes
    -----
    We leverage the `pooch` library to download and manage a data cache.
    You may use the `TIMAGETK_DATA_DIR` environment variable to control the path to sotre the downloaded data.

    Examples
    --------
    >>> from timagetk.io.image import _image_from_url
    >>> url = 'https://zenodo.org/record/3737630/files/sphere_membrane_t0.inr.gz'
    >>> # Use secure hash to validate downloaded file
    >>> md5_h = 'md5:24cc1edfaca092c26486a58fd6979827'
    >>> tmp_file = _image_from_url(url, hash_value=md5_h)
    >>> print(tmp_file)
    $HOME/.cache/timagetk/sphere_membrane_t0.inr.gz
    >>> # Read the temporary file image with timagetk ``imread``:
    >>> from timagetk.io.image import imread
    >>> img = imread(tmp_file)
    >>> # Open the stack browser:
    >>> from timagetk.visu.stack import stack_browser
    >>> v = stack_browser(img)
    >>> # Use WRONG secure hash to INvalidate downloaded file:
    >>> wrong_md5_h = 'md5:d3079693f5d25f0743c19b004e88eaf1'
    >>> tmp_file = _image_from_url(url, hash_value=wrong_md5_h)
    ValueError: MD5 hash of downloaded file (aada9f66b4175821b2824c0c08358273-sphere_membrane_t0.inr.gz) does not match the known hash: expected md5:d3079693f5d25f0743c19b004e88eaf1 but got 24cc1edfaca092c26486a58fd6979827. Deleted download for safety. The downloaded file may have been corrupted or the known hash may be outdated.

    """
    # Create a pooch object in a local directory for caching files.
    image_pooch = pooch.create(
        path=pooch.os_cache("timagetk"),
        base_url=url,
        env="TIMAGETK_DATA_DIR"  # You can specify an environment variable for the cache directory path.
    )
    # Extract the filename from the URL
    original_filename = basename(url)

    log.debug(f"Got image url: {url}")
    if hash_value is not None:
        log.debug(f"Got corresponding hash value: {hash_value}")
    else:
        log.debug(f"Without hash value!")

    # Fetch the file from URL and retrieve its local path.
    file_path = pooch.retrieve(
        url=url,
        known_hash=hash_value,
        path=image_pooch.abspath,
        fname=original_filename,
        progressbar=True,
    )
    log.debug(f"Cached image file: {file_path}")
    return file_path


def imread(fname, rtype=None, **kwargs):
    """Read an image (2D/3D +C).

    Parameters
    ----------
    fname : str or pathlib.Path
        File path to the image, can be a URL.
    rtype : type, optional
        If defined, force returned type of image to given class.

    Other Parameters
    ----------------
    channel_names : list[str]
        List of channel names, to use with ``MultiChannelImage`` instances to override those defined in the CZI file.
    channel : str
        Channel to return, *i.e* only this one will be returned, to use with ``MultiChannelImage`` instances.
    hash_value : str
        The hash value to use if the `fname` is a URL, in `algo:value` format, see examples.

    Returns
    -------
    timagetk.SpatialImage or timagetk.LabelledImage or timagetk.MultiChannelImage
        Image and metadata (such as voxel-size, extent, type, etc.)

    Notes
    -----
    In case of a URL as `fname`, we leverage the `pooch` library to download and manage a data cache.
    You may use the `TIMAGETK_DATA_DIR` environment variable to control the path to sotre the downloaded data.

    See Also
    --------
    timagetk.io.util.POSS_EXT

    Example
    -------
    >>> from timagetk.io.image import imread
    >>> # EXAMPLE #1 - Load a file from URL:
    >>> img = imread('https://zenodo.org/record/3737630/files/sphere_membrane_t0.inr.gz')
    >>> type(img)
    timagetk.SpatialImage
    >>> print((img.filepath, img.filename))
    ('/tmp', 'sphere_membrane_t0.inr.gz')

    >>> # EXAMPLE #2 - Load a file from URL and validate with secure hash:
    >>> md5_h = 'md5:24cc1edfaca092c26486a58fd6979827'
    >>> img = imread('https://zenodo.org/record/3737630/files/sphere_membrane_t0.inr.gz', hash_value=md5_h)
    >>> type(img)
    timagetk.SpatialImage
    >>> print((img.filepath, img.filename))
    ('/tmp', 'sphere_membrane_t0.inr.gz)

    >>> # EXAMPLE #3 - Load a file from URL and force returned instance type:
    >>> from timagetk import TissueImage3D
    >>> url = 'https://zenodo.org/record/3737630/files/sphere_membrane_t0_seg.inr.gz'
    >>> img = imread(url, TissueImage3D, background=1, not_a_label=0)
    >>> type(img)
    timagetk.components.tissue_image.TissueImage3D
    >>> # EXAMPLE #4 - Load a file from the shared data:
    >>> from timagetk.io.dataset import shared_data_path
    >>> image_path = shared_data_path('flower_confocal', 0)
    >>> img = imread(image_path)
    >>> type(img)
    timagetk.SpatialImage

    """
    if isinstance(fname, str):
        # URL checkpoint:
        if fname.startswith('http'):
            return imread(_image_from_url(fname, hash_value=kwargs.pop('hash_value', None)), rtype, **kwargs)
        else:
            fname = Path(fname)
    fname = fname.expanduser().absolute()
    # Check file exists:
    if not fname.exists():
        log.error(f"Could not find file `{fname}`!")
        return None
    # Get file path, name, name and extension:
    filepath, filename, ext = get_pne(fname, test_format=True)
    log.debug("Loading image: {}".format(fname))

    channel_names = kwargs.get("channel_names", None)

    if ext == ".czi":
        img = read_czi_image(fname, channel_names)
        if 'channel' in kwargs:
            img = img.get_channel(kwargs.get('channel'))
        if isinstance(img, dict):
            return img
    elif ext == ".lsm":
        img = read_lsm_image(fname, channel_names)
        if 'channel' in kwargs:
            img = img.get_channel(kwargs.get('channel'))
    elif ext == ".nd2":
        img = read_nd2_image(fname, channel_names)
        if 'channel' in kwargs:
            img = img.get_channel(kwargs.get('channel'))
    elif ext in TIF_EXT:
        try:
            img = read_tiff_image(fname, channel_names)
        except Exception as e:
            log.warning(f"TiffFile raised an error! '{e}'")
            log.warning(f"Could not load file {fname} using TiffFile, trying with vt...")
            img = vt_to_spatial_image(Image(str(fname)))
        if isinstance(img, MultiChannelImage) and 'channel' in kwargs:
            img = img.get_channel(kwargs.get('channel'))
    else:
        img = vt_to_spatial_image(Image(str(fname)))

    img = _check_class_def(img)
    img = _check_fname_def(img, filename=filename, filepath=filepath)

    if isinstance(img, MultiChannelImage):
        channels = img.get_channel_names()
        ch = channels[0]
        _check_physical_ppty(img.metadata_image[ch].get_dict())
    else:
        _check_physical_ppty(img.metadata_image.get_dict())

    # Type conversion according to detected class:
    md_class = img.metadata.get('timagetk', None)
    if md_class == 'LabelledImage':
        img = LabelledImage(img, **kwargs)
    elif md_class == 'TissueImage2D':
        img = TissueImage2D(img, **kwargs)
    elif md_class == 'TissueImage3D':
        img = TissueImage3D(img, **kwargs)
    else:
        img.metadata.update({'timagetk': {'class': 'SpatialImage'}})

    if rtype is not None:
        img = rtype(img, **kwargs)
    return img


def imsave(fname, sp_img, **kwargs):
    """Save an image (2D/3D).

    Parameters
    ----------
    fname : str or pathlib.Path
        File path where to save the image.
    sp_img : timagetk.SpatialImage or timagetk.LabelledImage or timagetk.TissueImage3D or timagetk.MultiChannelImage
        Image instance to save on drive.

    Other Parameters
    ----------------
    pattern : str
        Physical dimension order to use to save the image, "ZCYX" by default.
    force : bool
        Overwrite any existing file of same `fname`.

    See Also
    --------
    timagetk.io.util.POSS_EXT

    Example
    -------
    >>> from timagetk.array_util import random_spatial_image
    >>> import tempfile
    >>> import numpy as np
    >>> from timagetk.io.image import imsave, imread
    >>> from timagetk import SpatialImage
    >>> sp_image = random_spatial_image((3, 4, 5), voxelsize=[0.3, 0.4 ,0.5])
    >>> # Save the image in a temporary file:
    >>> tmp_path = tempfile.NamedTemporaryFile()
    >>> imsave(tmp_path.name+'.tif', sp_image)
    >>> # Load this file:
    >>> sp_image_cp = imread(tmp_path.name+'.tif')
    >>> # Compare arrays and voxelizes:
    >>> np.testing.assert_array_equal(sp_image, sp_image_cp)
    >>> np.testing.assert_array_equal(sp_image.voxelsize, sp_image_cp.voxelsize)

    >>> sp_image = random_spatial_image((3, 4), voxelsize=[0.3, 0.4])
    >>> # Save the image in a temporary file:
    >>> tmp_path = tempfile.NamedTemporaryFile()
    >>> imsave(tmp_path.name+'.tif', sp_image)
    >>> # Load this file:
    >>> sp_image_cp = imread(tmp_path.name+'.tif')
    >>> # Compare arrays and voxelizes:
    >>> np.testing.assert_array_equal(sp_image, sp_image_cp)
    >>> np.testing.assert_array_equal(sp_image.voxelsize, sp_image_cp.voxelsize)

    >>> from timagetk import MultiChannelImage
    >>> from timagetk.io import imsave
    >>> from timagetk.synthetic_data.nuclei_image import example_nuclei_signal_images
    >>> n_img, s_img, _, _= example_nuclei_signal_images(n_points=10, extent=20, voxelsize=(1,1,1))
    >>> img = MultiChannelImage({'nuclei':n_img, 'signal':s_img})
    >>> imsave('~/test.tif', img)

    >>> tmp_path = tempfile.NamedTemporaryFile()
    >>> imsave(tmp_path.name+'.tif', img)

    """
    if isinstance(fname, str):
        fname = Path(fname)

    # - Assert sp_img is a SpatialImage or a MultiChannelImage instance:
    try:
        assert isinstance(sp_img, SpatialImage) or isinstance(sp_img, MultiChannelImage)
    except AssertionError:
        raise TypeError("Parameter 'sp_img' is not a SpatialImage or a MultiChannelImage!")

    filepath, filename, ext = get_pne(fname, test_format=True)

    # Check file do NOT exist:
    try:
        assert not fname.exists()
    except AssertionError:
        if not kwargs.get('force', False):
            from timagetk.util import yn_query
            log.warning(f"Found existing file `{fname}`!")
            if not yn_query(f"Overwrite existing image `{fname}`", default='no'):
                log.info("Will NOT overwrite existing image!")
                return
            else:
                kwargs['force'] = True

    # - METADATA: Update file path & name before saving:
    sp_img = _check_fname_def(sp_img, filename=filename, filepath=filepath)

    ctype = clean_type(sp_img)
    log.debug("Saving {} under: {}".format(ctype, fname))

    if ext in TIF_EXT:
        save_tiff_image(fname, sp_img, **kwargs)
    else:
        # - Assert sp_img is a SpatialImage instance:
        try:
            assert isinstance(sp_img, SpatialImage)
        except AssertionError:
            raise TypeError("Parameter 'sp_img' is not a SpatialImage!")
        vt_im = sp_img.to_vt_image()
        vt_im.write(str(fname))
        # FIXME: This does not allow to use metadata!

    return


def apply_mask(img, mask_filename, masking_value=0, crop_mask=False):
    """Load and apply a z-projection mask (2D) to a SpatialImage (2D/3D).

    In case of an intensity image, allows removing unwanted signal intensities.
    If the SpatialImage is 3D, it is applied in z-direction.
    The mask should contain a distinct "masking value".

    Parameters
    ----------
    img : timagetk.SpatialImage
        Image to modify with the mask.
    mask_filename : str
        String giving the location of the mask file
    masking_value : int, optional
        Value (default 0) defining the masked region
    crop_mask : bool, optional
        If ``True`` (default ``False``), the returned SpatialImage is cropped around the non-masked region

    Returns
    -------
    timagetk.SpatialImage
        The masked SpatialImage.
    """
    try:
        from pillow import Image
    except ImportError:
        from PIL import Image

    xsh, ysh, zsh = img.shape
    # Read the mask file:
    mask_im = Image.open(mask_filename)
    # Detect non-masked values positions
    mask_im = np.array(mask_im) != masking_value
    # Transform mask to 3D by repeating the 2D-mask along the z-axis:
    mask_im = np.repeat(mask_im[:, :, np.newaxis], zsh, axis=2)
    # Remove masked values from ``img``:
    masked_im = img * mask_im
    # Crop the mask if required:
    if crop_mask:
        bbox = nd.find_objects(mask_im)
        masked_im = masked_im[bbox]

    return masked_im


def default_image_attributes(array, log_undef=log.debug, **kwargs):
    """Set the default image attribute values given the array dimensionality.

    Parameters
    ----------
    array : numpy.ndarray
        The array to use as image.
    log_undef : fun
        The logging level to use for undefined image attributes.

    Other Parameters
    ----------------
    dtype : str or numpy.dtype
        A valid ``dtype`` to use with this array, checked against ``AVAIL_TYPES``.
        Defaults to ``array.dtype``.
    axes_order : str
        The physical axes ordering to use with this array.
        Defaults to ``default_axes_order(array)``.
    origin : list[int]
        The coordinate of the origin for this array.
        Defaults to ``default_origin(array)``.
    unit : float
        The units associated to the voxel-sizes to use with this array.
        Defaults to ``DEFAULT_SIZE_UNIT``.
    voxelsize :  list[float]
        The voxel-sizes to use with this array.
        Defaults to ``default_voxelsize(array)``.
    metadata : dict
        The dictionary of metadata to use with this array.
        Defaults to ``{}``.

    Returns
    -------
    dict
        The image attributes dictionary.

    See Also
    --------
    timagetk.components.spatial_image.AVAIL_TYPES
    timagetk.components.spatial_image.DEFAULT_SIZE_UNIT
    timagetk.components.spatial_image.default_axes_order
    timagetk.components.spatial_image.default_origin
    timagetk.components.spatial_image.default_voxelsize

    Notes
    -----
    Any extra keyword argument not listed above will be kept and returned.

    Examples
    --------
    >>> from timagetk.io.image import default_image_attributes
    >>> from timagetk.array_util import random_array
    >>> # Example 1: set the default image attributes for a 2D array:
    >>> img_2D = random_array([7, 5])
    >>> attr_2D = default_image_attributes(img_2D)
    >>> print(attr_2D)
    {'dtype': 'uint8', 'axes_order': 'YX', 'origin': [0, 0], 'unit': 1e-06, 'voxelsize': [1.0, 1.0], 'metadata': {}}

    >>> # Example 2: set the default image attributes for a 3D array:
    >>> img_3D = random_array([7, 5, 4])
    >>> attr_3D = default_image_attributes(img_3D)
    >>> print(attr_3D)
    {'dtype': 'uint8', 'axes_order': 'ZYX', 'origin': [0, 0, 0], 'unit': 1e-06, 'voxelsize': [1.0, 1.0, 1.0], 'metadata': {}}
    >>> # Example 3: set the default image attributes for a 3D array, except voxelsize:
    >>> attr_3D = default_image_attributes(img_3D, voxelsize=[0.5, 0.25, 0.25])
    >>> print(attr_3D)
    {'voxelsize': [0.5, 0.25, 0.25], 'dtype': 'uint8', 'axes_order': 'ZYX', 'origin': [0, 0, 0], 'unit': 1e-06, 'metadata': {}}

    """
    from timagetk.components.spatial_image import default_axes_order
    from timagetk.components.spatial_image import default_origin
    from timagetk.components.spatial_image import DEFAULT_SIZE_UNIT
    from timagetk.components.spatial_image import default_voxelsize

    # Get the number of dimension in the array:
    ndim = array.ndim

    # ----------------------------------------------------------------------
    # DTYPE:
    # ----------------------------------------------------------------------
    dtype = kwargs.get('dtype', None)
    if dtype is None:
        # - Set it to the array type:
        dtype = np.dtype(array.dtype).name
    else:
        # Try to convert it to a str
        try:
            dtype = np.dtype(dtype).name
        except:
            log.error(f"Given ``dtype`` ({dtype}) is not valid!")
            dtype = array.dtype
            log.info(f"Falling back to ``array`` dtype: '{dtype}'.")
        else:
            log.debug(f"Defined parameter ``dtype`` as: {dtype}")

    # ----------------------------------------------------------------------
    # Physical axes ordering:
    # ----------------------------------------------------------------------
    axes_order = kwargs.get('axes_order', None)
    if axes_order is None:
        axes_order = default_axes_order(array)
        log_undef(f"Undefined parameter ``axes_order``, using default: {axes_order}")
    else:
        axes_order = ''.join([ax.upper() for ax in axes_order])

        extra_dim = ""
        if "B" in axes_order:
            # ``BlendImage`` case:
            ndim -= 1
            extra_dim = "B"
        if "C" in axes_order:
            # ``MultiChannelImage`` case:
            ndim -= 1
            extra_dim = "C"

        test_axes_order = axes_order.replace(extra_dim, '')
        try:
            assert len(test_axes_order) == ndim
        except AssertionError:
            log.error(f"Given ``axes_order`` is of size {len(test_axes_order)} for an array of dimension {ndim}!")
            axes_order = default_axes_order(array)
            axes_order += extra_dim
            log.info(f"Falling back to default value: '{axes_order}'.")
        log.debug(f"Defined parameter ``axes_order`` as: {axes_order}")

    # ----------------------------------------------------------------------
    # Origin:
    # ----------------------------------------------------------------------
    origin = kwargs.get('origin', None)
    if origin is None:
        origin = default_origin(array)
        log_undef(f"Undefined parameter ``origin``, using default: {origin}")
    else:
        origin = list(map(int, origin))
        try:
            assert len(origin) == ndim
        except AssertionError:
            log.error(f"Given ``origin`` is of size {len(origin)} for an array of dimension {ndim}!")
            origin = default_origin(array)
            log.info(f"Falling back to default value: '{origin}'.")
        log.debug(f"Defined parameter ``origin`` as: {origin}")

    # ----------------------------------------------------------------------
    # Voxel-size:
    # ----------------------------------------------------------------------
    voxelsize = kwargs.get('voxelsize', None)
    if voxelsize is None:
        voxelsize = default_voxelsize(array)
        log_undef(f"Undefined parameter ``voxelsize``, using default: {voxelsize}")
    else:
        voxelsize = list(map(float, voxelsize))
        try:
            assert len(voxelsize) == ndim
        except AssertionError:
            log.error(f"Given ``voxelsize`` is of size {len(voxelsize)} for an array of dimension {ndim}!")
            voxelsize = default_voxelsize(array)
            log.info(f"Falling back to default value: '{voxelsize}'.")
        log.debug(f"Defined parameter ``voxelsize`` as: {voxelsize}")

    # ----------------------------------------------------------------------
    # Size unit:
    # ----------------------------------------------------------------------
    unit = kwargs.get('unit', None)
    if unit is None:
        unit = DEFAULT_SIZE_UNIT
        log_undef(f"Undefined parameter ``unit``, using default: {unit}")
    else:
        log.debug(f"Defined parameter ``unit`` as: {unit}")

    # ----------------------------------------------------------------------
    # Metadata:
    # ----------------------------------------------------------------------
    metadata = kwargs.get('metadata', None)
    # - Initialize (empty) or check type of given metadata dictionary:
    if metadata == None:
        metadata = {}
        log.debug("No metadata dictionary provided!")
    else:
        check_type(metadata, 'metadata', dict)

    kwargs['dtype'] = dtype
    kwargs['axes_order'] = axes_order
    kwargs['origin'] = origin
    kwargs['unit'] = unit
    kwargs['voxelsize'] = voxelsize
    kwargs['metadata'] = metadata

    return kwargs


VALID_DIMS = "TCXYZ"


def validate_pattern(pattern: str) -> bool:
    """Validate dimension pattern string."""
    valid_chars = set('ZCYX')
    pattern_chars = set(pattern)
    if not pattern_chars.issubset(valid_chars):
        raise ValueError(f"Invalid characters in pattern: {pattern_chars - valid_chars}")
    if len(pattern) != len(set(pattern)):
        raise ValueError("Duplicate dimensions in pattern")
    return True


def read_czi_image(czi_file, channel_names=None, **kwargs):
    """Read Carl Zeiss Image (CZI) file.

    Parameters
    ----------
    czi_file : str
        File path to the image to load.
    channel_names : list[str], optional
        Defines channel names to use.
        By default, try to load channel names from the image metadata.

    Other Parameters
    ----------------
    return_order : str
        Physical dimension order to use for the returned 2D/3D image array.
        Defaults to "ZYX".

    Returns
    -------
    timagetk.SpatialImage or timagetk.MultiChannelImage
        If a single channel and time-point is found, returns a ``SpatialImage`` instance.
        If multiple channels are found but with a single time-point, returns a ``MultiChannelImage`` instance.

    Notes
    -----
    If more than one channel is found, return a ``MultiChannelImage``.

    For now, only the following dimension are supported:

     - ``T`` as time;
     - ``C`` as channel;
     - ``X`` as x-axis (rows);
     - ``Y`` as y-axis (planes);
     - ``Z`` as z-axis (columns);

    Examples
    --------
    >>> from timagetk.io.image import read_czi_image
    >>> from timagetk.io.image import _image_from_url
    >>> im_url = "https://zenodo.org/record/3737795/files/qDII-CLV3-PIN1-PI-E35-LD-SAM4.czi"
    >>> im_fname = _image_from_url(im_url)  # download the CZI image
    >>> ch_names = ["DII-VENUS-N7", "pPIN1:PIN1-GFP", "Propidium Iodide", "pRPS5a:TagBFP-SV40", "pCLV3:mCherry-N7"]
    >>> image = read_czi_image(im_fname, ch_names)
    >>> print(image.get_channel_names())
    ['DII-VENUS-N7', 'pPIN1:PIN1-GFP', 'Propidium Iodide', 'pRPS5a:TagBFP-SV40', 'pCLV3:mCherry-N7']
    >>> print(image)

    >>> im_url = "https://zenodo.org/record/3737795/files/qDII-CLV3-PIN1-PI-E35-LD-SAM4.czi"
    >>> im_fname = _image_from_url(im_url)  # download the CZI image
    >>> from czifile import CziFile
    >>> czi_img = CziFile(im_fname)
    >>> import xmltodict
    >>> md = xmltodict.parse(czi_img.metadata())['ImageDocument']['Metadata']

    """
    import xmltodict as xmltodict
    from czifile import CziFile

    return_order = kwargs.get('return_order', 'ZYX')
    pattern = kwargs.get('pattern', None)
    if pattern is not None:
        DeprecationWarning(f"Usage of `pattern` keyword is deprecated, we use the image metadata instead.")

    # - Load the file with third-party reader:
    log.debug(f"Using `CziFile` to load: {czi_file}")
    czi_img = CziFile(czi_file)
    log.debug("Done!")

    # -- AXES DIMENSIONS:
    # - Get the axes order pattern:
    # axes_pattern = _czi_dimensions_pattern(czi_img)
    axes_pattern = ''.join(czi_img.axes)
    log.debug(f"Found dimension pattern in metadata: {axes_pattern}...")
    # - Get the axes shape values:
    # axes_shape = _czi_array_shape_from_information_image(md)
    axes_shape = czi_img.shape
    log.debug(f"Found image shape in metadata: {axes_shape}...")
    # Make an axes shape dictionary:
    shape_dict = dict(zip(czi_img.axes, czi_img.shape))
    log.debug(f"Got shape dictionary from metadata: {shape_dict}...")
    # - Filter-out unidimensional axes from axes shape dict, if any:
    shape_dict = {ax: ndims for ax, ndims in shape_dict.items() if ndims != 1}
    log.debug(f"Will use shape dictionary: {shape_dict}...")
    # - Filter-out unidimensional axes from axes return order pattern, if any:
    return_order = ''.join([ax for ax in return_order if shape_dict.get(ax, 1) >= 1])
    # Test if we have unwanted dimensions, only those in VALID_DIMS are handled for now...
    non_valid_ax = [ax for ax in shape_dict if ax not in VALID_DIMS]
    if len(non_valid_ax) > 0:
        log.critical(f"Found unwanted dimension in the array: {', '.join(non_valid_ax)}")
        return None

    # - Check the given channel names are enough for the number of channels found in the image:
    n_channels = shape_dict.get('C', 1)
    if n_channels > 1 and channel_names is not None:
        try:
            assert len(channel_names) == n_channels
        except AssertionError:
            # Reset channel names to `None` to use those in the metadata:
            log.error(f"Found {n_channels} channels but {len(channel_names)} were names were given.")
            channel_names = None

    # -- METADATA:
    md = xmltodict.parse(czi_img.metadata())['ImageDocument']['Metadata']

    # Note: no other dict entries except 'ImageDocument'/'Metadata' so we have all the metadata!
    # list(md.keys()) => 'Experiment', 'HardwareSetting', 'ImageScaling', 'Information', 'Scaling', 'DisplaySetting', 'Layers'

    def _czi_array_bitdepth(md):
        return md['Information']['Image']["ComponentBitCount"]

    def _czi_array_shape_from_information_image(md):
        sub_md = md['Information']['Image']
        md_shape = {ax.lower(): sub_md.get(f"Size{ax}", 1) for ax in "XYSHMCZT"}
        return md_shape

    def _czi_voxelsize_from_scaling_item_distance(md):
        sub_md = md['Scaling']['Items']['Distance']
        md_vxs = {sub_md[ax]['@Id']: float(sub_md[ax]['Value']) for ax in range(3)}
        return md_vxs

    def _czi_dimensions_pattern(czi_img):
        axes_pattern = None
        for s in czi_img.segments():
            if s.SID == "ZISRAWSUBBLOCK":
                metadata = str(s).split('\n')
                for i, row in enumerate(metadata):
                    if 'DirectoryEntry' in row:
                        next_md = metadata[i + 1]
                        axes_pattern = next_md.split(' ')[4]
                        if axes_pattern[:2] == 'b\'':  # decoding issue?
                            axes_pattern = axes_pattern[2:-1]
        return axes_pattern

    # - Get the voxel-size dictionary!
    md_vxs = _czi_voxelsize_from_scaling_item_distance(md)
    log.debug(f"Found metadata 'voxelsize': {md_vxs}")

    # - Get the name of the Microscope:
    if 'Microscopes' in md['Information']['Instrument']:
        micro_md = md['Information']['Instrument']['Microscopes']['Microscope']
        if '@Name' in micro_md and 'Type' in micro_md:
            m_name = micro_md['@Name']
            m_type = micro_md['Type']
            m_system = micro_md['System']
            microscope = f"{m_name} - {m_type} {m_system}"
        else:
            microscope = micro_md['System']
    else:
        microscope = "Unknown microscope"
    log.debug(f"Found metadata from {microscope}")

    # - Get the name of the image:
    if 'Name' in md['Information']['Document']:
        name = md['Information']['Document']['Name']
        log.debug(f"Found metadata 'Name': {name}")
    else:
        name = None
        log.debug(f"Could not find image name in metadata!")

    # - Get the acquisition date:
    if 'CreationDate' in md['Information']['Document']:
        creation_date = md['Information']['Document']['CreationDate']
        log.debug(f"Found metadata 'CreationDate': {creation_date}")
    else:
        creation_date = "Unknown"
        log.warning(f"Could not find image creation date in metadata!")

    # - Get the channel & dye names:
    md_channels = md['DisplaySetting']['Channels']['Channel']
    md_dye_names = None
    if n_channels > 1:
        # Multi channel case
        md_ch_names = [md_channels[ch]['@Name'] for ch in range(n_channels)]
        if 'DyeName' in md_channels[0].keys():
            md_dye_names = [md_channels[ch]['DyeName'] for ch in range(n_channels)]
    else:
        # Single channel case
        md_ch_names = md_channels['@Name']
        if 'DyeName' in md_channels.keys():
            md_dye_names = md_channels['DyeName']
    log.debug(f"Found channel names in the metadata: {md_ch_names}...")
    if channel_names is None:
        channel_names = md_ch_names
        log.debug(f"Using channel names found in metadata: {channel_names}")
    if md_dye_names is not None:
        log.debug(f"Found dye names in the metadata: {md_dye_names}")
    else:
        log.warning(f"Could not find dye names in the metadata")
    # TODO: Some channels may have color info under ``channels[ch]['Color']``
    # TODO: Other channels should have a "palette" info under ``channels[ch]['PaletteName']``

    # -- voxel-size:
    unit = 1  # meter unit
    voxelsize = tuple([md_vxs[dim] for dim in return_order])  # re-order to match axes return order
    # Convert unit:
    if all(vxs < 1e-8 for vxs in voxelsize):
        # Convert to nanometer unit:
        unit = 1e-9
    elif all(vxs < 1e-5 for vxs in voxelsize):
        # Convert to micrometer unit:
        unit = 1e-6
    elif all(vxs < 1e-2 for vxs in voxelsize):
        # Convert to millimeter unit:
        unit = 1e-3
    voxelsize = [vxs / unit for vxs in voxelsize]

    # Defines file name & path:
    fname = Path(czi_file).name
    fpath = Path(czi_file).parent

    def _reshape_array(arr, current_order, return_order):
        """Re-order the array to match requested axes order."""
        if current_order == return_order:
            return arr
        else:
            log.debug(f"Transposing array from '{current_order}' to '{return_order}'...")
        log.debug(f"Array '{current_order}' shape BEFORE transpose: {arr.shape}")
        # Axes id order for axes to returns ('X', 'Y', 'Z'):
        axes = [current_order.find(ax) for ax in return_order]
        # Transpose axes to get requested axes order:
        arr = np.transpose(arr, axes)
        log.debug(f"Array '{return_order}' shape AFTER transpose: {arr.shape}")
        return arr

    def _image_instance(array, return_order, channel_names, property_kwargs, file_kwargs):
        current_order = ''.join([ax for ax in shape_dict.keys() if ax in 'XYZ'])
        if shape_dict.get("C", 1) > 1:
            # Return a MultiChannelImage
            ch_axis_id = "".join(shape_dict.keys()).find('C')
            if channel_names is None:
                channel_names = [f"CH_{n}" for n in md_ch_names]
            channel_images = []
            metadata = property_kwargs.pop('metadata')
            for i_ch, _ in enumerate(channel_names):
                ch_array = np.take(array, i_ch, axis=ch_axis_id)
                ch_array = _reshape_array(ch_array, current_order, return_order)
                im_kwargs = default_image_attributes(ch_array, **property_kwargs)
                channel_images.append(SpatialImage(ch_array, **im_kwargs))
            img = MultiChannelImage(channel_images, channel_names=channel_names, **file_kwargs,
                                    name=name if name is not None else fname, metadata=metadata)
            log.debug(f"Channel names: {', '.join(img.get_channel_names())}")
        else:
            # Return a SpatialImage:
            array = _reshape_array(array, current_order, return_order)
            im_kwargs = default_image_attributes(array, **property_kwargs)
            img = SpatialImage(array, **file_kwargs, **im_kwargs)
        return img

    # -- ARRAYS:
    czi_array = czi_img.asarray()
    log.debug(f"Loaded {czi_array.ndim}D CZI file of shape: {czi_array.shape}")
    czi_array = np.squeeze(czi_array)  # get rid of one-dimensional axes
    log.debug(f"Squeezed into a {czi_array.ndim}D CZI array of shape: {czi_array.shape}")
    # By now, the only axes we can find are the "valid ones", so: max(czi_array.ndim) = 5
    property_kwargs = {'voxelsize': voxelsize, 'unit': unit, 'acquisition_date': creation_date, 'metadata': md}
    file_kwargs = {'filename': str(fname), 'filepath': str(fpath)}

    if shape_dict.get("T", 1) > 1:
        img = {}
        t_idx = "".join(shape_dict.keys()).find("T")  # get the index of the temporal axis
        for t in range(shape_dict["T"]):
            new_arr = np.take(czi_array, t, axis=t_idx)
            img[t] = _image_instance(new_arr, return_order, channel_names, property_kwargs, file_kwargs)
    else:
        img = _image_instance(czi_array, return_order, channel_names, property_kwargs, file_kwargs)

    return img


def read_lsm_image(lsm_file, channel_names=None, **kwargs):
    """Read LSM file.

    Parameters
    ----------
    lsm_file : str
        File path to the image to load.
    channel_names : list[str], optional
        Defines channel names to use.
        By default, try to load channel names from the image metadata.

    Other Parameters
    ----------------
    pattern : str, optional
        Physical dimension order to use to load the image, "TZCYX" by default.
        By default, try to load the pattern from the image metadata.
    return_order : str
        Physical dimension order to use for the returned array.
        Defaults to "ZYXC".

    Returns
    -------
    timagetk.SpatialImage or timagetk.MultiChannelImage
        If a single channel and time-point is found, returns a ``SpatialImage`` instance.
        If multiple channels are found but with a single time-point, returns a ``MultiChannelImage`` instance.

    Notes
    -----
    If more than one channel is found, return a ``MultiChannelImage``.

    For the `pattern` variable, use:

     - ``C`` as channel;
     - ``T`` as time;
     - ``X`` as x-axis (rows);
     - ``Y`` as y-axis (planes);
     - ``Z`` as z-axis (columns);

    Examples
    --------
    >>> from timagetk.io.image import read_lsm_image
    >>> from timagetk.io.dataset import shared_data_path
    >>> im = read_lsm_image(shared_data_path("flower_multiangle", 0))
    >>> im.get_voxelsize()
    [1.0, 0.20031953747828882, 0.20031953747828882]
    >>> im.get_shape()
    [50, 460, 460]
    >>> im.unit
    1e-06
    >>> im.axes_order
    'ZYX'

    """
    from tifffile.tifffile import imread as tiffread
    from tifffile.tifffile import TiffFile

    return_order = kwargs.get('return_order', 'ZYXC')
    pattern = kwargs.get('pattern', 'TZCYX')

    # Get absolute path and check extension is valid:
    if isinstance(lsm_file, str):
        lsm_file = Path(lsm_file)
    lsm_file = lsm_file.expanduser().absolute()
    filename = lsm_file.name
    filepath = str(lsm_file.parent)
    ext = ''.join(lsm_file.suffixes)
    try:
        assert ext in LSM_EXT
    except AssertionError:
        raise NotImplementedError(f"Unknown file extension '{ext}', should be in {TIF_EXT}.")

    # - Load the file with third-party reader:
    log.debug(f"Using `tifffile.tifffile.TiffFile` to load metadata of {lsm_file}")
    tif = TiffFile(lsm_file)
    log.debug("Done!")

    series = tif.series[0]  # get shape and dtype of the first image series
    lsm_pattern = series.axes
    if lsm_pattern is not None or lsm_pattern != "":
        log.debug(f"Found LSM dimensions pattern: {lsm_pattern}")  # pattern can be TZCYX or TCZYX...
        pattern = lsm_pattern
        # Remove "channel" symbol from physical dimension order to use
        if 'c' not in pattern.lower():
            return_order = return_order.replace('C', '')

    lsm_info = series.pages[0].tags[34412]
    md_vxs = {"Z": lsm_info.value['VoxelSizeZ'], 'Y': lsm_info.value['VoxelSizeY'], 'X': lsm_info.value['VoxelSizeX']}
    log.debug(f"Found metadata 'voxelsize': {md_vxs}")
    md_ori = {"Z": lsm_info.value['OriginZ'], 'Y': lsm_info.value['OriginY'], 'X': lsm_info.value['OriginX']}
    log.debug(f"Found metadata 'origin': {md_ori}")
    md_n_ch = lsm_info.value['DimensionChannels']
    md_n_time = lsm_info.value['DimensionTime']
    try:
        assert md_n_time == 1
    except AssertionError:
        raise NotImplementedError("Found more than one time point in metadata!")

    lsm_md = lsm_info.value['ScanInformation']
    name = lsm_md['Name']
    # md_ch_names = lsm_info.value['ScanInformation']['Tracks']['Name']

    # -- ORIGIN:
    origin = tuple([md_ori[dim] for dim in return_order if dim in md_ori])
    # -- VOXELSIZE:
    voxelsize = tuple([md_vxs[dim] for dim in return_order if dim in md_vxs])
    unit = 1
    if all(vxs < 1e-5 for vxs in voxelsize):
        voxelsize = [vxs * 1e6 for vxs in voxelsize]
        unit = 1e-6

    # -- ARRAYS:
    log.debug(f"Using `tifffile.tifffile.imread` to load array of {lsm_file}")
    lsm_arr = tiffread(lsm_file)
    log.debug(f"Loaded {lsm_arr.ndim}D LSM file of shape: {lsm_arr.shape}")

    # Defines number of channels in array
    ch_axis_id = pattern.find('C')  # a value of `-1` means "not found it the str", so no channel defined!
    if ch_axis_id != -1:
        n_channels = lsm_arr.shape[ch_axis_id]
        log.debug(f"Found {n_channels} channels in the array...")
        try:
            assert n_channels == md_n_ch
        except AssertionError:
            log.error(f"Loaded an array of shape: {lsm_arr.shape}")
            log.error(f"Found a dimension pattern in the metadata: {pattern}")
            raise ValueError(f"Metadata ({md_n_ch}) and array ({n_channels}) mismatch on channel number!")
    else:
        n_channels = 0

    if n_channels > 1 and 'C' not in return_order:
        return_order += 'C'  # add 'channel' to output axis ordering pattern before re-ordering
    # - Re-order the array according to ``return_order``
    log.debug(f"Array shape BEFORE transpose & reshape: {lsm_arr.shape}")
    # Axes id order for axes to returns ('X', 'Y', 'Z' & 'C'):
    reorder_axis = [pattern.find(c) for c in return_order]
    log.debug(f"Detected axes order: {dict(zip(return_order, reorder_axis))}")
    # Axes id order for axes NOT to returns (others like 'T'):
    extra_axis = [i for i in range(lsm_arr.ndim) if pattern[i] not in return_order]
    # Transpose axes to get array with returned axes as the 4 first
    lsm_arr = np.transpose(lsm_arr, tuple(reorder_axis + extra_axis))
    # Clip the array to the required axes:
    lsm_arr = lsm_arr.reshape(lsm_arr.shape[:len(return_order)])
    log.debug(f"Array shape AFTER transpose & reshape: {lsm_arr.shape}")

    # Defines file name & path:
    if isinstance(lsm_file, str):
        fname = lsm_file.split('/')[-1]
        fpath = sep.join(lsm_file.split('/')[:-1])
    elif isinstance(lsm_file, Path):
        fname = lsm_file.name
        fpath = lsm_file.parent
    else:
        fname = ""
        fpath = ""
    log.debug(f"Loaded file '{fname}' with {n_channels} channels!")

    if n_channels > 1:
        ch_axis_id = return_order.find('C')  # update `ch_axis_id` after reordering
        if channel_names is None:
            channel_names = [f"CH_{i}" for i in range(n_channels)]
        channel_images = []
        for i_ch, _ in enumerate(channel_names):
            array = np.take(lsm_arr, i_ch, axis=ch_axis_id)
            axes_order = return_order[:ch_axis_id:]  # remove the "C"
            im_kwargs = default_image_attributes(array, axes_order=axes_order, voxelsize=voxelsize, unit=unit,
                                                 origin=origin, metadata=lsm_md)
            channel_images.append(SpatialImage(array, filepath=filepath, filename=filename, **im_kwargs))
        img = MultiChannelImage(channel_images, channel_names=channel_names, filename=fname, filepath=fpath, name=name)
    else:
        im_kwargs = default_image_attributes(lsm_arr, axes_order=return_order, voxelsize=voxelsize, unit=unit,
                                             origin=origin, metadata=lsm_md)
        img = SpatialImage(lsm_arr, filepath=filepath, filename=filename, **im_kwargs)

    return img


def read_tiff_image(tiff_file, channel_names=None, **kwargs):
    """Read (ImageJ) TIFF image file.

    Parameters
    ----------
    tiff_file : str or pathlib.Path
        File path to the image to load.
    channel_names : list[str], optional
        Defines channel names to use.
        By default, try to load channel names from the image metadata.

    Other Parameters
    ----------------
    pattern : str, optional
        Physical dimension order to use to load the image, "ZCYX" by default.
        By default, try to load the pattern from the image metadata.
    return_order : str
        Physical dimension order to use for the returned array.
        Defaults to "ZYXC".

    Returns
    -------
    timagetk.SpatialImage or timagetk.MultiChannelImage
        If a single channel and time-point is found, returns a ``SpatialImage`` instance.
        If multiple channels are found but with a single time-point, returns a ``MultiChannelImage`` instance.

    Notes
    -----
    If more than one channel is found, return a ``MultiChannelImage``.

    For the `pattern` variable, use:

     - ``C`` as channel;
     - ``X`` as x-axis (rows);
     - ``Y`` as y-axis (planes);
     - ``Z`` as z-axis (columns);

    Examples
    --------
    >>> from timagetk.io.image import read_tiff_image
    >>> from timagetk.io.dataset import shared_data_path
    >>> im = read_tiff_image(shared_data_path("flower_confocal", 0))
    >>> im.get_voxelsize()
    [0.2003195434808731, 0.20031954352751363, 0.20031954352751363]
    >>> im.get_shape()
    [59, 460, 460]
    >>> im.axes_order
    'ZYX'

    """
    from tifffile.tifffile import TiffFile

    return_order = kwargs.get('return_order', 'ZYXC')
    pattern = kwargs.get('pattern', 'ZCYX')
    vxs_order = return_order.replace('C', '')

    # Get absolute path and check extension is valid:
    if isinstance(tiff_file, str):
        tiff_file = Path(tiff_file)
    tiff_file = tiff_file.expanduser().absolute()
    filename = tiff_file.name
    filepath = str(tiff_file.parent)
    ext = tiff_file.suffix  # should be '.tif' or '.tiff'
    try:
        assert ext in TIF_EXT
    except AssertionError:
        raise NotImplementedError(f"Unknown file extension '{ext}', should be in {TIF_EXT}.")

    # - Load the file with third-party reader:
    tiff_img = TiffFile(tiff_file)

    # - Get the array dtype
    array_dtype = tiff_img.pages[0].dtype  # get dtype of the image in the first page
    # - Get the image array:
    tiff_channels = tiff_img.asarray().astype(array_dtype)
    # - If the first physical dimension is of size one, remove it:
    if tiff_channels.shape[0] == 1:
        tiff_channels = tiff_channels[0]

    # - Gather some metadata:
    metadata_dict = {}
    int_tags = ['XResolution', 'YResolution', 'ImageWidth', 'ImageLength', 'ImageDescription', 'Location']

    try:
        page = tiff_img.pages.pages[0]
    except:
        page = tiff_img.pages[0]

    for tag in page.tags.values():
        if tag.name in int_tags:
            if 'resolution' in tag.name:
                if (isinstance(tag.value, tuple) and len(tag.value) == 2):
                    res = float(tag.value[0] / tag.value[1])
                    metadata_dict[tag.name] = res
                elif isinstance(tag.value, float):
                    metadata_dict[tag.name] = res
            else:
                metadata_dict[str(tag.name)] = tag.value

    # - Process the 'metadata'
    if 'ImageDescription' in metadata_dict:
        description = metadata_dict['ImageDescription'].split('\n')
        md = dict(x.split('=') for x in description if len(x.split('=')) == 2)
        parsed_md = {}
        for k, v in md.items():
            try:
                parsed_md[k] = eval(v)
            except:
                parsed_md[k] = v
        metadata_dict.update(parsed_md)
        _ = metadata_dict.pop('ImageDescription')

        if 'frames' in parsed_md:
            log.warning("Temporal stack!!!")
            # TODO: write a temporal stack parser!
            # From '/data/Laure/220816_B82-5_bud03/raw/C1-220816_B82-5-allbis.czi - 220816_B82-5-allbis.czi #3.tif':
            # {'ImageJ': '1.53q',
            #  'images': 4200,
            #  'slices': 300,
            #  'frames': 14,
            #  'hyperstack': 'true',
            #  'unit': 'micron',
            #  'finterval': 7183.146230769231,
            #  'spacing': 0.59,
            #  'loop': 'false',
            #  'min': 0.0,
            #  'max': 65535.0}

    # - Process voxel size info and defines the list of values
    md_vxs = {'X': 1., 'Y': 1., 'Z': 1.}
    if 'XResolution' in metadata_dict:
        md_vxs['X'] = float(metadata_dict['XResolution'][1] / metadata_dict['XResolution'][0])
    if 'YResolution' in metadata_dict:
        md_vxs['Y'] = float(metadata_dict['YResolution'][1] / metadata_dict['YResolution'][0])
    if 'spacing' in metadata_dict:
        md_vxs['Z'] = float(metadata_dict['spacing'])
    else:
        # Do not lose correct XY voxelsize (when IJ doesn't bother writing a 1.0 spacing)
        log.error(f"No metadata found for Z spacing! Falling back to default value: '1.0'")
        md_vxs['Z'] = 1.0
    voxelsize = tuple([md_vxs[dim] for dim in vxs_order])

    # - Defines the number of channels
    try:
        n_channels = metadata_dict["channels"]
    except KeyError:
        n_channels = 1 if tiff_channels.ndim <= 3 else tiff_channels.shape[pattern.find('C')]
    ch_axis_id = return_order.find('C')  # where the channel axes will be after reordering

    if n_channels > 1:
        if tiff_channels.ndim <= 3:
            pattern = pattern.replace('Z', '')
            return_order = return_order.replace('Z', '')
            voxelsize = voxelsize[1:]
        tiff_channels = np.transpose(tiff_channels, tuple([pattern.find(c) for c in return_order]))
        if "channel names" in metadata_dict:
            try:
                channel_names = metadata_dict["channel names"]
                assert isinstance(channel_names, list) and len(channel_names) == n_channels
            except:
                pass
        if channel_names is None:
            channel_names = ["CH" + str(i) for i in range(n_channels)]
        img_channels = []
        for i_ch in range(n_channels):
            array = np.take(tiff_channels, i_ch, axis=ch_axis_id)
            im_kwargs = default_image_attributes(array, voxelsize=voxelsize, metadata=metadata_dict)
            img_channels.append(SpatialImage(array, **im_kwargs))
        img = MultiChannelImage(img_channels, channel_names=channel_names, filepath=filepath, filename=filename)
    else:
        pattern = pattern.replace('C', '')
        return_order = return_order.replace('C', '')
        if tiff_channels.ndim > 3:
            tiff_channels = tiff_channels[tuple(
                [slice(0, tiff_channels.shape[d]) if (pattern[d] in 'XYZ') else 0 for d in range(tiff_channels.ndim)])]
        if tiff_channels.ndim <= 2:
            pattern = pattern.replace('Z', '')
            return_order = return_order.replace('Z', '')
            if len(voxelsize) == 3:
                voxelsize = voxelsize[1:]
        array = np.transpose(tiff_channels, tuple([pattern.find(c) for c in return_order]))
        im_kwargs = default_image_attributes(array, voxelsize=voxelsize, metadata=metadata_dict)
        img = SpatialImage(array, filepath=filepath, filename=filename, **im_kwargs)

    return img


def save_tiff_image(tiff_file, img, **kwargs):
    """Save an image as a TIFF file.

    Parameters
    ----------
    tiff_file : str or pathlib.Path
        File path where to save the image.
    img : timagetk.SpatialImage or timagetk.LabelledImage or timagetk.TissueImage3D or timagetk.MultiChannelImage
        Image instance to save on drive.

    Other Parameters
    ----------------
    pattern : str, optional
        Physical dimension order to use to load the image, "ZCYX" by default.
        By default, try to load the pattern from the image metadata.
    compression : str, optional
        The compression level to use for the TIFF file.

    Notes
    -----
    For the `pattern` variable, use:

     - ``C`` as channel;
     - ``X`` as x-axis (rows);
     - ``Y`` as y-axis (planes);
     - ``Z`` as z-axis (columns);

    Example
    -------
    >>> import tempfile
    >>> from timagetk import MultiChannelImage
    >>> from timagetk.io.image import save_tiff_image
    >>> # EXAMPLE#1 - Save a SpatialImage as a TIFF file:
    >>> from timagetk.array_util import random_spatial_image
    >>> sp_image = random_spatial_image((3, 4, 5), voxelsize=[0.3, 0.4 ,0.5])
    >>> # Save the image in a temporary file:
    >>> tmp_path = tempfile.NamedTemporaryFile()
    >>> save_tiff_image(tmp_path.name+'.tif', sp_image)

    >>> # EXAMPLE#2 - Save a MultiChannel as a TIFF file:
    >>> from timagetk.synthetic_data.nuclei_image import example_nuclei_signal_images
    >>> n_img, s_img, _, _= example_nuclei_signal_images(n_points=10, extent=20, voxelsize=(1,1,1))
    >>> img = MultiChannelImage({'nuclei':n_img, 'signal':s_img})
    >>> tmp_path = tempfile.NamedTemporaryFile()
    >>> save_tiff_image(tmp_path.name+'.tif', img)

    """
    from tifffile.tifffile import TiffWriter
    # Default dimension order pattern if not provided in kwargs
    pattern = kwargs.get('pattern', 'ZCYX')
    _ = validate_pattern(pattern)

    # Validate input image type
    try:
        assert isinstance(img, SpatialImage) or isinstance(img, MultiChannelImage)
    except AssertionError:
        raise TypeError("Parameter 'img' is not a SpatialImage or a MultiChannelImage!")

    # Convert string path to Path object and resolve to absolute path
    if isinstance(tiff_file, str):
        tiff_file = Path(tiff_file)
    tiff_file = tiff_file.expanduser().absolute()

    # Check if file extension is valid
    ext = ''.join(tiff_file.suffixes)
    try:
        assert ext in TIF_EXT
    except AssertionError:
        raise NotImplementedError(f"Unknown file extension '{ext}', should be in {TIF_EXT}.")

    # Handle existing file - prompt for overwrite unless force=True
    try:
        assert not tiff_file.exists()
    except AssertionError:
        if not kwargs.get('force', False):
            from timagetk.util import yn_query
            log.warning(f"Found existing file `{tiff_file}`!")
            if not yn_query(f"Overwrite existing image `{tiff_file}`", default='no'):
                log.info("Will NOT overwrite existing image!")
                return

    # Handle MultiChannelImage vs single channel image
    if isinstance(img, MultiChannelImage):
        channel_names = img.channel_names
        # Stack channel arrays along first axis
        data = np.array([img.get_channel(c).get_array() for c in channel_names])
        n_channels = len(channel_names)
        metadata_dict = {'channel names': channel_names}
    else:
        # Add channel dimension to single channel image
        data = img.get_array()[np.newaxis]
        n_channels = 1
        metadata_dict = {}

    # Set dimension order and handle 2D/3D cases
    order = 'CZYX'
    voxelsize = img.voxelsize
    n_slices = img.shape[0] if img.is3D() else 1

    if img.is2D():
        # Add dummy Z dimension for 2D images
        voxelsize = [1.] + list(voxelsize)
        pattern = pattern.replace('Z', '')
        order = order.replace('Z', '')

    # Transpose data to match desired dimension order
    data = np.transpose(data, tuple([pattern.find(c) for c in order]))

    # Update metadata with image properties
    metadata_dict |= {'slices': n_slices, 'channels': n_channels}
    if img.is3D():
        metadata_dict |= {'spacing': voxelsize[0]}

    # Safely write TIFF file with error handling
    try:
        with TiffWriter(tiff_file, bigtiff=False, imagej=True) as tif:
            # Write TIFF file with metadata
            tif.write(data, compression=kwargs.get('compression', 0),
                      resolution=(1.0 / voxelsize[2], 1.0 / voxelsize[1]),
                      software='timagetk',
                      datetime=now(),
                      metadata=metadata_dict)
    except (IOError, MemoryError) as e:
        log.error(f"Failed to write TIFF file: {e}")
        if tiff_file.exists():
            tiff_file.unlink()  # cleanup partial file
        raise e
    else:
        log.info(f"Saved image under '{tiff_file}'.")
    return


def _brute_search_tiff_metadata(tiff_img):
    """Performs a brute force search of metadata in TIFF image.

    Parameters
    ----------
    tiff_img : tifffile.tifffile.TiffFile
        The TIFF file to search for metadata.

    Returns
    -------
    dict
        A dictionary with parsed metadata.

    """
    # List all attributes related to metadata:
    md_attrs = [attr for attr in dir(tiff_img) if 'metadata' in attr]

    md_dict = {}
    for md_attr in md_attrs:
        md = getattr(tiff_img, md_attr)
        if md is not None:
            md_dict[md_attr] = md

    if len(md_dict) == 0:
        log.error(f"No metadata found!")
        return
    elif len(md_dict) == 1:
        md_attr = list(md_dict.keys())[0]
        log.info(f"Found metadata in '{md_attr}' TIFF image attribute!")
        return md_attr, md_dict[md_attr]
    else:
        log.warning(f"Found metadata in more than one TIFF image attribute!")
        return md_dict


def _ome_metadata_parser(xml_input):
    """Parse metadata from OME-XML metadata.

    Parameters
    ----------
    xml_input : str
        The OME-XML string to parse.

    Returns
    -------
    dict
        A dictionary with parsed metadata.

    """
    import xmltodict
    xml_dict = xmltodict.parse(xml_input)
    pixel_md = xml_dict['OME']['Image']['Pixels']

    out_md = {}
    out_md['channels'] = pixel_md['@SizeC']
    out_md['frames'] = pixel_md['@SizeT']
    out_md['pattern'] = pixel_md['@DimensionOrder']

    return out_md


def read_nd2_image(file_path, channel_names=None, **kwargs):
    """Reads an ND2 image file and returns the image data and metadata.

    Parameters
    ----------
    file_path : str
        Path to the ND2 file to be read.

    Returns
    -------
    ndarray
        The image data from the ND2 file.
    dict
        A dictionary containing the metadata from the ND2 file.

    Examples
    --------
    >>> from timagetk.io.image import read_nd2_image
    >>> im = read_nd2_image('/data/anther_morpho/multiangle_anther/raw/S6_Vertical#2.nd2')
    >>> print(im)

    """
    import pims
    nd2_images = pims.Bioformats(file_path)

    shape_dict = nd2_images.sizes
    if 't' in shape_dict and shape_dict['t'] > 1:
        log.critical(f"Parser not ready for time-indexed images.")

    # Extract metadata
    md = nd2_images.metadata
    # To list valid metadata fields : `nd2_images.metadata.fields`
    # x_vxs = md.PixelsPhysicalSizeX(0)
    # y_vxs = md.PixelsPhysicalSizeY(0)
    # z_vxs = md.PixelsPhysicalSizeZ(0)
    # vxs = [z_vxs, y_vxs, x_vxs]
    xy_vxs = nd2_images.calibration
    z_vxs = nd2_images.calibrationZ
    voxelsize = [z_vxs, xy_vxs, xy_vxs]
    filename = md.ImageName(0)

    # Process each 3D stack
    def _zyx_image(nd2_image, voxelsize, filename):
        axes_order = 'zyx'
        nd2_image.bundle_axes = axes_order
        nd2_image.iter_axes = 'z'
        return SpatialImage(nd2_image, origin=[0, 0, 0], voxelsize=voxelsize,
                            axes_order=axes_order, unit=1e-06, filename=filename)

    if nd2_images.sizes['c'] > 1:
        # Multichannel case
        images = {}
        nd2_images.iter_axes = 'c'
        for i_ch, nd2_image in enumerate(nd2_images):
            if channel_names is None:
                channel_name = md.ChannelName(0, i_ch)
            else:
                channel_name = channel_names[i_ch]
            images[channel_name] = _zyx_image(nd2_image, voxelsize, filename)
        image = MultiChannelImage(images, filename=filename)
    else:
        # SpatialImage case
        image = _zyx_image(nd2_images, voxelsize, filename)

    return image


def pims_read(fname, **kwargs):
    """Reads an image file and returns the correct image object.

    Parameters
    ----------
    fname : str
        Path to the image file to be read.

    Other Parameters
    ----------------
    channel_names : list[str]
        List of channel names, to use with ``MultiChannelImage`` instances to override those defined in the CZI file.
    channel : str
        Channel to return, *i.e* only this one will be returned, to use with ``MultiChannelImage`` instances.

    Returns
    -------
    SpatialImage or MultichannelImage
        The loaded image data from the file.

    Notes
    -----
    In case of a URL as `fname`, we leverage the `pooch` library to download and manage a data cache.
    You may use the `TIMAGETK_DATA_DIR` environment variable to control the path to sotre the downloaded data.

    Examples
    --------
    >>> from timagetk.io.image import pims_read
    >>> fname = '/data/anther_morpho/multiangle_anther/raw/S6_Vertical#2.nd2'
    >>> im = pims_read(fname)
    >>> print(im)

    >>> im = pims_read(fname,channel_names=["Nuclei", "Membrane"],channel='Membrane')
    >>> print(im)

    >>> from timagetk.io.dataset import DATA_REGISTRY
    >>> subreg = DATA_REGISTRY['flower_multiangle'][0]
    >>> im = pims_read(subreg['url'], hash_value=subreg['hash'])
    >>> print(im)

    >>> from timagetk.io.dataset import DATA_REGISTRY
    >>> subreg = DATA_REGISTRY['flower_confocal'][2]
    >>> im = pims_read(subreg['url'], hash_value=subreg['hash'])
    >>> print(im)

    >>> from timagetk.io.dataset import DATA_REGISTRY
    >>> subreg = DATA_REGISTRY['sam_confocal'][1]
    >>> im = pims_read(subreg['url'], hash_value=subreg['hash'])
    >>> print(im)

    """
    import pims

    if isinstance(fname, str):
        # URL checkpoint:
        if fname.startswith('http'):
            return pims_read(_image_from_url(fname, hash_value=kwargs.pop('hash_value', None)), **kwargs)
        else:
            fname = Path(fname)
    fname = fname.expanduser().absolute()
    # Check file exists:
    if not fname.exists():
        log.error(f"Could not find file `{fname}`!")
        return None

    nd_images = pims.Bioformats(fname)
    md = nd_images.metadata

    # Check we do NOT have a time indexed image as input
    shape_dict = nd_images.sizes
    if shape_dict.get('t', 1) > 1:
        log.critical(f"Library not ready for time-indexed images. Sorry...")
        raise sys.exit("Contribute to the library by creating the `TimeSeries` class!")

    # -- CHANNELS --
    # Get the number of channels in image
    n_channels = shape_dict.get('c', 1)
    # Get the channel names if more than one is found
    if n_channels > 1:
        channel_names = kwargs.pop('channel_names', None)
        md_channel_names = [md.ChannelName(0, i_ch) for i_ch in range(n_channels)]
        if channel_names is None:
            channel_names = md_channel_names
            log.debug(f"Using metadata channel names: {', '.join(md_channel_names)}")
        else:
            try:
                assert len(channel_names) == n_channels
            except AssertionError:
                log.error(f"Got {len(channel_names)} manually defined channel names for a {n_channels} image!")
                log.info(f"Using metadata channel names: {', '.join(md_channel_names)}")
                channel_names = md_channel_names
            else:
                log.debug(f"Using manually defined channel names: {', '.join(channel_names)}")
        # Get the channel keyword-argument, later used to return only one channel if defined
        single_channel = kwargs.pop('channel', None)
        if single_channel is not None:
            try:
                single_channel_idx = channel_names.index(single_channel)
            except ValueError:
                log.critical(
                    f"Given channel name '{single_channel}' is not in the list of channel names: {', '.join(channel_names)}")
            else:
                log.debug(f"Extracting a single channel named '{single_channel}' out of {n_channels} channels.")

    # -- VOXELSIZE --
    # Get the ZYX image voxelsize
    xy_vxs = nd_images.calibration
    z_vxs = nd_images.calibrationZ
    voxelsize = [z_vxs, xy_vxs, xy_vxs]
    log.debug(f"Found ZYX image voxelsize: {voxelsize}")

    # Extract other metadata
    # To list valid metadata fields : `nd_images.metadata.fields`
    # x_vxs = md.PixelsPhysicalSizeX(0)
    # y_vxs = md.PixelsPhysicalSizeY(0)
    # z_vxs = md.PixelsPhysicalSizeZ(0)
    # vxs = [z_vxs, y_vxs, x_vxs]
    filename = md.ImageName(0)

    # Get a 3D stack as ZYX arrays
    def _zyx_image(zyx_image, voxelsize, filename):
        axes_order = 'zyx'
        zyx_image.bundle_axes = axes_order
        # zyx_image.iter_axes = 'z'
        zyx_arr = np.squeeze(np.array(zyx_image))
        return SpatialImage(zyx_arr, origin=[0, 0, 0], voxelsize=voxelsize,
                            axes_order=axes_order, unit=1e-06, filename=filename)

    if n_channels > 1:
        nd_images.iter_axes = 'c'
        if single_channel is not None:
            image = _zyx_image(nd_images[single_channel_idx], voxelsize, filename)
        else:
            # Multichannel case
            images = {}
            for i_ch, nd2_image in enumerate(nd_images):
                if channel_names is None:
                    channel_name = md.ChannelName(0, i_ch)
                else:
                    channel_name = channel_names[i_ch]
                images[channel_name] = _zyx_image(nd2_image, voxelsize, None)
            image = MultiChannelImage(images, filename=filename)
    else:
        # SpatialImage case
        image = _zyx_image(nd_images, voxelsize, filename)

    rtype = kwargs.pop('rtype', None)
    if rtype is not None:
        return rtype(image, **kwargs)
    else:
        return image
