#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""This module introduces miscellaneous methods used by the IO module."""

from pathlib import Path

from timagetk.bin.logger import get_logger

log = get_logger(__name__)

INR_EXT = ['.inr', '.inr.gz', '.inr.zip']
LSM_EXT = ['.lsm']
MHA_EXT = ['.mha', '.mha.gz', '.mha.zip']
TIF_EXT = ['.tif', '.tiff']
CZI_EXT = [".czi"]
ND2_EXT = [".nd2"]
#: List of known image file formats.
POSS_EXT = INR_EXT + LSM_EXT + MHA_EXT + TIF_EXT + CZI_EXT + ND2_EXT

UNKNOWN_EXT_MSG = "Unknown file extension '{}'!"
UNKNOWN_EXT_MSG += f"Valid formats are: {POSS_EXT}."


def get_format(filename):
    """Get the image file format from the filename.

    Parameters
    ----------
    filename : str
        Name of the file

    Returns
    -------
    str
        The name of the format

    Raises
    ------
    NotImplementedError
        If the format is unknown to the readers

    """
    # Get file path, name and extension:
    filepath, shortname, ext = get_pne(filename, test_format=True)

    return ext


def is_known_format(ext):
    """Test if the given extension of file format is known.

    Parameters
    ----------
    ext : str
        Extension or file format.

    Returns
    -------
    bool
        ``True`` if the format is known, else ``False``

    """
    if not ext.startswith('.'):
        ext = '.' + ext

    return ext in POSS_EXT


def assert_format(ext):
    """Assert if we have a known format, else raises an error message.

    Parameters
    ----------
    ext : str
        Extension or file format.

    Raises
    ------
    NotImplementedError
        If the format is unknown to the readers.

    """
    try:
        assert is_known_format(ext)
    except AssertionError:
        raise NotImplementedError(UNKNOWN_EXT_MSG.format(ext))

    return


def get_pne(location, test_format=True):
    """Get the path, name and extension of an image file based on its location.

    Parameters
    ----------
    location : str or pathlib.Path
        Location of the file.
    test_format : bool, optional
        Assert the format of the file is a known image format. Defaults to ``True``.

    See Also
    --------
    timagetk.io.assert_format

    Returns
    -------
    str
        The path to the file, if any.
    str
        The name of the file, with extension (if any).
    str
        The extension of the file, if any.

    Examples
    --------
    >>> from timagetk.io.util import get_pne
    >>> path = "/Data/p194/watershed_segmentation/p194-t1.inr.gz"
    >>> get_pne(path)
    ('/Data/p194/watershed_segmentation', 'p194-t1.inr.gz', '.inr.gz')
    >>> get_pne("filename")
    ('', 'filename', '')
    """
    # Check if the location is a string and convert it to a Path object if necessary
    if isinstance(location, str):
        location = Path(location)

    # Handle case where the file has a parent directory
    if location.parent != Path('.'):
        location = location.expanduser()  # Expand any `~` to user's home directory
        filepath = str(location.parent)  # Get the string representation of the parent directory
    else:
        filepath = ''  # Empty path for files without a directory (in the current directory)

    # Extract the filename and list of extensions from the Path object
    filename = location.name  # Name of the file (e.g., 'file.ext')
    ext = location.suffixes  # List of all suffixes/extensions (e.g., ['.tar', '.gz'])

    # Warn the user if there are more than two extensions, and they are not valid compressed archives
    if len(ext) > 2 and ext[-1] not in ['.gz', '.zip']:
        log.warning(f"Too many dots ({len(ext)}) in the filename: '{filename}'")
        log.info("Dots should only be used to indicate the file extension, not within the name!")

    # Handle cases where the file has a valid extension
    if len(ext) != 0:
        # Check if the file might be compressed (e.g., .gz, .zip)
        if ext[-1] in ['.gz', '.zip']:
            compress_ext = ext[-1]  # Compression extension (e.g., '.gz')
            format_ext = ext[-2]  # Format extension (e.g., '.tar')
            ext = f"{format_ext}{compress_ext}"  # Combine into a single extension (e.g., '.tar.gz')
        else:
            format_ext = ext[-1]  # Set the main format extension (e.g., '.png', '.txt')
            ext = format_ext  # Use the format as the main extension

        # Validate the file format if `test_format` is `True`
        if test_format:
            assert_format(format_ext)  # Ensure the format matches expected image formats
    else:
        ext = ''

    # Return the directory path, filename, and parsed extension
    return filepath, filename, ext


def assert_exists(filename):
    """Assert if the given filename or path exists, else raise an Error.

    Parameters
    ----------
    filename : str or pathlib.Path
        A filename or path to assert
    """
    try:
        assert Path(filename).exists()
    except AssertionError:
        raise IOError(f"This file does not exists: {filename}")
    return


def all_in_same_folder(file_list):
    """Test if all given files in list are in the same directory.

    Parameters
    ----------
    file_list : list[str] or list[pathlib.Path]
        List of file paths to check.

    Returns
    -------
    bool
        ``True`` if all paths have common root, else ``False``.

    Examples
    --------
    >>> from timagetk.io.util import all_in_same_folder
    >>> file1 = '/home/test/project/file1.py'
    >>> file2 = '/home/test/project/file2.py'
    >>> file3 = '/home/test/file3.py'
    >>> all_in_same_folder([file1, file2])
    True
    >>> all_in_same_folder([file1, file3])
    False
    >>> all_in_same_folder([file1, file2, file3])
    False
    """
    file_folders = [Path(p).parent for p in file_list]
    if all(file_folders[0] == folder for folder in file_folders[1:]):
        return True
    else:
        return False


def assert_all_files(files):
    """Assert all files exists.

    Parameters
    ----------
    files : list[str] or list[pathlib.Path]
        A list of file names.

    """
    test = []
    for f in files:
        try:
            test.append(Path(f).is_file())
        except:
            test.append(False)
    try:
        assert all(test)
    except AssertionError:
        raise ValueError("Given file list contains missing files!")
    return


def guess_delimiter(filename, delimiters='_|-| '):
    """Guess the most likely delimiter from a given filename based on the specified delimiters.

    This function counts the occurrences of each delimiter in the filename
    and returns the delimiter that appears most frequently.

    Parameters
    ----------
    filename : str
        The name of the file or a string on which the delimiter counting will be performed.
    delimiters : str, optional
        A string containing potential delimiters separated by the `|` character.
        The function will split this string and use each delimiter for counting
        its occurrences in `filename`. Defaults to ``'_|-| '``.

    Returns
    -------
    str
        The delimiter that occurs most frequently in the `filename`.

    Examples
    --------
    >>> from timagetk.io.util import guess_delimiter
    >>> from timagetk.io.dataset import shared_data_path
    >>> path = shared_data_path('flower_multiangle', 0)
    >>> print(path)
    '$HOME/.cache/timagetk/090223-p58-flo-top.lsm'
    >>> guess_delimiter(path)
    '-'
    """
    delimiter_count = {}
    for d in delimiters.split('|'):
        delimiter_count[d] = filename.count(d)
    return max(delimiter_count, key=delimiter_count.get)


def find_common_segments(filenames, delimiter='_|-| '):
    """Finds the common segments among a list of filenames, after removing their directory & extensions.

    The function works iteratively to determine the common segments of all specified filenames.
    The comparison is case-sensitive.

    Parameters
    ----------
    filenames : list[str]
        A list of filenames to process. Path and file extensions are removed.
    delimiter : str, optional
        A string containing the delimiters to use for splitting the filenames.
        A list of potential delimiters separated by the `|` character can be specified.
        The function will guess the most likely delimiter in this case. Defaults to ``'_|-| '``

    Returns
    -------
    str
        The common segments shared among all filenames in the list.
        Returns an empty string if the list of cleaned up filename is empty.

    See Also
    --------
    timagetk.io.util.guess_delimiter : Guess the most likely delimiter from a given filename based on the specified delimiters.
    timagetk.io.util.get_pne : Get the path, name and extension of an image file based on its location.

    Examples
    --------
    >>> from timagetk.io.util import find_common_segments
    >>> from timagetk.io.dataset import shared_data_path
    >>> path = shared_data_path('flower_multiangle')
    >>> print(path)
    {0: '$HOME/.cache/timagetk/090223-p58-flo-top.lsm', 1: '$HOME/.cache/timagetk/090223-p58-flo-tilt1.lsm', 2: '$HOME/.cache/timagetk/090223-p58-flo-tilt2.lsm'}
    >>> find_common_segments(path.values())
    '090223-p58-flo'
    >>> path = shared_data_path('flower_confocal')
    >>> print(path)
    {0: '$HOME/.cache/timagetk/p58-t0-imgFus.inr.gz', 1: '$HOME/.cache/timagetk/p58-t1-imgFus.inr.gz', 2: '$HOME/.cache/timagetk/p58-t2-imgFus.inr.gz'}
    >>> find_common_segments(path.values())
    'p58-imgFus'
    """
    # Remove directory & extensions from the filenames
    base_names = []
    for name in filenames:
        _, fname, ext = get_pne(name)
        base_names.append(fname.replace(ext, ''))

    # Returns an empty string if the list of cleaned up filename is empty
    if not base_names:
        return ""

    # Try to guess the delimiter if it is not unique
    if "|" in delimiter:
        delimiter = guess_delimiter(base_names[0], delimiters=delimiter)

    # Split filenames into parts based on the delimiter
    split_names = [name.split(delimiter) for name in base_names]

    # Determine the common parts across all filenames
    common_parts = []
    for parts in zip(*split_names):  # Transpose the list of lists
        if len(set(parts)) == 1:  # Check if all parts are the same
            common_parts.append(parts[0])

    return delimiter.join(common_parts)
