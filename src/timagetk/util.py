#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""This module regroups various utilities."""

import re
import time

import numpy as np

from timagetk.bin.logger import get_logger

try:
    from skimage.util.dtype import dtype_range
except ImportError:
    msg = "Could not import `scikit-image` package, please install it!"
    msg += "\n"
    msg += "Use `conda install scikit-image` or `pip install scikit-image -U`."
    raise ImportError(msg)

log = get_logger(__name__)

INPUT_TYPE_ERROR = "Input '{}' should be a ``{}`` instance, got: {}"
DEC_VAL = 6


def same_type(input_list, input_type=None):
    """Test if all elements of a list are of the same type.

    Parameters
    ----------
    input_list : list(any)
        List of instance to check
    input_type : type, optional
        If defined, should be the type of instance to find in the list

    Returns
    -------
    bool
        ``True`` if all the same, else ``False``.

    """
    if input_type is None:
        input_type = type(input_list[0])

    return all(isinstance(elem, input_type) for elem in input_list)


def check_type(obj, obj_name, obj_type):
    """Check ``obj`` is the right instance against ``obj_type``.

    Parameters
    ----------
    obj : any
        An instance to test against ``obj_type``
    obj_name : str
        Name of the obj, use for printing
    obj_type : any|list(any)
        Type of instance that ``obj`` should match, can also be a list of types

    Raises
    ------
    TypeError
        Standard error message for input type
    """
    if isinstance(obj_type, list) and len(obj_type) >= 2:
        try:
            assert True in [isinstance(obj, ot) for ot in obj_type]
        except AssertionError:
            raise TypeError(INPUT_TYPE_ERROR.format(obj_name, obj_type, type(obj)))
    else:
        try:
            assert isinstance(obj, obj_type)
        except AssertionError:
            raise TypeError(INPUT_TYPE_ERROR.format(obj_name, obj_type, type(obj)))


def stuple(t):
    """Sort given iterable by values.

    Parameters
    ----------
    t : Iterable
        The iterable to sort.

    Returns
    -------
    tuple
        A value sorted tuple.

    Examples
    --------
    >>> from timagetk.util import stuple
    >>> t = (5, 2, 6)
    >>> stuple(t)
    (2, 5, 6)
    >>> t = [5, 2, 6]
    >>> stuple(t)
    (2, 5, 6)
    >>> t = "BAC"
    >>> stuple(t)
    ('A', 'B', 'C')

    """
    return tuple(sorted(t))


def elapsed_time(start, stop=None, round_to=3):
    """Return a rounded elapsed time float.

    Parameters
    ----------
    start : float
        Start time
    stop : float, optional
        Stop time, if ``None``, get it now
    round_to : int, optional
        Number of digits to display after comma.

    Returns
    -------
    float
        Rounded elapsed time

    Examples
    --------
    >>> import time
    >>> from timagetk.util import elapsed_time
    >>> t_start = time.time()
    >>> time.sleep(2)
    >>> elapsed_time(t_start)
    'done in 2.001s'

    """
    if stop is None:
        stop = time.time()
    t = round(stop - start, round_to)

    return "done in {}s".format(t)


def clean_type(obj):
    """Get a clean string of ``obj`` type.

    Parameters
    ----------
    obj : any
        Any object for which you want to get the class name.

    Returns
    -------
    str
        The `obj` class name as a string.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.util import clean_type
    >>> from timagetk.array_util import random_spatial_image
    >>> # Example 1 - Get the class name from a random ``numpy.ndarray``:
    >>> arr = np.random.randint(low=0, high=255, size=(10, 10), dtype='uint8')
    >>> print(clean_type(arr))
    ndarray
    >>> # Example 2 - Get the class name from a random ``timagetk.SpatialImage``:
    >>> img = random_spatial_image(size=(10, 10))
    >>> print(clean_type(img))
    SpatialImage

    """
    s = str(type(obj))
    if s.startswith("<type"):
        regexp = "<type \'(.+)\'>"
        m = re.search(regexp, s)
        try:
            ct = m.group(1)
        except AttributeError:
            msg = "Could not find regex '{}' in '{}'!"
            raise ValueError(msg.format(regexp, s))
    else:
        regexp = "<class \'(.+)\'>"
        m = re.search(regexp, s)
        try:
            ct = m.group(1).split(".")[-1]
        except AttributeError:
            msg = "Could not find regex '{}' in '{}'!"
            raise ValueError(msg.format(regexp, s))
    return ct


def get_class_name(obj):
    """Return a string defining the class name without module & package hierarchy returned.

    Parameters
    ----------
    obj : any
        Any object for which you want to get the class name.

    Returns
    -------
    str
        The `obj` class name as a string.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.util import get_class_name
    >>> from timagetk.array_util import random_spatial_image
    >>> # Example 1 - Get the class name from a random ``numpy.ndarray``:
    >>> arr = np.random.randint(low=0, high=255, size=(10, 10), dtype='uint8')
    >>> print(get_class_name(arr))
    ndarray
    >>> # Example 2 - Get the class name from a random ``timagetk.SpatialImage``:
    >>> img = random_spatial_image(size=(10, 10))
    >>> print(get_class_name(img))
    SpatialImage

    """
    return str(type(obj))[:-2].split('.')[-1]


def get_attributes(obj, attr_list):
    """Return a dictionary of attribute values from `obj`.

    Parameters
    ----------
    obj : any
        An object from which to retrieve attributes.
    attr_list : list[str]
        A list of attribute names to retrieve from the object.

    Returns
    -------
    dict
        A dictionary indexed by attribute names, containing their values.

    Notes
    -----
    Attributes that are not defined in the object will have a default value of ``None``.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.util import get_attributes
    >>> arr = np.random.randint(low=0, high=255, size=(10, 10), dtype='uint8')
    >>> get_attributes(arr, ['ndim', 'shape', 'extent'])
    {'ndim': 2, 'shape': (10, 10), 'extent': None}

    """
    return {attr: getattr(obj, attr, None) for attr in attr_list}


# TODO: write a ``_connexity_check`` function working the same way than ``_method_check`` ?

def _method_check(method, valid_methods, default_method):
    """Check whether a given method is valid, or return the default method if it is ``None``.

    Parameters
    ----------
    method : str or None
        The name of the method to check. Returns the default method if ``None`` is provided.
    valid_methods : list[str]
        A list containing valid methods.
    default_method : str
        The default method to use, which must be included in the list of valid methods.

    Returns
    -------
    str
        A valid method name.

    Raises
    ------
    ValueError
        Raised if ``method`` is not within ``valid_methods``, unless it is ``None``.
    """
    if method is None:
        # - Set the method to its default values if `None`:
        method = default_method
    else:
        # - Test the given method is valid:
        try:
            assert method in valid_methods
        except AssertionError:
            msg = f"Unknown method '{method}', available methods are: {valid_methods}"
            raise ValueError(msg)

    return method


def around_list(input_list, dec_val=DEC_VAL):
    """Round values in a list to a specified number of decimal places.

    Parameters
    ----------
    input_list : list
        A list containing the values to be rounded.
    dec_val : int
        The number of decimal places to round each value to.

    Returns
    -------
    list
        A list containing the values rounded to the specified number of decimal places.
    """
    return np.around(input_list, decimals=dec_val).tolist()


def _to_list(val):
    """Return a list from tuple or array, else raise a TypeError."""
    if isinstance(val, np.ndarray):
        val = val.tolist()
    if isinstance(val, tuple):
        val = list(val)
    if not isinstance(val, list):
        raise TypeError(f"Accepted type are tuple, list and np.array, got: {type(val)}")
    else:
        return val


def compute_shape(voxelsize, extent):
    """Compute new shape of object from extent and voxel-size.

    Parameters
    ----------
    voxelsize : list[float]
        Voxel-size of the image.
    extent : list[float]
        Extent of the image, *i.e.* its real size.

    Returns
    -------
    list
        Shape of the image

    Examples
    --------
    >>> from timagetk.util import compute_shape
    >>> compute_shape([0.5, 0.5], [10, 10])
    [20, 20]

    """
    extent = np.array(extent)
    voxelsize = np.array(voxelsize, dtype=float)
    return tuple(map(int, np.around(np.divide(extent, voxelsize), 0)))


def compute_extent(voxelsize, shape):
    """Compute image extent from shape and voxel-size.

    Parameters
    ----------
    voxelsize : list[float]
        Voxel-size of the image.
    shape : list[int]
        Shape of the image, *i.e.* its size in voxel.

    Returns
    -------
    list
        Extent of the image.

    Examples
    --------
    >>> from timagetk.util import compute_extent
    >>> compute_extent([0.5, 0.5], (10, 10))
    [4.5, 4.5]

    """
    shape = np.array(shape) - 1
    voxelsize = np.array(voxelsize, dtype=float)
    return np.multiply(shape, voxelsize).tolist()


def compute_outer_extent(voxelsize, shape):
    """Compute outer image extent from shape and voxel-size.

    Parameters
    ----------
    voxelsize : list[float]
        Voxel-size of the image
    shape : list[int]
        Shape of the image, *i.e.* its outer size in voxel

    Returns
    -------
    list
        Outer extent of the image

    Examples
    --------
    >>> from timagetk.util import compute_outer_extent
    >>> compute_outer_extent([0.5, 0.5],(10, 10))
    [5., 5.]

    """
    shape = np.array(shape)
    voxelsize = np.array(voxelsize, dtype=float)
    return np.multiply(shape, voxelsize).tolist()


def compute_voxelsize(shape, extent):
    """Compute new voxel-size of object from shape and get_extent.

    Parameters
    ----------
    shape : list[int]
        Shape of the image, *i.e.* its size in voxel.
    extent : list[float]
        Extent of the image, *i.e.* its real size.

    Returns
    -------
    list[float]
        Extent of the image.

    Examples
    --------
    >>> from timagetk.util import compute_voxelsize
    >>> compute_voxelsize((10, 10),[15., 15.])
    [5., 5.]

    """
    shape = np.array(shape)
    extent = np.array(extent, dtype=float)
    return np.divide(extent, shape).tolist()


def dimensionality_test(dim, list2test):
    """Quick testing of dimensionality with print in case of error."""
    d = len(list2test)
    try:
        assert d == dim
    except AssertionError:
        raise ValueError(f"Provided values ({d}) is not of the same than the array ({dim})!")


def check_dimensionality(dim, list2test):
    """Test list dimensionality against array dimensionality."""
    try:
        dimensionality_test(dim, list2test)
    except ValueError:
        return None
    else:
        return list2test


def type_to_range(image):
    """Return the minimum and maximum values of an `image` based on its ``dtype``.

    Parameters
    ----------
    image : numpy.ndarray or timagetk.SpatialImage
        The image from which to extract the slice.

    Returns
    -------
    int
        The minimum value associated with the image type.
    int
        The maximum value associated with the image type.
    """
    try:
        assert hasattr(image, 'dtype')
    except AssertionError:
        raise ValueError("Input 'image' has no attribute 'dtype'!")

    return dtype_range[image.dtype.type]


DATE_FMT = "%Y-%m-%d_%H:%M:%S"


def now(to_str=True, fmt=DATE_FMT):
    from datetime import datetime
    now = datetime.now()
    if to_str:
        return now.strftime(fmt)
    else:
        return now


def yn_query(msg, default='n'):
    """Prompt a yes/no query to the user.

    Examples
    --------
    >>> from timagetk.util import yn_query
    >>> yn_query("Is the earth flat?")
    Is the earth flat? [y/N]>?
    False

    """
    VALID = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}

    def confirm(query, default='n'):
        if query == '':
            return VALID[default]
        else:
            return VALID[query]

    if VALID[default]:
        msg += " [Y/n]"
    else:
        msg += " [y/N]"
    return confirm(input(msg).lower(), default=default)


def auto_format_bytes(size_bytes, unit='octets'):
    """Auto format bytes size.

    Parameters
    ----------
    size_bytes : int
        The size in bytes to convert.
    unit : {'Bytes', 'octets'}
        The type of units you want.

    Examples
    --------
    >>> from timagetk.util import auto_format_bytes
    >>> auto_format_bytes(1024)
    '1.0 Ko'
    >>> auto_format_bytes(300000)
    '292.97 Ko'
    >>> auto_format_bytes(300000, 'Bytes')
    '292.97 KB'

    """
    import math
    if unit.lower() == 'bytes':
        size_name = ("Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    else:
        size_name = ("octets", "Ko", "Mo", "Go", "To", "Po", "Eo", "Zo", "Yo")
    if size_bytes == 0:
        return f"0{size_name[0]}"
    # Auto formatting:
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)
    return f"{s} {size_name[i]}"


def _not_nan(value):
    """Test if the value is NOT ``np.nan``."""
    return not np.isnan(value)


def _not_none(value):
    """Test if the value is NOT ``None``."""
    return value is not None


def not_default_test(default):
    """Return the test to perform according to the default value.

    Parameters
    ----------
    default : any
        The default value to test against.

    Returns
    -------
    func
        The function to use to test against this `default` value.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.util import not_default_test
    >>> is_not_default = not_default_test(None)
    >>> is_not_default(1)
    True
    >>> is_not_default(np.nan)
    True
    >>> is_not_default(None)
    False
    >>> is_not_default = not_default_test("None")
    >>> is_not_default(None)
    True
    >>> is_not_default("None")
    False

    """
    _not_value = lambda value: value != default

    if default is None:
        return _not_none
    elif isinstance(default, str):
        return _not_value
    elif np.isnan(default):
        return _not_nan
    else:
        return _not_value
