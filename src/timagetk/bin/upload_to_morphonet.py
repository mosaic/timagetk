#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
from os.path import join
from os.path import split

from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.tasks.json_parser import DEFAULT_OBJ_DATASET
from timagetk.tasks.json_parser import json_intensity_image_parsing
from timagetk.tasks.json_parser import json_mesh_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.json_parser import json_time_metadata
from timagetk.third_party.morphonet import TYPE_MAPPING
from timagetk.third_party.morphonet import _test_morphonet
from timagetk.third_party.morphonet import upload_to_morphonet

_test_morphonet()

DESCRIPTION = """Upload meshes and RAW intensity images to MorphoNet.
"""

DEFAULT_RAW_DATASET = "raw"


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION,
                                     epilog="This requires a valid MorphoNet account!")

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('user', type=str,
                        help="morphoNet user name.")
    parser.add_argument('--obj', type=str, default=DEFAULT_OBJ_DATASET,
                        help=f"the name of the dataset containing the mesh to upload, '{DEFAULT_OBJ_DATASET}' by default.")
    parser.add_argument('--raw', type=str, default=DEFAULT_RAW_DATASET,
                        help=f"the name of the dataset containing the raw intensity images to upload, '{DEFAULT_RAW_DATASET}' by default.")

    mn_arg = parser.add_argument_group('MorphoNet arguments')
    mn_arg.add_argument('--type', type=str, default="observed", choices=list(TYPE_MAPPING.keys()),
                        help="type of data, defaults to 'observed'.")
    mn_arg.add_argument('--password', type=str, default="",
                        help="morphoNet password, by default a prompt will ask for it!")
    mn_arg.add_argument('--upload_raw', action="store_true",
                        help="use this to upload intensity images used to segment the images.")
    mn_arg.add_argument('--dataset-name', type=str, default="",
                        help="MorphoNet dataset name. Default is the name of the json dataset.")
    mn_arg.add_argument('--force', action="store_true",
                        help="override any existing dataset.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default.")

    return parser


def main(args):
    # Get the name of the series
    series_md = json_series_metadata(args.json)
    xp_id = series_md['series_name']
    # Defines some location variables:
    json_root_path, _ = split(args.json)  # the root directory is where the JSON file is...

    # Initialize logger
    logger = get_logger('upload_to_morphonet', join(json_root_path, 'upload_to_morphonet.log'), args.log_level.upper())

    # Get the list of meshes per time-point
    obj_mesh, obj_info = json_mesh_parsing(args.json, args.obj)
    n_objs = len(obj_mesh)
    logger.info(f"Found {n_objs} meshes under '{args.obj}'.")
    logger.info(f"Found {len(obj_info)} info files under '{args.obj}'.")
    logger.debug(obj_mesh)
    logger.debug(obj_info)

    time_points = json_time_metadata(args.json)['points']
    n_tp = len(time_points)
    logger.info(f"Found {n_tp} time-points indexed.")
    logger.debug(time_points)

    try:
        assert n_objs == n_tp
    except AssertionError:
        raise ValueError(f"Not the same number of time-points ({n_tp}) and meshes ({n_objs})!")

    intensity_img_files = {}
    if args.upload_raw:
        # Get the list of intensity images per time-point
        intensity_img_files = json_intensity_image_parsing(args.json, args.raw)
        n_imgs = len(intensity_img_files)
        logger.info(f"Found {n_imgs} intensity images under '{args.raw}'.")
        logger.debug(intensity_img_files)

        try:
            assert n_objs == n_imgs
        except AssertionError:
            raise ValueError(
                f"Not the same number of time-points ({n_tp}), meshes ({n_objs}) and intensity images ({n_imgs})!")

    kwargs = {}
    if args.password != "":
        kwargs['password'] = args.password
    kwargs['force'] = getattr(args, 'force', False)
    kwargs['id_type'] = TYPE_MAPPING[getattr(args, 'type', "observed")]
    try:
        kwargs['id_NCBI'] = int(series_md.get("NCBI id", 0))
    except:
        pass

    # Set the dataset name if given. Else, use the series name from the json file.
    dataset_name = args.dataset_name if args.dataset_name != "" else xp_id

    upload_to_morphonet(dataset_name, args.user, obj_mesh, obj_info, intensity_img_files, **kwargs)


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
