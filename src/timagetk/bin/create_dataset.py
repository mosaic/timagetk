#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import argparse
from timagetk.gui.dataset_creation import DatasetCreationManager

DESCRIPTION = """
DatasetCreationManager is the main application window for the TimageTK - Dataset Creation Tool.

This application provides a complete graphical interface to import, manage, and export image datasets.
It allows users to:

  - Import individual image files or entire folders.
  - Dynamically arrange images on a grid, ensuring a minimum number of rows and columns.
  - View image metadata and a larger preview of the selected image.
  - Drag and drop images to rearrange their positions, with visual feedback and smart grid
    adjustments (including automatic row/column addition or removal).
  - Remove individual images or clear the entire grid.
  - Export the assembled dataset using a separate ExportManager, which organizes the output
    into a predefined folder structure.
"""

def get_parser() -> argparse.ArgumentParser:
    """Configure and return the argument parser for DatasetCreationManager."""
    parser = argparse.ArgumentParser(
        description=DESCRIPTION,
        formatter_class=argparse.RawDescriptionHelpFormatter
    )

    return parser

def run():
    parser = get_parser()
    args = parser.parse_args()

    manager = DatasetCreationManager()
    manager.run()

if __name__ == '__main__':
    run()


