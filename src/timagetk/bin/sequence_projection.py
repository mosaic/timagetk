#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Generate 2D projections figures of a time-series."""

import argparse
import sys
import time
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

from timagetk import LabelledImage
from timagetk.algorithms.resample import isometric_resampling
from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.io import imread
from timagetk.tasks.json_parser import DEFAULT_INTENSITY_DATASET
from timagetk.tasks.json_parser import DEFAULT_SEGMENTATION_DATASET
from timagetk.tasks.json_parser import json_channel_names
from timagetk.tasks.json_parser import json_intensity_image_parsing
from timagetk.tasks.json_parser import json_metadata
from timagetk.tasks.json_parser import json_segmentation_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.json_parser import json_threshold_metadata
from timagetk.tasks.json_parser import json_time_metadata
from timagetk.tasks.json_parser import time_intervals
from timagetk.util import elapsed_time
from timagetk.visu.mplt import grayscale_imshow
from timagetk.visu.mplt import image_plot
from timagetk.visu.mplt import plot_img_and_hist
from timagetk.visu.projection import PROJECTION_METHODS

OUT_DATASET = DEFAULT_INTENSITY_DATASET

DESCRIPTION = """Generate 2D projections figures of a time-series."""
EPILOG = """Notes that the `background` method can only be used if you already have the corresponding segmentations."""

cli_name = Path(__file__).stem


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION, epilog=EPILOG)

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")

    ts_arg = parser.add_argument_group('time-series options')
    ts_arg.add_argument('-int', '--intensity_dataset', type=str, default=DEFAULT_INTENSITY_DATASET,
                        help=f"name of the dataset containing the intensity images, '{DEFAULT_INTENSITY_DATASET}' by default.")
    ts_arg.add_argument('-seg', '--segmentation_dataset', type=str, default=DEFAULT_SEGMENTATION_DATASET,
                        help=f"the dataset containing the segmented image on which to project the dataset, '{DEFAULT_SEGMENTATION_DATASET}' by default.")
    ts_arg.add_argument('-index', '--time_index', type=int, nargs='+', default=None,
                        help="select a subset of images, given as a list of time-indexes, all of them by default.")

    img_arg = parser.add_argument_group('images arguments')
    img_arg.add_argument('--channel', type=str, default=None,
                         help="channel to project, mandatory for multichannel images.")
    img_arg.add_argument('--orientation', type=int, default=1, choices={-1, 1},
                         help="image orientation, use `-1` with an inverted microscope, `1` by default.")
    img_arg.add_argument('--iso_vxs', type=float, default=None,
                         help="performs isometric resampling, `None` by default.")

    proj_arg = parser.add_argument_group('projection arguments')
    proj_arg.add_argument('--method', type=str, default='contour', choices=PROJECTION_METHODS,
                          help="projection method to use, 'contour' (default) works well with cell walls/membranes and 'maximum' with nuclei.")
    proj_arg.add_argument('--threshold', type=float, nargs='+', default=None, choices=PROJECTION_METHODS,
                          help="threshold to use with 'contour' projection method.")
    proj_arg.add_argument('--gaussian_sigma', type=float, default=None,
                          help="gaussian sigma to apply to the intensity images prior to 'contour' projection method.")
    proj_arg.add_argument('--altimap_smoothing', action='store_true',
                          help="use this to smooth the altitude map prior to contour projection.")
    proj_arg.add_argument('--no_iso_upsampling', action='store_false',
                          help="use this to avoid isometric resampling of the image prior to contour projection.")

    fig_arg = parser.add_argument_group('figure arguments')
    fig_arg.add_argument('--colormap', type=str, default='gray',
                         help="the colormap to use for the figures.")
    fig_arg.add_argument('--vignettes', action='store_true',
                         help="use this to generate vignettes, that is a snapshot per images.")
    fig_arg.add_argument('--histogram', action='store_true',
                         help="use this to generate vignettes with their intensity histogram.")

    out_arg = parser.add_argument_group('output arguments')
    out_arg.add_argument('--out_dataset', type=str, default=None,
                         help="where to export the figures, same as intensity dataset by default.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default.")

    return parser


def run(xp_id, args, logger):
    from timagetk.visu.projection import projection
    time_md = json_time_metadata(args.json)
    tp = time_md['points']
    tu = time_md['unit']

    t_start = time.time()  # start a timer

    if args.channel is not None:
        logger.info(f"Starting '{args.method}' projection of channel '{args.channel}'...")
    else:
        logger.info(f"Starting '{args.method}' projection of intensity images...")

    if args.threshold is None:
        try:
            thresholds = json_threshold_metadata(args.json)
            assert thresholds is not None and len(thresholds) == len(tp)
        except AssertionError:
            args.threshold = {idx: None for idx in args.time_index}
        else:
            args.threshold = {idx: thresholds[idx] for idx in args.time_index}
            logger.info(f"Got thresholds from JSON: {args.threshold}")
    elif len(args.threshold) == 1:
        args.threshold = {idx: args.threshold for idx in args.time_index}
    else:
        pass

    # -- Starts with the projection of the images:
    proj_imgs = {}
    logger.info(f"Generating {args.method} projections...")
    for idx in tqdm(args.time_index, unit='time-point'):
        if args.method == 'background':
            seg_img = imread(args.segmented_imgs[idx], LabelledImage, not_a_label=0)
            # Perform isometric resampling of the segmentation image if requested:
            if args.iso_vxs is not None:
                seg_img = isometric_resampling(seg_img, args.iso_vxs, interpolation='cell-based', cell_based_sigma=2)
        else:
            seg_img = None

        def _projection(int_img, threshold):
            if args.iso_vxs is not None:
                int_img = isometric_resampling(int_img, args.iso_vxs, interpolation='linear')
            proj_img = projection(int_img, segmentation=seg_img,
                                  method=args.method, orientation=args.orientation, threshold=threshold,
                                  gaussian_sigma=args.gaussian_sigma, altimap_smoothing=args.altimap_smoothing,
                                  iso_upsampling=args.no_iso_upsampling)
            return proj_img

        def _save_vignette(proj_img, proj_idx):
            logger.info(f"Generating and saving vignettes under '{args.intensity_dataset}'...")
            figname = f"{xp_id}-t{proj_idx}-top_view_projections-{args.method}.png"
            fig_path = args.abs_out_path.joinpath(figname)
            projection_vignette(proj_img, fig_path, colormap=args.colormap)

        if isinstance(args.intensity_imgs[idx], dict):
            if isinstance(args.threshold[idx], (int, float)):
                args.threshold[idx] = [args.threshold[idx]] * len(args.intensity_imgs[idx])
            # multi-angle views:
            for view_idx, img in args.intensity_imgs[idx].items():
                int_img = imread(img, channel_names=args.channel_names, channel=args.channel)
                proj_idx = f"{idx}-{view_idx}"
                proj_imgs[proj_idx] = _projection(int_img, threshold=args.threshold[idx][view_idx])
                if args.vignettes:
                    _save_vignette(proj_imgs[proj_idx], proj_idx)
        else:
            int_img = imread(args.intensity_imgs[idx], channel=args.channel)
            proj_idx = f"{idx}"
            proj_imgs[proj_idx] = _projection(int_img, threshold=args.threshold[idx])
            if args.vignettes:
                _save_vignette(proj_imgs[proj_idx], proj_idx)

    if args.histogram:
        logger.info(f"Generating and saving vignette and histogram figures under '{args.abs_out_path}'...")
        # -- Create the figures:
        # - Generate a figure for each selected time-point:
        for idx, proj_img in proj_imgs.items():
            figname = f"{xp_id}-t{idx}-projection_histogram-{args.method}.png"
            plot_img_and_hist(proj_imgs[idx], data=imread(args.intensity_imgs[idx], channel=args.channel),
                              threshold=args.threshold[idx], cmap=args.colormap,
                              suptitle="Top-view projection & histogram",
                              title=f"{xp_id} - {idx} - {args.method} projection",
                              figname=args.abs_out_path.joinpath(figname), thumb_size=8.)

    logger.info(f"Generating and saving sequence projection figure under '{args.abs_out_path}'...")
    # - Generate a figure for the whole time-series:
    figname = f"{xp_id}-time_series-top_view_projections-{args.method}.png"
    fig = grayscale_imshow(list(proj_imgs.values()), suptitle=f"{xp_id} time-series top-view projections",
                           thumb_size=6., title=[f"t{idx}" for idx in proj_imgs.keys()],
                           cmap=args.colormap, max_per_line=5,
                           figname=args.abs_out_path.joinpath(figname))

    logger.info(f"All {elapsed_time(t_start)}")
    return


def projection_vignette(proj_img, fig_path, colormap=None):
    """Generate and save a vignette plot of a projection image.

    Parameters
    ----------
    proj_img : object
        A projection image to be plotted. The input is expected to have a
        method `get_extent()` for determining its spatial dimensions.
    fig_path : str
        Path to save the generated vignette plot. This should be a file path in
        string format, including the filename and extension.
    colormap : optional
        Colormap to apply to the projection image. If None, the default colormap
        will be used.
    """
    fig = plt.figure(figsize=np.array(proj_img.get_extent()) / 10.)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    ax, fig_img = image_plot(proj_img, ax, cmap=colormap, val_range='pc-2',
                             no_xtick=True, no_ytick=True, no_xlab=True, no_ylab=True)
    plt.savefig(fig_path)
    del fig, ax, fig_img


def parse_json(args, logger):
    """
    Parses input JSON data and prepares necessary configurations for processing a time-series
    of intensity and segmented images.

    Parameters
    ----------
    args : Namespace
        An object containing user-defined arguments, including paths to the JSON file, output
        directories, datasets, and processing configurations.
    logger : logging.Logger
        Logger instance for recording debug and error messages during the processing routine.

    Returns
    -------
    Namespace
        Returns the updated args object with additional attributes like parsed time-points,
        image dictionaries for intensity and segmentation, output paths, image orientation,
        and colormaps.
    """
    time_md = json_time_metadata(args.json)
    # By default, the figures should be exported to the same dataset as the intensity images:
    if args.out_dataset is None:
        args.out_dataset = args.intensity_dataset
    # Defines absolute path to output directory:
    args.abs_out_path = args.root_path.joinpath(args.out_dataset)  # but needs absolute path to save files
    args.abs_out_path.mkdir(exist_ok=True)  # make sure the directory exists

    # Get the dictionary of intensity images indexed by time-point
    args.intensity_imgs = json_intensity_image_parsing(args.json, args.intensity_dataset)
    n_imgs = len(args.intensity_imgs)
    logger.info(f"Found {n_imgs} intensity images under '{args.intensity_dataset}'.")
    logger.debug(args.intensity_imgs)
    # Check we have at least 3 time-points in this time-series
    try:
        assert n_imgs >= 2
    except AssertionError:
        logger.critical(f"Found less than 2 images ({n_imgs}) in the time series!")
        exit(1)

    # Try to get the channel names from the JSON metadata:
    args.channel_names = json_channel_names(args.json)

    # If the 'background' projection method is requested, make sure we have the segmented images:
    if args.method == 'background':
        # Get the dictionary of segmented images indexed by time-point
        args.segmented_imgs = json_segmentation_parsing(args.json, args.segmentation_dataset)
        logger.info(
            f"Found {len(args.segmented_imgs)} segmented images under '{args.segmentation_dataset}'.")
        logger.debug(args.segmented_imgs)
        # Check there is the same number of intensity and segmented images
        try:
            assert len(args.intensity_imgs) == len(args.segmented_imgs)
        except AssertionError:
            logger.error("There should be the same number of intensity and segmented images!")
            logger.error(f"Found {len(args.intensity_imgs)} intensity images!")
            logger.error(f"Found {len(args.segmented_imgs)} segmented images!")
            sys.exit("The 'background' projection method require the intensity and segmented images dataset!")

    # -- TIME POINTS - Try to get the 'selected' time-points from the JSON metadata:
    if args.time_index is not None:
        wrong_index = [idx for idx in args.time_index if idx not in args.intensity_imgs.keys()]
        if wrong_index != []:
            logger.warning(f"Got some manually defined time-index that do not exists: {wrong_index}")
        args.time_index = [idx for idx in args.time_index if idx in args.intensity_imgs.keys()]
    else:
        all_time_points = time_md.get("points", None)
        sel_time_points = time_md.get("selected", None)
        t_unit = time_md.get("unit", "?")  # TODO: raise something if time unit is not properly defined ?
        if sel_time_points is not None:
            logger.info(f"Got a 'selected' list of {len(sel_time_points)} time-points from JSON: {sel_time_points}")
            args.time_index = [all_time_points.index(tp) for tp in sel_time_points]
            logger.info(f"Corresponding time-indexes: {args.time_index}")
        else:
            sel_time_points = all_time_points
            args.time_index = list(range(0, n_imgs))
        logger.info(f"Corresponding time-intervals (in {t_unit}) from JSON: {time_intervals(sel_time_points)}")

    # Try to get the image 'orientation' from the JSON metadata:
    orientation = json_metadata(args.json).get('orientation', None)
    if orientation is not None:
        args.orientation = orientation
        logger.info(f"Got an 'image orientation' from JSON: `{args.orientation}`")

    if "," in args.colormap:
        from visu_core.matplotlib.colormap import multicolor_gradient_colormap
        # convert the list of color (comma separated color names) to a colormap
        color_list = args.colormap.replace(" ", "")
        color_list = color_list.split(',')
        args.colormap = multicolor_gradient_colormap(color_list)

    return args


def main():
    # Get the parser and parser the CLI arguments:
    parser = parsing()
    args = parser.parse_args()
    # Get the name of the time-series:
    xp_id = json_series_metadata(args.json)['series_name']
    # Get the root directory location to be able to initialize the logger
    args.root_path = Path(args.json).parent.absolute()  # the root directory is where the JSON file is...
    # Initialize logger
    logger = get_logger(cli_name, args.root_path.joinpath(f'{cli_name}.log'), args.log_level.upper())
    # Log first welcome message
    logger.info(f"Running '{cli_name}' for time-series '{xp_id}' located under '{args.root_path}'.")
    # Parse the info from the JSON file
    args = parse_json(args, logger)
    # Run the task:
    run(xp_id, args, logger)


if __name__ == '__main__':
    main()
