#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
import os
import re
import time
from pathlib import Path

from timagetk.algorithms.trsf import apply_trsf
from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.io import imread
from timagetk.io import imsave
from timagetk.io.util import assert_exists
from timagetk.io.util import assert_format
from timagetk.io.util import get_pne
from timagetk.util import elapsed_time
from timagetk.visu.orientation import main_axes_trsf

# META_CHAR = ['.', '^', '$', '*', '+', '?', '{', '}', '[', ']', '\\', '|', '(', ')']
META_CHAR = ['.', '^', '$', '*', '+', '?', '{', '}', '[', ']', '|', '(', ')']

cli_name = Path(__file__).stem


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(
        description='Automatic orientation of object in images.',
        epilog="Orient the object along its main axes.")

    parser.add_argument('images', type=str, nargs='+',
                        help="list of images filename to orient.")

    # optional arguments:
    x_ch = parser.add_argument_group('Extra channels options')
    x_ch.add_argument('--extra_channels', type=str, nargs='+',
                      help="list of extra channels to orient, limit to one image as first argument!.")
    x_ch.add_argument('--regexp_channels', type=int, nargs=1,
                      help="the position of the variable character in file names to define extra-channel.")

    out = parser.add_argument_group('output arguments')
    out.add_argument('--out_suffix', type=str, default="-auto_rotated",
                     help="suffix to add to the file prior to saving.")
    out.add_argument('--format', type=str,
                     help="if specified, may change the output image format.")
    out.add_argument('--output_path', type=str,
                     help="if specified, change the output path.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                          help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default.")

    return parser


def main(args):
    # Initialize logger
    logger = get_logger(cli_name, args.root_path.joinpath(f'{cli_name}.log'), args.log_level.upper())

    # - Variables definition from argument parsing:
    # -- Image list:
    list_img_fnames = args.images
    logger.info("Got {} input images".format(len(list_img_fnames)))

    # - OPTIONAL ARGUMENTS:

    # -- File name suffix:
    out_suffix = args.out_suffix
    logger.info("Got following name suffix: '{}'".format(out_suffix))

    # -- Output format:
    if args.format:
        if not args.format.startswith('.'):
            args.format = '.' + args.format
        assert_format(args.format)

    if args.regexp_channels:
        args.regexp_channels = args.regexp_channels[0]

    # -- Extra channels:
    extra_channels = []
    if args.extra_channels:
        try:
            assert len(list_img_fnames) == 1
        except AssertionError:
            raise ValueError(
                "Only one input image is accepted as first argument with 'extra channels'!")
        for x_ch in args.extra_channels:
            assert_exists(x_ch)
            extra_channels.append(x_ch)

    # -- Output path:
    if args.output_path:
        assert_exists(args.output_path)

    # - Load images:
    for fname in list_img_fnames:
        logger.info("\n# - Loading '{}'...".format(fname))
        img = imread(fname)
        # - Get name and extension:
        _, name, ext = get_pne(img.filename)

        logger.info("\n# - Finding object main axes...")
        t_start = time.time()
        trsf = main_axes_trsf(img)
        # - Rotate the image:
        auto_rotated_img = apply_trsf(img, trsf)
        elapsed_time(t_start)

        # TODO:
        # 1. Use an argument to decide how to 'detect' the object: max-size | intensity threshold
        # 2. Add "image cropping" to redefine the area around the object

        # -- output PATH:
        if args.output_path:
            out_path = args.output_path
        else:
            out_path = img.filepath

        # -- output FORMAT:
        if args.format:
            out_ext = args.format
        else:
            out_ext = ext

        logger.info("\n# - Saving auto-oriented image...")
        imsave(os.path.join(out_path, name + out_suffix + out_ext),
               auto_rotated_img)

        if args.regexp_channels:
            extra_channels = []
            re_pos = args.regexp_channels
            ls = os.listdir(img.filepath)
            pattern = img.filename
            n = len(pattern)
            pattern = pattern[0:re_pos - 1] + '\d' + pattern[re_pos:n]
            for mc in META_CHAR:
                pattern = pattern.replace(mc, '\{}'.format(mc))
            logger.info("\n# - RegExp search for extra channels:")
            logger.info(" - pattern: {}".format(pattern))
            if len(ls) < 10:
                logger.info(" - file list: {}".format(ls))
            for x_fname in ls:
                m = re.match(pattern, x_fname)
                if m and m.group(0) != img.filename:
                    extra_channels.append(
                        os.path.join(img.filepath, m.group(0)))
            logger.info("Found: {}".format(extra_channels))

        if extra_channels:
            for x_ch in extra_channels:
                logger.info("\n# - Extra-channel '{}'...".format(x_ch))
                logger.info("Loading...")
                x_ch = imread(x_ch)
                # - Get name and extension:
                _, name, ext = get_pne(x_ch.filename)
                logger.info("Applying transformation...")
                auto_rotated_img = apply_trsf(x_ch, trsf)
                logger.info("Saving auto-oriented extra channel...")
                imsave(os.path.join(out_path, name + out_suffix + out_ext),
                       auto_rotated_img)


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
