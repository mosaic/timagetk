#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2023 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import argparse
from pathlib import Path

import numpy as np
import pandas as pd
from sklearn.preprocessing import RobustScaler

from timagetk.bin.feature_projection import check_selected_features
from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.features.utils import lof_df
from timagetk.graphs.utils import cell_feature_df
from timagetk.io.graph import from_csv
from timagetk.io.graph import to_csv
from timagetk.tasks.json_parser import DEFAULT_FEATURES_DATASET
from timagetk.tasks.json_parser import json_csv_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.json_parser import json_time_metadata

DESCRIPTION = """Detect outliers using Local Outlier Factor algorithm.

"""
cli_name = Path(__file__).stem


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION)

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('name', type=str,
                        help="the name to give to the set of outliers, used for export.")
    parser.add_argument('features', type=str, nargs='+',
                        help="the cell features to use to detect the outliers.")

    ds_arg = parser.add_argument_group('dataset arguments')
    ds_arg.add_argument('--features_dataset', type=str, default=DEFAULT_FEATURES_DATASET,
                        help=f"name of the dataset containing the CSV files with exported features, '{DEFAULT_FEATURES_DATASET}' by default.")

    outliers_arg = parser.add_argument_group('outliers arguments')
    outliers_arg.add_argument('--identities', type=str, nargs='+', default=None,
                              help="restrict the cell feature values to cells with selected identities.")
    outliers_arg.add_argument('--robust_scaling', action="store_true",
                              help="whether to apply robust scaling prior to using LOF.")
    outliers_arg.add_argument('--scaling_range', type=float, nargs='2', default=[0.1, 0.9],
                              help="quantile range to use to compute the scaling factor, default to `[0.1, 0.9]`.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default.")

    return parser


def detect_outliers(ttg, features, identities, robust_scaling, scaling_range):
    """Detect outliers on a set of features using LOF.

    Parameters
    ----------
    ttg : timagetk.graphs.TissueGraph or timagetk.graphs.TemporalTissueGraph
        The (temporal) tissue graph to extract features from.
    features : list[str]
        List of feature names to use to create the DataFrame.
    ids : list, optional
        Identity names to find in the graph to restrict the list of cell ids.
        This can also include ``'forward_lineaged'`` to include only cells that have at least one ancestor.
        This can also include ``'layer_*'`` to include only cells belonging to the specified layers.
        For example ``'layer_1_2'`` will include only cells that are in the first two layers.
    robust_scaling : bool
        Whether to perform robust scaling prior to LOF.
    scaling_range : tuple
        A len-2 tuple specifying the quantile range to use to scale the features prior to LOF.
        Should be in ``[0., 100.]``.

    Returns
    -------
    list
        The list of outliers ids.

    """
    cell_df = cell_feature_df(ttg, features, ids=identities, default=np.nan)

    if robust_scaling:
        scaler = RobustScaler(quantile_range=tuple(scaling_range))
        scaled_array = scaler.fit_transform(cell_df)
        cell_df = pd.DataFrame(scaled_array, columns=cell_df.columns, index=cell_df.index)

    outlier_ids, _ = lof_df(cell_df, features)
    return outlier_ids


def run(args):
    """Get and check the data from dataset prior to execute the `features_projection` task.

    Parameters
    ----------
    args : Namespace
        The parsed CLI arguments.

    """
    # Get the name of the series
    args.xp_id = json_series_metadata(args.json)['series_name']
    # Get time related info
    time_md = json_time_metadata(args.json)
    # Defines some location variables:
    args.root_path = Path(args.json).parent.absolute()  # the root directory is where the JSON file is...
    args.out_path = args.root_path.joinpath(args.features_dataset)  # but needs absolute path to save files
    args.out_path.mkdir(exist_ok=True)  # make sure the directory exists

    # - Initialize logger
    logger = get_logger(cli_name, args.root_path.joinpath(f'{cli_name}.log'), args.log_level.upper())
    # Log first welcome message
    logger.info(f"Cell feature outliers detection for time-series '{args.xp_id}' located under '{args.root_path}'.")

    # ----- GRAPH CSV FILES -----
    # - Get the dictionary of graph CSVs indexed by time-point:
    csv_files = json_csv_parsing(args.json, args.features_dataset)
    logger.info(f"Found {len(csv_files)} CSV files under '{args.features_dataset}'.")
    logger.debug(csv_files)

    # ----- TEMPORAL TISSUE GRAPH -----
    # - Build the TemporalTissueGraph from the CSV files:
    logger.info("Re-build the TemporalTissueGraph from the CSV files...")
    ttg = from_csv(csv_files)
    logger.info(
        f"Obtained a TemporalTissueGraph with the following cell features: {sorted(ttg.list_cell_properties())}")

    args.features = check_selected_features(ttg, args.features, logger)
    logger.info(f"Got a list of {len(args.features)} cell features: {args.features}")
    if args.identities:
        logger.info(f"Got a list of {len(args.identities)} cell identities: {args.identities}")

    # - Detect the outliers for selected features and cell identities:
    outlier_ids = detect_outliers(ttg, args.features, args.identities, args.robust_scaling, args.scaling_range)
    logger.info(f"Detected {len(outlier_ids)}!")
    ttg.add_cell_identity(args.name, outlier_ids)

    logger.info("Exporting TemporalTissueGraph to CSV files...")
    csv_files = to_csv(ttg, [args.out_path.joinpath(csv) for csv in csv_files[:2]],
                       cell_features='all', wall_features='all', export_dual=False)
    logger.info(f"Done writing CSVs: {', '.join([str(csv) for csv in csv_files])}.")

    return


def main():
    parser = parsing()
    args = parser.parse_args()
    run(args)


if __name__ == '__main__':
    main()
