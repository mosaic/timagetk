#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
import os
from os.path import join
from os.path import split

import matplotlib.pyplot as plt
import numpy as np

from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.gui.mpl_image_surface_landmarks import SurfaceLandmarks
from timagetk.io import imread
from timagetk.tasks.json_parser import DEFAULT_INTENSITY_DATASET
from timagetk.tasks.json_parser import add2json
from timagetk.tasks.json_parser import json_intensity_image_parsing
from timagetk.tasks.json_parser import json_landmarks_parsing
from timagetk.tasks.json_parser import json_series_metadata

OUT_DATASET = "lineage_landmarks"

DESCRIPTION = """Initialisation of lineage procedure by manual definition of temporal landmarks.
 
"""


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION,
                                     epilog=f"Spatio-temporal landmarks are stored in the '{OUT_DATASET}' dataset.")

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('--intensity_dataset', type=str, default=DEFAULT_INTENSITY_DATASET,
                        help=f"the dataset to use as intensity image input for the lineage algorithm, '{DEFAULT_INTENSITY_DATASET}' by default.")
    parser.add_argument('-t', '--time_index', type=list, default=[],
                        help="the time-index of the multi-angles image used in fusion procedure, all of them by default.")

    out = parser.add_argument_group('output arguments')
    out.add_argument('-o', '--out_dataset', type=str, default=OUT_DATASET,
                     help=f"the dataset name to use to save landmarks files, '{OUT_DATASET}' by default.")
    out.add_argument('--save_figures', action="store_true",
                     help="export renderer snapshots instead of interactive display.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default")

    return parser


def main(args):
    # Get the name of the series
    series_md = json_series_metadata(args.json)
    xp_id = series_md['series_name']
    # Defines some location variables:
    json_root_path, _ = split(args.json)  # the root directory is where the JSON file is...
    abs_out_path = join(json_root_path, args.out_dataset)  # absolute path to save files

    logger = get_logger('lineage_initialisation', join(json_root_path, 'lineage_initialisation.log'), args.log_level.upper())

    # Make sure the destination directory exists:
    try:
        os.mkdir(abs_out_path)
    except:
        pass
    # Get the list of multi-angle images per time-point
    int_time_serie = json_intensity_image_parsing(args.json, args.intensity_dataset)
    n_imgs = len(int_time_serie)
    logger.info(f"Found {n_imgs} intensity images indexed.")
    logger.debug(int_time_serie)

    # Try to load existing landmarks
    try:
        ldmk_json = json_landmarks_parsing(args.json, args.out_dataset)
    except KeyError:
        ldmk_json = {}
    else:
        logger.info("Found existing landmarks files:")
        logger.info(ldmk_json)

    logger.info("Starting lineage manual initialisation task...")
    # Loop to call a GUI to create landmarks between intensity images to register
    if args.time_index == []:
        args.time_index = list(range(n_imgs))
    else:
        args.time_index = list(map(int, args.time_index))
    for ti, ref_img_fname in int_time_serie.items():
        if ti not in args.time_index:
            logger.info(f"Skipping time-index {ti}")
            continue
        if ti + 1 not in int_time_serie:
            break
        # Load the reference intensity image (t_n)
        logger.info(f"Loading reference image #{ti}...")
        ref_img = imread(ref_img_fname)
        # Load the floating intensity image (t_n+1)
        logger.info(f"Loading floating image #{ti + 1}...")
        float_img = imread(int_time_serie[ti + 1])
        # Initialize the landmarks, try to get them from JSON file:
        ldmks = {'left': [], 'right': []}
        try:
            l_pts = [np.loadtxt(jf) for jf in ldmk_json[(ti, ti + 1)] if 'reference' in jf][0]
            r_pts = [np.loadtxt(jf) for jf in ldmk_json[(ti, ti + 1)] if 'floating' in jf][0]
            assert len(l_pts) == len(r_pts)  # we need the same number of points!
            ldmks = {'left': l_pts, 'right': r_pts}
        except KeyError:
            pass  # Undefined yet, no need to explicit the caught error here...
        except IOError as e:
            logger.critical(e)  # print the caught error as there might be some files missing...
            import sys
            sys.exit("Error while loading files!")
        else:
            logger.info(f"Loaded reference and floating landmarks (n={len(l_pts)})")

        logger.info("Initializing manual landmarks definition GUI...")
        logger.info("Press 'R' to rotate the floating image on the right panel...")
        logger.info("Press 'A' and left-click on a panel to add a point on both side...")
        logger.info("Close the window to move on to the next iteration (if any)...")
        figure = plt.figure(0)
        figure.clf()
        figure.set_size_inches(15, 7)
        landmarks = SurfaceLandmarks(figure=figure, img_left=ref_img, img_right=float_img,
                                     surface_threshold=45, value_range=(0, 255), points=ldmks,
                                     orientation={'left': 1, 'right': 1})
        plt.show(block=True)  # to avoid looping ... should we use a user 'input' instead ?
        # - Returned XYZ landmarks coordinates are in the original reference geometry for the right image:
        l_pts = landmarks.points_3d_coordinates()['left']
        r_pts = landmarks.points_3d_coordinates()['right']

        # - Define the landmarks file names and save them:
        logger.info("Saving landmarks files...")
        ref_ldmks_file = f"{xp_id}_t{ti}-t{ti + 1}_reference_lineage_ldmk.txt"
        flo_ldmks_file = f"{xp_id}_t{ti}-t{ti + 1}_floating_lineage_ldmk.txt"
        np.savetxt(join(abs_out_path, ref_ldmks_file), np.array(l_pts))
        np.savetxt(join(abs_out_path, flo_ldmks_file), np.array(r_pts))
        # Save the location of the landmarks files & update the JSON file with it
        logger.info("Updating JSON metadata file...")
        ldmk_json[(ti, ti + 1)] = [ref_ldmks_file, flo_ldmks_file]
        add2json(args.json, args.out_dataset, ldmk_json)

        logger.info("Taking a snapshot of the lineage landmarks...")
        # Save a snapshot of the manual lineage
        figure = plt.figure(0)
        figure.clf()
        figure.set_size_inches(15, 7)
        snap = SurfaceLandmarks(figure, img_left=ref_img, img_right=float_img,
                                surface_threshold=45, value_range=(0, 255),
                                rotations=landmarks.rotations, points={'left': l_pts, 'right': r_pts})
        snap.save_landmarks_figure(join(abs_out_path, f"{xp_id}_t{ti}-t{ti + 1}_lineage_ldmk.png"))
        plt.close()


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
