#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
from pathlib import Path

from timagetk import TissueImage3D
from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.io import imread
from timagetk.io import imsave
from timagetk.tasks.json_parser import DEFAULT_SEGMENTATION_DATASET
from timagetk.tasks.json_parser import json_segmentation_parsing

cli_name = Path(__file__).stem

DESCRIPTION = """Change the value of the background
Be careful with this script or you may loose your original data!

"""


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION)

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('old_bkgd', type=int,
                        help="old value of the background.")
    parser.add_argument('new_bkgd', type=int,
                        help="new value of the background.")
    parser.add_argument('-seg', '--segmentation_dataset', type=str, default=DEFAULT_SEGMENTATION_DATASET,
                        help=f"name of the dataset containing the segmented images, '{DEFAULT_SEGMENTATION_DATASET}' by default.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default")

    return parser


def main(args):
    # Defines some location variables:
    args.root_path = Path(args.json).parent.absolute()  # the root directory is where the JSON file is...

    # Initialize logger
    logger = get_logger(cli_name, args.root_path.joinpath(f'{cli_name}.log'), args.log_level.upper())

    # Get the list of images per time-point
    seg_imgs = json_segmentation_parsing(args.json, args.segmentation_dataset)
    n_seg_imgs = len(seg_imgs)
    logger.info(f"Found {n_seg_imgs} segmented images under '{args.segmentation_dataset}'.")
    logger.debug(seg_imgs)

    for idx in range(n_seg_imgs):
        im = imread(seg_imgs[idx], TissueImage3D)
        if args.old_bkgd in im:
            if args.new_bkgd not in im:
                im = im.relabelling_cells_from_mapping({args.old_bkgd: args.new_bkgd})
                imsave(seg_imgs[idx], im)
            else:
                logger.error(f"The new label {args.new_bkgd} is already used in the image '{im.filename}'...")
        else:
            logger.warning(f"The old label {args.old_bkgd} was not found in the image '{im.filename}'...")


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
