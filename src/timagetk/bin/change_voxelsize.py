#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
from os.path import join
from pathlib import Path

from timagetk.algorithms.resample import resample
from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.bin.tasks import external_voxelsize
from timagetk.io import imread
from timagetk.io import imsave
from timagetk.tasks.json_parser import DEFAULT_INTENSITY_DATASET
from timagetk.tasks.json_parser import DEFAULT_SEGMENTATION_DATASET
from timagetk.tasks.json_parser import json_intensity_image_parsing
from timagetk.tasks.json_parser import json_segmentation_parsing

cli_name = Path(__file__).stem

DESCRIPTION = """Modify the voxel-size of the intensity and segmented images using an external file.
WARNING: this will modify your dataset by rewriting over original files, intensity and segmented images.
Be careful with this script or you may loose your original data!

"""


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION)

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('vxs', type=str,
                        help="name of the voxelsize file, should be at the same location the the JSON file.")
    parser.add_argument('-int', '--intensity_dataset', type=str, default=DEFAULT_INTENSITY_DATASET,
                        help=f"name of the dataset containing the intensity images, '{DEFAULT_INTENSITY_DATASET}' by default.")
    parser.add_argument('-seg', '--segmentation_dataset', type=str, default=DEFAULT_SEGMENTATION_DATASET,
                        help=f"name of the dataset containing the segmented images, '{DEFAULT_SEGMENTATION_DATASET}' by default.")

    vxs_arg = parser.add_argument_group('voxelsize arguments')
    vxs_arg.add_argument('--separator', type=str, default=":",
                         help="specify the type of separators to parse the external voxelsizes file.")
    vxs_arg.add_argument('--axes_order', type=str, default="ZYX",
                         help="specify the axes order of the external voxelsizes file.")

    img_arg = parser.add_argument_group('images arguments')
    img_arg.add_argument('--no_resample', action="store_true",
                         help="if specified, override the image voxelsize, do NOT resample.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default")

    return parser


def change_voxelsize(image, voxelsize, resampling=True):
    """Change the voxel-size og the image, by resampling it or by overriding the known values.

    Parameters
    ----------
    image : timagetk.SpatialImage
        The image to modify.
    voxelsize : list[float]
        The ZYX voxel-size to apply to the image.
    resampling : bool
        If ``True`` (default), resample the image to given voxel-size.
        Else override the known voxel-size attribute.

    Returns
    -------
    timagetk.SpatialImage
        The modified image.

    """
    if resampling:
        # - Resample the image:
        return resample(image, voxelsize=voxelsize)
    else:
        # - Override the known voxel-size:
        image.voxelsize = voxelsize
        return image


def main(args):
    # Defines some location variables:
    args.root_path = Path(args.json).parent.absolute()  # the root directory is where the JSON file is...

    # Initialize logger
    logger = get_logger(cli_name, args.root_path.joinpath(f'{cli_name}.log'), args.log_level.upper())

    voxelsizes_dict = external_voxelsize(join(args.root_path, args.vxs), args.separator, args.axes_order)
    n_vxs = len(voxelsizes_dict)

    # Get the list of images per time-point
    time_series = json_intensity_image_parsing(args.json, args.intensity_dataset)
    n_imgs = len(time_series)
    logger.info(f"Found {n_imgs} intensity images under '{args.intensity_dataset}'.")
    logger.debug(time_series)

    seg_imgs = json_segmentation_parsing(args.json, args.segmentation_dataset)
    n_seg_imgs = len(seg_imgs)
    logger.info(f"Found {n_seg_imgs} segmented images under '{args.segmentation_dataset}'.")
    logger.debug(seg_imgs)

    try:
        assert n_imgs == n_seg_imgs == n_vxs
    except AssertionError:
        logger.critical(
            f"Not the same number of external voxelsize {n_vxs}, intensity {n_imgs} and segmented {n_seg_imgs} images!")
        exit(1)

    img_ppties = ['voxelsize', 'shape', 'extent']
    for idx in range(n_imgs):
        # - Change the voxelsize of the intensity image:
        logger.info(f"Changing the voxelsize of intensity image #{idx}")
        im = imread(time_series[idx])
        current_ppties = {ppty: getattr(im, ppty) for ppty in img_ppties}
        logger.info(f"Current physical properties are: {current_ppties}")
        im = change_voxelsize(im, voxelsizes_dict[idx], not args.no_resample)
        imsave(time_series[idx], im)
        # - Change the voxelsize of the segmented image:
        logger.info(f"Changing the voxelsize of segmented image #{idx}")
        im = imread(seg_imgs[idx])
        current_ppties = {ppty: getattr(im, ppty) for ppty in img_ppties}
        logger.info(f"Current physical properties are: {current_ppties}")
        im = change_voxelsize(im, voxelsizes_dict[idx], not args.no_resample)
        imsave(seg_imgs[idx], im)


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
