#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import argparse
from os.path import join
from tempfile import gettempdir

from timagetk import MultiChannelImage
from timagetk.algorithms.linearfilter import linearfilter
from timagetk.io import imsave
from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image
from timagetk.synthetic_data.nuclei_image import nuclei_image_from_point_positions
from timagetk.synthetic_data.wall_image import wall_image_from_labelled_image

N_POINTS_SMALL_IMG = 3
N_POINTS_MEDIUM_IMG = 10
N_POINTS_LARGE_IMG = 15
DEFAULT_EXT = 'tif'

DESCRIPTION = """Create 3D synthetic image data as a layered sphere.
"""


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION)
    parser.add_argument('prefix', type=str,
                        help="the prefix to use to save the image(s). Overriden if a pre-defined setup is used.")
    parser.add_argument('type', type=str, nargs='+', choices=["labelled", "nuclei", "wall", "multichannel"],
                        help="the type of image to create, more than one type can be specified.")

    phy_arg = parser.add_argument_group("physical arguments")
    phy_arg.add_argument("-e", "--extent", type=float, default=50.,
                         help="the physical extent of the synthetic image.")
    phy_arg.add_argument("-v", "--voxelsize", type=float, nargs=3, default=(1., 0.5, 0.5),
                         help="the ZYX voxel-size of the synthetic image.")

    struct_arg = parser.add_argument_group("structure arguments")
    struct_arg.add_argument("--n_points", type=int, default=N_POINTS_MEDIUM_IMG,
                            help="the number of cells to create in the synthetic image.")
    struct_arg.add_argument("--n_layers", type=int, default=2.,
                            help="the number of cell-layers to create in the synthetic image.")

    wall_arg = parser.add_argument_group("wall arguments")
    wall_arg.add_argument("--wall_sigma", type=float, default=0.05,
                          help="the sigma to use to create walls in the synthetic image.")
    wall_arg.add_argument("--wall_intensity", type=int, default=255,
                          help="the signal intensity to use to create walls in the synthetic image.")

    wall_arg = parser.add_argument_group("nuclei arguments")
    wall_arg.add_argument("--nuclei_radius", type=float, default=1.5,
                          help="the radius to use to create nuclei in the synthetic image.")
    wall_arg.add_argument("--nuclei_intensity", type=int, default=255,
                          help="the signal intensity to use to create nuclei in the synthetic image.")

    img_arg = parser.add_argument_group("image arguments")
    img_arg.add_argument("--dtype", type=str, choices=["uint8", "uint16"], default="uint8",
                         help="the bit-depth of the synthetic image.")

    predef_arg = parser.add_argument_group("pre-defined image arguments")
    predef_arg.add_argument("--small", action="store_true",
                            help="use the pre-defined settings to create a small synthetic wall image.")
    predef_arg.add_argument("--medium", action="store_true",
                            help="use the pre-defined settings to create a medium synthetic wall image.")
    predef_arg.add_argument("--large", action="store_true",
                            help="use the pre-defined settings to create a large synthetic wall image.")

    out_arg = parser.add_argument_group("output arguments")
    out_arg.add_argument('--path', type=str, default=gettempdir(),
                         help=f"the path to use to save the image(s), '{gettempdir()}' by default.")
    out_arg.add_argument('--ext', type=str, default=DEFAULT_EXT,
                         help=f"the file extension to use to save the image(s), '{DEFAULT_EXT}' by default.")

    return parser


xsmall_kwargs = {"extent": 5., "voxelsize": (1., 1., 1.), "n_points": N_POINTS_SMALL_IMG, "n_layers": 1}
small_kwargs = {"extent": 15., "voxelsize": (0.5, 0.5, 0.5), "n_points": N_POINTS_SMALL_IMG, "n_layers": 1}
medium_kwargs = {"extent": 50., "voxelsize": (1., 0.5, 0.5), "n_points": N_POINTS_MEDIUM_IMG, "n_layers": 2}
large_kwargs = {"extent": 100., "voxelsize": (1., 0.5, 0.5), "n_points": N_POINTS_LARGE_IMG, "n_layers": 2}

xsmall_wall_kwargs = {"wall_sigma": 0.01, "wall_intensity": 250}
small_wall_kwargs = {"wall_sigma": 0.05, "wall_intensity": 250}
medium_wall_kwargs = {"wall_sigma": 0.1, "wall_intensity": 250}
large_wall_kwargs = {"wall_sigma": 0.15, "wall_intensity": 250}

xsmall_nuclei_kwargs = {"nuclei_radius": 1., "nuclei_intensity": 250}
small_nuclei_kwargs = {"nuclei_radius": 1., "nuclei_intensity": 250}
medium_nuclei_kwargs = {"nuclei_radius": 1.5, "nuclei_intensity": 250}
large_nuclei_kwargs = {"nuclei_radius": 2., "nuclei_intensity": 250}


def make_images(labelled=False, wall=False, nuclei=False, multichannel=False, **kwargs):
    # Make a layered sphere labelled image:
    seg_img, points = example_layered_sphere_labelled_image(return_points=True, **kwargs)

    if wall or multichannel:
        wall_img = wall_image_from_labelled_image(seg_img, **kwargs)
        wall_img = linearfilter(wall_img, method="smoothing", sigma=1., real=False)
    if nuclei or multichannel:
        nuclei_img = nuclei_image_from_point_positions(points, **kwargs)
    if multichannel:
        mc_img = MultiChannelImage({'wall': wall_img, 'nuclei': nuclei_img})

    images_dict = {}
    if labelled:
        images_dict.update({"labelled": seg_img})
    if wall:
        images_dict.update({"wall": wall_img})
    if nuclei:
        images_dict.update({"nuclei": nuclei_img})
    if multichannel:
        images_dict.update({"multichannel": mc_img})

    return images_dict


def save_images(images_dict, fpath=gettempdir(), prefix="", ext=DEFAULT_EXT):
    images_path = {}
    # - Write TIFF images:
    if "labelled" in images_dict:
        img_fname = fname(fpath, prefix, "labelled", ext)
        images_path.update({"labelled": img_fname})
        imsave(img_fname, images_dict["labelled"])
    if "wall" in images_dict:
        img_fname = fname(fpath, prefix, "wall", ext)
        images_path.update({"wall": img_fname})
        imsave(img_fname, images_dict["wall"])
    if "nuclei" in images_dict:
        img_fname = fname(fpath, prefix, "nuclei", ext)
        images_path.update({"nuclei": img_fname})
        imsave(img_fname, images_dict["nuclei"])
    if "multichannel" in images_dict:
        img_fname = fname(fpath, prefix, "multichannel", ext)
        images_path.update({"multichannel": img_fname})
        imsave(img_fname, images_dict["multichannel"])
    return images_path


def fname(fpath, prefix, img_type, ext):
    return join(fpath, f"{prefix}_{img_type}_image.{ext}")


def main(args):
    if args.path == "":
        args.path = gettempdir()

    labelled = "labelled" in args.type  # defines the type of image to produce
    wall = "wall" in args.type  # defines the type of image to produce
    nuclei = "nuclei" in args.type  # defines the type of image to produce
    multichannel = "multichannel" in args.type  # defines the type of image to produce

    if args.xsmall:
        images_dict = make_images(labelled, wall, nuclei, multichannel, dtype=args.dtype,
                                  **xsmall_kwargs, **xsmall_wall_kwargs, **xsmall_nuclei_kwargs)
        save_images(images_dict, args.path, "xsmall", args.ext)
    if args.small:
        images_dict = make_images(labelled, wall, nuclei, multichannel, dtype=args.dtype,
                                  **small_kwargs, **small_wall_kwargs, **small_nuclei_kwargs)
        save_images(images_dict, args.path, "small", args.ext)
    if args.medium:
        images_dict = make_images(labelled, wall, nuclei, multichannel, dtype=args.dtype,
                                  **medium_kwargs, **medium_wall_kwargs, **medium_nuclei_kwargs)
        save_images(images_dict, args.path, "medium", args.ext)
    if args.large:
        images_dict = make_images(labelled, wall, nuclei, multichannel, dtype=args.dtype,
                                  **large_kwargs, **large_wall_kwargs, **large_nuclei_kwargs)
        save_images(images_dict, args.path, "large", args.ext)

    # If not a predefined config, use arguments:
    if not args.xsmall and not args.small and not args.medium and not args.large:
        images_dict = make_images(labelled, wall, nuclei, multichannel, dtype=args.dtype,
                                  extent=args.extent, voxelsize=args.voxelsize,
                                  n_points=args.n_points, n_layers=args.n_layers,
                                  wall_sigma=args.wall_sigma, wall_intensity=args.wall_intensity,
                                  nuclei_radius=args.nuclei_radius, nuclei_intensity=args.nuclei_intensity)
        save_images(images_dict, args.path, args.prefix, args.ext)


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
