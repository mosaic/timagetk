#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
from pathlib import Path

import numpy as np
from timagetk.algorithms.exposure import global_contrast_stretch
from timagetk.algorithms.pointmatching import pointmatching
from timagetk.array_util import test_array_n_percent_range
from timagetk.array_util import to_uint8
from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.io import imread
from timagetk.io import imsave
from timagetk.io import read_trsf
from timagetk.tasks.fusion import fusion_on_reference
from timagetk.tasks.fusion import iterative_fusion
from timagetk.tasks.json_parser import DEFAULT_EXT
from timagetk.tasks.json_parser import DEFAULT_FUSION_DATASET
from timagetk.tasks.json_parser import DEFAULT_INTENSITY_DATASET
from timagetk.tasks.json_parser import add2json
from timagetk.tasks.json_parser import json_channel_names
from timagetk.tasks.json_parser import json_intensity_image_parsing
from timagetk.tasks.json_parser import json_landmarks_parsing
from timagetk.tasks.json_parser import json_multiangle_trsf_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.third_party.vt_parser import AVERAGING_METHODS
from timagetk.third_party.vt_parser import DEF_AVG_METHOD
from timagetk.third_party.vt_parser import DEF_PY_LL
from timagetk.visu.stack import orthogonal_view

DEFAULT_N_ITER = 1
OUT_DATASET = DEFAULT_FUSION_DATASET
cli_name = Path(__file__).stem

DESCRIPTION = """Multi-angle images registration and fusion.

Supports automated and manual transformations, intensity rescaling, and iterative algorithms to produce 
high-quality fused images.
Provides options for super-resolution processing, channel-specific operations, flexible averaging methods,
and supports logging for process transparency.
"""

# -----------------------------------------------------------------------------
# Can be tested with:
# multiangle_fusion /home/jonathan/Projects/timagetk/data/p58/p58.json --manual_ldmks multiangle_landmarks --uint8 --rescale_intensity --final_vxs 0.5
# -----------------------------------------------------------------------------

def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION)

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('-d', '--dataset', type=str, default=DEFAULT_INTENSITY_DATASET,
                        help=f"the dataset to use as intensity image input for multi-angles image fusion procedure, '{DEFAULT_INTENSITY_DATASET}' by default.")
    parser.add_argument('-t', '--time_index', type=list, default=[],
                        help="the time-index of the multi-angles image used in fusion procedure, all of them by default.")

    preproc_arg = parser.add_argument_group('pre-process arguments')
    preproc_arg.add_argument('--uint8', action="store_true",
                             help="convert the image to 8bit unsigned integers, useful to save memory and speed up stuffs.")
    preproc_arg.add_argument('--rescale_intensity', action="store_true",
                             help="rescale the intensity images to their dtype intensity range.")

    reg_arg = parser.add_argument_group('fusion arguments')
    reg_arg.add_argument('--manual_ldmks', type=str, default=None,
                         help="the dataset hosting prior manual definition of landmarks, often done with `multiangle_initialisation.py`.")
    reg_arg.add_argument('--manual_init', type=str, default=None,
                         help="add this option to use prior manual definition initial transformations, often done with `multiangle_manual_rotation.py`")
    reg_arg.add_argument('--n_iter', type=int, nargs='+', default=DEFAULT_N_ITER,
                         help=f"number of iterations to performs when fusing multi-angle images, default is {DEFAULT_N_ITER}.")
    reg_arg.add_argument('--average_method', type=str, default=DEF_AVG_METHOD, choices=AVERAGING_METHODS,
                         help=f"averaging method to use when fusing multi-angle images, default is {DEF_AVG_METHOD}.")
    reg_arg.add_argument('--super_resolution', action="store_true",
                         help="creates a super-resolution image when fusing multi-angle images at last step.")
    reg_arg.add_argument('--final_vxs', type=float, default=None,
                         help="the voxelsize value for the isometric final image.")
    reg_arg.add_argument('--global_averaging', action="store_true",
                         help="perform global averaging of each voxel by the total number of images, else use mask for local averaging.")
    reg_arg.add_argument('--last_non_linear', action="store_true",
                         help="perform non-linear registration only at last iteration step and only rigid and affine registrations are used in the previous ones.")
    reg_arg.add_argument('--py_ll', type=int, nargs='+', default=DEF_PY_LL,
                         help=f"use this to control the pyramid lowest level for block-matching, {DEF_PY_LL} by default.")

    log_arg = parser.add_argument_group('multichannel image arguments')
    log_arg.add_argument('--channel', type=str,
                         help="channel to use with a multichannel image.")

    out_arg = parser.add_argument_group('output arguments')
    out_arg.add_argument('--ext', type=str, default=DEFAULT_EXT,
                         help=f"extension used to save fused images, '{DEFAULT_EXT}' by default.")
    out_arg.add_argument('--out_dataset', type=str, default=OUT_DATASET,
                         help=f"export fused images in a '{OUT_DATASET}' dataset.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default.")

    return parser


def _load_image(image_fname, img_status, logger, ch_names=None, channel=None):
    """Load an intensity image from its filename, might try to load only one channel if `channel` is defined.

    Parameters
    ----------
    image_fname : str or pathlib.Path
        A filename or path of the image to be loaded.
    img_status : str
        A status or description of the image being loaded.
    logger : logging.Logger
        A logger instance for logging information and errors.
    ch_names : list[str], optional
        A list of channel names, by default None.
    channel : str, optional
        A specific channel to load, by default None.

    Returns
    -------
    timagetk.SpatialImage
        The loaded intensity image.
    """
    # Load the reference intensity image
    if channel != None:
        try:
            logger.info(f"Loading channel '{channel}' of '{img_status}' image '{Path(image_fname).name}'")
            image = imread(image_fname, channel_names=ch_names, channel=channel)
        except AttributeError:
            logger.error(f"Could not get '{channel}' channel from '{Path(image_fname).name}'.")
            logger.info(f"Loading '{img_status}' image '{Path(image_fname).name}'")
            image = imread(image_fname)
    else:
        logger.info(f"Loading '{img_status}' image '{Path(image_fname).name}'")
        image = imread(image_fname)
    return image


def _pre_process(image, img_2_uint8, rescale, logger):
    """Performs some preprocessing on the intensity image.

    Parameters
    ----------
    image : timagetk.SpatialImage
        An input intensity image that needs to be pre-processed.
    img_2_uint8 : bool
        A flag indicating whether the image should be converted to uint8 type.
    rescale : bool
        A flag indicating whether the image intensity should be rescaled.
    logger : logging.Logger
        A logger instance for logging informational messages.

    Returns
    -------
    timagetk.SpatialImage
        The pre-processed intensity image.
    """
    # Check if rescaling is needed or if the array's intensity range needs adjustment
    if rescale or not test_array_n_percent_range(image.get_array(), pc_range_threshold=75):
        # Log the action of applying global contrast stretching
        logger.info(f"- Global contrast stretching...")
        # Rescale the image intensity to the 'uint8' range
        image = global_contrast_stretch(image, pc_min=2., pc_max=98.)

    # Check if the image needs to be converted to uint8 type
    if img_2_uint8:
        # Log the action of converting the image to uint8
        logger.info(f"- Conversion to uint8...")
        # Perform the conversion only if the current type is not uint8
        if image.dtype != np.uint8:
            image = to_uint8(image)

    # Return the processed image
    return image


def load_ldmk_trsfs(ldmk_json, ti, ma_images, ref_idx, logger):
    """Load multi-angle landmarks and compute initial transformations.

    Parameters
    ----------
    ldmk_json : dict
        A dictionary containing landmark JSON data with keys specifying the time indices.
    ti : int
        A time index for the specific landmarks to be loaded.
    ma_images : dict[int, timagetk.SpatialImage]
        An indexed dictionary of multi-angle images.
    ref_idx : int or str
        The expected reference image index for the transformations.
    logger : logging.Logger
        A logger instance used for logging information during the loading and transformation process.

    Returns
    -------
    dict[(int, int), timagetk.Trsf]
        A 2-tuple indexed dictionary of linear transformations from manual landmarks.
        It is indexed by pairs of reference and floating images indexes, that is `(ref_img_idx, flo_img_idx)`.
    """
    float_idx = set(ma_images.keys()) - {ref_idx}
    logger.info("Loading multi-angle landmarks and computing initial transformations...")
    # FIXME: need to update `multiangle_initialisation.py` to return dict of image-indexed pairs of ldmk files
    init_trsfs = {}
    # Iterate over the floating image indexes to calculate transformations for each floating-reference pair.
    for flo_idx in float_idx:
        # Load landmark points for the reference image.
        l_pts = [np.loadtxt(jf) for jf in ldmk_json[ti][(ref_idx, flo_idx)] if 'reference' in jf][0]
        # Load landmark points for the floating image.
        r_pts = [np.loadtxt(jf) for jf in ldmk_json[ti][(ref_idx, flo_idx)] if 'floating' in jf][0]
        logger.info(f"- Loaded {len(l_pts)} landmarks for T(i_{flo_idx}->i_{ref_idx})")
        # Compute the transformation between the reference and floating points using `pointmatching`
        init_trsfs[(ref_idx, flo_idx)] = pointmatching(r_pts, l_pts, template_img=ma_images[ref_idx],
                                                       method='rigid', real=True)
        logger.info(f"- Computed rigid transformation T(i_{flo_idx}->i_{ref_idx})")
    return init_trsfs


def load_init_trsf(init_trsf_json, t_idx, ref_idx, logger):
    """Load the initial transformations for given time-index using JSON dictionary.

    Parameters
    ----------
    init_trsf_json : dict
        A dictionary containing the initial transformation filenames indexed by image pairs.
    t_idx : str
        A time-index for `init_trsf_json`.
    ref_idx : int or str
        The expected reference image index for the transformations.
    logger : logging.Logger
        A logger instance for logging messages and errors.

    Returns
    -------
    dict[(int, int), timagetk.Trsf]
        A 2-tuple indexed dictionary of linear transformations from files.
        It is indexed by pairs of reference and floating images indexes, that is `(ref_img_idx, flo_img_idx)`.
    """
    logger.info("Loading initial manual transformations...")
    # Load the initial linear trsf files to an image-indexed pairs dict of `Trsf`
    init_trsfs = {}
    for imgs_idx, trsf_fname in init_trsf_json[t_idx].items():
        if imgs_idx[0] != ref_idx:
            logger.error(f"The reference image in {imgs_idx} is not the expected one ({ref_idx})!")
            exit(1)
        init_trsfs[imgs_idx] = read_trsf(trsf_fname)
    return init_trsfs


def main(args):
    """For help, in a terminal: `$ python multiangle_fusion.py -h`
    """
    # Get the name of the series
    series_md = json_series_metadata(args.json)
    xp_id = series_md['series_name']
    # Defines some location variables:
    args.root_path = Path(args.json).parent.absolute()  # the root directory is where the JSON file is...
    args.out_path = args.root_path.joinpath(args.out_dataset)  # but needs absolute path to save files
    args.out_path.mkdir(exist_ok=True)  # make sure the directory exists

    # Initialize logger
    logger = get_logger(cli_name, args.root_path.joinpath(f'{cli_name}.log'), args.log_level.upper())
    # Log first welcome message
    logger.info(f"Lineage projections for time-series '{xp_id}' located under '{args.root_path}'.")

    # Get the dictionary of multi-angle images indexed by time-point
    multiangles = json_intensity_image_parsing(args.json, args.dataset)
    n_ma_images = len(multiangles)
    logger.info(f"Found {n_ma_images} multi-angle intensity images under '{args.dataset}'.")
    logger.debug(multiangles)

    # Try to load channel names:
    try:
        ch_names = json_channel_names(args.json)
    except:
        ch_names = []

    if args.manual_ldmks:
        # Try to load existing landmarks
        try:
            ldmk_json = json_landmarks_parsing(args.json, args.manual_ldmks)
        except KeyError:
            logger.error(f"Could not find `{args.manual_ldmks}` entry in JSON file '{args.json}'!")
            exit(1)
        else:
            logger.info("Found landmarks files definition.")
            logger.debug(ldmk_json)
    elif args.manual_init:
        try:
            init_trsf_json = json_multiangle_trsf_parsing(args.json, args.manual_init)
        except KeyError:
            logger.error(f"Could not find `{args.manual_init}` entry in JSON file '{args.json}'!")
            exit(1)
        else:
            logger.info("Found landmarks files definition.")
            logger.debug(init_trsf_json)

    # Check the number of iteration parameter:
    if isinstance(args.n_iter, int):
        args.n_iter = [args.n_iter]
    if isinstance(args.n_iter, list) and len(args.n_iter) == 1:
        args.n_iter = args.n_iter * n_ma_images
    else:
        try:
            assert len(args.n_iter) == n_ma_images
        except AssertionError:
            raise ValueError(
                f"Not enough number of iterations ({len(args.n_iter)}) compared to series ({n_ma_images})!")
    # Check the number of iterations requested is greater than 0:
    try:
        assert all([n_iter >= 1 for n_iter in args.n_iter])
    except AssertionError:
        raise ValueError(f"Number of iterations must be greater than 0, found {args.n_iter}!")

    # Check the "pyramid_lowest_level" parameter:
    if isinstance(args.py_ll, int):
        args.py_ll = [args.py_ll]
    if isinstance(args.py_ll, list) and len(args.py_ll) == 1:
        args.py_ll = args.py_ll * n_ma_images
    else:
        try:
            assert len(args.py_ll) == n_ma_images
        except AssertionError:
            raise ValueError(
                f"Not enough values for parameter 'py_ll' ({len(args.py_ll)}) compared to series ({n_ma_images})!")

    # Check the selected time-indexes:
    if args.time_index == []:
        args.time_index = list(range(n_ma_images))
    else:
        args.time_index = list(map(int, args.time_index))

    # Any given value for final voxelsize override the super-resolution parameter
    if args.final_vxs is not None:
        args.super_resolution = False

    fusions = {}
    logger.info("Starting multi-angles fusion loop (time-series)...")
    # Loop to call a GUI to create landmarks between intensity images to register
    for ti, ma_fnames in multiangles.items():
        if ti not in args.time_index:
            logger.info(f"Skipping time-index {ti}")
            continue
        else:
            logger.info(f"Got {len(ma_fnames)} images to fuse for time-index {ti}...")

        if len(ma_fnames) < 2:
            logger.info(f"Skip the time-index {ti}, there is no multi-angle images!")
            continue

        if args.manual_ldmks and ti not in ldmk_json:
            logger.warning(f"Could not find `{args.manual_ldmks}` for time-point {ti} in JSON file '{args.json}'!")
        elif args.manual_init and ti not in init_trsf_json:
            logger.warning(f"Could not find `{args.manual_init}` for time-point {ti} in JSON file '{args.json}'!")

        ma_images = {im_idx: _pre_process(_load_image(im, f'view-{im_idx}', logger,
                                                      ch_names=ch_names, channel=args.channel),
                                          args.uint8, args.rescale_intensity, logger) for im_idx, im in
                     ma_fnames.items()}

        ref_idx = 0  # For now, we force the reference image for fusion is the first one!
        #TODO: This should be notified somewhere (maybe really early during the dataset creation ?)
        if args.manual_ldmks:
            init_trsfs = load_ldmk_trsfs(ldmk_json, ti, ma_images, ref_idx, logger)
        elif args.manual_init:
            init_trsfs = load_init_trsf(init_trsf_json, ti, ref_idx, logger)
        else:
            floats_idx = set(ma_images.keys()) - {ref_idx}
            init_trsfs = {(ref_idx, flo_idx): None for flo_idx in floats_idx}
            # `None` will force the computation of a rigid registration on the reference image prior to fusion

        if args.n_iter[ti] == 1:
            logger.info(f"Fusion algorithm for {xp_id}-t{ti}...")
            fused_img, _ = fusion_on_reference(ma_images, init_trsfs=init_trsfs, method=args.average_method,
                                            pyramid_lowest_level=args.py_ll[ti], vectorfield=True,
                                            final_vxs=args.final_vxs, super_resolution=args.super_resolution,
                                            global_averaging=args.global_averaging,
                                            refine_init_trsf=True)
            fig_title = f'Fused image - {xp_id} - t{ti}'
        else:
            logger.info(f"Iterative fusion algorithm for {xp_id}-t{ti} (n_iter={args.n_iter[ti]})...")
            fused_img = iterative_fusion(ma_images, init_trsfs=init_trsfs, method=args.average_method,
                                         n_iter=args.n_iter[ti], pyramid_lowest_level=args.py_ll[ti],
                                         final_vxs=args.final_vxs, super_resolution=args.super_resolution,
                                         global_averaging=args.global_averaging,
                                         vectorfield_at_last=args.last_non_linear,
                                         refine_init_trsf=True)
            fig_title = f'Fused image after {args.n_iter[ti]} iterations - {xp_id} - t{ti}'

        orthogonal_view(fused_img, suptitle=fig_title,
                        figname=args.out_path.joinpath(f"{xp_id}_t{ti}-fusion.png"), figsize=(15, 15), val_range='auto')

        out_img_fname = f"{xp_id}_t{ti}_fused.{args.ext}"
        imsave(args.out_path.joinpath(out_img_fname), fused_img, force=True)
        fusions[ti] = out_img_fname
        add2json(args.json, args.out_dataset, fusions)


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
