#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import sys

from timagetk.bin.logger import get_dummy_logger


def check_time_index(time_index, n_img):
    """Method taking care of the time-index in tasks scripts.

    Parameters
    ----------
    time_index : list, int
        A single or a list (can be empty) of time index to use when iterating tasks methods
    n_img : int
        The number of image loaded by a task script

    Returns
    -------
    list
        The list of time index to use

    Examples
    --------
    >>> from timagetk.bin.tasks import check_time_index
    >>> check_time_index([], 2)
    [0, 1]
    >>> check_time_index([0, 1, 2], 2)
    [0, 1]
    >>> check_time_index(1, 2)
    [1]

    """
    default_index = list(range(n_img))
    if time_index == []:
        time_index = default_index
    else:
        if isinstance(time_index, int):
            time_index = [time_index]
        time_index = list(set(map(int, time_index)) & set(default_index))

    return time_index


def external_voxelsize(filepath, sep=" = ", axes_order="XYZ"):
    """Load image voxel-size from an external file.

    Parameters
    ----------
    filepath : str
        The path to the file with the voxel-sizes.
    sep : str
        The separator between the key and the voxel-sizes.
    axes_order : str
        The order of the axes in the provided external voxel-size file.

    Returns
    -------
    dict
        The ZYX ordered voxel-size dictionary.

    Examples
    --------
    >>> from timagetk.bin.tasks import external_voxelsize
    >>> voxelsizes = external_voxelsize('/data/FPS_paper/wt/FM1/resolutions.txt', sep=" = ")
    >>> voxelsizes

    """
    voxelsizes = {}
    f = open(filepath, 'r')
    lines = f.readlines()
    for n, line in enumerate(lines):
        if line.endswith("\n"):
            line = line[:-2]
        if line.endswith(","):
            line = line[:-1]
        if len(line) == 0:
            continue
        im_id, voxelsize = line.split(sep)
        voxelsize = voxelsize.replace(" ", "")
        voxelsize = voxelsize.replace("[", "")
        voxelsize = voxelsize.replace("]", "")
        voxelsize = voxelsize.split(",")
        voxelsizes[n] = [float(vxs) for vxs in voxelsize]

    if axes_order == "XYZ":
        voxelsizes = {n: vxs[::-1] for n, vxs in voxelsizes.items()}

    return voxelsizes


def get_trsf_files(args, logger, **kwargs):
    """Get the dictionary of transformation files from the parsed arguments.

    Parameters
    ----------
    args :
        Parsed argument from the CLI.
    logger : logging.Logger
        A logger to use.

    Other Parameters
    ----------------
    trsf_type : str
        The type of trsf to return in the dictionary.
        Should be either 'rigid', 'affine' or 'vectorfield'.
        Defaults to 'rigid'.

    Returns
    -------
    dict or None
        A 2-tuple indexed dictionary of transformations.
        If any required transformation is missing, returns `None`.
        If the ``no_registration`` attribute is ``True``, returns `None`.

    """
    from timagetk.tasks.json_parser import json_trsf_parsing

    trsf_type = kwargs.get("trsf_type", "rigid")

    if getattr(args, 'no_registration', False):
        logger.info("Image registration is manually disabled.")
        return None

    trsf_json = json_trsf_parsing(args.json, args.trsf_dataset)
    # - Check there are 'rigid' transformation files referenced in the JSON:
    try:
        trsf_files = trsf_json[trsf_type]
    except KeyError:
        logger.error(f"Could not find '{trsf_type}' temporal transformations for time series {args.xp_id}!")
        trsf_files = None
        logger.info("Continuing without these transformation files!")
    else:
        # - Check the consistency between the number of image and transformation files:
        required_regs = [(t, t + 1) for t in range(args.n_imgs - 1)]
        try:
            assert [reg in trsf_files for reg in required_regs]
        except AssertionError:
            logger.critical(f"Could not find all the required consecutive '{trsf_type}' transformations!")
            missing_trsf = [reg not in trsf_files for reg in required_regs]
            n_miss = len(missing_trsf)
            logger.critical(f"Missing {n_miss} consecutive '{trsf_type}' transformations for intervals: {missing_trsf}")
            logger.info("Run `consecutive_registration.py` to compute them!")
            trsf_files = None
            logger.info("Continuing without these transformation files!")
        else:
            logger.info(f"Found the required '{trsf_type}' transformations under '{args.trsf_dataset}'.")
            logger.info(f"Existing intervals: {', '.join(list(map(str, trsf_files.keys())))}.")
            logger.debug(trsf_files)

    return trsf_files


def check_temporal_rank(rank, time_index, logger=None):
    """Verify the values of given temporal 'rank'.

    Parameters
    ----------
    rank : int or list of int
        The (list of) temporal ranks to consider.
    time_index : list of int
        The list of available time indexes to consider.
    logger : logging.Logger, optional
        The logger to use with this function.

    Returns
    -------
    int or list of int
        The updated (list of) temporal ranks to consider.

    Examples
    --------
    >>> from timagetk.bin.tasks import check_temporal_rank
    >>> check_temporal_rank(-1, [0, 1, 3, 5])
    5
    >>> check_temporal_rank([1, -1], [0, 1, 3, 5])
    [1, 5]
    """
    if logger is None:
        logger = get_dummy_logger(sys._getframe().f_code.co_name)

    if isinstance(rank, list):
        return [check_temporal_rank(r, time_index, logger) for r in rank]

    max_tidx = sorted(time_index)[-1]
    if rank < 0:
        # If the input rank is negative, change it to a positive by using the time-index
        rank = sorted(time_index)[rank]
    elif rank >= 1:
        try:
            assert rank <= max_tidx
        except AssertionError:
            logger.error(f"Given `rank` ({rank}) is greater than the max time-index ({max_tidx})!")
            rank = max_tidx
            logger.info(f"Setting `rank` to the max time-index (rank={rank}).")
    else:
        logger.error(f"Given rank is null ({rank})!")

    return rank


def load_trsf_from_files(trsf_files, ti, tj):
    """Load transformation matrix from a dictionary of transformation files.

    Parameters
    ----------
    trsf_files : dict
        The dictionary of transformation files, indexed by time-index 2-tuples.
    ti : int
        The initial time-index to load.
    tj : int
        The final time-index to load.

    Returns
    -------
    timagetk.Trsf
        The loaded transformation.
    """
    from timagetk.io import read_trsf
    if (ti, tj) in trsf_files.keys():
        return read_trsf(trsf_files[(ti, tj)])
    else:
        # Compose the consecutive transformations in between `ti` and `tj` to create the desired transformation:
        from timagetk.algorithms.trsf import compose_trsf
        return compose_trsf([read_trsf(trsf_files[(t, t + 1)]) for t in range(ti, tj)])


def load_lineage_from_files(lineage_files, ti, tj, fmt='marsalt'):
    """Load lineage from a dictionary of lineage files.

    Parameters
    ----------
    trsf_file : dict
        The dictionary of lineage files, indexed by time-index 2-tuples.
    ti : int
        The initial time-index to load.
    tj : int
        The final time-index to load.

    Returns
    -------
    ctrl.lineage.Lineage
        The loaded lineage.
    """
    if (ti, tj) in lineage_files.keys():
        from timagetk.third_party.ctrl.io import read_lineage
        return read_lineage(lineage_files[(ti, tj)], fmt=fmt)
    else:
        # Compose the consecutive lineage in between `ti` and `tj` to create the desired lineage:
        from timagetk.third_party.ctrl.lineage import extend_lineage
        return extend_lineage([lineage_files[(t, t + 1)] for t in range(ti, tj)], fmt=fmt)


def clear_existing_clustering(args, logger):
    """Clear the JSON and output dataset from any previously computed clustering.

    Parameters
    ----------
    args :
        Parsed argument from the CLI.
    logger : logging.Logger
        A logger to use.

    Returns
    -------
    dict
        The empty clustering dictionary.

    """
    from shutil import rmtree
    from timagetk.tasks.json_parser import json_clustering_parsing
    from timagetk.tasks.json_parser import add2json

    # Check if JSON file has clustering entries:
    clustering_dict = json_clustering_parsing(args.json, args.out_dataset)
    if len(clustering_dict) != 0:
        # Clear JSON:
        add2json(args.json, args.out_dataset, {}, replace=True)
    # Check if directory exists & has files:
    if args.out_path.exists():
        empty_dir = len(list(args.out_path.iterdir())) == 0
        if not empty_dir:
            # Clear folder:
            rmtree(args.out_path)
            while not args.out_path.exists():
                args.out_path.mkdir()
            logger.info("Cleared any existing clustering in JSON & dataset directory.")
    else:
        args.out_path.mkdir(exist_ok=True)  # make sure the directory exists

    return {}
