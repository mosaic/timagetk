#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
import json
from os.path import join
from pathlib import Path

from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.tasks.json_parser import DEFAULT_INTENSITY_DATASET
from timagetk.tasks.json_parser import DEFAULT_EXT
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.io import imread, imsave
from timagetk.algorithms.exposure import global_contrast_stretch, equalize_adapthist
from timagetk.tasks.json_parser import add2json, json_intensity_image_parsing, json_series_metadata

OUT_DATASET = "exposure"

DESCRIPTION = """Apply global contrast stretching and/or adaptive histogram equalization to images.

Choose global contrast stretching and/or adaptive histogram equalization depending on the task.

"""


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION,
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     epilog="Use the appropriate options to choose the contrast method.")

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('-int', '--intensity_dataset', type=str, default=DEFAULT_INTENSITY_DATASET,
                        help=f"name of the dataset that contains the intensity images to segment, '{DEFAULT_INTENSITY_DATASET}' by default.")
    parser.add_argument('-index', '--time_index', type=int, nargs='+',
                        help="selection of images to segment, as a list of time-indexes, all of them by default.")

    # Method selection: contrast stretching, adaptive histogram, or both
    method_arg = parser.add_argument_group('method arguments')
    method_arg.add_argument('--methods', choices=['contrast_stretch', 'adapthist', 'both'], required=True,
                            help="choose one of: 'contrast_stretch' for global contrast stretching, 'adapthist' for adaptive histogram equalization, 'both' for applying both methods.")

    # Global contrast stretch arguments
    contrast_arg = parser.add_argument_group('global contrast stretch arguments')
    contrast_arg.add_argument('--pc_min', type=float, default=2,
                              help="lower percentile for global contrast stretching (default: 2).")
    contrast_arg.add_argument('--pc_max', type=float, default=99,
                              help="upper percentile for global contrast stretching (default: 99).")

    # Adaptive histogram equalization arguments
    adapthist_arg = parser.add_argument_group('adaptive histogram equalization arguments')
    adapthist_arg.add_argument('--kernel_size', type=float, default=1 / 5.,
                               help="kernel size for adaptive histogram equalization (default: 1/5).")
    adapthist_arg.add_argument('--clip_limit', type=float, default=0.01,
                               help="clip limit for adaptive histogram equalization (default: 0.01).")
    adapthist_arg.add_argument('--nbins', type=int, default=256,
                               help="number of bins for histogram equalization (default: 256).")

    out_arg = parser.add_argument_group('output arguments')
    out_arg.add_argument('--ext', default=DEFAULT_EXT,
                         help=f"file extension used to save cropped images, '{DEFAULT_EXT}' by default.")
    out_arg.add_argument('--out_dataset', type=str, default=OUT_DATASET,
                         help=f"name of the dataset to export the cropped images, '{OUT_DATASET}' by default.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default.")

    return parser

def main(xp_id, args, logger):
    processed_images = {}  # Dictionary to store processed image file names, indexed by time-series index
    process = {}  # Dictionary to store the processing parameters, indexed by time-series index
    logger.info("Starting image exposure task...")

    exposure_json_path = args.out_path.joinpath('exposure.json')
    if exposure_json_path.exists():
        with exposure_json_path.open('r') as crop_f:
            existing_process = json.load(crop_f)
        process.update(existing_process)

    # Iterate over intensity images and apply contrast adjustments
    for im_idx, int_img_fname in args.intensity_imgs.items():
        logger.info(f"Processing image file '{int_img_fname}' at time-index {im_idx}...")
        image = imread(int_img_fname)  # Read the input image

        process_entry = {}  # Prepare the entry for this image in the process file
        process_entry['intensity_image'] = int_img_fname

        # Apply Global Contrast Stretching
        if args.methods in ['contrast_stretch', 'both']:
            logger.info(f"Applying global contrast stretching to image at time-index {im_idx}...")
            processed_img = global_contrast_stretch(image, pc_min=args.pc_min, pc_max=args.pc_max)
            processed_fname = f"{xp_id}_t{im_idx}_global_contrast_stretch.{args.ext}"
            imsave(args.out_path.joinpath(processed_fname), processed_img)
            logger.info(f"Saved processed image '{processed_fname}'")

            # Record parameters for Global Contrast Stretching
            process_entry['contrast_stretch'] = {
                'pc_min': args.pc_min,
                'pc_max': args.pc_max,
                'output_filename': processed_fname
            }

            image = processed_img  # Update the image for the next step (adapthist)

        # Apply Adaptive Histogram Equalization
        if args.methods in ['adapthist', 'both']:
            logger.info(f"Applying adaptive histogram equalization to image at time-index {im_idx}...")
            processed_img = equalize_adapthist(image,
                                               kernel_size=args.kernel_size,
                                               clip_limit=args.clip_limit,
                                               nbins=args.nbins)
            processed_fname = f"{xp_id}_t{im_idx}_adapthist.{args.ext}"
            imsave(args.out_path.joinpath(processed_fname), processed_img)
            logger.info(f"Saved processed image '{processed_fname}'")

            # Record parameters for Adaptive Histogram Equalization
            process_entry['adapthist'] = {
                'kernel_size': args.kernel_size,
                'clip_limit': args.clip_limit,
                'nbins': args.nbins,
                'output_filename': processed_fname
            }

        # Handle replacing information if this time index has been processed before
        str_im_idx = str(im_idx)
        if str_im_idx in process:
            logger.info(f"Replacing existing process information for time-index {im_idx}.")
            process[str_im_idx].update(process_entry)  # Update the existing entry
        else:
            process[str_im_idx] = process_entry  # Add a new entry for this time index

        # Add the processed image file to the dictionary of processed images
        processed_images[im_idx] = process_entry['contrast_stretch']['output_filename'] \
            if 'contrast_stretch' in process_entry else process_entry['adapthist']['output_filename']

    # Save process information to process.json
    with exposure_json_path.open('w') as process_f:
        json.dump(process, process_f, indent=4)
    logger.info("Saved process information to 'exposure.json'.")

    # Add the processed images to the global JSON output file
    add2json(args.json, args.out_dataset, processed_images)


def run():
    parser = parsing()
    args = parser.parse_args()

    # Get the name of the series
    series_md = json_series_metadata(args.json)
    xp_id = series_md['series_name']

    # Defines some location variables:
    args.root_path = Path(args.json).parent.absolute()  # Root directory is where the JSON file is...
    args.out_path = args.root_path.joinpath(args.out_dataset)  # Output directory
    args.out_path.mkdir(exist_ok=True)  # Ensure the directory exists

    # Initialize logger
    logger = get_logger('image_exposure', join(args.root_path, 'image_exposure.log'), args.log_level.upper())
    logger.info(f"Exposure images for time-series '{xp_id}' located under '{args.root_path}'.")

    # Get the dictionary of intensity images indexed by time-point
    args.intensity_imgs = json_intensity_image_parsing(args.json, args.intensity_dataset)
    n_imgs = len(args.intensity_imgs)
    logger.info(f"Found {n_imgs} intensity images under '{args.intensity_dataset}'.")

    # -- TIME POINTS - Try to get the 'selected' time-points from the JSON metadata:
    if args.time_index:
        wrong_index = [idx for idx in args.time_index if idx not in args.intensity_imgs.keys()]
        if wrong_index:
            logger.warning(f"Got some manually defined time-index that do not exist: {wrong_index}")
        args.time_index = [idx for idx in args.time_index if idx in args.intensity_imgs.keys()]
    else:
        args.time_index = list(range(n_imgs))  # Default to all time-points

    # Re-index the time-series:
    args.intensity_imgs = {idx: img for idx, img in args.intensity_imgs.items() if idx in args.time_index}

    # Perform contrast adjustments
    main(xp_id, args, logger)


if __name__ == '__main__':
    run()
