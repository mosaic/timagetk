#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
import os
from os.path import join
from os.path import split

from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.io.graph import from_csv
from timagetk.tasks.features import CELL_SCALAR_FEATURES
from timagetk.tasks.features import TEMPORAL_DIFF_FEATURES
from timagetk.tasks.json_parser import DEFAULT_OBJ_DATASET
from timagetk.tasks.json_parser import add2json
from timagetk.tasks.json_parser import json_csv_parsing
from timagetk.tasks.json_parser import json_mesh_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.third_party.morphonet import _test_morphonet
from timagetk.third_party.morphonet import temporal_graph_to_info_file

_test_morphonet()

DESCRIPTION = """Convert labelled images to cell-based mesh OBJ files and basic info files ('space' & 'time').
"""


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION,
                                     epilog="This requires a valid MorphoNet account!")

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")

    ppty_arg = parser.add_argument_group('cell features arguments')
    ppty_arg.add_argument('--cell_features', type=str, nargs='+', default="all",
                          help="the cell features to export.")
    ppty_arg.add_argument('--background_id', type=int, default=1,
                          help="the id corresponding to the background position.")

    out_arg = parser.add_argument_group('output arguments')
    out_arg.add_argument('--out_dataset', type=str, default=DEFAULT_OBJ_DATASET,
                         help=f"export the mesh OBJ files in a '{DEFAULT_OBJ_DATASET}' dataset")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default")

    return parser


def main(args):
    # Get the name of the series
    series_md = json_series_metadata(args.json)
    xp_id = series_md['series_name']
    # Defines some location variables:
    json_root_path, _ = split(args.json)  # the root directory is where the JSON file is...
    abs_out_path = join(json_root_path, args.out_dataset)  # but needs absolute path to save files

    logger = get_logger('create_cell_mesh', join(json_root_path, 'create_cell_mesh.log'), args.log_level.upper())

    # Make sure the destination directory exists:
    try:
        os.mkdir(abs_out_path)
    except:
        pass

    if args.cell_features == "all":
        logger.info("Selected ALL cell features!")
        args.cell_features = CELL_SCALAR_FEATURES + TEMPORAL_DIFF_FEATURES
        n_features = len(args.cell_features)
        field_types = ['float'] * n_features
    elif args.cell_features != []:
        n_features = len(args.cell_features)
        logger.info(f"Selected {n_features} cell features: {args.cell_features}")
    else:
        logger.info("Selected NO cell features!")
        exit(0)

    # - Get CSV files:
    csv_files = json_csv_parsing(args.json)
    # - Make a temporal tissue graph:
    ttg = from_csv(csv_files, background=args.background_id)
    # - Export as info files:
    info_fnames = temporal_graph_to_info_file(ttg, args.cell_features, field_types, path=abs_out_path,
                                              suffix=f"{xp_id}-")
    logger.info("Updating JSON metadata file...")
    obj_mesh, obj_info = json_mesh_parsing(args.json, args.out_dataset)
    obj_info.update(info_fnames)
    add2json(args.json, args.out_dataset, {"mesh": obj_mesh, "info": obj_info})


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
