#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Generic implementation of several sequence registration algorithms.

Sequence registration is a (temporal) forward registration of all images of a time series onto the last one.
It allows to obtain a sequence fully registered on the same template, *i.e.* same image frame (shape, extent, ...).

"""

import argparse
import sys
import time
from os import error
from pathlib import Path
import numpy as np

from timagetk import LabelledImage
from timagetk.algorithms.trsf import apply_trsf
from timagetk.algorithms.trsf import inv_trsf
from timagetk.algorithms.trsf import compose_trsf
from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.io import imread
from timagetk.io import imsave
from timagetk.io import read_trsf
from timagetk.tasks.json_parser import DEFAULT_EXT
from timagetk.tasks.json_parser import DEFAULT_INTENSITY_DATASET
from timagetk.tasks.json_parser import DEFAULT_REGISTRATION
from timagetk.tasks.json_parser import DEFAULT_REGISTRATION_DATASET
from timagetk.tasks.json_parser import DEFAULT_SEGMENTATION_DATASET
from timagetk.tasks.json_parser import DEFAULT_TRSF_DATASET
from timagetk.tasks.json_parser import add2json
from timagetk.tasks.json_parser import json_file_exists
from timagetk.tasks.json_parser import json_intensity_image_parsing
from timagetk.tasks.json_parser import json_metadata
from timagetk.tasks.json_parser import json_registration_parsing
from timagetk.tasks.json_parser import json_segmentation_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.json_parser import json_time_metadata
from timagetk.tasks.json_parser import json_trsf_parsing
from timagetk.third_party.vt_parser import BLOCKMATCHING_METHODS
from timagetk.util import elapsed_time
from timagetk.visu.mplt import grayscale_imshow
from timagetk.visu.projection import PROJECTION_METHODS

OUT_DATASET = DEFAULT_REGISTRATION_DATASET

DESCRIPTION = """Sequence registration of a time-series.

Sequence registration is a (temporal) forward registration of all images of a time series onto the last one.
Use image `I[t]` (image at time `t`) as float and image `I[N]` as reference, with ``N`` the index of the last image of the temporal sequence.
It allows to obtain a sequence fully registered on the same template, *i.e.* same image frame (shape, extent, ...).

This tool will produce the following data:

  1. a **series of sequence transformations** allowing registration of `I[t]` onto `I[N]` frame (saved under the `trsf_dataset` location).
  2. a **series of sequence registered images**, that is `I[t]` registered onto `I[N]` frame (saved under the `out_dataset` location).

This tool will produce the following figure:

  1. a set of "original" projections for the time-series
  2. a set of "registered" projections for the time-series

Note that the "rigid" part of the transformations is always saved.
Except for the figures, the produced data is automatically referenced in the JSON file.

"""
cli_name = Path(__file__).stem


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION)

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('-int', '--intensity_dataset', type=str, default=DEFAULT_INTENSITY_DATASET,
                        help=f"name of the dataset containing the intensity images, '{DEFAULT_INTENSITY_DATASET}' by default.")
    parser.add_argument('-seg', '--segmentation_dataset', type=str, default=DEFAULT_SEGMENTATION_DATASET,
                        help=f"the dataset containing the segmented image on which to project the dataset, '{DEFAULT_SEGMENTATION_DATASET}' by default.")
    parser.add_argument('-trsf', '--trsf_dataset', type=str, default=DEFAULT_TRSF_DATASET,
                        help=f"name of the dataset containing the transformations, '{DEFAULT_TRSF_DATASET}' by default.")

    img_arg = parser.add_argument_group('images arguments')
    img_arg.add_argument('--channel', type=str, default=None,
                         help="channel to register, mandatory for multichannel images.")
    img_arg.add_argument('--orientation', type=int, default=1, choices={-1, 1},
                         help="image orientation, use `-1` with an inverted microscope, `1` by default.")

    reg_arg = parser.add_argument_group('registration arguments')
    reg_arg.add_argument('--registration_type', type=str, default=DEFAULT_REGISTRATION,
                         choices=BLOCKMATCHING_METHODS,
                         help=f"type of registration to performs, '{DEFAULT_REGISTRATION}' by default.")

    out_arg = parser.add_argument_group('figure arguments')
    out_arg.add_argument('--proj_method', type=str, default='none', choices=PROJECTION_METHODS + ['none'],
                         help="projection method to use. 'contour' works well with cell walls/membranes. 'maximum' "
                              "works well with nuclei. 'none' by default.")

    out_arg = parser.add_argument_group('output arguments')
    out_arg.add_argument('--ext', type=str, default=DEFAULT_EXT,
                         help=f"file extension of registered images, '{DEFAULT_EXT}' by default.")
    out_arg.add_argument('--out_dataset', type=str, default=OUT_DATASET,
                         help=f"export images in '{OUT_DATASET}' dataset")
    out_arg.add_argument('--save-trsf', action='store_true',
                         help=f"Save or not (default) the transformations used to compute the registered images.")
    out_arg.add_argument('--save-seg', action='store_true',
                         help=f"If segmented image are given. Save or not (default) the registered images.")
    reg_arg.add_argument('--force', action="store_true",
                         help="force computation of transformations.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default.")

    return parser


def main(args):
    # Get the name of the series
    xp_id = json_series_metadata(args.json)['series_name']
    time_md = json_time_metadata(args.json)

    # Defines some location variables:
    args.root_path = Path(args.json).parent.absolute()  # the root directory is where the JSON file is...
    args.abs_out_path = args.root_path.joinpath(args.out_dataset)  # but needs absolute path to save files
    args.abs_out_path.mkdir(exist_ok=True)  # make sure the directory exists

    if args.save_trsf:
        args.trsf_out_path = args.root_path.joinpath(DEFAULT_TRSF_DATASET)  # but needs absolute path to save files
        args.trsf_out_path.mkdir(exist_ok=True)  # make sure the directory exists

    # Initialize logger
    logger = get_logger(cli_name, args.root_path.joinpath(f'{cli_name}.log'), args.log_level.upper())
    # Log first welcome message
    logger.info(f"Sequence registration for time-series '{xp_id}' located under '{args.root_path}'.")

    # Get the dictionary of intensity images indexed by time-point
    int_imgs = json_intensity_image_parsing(args.json, args.intensity_dataset)
    n_imgs = len(int_imgs)
    logger.info(f"Found {n_imgs} intensity images under '{args.intensity_dataset}'.")
    logger.debug(int_imgs)
    # Check we have at least 3 time-points in this time-series
    try:
        assert n_imgs >= 3
    except AssertionError:
        logger.critical(f"Found less than 3 images ({n_imgs}) in the time series!")
        exit(1)
    # Get the dictionary of segmented images indexed by time-point
    seg_imgs = json_segmentation_parsing(args.json, args.segmentation_dataset)
    logger.info(f"Found {len(seg_imgs)} segmented images under '{args.segmentation_dataset}'.")
    logger.debug(seg_imgs)

    # - Check
    if (args.proj_method == 'background') or args.save_seg:
        try:
            assert len(int_imgs) == len(seg_imgs)
        except AssertionError:
            logger.error("There should be the same number of intensity and segmented images!")
            logger.error(f"Found {len(int_imgs)} intensity images!")
            logger.error(f"Found {len(seg_imgs)} segmented images!")
            sys.exit("The 'background' projection method require the intensity and segmented images dataset!")

        args.abs_seg_out_path = args.root_path.joinpath(args.out_dataset + '-segmented')  # but needs absolute path to save files
        args.abs_seg_out_path.mkdir(exist_ok=True)  # make sure the directory exists

        out_seg_img_json = json_registration_parsing(args.json, args.out_dataset)
        if args.registration_type not in out_seg_img_json:
            out_seg_img_json[args.registration_type] = {}

    # Get the dictionary of intensity images indexed by time intervals (len-2 tuples)
    trsf_json = json_trsf_parsing(args.json, args.trsf_dataset)

    # Check the consistency of number of intensity images and transformations:
    for trsf_type in {"rigid", args.registration_type}:
        trsfs = [(flo_idx, ref_idx) if flo_idx < ref_idx else (ref_idx, flo_idx)
                 for flo_idx, ref_idx in trsf_json[trsf_type]] # avoid issues in case of forward/backward trsfs
        required_regs = [(t, t + 1) for t in range(n_imgs - 1)]
        try:
            assert np.all([reg in trsfs for reg in required_regs])
        except AssertionError:
            logger.critical(f"Could not find all the required consecutive {trsf_type} transformations!")
            logger.critical(
                f"Missing consecutive {trsf_type} transformations for intervals: {[reg not in trsfs for reg in required_regs]}")
            logger.info("Run `consecutive_registration.py` prior to using `sequence_registration.py`!")
            exit(1)
    # Now we can define the time-interval indexed dictionary of transformations:
    trsfs = trsf_json[args.registration_type]
    logger.info(
        f"Found the required {args.registration_type} transformations under '{args.trsf_dataset}'.")

    out_img_json = json_registration_parsing(args.json, args.out_dataset)
    if args.registration_type not in out_img_json:
        out_img_json[args.registration_type] = {}

    # Try to get the image 'orientation' from the JSON metadata:
    orientation = json_metadata(args.json).get('orientation', None)
    if orientation is not None:
        args.orientation = orientation
        logger.info(f"Got an 'image orientation' from JSON: `{args.orientation}`")

    ############################################################################
    # - Start the timer:
    t_start = time.time()

    logger.info("Starting the sequence registration task!")
    time_index = sorted(set(int_imgs.keys()))
    # Get the index of the reference image, that is the latest of the time-series:
    last_idx = time_index[-1]
    # Get the reverted floating images index (no need for the last two images):
    float_index = time_index[:-1][::-1]
    # Load the reference image:
    last_img = imread(int_imgs[last_idx], channel=args.channel)
    last_seg = None # init in case segmented images need to be saved

    # So we start with (t_N-1, t_N) trsf in the list and go backward in time in the list of floating images
    # We will append the previous (in time) trsf to the list: [(t_N-1, t_N), (t_N-2, t_N-1)]
    # And compose the list in the reverse order to obtain (t_N-2, t_N) trsf
    # That we will apply to time-series image[t_N-2]
    inv_trsf2cmp = None # initialize the composed trsf
    list_trsfs = []
    was_inverted = []
    for flo_idx in float_index:
        logger.info(f"Registering t{flo_idx} over t{last_idx}...")
        time_interval = (flo_idx, last_idx)
        has_req_trsf = json_file_exists(out_img_json[args.registration_type], time_interval)
        if has_req_trsf and not args.force:
            logger.info(f"Found existing '{args.registration_type}' transformation, skipping!")
            continue
        flo_img = imread(int_imgs[flo_idx], channel=args.channel)

        # Detect the previous transformation direction
        if (flo_idx, flo_idx + 1) in trsfs:
            # forward
            trsf_path = trsfs[(flo_idx, flo_idx + 1)]
            trsf = read_trsf(trsf_path)
            was_inverted.append(False)
        else:
            # backward, inv. the trsf.
            trsf_path = trsfs[(flo_idx + 1, flo_idx)]
            trsf = inv_trsf(read_trsf(trsf_path), template_img=last_img, error=1e-7)
            was_inverted.append(True) # keep track of the inversion

        list_trsfs.append(trsf_path)

        # Add the previous transformation (in time) to the list of transformations to compose
        if inv_trsf2cmp:
            inv_trsf2cmp = compose_trsf([trsf, inv_trsf2cmp], template_img=last_img)
        else:
            inv_trsf2cmp = trsf

        # Apply the transformation with the template
        reg_img = apply_trsf(flo_img, inv_trsf2cmp, template_img=last_img)

        if args.save_trsf:
            # Save the linear transformation and add its reference (file name) to the JSON file:
            trsf_fname = f"{xp_id}-t{flo_idx}_t{last_idx}-{args.registration_type}.trsf"
            trsf_json[args.registration_type][time_interval] = trsf_fname
            trsf.write(args.trsf_out_path.joinpath(trsf_fname))
            add2json(args.json, args.trsf_dataset, trsf_json)

            # Save the process
            process_trsf = {
                "trsfs": list_trsfs[::-1],
                "was_trsf_inverted": was_inverted[::-1]}
            add2json(args.trsf_out_path.joinpath("processing.json"), "{}/{}".format(*time_interval), process_trsf,
                     required_object_metadata=False,
                     keep_basename=False)  # Keep complete filepath & no need for `object`

        # Save the registered image and add its reference (file name) to the JSON file:
        img_fname = f"{xp_id}-t{flo_idx}_on_t{last_idx}-{args.registration_type}.{args.ext}"
        out_img_json[args.registration_type][time_interval] = img_fname
        imsave(args.abs_out_path.joinpath(img_fname), reg_img)
        add2json(args.json, args.out_dataset, out_img_json)

        # Save the process
        process_img = {
            "intensity_image": int_imgs[flo_idx],
            "reference_image": int_imgs[last_idx],
            "trsfs": list_trsfs[::-1],
            "was_trsf_inverted": was_inverted[::-1]}
        add2json(args.abs_out_path.joinpath("processing.json"), "{}/{}".format(*time_interval), process_img,
                 required_object_metadata=False,
                 keep_basename=False)  # Keep complete filepath & no need for `object`

        if args.save_seg:
            if last_seg is None:
                last_seg = imread(seg_imgs[last_idx], LabelledImage, not_a_label=0)

            img_seg = imread(seg_imgs[flo_idx], LabelledImage, not_a_label=0)
            reg_seg = apply_trsf(img_seg, inv_trsf2cmp, template_img=last_seg,
                                 interpolation='cellbased', cell_based_sigma=1)

            # Save the registered image and add its reference (file name) to the JSON file:
            img_fname = f"{xp_id}-t{flo_idx}_on_t{last_idx}-{args.registration_type}_segmented.{args.ext}"
            out_seg_img_json[args.registration_type][time_interval] = img_fname
            imsave(args.abs_seg_out_path.joinpath(img_fname), reg_seg)
            add2json(args.json, args.out_dataset + '-segmented', out_seg_img_json)

            # Save the process
            process_seg_img = {
                "segmented_image": seg_imgs[flo_idx],
                "reference_image": seg_imgs[last_idx],
                "trsfs": list_trsfs[::-1],
                "was_trsf_inverted": was_inverted[::-1]}
            add2json(args.abs_seg_out_path.joinpath("processing.json"), "{}/{}".format(*time_interval), process_seg_img,
                     required_object_metadata=False,
                     keep_basename=False)  # Keep complete filepath & no need for `object`

    if args.proj_method != 'none':
        from timagetk.visu.projection import projection
        if args.channel is None:
            logger.info(f"{args.proj_method.capitalize()} projection of original intensity...")
        else:
            logger.info(f"{args.proj_method.capitalize()} projection of original intensity channel '{args.channel}'...")

        proj_imgs = []
        # -- Starts with the projection of the original (unregistered) images:
        for idx in time_index:
            logger.info(f"Projecting time-index '{idx}'...")
            int_img = imread(int_imgs[idx], channel=args.channel)
            if args.proj_method == 'background':
                seg_img = imread(seg_imgs[idx], LabelledImage, not_a_label=0)
            else:
                seg_img = None
            proj_img = projection(int_img, segmentation=seg_img, method=args.proj_method, orientation=args.orientation)
            proj_imgs += [proj_img]

        if args.channel is None:
            logger.info(f"{args.proj_method.capitalize()} projection of registered intensity...")
        else:
            logger.info(f"{args.proj_method.capitalize()} projection of registered intensity channel '{args.channel}'...")

        # -- Now get the projection of the registered images:
        proj_reg_imgs = []
        for idx in time_index:
            logger.info(f"Projecting time-index '{idx}'...")
            int_img = imread(int_imgs[idx], channel=args.channel)
            if args.proj_method == 'background':
                seg_img = imread(seg_imgs[idx], LabelledImage, not_a_label=0)
                if idx < last_idx:
                    trsf = read_trsf(trsf_json['rigid'][(idx, last_idx)])
                    int_img = apply_trsf(int_img, trsf, template_img=last_img, interpolation='linear')
                    seg_img = apply_trsf(seg_img, trsf, template_img=last_img, interpolation='cellbased')
            else:
                seg_img = None
            proj_reg_img = projection(int_img, segmentation=seg_img, method=args.proj_method, orientation=args.orientation)
            proj_reg_imgs += [proj_reg_img]

        # -- Create the figure:
        tp = time_md['points']
        tu = time_md['unit']
        titles = [f"t{idx} ({tp[idx]}{tu})" for idx in time_index]
        figname = f"{xp_id}-time_series-top_view_{args.proj_method}_projections.png"
        suptitle=f"{xp_id} time-series top-view projections"
        fig = grayscale_imshow(proj_imgs, suptitle=suptitle, title=titles, max_per_line=len(time_index),
                         figname=args.abs_out_path.joinpath(figname))

        titles = [f"Registered t{idx} on t{last_idx}" for idx in time_index[:-1]]
        titles += [f"Original t{last_idx}"]
        figname = f"{xp_id}-{args.registration_type}_registered_time_series-top_view_{args.proj_method}_projections.png"
        suptitle=f"{xp_id} time-series top-view projections ({args.registration_type} registered)"
        fig = grayscale_imshow(proj_reg_imgs, suptitle=suptitle, title=titles, max_per_line=len(time_index),
                         figname=args.abs_out_path.joinpath(figname))

    logger.info(f"All {elapsed_time(t_start)}")


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
