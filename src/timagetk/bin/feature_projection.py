#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
import logging
import sys
from datetime import datetime
from os.path import join
from pathlib import Path

import markdown
import numpy as np
from matplotlib import pyplot as plt

from timagetk import TissueImage3D
from timagetk.algorithms.resample import resample
from timagetk.algorithms.trsf import apply_trsf
from timagetk.bin.feature_clustering import trsf_temporal_resampling
from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.bin.markdown_report import FIGCAPTION_STYLE
from timagetk.bin.markdown_report import HTML_TABLE_STYLE
from timagetk.bin.markdown_report import _add_css_styles
from timagetk.bin.markdown_report import include_figure_html
from timagetk.bin.tasks import get_trsf_files
from timagetk.features.utils import flatten
from timagetk.gui.vtk_views import vtk_viewer
from timagetk.io import imread
from timagetk.io import read_trsf
from timagetk.io.graph import from_csv
from timagetk.tasks.json_parser import DEFAULT_FEATURES_DATASET
from timagetk.tasks.json_parser import DEFAULT_SEGMENTATION_DATASET
from timagetk.tasks.json_parser import DEFAULT_TRSF_DATASET
from timagetk.tasks.json_parser import json_csv_parsing
from timagetk.tasks.json_parser import json_orientation_metadata
from timagetk.tasks.json_parser import json_segmentation_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.json_parser import json_time_metadata
from timagetk.tasks.json_parser import time_intervals

OUT_DATASET = DEFAULT_FEATURES_DATASET
DESCRIPTION = """Generate snapshots of cell features projected on 3D mesh representing the segmented images.

"""
cli_name = Path(__file__).stem

DEFAULT_FIG_FMT = 'png'
DEFAULT_CMAP = 'viridis'


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION)

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('features', type=str, nargs='+',
                        help="the cell features to project on the segmented images.")

    ds_arg = parser.add_argument_group('dataset arguments')
    ds_arg.add_argument('--features_dataset', type=str, default=DEFAULT_FEATURES_DATASET,
                        help=f"name of the dataset containing the CSV files with exported features, '{DEFAULT_FEATURES_DATASET}' by default.")
    ds_arg.add_argument('--segmentation_dataset', type=str, default=DEFAULT_SEGMENTATION_DATASET,
                        help=f"name of the dataset containing the segmented image files, '{DEFAULT_SEGMENTATION_DATASET}' by default.")
    ds_arg.add_argument('--trsf_dataset', type=str, default=DEFAULT_TRSF_DATASET,
                        help=f"name of the dataset containing the transformation files, '{DEFAULT_TRSF_DATASET}' by default.")

    fig_arg = parser.add_argument_group('figures arguments')
    fig_arg.add_argument('--layers', default=[1], type=int, nargs='+',
                         help="the list of layer used for projection, `1` by default.")
    fig_arg.add_argument('--colormap', default=DEFAULT_CMAP, type=str,
                         help=f"the colormap to use to represent the cell features, `'{DEFAULT_CMAP}'` by default.")
    fig_arg.add_argument('--identities', type=str, nargs='+', default=None,
                         help="restrict the cell feature values to cells with selected identities.")
    fig_arg.add_argument('--outliers', default='', type=str,
                         help="the name of a cell identity referencing outliers to exclude from the figures, none by default.")

    img_arg = parser.add_argument_group('images arguments')
    img_arg.add_argument('--orientation', type=int, default=1, choices={-1, 1},
                         help="image stack orientation, use `-1` with an inverted microscope, `1` by default.")
    img_arg.add_argument('--resampling', type=float, nargs='+', default=[1., 1., 1.],
                         help="resample the image to given voxel-size prior to meshing, `[1., 1., 1.]` by default.")
    img_arg.add_argument('--no_registration', action="store_true",
                         help="use this to prevent image registration.")

    out_arg = parser.add_argument_group('output arguments')
    out_arg.add_argument('--figure_format', type=str, default=DEFAULT_FIG_FMT, choices=['png', 'jpeg', "svg", 'eps'],
                         help=f"change the format of the output image, '{DEFAULT_FIG_FMT}' by default.")
    out_arg.add_argument('--out_dataset', type=str, default=OUT_DATASET,
                         help=f"export snapshots in a '{OUT_DATASET}' dataset")
    out_arg.add_argument('--suffix', type=str, default="",
                         help=f"suffix to add to the report filename.")
    out_arg.add_argument('--html', action="store_true",
                         help="use this to also export the report as HTML.")
    out_arg.add_argument('--force', action="store_true",
                         help="use this to force the creation of feature projections.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default.")

    return parser


def cid_by_layers(tg, layers):
    """Get the list of cell ids per layers.

    Parameters
    ----------
    tg : timagetk.graphs.TissueGraph
        A TissueGraph instance with the 'layers' property.
    layers : list
        The list of cell layers to returns.

    Returns
    -------
    dict
        A layer indexed dictionary of list of cell ids.

    Examples
    --------
    >>> from timagetk.bin.feature_projection import cid_by_layers
    >>> from timagetk.graphs.tissue_graph import example_tissue_graph
    >>> tg = example_tissue_graph('sphere', features=['layers'], time_point=0)
    >>> cell_by_layers = cid_by_layers(tg, [1, 2])
    >>> {layer: len(cids) for layer, cids in cell_by_layers.items()}  # print the number of cells by layers
    {1: 49, 2: 13}

    """
    # Get the layer property dict:
    layers_cid = tg.cell_property_dict('layers', default=None, only_defined=True)
    # Make a layer indexed list of cids:
    cell_by_layers = {layer: [] for layer in layers}
    for cid, layer in layers_cid.items():
        if layer in layers:
            cell_by_layers[layer] += [cid]
    return cell_by_layers


def check_selected_features(ttg, features, logger):
    # -- Some features might not be in the graph yet, let's try to compute them:
    for feat in features:
        if feat not in ttg.list_cell_properties():
            try:
                eval(f"ttg.{feat}")
            except:
                pass

    # -- Check the list of selected features:
    valid_features = ttg.list_cell_properties()
    existing_features = [feat for feat in features if feat in valid_features]
    missing_features = set(features) - set(existing_features)
    # - If none of the selected features are found in the TemporalTissueGraph, log & quit!
    if existing_features == []:
        logger.critical("Could not find any of the selected cell features in the TemporalTissueGraph!")
        logger.info(f"List of valid cell features from the TemporalTissueGraph: {', '.join(valid_features)}")
        sys.exit("Check the selected celle features against existing features from CSV files!")
    if missing_features != set():
        logger.error("Could not find some of the requested cell features from the TemporalTissueGraph!")
        logger.error(f"List of the missing cell features: {', '.join(missing_features)}!")
    return existing_features


def features_projection(xp_id, seg_imgs, trsfs, ttg, feature_data, value_range, layers, orientation, target_vxs,
                        out_path, logger, **kwargs):
    """Performs the features projection task.

    Parameters
    ----------
    xp_id : str
        Name of the time-series.
    seg_imgs : dict
        The dictionary of segmented images indexed by time-point.
    trsfs : dict
        The dictionary of transformations indexed by time intervals (len-2 tuples).
    ttg : TemporalTissueGraph
        The TemporalTissueGraph containing the features to project.
    features : list of str
        The list of features to project.
    layers : list of int
        The list of layers for which to project the features.
    orientation : {-1, 1}
        The image orientation.
    target_vxs : tuple of float
        A 3-tuple of voxelsize to use for resampling prior to meshing.
    no_registration : bool
        Whether to apply temporal registration prior to snapshot creation.
    out_path : pathlib.Path or str
        The path to save the figures.
    logger : logging.Logger
        The logger to use to display messages.

    Other Parameters
    ----------------
    cmap : str
        The name of a colormap to use to represent the cell features.
    fmt : str
        The extension defining the image format to save.
    force : bool
        Whether to for the creation of feature snapshot even if they already exists in the output dataset.
    suffix : str
        Suffix to add to the report filename.

    Returns
    -------
    dict
        Path to feature boxplot figures, indexed by feature names.
    """
    registration = kwargs.get("registration", False)
    cmap = kwargs.get("cmap", DEFAULT_CMAP)
    fmt = kwargs.get("fmt", DEFAULT_FIG_FMT)
    suffix = kwargs.get("suffix", "")
    force = kwargs.get("force", False)

    out_path = Path(out_path)
    time_index = list(range(ttg.nb_time_points))
    features = list(feature_data.keys())

    # -- Get the list of cell ids for corresponding layers by time-point:
    cell_ids = {}  # {tp: [cell_ids]}
    cell_layers = {}  # {tp: {layer: [cell_ids]}}
    for t in time_index:
        cell_layers[t] = cid_by_layers(ttg._tissue_graph[t], layers)
        # Create the list of ids based on the selected layers for current time-point:
        cell_ids[t] = list(flatten(list(cell_layers[t].values())))

    # Pre-determines the filenames to create:
    fnames = {t: {feat: "" for feat in features} for t, _ in seg_imgs.items()}
    layers_str = f"L{'L'.join(list(map(str, layers)))}"
    for t, features in fnames.items():
        for feat in features:
            fig_path = out_path.joinpath(f"{xp_id}-t{t}_{layers_str}-{feat}{suffix}.{fmt}")
            fnames[t][feat] = fig_path

    all_exists = all([fnames[t][feat].exists() for feat in features for t, features in fnames.items()])
    if all_exists and not force:
        logger.info("All features 3D projections image already exists, skipping this step!")
        return fnames

    # - Group feature data by time-points:
    feature_data_tp = {feat: {tp: {} for tp in time_index} for feat in features}
    for feat in features:
        for (t, cid), value in feature_data[feat].items():
            feature_data_tp[feat][t].update({cid: value})

    # -- Final stage: load the segmented image, resample it, mesh it and project the features
    for t, seg_img in seg_imgs.items():
        # Load the associated segmented images:
        seg_img = imread(seg_img, TissueImage3D, not_a_label=0, background=1)
        # Try to load the t_n -> t_n+1 trsf
        if registration and trsfs is not None:
            try:
                trsf = read_trsf(trsfs[(t, t + 1)])
            except:
                logger.warning(f"Could not find 'rigid' transformation for {t}->{t + 1} interval!")
            else:
                logger.info(f"Applying {t}->{t + 1} 'rigid' transformation to t{t} segmented image.")
                seg_img = apply_trsf(seg_img, trsf, interpolation="cellbased", cell_based_sigma=1,
                                     template_img=imread(seg_imgs[t + 1], TissueImage3D, not_a_label=0, background=1))
        # -- Down-sample the image to speed up the meshing:
        seg_img = resample(seg_img, voxelsize=target_vxs, interpolation='cellbased', cell_based_sigma=1., quiet=True)
        # -- VTK rendering:
        for feat in features:
            data = feature_data_tp[feat][t]
            fig_path = fnames[t][feat]
            logger.info(f"# Saving {fig_path}...")
            vtk_viewer(seg_img, data, feat, layers=cell_layers[t], filename=fig_path, orientation=orientation,
                       labels=list(cell_ids[t]), value_range=value_range[feat], colormap=cmap, logger=logger)
            fnames[t][feat] = fig_path

    return fnames


def feature_distribution(xp_id, ttg, feature_data, layers, out_path, logger, **kwargs):
    """Create the feature boxplot.

    Parameters
    ----------
    xp_id : str
        Name of the time-series.
    ttg : TemporalTissueGraph
        The TemporalTissueGraph containing the features to project.
    feature_data : dict
        A feature indexed dictionary of cell-id indexed feature values, that is ``{feature: {cell: feature_value}}``.
    layers : list of int
        The list of layers for which to project the features.
    outliers_tcids : list or set or tuple
        An iterable of time-indexed cell-id tuples, that is ``[(t, cid)]``.
    out_path : pathlib.Path or str
        The path to save the figures.
    logger : logging.Logger
        The logger to use to display messages.

    Other Parameters
    ----------------
    fmt : str
        The extension defining the image format to save.
    suffix : str
        Suffix to add to the report filename.

    Returns
    -------
    dict
        Path to feature boxplot figures, indexed by feature names.
    """
    fmt = kwargs.get("fmt", DEFAULT_FIG_FMT)
    suffix = kwargs.get("suffix", "")

    time_index = list(range(ttg.nb_time_points))
    features = list(feature_data.keys())

    # - Group feature data by time-points:
    feature_data_tp = {feat: {tp: {} for tp in time_index} for feat in features}
    for feat, data in feature_data.items():
        for (t, cid), value in data.items():
            feature_data_tp[feat][t].update({cid: value})

    fnames = {feat: "" for feat in features}
    for feat, t_values in feature_data_tp.items():
        data = []
        labels = []
        for t in time_index:
            ppty_dict = t_values[t]
            all_values = list(ppty_dict.values())
            labels += [f"t{t}"]
            if all_values != []:
                data += [all_values]
            else:
                data += [[np.nan]]
        fig_path = join(out_path, f"{xp_id}-L{'L'.join(list(map(str, layers)))}-boxplot-{feat}{suffix}.{fmt}")
        fnames[feat] = fig_path
        fig, ax = plt.subplots(figsize=(max(5, len(time_index)), 6))
        bplot = ax.boxplot(data, widths=0.4, showbox=True, flierprops={"marker": '+'}, labels=labels)
        ax.grid(linestyle='-.')
        ppt_unit = ttg.get_cell_property_unit(feat)
        plt.ylabel(f"{feat} ({ppt_unit})", family='freesans', fontsize=13)
        logger.info(f"# Saving {fig_path}...")
        plt.savefig(fig_path)

    return fnames


def feature_distribution_by_layers(xp_id, ttg, feature_data, layers, out_path, logger, **kwargs):
    """Create the feature boxplot by time-index & layers.

    Parameters
    ----------
    xp_id : str
        Name of the time-series.
    ttg : TemporalTissueGraph
        The TemporalTissueGraph containing the features to project.
    feature_data : dict
        A feature indexed dictionary of cell-id indexed feature values, that is ``{feature: {cell: feature_value}}``.
    layers : list of int
        The list of layers for which to project the features.
    out_path : pathlib.Path or str
        The path to save the figures.
    logger : logging.Logger
        The logger to use to display messages.

    Other Parameters
    ----------------
    cmap : str
        The name of a colormap to use to represent the cell features.
    fmt : str
        The extension defining the image format to save.
    suffix : str
        Suffix to add to the report filename.

    Returns
    -------
    dict
        Path to feature boxplot figures, indexed by feature names.

    """
    from typing import Iterable
    from matplotlib import colormaps as cm

    cmap = kwargs.get("cmap", 'Pastel1')
    fmt = kwargs.get("fmt", DEFAULT_FIG_FMT)
    suffix = kwargs.get("suffix", "")

    time_index = list(range(ttg.nb_time_points))
    features = list(feature_data.keys())

    # - Group feature data by time-points:
    feature_data_tp_layers = {feat: {tp: {layer: [] for layer in layers} for tp in time_index} for feat in features}
    for feat, data in feature_data.items():
        for (t, cid), value in data.items():
            layer = ttg.cell_property(t, cid, 'layers')
            feature_data_tp_layers[feat][t][layer] += [value]

    n_groups = len(time_index)

    # - Get the list of colors to apply to the boxes, if any:
    patch_artist = False
    if isinstance(cmap, str):
        cmap = cm.get_cmap(cmap).colors[:n_groups]
        patch_artist = True
    elif isinstance(cmap, dict):
        cmap = [cmap[ti] for ti in time_index]
        patch_artist = True
    elif isinstance(cmap, Iterable):
        patch_artist = True

    # - Create the list of labels to use
    layer_labels = [f"L{layer}" for layer in layers]

    fnames = {feat: "" for feat in features}
    for feat, data_tp_layers in feature_data_tp_layers.items():
        fig, axes = plt.subplots(ncols=n_groups, figsize=(n_groups * len(layers), 6), sharey=True)

        for i, ax in enumerate(axes):
            ti = time_index[i]
            data_layers = data_tp_layers[ti]
            # - Get the data to plot by layers:
            data = [data_layers[layer] for layer in layers]
            # Remove default values as the boxplot method may fail
            data = [np.array(g_data)[np.where(~np.isnan(g_data))[0]] for g_data in data]

            # - Add the number of VALID observations (not default) per group:
            n_data = [len(g_data) for g_data in data]
            subgroup_labels_i = [label + f"\n(n={n_data[n]})" for n, label in enumerate(layer_labels)]

            # Create the boxplot:
            bplot = ax.boxplot(data, widths=0.4, showbox=True, labels=subgroup_labels_i, patch_artist=patch_artist,
                               flierprops={"marker": 'x', 'markerfacecolor': 'r', 'markeredgecolor': 'r'})

            # - Add color to the boxplot, if any:
            if cmap is not None:
                for patch, color in zip(bplot['boxes'], cmap):
                    patch.set_facecolor(color)

            # - Add a grid:
            ax.grid(linestyle='-.')

            ppt_unit = ttg.get_cell_property_unit(feat)
            ax.set_title(f"t{ti}", fontsize=14)
            ax.set_xlabel('layers', family='freesans', fontsize=13)
            if i == 0:
                # Add the feature name as Y-axis label with unit:
                ax.set_ylabel(f"{feat} ({ppt_unit})", family='freesans', fontsize=13)

        # Create & save the boxplot figure:
        fig_path = join(out_path, f"{xp_id}-L{'L'.join(list(map(str, layers)))}-layer_boxplot-{feat}{suffix}.{fmt}")
        logger.info(f"# Saving {fig_path}...")
        plt.savefig(fig_path)
        fnames[feat] = fig_path

    return fnames


def run(args):
    """Get and check the data from dataset prior to execute the `features_projection` task.

    Parameters
    ----------
    args : Namespace
        The parsed CLI arguments.

    """
    # Get the name of the series
    args.xp_id = json_series_metadata(args.json)['series_name']
    # Get time related info
    time_md = json_time_metadata(args.json)
    # Defines some location variables:
    args.root_path = Path(args.json).parent.absolute()  # the root directory is where the JSON file is...
    args.out_path = args.root_path.joinpath(args.out_dataset)  # but needs absolute path to save files
    args.out_path.mkdir(exist_ok=True)  # make sure the directory exists

    # - Initialize logger
    logger = get_logger(cli_name, args.root_path.joinpath(f'{cli_name}.log'), args.log_level.upper())
    # Log first welcome message
    logger.info(f"Cell feature projections for time-series '{args.xp_id}' located under '{args.root_path}'.")

    # ----- GRAPH CSV FILES -----
    # - Get the dictionary of graph CSVs indexed by time-point:
    csv_files = json_csv_parsing(args.json, args.features_dataset)
    logger.info(f"Found {len(csv_files)} CSV files under '{args.features_dataset}'.")
    logger.debug(csv_files)

    # ----- SEGMENTED IMAGES FILES -----
    # - Get the dictionary of segmented images indexed by time-point:
    seg_imgs = json_segmentation_parsing(args.json, args.segmentation_dataset)
    args.n_imgs = len(seg_imgs)
    logger.info(f"Found {args.n_imgs} segmented images under '{args.segmentation_dataset}'.")
    logger.debug(seg_imgs)

    # ----- IMAGE ORIENTATION -----
    # Try to get the image/microscope 'orientation' from the JSON metadata:
    orientation = json_orientation_metadata(args.json, None)
    if orientation is not None:
        # If defined in JSON, override input argument:
        args.orientation = orientation
        logger.info(f"Got an image/microscope 'orientation' from JSON: `{args.orientation}`")
    else:
        logger.info(f"Got an image/microscope 'orientation' from input argument: `{args.orientation}`")

    # ----- TRANSFORMATION FILES -----
    # - Get the dictionary of transformations indexed by time intervals (len-2 tuples)
    trsfs = get_trsf_files(args, logger)
    logger.info(f"Found {len(trsfs)} transformation files.")
    logger.info(f"Known transformation for intervals: {', '.join(list(map(str, trsfs.keys())))}.")

    # ----- TIME INDEXES -----
    # - Check definition of time-points ('point' entry) in JSON 'time' metadata:
    all_time_points = time_md.get("points", None)
    if all_time_points is None:
        logger.critical("No 'point' entry defined in 'time' metadata!")
        sys.exit("Required 'point' entry in 'time' metadata is missing!")
    # - Try to get the 'selected' time-points from the JSON 'time' metadata:
    sel_time_points = time_md.get("selected", None)
    t_unit = time_md.get("unit", "?")
    if sel_time_points is not None:
        logger.info(f"Got a list of 'selected' time-points from JSON: {sel_time_points}")
        args.time_index = [all_time_points.index(tp) for tp in sel_time_points]
        logger.info(f"Corresponding time-indexes: {args.time_index}")
        # Select corresponding segmented images and re-index segmentation dictionary with contiguous time-indexes:
        seg_imgs = {t: seg_imgs[ti] for t, ti in enumerate(args.time_index)}
        args.n_imgs = len(seg_imgs)
        # Select & compose corresponding registration trsfs and relabel with contiguous time-indexes:
        if trsfs is not None:
            trsfs = trsf_temporal_resampling(trsfs, args.time_index)
    else:
        sel_time_points = all_time_points
        args.time_index = list(range(0, len(sel_time_points)))
    logger.info(f"Corresponding time-intervals (in {t_unit}) from JSON: {time_intervals(sel_time_points)}")

    # ----- TARGET IMAGE VOXELSIZE -----
    target_vxs = args.resampling
    if isinstance(target_vxs, (float, int)):
        target_vxs = [float(target_vxs)] * 3

    # ----- TEMPORAL TISSUE GRAPH -----
    # - Build the TemporalTissueGraph from the CSV files:
    logger.info("Re-build the TemporalTissueGraph from the CSV files...")
    ttg = from_csv(csv_files)
    logger.info(f"Obtained a TemporalTissueGraph with the following cell features:{sorted(ttg.list_cell_properties())}")
    # - Check the consistency between the number of images and time-points in temporal tissue graph:
    try:
        assert ttg.nb_time_points == args.n_imgs
    except AssertionError:
        n_tp = ttg.nb_time_points
        logger.critical(
            f"Not the same number of images ({args.n_imgs}) and time-points in the TemporalTissueGraph ({n_tp})!")
        sys.exit("Selected number of images and TemporalTissueGraph do NOT match!")

    args.features = check_selected_features(ttg, args.features, logger)
    logger.info(f"Got a list of {len(args.features)} cell features: {args.features}")

    # - Get the time-indexed cell-id tuples:
    if args.identities:
        # Select cell-ids as a subset of those with selected cell identities:
        logger.info(f"Got a list of {len(args.identities)} cell identities: {args.identities}")
        all_tcids = set(ttg.cell_ids(identity=args.identities))
        if all_tcids != set([]):
            logger.info(f"Using {len(all_tcids)} cell ids with '{len(args.identities)}' identities: {args.identities}")
        else:
            logger.warning(f"Filtering cell by identities lead to an empty set of cell ids!")
    else:
        # Select all cell-ids:
        all_tcids = ttg.cell_ids()

    # - Remove outliers from the list of selected cell-ids if required:
    if args.outliers:
        outliers_tcids = set(ttg.list_cells_with_identity(args.outliers))
        logger.info(f"Excluding {len(outliers_tcids)} outliers listed in identity '{args.outliers}'.")
        all_tcids = list(set(all_tcids) - outliers_tcids)

    # -- Get the feature values and value range:
    feature_data = {}
    value_range = {}
    for feat in args.features:
        feature_data[feat] = ttg.cell_property_dict(feat, all_tcids, default=np.nan, only_defined=False)
        data = np.array(list(flatten(list(feature_data[feat].values()))))
        value_range[feat] = (np.nanmin(data), np.nanmax(data))
        logger.info(f"Detected a value range of {value_range[feat]} for the cell feature named '{feat}'.")

    proj_fnames = features_projection(args.xp_id, seg_imgs, trsfs, ttg, feature_data, value_range,
                                      args.layers, orientation, target_vxs,
                                      args.out_path, logger, registration=~args.no_registration,
                                      cmap=args.colormap, fmt=args.figure_format, suffix=args.suffix, force=args.force)

    distrib_fnames = feature_distribution(args.xp_id, ttg, feature_data, args.layers, args.out_path, logger,
                                          fmt=args.figure_format, suffix=args.suffix)

    layer_distrib_fnames = feature_distribution_by_layers(args.xp_id, ttg, feature_data, args.layers, args.out_path,
                                                          logger, fmt=args.figure_format, suffix=args.suffix)

    feature_report(args.xp_id, args.out_path, ttg, args.features, args.identities, args.outliers, proj_fnames,
                   distrib_fnames, layer_distrib_fnames, html=args.html, suffix=args.suffix)

    return


def feature_report(time_series, out_path, ttg, features, identities, outliers, proj_fnames, distrib_fnames,
                   layer_distrib_fnames, **kwargs):
    out_path = Path(out_path)
    suffix = kwargs.get("suffix", "")
    report_fame = out_path.joinpath(f"{time_series}-feature_report{suffix}.md")
    html_report_fame = out_path.joinpath(f"{time_series}-feature_report{suffix}.html")

    md = "# Cell feature report"
    md += "\n"
    md += f"``Report generated on {datetime.now().strftime('%A %d %B %Y')}.``\n\n"

    md += f"This is a cell feature report for time-series {time_series}.\n\n"
    md += f"A list of {len(features)} cell features has been selected to generate this report, namely"
    md += "\n\n"
    md += "".join([f"  - {feat.replace('_', ' ')}" + "\n" for feat in features])
    md += "\n"

    if identities:
        n_id = len(identities)
        all_tcids = set(ttg.cell_ids(identity=identities))
        md += f"A list of {len(identities)} cell identities has been selected to generate this report, namely:"
        md += "\n\n"
        md += "".join([f"  - {identity}" + "\n" for identity in identities])
        md += "\n"
        md += f"{len(all_tcids)} cell-ids have been found with this {'set of' if n_id > 1 else ''} identit{'ies' if n_id > 1 else 'y'}."
        md += "\n\n"
    else:
        all_tcids = set(ttg.cell_ids(identity=identities))

    if outliers:
        outliers_tcids = set(ttg.list_cells_with_identity(outliers))
        all_tcids = list(set(all_tcids) - outliers_tcids)
        md += f"{len(outliers_tcids)} **outliers** listed in identity '{outliers}' have been excluded from the list of cell-ids."
        md += "\n\n"

    md += f"A list of {len(all_tcids)} cell-ids has been used in this report."
    md += "\n\n"

    md += "## Cell feature distributions by time-points"
    md += "\n\n"
    for feat in features:
        feat_img = Path(distrib_fnames[feat]).name
        md += f"### Boxplot for cell feature '{feat.replace('_', ' ')}'" + "\n"
        md += f"![]({feat_img})" + "\n\n"

    md += "\n"
    md += "## Cell feature distributions by time-points and layers"
    md += "\n\n"
    for feat in features:
        feat_img = Path(layer_distrib_fnames[feat]).name
        md += f"### Boxplot for cell feature '{feat.replace('_', ' ')}'" + "\n"
        md += f"![]({feat_img})" + "\n\n"

    md += "\n"
    md += "## Cell feature 3D projections by time-points and layers"
    md += "\n\n"
    time_points = sorted(set(proj_fnames.keys()))
    for feat in features:
        md += f"### 3D projection for cell feature '{feat.replace('_', ' ')}'" + "\n"
        for t in time_points:
            caption = f"Cell {feat.replace('_', ' ')} at time {t} - First layer (left) and second layer (right)."
            md += include_figure_html(Path(proj_fnames[t][feat]).name, caption=caption, end="\n\n")

    with open(report_fame, 'w') as f:
        f.write(md)

    if kwargs.get("html", False):
        html = ""
        # HTML header:
        html += "<!DOCTYPE html>\n<html>\n"
        # Table styling:
        html += _add_css_styles([HTML_TABLE_STYLE, FIGCAPTION_STYLE])
        # HTML body:
        html += "<body>\n"
        html += markdown.markdown(md, extensions=['tables'])
        # HTML end of body:
        html += "\n</body>\n</html>"
        # Export HTML file:
        with open(html_report_fame, 'w') as f:
            f.write(html)

    return


def main():
    parser = parsing()
    args = parser.parse_args()
    run(args)


if __name__ == '__main__':
    main()
