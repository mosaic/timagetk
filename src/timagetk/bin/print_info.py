# ------------------------------------------------------------------------------
#  Copyright (c) 2023 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
from datetime import datetime
from pathlib import Path

import markdown
import numpy as np
import pandas as pd
from timagetk.third_party.ctrl.io import AVAILABLE_LINEAGE_FMT
from timagetk.third_party.ctrl.io import DEFAULT_LINEAGE_FMT
from timagetk.third_party.ctrl.io import read_lineage
from timagetk.third_party.ctrl.lineage import extend_lineage
from tqdm import tqdm

from timagetk import SpatialImage
from timagetk import TissueImage3D
from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.bin.markdown_report import HTML_TABLE_STYLE
from timagetk.bin.markdown_report import _add_css_styles
from timagetk.components.labelled_image import labels_at_stack_margins
from timagetk.io import imread
from timagetk.tasks.json_parser import DEFAULT_INTENSITY_DATASET
from timagetk.tasks.json_parser import DEFAULT_LINEAGE_DATASET
from timagetk.tasks.json_parser import DEFAULT_SEGMENTATION_DATASET
from timagetk.tasks.json_parser import json_intensity_image_parsing
from timagetk.tasks.json_parser import json_lineage_parsing
from timagetk.tasks.json_parser import json_segmentation_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.json_parser import json_time_metadata
from timagetk.tasks.json_parser import time_intervals

cli_name = Path(__file__).stem


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description="Print information about the dataset.")

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")

    parser.add_argument('-int', '--intensity_dataset', type=str, default=DEFAULT_INTENSITY_DATASET,
                        help=f"the dataset to use as intensity image input for the lineage algorithm, '{DEFAULT_INTENSITY_DATASET}' by default.")
    parser.add_argument('-seg', '--segmentation_dataset', type=str, default=DEFAULT_SEGMENTATION_DATASET,
                        help=f"the dataset containing the segmented images, '{DEFAULT_SEGMENTATION_DATASET}' by default.")
    parser.add_argument('-lin', '--lineage_dataset', type=str, default=DEFAULT_LINEAGE_DATASET,
                        help=f"the dataset containing the lineages, '{DEFAULT_LINEAGE_DATASET}' by default.")
    parser.add_argument('-fmt', '--lineage_format', type=str, default=DEFAULT_LINEAGE_FMT,
                        choices=AVAILABLE_LINEAGE_FMT,
                        help=f"the format used to read the lineage file, '{DEFAULT_LINEAGE_FMT}' by default.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default.")

    out_arg = parser.add_argument_group('output arguments')
    out_arg.add_argument('--html', action="store_true",
                         help="use this to also export the report as HTML.")

    return parser


def intensity_images_info(int_imgs, all_time_points, time_index, t_unit):
    """Create a table containing the intensity image information.

    Parameters
    ----------
    int_imgs : list[str]
        The list of intensity image files.
    all_time_points : list[int]
        The list of all time-points.
    time_index : list[int]
        The list of selected time index.
    t_unit : str
        The time unit to use.

    Returns
    -------
    pandas.DataFrame
        The table containing the image information.
    """
    info = {}
    for ti in tqdm(time_index, unit="time-index"):
        info[f"t{ti}"] = {'time-point': f"{all_time_points[ti]} {t_unit}"}
        if not isinstance(int_imgs[ti], SpatialImage):
            intensity_image = imread(int_imgs[ti], SpatialImage)
        info[f"t{ti}"].update({"Voxelsize (XYZ)": list(np.round(intensity_image.get_voxelsize()[::-1], 3))})

    return pd.DataFrame.from_dict(info)


def segmented_images_info(seg_imgs, all_time_points, time_index, t_unit):
    """Create a table containing the segmented image information.

    Parameters
    ----------
    seg_imgs : list[str]
        The list of segmented image files.
    all_time_points : list[int]
        The list of all time-points.
    time_index : list[int]
        The list of selected time index.
    t_unit : str
        The time unit to use.

    Returns
    -------
    pandas.DataFrame
        The table containing the image information.
    """
    info = {}
    for ti in tqdm(time_index, unit="time-index"):
        info[f"t{ti}"] = {'time-point': f"{all_time_points[ti]} {t_unit}"}
        if not isinstance(seg_imgs[ti], TissueImage3D):
            try:
                segmented_img = TissueImage3D(seg_imgs[ti])
            except:
                segmented_img = imread(seg_imgs[ti], TissueImage3D, not_a_label=0, background=1)
        margin_cells = labels_at_stack_margins(segmented_img, 5)
        info[f"t{ti}"].update({"Voxelsize (XYZ)": list(np.round(segmented_img.get_voxelsize()[::-1], 3))})
        info[f"t{ti}"].update({"Nb cells": len(segmented_img.cells.ids()) - len(margin_cells)})

    return pd.DataFrame.from_dict(info)


def lineage_info(lineage_files, fmt, all_time_points, time_index, t_unit):
    """Create a table containing the lineage information.

    Parameters
    ----------
    lineage_files : list[str]
        The list of cell lineage files.
    fmt : {'basic', 'dict', 'marsalt'}
        The name of the format to use to read the lineage file.
    all_time_points : list
        The list of all time-points.
    time_index : list
        The list of selected time index.
    t_unit : str
        The time unit to use.

    Returns
    -------
    pandas.DataFrame
        The table containing the lineage information.
    """
    # -- Select the corresponding tissues (segmented images) & lineages:
    lineages = {}
    tp_pairs = list(zip(time_index[:-1], time_index[1:]))
    for (ti, tj) in tp_pairs:
        lineages[f"t{ti}-t{tj}"] = {}
        if tj - ti > 1:
            lineage = extend_lineage([lineage_files[(i, i + 1)] for i in range(ti, tj)], fmt=fmt)
        else:
            lineage = read_lineage(lineage_files[(ti, tj)], fmt=fmt)
        lineages[f"t{ti}-t{tj}"].update({"NbAncestors": len(lineage.get_ancestors())})
        lineages[f"t{ti}-t{tj}"].update({"NbDescendants": len(lineage.get_descendants())})
        lineages[f"t{ti}-t{tj}"].update({"TimeInterval": f"{all_time_points[tj] - all_time_points[ti]}{t_unit}"})

    return pd.DataFrame.from_dict(lineages)


def main(args):
    # Get the name of the series
    args.xp_id = json_series_metadata(args.json)['series_name']
    # Defines some location variables:
    args.root_path = Path(args.json).parent.absolute()  # the root directory is where the JSON file is...

    # Initialize logger
    global logger
    logger = get_logger(cli_name, log_level=args.log_level.upper())

    # ----- INTENSITY IMAGES FILES -----
    # - Get the dictionary of intensity images indexed by time-point:
    int_imgs = json_intensity_image_parsing(args.json, args.intensity_dataset)
    # ----- SEGMENTED IMAGES FILES -----
    # - Get the dictionary of segmented images indexed by time-point:
    seg_imgs = json_segmentation_parsing(args.json, args.segmentation_dataset)
    # ----- CELL LINEAGE FILES -----
    # - Get the dictionary of segmented images indexed by time-point:
    lineage_files = json_lineage_parsing(args.json, args.lineage_dataset)

    # ----- CHECK THIS IS CONSISTENT -----
    # Check we have the same number of intensity and segmented images:
    try:
        assert len(int_imgs) == len(seg_imgs)
    except AssertionError:
        logger.error(f"Found {len(int_imgs)} intensity images and {len(seg_imgs)} segmented images.")
        exit(1)
    else:
        n_imgs = len(int_imgs)
    # Check we have "n_image - 1" lineage files:
    try:
        assert len(lineage_files) == n_imgs - 1
    except AssertionError:
        logger.error("We need `N-1` lineage files, where `N` is the number of segmented images...")
        logger.error(f"Found {n_imgs} images and {len(lineage_files)} lineage files.")
        exit(1)

    logger.info(f"This is dataset {args.xp_id}, with {n_imgs} intensity images.")

    # ----- TIME AND TEMPORAL RESAMPLING -----
    # - Get time related info
    time_md = json_time_metadata(args.json)
    all_time_points = time_md.get("points", None)
    sel_time_points = time_md.get("selected", None)
    t_unit = time_md.get("unit", "?")
    logger.info(f"Original time-intervals (in {t_unit}) from JSON: {time_intervals(all_time_points)}")

    if sel_time_points is not None:
        logger.info(f"Got a list of 'selected' time-points from JSON: {sel_time_points}")
        logger.info(f"Corresponding time-intervals (in {t_unit}) from JSON: {time_intervals(sel_time_points)}")

    # Create the Markdown text:
    md = markdown_header(args.xp_id)
    md += markdown_info(int_imgs, seg_imgs, lineage_files, all_time_points, sel_time_points, t_unit,
                        args.lineage_format)

    # Save the Markdown text to a Markdown file:
    save_markdown(args.root_path, args.xp_id, md)
    if args.html:
        save_html(args.root_path, args.xp_id, md)

    logger.info("Done!")


def save_markdown(root_path, xp_id, md_text):
    """Save the text as Markdown."""
    md_file = root_path.joinpath(f"{xp_id}_dataset_info.md")
    logger.info(f"Saving Markdown file: '{md_file}'")
    with open(md_file, 'w') as mdf:
        mdf.writelines(md_text)
    return


def save_html(root_path, xp_id, md_text):
    """Save the text as Markdown."""
    html_file = root_path.joinpath(f"{xp_id}_dataset_info.html")
    logger.info(f"Saving HTML file: '{html_file}'")

    html = ""
    # HTML header:
    html += "<!DOCTYPE html>\n<html>\n"
    # Table styling:
    html += _add_css_styles([HTML_TABLE_STYLE])
    # HTML body:
    html += "<body>\n"
    html += markdown.markdown(md_text, extensions=['tables'])
    # HTML end of body:
    html += "\n</body>\n</html>"
    # Export HTML file:
    with open(html_file, 'w') as f:
        f.write(html)

    return


def markdown_header(dataset_name):
    """Generates the header for the Markdown file.

    Parameters
    ----------
    dataset_name : str
        The name of the dataset.

    Returns
    -------
    str
        A Markdown formatted text describing the dataset.
    """
    md = f"# Dataset {dataset_name}\n\n"  # Markdown text
    md += f"``Report generated on {datetime.now().strftime('%A %d %B %Y')}.``\n\n"
    return md


def markdown_info(int_imgs, seg_imgs, lineage_files, all_time_points, sel_time_points, t_unit, lin_fmt):
    """Create a Markdown text describing the dataset.

    Parameters
    ----------
    int_imgs : list of str
        The list of intensity image files.
    seg_imgs : list of str
        The list of segmented image files.
    lineage_files : list of str
        The list of cell lineage files.
    all_time_points : list
        The list of all time-points.
    sel_time_points : list
        The list of selected time-points.
    t_unit : str
        The time unit to use.
    lin_fmt : {'basic', 'dict', 'marsalt'}
        The name of the format to use to read the lineage file.

    Returns
    -------
    str
        A Markdown formatted text describing the dataset.
    """
    n_imgs = len(int_imgs)
    md = ""
    md += f"This dataset has {n_imgs} intensity images.\n\n"
    md += f"The original time-points (in {t_unit}) are: {', '.join(map(str, all_time_points))}.\n\n"
    # md += f"The original time-intervals (in {t_unit}) are: {', '.join(map(str, time_intervals(all_time_points)))}.\n\n"

    all_time_indexes = range(0, n_imgs)
    sel_time_indexes = None

    if sel_time_points is not None:
        sel_time_indexes = [all_time_points.index(tp) for tp in sel_time_points]
        md += f"A temporal subsampling was defined as follows: {', '.join(map(str, sel_time_points))}.\n\n"
        md += f"The corresponding list of selected time-indexes are: {', '.join(map(str, sel_time_indexes))}.\n\n"
        # md += f"The corresponding time-intervals (in {t_unit}) are: {', '.join(map(str, time_intervals(sel_time_points)))}.\n\n"

    logger.info("Retrieving original images info...")
    im_info, seg_im_info = None, None
    if int_imgs is not None:
        im_info = intensity_images_info(int_imgs, all_time_points, all_time_indexes, t_unit)
    if seg_imgs is not None:
        seg_im_info = segmented_images_info(seg_imgs, all_time_points, all_time_indexes, t_unit)

    md += "\n\n"
    md += "## Images information\n\n"

    if sel_time_points is not None:
        md += "### Original temporal sampling:\n\n"
    if int_imgs is not None:
        md += "Information about intensity images:\n\n"
        md += im_info.to_markdown()
        md += "\n\n"
    if seg_imgs is not None:
        md += "Information about segmented images:\n\n"
        md += seg_im_info.to_markdown()
        md += "\n\n"

    if sel_time_points is not None:
        md += "### Selected temporal sampling:\n\n"
        if int_imgs is not None:
            md += "Information about intensity images:\n\n"
            md += im_info[[f"t{ti}" for ti in sel_time_indexes]].to_markdown()
            md += "\n\n"
        if seg_imgs is not None:
            md += "Information about segmented images:\n\n"
            md += seg_im_info[[f"t{ti}" for ti in sel_time_indexes]].to_markdown()
            md += "\n\n"

    logger.info("Retrieving original lineages info...")
    ori_lin_info = lineage_info(lineage_files, lin_fmt, all_time_points, all_time_indexes, t_unit)
    if sel_time_points is not None:
        sub_lin_info = lineage_info(lineage_files, lin_fmt, all_time_points, sel_time_indexes, t_unit)

    md += "\n"
    md += "## Lineages information\n\n"

    if sel_time_points is not None:
        md += "### Original temporal sampling:\n\n"
    md += ori_lin_info.to_markdown()
    md += "\n\n"

    if sel_time_points is not None:
        md += "### Selected temporal sampling:\n\n"
        md += sub_lin_info.to_markdown()
        md += "\n\n"

    return md


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
