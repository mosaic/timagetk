#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Generic implementation of consecutive registration algorithms.

Should be applied to a time-series of intensity images.
It computes the transformations required to register ``I[t]``, an image at time ``t``, onto the next image ``I[t+1]``.
We thus obtain a series of _forward transformations_, as they allow to resample ``I[t]`` onto ``I[t+1]`` frame.

"""

import logging
import time
from pathlib import Path

import click
from functools import partial
from timagetk import MultiChannelImage
from timagetk.algorithms.blockmatching import blockmatching
from timagetk.algorithms.quaternion import centered_rotation_trsf
from timagetk.algorithms.reconstruction import image_surface_projection
from timagetk.algorithms.trsf import apply_trsf
from timagetk.algorithms.trsf import compose_trsf
from timagetk.array_util import guess_intensity_threshold
from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.io import imread
from timagetk.io import imsave
from timagetk.io import read_trsf
from timagetk.tasks.json_parser import DEFAULT_EXT
from timagetk.tasks.json_parser import DEFAULT_INTENSITY_DATASET
from timagetk.tasks.json_parser import DEFAULT_REGISTRATION
from timagetk.tasks.json_parser import DEFAULT_REGISTRATION_DATASET
from timagetk.tasks.json_parser import DEFAULT_TRSF_DATASET
from timagetk.tasks.json_parser import DEFAULT_INIT_TRSF_DATASET
from timagetk.tasks.json_parser import add2json
from timagetk.tasks.json_parser import json_file_exists
from timagetk.tasks.json_parser import time_intervals
from timagetk.tasks.json_parser import json_has_dataset
from timagetk.tasks.json_parser import json_intensity_image_parsing
from timagetk.tasks.json_parser import json_orientation_metadata
from timagetk.tasks.json_parser import json_time_metadata
from timagetk.tasks.json_parser import json_registration_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.json_parser import json_trsf_parsing
from timagetk.third_party.vt_parser import BLOCKMATCHING_METHODS
from timagetk.util import elapsed_time
from timagetk.visu.mplt import grayscale_imshow
from timagetk.visu.projection import PROJECTION_METHODS
from timagetk.visu.stack import stack_panel

DESCRIPTION = """Consecutive registration of a time-series.

Use image `I[t]` (image at time `t`) as float and image `I[t+1]` as reference to obtain a series of _forward transformations_.
Obtained transformations allow registering `I[t]` onto `I[t+1]` frame.
This can later be used by other CLI tools (working with temporal information) to improve results & visualisations.

This tool will produce the following data:

  1. a **series of consecutive transformations** allowing registration of `I[t]` onto `I[t+1]` frame.

with the flag `--save-imgs`:
  2. a **series of consecutive registered images**, that is `I[t]` registered onto `I[t+1]` frame.

This tool will produce the following figures:

  1. a panel view of the registration for each consecutive transformation

with the flag `--proj-method contour`:
  2. an "original/registered/reference" projections for each consecutive transformation

Except for the figures, the produced data is automatically referenced in the JSON file.

"""


def parse_list_float(ctx, param, value):
    if value:
        values = value.split()
        try:
            return [float(v) for v in values]
        except ValueError:
            raise click.BadParameter('All values must be valid floats separated by spaces.')
    return []


def parse_list_int(ctx, param, value):
    if value:
        values = value.split()
        try:
            return [int(v) for v in values]
        except ValueError:
            raise click.BadParameter('All values must be valid int separated by spaces.')
    return []


def parse_list_str(ctx, param, value, valid_str=None):
    if value:
        values = value.split()
        if valid_str:
            invalid_values = [val for val in values if val not in valid_str]
            if invalid_values:
                raise click.BadParameter(f"The following values are invalid: {invalid_values}. "
                                         f"Only {valid_str} are valid.")
        return values
    return []


@click.command(help=DESCRIPTION)
@click.argument('json-file', type=click.Path(exists=True))
@click.option('-int', '--intensity-dataset', type=str, default=DEFAULT_INTENSITY_DATASET,
              help=f"Name of the dataset that contains the intensity images to register, {DEFAULT_INTENSITY_DATASET} by default.")
@click.option('-t', '--time-index', type=str, callback=parse_list_int,
              help="Select images from the time-series using their time-index, all of them by default.")
@click.option('-trsf', '--init-trsf-dataset', type=str, default=DEFAULT_INIT_TRSF_DATASET,
              help=f"Name of the dataset that contains the initial transformations. {DEFAULT_INIT_TRSF_DATASET} by default.")
@click.option('--channel', type=str,
              help="Channel to perform prediction for, mandatory for multichannel images.")
@click.option('-orient', '--orientation', type=click.Choice(['-1', '1']), default='1',
              help="Image orientation, use `-1` with an inverted microscope, `1` by default.")
@click.option('-reg-type', '--registration-type', type=str,
              callback=partial(parse_list_str, valid_str=BLOCKMATCHING_METHODS),
              default=DEFAULT_REGISTRATION, help=f"Type of registrations to perform, only '{DEFAULT_REGISTRATION}' by"
                                                 f" default. Can specify multiple methods from {BLOCKMATCHING_METHODS}.")
@click.option('-direction', '--registration-direction', type=click.Choice(['forward', 'backward']),
              default='forward', help=f"Direction of registration, either `forward` (default) ie. t-->t+Δt or"
                                      f" `backward` ie. t+Δt -->t.")
@click.option('-py-hl', '--pyramid-highest-level', type=int, default=5,
              help="Highest level of the pyramid to use, 5 by default.")
@click.option('-py-ll', '--pyramid-lowest-level', type=int, default=2,
              help="Lowest level of the pyramid to use, 2 by default.")
@click.option('-py-hl-vx', '--pyramid-highest-voxel', type=float, default=None,
              help="Highest voxelsize of the pyramid to use, None by default.")
@click.option('-py-ll-vx', '--pyramid-lowest-voxel', type=float, default=None,
              help="Lowest voxelsize of the pyramid to use, None by default.")
@click.option('-init-z-rot', '--init-z-rotation', type=str, callback=parse_list_float,
              help="Apply a clockwise centered rotation along the z-axis to initialize registration."
                   " Accepts multiple angles with quotes (ie. '0 20 -23 0').")
@click.option('--proj-method', type=click.Choice(PROJECTION_METHODS + ['none']), default='none',
              help="Projection method to use. 'contour' works well with cell walls/membranes. 'maximum' works well"
                   " with nuclei. 'none' by default.")
@click.option('--ext', type=str, default=DEFAULT_EXT,
              help=f"File extension of registered images, '{DEFAULT_EXT}' by default.")
@click.option('--out-trsf-dataset', type=str, default=DEFAULT_TRSF_DATASET,
              help=f"Dataset name for output registered transformations, '{DEFAULT_TRSF_DATASET}' by default.")
@click.option('--out-img-dataset', type=str, default=DEFAULT_REGISTRATION_DATASET,
              help=f"Dataset name for output registered images, '{DEFAULT_REGISTRATION_DATASET}' by default.")
@click.option('--save-imgs', is_flag=False,
              help=f"Save the registered intensity images. Default is False. Output directory is defined by"
                   f" --out-img-dataset, {DEFAULT_REGISTRATION_DATASET} by default.")
@click.option('--out-dataset-suffix', type=str, default='',
              help=f"Dataset name suffix for output registered images & transformations. None by default")
@click.option('--force', is_flag=True,
              help="Force computation of transformations.")
@click.option('--log-level', 'log_level', type=click.Choice(LOG_LEVELS), default=DEFAULT_LOG_LEVEL,
              help=f"Logging level to use, '{DEFAULT_LOG_LEVEL}' by default.")
def main(json_file,
         intensity_dataset=DEFAULT_INTENSITY_DATASET,
         time_index=None,
         init_trsf_dataset=DEFAULT_INIT_TRSF_DATASET,
         channel=None,
         orientation='1',
         registration_type=DEFAULT_REGISTRATION,
         registration_direction='forward',
         pyramid_highest_level=5,
         pyramid_lowest_level=2,
         pyramid_highest_voxel=None,
         pyramid_lowest_voxel=None,
         init_z_rotation=None,
         proj_method='none',
         ext=DEFAULT_EXT,
         out_trsf_dataset=DEFAULT_TRSF_DATASET,
         out_img_dataset=DEFAULT_REGISTRATION_DATASET,
         save_imgs=False,
         out_dataset_suffix='',
         force=False,
         log_level=DEFAULT_LOG_LEVEL):
    # Get the name of the series
    series_md = json_series_metadata(json_file)
    time_md = json_time_metadata(json_file)
    xp_id = series_md['series_name']

    # Defines some location variables:
    root_path = Path(json_file).parent.absolute()  # the root directory is where the JSON file is...

    # Initialize logger
    logger = get_logger(__name__, root_path / f'{__name__}.log', log_level.upper())
    # Log first welcome message
    logger.info(f"Registration with blockmatching for time-series '{xp_id}' located under '{root_path}'.")

    # Get the dictionary of intensity images indexed by time-point
    intensity_imgs = json_intensity_image_parsing(json_file, intensity_dataset)
    n_imgs = len(intensity_imgs)
    logger.info(f"Found {n_imgs} intensity images under '{intensity_dataset}'.")
    logger.debug(intensity_imgs)

    # -- TIME POINTS - Try to get the 'selected' time-points from the JSON metadata:
    if time_index:
        wrong_index = [idx for idx in time_index if idx not in intensity_imgs.keys()]
        if wrong_index != []:
            logger.warning(f"Got some manually defined time-index that do not exists: {wrong_index}")
        time_index = [idx for idx in time_index if idx in intensity_imgs.keys()]
        logger.info(f"Selected a subset of time-points from JSON: {time_index} (out of {n_imgs} original images).")
    else:
        all_time_points = time_md.get("points", None)
        sel_time_points = time_md.get("selected", None)
        t_unit = time_md.get("unit", "?")  # TODO: raise something if time unit is not properly defined ?

        if sel_time_points is not None:
            logger.info(f"Got a list of 'selected' time-points from JSON: {sel_time_points}")
            time_index = [all_time_points.index(tp) for tp in sel_time_points]
            logger.info(f"Corresponding time-indexes: {time_index}")
        else:
            sel_time_points = all_time_points
            time_index = list(range(0, n_imgs))
        logger.info(f"Corresponding time-intervals (in {t_unit}) from JSON: {time_intervals(sel_time_points)}")

    # Re-index the time-series:
    intensity_imgs = {idx: img for idx, img in intensity_imgs.items() if idx in time_index}
    time_index = sorted(set(intensity_imgs.keys()))

    # Try to get the image/microscope 'orientation' from the JSON metadata:
    md_orientation = json_orientation_metadata(json_file, None)
    if md_orientation is not None:
        orientation = md_orientation
        logger.info(f"Got an image/microscope 'orientation' from JSON: `{orientation}`")

    if init_z_rotation:
        n_zrot = len(init_z_rotation)
        try:
            assert n_zrot == len(time_index)
        except AssertionError:
            raise ValueError(f"Found {n_zrot} z-rotations values for a time series of {len(time_index)} images!")
    else:
        init_z_rotation = [0] * n_imgs

    init_z_rotation = {idx: rot for idx, rot in zip(time_index, init_z_rotation)}

    # - Start the timer:
    t_start = time.time()

    # - Loop for the registration methods
    for reg_method in registration_type:
        logger.info(f"Starting consecutive registration task with {reg_method} method!")

        # Get the floating images index (no need for the last two images):
        float_index = time_index[:-1]

        # Form the output transformation directory
        current_out_trsf_dir = f"{out_trsf_dataset}-{reg_method}"

        if out_dataset_suffix:
            current_out_trsf_dir += f"-{out_dataset_suffix}"

        trsf_path = root_path.joinpath(current_out_trsf_dir)  # but needs absolute path to save files
        trsf_path.mkdir(exist_ok=True)  # make sure the directory exists

        # If needed, form the output images directory
        if save_imgs:
            current_out_img_dir = f"{out_img_dataset}-{reg_method}"

            if out_dataset_suffix:
                current_out_img_dir += f"-{out_dataset_suffix}"

            out_path = root_path.joinpath(current_out_img_dir)  # but needs absolute path to save files
            out_path.mkdir(exist_ok=True)  # make sure the directory exists

            # - JSON dictionary of output "registered images":
            # TODO: not really clear here, what is the wanted behavior ?
            if not json_has_dataset(json_file, current_out_img_dir) or force:
                # Clear the registry of all transformations (like non-consecutive ones)
                # as recomputing transformation should change the rest...
                trsf_json = {reg_method: {}}
                # Also clear the registry of registered images
                out_img_json = {reg_method: {}}
            else:
                # Load the current registry of registered images
                # (should not be cleared has we may skip some steps in the loop!)
                out_img_json = json_registration_parsing(json_file, current_out_img_dir)
                if reg_method not in out_img_json:
                    out_img_json[reg_method] = {}

        # Get the list of initial "transformations" between time-points from JSON.
        init_trsf_json = json_trsf_parsing(json_file, init_trsf_dataset)

        # TODO: need to change the trsf. database system to avoid the following ugly thing
        if init_trsf_json:
            init_trsf_json = list(init_trsf_json.values())[0]

        # Get the list of current "transformations" between time-points from JSON.
        trsf_json = json_trsf_parsing(json_file, current_out_trsf_dir)

        # Logging found rigid and requested "transformation" entries in JSON
        if reg_method not in trsf_json:
            trsf_json[reg_method] = {}
        n_trsfs = len(trsf_json[reg_method])
        logger.info(
            f"Found {n_trsfs} {reg_method} transformations under '{current_out_trsf_dir}'.")
        logger.debug(trsf_json[reg_method])

        for flo_idx in float_index:
            # Get the index of the reference image, that is the next one of the time-series
            ref_idx = flo_idx + 1

            if flo_idx >= n_imgs - 1:
                break

            if registration_direction == 'backward':
                flo_idx, ref_idx = ref_idx, flo_idx  # inverse the time-point index

            logger.info(f"Registering t{flo_idx} over t{ref_idx}...")
            time_interval = (flo_idx, ref_idx)

            # Try to see if the file exists from a previous run
            has_req_trsf = json_file_exists(trsf_json[reg_method], time_interval)

            if has_req_trsf and not force:
                logger.info(f"Found existing {reg_method} transformations!")
                continue

            # Load the floating image
            flo_img = imread(intensity_imgs[flo_idx], channel=channel)
            # Load the reference image
            ref_img = imread(intensity_imgs[ref_idx], channel=channel)

            # Create the initial rotation transformation if needed
            z_rot = init_z_rotation[flo_idx] - init_z_rotation[ref_idx]
            if z_rot != 0:
                init_trsf = centered_rotation_trsf(flo_img, z_rot, 'z')
            else:
                init_trsf = None

            # Add other initial transformation if provided
            has_init_trsf = False
            init_trsf_fname = None

            if json_file_exists(init_trsf_json, time_interval):
                init_trsf_fname = init_trsf_json[time_interval]
                init_trsf_reg = read_trsf(init_trsf_fname)

                if init_trsf is not None:
                    init_trsf = compose_trsf([init_trsf, init_trsf_reg])
                else:
                    init_trsf = init_trsf_reg

            # Performs blockmatching:
            trsf = blockmatching(flo_img, ref_img, method=reg_method,
                                 pyramid_lowest_level=pyramid_lowest_level,
                                 pyramid_highest_level=pyramid_highest_level,
                                 left_trsf=init_trsf)
            if init_trsf is not None:
                trsf = compose_trsf([init_trsf, trsf])

            # Save the transformation and add its reference (file name) to the JSON file:
            trsf_fname = f"{xp_id}-t{flo_idx}_t{ref_idx}-{reg_method}.trsf"
            trsf_json[reg_method][time_interval] = trsf_fname
            trsf.write(trsf_path.joinpath(trsf_fname))
            add2json(json_file, current_out_trsf_dir, trsf_json)

            # Save the process
            # TODO: add2json need to be refactored in order to 1) allow to write other json file than the main one
            # and 2) allow the complete filepath to be stored (only basename is saved right now)
            # A first try has been done, need to be checked
            process_trsf = {"floating_intensity_image": intensity_imgs[flo_idx],
                            "reference_intensity_image": intensity_imgs[ref_idx],
                            "registration_channel": channel,
                            "registration_method": reg_method,
                            "registration_direction": registration_direction,
                            "pyramid_lowest_level": pyramid_lowest_level,
                            "pyramid_highest_level": pyramid_highest_level,
                            "init_trsf": init_trsf_fname,
                            "init_z_rotation": z_rot}
            add2json(trsf_path.joinpath("processing.json"), "{}/{}".format(*time_interval), process_trsf,
                     required_object_metadata=False,
                     keep_basename=False)  # Keep complete filepath & no need for `object`

            # Save a visual of the registration quality (floating image registered onto the reference image)
            reg_img = apply_trsf(flo_img, trsf, template_img=ref_img)
            mc = MultiChannelImage([reg_img, ref_img], channel_names=['Registered', 'Reference'])
            step = mc.get_shape("z") // 6
            figname = f"{xp_id}-t{flo_idx}_on_t{ref_idx}-{reg_method}.png"
            stack_panel(mc, axis="z", start=step, step=step,
                        suptitle=f"{xp_id} - t{flo_idx} on t{ref_idx} - {reg_method} registration",
                        figname=trsf_path.joinpath(figname))

            # Save the registered image and add its reference (file name) to the JSON file:
            if save_imgs:
                img_fname = f"{xp_id}-t{flo_idx}_on_t{ref_idx}-{reg_method}.{ext}"
                out_img_json[reg_method][time_interval] = img_fname
                imsave(out_path.joinpath(img_fname), reg_img)
                add2json(json_file, current_out_img_dir, out_img_json)

                # Save the process
                process_img = {"intensity_image": intensity_imgs[flo_idx],
                               "reference_image": intensity_imgs[ref_idx],
                               "trsf": Path(current_out_trsf_dir).joinpath(trsf_fname)}
                add2json(out_path.joinpath("processing.json"), "{}/{}".format(*time_interval), process_img,
                         required_object_metadata=False,
                         keep_basename=False)  # Keep complete filepath & no need for `object`

        # Update the init_trsf_dataset to prepare the next possible registration
        init_trsf_dataset = current_out_trsf_dir
        init_z_rotation = {idx: 0 for idx in time_index}  # do not use for next registration

    if proj_method != 'none':  # avoid unwanted projection, for now...
        thresholds = {}
        projected_images = {}
        # Display results with 2D projections:
        for flo_idx in float_index:
            # Get the index of the reference image, that is the next one of the time-series
            ref_idx = flo_idx + 1

            if flo_idx >= n_imgs - 1:
                break

            if registration_direction == 'backward':
                flo_idx, ref_idx = ref_idx, flo_idx  # inverse the time-point index

            titles = []
            # Loading t_n original image:
            ori_img = imread(intensity_imgs[flo_idx], channel=channel)
            titles += [f"Original t{flo_idx}"]
            # Loading t_n on t_n+1 registered image:
            reg_img_fname = out_path.joinpath(out_img_json[reg_method][(flo_idx, ref_idx)])
            reg_img = imread(reg_img_fname)
            titles += [f"Registered t{flo_idx} on t{ref_idx}"]
            # Loading t_n+1 reference image:
            ref_img = imread(intensity_imgs[ref_idx], channel=channel)
            titles += [f"Original t{ref_idx}"]
            # Get/Compute t_n image intensity threshold:
            try:
                ori_th = thresholds[flo_idx]
            except KeyError:
                logging.info(f"Estimating intensity threshold for {intensity_imgs[flo_idx]}")
                thresholds[flo_idx] = guess_intensity_threshold(ori_img, logger=logger)
                ori_th = thresholds[flo_idx]
            # Get/Compute t_n+1 image intensity threshold:
            try:
                ref_th = thresholds[ref_idx]
            except KeyError:
                logging.info(f"Estimating intensity threshold for {intensity_imgs[ref_idx]}")
                thresholds[ref_idx] = guess_intensity_threshold(ref_img, logger=logger)
                ref_th = thresholds[ref_idx]
            # Get/Compute t_n image intensity projection:
            try:
                ori_proj = projected_images[flo_idx]
            except KeyError:
                logging.info(f"Projecting intensity image {intensity_imgs[flo_idx]}")
                projected_images[flo_idx], _ = image_surface_projection(ori_img, threshold=ori_th,
                                                                        orientation=orientation)
                ori_proj = projected_images[flo_idx]
            # Get/Compute t_n+1 image intensity projection:
            try:
                ref_proj = projected_images[ref_idx]
            except KeyError:
                logging.info(f"Projecting intensity image {intensity_imgs[ref_idx]}")
                projected_images[ref_idx], _ = image_surface_projection(ref_img, threshold=ref_th,
                                                                        orientation=orientation)
                ref_proj = projected_images[ref_idx]
            # Compute registered t_n image intensity projection:
            logging.info(f"Projecting registered intensity image {reg_img_fname}")
            reg_proj, _ = image_surface_projection(reg_img, threshold=ori_th, orientation=orientation)
            # Create the consecutive registration figure with intensity projections
            figname = f"{xp_id}-t{flo_idx}_{ref_idx}-consecutive_registration-{reg_method}.png"
            grayscale_imshow([ori_proj, reg_proj, ref_proj],
                             suptitle=f"Consecutive {reg_method} registration",
                             title=titles, method=proj_method, max_per_line=3,
                             figname=out_path.joinpath(figname))

    logger.info(f"All {elapsed_time(t_start)}")


if __name__ == '__main__':
    main()
