#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import json
from os.path import join
from pathlib import Path

import click

from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.tasks.json_parser import json_segmentation_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.json_parser import json_time_metadata
from timagetk.tasks.segmentation_comparison import overlap_analysis
from timagetk.tasks.segmentation_comparison import overlap_interpretation
from timagetk.tasks.segmentation_comparison import statistics_segmentation

CLI_NAME = __file__.split("/")[-1].split(".")[0]
OUT_DATASET = CLI_NAME


@click.command()
@click.argument('json_file', type=click.Path(exists=True, dir_okay=False, readable=True),
                help="Path to the JSON file for the reference segmentation experiment.")
@click.option("--rd", "--reference_dataset", type=click.Path(exists=True), required=True,
              help="Path to the reference segmentation dataset.")
@click.option("--cd", "--candidate_dataset", type=click.Path(exists=True), required=True,
              help="Path to the candidate segmentation dataset.")
@click.option("--json_candidate", type=click.Path(exists=True, dir_okay=False, readable=True), default=None,
              help="Path to the JSON file for the candidate segmentation experiment.")
@click.option('--out-dataset', type=str, default=OUT_DATASET, show_default=True,
              help=f"Name of the dataset to export the comparison results to.")
@click.option('--log-level', type=click.Choice(LOG_LEVELS), default=DEFAULT_LOG_LEVEL, show_default=True,
              help=f"Logging level to use.")
def main(json_file, reference, candidate, json_candidate, out_dataset, log_level):
    # Get the name of the series
    series_md = json_series_metadata(json_file)
    time_md = json_time_metadata(json_file)
    xp_id = series_md['series_name']
    # Defines some location variables:
    root_path = Path(json_file).parent.absolute()  # the root directory is where the JSON file is...
    out_path = root_path.joinpath(out_dataset)  # but needs absolute path to save files
    out_path.mkdir(exist_ok=True)  # make sure the directory exists

    # Initialize logger
    logger = get_logger(CLI_NAME, join(root_path, f'{CLI_NAME}.log'), log_level.upper())
    # Log first welcome message
    logger.info(f"Segmentation comparison for time-series '{xp_id}' located under '{root_path}'.")


    # --- INTENSITY IMAGES ---
    # Get the dictionary of intensity images indexed by time-point
    ref_seg_imgs = json_segmentation_parsing(json_file, reference)
    # Some logging:
    n_imgs = len(ref_seg_imgs)
    logger.info(f"Found {n_imgs} reference segmentation images under '{reference}'.")
    logger.debug(ref_seg_imgs)
    # Get the dictionary of intensity images indexed by time-point
    can_seg_imgs = json_segmentation_parsing(json_file, candidate)
    # Some logging:
    n_imgs = len(can_seg_imgs)
    logger.info(f"Found {n_imgs} candidate segmentation images under '{candidate}'.")
    logger.debug(can_seg_imgs)

    try:
        assert list(ref_seg_imgs.keys()) == list(can_seg_imgs.keys())
    except AssertionError:
        logger.error(f"The reference and candidate segmentation datasets do not have the same time-points!")
        raise AssertionError
    else:
        time_index = list(ref_seg_imgs.keys())
        logger.info(f"Comparing segmentation results for {len(time_index)} time-points.")

    for t_idx in time_index:
        seg_img_A = ref_seg_imgs[t_idx]
        seg_img_B = can_seg_imgs[t_idx]
        candidate_overlap, reference_overlap = overlap_analysis(seg_img_A, seg_img_B)
        out_df = overlap_interpretation(candidate_overlap, reference_overlap)
        cell_statistics = statistics_segmentation(out_df)

        # Save the processing pipeline and values
        with open(out_path.joinpath('overlap_interpretation.csv'), 'w') as proc_f:
            out_df.to_csv(proc_f, index=False)
        # Save the processing pipeline and values
        with open(out_path.joinpath('cell_statistics.json'), 'w') as proc_f:
            json.dump(cell_statistics, proc_f, indent=4)

    click.echo("Comparisons completed successfully.")


if __name__ == "__main__":
    main()
