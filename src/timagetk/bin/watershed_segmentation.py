#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
import json
import sys
from os.path import join
from pathlib import Path

from timagetk import TissueImage3D
from timagetk.algorithms.reconstruction import project_segmentation
from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.components.multi_channel import label_blending
from timagetk.io import imread
from timagetk.io import imsave
from timagetk.tasks.json_parser import DEFAULT_EXT
from timagetk.tasks.json_parser import DEFAULT_INTENSITY_DATASET
from timagetk.tasks.json_parser import DEFAULT_SEED_DATASET
from timagetk.tasks.json_parser import add2json
from timagetk.tasks.json_parser import json_intensity_image_parsing
from timagetk.tasks.json_parser import json_orientation_metadata
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.json_parser import json_time_metadata
from timagetk.tasks.json_parser import time_intervals
from timagetk.tasks.segmentation import seeded_watershed
from timagetk.tasks.segmentation import watershed_postprocessing
from timagetk.tasks.segmentation import watershed_preprocessing
from timagetk.third_party.vt_parser import DEF_LABELCHOICE
from timagetk.third_party.vt_parser import LABELCHOICE_METHODS
from timagetk.visu.mplt import grayscale_imshow
from timagetk.visu.stack import orthogonal_view
from timagetk.visu.stack import stack_panel
from timagetk.visu.util import relabel_8bits

DEFAULT_HMIN = 50
BACKGROUND_LABEL = 1
OUT_DATASET = "watershed_segmentation"

DESCRIPTION = """Performs watershed segmentation of an intensity image after detection of connexe local minima

We recommended to perform automatic global contrast stretching (-c) OR adaptive histogram equalization (-e) to improve overall segmentation quality.
It is done prior to the Gaussian smoothing step.
A Gaussian smoothing is applied prior to local minima detection to avoid detecting to many small local minima due to noise.
The (stretched/equalized) smoothed intensity image is used for seed detection (connexe local minima) AND watershed.
"""


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION,
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     epilog=f"Segmented images are stored in the '{OUT_DATASET}' dataset.")

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('-int', '--intensity_dataset', type=str, default=DEFAULT_INTENSITY_DATASET,
                        help=f"name of the dataset that contains the intensity images to segment, '{DEFAULT_INTENSITY_DATASET}' by default.")
    parser.add_argument('-seed', '--seed_dataset', type=str, default=DEFAULT_SEED_DATASET,
                        help=f"name of the dataset to save the seed images, '{DEFAULT_SEED_DATASET}' by default.")
    parser.add_argument('-index', '--time_index', type=int, nargs='+', default=[],
                        help="selection of images to segment, as a list of time-indexes, all of them by default.")

    img_arg = parser.add_argument_group('images arguments')
    img_arg.add_argument('--channel', type=str, default=None,
                         help="channel to register, mandatory for multichannel images.")
    img_arg.add_argument('--orientation', type=int, default=1, choices={-1, 1},
                         help="image orientation, use `-1` with an inverted microscope, `1` by default.")

    wat_arg = parser.add_argument_group('watershed arguments')
    wat_arg.add_argument('--h_min', type=int, nargs='+', default=[DEFAULT_HMIN],
                         help=f"height-minima value(s) to use per intensity image, can be a single integer or a list, '{DEFAULT_HMIN}' by default.")
    wat_arg.add_argument('--min_volume', type=float, default=None,
                         help="minimum acceptable cell volume, used in post-processing, `None` by default.")
    wat_arg.add_argument('--max_volume', type=float, default=None,
                         help="maximum acceptable cell volume, used in post-processing, `None` by default.")
    wat_arg.add_argument('--labelchoice', type=str, choices=LABELCHOICE_METHODS, default=DEF_LABELCHOICE,
                         help=f"how to deal with 'conflicts', where several labels meet, `{DEF_LABELCHOICE}` by default.")

    filter_arg = parser.add_argument_group('filtering arguments')
    filter_arg.add_argument('-s', '--sigma', type=float, nargs='+',
                            help="gaussian sigma value(s) to use per intensity image, can be a single float or a list.")
    filter_arg.add_argument('-e', '--equalize_hist', action="store_true",
                            help="perform histogram equalization of intensity images prior to Gaussian smoothing.")
    filter_arg.add_argument('-c', '--contrast_stretch', action="store_true",
                            help="perform contrasts stretching of intensity images prior to Gaussian smoothing.")

    out_arg = parser.add_argument_group('output arguments')
    out_arg.add_argument('--ext', default=DEFAULT_EXT,
                         help=f"file extension used to save seed & segmented images, '{DEFAULT_EXT}' by default.")
    out_arg.add_argument('--side_projections', action="store_true",
                         help="use it to generates a figure with extra side view projections (+X/+Y/-Y).")
    out_arg.add_argument('--seed_images', action="store_true",
                         help=f"use it to export seed images in a '{DEFAULT_SEED_DATASET}' dataset.")
    out_arg.add_argument('--out_dataset', type=str, default=OUT_DATASET,
                         help=f"name of the dataset to export the segmented images, '{OUT_DATASET}' by default.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default.")

    return parser


def ticked_histogram(data_dict, figname, vmini=None, vmaxi=None, **kwargs):
    from matplotlib import pyplot as plt
    dpi = kwargs.pop("dpi", None)
    fig, ax = plt.subplots(nrows=1, ncols=1, dpi=dpi)
    hist_size = kwargs.pop('hist_size', 5.)  # size of the matrix figure
    fig.set_size_inches(2.5 * hist_size, hist_size)  # width, height
    vect_data = list(data_dict.values())
    N, bins, patches = ax.hist(vect_data, **kwargs)
    ymini = - max(N) * 0.08
    ymaxi = max(N) * 1.05
    _ = ax.plot(vect_data, [ymini / 2] * len(vect_data), '|', color='gray')
    ax.set_ylim(ymini, ymaxi)
    ax.set_xlim(0, ax.get_xlim()[1])
    if vmini is not None:
        ax.vlines(vmini, *ax.get_ylim(), color='red', linestyles='dashed')
    if vmaxi is not None:
        ax.vlines(vmaxi, *ax.get_ylim(), color='red', linestyles='dashed')
    ax.set_title("Cell volume histogram.")
    plt.savefig(figname)


def watershed_segmentation(xp_id, args, logger):
    segmentations = {}  # segmentation file names dictionary, indexed by time-series index
    process = {}  # processing dictionary, indexed by time-series index
    logger.info("Starting cell-based segmentation task...")
    for im_idx, int_img_fname in args.intensity_imgs.items():
        process[im_idx] = {}
        process[im_idx]['intensity_image'] = int_img_fname  # keep track of the used intensity image
        # Load the intensity image file
        if args.channel is not None:
            logger.info(f"Loading channel '{args.channel}' of image file '{int_img_fname}' ({im_idx})...")
        else:
            logger.info(f"Loading single channel of image file '{int_img_fname}' ({im_idx})...")
        int_img = imread(int_img_fname, channel=args.channel)
        logger.info("Performing intensity image pre-processing...")
        int_img, p = watershed_preprocessing(int_img, args.sigma[im_idx], args.equalize_hist, args.contrast_stretch)
        orthogonal_view(int_img, suptitle=f"{int_img_fname} pre-processing",
                        figname=args.out_path.joinpath(f"preproc_{xp_id}_t{im_idx}.png"), figsize=(12, 12))

        logger.info("Performing seed-based watershed segmentation...")
        seg_img, seed_img, p = seeded_watershed(int_img, args.h_min[im_idx], labelchoice=args.labelchoice, process=p)

        logger.info("Computing cell volumes to generates distribution histogram...")
        # Instantiate a `TissueImage3D` object to compute the cell volumes:
        tissue = TissueImage3D(seg_img, background=BACKGROUND_LABEL)
        volumes = tissue.cells.volume(real=True)
        # Remove the background label from the dictionary of cell volumes
        try:
            volumes.pop(BACKGROUND_LABEL)
        except:
            pass

        try:
            png_name = args.out_path.joinpath(f"preproc_cell_volume_{xp_id}_t{im_idx}.png")
            ticked_histogram(volumes, bins=100, vmini=args.min_volume, vmaxi=args.max_volume, figname=png_name)
        except Exception as e:
            logger.error("An error occurred during creation of cell volume histogram!")
            logger.error(e)

        logger.info("Performing segmented image post-processing...")
        seg_img, seed_img, p = watershed_postprocessing(int_img, seg_img, seed_img, sizes_data=volumes,
                                                        min_size=args.min_volume, max_size=args.max_volume,
                                                        labelchoice=args.labelchoice, process=p)
        process[im_idx].update(p)  # keep track of the watershed process for this image

        # Instantiate a `TissueImage3D` object to compute the cell volumes:
        tissue = TissueImage3D(seg_img, background=BACKGROUND_LABEL)
        volumes = tissue.cells.volume(real=True)
        # Remove the background label from the dictionary of cell volumes
        try:
            volumes.pop(BACKGROUND_LABEL)
        except:
            pass

        try:
            png_name = args.out_path.joinpath(f"postproc_cell_volume_{xp_id}_t{im_idx}.png")
            ticked_histogram(volumes, bins=100, vmini=0, vmaxi=args.max_volume, figname=png_name)
        except Exception as e:
            logger.error("An error occurred during creation of cell volume histogram!")
            logger.error(e)

        # Save seed image if required
        if args.seed_images:
            seed_filename = f"{xp_id}_t{im_idx}_seeds.{args.ext}"
            imsave(args.seed_path.joinpath(seed_filename), seed_img)
            add2json(args.json, args.seed_dataset, {im_idx: str(seed_filename)})

        seg_filename = f"{xp_id}_t{im_idx}_segmented.{args.ext}"
        segmentations[im_idx] = seg_filename
        logger.info(f"Saving segmentation file '{seg_filename}' ({im_idx})...")
        imsave(args.out_path.joinpath(seg_filename), seg_img)
        add2json(args.json, args.out_dataset, segmentations)

        # Save the processing pipeline and values
        with open(args.out_path.joinpath('processing.json'), 'w') as proc_f:
            json.dump(process, proc_f, indent=4)

        logger.info("Saving projection maps of the segmented image...")
        max_cell_id = min(max(seg_img.labels()), 255)
        # Create a projection map of the segmented image
        proj_maps = []
        logger.info("Creating the +Z-axis projection map...")
        proj_maps += [project_segmentation(seg_img, 'z', orientation=args.orientation, return_background=False)]
        if args.side_projections:
            logger.info("Creating the +X-axis projection map...")
            proj_maps += [project_segmentation(seg_img, 'x', orientation=args.orientation, return_background=False)]
            logger.info("Creating the +Y-axis projection map...")
            proj_maps += [project_segmentation(seg_img, 'y', orientation=args.orientation, return_background=False)]
            logger.info("Creating the -Y-axis projection map...")
            proj_maps += [project_segmentation(seg_img, 'y', orientation=-args.orientation, return_background=False)]

            # Re-labelling to 8bit values to be able to use 'Glasbey' colormap
            for pi, proj in enumerate(proj_maps):
                proj_maps[pi] = relabel_8bits(proj)

            png_name = f"{xp_id}_t{im_idx}_segmented_projections.png"
            png_name = args.out_path.joinpath(png_name)
            grayscale_imshow(proj_maps, suptitle="Segmentation projection map",
                             title=["z-projection", "x-projection", "y-projection", "-y-projection"],
                             cmap='glasbey', val_range=[0, max_cell_id], max_per_line=2, thumb_size=8, figname=png_name)

        png_name = f"{xp_id}_t{im_idx}_segmented_zproj.png"
        png_name = args.out_path.joinpath(png_name)
        grayscale_imshow(proj_maps[0], suptitle="z-projection map",
                         cmap='glasbey', val_range=[0, max_cell_id], figname=png_name)

        png_name = f"{xp_id}_t{im_idx}_blend_panels.png"
        png_name = args.out_path.joinpath(png_name)
        int_img = imread(int_img_fname, channel=args.channel)
        blend = label_blending(seg_img, int_img)
        z_shape = blend.get_shape('z')
        n_panels = 10
        inc = z_shape // (n_panels+2)
        stack_panel(blend, start=inc, step=inc, stop=inc+n_panels*inc, thumb_size=8., max_per_line=n_panels//2,
                    suptitle="Intensity & labelled image blending", figname=png_name)


def run():
    parser = parsing()
    args = parser.parse_args()

    # Get the name of the series
    series_md = json_series_metadata(args.json)
    time_md = json_time_metadata(args.json)
    xp_id = series_md['series_name']
    # Defines some location variables:
    args.root_path = Path(args.json).parent.absolute()  # the root directory is where the JSON file is...
    args.out_path = args.root_path.joinpath(args.out_dataset)  # but needs absolute path to save files
    args.out_path.mkdir(exist_ok=True)  # make sure the directory exists
    args.seed_path = args.root_path.joinpath(args.seed_dataset)  # but needs absolute path to save files
    if args.seed_images:
        args.seed_path.mkdir(exist_ok=True)  # make sure the directory exists

    # Initialize logger
    logger = get_logger('watershed_segmentation', join(args.root_path, 'watershed_segmentation.log'), args.log_level.upper())
    # Log first welcome message
    logger.info(f"Watershed segmentation for time-series '{xp_id}' located under '{args.root_path}'.")

    # Get the dictionary of intensity images indexed by time-point
    args.intensity_imgs = json_intensity_image_parsing(args.json, args.intensity_dataset)
    n_imgs = len(args.intensity_imgs)
    logger.info(f"Found {n_imgs} intensity images under '{args.intensity_dataset}'.")
    logger.debug(args.intensity_imgs)

    # -- TIME POINTS - Try to get the 'selected' time-points from the JSON metadata:
    if args.time_index != []:
        wrong_index = [idx for idx in args.time_index if idx not in args.intensity_imgs.keys()]
        if wrong_index != []:
            logger.warning(f"Got some manually defined time-index that do not exists: {wrong_index}")
        args.time_index = [idx for idx in args.time_index if idx in args.intensity_imgs.keys()]
    else:
        all_time_points = time_md.get("points", None)
        sel_time_points = time_md.get("selected", None)
        t_unit = time_md.get("unit", "?")  # TODO: raise something if time unit is not properly defined ?
        if sel_time_points is not None:
            logger.info(f"Got a list of 'selected' time-points from JSON: {sel_time_points}")
            args.time_index = [all_time_points.index(tp) for tp in sel_time_points]
            logger.info(f"Corresponding time-indexes: {args.time_index}")
        else:
            sel_time_points = all_time_points
            args.time_index = list(range(0, n_imgs))
        logger.info(f"Corresponding time-intervals (in {t_unit}) from JSON: {time_intervals(sel_time_points)}")
    # Re-index the time-series:
    args.intensity_imgs = {idx: img for idx, img in args.intensity_imgs.items() if idx in args.time_index}

    # Check the list of `sigma` values to use:
    sigma = [None] * n_imgs
    if args.sigma:
        if len(args.sigma) == 1 and isinstance(args.sigma[0], float):
            sigma = args.sigma * n_imgs
        else:
            try:
                assert len(args.sigma) == n_imgs
            except AssertionError:
                logger.error("There should be as many sigma values as intensity images...")
                logger.error(f"Found {n_imgs} intensity images but got {len(args.sigma)} sigma values.")
                sys.exit("Error with `sigma` definition!")
            else:
                sigma = args.sigma
        logger.info(f"Manually defined Gaussian sigma values: {sigma}")
    args.sigma = sigma

    # Check the list of `h_min` values to use:
    h_min = [DEFAULT_HMIN] * n_imgs
    if args.h_min:
        if len(args.h_min) == 1 and isinstance(args.h_min[0], int):
            h_min = args.h_min * n_imgs
        else:
            try:
                assert len(args.h_min) == n_imgs
            except AssertionError:
                logger.error("There should be as many h-minima values as intensity images...")
                logger.error(f"Found {n_imgs} intensity images but got {len(args.h_min)} h-minima values.")
                sys.exit("Error with `h-minima` definition!")
            else:
                h_min = args.h_min
        logger.info(f"Manually defined h-minima values: {h_min}")
    args.h_min = h_min

    # Try to get the image/microscope 'orientation' from the JSON metadata:
    orientation = json_orientation_metadata(args.json, None)
    if orientation is not None:
        args.orientation = orientation
        logger.info(f"Got an image/microscope 'orientation' from JSON: `{args.orientation}`")

    watershed_segmentation(xp_id, args, logger)


if __name__ == '__main__':
    run()
