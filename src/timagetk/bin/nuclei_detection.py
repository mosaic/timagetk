#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
from pathlib import Path

import numpy as np

from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.bin.tasks import check_time_index
from timagetk.components.multi_channel import MultiChannelImage
from timagetk.io import imread
from timagetk.tasks.json_parser import DEFAULT_INTENSITY_DATASET
from timagetk.tasks.json_parser import DEFAULT_NUCLEI_DATASET as OUT_DATASET
from timagetk.tasks.json_parser import add2json
from timagetk.tasks.json_parser import json_channel_names
from timagetk.tasks.json_parser import json_intensity_image_parsing
from timagetk.tasks.json_parser import json_registration_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.nuclei_detection import nuclei_detection

DEFAULT_TH = 3000  # Valid for a 18-bit image.
DEF_RADIUS_RANGE = (0.8, 1.4)
DEF_RADIUS_STEP = 0.2

DESCRIPTION = """Performs nuclei detection on an intensity image using detection of local intensity peaks.

We recommended to perform automatic global contrast stretching (-c) OR adaptive histogram equalization (-e) to improve overall detection quality.
It is done prior to the detection of local intensity peaks.

When using a registered dataset you may specify the type of registered image you want to analyse by adding it to the dataset name after a '/', *e.g.* 'temporal_registration/rigid'.
"""
cli_name = Path(__file__).stem


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION,
                                     epilog=f"The coordinates of the detected nuclei are stored in the '{OUT_DATASET}' dataset.")

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('-d', '--dataset', type=str, default=DEFAULT_INTENSITY_DATASET,
                        help=f"the dataset to use as intensity image input for detection algorithm, '{DEFAULT_INTENSITY_DATASET}' by default.")
    parser.add_argument('-t', '--time_index', type=list, default=[],
                        help="the time indexes of the image to use with the detection procedure, all of them by default.")

    wat_arg = parser.add_argument_group('local peak detection arguments')
    wat_arg.add_argument('--threshold', type=int, nargs='+', default=[DEFAULT_TH],
                         help=f"threshold value to use for peak detection, '{DEFAULT_TH}' by default")
    wat_arg.add_argument('--radius_range', type=tuple, nargs='+', default=[DEF_RADIUS_RANGE],
                         help=f"range of nuclei size to detect, '{DEF_RADIUS_RANGE}' by default")
    wat_arg.add_argument('--radius_step', type=float, nargs='+', default=[DEF_RADIUS_STEP],
                         help=f"step of radius range, '{DEFAULT_TH}' by default")

    filter_arg = parser.add_argument_group('filtering arguments')
    filter_arg.add_argument('-e', '--equalize_hist', action="store_true",
                            help="performs histogram equalization of intensity images prior to Gaussian smoothing")
    filter_arg.add_argument('-c', '--contrast_stretch', action="store_true",
                            help="performs contrasts stretching of intensity images prior to Gaussian smoothing")

    out_arg = parser.add_argument_group('output arguments')
    out_arg.add_argument('--out_dataset', type=str, default=OUT_DATASET,
                         help=f"export segmented images in a '{OUT_DATASET}' dataset")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default")

    log_arg = parser.add_argument_group('image arguments')
    log_arg.add_argument('--channel', type=str,
                         help="channel to use with a multichannel image.")

    return parser


def main(args):
    # Get the name of the series
    series_md = json_series_metadata(args.json)
    xp_id = series_md['series_name']
    # Defines some location variables:
    args.root_path = Path(args.json).parent.absolute()  # the root directory is where the JSON file is...
    args.out_path = args.root_path.joinpath(args.out_dataset)  # but needs absolute path to save files
    args.out_path.mkdir(exist_ok=True)  # make sure the directory exists

    # Initialize logger
    logger = get_logger(cli_name, args.root_path.joinpath(f'{cli_name}.log'), args.log_level.upper())
    # Log first welcome message
    logger.info(f"Nuclei detection for time-series '{xp_id}' located under '{args.root_path}'.")

    # Get the list of nuclei images per time-point
    if 'registration' in args.dataset:
        args.dataset, trsf_type = args.dataset.split("/")
        intensity_imgs = json_registration_parsing(args.json, args.dataset)[trsf_type]
    else:
        intensity_imgs = json_intensity_image_parsing(args.json, args.dataset)
    n_imgs = len(intensity_imgs)
    logger.info(f"Found {n_imgs} intensity images under '{args.dataset}'.")
    logger.debug(intensity_imgs)
    # Try to load channel names:
    try:
        ch_names = json_channel_names(args.json)
    except:
        ch_names = []

    threshold = [DEFAULT_TH] * n_imgs
    if args.threshold:
        if len(args.threshold) == 1 and isinstance(args.threshold[0], int):
            threshold = args.threshold * n_imgs
        else:
            try:
                assert len(args.threshold) == n_imgs
            except AssertionError:
                logger.error("There should be as many threshold values as intensity images...")
                logger.error(f"Found {n_imgs} intensity images but got {len(args.threshold)} threshold values.")
                exit(1)
            else:
                threshold = args.threshold
        logger.info(f"Manually defined threshold values: {threshold}")

    radius_range = [DEF_RADIUS_RANGE] * n_imgs
    if args.radius_range:
        if len(args.radius_range) == 1 and isinstance(args.radius_range[0], tuple):
            radius_range = args.radius_range * n_imgs
        else:
            try:
                assert len(args.radius_range) == n_imgs
            except AssertionError:
                logger.error("There should be as many radius range tuples as intensity images...")
                logger.error(f"Found {n_imgs} intensity images but got {len(args.radius_range)} radius range tuples.")
                exit(1)
            else:
                radius_range = args.radius_range
        logger.info(f"Manually defined radius range tuples: {radius_range}")

    radius_step = [DEF_RADIUS_STEP] * n_imgs
    if args.radius_step:
        if len(args.radius_step) == 1 and isinstance(args.radius_step[0], float):
            radius_step = args.radius_step * n_imgs
        else:
            try:
                assert len(args.radius_step) == n_imgs
            except AssertionError:
                logger.error("There should be as many radius step values as intensity images...")
                logger.error(f"Found {n_imgs} intensity images but got {len(args.radius_step)} radius step values.")
                exit(1)
            else:
                radius_step = args.radius_step
        logger.info(f"Manually defined radius step values: {radius_step}")

    # Takes care of time index
    args.time_index = check_time_index(args.time_index, n_imgs)
    # If a registration, compute for consecutive time index:
    if 'registration' in args.dataset:
        args.time_index = list(zip(args.time_index[:-1], args.time_index[1:]))
        #
        threshold = dict(zip(args.time_index, threshold))
        radius_range = dict(zip(args.time_index, radius_range))
        radius_step = dict(zip(args.time_index, radius_step))
    logger.info(f"Selected time index{'es' if len(args.time_index) > 1 else ''}: {args.time_index}")

    coordinates = {}  # time-indexed dictionary of nuclei coordinates
    process = {}  # time-indexed dictionary of processing metadata
    logger.info("Starting nuclei detection task!")
    for im_id, int_img_fname in intensity_imgs.items():
        if im_id not in args.time_index:
            logger.info(f"Skipping time-index {im_id}")
            continue

        process[im_id] = {}
        process[im_id]['file'] = int_img_fname
        # Load the intensity image file
        logger.info(f"Loading image file '{int_img_fname}' ({im_id})...")
        int_img = imread(int_img_fname)
        if isinstance(int_img, MultiChannelImage):
            if ch_names is not None:
                for old, new in zip(int_img.get_channel_names(), ch_names):
                    logger.info(f"Changing default channel name '{old}' to '{new}'")
                    int_img.set_channel_name(old, new)
            logger.info(f"Got a MultiChannelImage with channels: {int_img.get_channel_names()}")
            int_img = int_img.get_channel(args.channel)
        nuclei_array, p = nuclei_detection(int_img, threshold=threshold[im_id],
                                           radius_range=radius_range[im_id], step=radius_step[im_id],
                                           equalize_hist=args.equalize_hist, contrast_stretch=args.contrast_stretch)
        n_nuclei = nuclei_array.shape[0]
        process[im_id].update(p)

        seg_filename = f"{xp_id}_t{im_id}_nuclei_coordinates.txt"
        coordinates[im_id] = seg_filename
        np.savetxt(args.out_path.joinpath(seg_filename), nuclei_array)
        add2json(args.json, args.out_dataset, coordinates)

        # Create a projection map of the segmented image
        png_name = f"{xp_id}_t{im_id}_nuclei_projection.png"
        png_name = args.out_path.joinpath(png_name)
        import matplotlib.pyplot as plt
        plt.figure(figsize=(15, 15))
        # Show X & Y coordinates of detected nuclei
        plt.scatter(nuclei_array[:, 0], nuclei_array[:, 1], c=range(1, n_nuclei + 1), cmap='glasbey')
        # Add maximum intensity projection of the nuclei signal
        plt.imshow(int_img.max(axis=0), cmap='gray',
                   extent=(0, int_img.extent[2], int_img.extent[1], 0))
        plt.savefig(png_name)
        plt.close()


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
