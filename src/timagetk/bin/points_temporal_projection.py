import argparse
import logging
from pathlib import Path

import pandas as pd

from timagetk.algorithms.blockmatching import blockmatching
from timagetk.algorithms.pointmatching import apply_trsf_to_points
from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.components.multi_channel import MultiChannelImage
from timagetk.io import imread

CLI_NAME = Path(__file__).stem


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(
        description='Temporal prediction of points position using non-linear registration of cell-wall intensity images.',
        epilog="Output a CSV file of predicted XYZ coordinates in the same directory."
    )

    parser.add_argument('pts', type=str,
                        help="path to the CSV file containing the points to register, must be XYZ coordinates.")
    parser.add_argument('ref', type=str,
                        help="path to the reference image to load.")
    parser.add_argument('float', type=str,
                        help="path to the float image to load.")
    parser.add_argument('-c', '--channel', type=str, default=None,
                        help=f"name of the channel to load if a multichannel image, 'None' by default.")
    parser.add_argument('-l', '--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                        help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default")

    return parser


def load_image(img_path, channel=None):
    """Load a 3D intensity image.

    Parameters
    ----------
    img_path : str
        Path to the image to load.
    channel: str, optional
        Name of the channel to load if a multichannel image.

    Returns
    -------
    timagetk.SpatialImage
        The loaded 3D intensity image.
    """
    image = imread(img_path)
    if isinstance(image, MultiChannelImage):
        image = image.get_channel(channel)
    return image


def points_temporal_projection(pts_path, ref_img_path, float_img_path, channel=None):
    """Temporal prediction of points position using non-linear registration of cell-wall intensity images.

    Parameters
    ----------
    pts_path : str
        Path to the CSV file containing the points to register.
    ref_img_path : str
        Path to the reference image to load.
    float_img_path : str
        Path to the float image to load.
    channel: str, optional
        Name of the channel to load if a multichannel image.

    Returns
    -------
    numpy.ndarray
        Predicted point location in the floating image frame.
    """
    logger = logging.getLogger(CLI_NAME)
    # Get the array of XYZ points from the CSV:
    logger.info(f"Loading the original points from `{pts_path}`...")
    pts = pd.read_csv(pts_path).to_numpy()
    # Load the images:
    logger.debug(f"Loading the reference image from `{ref_img_path}`...")
    reference_image = load_image(ref_img_path, channel=channel)
    logger.debug(f"Loading the floating image from `{float_img_path}`...")
    floating_image = load_image(float_img_path, channel=channel)

    # Registration to compute the transformation T(flo <- ref):
    logger.debug("Estimating rigid transformation matrices...")
    trsf_rigid = blockmatching(floating_image, reference_image,
                               method="rigid", pyramid_lowest_level=2, quiet=True)
    logger.debug("Estimating non-linear transformation matrices...")
    trsf_def = blockmatching(floating_image, reference_image,
                             method="vectorfield", init_trsf=trsf_rigid, pyramid_lowest_level=3, quiet=True)
    # Apply the transformation T(flo <- ref) to the reference points to predict their position in floating image frame:
    logger.debug("Applying transformation to original points")
    pred_nuclei_pts = apply_trsf_to_points(pts, trsf_def)
    return pred_nuclei_pts


def main(args):
    # Get the directory where the original points are located:
    root_path = Path(args.pts).parent
    # Initialize logger
    logger = get_logger(CLI_NAME, root_path.joinpath(f'{CLI_NAME}.log'), args.log_level.upper())
    # Compute the temporal projection of the points:
    pred_pts = points_temporal_projection(args.pts, args.ref, args.float, args.channel)
    # Export the array of XYZ points to a CSV in same directory:
    out_fname = root_path / "predicted_points.csv"
    logger.info(f"Writing predicted point positions to `{out_fname}`.")
    pd.DataFrame(pred_pts).to_csv(out_fname, index=False, header=False)
    return


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)
    return


if __name__ == "__main__":
    run()
