#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Tools to generate 2D intensity projections for a time series of grayscale images."""

import argparse
import logging
import sys
import time
from os.path import join
from os.path import split
from pathlib import Path

from timagetk import LabelledImage
from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.tasks.json_parser import DEFAULT_INTENSITY_DATASET
from timagetk.tasks.json_parser import DEFAULT_REGISTRATION_DATASET
from timagetk.tasks.json_parser import DEFAULT_SEGMENTATION_DATASET
from timagetk.tasks.json_parser import DEFAULT_TRSF_DATASET
from timagetk.tasks.json_parser import json_intensity_image_parsing
from timagetk.tasks.json_parser import json_orientation_metadata
from timagetk.tasks.json_parser import json_segmentation_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.json_parser import json_trsf_parsing
from timagetk.util import elapsed_time
from timagetk.visu.animations import animate_time_series
from timagetk.visu.projection import PROJECTION_METHODS

# Default projection method:
DEF_METHOD = 'contour'
# Default number of frames per second:
DEF_FPS = 1.
# Default intensity threshold:
DEF_INT_THRESHOLD = None

ANIM_EXT = ['gif', 'mp4']
DEFAULT_EXT = 'gif'
OUT_DATASET = DEFAULT_REGISTRATION_DATASET


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(
        description='Temporal sequence animation using registered intensity images in 2D projections.',
        epilog="File extensions can be either 'gif' or 'mp4'.")

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('-int', '--intensity_dataset', type=str, default=DEFAULT_INTENSITY_DATASET,
                        help=f"the name of the dataset containing the intensity images, '{DEFAULT_INTENSITY_DATASET}' by default. Used to get the last image of the time-series.")
    parser.add_argument('-seg', '--segmentation_dataset', type=str, default=DEFAULT_SEGMENTATION_DATASET,
                        help=f"the dataset containing the segmented image on which to project the dataset, '{DEFAULT_SEGMENTATION_DATASET}' by default.")
    parser.add_argument('-trsf', '--trsf_dataset', type=str, default=DEFAULT_TRSF_DATASET,
                        help=f"name of the dataset containing the transformations, '{DEFAULT_TRSF_DATASET}' by default.")

    # optional arguments:
    # -- image arguments:
    img_arg = parser.add_argument_group('images arguments')
    img_arg.add_argument('-c', '--channel', type=str, default='',
                         help="channel to register, mandatory for multichannel images.")
    img_arg.add_argument('--orientation', type=int, default=1, choices={-1, 1},
                         help="image stack orientation, use `-1` with an inverted microscope, `1` by default.")

    proj_arg = parser.add_argument_group('projection arguments')
    proj_arg.add_argument('--method', type=str, default=DEF_METHOD, choices=PROJECTION_METHODS,
                          help=f"method to use for 2D projection of 3D intensity image, '{DEF_METHOD}' by default.")
    proj_arg.add_argument('--save_projections', action='store_true',
                          help="use this option to export individual 2D projections as PNG.")
    proj_arg.add_argument('--threshold', type=int, default=DEF_INT_THRESHOLD,
                          help=f"defines the intensity threshold to use for 'contour' projections, default is {DEF_INT_THRESHOLD} which is the 75-th percentile of the intensities distribution.")
    proj_arg.add_argument('--axis', type=str, default="z",
                          help="select axis to use for projection, 'z' by default.")

    vid_arg = parser.add_argument_group('animation arguments')
    vid_arg.add_argument('--fps', type=float, default=DEF_FPS,
                         help=f"number of frames per second of the animation, default is {DEF_FPS}.")
    vid_arg.add_argument('--bitrate', type=float, default=-1,
                         help="bitrate for the saved movie file, which is one way to control the output file size and quality.")
    vid_arg.add_argument('--duration', type=float, default=5.,
                         help="bitrate for the saved movie file, which is one way to control the output file size and quality.")

    out_arg = parser.add_argument_group('output arguments')
    out_arg.add_argument('--ext', type=str, default=DEFAULT_EXT, choices=ANIM_EXT,
                         help=f"extension used to save animation, '{DEFAULT_EXT}' by default.")
    out_arg.add_argument('--force', action="store_true",
                         help="force computation of transformations.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default.")

    return parser


def project_time_series(xp_id, abs_out_path, img_fnames, args, registered, trsfs=None, rtype=None, **kwargs):
    """Project the time-series and make animations.

    Parameters
    ----------
    xp_id : str
        Dataset id, used to create generic file names.
    abs_out_path : str
        Path where to save the animations and 2D projections.
    img_fnames : dict
        Time indexed dictionary of image file names to project.
    args : dict
        Dictionary of parsed keyword argument.
    registered : bool
        Indicate if the images are registered or not.
    trsfs : dict of Trsf, optional
        If specified, use this time interval indexed dictionary of transformations to register images prior to projection.
    rtype : ??
        The type of images, *e.g.* ``SpatialImage`` or ``LabelledImage``.

    """
    from os.path import exists
    # - MIPs generation:
    if registered:
        anim_fname = f"{xp_id}-time_series.{args.ext}"
    else:
        anim_fname = f"{xp_id}-time_series-unregistered.{args.ext}"

    if rtype == LabelledImage:
        kwargs['cmap'] = 'glasbey'
    else:
        kwargs['cmap'] = 'gray'
        kwargs['method'] = args.method
        kwargs['threshold'] = args.threshold

    kwargs['axis'] = args.axis
    kwargs['duration'] = args.duration
    kwargs['orientation'] = args.orientation

    anim_fname = join(abs_out_path, anim_fname)
    if exists(anim_fname) and not args.force:
        logging.warning(f"Detected existing animation: '{anim_fname}'")
        logging.info("Skip the animation creation (can be forced)!")
        return

    proj_list = animate_time_series(img_fnames, anim_fname, trsfs=trsfs, rtype=rtype, **kwargs)

    # -- Export PNGs for each 2D projection:
    if args.save_projections:
        from imageio import imsave
        for n, im in enumerate(proj_list):
            f = Path(img_fnames[n])
            if registered:
                suffix = f"_{args.method}_rigid_registered.png"
            else:
                suffix = f"_{args.method}_unregistered.png"
            fname = join(abs_out_path, f.stem + suffix)
            logging.info(f"Saving 2D projection: {fname}")
            imsave(fname, im, format="png")

    return


def main(args):
    """Main method to generate 2D intensity projections for a time series of grayscale images."""
    # Get the name of the series
    series_md = json_series_metadata(args.json)
    xp_id = series_md['series_name']
    # Defines some location variables:
    json_root_path, _ = split(args.json)  # the root directory is where the JSON file is...

    # Initialize logger
    logger = get_logger('temporal_projection', join(json_root_path, 'temporal_projection.log'), args.log_level.upper())
    # Log first welcome message
    logger.info(f"Temporal projections for time-series '{xp_id}' located under '{json_root_path}'.")

    # Get the dictionary of intensity images indexed by time-point
    int_imgs = json_intensity_image_parsing(args.json, args.intensity_dataset)
    logger.info(f"Found {len(int_imgs)} files under '{args.intensity_dataset}'.")

    # Get the dictionary of segmented images indexed by time-point
    seg_imgs = json_segmentation_parsing(args.json, args.segmentation_dataset)
    logger.info(f"Found {len(seg_imgs)} segmented images under '{args.segmentation_dataset}'.")
    logger.debug(seg_imgs)

    # Check the consistency of number of intensity and segmented images:
    try:
        assert len(int_imgs) == len(seg_imgs)
    except AssertionError:
        logger.error("There should be the same number of intensity and segmented images!")
        logger.error(f"Found {len(int_imgs)} intensity images!")
        logger.error(f"Found {len(seg_imgs)} segmented images!")
        sys.exit("Check the intensity and segmented images dataset!")

    n_imgs = len(int_imgs)

    # Get the dictionary of intensity images indexed by time intervals (len-2 tuples)
    trsf_json = json_trsf_parsing(args.json, args.trsf_dataset)
    # Check the consistency of number of intensity images and transformations:
    required_regs = [(t, t + 1) for t in range(n_imgs - 1)]
    trsf_type = 'rigid'
    trsfs = trsf_json[trsf_type]
    try:
        assert [reg in trsfs for reg in required_regs]
    except AssertionError:
        logger.critical(f"Could not find all the required consecutive {trsf_type} transformations!")
        logger.critical(
            f"Missing consecutive {trsf_type} transformations for intervals: {[reg not in trsfs for reg in required_regs]}")
        logger.info("Run `consecutive_registration.py` prior to using `sequence_registration.py`!")
        required_regs = None
    else:
        n_trsfs = len(required_regs)
        logger.info(
            f"Found the {n_trsfs} required consecutive {trsf_type} transformations under '{args.trsf_dataset}'.")
        logger.debug(trsfs)
        # Check the consistency of number of trsf & image files
        try:
            assert n_trsfs + 1 == n_imgs
        except AssertionError:
            logger.error(
                f"Found {n_trsfs} consecutive {trsf_type} transformations for a time series of {n_imgs} images!")

    args.intensity_dataset = join(json_root_path, args.intensity_dataset)
    args.segmentation_dataset = join(json_root_path, args.segmentation_dataset)

    # Try to get the image/microscope 'orientation' from the JSON metadata:
    orientation = json_orientation_metadata(args.json, None)
    if orientation is not None:
        args.orientation = orientation
        logger.info(f"Got an image/microscope 'orientation' from JSON: `{args.orientation}`")

    ############################################################################
    # - Start the timer:
    t_start = time.time()

    # Project unregistered time-series:
    logger.info(f"Starting intensity images '{args.method}' projection...")
    project_time_series(xp_id, args.intensity_dataset, int_imgs, args, registered=False)
    logger.info("Starting segmented images projection...")
    project_time_series(xp_id, args.segmentation_dataset, seg_imgs, args, registered=False, rtype=LabelledImage)

    if required_regs is not None:
        # Project rigid registered time-series:
        logger.info(f"Starting registered intensity images '{args.method}' projection...")
        project_time_series(xp_id, args.intensity_dataset, int_imgs, args, registered=True, trsfs=trsfs)
        logger.info("Starting registered segmented images projection...")
        project_time_series(xp_id, args.segmentation_dataset, seg_imgs, args, registered=True, trsfs=trsfs,
                            rtype=LabelledImage)

    logger.info(f"All {elapsed_time(t_start)}")


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
