#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
import sys
from itertools import combinations
from pathlib import Path

from timagetk.third_party.ctrl.io import AVAILABLE_LINEAGE_FMT
from timagetk.third_party.ctrl.io import DEFAULT_LINEAGE_FMT
from timagetk import TissueImage3D
from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.bin.tasks import check_temporal_rank
from timagetk.bin.tasks import get_trsf_files
from timagetk.bin.tasks import load_lineage_from_files
from timagetk.bin.tasks import load_trsf_from_files
from timagetk.components.tissue_image import cell_layers_from_image
from timagetk.gui.lineage_views import lineage_viewer
from timagetk.io import imread
from timagetk.tasks.json_parser import DEFAULT_LINEAGE_DATASET
from timagetk.tasks.json_parser import DEFAULT_SEGMENTATION_DATASET
from timagetk.tasks.json_parser import DEFAULT_TRSF_DATASET
from timagetk.tasks.json_parser import json_lineage_parsing
from timagetk.tasks.json_parser import json_orientation_metadata
from timagetk.tasks.json_parser import json_segmentation_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.json_parser import json_time_metadata
from timagetk.tasks.json_parser import time_intervals
from timagetk.visu.pyvista import tissue_image_cell_polydatas

cli_name = Path(__file__).stem


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description='Lineage projection maps.')

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")

    ds_arg = parser.add_argument_group('dataset arguments')
    ds_arg.add_argument('-lin', '--lineage_dataset', type=str, default=DEFAULT_LINEAGE_DATASET,
                        help=f"the dataset containing the lineage to project, '{DEFAULT_LINEAGE_DATASET}' by default.")
    ds_arg.add_argument('-seg', '--segmentation_dataset', type=str, default=DEFAULT_SEGMENTATION_DATASET,
                        help=f"the dataset containing the segmented image on which to project the dataset, '{DEFAULT_SEGMENTATION_DATASET}' by default.")
    ds_arg.add_argument('-trsf', '--trsf_dataset', type=str, default=DEFAULT_TRSF_DATASET,
                        help=f"name of the dataset containing the transformations, '{DEFAULT_TRSF_DATASET}' by default.")
    ds_arg.add_argument('-t', '--time_index', type=int, nargs='+', default=None,
                        help="a subset of the images' time-index to use, all of them by default.")

    lin_arg = parser.add_argument_group('lineage arguments')
    lin_arg.add_argument('-fmt', '--lineage_format', type=str, default=DEFAULT_LINEAGE_FMT,
                         choices=AVAILABLE_LINEAGE_FMT,
                         help=f"the format used to read the lineage file, '{DEFAULT_LINEAGE_FMT}' by default.")
    lin_arg.add_argument('--rank', type=int, default=None, nargs='+',
                         help=f"the list of temporal ranked distances used to get lineage, '[1]' by default." \
                              "Use `-1` to get the 'max rank', `-2` to get the 'max rank - 1' and so on." \
                              "To get a range of temporal ranks, use `--rank_range` instead.")
    lin_arg.add_argument('--rank_range', type=int, nargs=2, default=None,
                         help=f"the range of temporal ranked distance used to get lineage, 'None' by default." \
                              "Use `-1` to get the 'max rank', `-2` to get the 'max rank - 1' and so on."
                              "Use `1 -1` to get all the whole range of temporal rank distances.")

    img_arg = parser.add_argument_group('images arguments')
    img_arg.add_argument('--orientation', type=int, default=1, choices={-1, 1},
                         help="image stack orientation, use `-1` with an inverted microscope, `1` by default.")
    img_arg.add_argument('--no_registration', action="store_true",
                         help="use this to prevent image registration.")
    img_arg.add_argument('--layers', default=[1], type=int, nargs='+',
                         help="the list of layer used for projection.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default.")

    return parser


def main(args):
    # Get the name of the time-series
    args.xp_id = json_series_metadata(args.json)['series_name']
    # Get 'time' metadata
    time_md = json_time_metadata(args.json)
    # Defines some location variables:
    args.root_path = Path(args.json).parent.absolute()  # the root directory is where the JSON file is...

    # Initialize logger
    global logger
    logger = get_logger(cli_name, args.root_path.joinpath(f'{cli_name}.log'), args.log_level.upper())
    # Log first welcome message
    logger.info(f"Lineage projections for time-series '{args.xp_id}' located under '{args.root_path}'.")

    # ----- LINEAGE FILES -----
    # - Get the list of lineage indexed by len-2 time-point tuple
    lineage_files = json_lineage_parsing(args.json, args.lineage_dataset)
    logger.info(f"Found {len(lineage_files)} lineage files under '{args.lineage_dataset}'.")
    logger.debug(lineage_files)

    # ----- SEGMENTED IMAGES FILES -----
    # - Get the dictionary of segmented images indexed by time-point:
    seg_img_files = json_segmentation_parsing(args.json, args.segmentation_dataset)
    args.n_imgs = len(seg_img_files)
    logger.info(f"Found {args.n_imgs} segmented images under '{args.segmentation_dataset}'.")
    logger.info(f"Existing intervals: {', '.join(list(map(str, seg_img_files.keys())))}.")
    logger.debug(seg_img_files)
    # Check the consistency of number of lineage & image files
    try:
        assert len(lineage_files) == args.n_imgs - 1
    except AssertionError:
        logger.error("We need `N-1` lineage files, where `N` is the number of segmented images...")
        logger.error(f"Found {args.n_imgs} segmented images and {len(lineage_files)} lineage files.")
        exit(1)

    # ----- IMAGE ORIENTATION -----
    # Try to get the image/microscope 'orientation' from the JSON metadata:
    orientation = json_orientation_metadata(args.json, None)
    if orientation is not None:
        # If defined in JSON, override input argument:
        args.orientation = orientation
        logger.info(f"Got an image/microscope 'orientation' from JSON: `{args.orientation}`")
    else:
        logger.info(f"Got an image/microscope 'orientation' from input argument: `{args.orientation}`")

    # ----- TRANSFORMATION FILES -----
    # - Get the dictionary of transformation files indexed by time intervals (len-2 tuples)
    trsf_files = get_trsf_files(args, logger)

    # ----- TIME INDEXES -----
    # - Check definition of time-points ('point' entry) in JSON 'time' metadata:
    all_time_points = time_md.get("points", [])
    if all_time_points == []:
        logger.critical("No 'point' entry defined in 'time' metadata!")
        sys.exit("Required 'point' entry in 'time' metadata is missing!")
    # - Try to get the 'selected' time-points from the JSON 'time' metadata:
    sel_time_points = time_md.get("selected", [])
    t_unit = time_md.get("unit", "?")
    if sel_time_points != []:
        logger.info(f"Got a list of 'selected' time-points from JSON: {sel_time_points}")
        time_index = [all_time_points.index(tp) for tp in sel_time_points]
        logger.info(f"This correspond to the following time-indexes: {time_index}")
    else:
        sel_time_points = all_time_points
        time_index = list(range(0, len(sel_time_points)))
    logger.info(f"Corresponding time-intervals (in {t_unit}) from JSON: {time_intervals(sel_time_points)}")

    # - Filter time-indexes by a selected subset:
    if args.time_index is not None:
        try:
            assert len(set(args.time_index) & set(time_index)) == len(args.time_index)
        except AssertionError:
            logger.critical(f"Selected time-indexes '{args.time_index}' are invalid!")
            sys.exit(f"Selected time-indexes should be a subset of {time_index}!")
        else:
            time_index = list(set(args.time_index) & set(time_index))

    # ----- TEMPORAL RANKS -----
    if args.rank_range is None and args.rank is None:
        # If no input for temporal rank, set it to [1] by default:
        args.rank = [1]
    elif args.rank_range is not None:
        # If a range of ranks, convert it to the full list of values:
        args.rank_range = check_temporal_rank(args.rank_range, time_index, logger)
        args.rank = list(range(args.rank_range[0], args.rank_range[1] + 1))
    else:
        args.rank = check_temporal_rank(args.rank, time_index, logger)

    # Create the list of all pairs of time-index to project the lineage for:
    t_idx_pairs = [(ti, tj) for (ti, tj) in combinations(time_index, 2) if tj - ti in args.rank]
    if len(t_idx_pairs) == 0:
        if len(args.rank) == 1:
            logger.critical("Could not generate pairs of time-indexes for a time-series with "
                            f"time-indexes of {time_index} and a temporal distance rank of {args.rank}.")
        else:
            logger.critical("Could not generate pairs of time-indexes for a time-series with "
                            f"time-indexes of {time_index} and a list of temporal distance ranks of {args.rank}.")
        sys.exit("Check the input parameters!")
    else:
        logger.info(f"Lineages will be projected for {len(t_idx_pairs)} pairs of time-indexes: {t_idx_pairs}")

    # -- Display the cell lineage for selected
    cell_polydatas = {}
    cell_layers = {}
    for (ti, tj) in sorted(t_idx_pairs):
        logger.info(f"Loading t{ti}-t{tj} lineage...")
        lineage = load_lineage_from_files(lineage_files, ti, tj, fmt=args.lineage_format)
        trsf = None
        if args.no_registration is False:
            if trsf_files is not None and (ti, tj) in trsf_files.keys():
                logger.info(f"Loading t{ti}-t{tj} transformation...")
                trsf = load_trsf_from_files(trsf_files, ti, tj).get_array()
            else:
                logger.warning(f"No registration file found for t{ti}-t{tj}!")
        # -- Load labelled images & pre-compute reusable stuffs like cell-layer dict and cell polydata:
        labelled_images = []
        for tp in [ti, tj]:
            logger.info(f"Loading t{tp} segmented image...")
            labelled_image = imread(seg_img_files[tp], TissueImage3D, not_a_label=0, background=1)
            labelled_image = labelled_image.transpose("XYZ")  # to generate cell polydata with correct coordinates
            labelled_images.append(labelled_image)
            # --- Pre-compute layers-indexed dictionary of cells:
            if cell_layers.get(tp, None) is None:
                logger.info(f"Computing cell layers for t{tp} segmented image...")
                cell_layers[tp] = cell_layers_from_image(labelled_image, args.layers)
            # --- Pre-compute cells' mesh polydata:
            if cell_polydatas.get(tp, None) is None:
                logger.info(f"Computing cell polydata for t{tp} segmented image...")
                cell_polydatas[tp] = tissue_image_cell_polydatas(labelled_image)

        plotter = lineage_viewer(labelled_images, lineage, trsf=trsf,
                                 layers=args.layers, orientation=args.orientation,
                                 cell_polydatas={0: cell_polydatas[ti], 1: cell_polydatas[tj]},
                                 cell_layers={0: cell_layers[ti], 1: cell_layers[tj]},
                                 logger=logger)
        input('Hit any key to move to the next lineage...')
        plotter.clear()
        plotter.close()
        del plotter

    return


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
