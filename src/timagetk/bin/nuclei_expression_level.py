#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
import json
from pathlib import Path

import numpy as np
import pandas as pd

from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.bin.tasks import check_time_index
from timagetk.components.multi_channel import MultiChannelImage
from timagetk.io import imread
from timagetk.tasks.json_parser import DEFAULT_INTENSITY_DATASET
from timagetk.tasks.json_parser import DEFAULT_NUCLEI_DATASET
from timagetk.tasks.json_parser import add2json
from timagetk.tasks.json_parser import json_channel_names
from timagetk.tasks.json_parser import json_intensity_image_parsing
from timagetk.tasks.json_parser import json_nuclei_segmentation_parsing
from timagetk.tasks.json_parser import json_registration_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.nuclei_detection import compute_nuclei_channel_intensities
from timagetk.visu.nuclei import channel_signal
from timagetk.visu.nuclei import nuclei_signals

OUT_DATASET = "nuclei_expression_level"

DESCRIPTION = """Performs nuclei expression level quantification from an intensity image.

We recommended to use a reference image with constitutive expression to compensate for potential acquisition biais.
It is also recommended to use raw intensity image to performs quantification as registration may change the 'original' intensity.

Usage Examples
$ python nuclei_expression_level.py my_dataset.json --channel CLV3 --ratio_channel qDII --reference_channel tagBFP --negative_reporter qDII

Will quantify nuclei expression levels on 3 channels (CLV3, qDII & tagBFP) and compute negative ratiometric values (1-qDII/tagBFP).
"""
cli_name = Path(__file__).stem


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION,
                                     epilog=f"Quantification are stored in the '{OUT_DATASET}' dataset.")

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('-d', '--dataset', type=str, default=DEFAULT_INTENSITY_DATASET,
                        help=f"the dataset to use as intensity image input for expression levels quantification, '{DEFAULT_INTENSITY_DATASET}' by default.")
    parser.add_argument('-c', '--coordinates_dataset', type=str, default=DEFAULT_NUCLEI_DATASET,
                        help=f"the dataset to use as detected nuclei coordinates, '{DEFAULT_NUCLEI_DATASET}' by default.")
    parser.add_argument('-t', '--time_index', type=list, default=[],
                        help="the time-index of the image used in segmentation procedure, all of them by default.")

    out_arg = parser.add_argument_group('output arguments')
    out_arg.add_argument('--out_dataset', type=str, default=OUT_DATASET,
                         help=f"export segmented images in a '{OUT_DATASET}' dataset")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default")

    img_arg = parser.add_argument_group('multichannel image arguments')
    img_arg.add_argument('--channel', type=str, nargs='+',
                         help="channel(s) to quantify.")
    img_arg.add_argument('--ratio_channel', type=str, nargs='+',
                         help="channel(s) to quantify and transform as ratiometric values with the reference.")
    img_arg.add_argument('--reference_channel', type=str,
                         help="channel to use as reference for ratiometric measurements.")

    quantif_arg = parser.add_argument_group('quantification arguments')
    quantif_arg.add_argument('--negative_reporter', type=str, nargs='+',
                             help="ratio channel(s) to be computed a negative reporter (1-ratio).")
    quantif_arg.add_argument('--sigma', type=float, default=0.5,
                             help="a real unit sigma to use as radius for intensity measurement.")

    return parser


def main(args):
    job_name = 'nuclei_expression_level'
    # Get the name of the series
    series_md = json_series_metadata(args.json)
    xp_id = series_md['series_name']
    # Defines some location variables:
    args.root_path = Path(args.json).parent.absolute()  # the root directory is where the JSON file is...
    args.out_path = args.root_path.joinpath(args.out_dataset)  # but needs absolute path to save files
    args.out_path.mkdir(exist_ok=True)  # make sure the directory exists

    # Initialize logger
    logger = get_logger(cli_name, args.root_path.joinpath(f'{cli_name}.log'), args.log_level.upper())
    # Log first welcome message
    logger.info(f"Nuclei expression level quantification for time-series '{xp_id}' located under '{args.root_path}'.")

    # Get the list of nuclei images per time-point
    if 'registration' in args.dataset:
        args.dataset, trsf_type = args.dataset.split("/")
        intensity_imgs = json_registration_parsing(args.json, args.dataset)[trsf_type]
    else:
        intensity_imgs = json_intensity_image_parsing(args.json, args.dataset)
    n_imgs = len(intensity_imgs)
    logger.info(f"Found {n_imgs} intensity images under '{args.dataset}'.")
    logger.debug(intensity_imgs)
    # Try to load channel names:
    try:
        ch_names = json_channel_names(args.json)
    except:
        ch_names = []

    # Get the list of nuclei coordinates
    nuclei_segmentations = json_nuclei_segmentation_parsing(args.json, args.coordinates_dataset)

    # Takes care of time index
    args.time_index = check_time_index(args.time_index, n_imgs)

    quantif = {}
    process = {}
    logger.info("Starting nuclei expression level quantification task!")
    for im_id, int_img_fname in intensity_imgs.items():
        if im_id not in args.time_index:
            logger.info(f"Skipping time-index {im_id}")
            continue

        process[im_id] = {}
        process[im_id]['file'] = int_img_fname
        # Load the intensity image file
        logger.info(f"Loading image file '{int_img_fname}' ({im_id})...")
        int_img = imread(int_img_fname)
        if isinstance(int_img, MultiChannelImage):
            if ch_names is not None:
                for old, new in zip(int_img.get_channel_names(), ch_names):
                    logger.info(f"Changing default channel name '{old}' to '{new}'")
                    int_img.set_channel_name(old, new)
            logger.info(f"Got a MultiChannelImage with channels: {int_img.get_channel_names()}")
        else:
            raise NotImplementedError("Sorry!")
        # Load the nuclei coordinates:
        nuclei_array = np.loadtxt(nuclei_segmentations[im_id])

        ratio_channel = {}
        negative_ratio = {}
        for ch in args.ratio_channel:
            if ch in args.negative_reporter:
                ratio_channel.update({f"1-{ch}/{args.reference_channel}": ch})
                negative_ratio.update({f"1-{ch}/{args.reference_channel}": True})
            else:
                ratio_channel.update({f"{ch}/{args.reference_channel}": ch})
        # Quantify fluorescence ratio at nuclei positions:
        signal_intensities, process = compute_nuclei_channel_intensities(int_img, nuclei_array,
                                                                         channel_names=args.channel,
                                                                         reference_channel=args.reference_channel,
                                                                         ratio_channels=ratio_channel,
                                                                         negative_ratios=negative_ratio,
                                                                         nuclei_sigma=args.sigma)
        # Save the computed nuclei signals in a CSV:
        quantif_filename = f"{xp_id}_t{im_id}_nuclei_signal.csv"
        quantif[im_id] = quantif_filename
        df = pd.DataFrame().from_dict(signal_intensities)
        df.to_csv(args.out_path.joinpath(quantif_filename))
        add2json(args.json, args.out_dataset, quantif)

        # Save the processing pipeline and values
        with open(args.out_path.joinpath('processing.json'), 'w') as proc_f:
            json.dump(process, proc_f, indent=4)

        # Save a 2D scatter plot of the quantified signal(s)
        quantif_png = f"{xp_id}_t{im_id}_nuclei_signal.png"
        quantif_png = args.out_path.joinpath(quantif_png)
        nuclei_signals(nuclei_array, signal_intensities, int_img.get_extent('x'), int_img.get_extent('y'),
                       figname=quantif_png)

        # Create intensity projection maps and associated 2D scatter plot of the quantified signal(s)
        for signal_name, nuclei_signal in signal_intensities.items():
            if signal_name in ratio_channel:
                channel = ratio_channel[signal_name]
            else:
                channel = signal_name
            suffix = signal_name.replace('/', '.over.')
            png_name = f"{xp_id}_t{im_id}_{suffix}_projection.png"
            png_name = args.out_path.joinpath(png_name)
            channel_signal(nuclei_array, nuclei_signal, int_img[channel], signal_name, figname=png_name)


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
