#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import click
import json
from os.path import join
from pathlib import Path

from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.io import imread
from timagetk.io import imsave
from timagetk.tasks.json_parser import DEFAULT_EXT
from timagetk.tasks.json_parser import DEFAULT_INTENSITY_DATASET
from timagetk.tasks.json_parser import add2json
from timagetk.tasks.json_parser import json_intensity_image_parsing
from timagetk.tasks.json_parser import json_orientation_metadata
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.json_parser import json_time_metadata
from timagetk.tasks.json_parser import time_intervals
from timagetk.third_party.plantseg import multicut_segmentation

BACKGROUND_LABEL = 1
CLI_NAME = __file__.split("/")[-1].split(".")[0]
OUT_DATASET = CLI_NAME

DESCRIPTION = """Performs MultiCut segmentation of an intensity image.

We recommended to perform `unet_predict` first.
"""


@click.command(help=DESCRIPTION)
@click.argument('json_file', type=click.Path(exists=True, dir_okay=False, readable=True))
@click.option('-int', '--intensity_dataset', type=str,
              default=DEFAULT_INTENSITY_DATASET, show_default=True,
              help=f"Name of the dataset that contains the intensity images to segment.")
@click.option('-index', '--time-index', type=int, multiple=True,
              help="Selection of images to segment, as a list of time-indexes, all of them by default.")
@click.option('-r', '--ref-idx', type=int, multiple=True,
              default=[0], show_default=True,
              help="A list of reference image index, in case of multi-angle images, to select from the time-series.")
@click.option('--channel', type=str, default=None,
              help="Channel to register, mandatory for multichannel images.")
@click.option('--beta', type=float,
              default=0.5, show_default=True,
              help="Beta parameter for the multicut."
                   "A small value will steer the segmentation towards under-segmentation. "
                   "While a high-value bias the segmentation towards over-segmentation.")
@click.option('--post-minsize', type=int,
              default=100, show_default=True,
              help="Minimal size of the segments after Multicut.")
@click.option('--threshold', type=float,
              default=0.5, show_default=True,
              help="Value for the threshold applied before distance transform.")
@click.option('--sigma-seeds', type=float,
              default=1., show_default=True,
              help="Smoothing factor for the watershed seed map.")
@click.option('--sigma-weights', type=float,
              default=2., show_default=True,
              help="Smoothing factor for the watershed weight map.")
@click.option('--min-size', type=int,
              default=100, show_default=True,
              help="Minimal size of watershed segments.")
@click.option('--alpha', type=float,
              default=1., show_default=True,
              help="Alpha used to blend input_ and distance_transform in order to obtain the watershed weight map.")
@click.option('--apply-nonmax-suppression', is_flag=True,
              help="Apply non-maximum suppression to filter out seeds.")
@click.option('--ext',
              default=DEFAULT_EXT, show_default=True,
              help=f"File extension used to save seed & segmented images.")
@click.option('--out-dataset', type=str,
              default=OUT_DATASET, show_default=True,
              help=f"Name of the dataset to export the segmented images.")
@click.option('--log-level', type=click.Choice(LOG_LEVELS),
              default=DEFAULT_LOG_LEVEL, show_default=True,
              help=f"Logging level to use.")
def main(json_file, intensity_dataset, time_index, ref_idx, channel,
         beta, post_minsize,
         threshold, sigma_seeds, sigma_weights, min_size, alpha, apply_nonmax_suppression,
         ext, out_dataset, log_level):
    # Get the name of the series
    series_md = json_series_metadata(json_file)
    time_md = json_time_metadata(json_file)
    xp_id = series_md['series_name']
    # Defines some location variables:
    root_path = Path(json_file).parent.absolute()  # the root directory is where the JSON file is...
    out_path = root_path.joinpath(out_dataset)  # but needs absolute path to save files
    out_path.mkdir(exist_ok=True)  # make sure the directory exists

    # Initialize logger
    logger = get_logger(CLI_NAME, join(root_path, f'{CLI_NAME}.log'), log_level.upper())
    # Log first welcome message
    logger.info(f"Multicut segmentation for time-series '{xp_id}' located under '{root_path}'.")

    # --- INTENSITY IMAGES ---
    # Get the dictionary of intensity images indexed by time-point
    intensity_imgs = json_intensity_image_parsing(json_file, intensity_dataset)
    # Duplicate the reference image index in case only one is given, meaning it is the same for all time-point:
    if len(ref_idx) == 1 and len(intensity_imgs) > 1:
        ref_idx = ref_idx * len(intensity_imgs)  # set the right number of reference image index
    # Check if we have multi-angle image, if yes select the one defined as reference:
    for t_idx, images in intensity_imgs.items():
        if isinstance(images, dict):
            try:
                ref_img = Path(images[ref_idx[t_idx]])
            except IndexError:
                pass
            else:
                logger.info(f"Using image '{ref_img.name}' ({ref_idx}), of the multi-angle set, at time-point {t_idx}!")
                intensity_imgs[t_idx] = ref_img
    # Some logging:
    n_imgs = len(intensity_imgs)
    logger.info(f"Found {n_imgs} intensity images under '{intensity_dataset}'.")
    logger.debug(intensity_imgs)

    # -- TIME POINTS - Try to get the 'selected' time-points from the JSON metadata:
    if time_index != ():
        wrong_index = [idx for idx in time_index if idx not in intensity_imgs.keys()]
        if wrong_index != []:
            logger.warning(f"Got some manually defined time-index that do not exists: {wrong_index}")
        time_index = [idx for idx in time_index if idx in intensity_imgs.keys()]
        logger.info(f"Selected a subset of time-points from JSON: {time_index} (out of {n_imgs} original images).")
    else:
        all_time_points = time_md.get("points", None)
        sel_time_points = time_md.get("selected", None)
        t_unit = time_md.get("unit", "?")  # TODO: raise something if time unit is not properly defined ?
        if sel_time_points is not None:
            logger.info(f"Got a list of 'selected' time-points from JSON: {sel_time_points}")
            time_index = [all_time_points.index(tp) for tp in sel_time_points]
            logger.info(f"Corresponding time-indexes: {time_index}")
        else:
            sel_time_points = all_time_points
            time_index = list(range(0, n_imgs))
            logger.info(f"Using all {len(time_index)} time-points: {time_index}!")
        logger.info(f"Corresponding time-intervals (in {t_unit}) from JSON: {time_intervals(sel_time_points)}")
    # Re-index the time-series:
    intensity_imgs = {idx: img for idx, img in intensity_imgs.items() if idx in time_index}

    # Try to get the image/microscope 'orientation' from the JSON metadata:
    orientation_metadata = json_orientation_metadata(json_file, None)
    if orientation_metadata is not None:
        orientation = orientation_metadata
        logger.info(f"Got an image/microscope 'orientation' from JSON: `{orientation}`")

    segmentations = {}  # segmentation file names dictionary, indexed by time-series index
    process = {}  # processing dictionary, indexed by time-series index
    logger.info("Starting cell-based segmentation task...")
    for im_idx, int_img_fname in intensity_imgs.items():
        process[im_idx] = {}
        process[im_idx]['intensity_image'] = int_img_fname  # keep track of the used intensity image
        # Load the intensity image file
        if channel is not None:
            logger.info(f"Loading channel '{channel}' of image file '{int_img_fname}' ({im_idx})...")
        else:
            logger.info(f"Loading single channel of image file '{int_img_fname}' ({im_idx})...")
        int_img = imread(int_img_fname, channel=channel)

        logger.info("Performing multicut segmentation...")
        # Create a dict of kwargs from parsed arguments
        dt_watershed_kwargs = {
            "threshold": threshold,
            "sigma_seeds": sigma_seeds,
            "sigma_weights": sigma_weights,
            "min_size": min_size,
            "alpha": alpha,
            "apply_nonmax_suppression": apply_nonmax_suppression
        }
        multicut_kwargs = {
            "beta": beta,
            "post_minsize": post_minsize,
        }
        seg_img = multicut_segmentation(int_img, **dt_watershed_kwargs, **multicut_kwargs)
        process[im_idx]["dt_watershed"] = dt_watershed_kwargs
        process[im_idx]["multicut"] = multicut_kwargs

        seg_filename = f"{xp_id}_t{im_idx}_segmented.{ext}"
        segmentations[im_idx] = seg_filename
        logger.info(f"Saving segmentation file '{seg_filename}' ({im_idx})...")
        imsave(out_path.joinpath(seg_filename), seg_img)
        add2json(json_file, out_dataset, segmentations)

        # Save the processing pipeline and values
        with open(out_path.joinpath('processing.json'), 'w') as proc_f:
            json.dump(process, proc_f, indent=4)


def ticked_histogram(data_dict, figname, vmini=None, vmaxi=None, **kwargs):
    """Parameters
    ----------
    data_dict : dict
        A dictionary containing the data to be histogrammed. The keys are data labels and the values are lists or arrays of data points.
    figname : str
        The name of the file where the histogram figure will be saved.
    vmini : float, optional
        An optional minimum value for a vertical dashed red line. If provided, a red dashed line will be drawn at this value.
    vmaxi : float, optional
        An optional maximum value for a vertical dashed red line. If provided, a red dashed line will be drawn at this value.
    kwargs : dict, optional
        Additional keyword arguments to be passed to the matplotlib `hist` function and figure setup, such as 'dpi' for figure resolution and 'hist_size' to set the figure size.
    """
    from matplotlib import pyplot as plt
    dpi = kwargs.pop("dpi", None)
    fig, ax = plt.subplots(nrows=1, ncols=1, dpi=dpi)
    hist_size = kwargs.pop('hist_size', 5.)  # size of the matrix figure
    fig.set_size_inches(2.5 * hist_size, hist_size)  # width, height
    vect_data = list(data_dict.values())
    N, bins, patches = ax.hist(vect_data, **kwargs)
    ymini = - max(N) * 0.08
    ymaxi = max(N) * 1.05
    _ = ax.plot(vect_data, [ymini / 2] * len(vect_data), '|', color='gray')
    ax.set_ylim(ymini, ymaxi)
    ax.set_xlim(0, ax.get_xlim()[1])
    if vmini is not None:
        ax.vlines(vmini, *ax.get_ylim(), color='red', linestyles='dashed')
    if vmaxi is not None:
        ax.vlines(vmaxi, *ax.get_ylim(), color='red', linestyles='dashed')
    ax.set_title("Cell volume histogram.")
    plt.savefig(figname)


if __name__ == '__main__':
    main()
