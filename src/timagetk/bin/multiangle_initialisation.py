#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.gui.mpl_image_surface_landmarks import SurfaceLandmarks
from timagetk.io.image import pims_read as imread
from timagetk.tasks.json_parser import DEFAULT_INTENSITY_DATASET
from timagetk.tasks.json_parser import DEFAULT_MULTIANGLE_LANDMARKS_DATASET
from timagetk.tasks.json_parser import add2json
from timagetk.tasks.json_parser import json_channel_names
from timagetk.tasks.json_parser import json_intensity_image_parsing
from timagetk.tasks.json_parser import json_landmarks_parsing
from timagetk.tasks.json_parser import json_series_metadata

OUT_DATASET = DEFAULT_MULTIANGLE_LANDMARKS_DATASET
cli_name = Path(__file__).stem


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description='Manually place landmarks for multi-angles fusion initialisation.')

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('-int', '--intensity_dataset', type=str, default=DEFAULT_INTENSITY_DATASET,
                        help=f"the dataset to use as intensity image input for fusion algorithm, '{DEFAULT_INTENSITY_DATASET}' by default.")
    parser.add_argument('-t', '--time_index', type=list, default=[],
                        help="the time-index of the multi-angles image used in fusion procedure, all of them by default.")

    log_arg = parser.add_argument_group('multichannel image arguments')
    log_arg.add_argument('--channel', type=str,
                         help="channel to use with a multichannel image.")

    log_arg = parser.add_argument_group('GUI arguments')
    log_arg.add_argument('--voxelsize', type=float, default=None,
                         help="Voxelsize to use for the main panel images in the GUI.")

    out_arg = parser.add_argument_group('output arguments')
    out_arg.add_argument('-o', '--out_dataset', type=str, default=OUT_DATASET,
                         help=f"the dataset name to use to save landmarks files, '{OUT_DATASET}' by default.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default")

    return parser


def add_landmarks(ax, ldmks):
    px, py, c = np.array([(p[0], p[1], n) for n, p in enumerate(ldmks)]).T
    ax.scatter(px, py, c=c, cmap='Set3', ec='k', s=80)


def proj_alti_maps(proj_maps, alti_maps, title, figname):
    fig, axes = plt.subplots(2, 2)
    fig.set_size_inches(12, 12)
    fig.set_dpi(160)
    fig.suptitle(title)
    for n, panel in enumerate(['left', 'right']):
        axes[n, 0].imshow(proj_maps[panel], cmap='gray', vmin=0, vmax=255)
        axes[n, 1].imshow(alti_maps[panel], cmap='viridis')

    fig.subplots_adjust(wspace=0, hspace=0)
    fig.tight_layout()
    fig.savefig(figname)
    plt.close()


def main(args):
    # Get the name of the series
    series_md = json_series_metadata(args.json)
    xp_id = series_md['series_name']
    # Defines some location variables:
    args.root_path = Path(args.json).parent.absolute()  # the root directory is where the JSON file is...
    args.out_path = args.root_path.joinpath(args.out_dataset)  # but needs absolute path to save files
    args.out_path.mkdir(exist_ok=True)  # make sure the directory exists

    # Initialize logger
    logger = get_logger(cli_name, args.root_path.joinpath(f'{cli_name}.log'), args.log_level.upper())
    # Log first welcome message
    logger.info(f"Initializing multi-angles fusion for time-series '{xp_id}' located under '{args.root_path}'.")

    # Get the list of multi-angle images per time-point
    multiangles = json_intensity_image_parsing(args.json, args.intensity_dataset)
    n_ma_imgs = len(multiangles)
    logger.info(f"Found {n_ma_imgs} multi-angle intensity images index.")
    logger.debug(multiangles)
    # Try to load channel names:
    try:
        ch_names = json_channel_names(args.json)
        logger.info(f"Found {len(ch_names)} channel names: {ch_names}")
    except:
        ch_names = None

    # Try to load existing landmarks
    try:
        ldmk_json = json_landmarks_parsing(args.json, args.out_dataset)
    except KeyError:
        ldmk_json = {}
        logger.warning(f"Could not find `{args.out_dataset}` entry in JSON file '{args.json}'!")
    else:
        logger.info("Found landmarks files definition.")
        logger.debug(ldmk_json)

    logger.info("Starting multi-angle landmark manual definition GUI loop")
    # Loop to call a GUI to create landmarks between intensity images to register
    if args.time_index == []:
        args.time_index = list(range(n_ma_imgs))
    else:
        args.time_index = list(map(int, args.time_index))
    # Loop to call a GUI to create landmarks between intensity images to register
    for ti, img_list in multiangles.items():
        if ti not in args.time_index:
            logger.info(f"Skipping time-index {ti}")
            continue
        else:
            logger.info(f"Got {len(img_list)} images to initialize for time-index {ti}...")
        if ti not in ldmk_json:
            ldmk_json[ti] = {}
        # Load the reference intensity image
        if args.channel != None:
            logger.info(f"Loading channel '{args.channel}' of reference image '{Path(img_list[0]).name}'")
            ref_img = imread(img_list[0], channel_names=ch_names, channel=args.channel)
        else:
            logger.info(f"Loading reference image '{Path(img_list[0]).name}'")
            ref_img = imread(img_list[0])

        # Set the list of floating images
        float_img = img_list[1:]
        for f_idx, f_img in enumerate(float_img):
            f_idx += 1  # float index is shifted by one compared to the original list
            # Load the floating intensity image to register on reference
            if args.channel != None:
                logger.info(f"Loading channel '{args.channel}' of floating image '{Path(f_img).name}'")
                f_img = imread(f_img, channel_names=ch_names, channel=args.channel)
            else:
                logger.info(f"Loading floating image '{Path(f_img).name}'")
                f_img = imread(f_img)

            ldmks = None
            try:
                l_pts = np.array([np.loadtxt(jf) for jf in ldmk_json[ti][(0, f_idx)] if 'reference' in jf][0])
                r_pts = np.array([np.loadtxt(jf) for jf in ldmk_json[ti][(0, f_idx)] if 'floating' in jf][0])
                assert len(l_pts) == len(r_pts)  # we need the same number of points!
                assert len(l_pts) > 3  # we need more than one points!
            except IOError as e:
                logger.critical(e)  # print the caught error as there might be some files missing...
                import sys
                sys.exit("Error while loading files!")
            except KeyError as e:
                logger.error(f"Could not load one of the landmark file!")
                logger.error(e)
            except AssertionError as e:
                logger.error(f"The landmark files did not statisfy the constraints!")
                logger.error(e)
            else:
                logger.info(f"Loaded reference and floating landmarks (n={len(l_pts)})")
                ldmks = {'left': l_pts, 'right': r_pts}

            logger.info("Initializing manual landmarks definition GUI...")
            logger.info("Press 'R' to rotate the image on the right panel...")
            logger.info("Press 'A' and left-click on a panel to add a point on both side...")
            logger.info("Close the window to move on to the next iteration (if any)...")

            # Initialize the matplotlib GUI object:
            landmarks = SurfaceLandmarks(img_left=ref_img, img_right=f_img, voxelsize=args.voxelsize,
                                         points=ldmks, orientation={'left': 1, 'right': 1})
            proj_alti_maps(landmarks.projected_images,
                           landmarks.altitude_maps,
                           f"Projection & Altitude maps - {xp_id} t{ti} reference & view #{f_idx}",
                           args.out_path.joinpath(f"{xp_id}_t{ti}_proj_alti-0{f_idx}.png"))
            plt.show(block=True)  # to avoid looping...
            # TODO: should we use a user 'input()' instead ?

            # - Returned XYZ landmarks coordinates in real units
            l_pts = landmarks.points_3d_coordinates()['left']
            r_pts = landmarks.points_3d_coordinates()['right']

            # - Define the landmarks file names and save them
            logger.info("Saving multi-angle landmarks files...")
            ref_ldmks_file = f"{xp_id}_t{ti}_reference_ldmk-0{f_idx}.txt"
            flo_ldmks_file = f"{xp_id}_t{ti}_floating_ldmk-0{f_idx}.txt"
            np.savetxt(args.out_path.joinpath(ref_ldmks_file), np.array(l_pts))
            np.savetxt(args.out_path.joinpath(flo_ldmks_file), np.array(r_pts))
            # Save the location of the landmarks files & update the JSON file with it
            logger.info("Updating JSON metadata file...")
            ldmk_json[ti][0, f_idx] = [ref_ldmks_file, flo_ldmks_file]
            add2json(args.json, args.out_dataset, ldmk_json)

            logger.info("Taking a snapshot of the multi-angle landmarks...")
            # Save a snapshot of the manual lineage
            snap = SurfaceLandmarks(img_left=ref_img, img_right=f_img,
                                    surface_threshold=np.percentile(ref_img, 60), value_range=(0, 255),
                                    rotations=landmarks.rotations,
                                    points={'left': l_pts, 'right': r_pts},
                                    orientation={'left': 1, 'right': 1})
            snap.save_landmarks_figure(args.out_path.joinpath(f"{xp_id}_t{ti}_ldmk-0{f_idx}.png"))
            plt.close()


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
