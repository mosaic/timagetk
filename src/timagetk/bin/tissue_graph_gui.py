#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
from os.path import split

import dash
from dash import dcc
from dash import html
import numpy as np
import plotly.express as px
import plotly.graph_objects as go
from dash.dependencies import Input
from dash.dependencies import Output

from timagetk.io.graph import from_csv
from timagetk.tasks.json_parser import DEFAULT_FEATURES_DATASET
from timagetk.tasks.json_parser import json_csv_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.json_parser import json_time_metadata
from timagetk.visu.plotly_tissue_graph import _select_valid_cell_features

DESCRIPTION = """Tissue Graph GUI.

"""


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION)

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('-feat', '--features_dataset', type=str, default=DEFAULT_FEATURES_DATASET,
                        help=f"the dataset containing the features CSVs, '{DEFAULT_FEATURES_DATASET}' by default.")

    return parser


def _cell_widgets(tissue_graph, feature_list, **kwargs):
    named_colorscales = sorted(px.colors.named_colorscales())
    w = [
        html.Label('Cell property:'),
        dcc.Dropdown(
            id="property-dropdown",
            options=[{"label": feat, "value": feat} for feat in feature_list],
            value=feature_list[0],
        ),
        html.Label('Node size:'),
        dcc.Input(
            id='node-size',
            placeholder='Node size',
            type='text',
            value='12',
        ),
        html.Label('Edge thickness:'),
        dcc.Input(
            id='edge-thickness',
            placeholder='Edge thickness',
            type='text',
            value='2.0',
        ),
        html.Label('Cell colormap:'),
        dcc.Dropdown(
            id="node-colormap",
            options=[{"label": cmap, "value": cmap} for cmap in named_colorscales],
            value='viridis',
        ),
        html.Label('Time:'),
        dcc.Slider(
            id='time-slider',
            min=0,
            max=tissue_graph.nb_time_points - 1,
            step=1,
        ),

    ]
    return w


def main(args):
    # Get the name of the series
    series_md = json_series_metadata(args.json)
    time_md = json_time_metadata(args.json)
    xp_id = series_md['series_name']
    # Defines some location variables:
    json_root_path, _ = split(args.json)  # the root directory is where the JSON file is...

    csv_files = json_csv_parsing(args.json)  # absolute path to cell & wall CSV files
    ttg = from_csv(csv_files)

    return ttg


def run():
    parser = parsing()
    args = parser.parse_args()
    ttg = main(args)
    feature_list = _select_valid_cell_features(ttg._tissue_graph[0])

    app = dash.Dash()
    app.layout = html.Div([
        dcc.Location(id='url'),
        html.Div(id='viewport-container'),

        html.H1(children='Tissue Graph GUI', style={'textAlign': 'center'}),

        *_cell_widgets(ttg, feature_list),
        dcc.Graph(id="tissue_graph"),
    ])

    @app.callback(
        Output('tissue_graph', "figure"),
        Input('property-dropdown', "value"),
        Input('time-slider', "value"),
        Input('node-size', "value"),
        Input('edge-thickness', "value"),
        Input('node-colormap', "value"),
    )
    def update_graph(ppty, time, nodesize, edgethick, cmap):
        from timagetk.visu.plotly_tissue_graph import get_node_coordinates
        from timagetk.visu.plotly_tissue_graph import get_axes_range
        from timagetk.visu.plotly_tissue_graph import get_edge_coordinates

        idx = ttg.cell_ids(tp=time)
        cwids = ttg.cell_wall_ids(tp=time)

        barycenter = ttg.cell_property_dict('barycenter', idx, default=[np.nan, np.nan, np.nan])
        x, y, z = get_node_coordinates(barycenter, idx)
        mini_x, maxi_x, mini_y, maxi_y, mini_z, maxi_z = get_axes_range(x, y, z)
        ppty_dict = ttg.cell_property_dict(ppty, ttg.cell_ids(time), default=np.nan)
        ppty_values = [ppty_dict[tcid] for tcid in idx]
        min_val, max_val = min(list(ppty_dict.values())), max(list(ppty_dict.values()))

        marker_kwargs = {'size': int(nodesize), 'color': ppty_values, 'colorscale': cmap,
                         'colorbar': {'title': "Property"}}
        node_trace = go.Scatter3d(x=x, y=y, z=z, ids=idx, mode='markers', name='cells', marker=marker_kwargs,
                                  hoverinfo='text', showlegend=False)

        edge_x, edge_y, edge_z = get_edge_coordinates(cwids, barycenter, idx)
        edge_trace = go.Scatter3d(x=edge_x, y=edge_y, z=edge_z, mode='lines', name='edges',
                                  line={'width': float(edgethick), 'color': '#888'},
                                  hoverinfo='none', showlegend=False)

        # -- Figure LAYOUT:
        layout_kwargs = {}
        layout_kwargs["width"] = 1400
        layout_kwargs["height"] = 900

        camera = {'eye': {'x': 0, 'y': 0, 'z': -2.5}}  # Top-view

        def _aspect_ratio(mini_x, maxi_x, mini_y, maxi_y, mini_z, maxi_z):
            xr, yr, zr = maxi_x - mini_x, maxi_y - mini_y, maxi_z - mini_z  # to size the axes depending on their extent
            rmax = float(max(xr, yr, zr))  # get the max axes extent to use for ratio computation
            return {'x': xr / rmax, 'y': yr / rmax, 'z': zr / rmax}

        layout = go.Layout(autosize=False, hovermode="closest",
                           scene={'dragmode': 'orbit',
                                  "aspectratio": _aspect_ratio(mini_x, maxi_x, mini_y, maxi_y, mini_z, maxi_z),
                                  'xaxis': {'autorange': False}, 'yaxis': {'autorange': False},
                                  'zaxis': {'autorange': False}},
                           scene_camera=camera, **layout_kwargs)
        fig_w = go.FigureWidget(data=[node_trace, edge_trace], layout=layout)
        # fig_w.update_layout(scene_aspectmode='data')
        fig_w.layout.scene.xaxis.range = [mini_x, maxi_x]
        fig_w.layout.scene.yaxis.range = [mini_y, maxi_y]
        fig_w.layout.scene.zaxis.range = [mini_z, maxi_z]

        return fig_w

    app.clientside_callback(
        """
        function(href) {
            var w = window.innerWidth;
            var h = window.innerHeight;
            return {'height': h, 'width': w};
        }
        """,
        Output('viewport-container', 'children'),
        Input('url', 'href'),
    )

    app.run_server(debug=True, use_reloader=True)  # Turn off reloader if inside Jupyter


if __name__ == '__main__':
    run()
