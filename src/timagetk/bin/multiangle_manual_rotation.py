#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
from pathlib import Path

import numpy as np

from timagetk.algorithms.exposure import global_contrast_stretch
from timagetk.algorithms.resample import isometric_resampling
from timagetk.array_util import to_uint8
from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.io.image import pims_read as imread
from timagetk.tasks.json_parser import DEFAULT_INTENSITY_DATASET
from timagetk.tasks.json_parser import DEFAULT_MULTIANGLE_INIT_DATASET
from timagetk.tasks.json_parser import add2json
from timagetk.tasks.json_parser import json_channel_names
from timagetk.tasks.json_parser import json_intensity_image_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.gui.registration_views import Rigid3DRegistrationGUI
from timagetk.util import auto_format_bytes

OUT_DATASET = DEFAULT_MULTIANGLE_INIT_DATASET
cli_name = Path(__file__).stem


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description='Manually define rotations for multi-angles fusion initialisation.')

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('-int', '--intensity_dataset', type=str, default=DEFAULT_INTENSITY_DATASET,
                        help=f"the dataset to use as intensity image input for fusion algorithm, '{DEFAULT_INTENSITY_DATASET}' by default.")
    parser.add_argument('-t', '--time_index', type=list, default=[],
                        help="the time-index of the multi-angles image used in fusion procedure, all of them by default.")

    log_arg = parser.add_argument_group('multichannel image arguments')
    log_arg.add_argument('--channel', type=str,
                         help="channel to use with a multichannel image.")

    log_arg = parser.add_argument_group('resampling arguments')
    log_arg.add_argument('--voxelsize', type=float, default=0.5,
                         help="Voxelsize to use for to resample the images prior to blockmatching registration.")

    out_arg = parser.add_argument_group('output arguments')
    out_arg.add_argument('-o', '--out_dataset', type=str, default=OUT_DATASET,
                         help=f"the dataset name to use to save landmarks files, '{OUT_DATASET}' by default.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default")

    return parser


def _prin_img_stats(img):
    """Prints statistics about the image intensity distribution.

    Parameters
    ----------
    img : array-like
        Input image data for which statistics will be computed.
    """

    def _round_pc(img, pc):
        return np.round(np.percentile(img, pc), 1)

    print(f"Current image statistics: min={img.min()}, max={img.max()}, " +
          ", ".join([f"{pc}-th.pc={_round_pc(img, pc)}" for pc in [5, 25, 50, 75, 95]]) +
          "\n")
    return


def pre_process(img, voxelsize, logger):
    """Image preprocessing prior to blockmatching registration.

    Parameters
    ----------
    img : Image
        The input image to be processed. Should be provided as an instance of an Image class with required methods.

    Returns
    -------
    Image
        The processed image, which has undergone isometric resampling, contrast stretching, and conversion to 8-bit unsigned integers.

    Notes
    -----
    - The function prints statistics about the image size and shape at different stages of processing.
    - Isometric resampling: Resamples the image to a voxel size of 0.5 using linear interpolation.
    - Contrast stretching: Performs global contrast stretching on the image with specified percentile values for minimum and maximum contrast.
    - Conversion to 8-bit unsigned integers: Converts the image data to uint8 format.
    """
    # -- Load image
    print(f'# - Input image: {img.filename}...')
    print(f"Current image size: {auto_format_bytes(img.get_array().nbytes)}")
    print(f"Current image shape: {img.shape}")
    _prin_img_stats(img)
    # -- Contrast stretching
    print(f'# - Global contrast stretching...')
    img = global_contrast_stretch(img, pc_min=2, pc_max=99)
    _prin_img_stats(img)
    # -- Convert to 8bits unsigned integers
    print("# - Converting to uint8...")
    img = to_uint8(img)
    print(f"Current image size: {auto_format_bytes(img.get_array().nbytes)}")
    _prin_img_stats(img)
    mini_vxs, maxi_vxs = min(img.voxelsize), max(img.voxelsize)
    if voxelsize < min(img.voxelsize):
        logger.warning(f"Isometric resampling voxelsize ({voxelsize}) is smaller than min voxelsize ({mini_vxs})!")
    elif voxelsize > max(img.voxelsize):
        logger.warning(f"Isometric resampling voxelsize ({voxelsize}) is larger than max voxelsize ({maxi_vxs})!")
    # -- Isometric resampling
    print(f'# - Isometric resampling to {voxelsize}...')
    img = isometric_resampling(img, value=voxelsize, interpolation='linear')
    print(f"Current image size: {auto_format_bytes(img.get_array().nbytes)}")
    print(f"Current image shape: {img.shape}")
    _prin_img_stats(img)
    return img


def _load_image(image_fname, img_status, logger, ch_names=None, channel=None):
    """Load an image from its filename, might try to load only one channel if `channel` is defined.

    Parameters
    ----------
    image_fname : str or pathlib.Path
        Filename or path of the image to be loaded.
    img_status : str
        Status or description of the image being loaded.
    logger : Logger
        A logger instance for logging information and errors.
    ch_names : list of str, optional
        List of channel names, by default None.
    channel : str, optional
        Specific channel to load, by default None.

    Returns
    -------
    timagetk.SpatialImage
        The loaded image.
    """
    # Load the reference intensity image
    if channel != None:
        try:
            logger.info(f"Loading channel '{channel}' of {img_status} image '{Path(image_fname).name}'")
            image = imread(image_fname, channel_names=ch_names, channel=channel)
        except AttributeError:
            logger.error(f"Could not get '{channel}' channel from '{Path(image_fname).name}'.")
            logger.info(f"Loading {img_status} image '{Path(image_fname).name}'")
            image = imread(image_fname)
    else:
        logger.info(f"Loading {img_status} image '{Path(image_fname).name}'")
        image = imread(image_fname)
    return image


def main(args):
    # Get the name of the series
    series_md = json_series_metadata(args.json)
    xp_id = series_md['series_name']
    # Defines some location variables:
    args.root_path = Path(args.json).parent.absolute()  # the root directory is where the JSON file is...
    args.out_path = args.root_path.joinpath(args.out_dataset)  # but needs absolute path to save files
    args.out_path.mkdir(exist_ok=True)  # make sure the directory exists

    # Initialize logger
    logger = get_logger(cli_name, args.root_path.joinpath(f'{cli_name}.log'), args.log_level.upper())
    # Log first welcome message
    logger.info(f"Initializing multi-angles fusion for time-series '{xp_id}' located under '{args.root_path}'.")

    # Get the list of multi-angle images per time-point
    multiangles = json_intensity_image_parsing(args.json, args.intensity_dataset)
    n_ma_imgs = len(multiangles)
    logger.info(f"Found {n_ma_imgs} multi-angle intensity images index.")
    logger.debug(multiangles)
    # Try to load channel names:
    try:
        ch_names = json_channel_names(args.json)
        logger.info(f"Found {len(ch_names)} channel names: {ch_names}")
    except:
        ch_names = None

    logger.info("Starting multi-angle landmark manual definition GUI loop")
    # Loop to call a GUI to create landmarks between intensity images to register
    if args.time_index == []:
        args.time_index = list(range(n_ma_imgs))
    else:
        args.time_index = list(map(int, args.time_index))

    # Loop to apply rotation and registration between multi-angle intensity images to register
    for ti, ma_fnames in multiangles.items():
        if ti not in args.time_index:
            logger.info(f"Skipping time-index {ti}")
            continue
        else:
            logger.info(f"Got {len(ma_fnames)} images to initialize for time-index {ti}...")

        if len(ma_fnames) < 2:
            logger.info(f"Skip the time-index {ti}, there is no multi-angle images!")
            continue

        # Assume that the image at index 0 is ALWAYS the reference image !
        statuses = ['reference'] + ['floating'] * (len(ma_fnames) - 1)
        multiangles_imgs = []
        for img_idx  in sorted(ma_fnames.keys()):
            img = _load_image(Path(ma_fnames[img_idx]), statuses[img_idx], logger, ch_names, args.channel)
            img = pre_process(img, args.voxelsize, logger)
            multiangles_imgs.append(img)

        # Initialize & run the GUI
        gui = Rigid3DRegistrationGUI(multiangles_imgs, reference_index=0)
        gui.show()

        # When close, get the rigid trsf dict. Keys are (reference_idx, floating_idx), thus (0, floating_idx)
        trsfs = gui.export_trsf()

        logger.info("Saving multi-angle initial transformation files...")
        for (ref_idx, flo_idx), rigid_trsf in trsfs.items():
            trsf_name = f"{xp_id}_t{ti}_{ref_idx}-{flo_idx}_rigid.trsf"
            rigid_trsf.write(args.out_path.joinpath(trsf_name))
            # Save the location of the landmarks files & update the JSON file with it
            logger.info("Updating JSON metadata file...")
            add2json(args.json, args.out_dataset, {ti: {(ref_idx, flo_idx): trsf_name}})


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
