#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
import sys
import json
from os.path import join
from pathlib import Path

from timagetk.algorithms.resample import resample
from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.io import imread
from timagetk.io import imsave
from timagetk.tasks.json_parser import DEFAULT_INTENSITY_DATASET
from timagetk.tasks.json_parser import DEFAULT_EXT
from timagetk.tasks.json_parser import add2json
from timagetk.tasks.json_parser import json_intensity_image_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.json_parser import json_time_metadata
from timagetk.tasks.json_parser import time_intervals
from timagetk.third_party.vt_parser import GRAY_INTERPOLATION_METHODS
from timagetk.third_party.vt_parser import LABEL_INTERPOLATION_METHODS

OUT_DATASET = "resampled"

DESCRIPTION = """Resampling an image allow to change its voxelsize and shape using interpolation methods while preserving 
the image physical extent."""

EPILOG = """Pay attention to the type of image you are interpolating, *intensity* or *labelled*, as it modify the 
information contained in the image depending on the selected method!"""


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION,
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     epilog=f"Resampled images are stored in the '{OUT_DATASET}' dataset.\n{EPILOG}")

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('-int', '--intensity_dataset', type=str, default=DEFAULT_INTENSITY_DATASET,
                        help=f"name of the dataset that contains the intensity images to segment,"
                             f" '{DEFAULT_INTENSITY_DATASET}' by default.")
    parser.add_argument('-index', '--time_index', type=int, nargs='+', default=[],
                        help="selection of images to segment, as a list of time-indexes, all of them by default.")

    img_arg = parser.add_argument_group('images arguments')
    img_arg.add_argument('--channel', type=str, default=None,
                         help="channel to register, mandatory for multichannel images.")

    resampled_arg = parser.add_argument_group('resampled arguments')
    resampled_arg.add_argument('--voxelsize', type=float, nargs='+', default=None,
                          help=f"The (Z)YX sorted voxel-size to use for image resampling. Assuming an isometric "
                               f"resampling if a single voxel-size is given.")
    resampled_arg.add_argument('--shape', type=float, nargs='+', default=None,
                          help=f"The (Z)YX sorted shape to use for image resampling.")
    resampled_arg.add_argument('--interpolation', type=str, default=None,
                          help=f"Interpolation method to use. See the 'Notes' section for detailled explanations. "
                               f"By default, try to guess it based on the type of input `image`."
                               f"\nUse one of the {GRAY_INTERPOLATION_METHODS} with grayscale (intensity) images."
                               f"\nUse one of the {LABEL_INTERPOLATION_METHODS} with labelled (segmented) images.")
    resampled_arg.add_argument('--cell_based_sigma', type=float, default=1,
                          help=f"Required when using `cellbased` interpolation method, sigma for cell-based "
                               f"interpolation. In voxel units!")

    out_arg = parser.add_argument_group('output arguments')
    out_arg.add_argument('--ext', default=DEFAULT_EXT,
                         help=f"file extension used to save predicted images, '{DEFAULT_EXT}' by default.")
    out_arg.add_argument('--out_dataset', type=str, default=OUT_DATASET,
                         help=f"name of the dataset to export the predicted images, '{OUT_DATASET}' by default.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default.")

    return parser


def main(xp_id, args, logger):
    resampled = {}  # segmentation file names dictionary, indexed by time-series index
    process = {}  # processing dictionary, indexed by time-series index
    logger.info("Starting resampling task...")

    for im_idx, int_img_fname in args.intensity_imgs.items():
        process[im_idx] = {}
        process[im_idx]['intensity_image'] = int_img_fname  # keep track of the used intensity image

        # Load the intensity image file
        if args.channel is not None:
            logger.info(f"Loading channel '{args.channel}' of image file '{int_img_fname}' ({im_idx})...")
        else:
            logger.info(f"Loading single channel of image file '{int_img_fname}' ({im_idx})...")
        int_img = imread(int_img_fname, channel=args.channel)

        ndim = int_img.ndim # get the dimension of the image

        # update the voxelsize if needed
        if args.voxelsize[im_idx]:
            if len(args.voxelsize[im_idx]) == 1:
                args.voxelsize[im_idx] = args.voxelsize[im_idx] * ndim

        logger.info("Performing resampling image processing...")
        resampled_img = resample(int_img, voxelsize=args.voxelsize[im_idx], shape=args.shape[im_idx],
                           interpolation=args.interpolation[im_idx], cell_based_sigma=args.cell_based_sigma[im_idx])

        #TODO: for now, write directly the `process` from the `resample` input, could be change later
        p = {'voxelsize': args.voxelsize[im_idx],
             'shape': args.shape[im_idx],
             'interpolation': args.interpolation[im_idx],
             'cell_based_sigma': args.cell_based_sigma[im_idx]}

        process[im_idx].update(p)  # keep track of the watershed process for this image

        # Save the predictions
        resampled_filename = f"{xp_id}_t{im_idx}_resampled.{args.ext}"
        resampled[im_idx] = resampled_filename
        logger.info(f"Saving resampled intensity image in file '{resampled_filename}' ({im_idx})...")
        imsave(args.out_path.joinpath(resampled_filename), resampled_img)
        add2json(args.json, args.out_dataset, resampled)

        # Save the processing pipeline and values
        with open(args.out_path.joinpath('processing.json'), 'w') as proc_f:
            json.dump(process, proc_f, indent=4)

def run():
    parser = parsing()
    args = parser.parse_args()

    # Get the name of the series
    series_md = json_series_metadata(args.json)
    time_md = json_time_metadata(args.json)
    xp_id = series_md['series_name']
    # Defines some location variables:
    args.root_path = Path(args.json).parent.absolute()  # the root directory is where the JSON file is...
    args.out_path = args.root_path.joinpath(args.out_dataset)  # but needs absolute path to save files
    args.out_path.mkdir(exist_ok=True)  # make sure the directory exists

    # Initialize logger
    logger = get_logger('resampling', join(args.root_path, 'resampling.log'), args.log_level.upper())
    # Log first welcome message
    logger.info(f"Resampling  for time-series '{xp_id}' located under '{args.root_path}'.")

    # Get the dictionary of intensity images indexed by time-point
    args.intensity_imgs = json_intensity_image_parsing(args.json, args.intensity_dataset)
    n_imgs = len(args.intensity_imgs)
    logger.info(f"Found {n_imgs} intensity images under '{args.intensity_dataset}'.")
    logger.debug(args.intensity_imgs)

    # -- TIME POINTS - Try to get the 'selected' time-points from the JSON metadata:
    if args.time_index != []:
        wrong_index = [idx for idx in args.time_index if idx not in args.intensity_imgs.keys()]
        if wrong_index != []:
            logger.warning(f"Got some manually defined time-index that do not exists: {wrong_index}")
        args.time_index = [idx for idx in args.time_index if idx in args.intensity_imgs.keys()]
    else:
        all_time_points = time_md.get("points", None)
        sel_time_points = time_md.get("selected", None)
        t_unit = time_md.get("unit", "?")  # TODO: raise something if time unit is not properly defined ?
        if sel_time_points is not None:
            logger.info(f"Got a list of 'selected' time-points from JSON: {sel_time_points}")
            args.time_index = [all_time_points.index(tp) for tp in sel_time_points]
            logger.info(f"Corresponding time-indexes: {args.time_index}")
        else:
            sel_time_points = all_time_points
            args.time_index = list(range(0, n_imgs))
        logger.info(f"Corresponding time-intervals (in {t_unit}) from JSON: {time_intervals(sel_time_points)}")
    # Re-index the time-series:
    args.intensity_imgs = {idx: img for idx, img in args.intensity_imgs.items() if idx in args.time_index}

    # Check the list of `voxelsize` values to use:
    args.voxelsize = [args.voxelsize] * n_imgs

    # Check the list of `shape` values to use:
    args.shape = [args.shape] * n_imgs

    # Check the list of `interpolation` values to use:
    args.interpolation = [args.interpolation] * n_imgs

    # Check the list of `cell_based_sigma` values to use:
    args.cell_based_sigma = [args.cell_based_sigma] * n_imgs

    main(xp_id, args, logger)

if __name__ == '__main__':
    run()
