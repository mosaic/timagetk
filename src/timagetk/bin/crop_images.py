#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
import json
from os.path import join
from pathlib import Path

from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.io import imread
from timagetk.io import imsave
from timagetk.components.multi_channel import MultiChannelImage
from timagetk.tasks.json_parser import DEFAULT_EXT
from timagetk.tasks.json_parser import DEFAULT_INTENSITY_DATASET
from timagetk.tasks.json_parser import add2json
from timagetk.tasks.json_parser import json_intensity_image_parsing
from timagetk.tasks.json_parser import json_series_metadata

OUT_DATASET = "crop"

DESCRIPTION = """Performs image cropping based on a given bounding box.
"""


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser for the image cropping tool."""
    parser = argparse.ArgumentParser(description=DESCRIPTION,
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     epilog=f"Cropped images are stored in the '{OUT_DATASET}' dataset.")

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('-int', '--intensity_dataset', type=str, default=DEFAULT_INTENSITY_DATASET,
                        help=f"name of the dataset that contains the intensity images to segment, '{DEFAULT_INTENSITY_DATASET}' by default.")
    parser.add_argument('-index', '--time_index', type=int, nargs='+',
                        help="selection of images to segment, as a list of time-indexes, all of them by default.")

    crop_arg = parser.add_argument_group('cropping arguments')
    crop_arg.add_argument('--box', type=int, nargs='+', required=True,
                          help="cropping box as start and stop positions, (Z)YX ordered.")

    img_arg = parser.add_argument_group('image arguments')
    img_arg.add_argument('--channel', type=str, default=None,
                         help="channel to use with a multichannel image.")

    out_arg = parser.add_argument_group('output arguments')
    out_arg.add_argument('--ext', default=DEFAULT_EXT,
                         help=f"file extension used to save cropped images, '{DEFAULT_EXT}' by default.")
    out_arg.add_argument('--out_dataset', type=str, default=OUT_DATASET,
                         help=f"name of the dataset to export the cropped images, '{OUT_DATASET}' by default.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default.")

    return parser


def crop_multiangle_images(imgs_path, box, channel):
    """Crops a list of images at specified angles, extracting a specific channel for each image.

    Parameters
    ----------
    imgs_path : list of str
        A list of file paths to the images to be cropped.
    box : tuple
        A tuple specifying the coordinates of the bounding box for cropping
        the images. The format should be (left, upper, right, lower).
    channel : int
        An integer indicating the channel of the image to be extracted during
        the cropping process.

    Returns
    -------
    list of images
        A list containing the cropped images, each with only the specified
        channel data.
    """
    cropped_imgs = []
    for img_path in imgs_path:
        cropped_imgs.append(crop_image(img_path, box, channel))
    return cropped_imgs


def crop_image(img_path, box, channel):
    """Crops a specified region from an image.

    Parameters
    ----------
    img_path : str
        The file path to the image that needs to be cropped.
    box : tuple
        A tuple defining the region to be cropped from the image. The tuple
        is expected to be in the format (x, y, width, height).
    channel : int or None
        An optional parameter that specifies the channel number to extract
        from the image if it is a multi-channel image. If `None`, the
        cropping is done on the image without isolating a specific channel.

    Returns
    -------
    cropped_img : ndarray
        An ndarray representing the cropped region of the image.

    """
    img_path = Path(img_path)
    img = imread(str(img_path))

    if isinstance(img, MultiChannelImage) and channel:
        img = img.get_channel(channel)

    cropped_img = img.get_region(box)
    return cropped_img


def main(xp_id, args, logger):
    """Main function to perform image cropping based on provided parameters.

    The function processes a set of intensity images, applying a cropping box and saving the results with updated
    metadata.
    It logs the progress and updates a JSON file with details of the cropping process.
    Multi-angle cropping is not currently implemented.

    Parameters
    ----------
    xp_id : str
        Experiment identifier used in naming output files.
    args : Namespace
        A namespace containing parsed command-line arguments, including paths, cropping box,
        channel, image filenames, and output settings.
    logger : logging.Logger
        A logger instance for recording informational and error messages throughout the process.

    """
    cropped_images = {}  # Dictionary to store cropped image file names, indexed by time-series index
    process = {}  # Dictionary to store the processing parameters, indexed by time-series index
    logger.info("Starting image cropping task...")

    # Load existing cropping process data if the file exists
    cropping_json_path = args.out_path.joinpath('cropping.json')
    if cropping_json_path.exists():
        with open(cropping_json_path, 'r') as crop_f:
            existing_process = json.load(crop_f)
        process.update(existing_process)  # Update the process dictionary with existing data

    # Iterate over intensity images and perform cropping
    for im_idx, int_img_fname in args.intensity_imgs.items():
        str_im_idx = str(im_idx)  # Convert the index to a string for comparison with JSON keys

        if str_im_idx in process:
            logger.info(f"Time-index {im_idx} already exists. Replacing the information...")

            # Option 2: Update specific keys (e.g., replace only 'box' and 'output_filename')
            process[str_im_idx].update({
                'box': args.box,  # New cropping box to replace old one
                'channel': args.channel if args.channel else "all",
                'output_filename': f"{xp_id}_t{im_idx}_cropped.{args.ext}",
            })
        else:
            # If not already processed, add a new entry
            process[str_im_idx] = {}
            process[str_im_idx]['intensity_image'] = int_img_fname
            process[str_im_idx].update({
                'box': args.box,
                'channel': args.channel if args.channel else "all",
                'output_filename': f"{xp_id}_t{im_idx}_cropped.{args.ext}",
            })
        logger.info(f"Cropping image file '{int_img_fname}' at time-index {im_idx}...")

        # Crop the image using the provided box and channel
        if isinstance(int_img_fname, list):
            # Multi-angle cropping not implemented yet
            logger.error('Multi-angle cropping not implemented yet')
            continue
        else:
            cropped_img = crop_image(int_img_fname, args.box, args.channel)

        # Define the output filename following the same pattern
        cropped_fname = f"{xp_id}_t{im_idx}_cropped.{args.ext}"

        # Save the cropped image
        logger.info(f"Saving cropped image to '{cropped_fname}'...")
        imsave(args.out_path.joinpath(cropped_fname), cropped_img)

        # Keep track of the saved file for the current time index
        cropped_images[im_idx] = cropped_fname
        logger.info(f"Saved cropped image in file '{cropped_fname}' ({im_idx})...")

    # Add to JSON and save processing information
    add2json(args.json, args.out_dataset, cropped_images)

    # Save the processed cropping information in a separate JSON file
    logger.info("Saving cropping process details...")
    with open(args.out_path.joinpath('cropping.json'), 'w') as crop_f:
        json.dump(process, crop_f, indent=4)
    logger.info("Cropping task completed.")


def run():
    """Orchestrates the cropping of images from a time-series dataset based on metadata specified in a JSON file.

    Notes
    -----
    - The function begins by parsing command-line arguments.
    - The JSON metadata is used to extract information such as the series name and image data.
    - The logging mechanism is initialized to track the execution process and output relevant messages.
    - User-specified or default time-points are selected for image processing.
    - The main processing function is called to perform the actual image cropping.

    Raises
    ------
    - May raise exceptions if there are errors in path operations, JSON parsing, or logging setup.
    """
    parser = parsing()
    args = parser.parse_args()

    # Get the name of the series
    series_md = json_series_metadata(args.json)
    xp_id = series_md['series_name']

    # Defines some location variables:
    args.root_path = Path(args.json).parent.absolute()  # The root directory is where the JSON file is...
    args.out_path = args.root_path.joinpath(args.out_dataset)  # Save files in the out_dataset directory
    args.out_path.mkdir(exist_ok=True)  # Ensure the directory exists

    # Initialize logger
    logger = get_logger('image_crop', join(args.root_path, 'image_crop.log'), args.log_level.upper())
    logger.info(f"Cropping images for time-series '{xp_id}' located under '{args.root_path}'.")

    # Get the dictionary of intensity images indexed by time-point
    args.intensity_imgs = json_intensity_image_parsing(args.json, args.intensity_dataset)
    n_imgs = len(args.intensity_imgs)
    logger.info(f"Found {n_imgs} intensity images under '{args.intensity_dataset}'.")

    # -- TIME POINTS - Try to get the 'selected' time-points from the JSON metadata:
    if args.time_index:
        wrong_index = [idx for idx in args.time_index if idx not in args.intensity_imgs.keys()]
        if wrong_index:
            logger.warning(f"Got some manually defined time-index that do not exist: {wrong_index}")
        args.time_index = [idx for idx in args.time_index if idx in args.intensity_imgs.keys()]
    else:
        args.time_index = list(range(n_imgs))  # Default to all time-points

    # Re-index the time-series:
    args.intensity_imgs = {idx: img for idx, img in args.intensity_imgs.items() if idx in args.time_index}

    # Crop the images
    main(xp_id, args, logger)


if __name__ == '__main__':
    run()
