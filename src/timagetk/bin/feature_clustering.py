#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
import copy as cp
import logging
import sys
from datetime import datetime
from os.path import exists
from os.path import join
from pathlib import Path

import markdown
import pandas as pd
import toml
from matplotlib import pyplot as plt
from sklearn.preprocessing import RobustScaler
from visu_core.matplotlib.colormap import multicolor_categorical_colormap

from timagetk.algorithms.clustering import n_elements_by_groups
from timagetk.algorithms.clustering import pc_elements_by_groups
from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.bin.markdown_report import FIGCAPTION_STYLE
from timagetk.bin.markdown_report import HTML_TABLE_STYLE
from timagetk.bin.markdown_report import _add_css_styles
from timagetk.bin.markdown_report import include_figure_html
from timagetk.bin.tasks import clear_existing_clustering
from timagetk.bin.tasks import get_trsf_files
from timagetk.components.clustering import ALL_CLUSTERING_METHODS
from timagetk.components.clustering import FeatureClusterer
from timagetk.components.clustering import _clustering_naming
from timagetk.components.clustering_analysis import ClustererChecker
from timagetk.io import read_trsf
from timagetk.io.graph import CSV_EXPORT_SUFFIXES
from timagetk.io.graph import from_csv
from timagetk.io.graph import import_identity
from timagetk.io.graph import to_csv
from timagetk.tasks.features import CELL_FEATURES
from timagetk.tasks.features import CELL_SCALAR_FEATURES
from timagetk.tasks.features import TEMPORAL_DIFF_FEATURES
from timagetk.tasks.json_parser import DEFAULT_CLUSTERING_DATASET
from timagetk.tasks.json_parser import DEFAULT_FEATURES_DATASET
from timagetk.tasks.json_parser import DEFAULT_IDENTITY_DATASET
from timagetk.tasks.json_parser import DEFAULT_SEGMENTATION_DATASET
from timagetk.tasks.json_parser import DEFAULT_TRSF_DATASET
from timagetk.tasks.json_parser import add2json
from timagetk.tasks.json_parser import json_clustering_parsing
from timagetk.tasks.json_parser import json_csv_parsing
from timagetk.tasks.json_parser import json_orientation_metadata
from timagetk.tasks.json_parser import json_segmentation_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.json_parser import json_time_metadata
from timagetk.tasks.json_parser import time_intervals
from timagetk.util import clean_type
from timagetk.visu.clustering import clustering_figures
from timagetk.visu.clustering import plot_mds_dimensionality_reduction
from timagetk.visu.clustering import plot_tsne_dimensionality_reduction
from timagetk.visu.util import colors_array

OUT_DATASET = DEFAULT_CLUSTERING_DATASET

DESCRIPTION = """Cell clustering procedure based on features distance.

"""
cli_name = Path(__file__).stem


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION)

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('--features_dataset', type=str, default=DEFAULT_FEATURES_DATASET,
                        help=f"the name of the dataset containing the CSV files with exported features, '{DEFAULT_FEATURES_DATASET}' by default.")
    parser.add_argument('--segmentation_dataset', type=str, default=DEFAULT_SEGMENTATION_DATASET,
                        help=f"the dataset containing the segmented image on which to project the dataset, '{DEFAULT_SEGMENTATION_DATASET}' by default.")
    parser.add_argument('--trsf_dataset', type=str, default=DEFAULT_TRSF_DATASET,
                        help=f"name of the dataset containing the transformations, '{DEFAULT_TRSF_DATASET}' by default.")
    parser.add_argument('--identity_dataset', type=str, default=DEFAULT_IDENTITY_DATASET,
                        help=f"name of the dataset containing the cell identities as text files, '{DEFAULT_IDENTITY_DATASET}' by default.")
    parser.add_argument('--toml', type=str,
                        help="a TOML configuration file with a 'clustering' entry.")

    clust_arg = parser.add_argument_group('clustering arguments')
    clust_arg.add_argument('--features', type=str, nargs='+',
                           help="the cell features to use for clustering.")
    clust_arg.add_argument('--weights', type=float, nargs='+',
                           help="the weight associated to features to build the distance matrix.")
    clust_arg.add_argument('--method', type=str, choices=ALL_CLUSTERING_METHODS,
                           help="the clustering method to use.")
    clust_arg.add_argument('--n_clusters', type=int, nargs=1,
                           help="the number of cluster to use, mandatory with supervised methods.")
    clust_arg.add_argument('--standardisation', type=str, default="L1", choices=['L1', 'L2'],
                           help="the standardisation method to use to combine the features during distance matrix computation.")
    clust_arg.add_argument('--delete_outliers', action='store_true',
                           help="detect outliers and exclude them from clustering.")
    clust_arg.add_argument('--identities', type=str, nargs='+',
                           help="restrict the pairwise distance matrix to cells with selected identities.")
    clust_arg.add_argument('--exclude_identities', type=str, nargs='+',
                           help="exclude the cells with selected identities from the pairwise distance matrix.")
    clust_arg.add_argument('--outliers_cluster_id', type=int, default=-1,
                           help="id to use for outliers cluster.")
    clust_arg.add_argument('--colormap', type=str, default='viridis', choices=plt.colormaps(),
                           help="name of the colormap to use with the clusters, `viridis` by default.")

    img_arg = parser.add_argument_group('images arguments')
    img_arg.add_argument('--layers', default=[1], type=int, nargs='+',
                         help="the list of layer used for projection.")
    img_arg.add_argument('--orientation', type=int, default=1, choices={-1, 1},
                         help="image stack orientation, use `-1` with an inverted microscope, `1` by default.")
    img_arg.add_argument('--no_registration', action="store_true",
                         help="use this to prevent image registration.")

    out_arg = parser.add_argument_group('output arguments')
    out_arg.add_argument('-o', '--out_dataset', type=str, default=OUT_DATASET,
                         help=f"the dataset name to use to save lineage files, '{OUT_DATASET}' by default.")
    out_arg.add_argument('--html', action="store_true",
                         help="use this to also export the report as HTML.")
    out_arg.add_argument('--force', action="store_true",
                         help="use this to force the computation of tissue graph and features.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default")

    return parser


def make_clustering(clusterer, method, n_clusters, features, weights, standardisation, delete_outliers, **kwargs):
    """Performs the clustering.

    Parameters
    ----------
    clusterer : FeatureClusterer
        The clusterer instance to use.
    method : {"single", "ward", "spectral", "dbscan", "hdbscan"}
        Clustering method to use.
    n_clusters : int, optional
        Number of cluster to create during supervised clustering.
        For unsupervised method, like DBSCAN, this is not required and will be computed from obtained clustering.
    features : list[str]
        List of variables names in ``self.`` to combine.
    weights : list[float]
        List of variables used to create the global weighted distance matrix.
    standardisation : {"L1", "L2"}
        The standardisation metric to apply to the data, "L1" by default.
    delete_outliers : bool, optional
        If ``True`` delete outliers (values set to ``np.nan``) of pairwise distance matrix.

    Other Parameters
    ----------------
    logger : logging.Logger
        A logger
    identities : list[str]
        List of cell identities allowing to define the set of cell ids to use for clustering by including them.
    exclude_identities : list[str]
        List of cell identities allowing to define the set of cell ids to use for clustering by excluding them.

    Returns
    -------
    FeatureClusterer
        The clusterer instance with the data (features) and clustering.

    """
    logger = kwargs.get('logger')

    if method in ("single", "ward", "spectral"):
        try:
            assert n_clusters is not None
        except AssertionError:
            logger.critical(f"You need to specify the number of clusters with method {method}!")
            exit(1)
        else:
            if isinstance(n_clusters, list):
                n_clusters = n_clusters[0]

    try:
        assert len(features) == len(weights)
    except AssertionError:
        logger.error(f"You should provide as many features {len(features)} and weights {len(weights)}!")
        exit(1)

    # -- Set the standardisation method:
    clusterer.standardisation_method = standardisation

    # -- Set the list of ids to use in global pairwise distance matrix:
    # NOTE: this has to be done prior to adding features as they are extracted using the list of ids!
    identities = kwargs.get('identities', [])
    if identities != []:
        logger.info(f"Got a list of cell identities to include: {identities}")
        clusterer.ids = identities
    exclude_identities = kwargs.get('exclude_identities', [])
    if exclude_identities != []:
        logger.info(f"Got a list of cell identities to exclude: {exclude_identities}")
        clusterer.exclude_ids(exclude_identities)

    logger.info(f"Will use a list of {len(clusterer.ids)} cell ids to build the global distance matrix!")

    # -- Set the list of features to use in global pairwise distance matrix:
    remaining_features = cp.copy(features)
    # - SPATIAL features:
    for feat in features:
        if feat in CELL_SCALAR_FEATURES:
            clusterer.add_spatial_variable(feat, 'numeric')
            remaining_features.remove(feat)
        elif feat in CELL_FEATURES:
            logger.warning(f"Can not use '{feat}' cell feature for clustering as it is not a scalar!")
            remaining_features.pop(feat)
        else:
            pass

    # - DISTANCE features:
    if 'topological' in features:
        clusterer.add_topological_distance_matrix()
        remaining_features.remove('topological')
    if 'euclidean' in features:
        clusterer.get_euclidean_distance_matrix()
        remaining_features.remove('euclidean')

    # - TEMPORAL features:
    temporal_features = TEMPORAL_DIFF_FEATURES
    for feat in features:
        if feat in temporal_features:
            logger.debug(f"Adding {feat} temporal variable...")
            clusterer.add_temporal_variable(feat, "numeric")
            remaining_features.remove(feat)
        else:
            pass

    if len(remaining_features) != 0:
        logger.warning(f"Some features were not added to the `FeatureClusterer` object: {', '.join(features)}")

    # -- Assemble the global pairwise distance matrix:
    logger.info("Assembling pairwise distance matrix...")
    clusterer.assemble_matrix(features, weights, ignore_outliers=True, delete_outliers=delete_outliers)

    # -- Cluster the global pairwise distance matrix:
    param_kwargs = {'n_clusters': n_clusters}
    logger.info(f"Perform {method} clustering with: {param_kwargs}")
    clusterer.compute_clustering(method=method, **param_kwargs)

    return clusterer


def lineage_info_from_graph(graph):
    """Create a table containing the lineage information from a spatio-temporal graph.

    Parameters
    ----------
    graph : timagetk.TemporalPropertyGraph
        A spatio-temporal graph to get lineage information from.

    Returns
    -------
    pandas.DataFrame
        The table containing the lineage information.
    """
    from timagetk.third_party.ctrl.lineage import Lineage

    time_index = range(0, graph.nb_time_points)
    tp_pairs = list(zip(time_index[:-1], time_index[1:]))
    lineages = {}
    for (ti, tj) in tp_pairs:
        lineages[f"t{ti}-t{tj}"] = {}
        lineage = Lineage(graph.lineage(ti, tj))
        lineages[f"t{ti}-t{tj}"].update({"NbAncestors": len(lineage.get_ancestors())})
        lineages[f"t{ti}-t{tj}"].update({"NbDescendants": len(lineage.get_descendants())})
        lineages[f"t{ti}-t{tj}"].update({"TimeInterval": f"{graph.time_interval(ti, tj)}{graph._time_unit}"})

    return pd.DataFrame.from_dict(lineages)


def clustering_report(clusterer, time_series, out_path, proj_fnames, colormap, idx=None, **kwargs):
    """Generates a clustering report in Markdown format.

    Parameters
    ----------
    clusterer : FeatureClusterer
        The clustering instance to use to build the report.
    time_series : str
        The name of the time-series.
    out_path : str or pathlib.Path
        The absolute output path of the report and associated files.
    proj_fnames : list of str
        The list of file paths for the clustering projection on segmented images.
    colormap : dict
        Cluster indexed dictionary of RGBA colors. Should be the same as in `clustering_figure`.
    idx : str or int, optional
        Id to give to the clustering.
        By default, search in `out_path` for an available integer identifier.

    Other Parameters
    ----------------
    html : bool
        Use this to convert the generated markdown file to HTML format.

    Returns
    -------
    str
        The absolute path to the *report* file.
    str
        The absolute path to the *cluster distances heatmap* file.
    str
        The absolute path to the *element distance to cluster* file.
    str
        The absolute path to the *properties boxplot by cluster* file.

    """
    out_path = Path(out_path)
    report_fame = "{}-clustering_report_{}.md"  # {time_series} {idx}
    html_report_fame = "{}-clustering_report_{}.html"  # {time_series} {idx}
    if idx is None:
        # Start by defining the index to use with the clustering report:
        idx = 1
        while exists(out_path.joinpath(report_fame.format(time_series, idx))):
            idx += 1

    report_fame = out_path.joinpath(report_fame.format(time_series, idx))
    html_report_fame = out_path.joinpath(html_report_fame.format(time_series, idx))

    # Define the file names of the associated figures:
    cdist_fname = out_path.joinpath(f"{time_series}-cluster_distances_heatmap_{idx}.png")
    edist_fname = out_path.joinpath(f"{time_series}-element_distance_to_cluster_{idx}.png")
    bxp_fname = out_path.joinpath(f"{time_series}-properties_boxplot_by_cluster_{idx}.png")

    ccheck = ClustererChecker(clusterer)
    clustering_name = clusterer.clustering_name()
    cluster_ids = clusterer.list_cluster_ids()

    md = "# Clustering report"
    md += "\n\n"
    md += f"``Report generated on {datetime.now().strftime('%A %d %B %Y')}.``\n\n"

    md += f"This is a clustering report for time-series {time_series}.\n\n"

    lin_info = lineage_info_from_graph(clusterer.graph)
    md += lin_info.to_markdown()
    md += "\n\n"

    md += "## Clustering overview"
    md += "\n\n"
    md += f"{clusterer._method.capitalize()} clustering method "
    if clusterer._method in ("single", "ward", "spectral"):
        md += f"with {clusterer._nb_clusters} clusters "
    md += f"on a {clusterer.standardisation_method} standardized pairwise distance matrix "
    md += "made of the following weighted feature combination:\n"
    md += "\n"
    wf = list(zip(clusterer._global_pw_dist_weights, clusterer._global_pw_dist_variables))
    wf = [f"  - {round(float(pc) * 100, 1)}% {feat.replace('_', ' ')}" for (pc, feat) in wf]
    md += '\n'.join(wf)
    md += "\n\n"
    if clusterer._outliers_mgt != "":
        md += f"Please note that **{len(clusterer._outlier_ids)} outlier cells** were "
        if clusterer._outliers_mgt == "ignored":
            md += "excluded from the values taken into account during the standardisation process!"
        if clusterer._outliers_mgt == "deleted":
            md += "excluded from the values taken into account in the creation of the pairwise distance matrix!"

    md += "\n\n"
    md += f"Abbreviated name is: `{clustering_name}`"
    md += "\n"

    md += "### Silhouette metrics"
    md += "\n"
    silhouette, nb_q = clusterer.silhouette_estimator(clusterer._method, k_max=7)
    md += f"As per the silhouette metrics, for this feature combination, the best number of cluster by '{clusterer._method}' method is {nb_q}."
    md += "\n\n"
    md += "|       | " + " | ".join([f"{q} clusters" for q in silhouette.keys()]) + " |"
    md += "\n"
    md += "| " + " | ".join(["-----"] * (len(silhouette.keys()) + 1)) + " |"
    md += "\n"
    md += "| Score | " + " | ".join([f"{round(sc, 3)}" for sc in silhouette.values()]) + " |"
    md += "\n\n"
    md += "Remember: the closer to `1`, the better!"
    md += "\n\n"

    md += "### Pairwise distance matrix 2D projections"
    md += "\n"
    md += "Hereafter we represent the 2D projections of the global pairwise distance matrix by MDS and t-SNE."
    md += "\n\n"
    mds_figname = out_path.joinpath(f"{time_series}-mds_2D_pwdm_{idx}.png")
    tsne_figname = out_path.joinpath(f"{time_series}-tsne_2D_pwdm_{idx}.png")
    plot_mds_dimensionality_reduction(clusterer._global_pw_dist_mat, clusterer.get_clustering_dict(),
                                      figname=mds_figname)
    plot_tsne_dimensionality_reduction(clusterer._global_pw_dist_mat, clusterer.get_clustering_dict(),
                                       figname=tsne_figname)
    md += include_figure_html([Path(mds_figname).name, Path(tsne_figname).name],
                              "Multi-Dimensional Scaling (left) and t-distributed Stochastic Neighbor Embedding (right).")

    md += "\n\n"
    md += "## Clustering projections"
    md += "\n\n"
    for t_idx, proj_fname in enumerate(proj_fnames):
        if t_idx == 0:
            md += f"### Time-point {t_idx} (0{clusterer.graph._time_unit})"
        else:
            md += f"### Time-point {t_idx} ({int(clusterer.graph.time_interval(0, t_idx))}{clusterer.graph._time_unit})"
        md += "\n"
        # md += f"![]({proj_fname.split('/')[-1]})"
        md += include_figure_html(Path(proj_fname).name,
                                  f"t{t_idx} - First layer (left) and second layer (right).")
        md += "\n"

    md += "\n\n"
    md += "## Summary"
    md += "\n\n"

    md += "### Number of cells by cluster"
    md += "\n"
    md += f"The distribution of the {len(ccheck.ids)} cells by cluster is as follows:"
    md += "\n\n"
    nb_elts_by_q = ccheck.nb_elements_by_clusters()
    total_ids = sum(list(nb_elts_by_q.values()))
    pc_elts_by_q = {q: nb_elts_by_q[q] / float(total_ids) * 100 for q in cluster_ids}
    id_by_q = [f"  - Cluster {q} has {nb_elts_by_q[q]} cells ({round(pc_elts_by_q[q], 1)}%)" for q in cluster_ids]
    md += "\n".join(id_by_q)
    md += "\n\n"

    md += "### Time-points representativity by clusters"
    md += "\n\n"
    # time_point_by_q = ccheck.time_point_by_clusters()
    clustering = ccheck.clusterer.graph.cell_property_dict(clustering_name, default=-1, only_defined=True)
    time_points = ccheck.clusterer.graph.cell_property_dict('time-point', list(clustering.keys()))
    n_elt_time_point_by_q = n_elements_by_groups(time_points, clustering)
    pc_elt_time_point_by_q = pc_elements_by_groups(time_points, clustering, sub_groups=True)

    # Initialize the table with a header:
    # Starts with a 'time-point' column:
    md += "| Time-point |"
    # Then add 'cluster id' as columns:
    for q in cluster_ids:
        n_tot_q = nb_elts_by_q[q]
        md += f" Cluster {q} (n={n_tot_q}) |"
    md += "\n"
    # Add header separation row:
    md += "| " + " | ".join(["-----"] * (len(cluster_ids) + 1)) + " |"
    md += "\n"
    # Fill the table:
    for tp in sorted(pc_elt_time_point_by_q.keys()):
        pc_tp_clust = pc_elt_time_point_by_q[tp]
        t_idx = clusterer.graph._elapsed_time.index(tp)
        # Time index and time-point (elapsed time since beginning of acquisition):
        if t_idx == 0:
            md += f"| t{t_idx} |"
        else:
            md += f"| t{t_idx} ({tp}{clusterer.graph._time_unit})|"
        # Number of cells by cluster from this
        for q, pc in pc_tp_clust.items():
            n = n_elt_time_point_by_q[(tp, q)]
            if n == 0:
                md += " - |"
            else:
                md += f" {round(pc * 100, 1)}% ({n}) |"
        md += "\n"
    md += "\n"

    md += "### Layers representativity by clusters"
    md += "\n\n"
    # time_point_by_q = ccheck.time_point_by_clusters()
    clustering = ccheck.clusterer.graph.cell_property_dict(clustering_name, default=-1, only_defined=True)
    layers = ccheck.clusterer.graph.cell_property_dict('layers', list(clustering.keys()))
    n_elt_layer_by_q = n_elements_by_groups(layers, clustering, exclude=[None, -1])
    pc_elt_layer_by_q = pc_elements_by_groups(layers, clustering, exclude=[None, -1], sub_groups=True)

    # Initialize the table with a header:
    # Starts with a 'layers' column:
    md += "| Layers |"
    # Then add 'cluster id' as columns:
    for q in cluster_ids:
        n_tot_q = nb_elts_by_q[q]
        md += f" Cluster {q} (n={n_tot_q}) |"
    md += "\n"
    # Add header separation row:
    md += "| " + " | ".join(["-----"] * (len(cluster_ids) + 1)) + " |"
    md += "\n"
    # Fill the table:
    for layer in sorted(pc_elt_layer_by_q.keys()):
        pc_layer_clust = pc_elt_layer_by_q[layer]
        # Layer id:
        md += f"| L{int(layer)}|"
        # Number of cells by cluster from this
        for q, pc in pc_layer_clust.items():
            n = n_elt_layer_by_q[(layer, q)]
            if n == 0:
                md += " - |"
            else:
                md += f" {round(pc * 100, 1)}% ({n}) |"
        md += "\n"
    md += "\n"

    md += "\n"
    md += "## Cluster shapes"
    md += "\n\n"

    # Initialize the table with a header:
    # Starts with a 'Metrics' column:
    md += "| Metrics |"
    # Then add 'cluster id' as columns:
    for q in cluster_ids:
        n_tot_q = nb_elts_by_q[q]
        md += f" Cluster {q} (n={n_tot_q}) |"
    md += "\n"
    # Add header separation row:
    md += "| " + " | ".join(["-----"] * (len(cluster_ids) + 1)) + " |"
    md += "\n"
    # Fill the table:
    metric = ccheck.within_cluster_distances()
    md += "| within | " + " | ".join([f"{round(m, 3)}" for m in metric.values()]) + " |"
    md += "\n"
    metric = ccheck.between_cluster_distances()
    md += "| between | " + " | ".join([f"{round(m, 3)}" for m in metric.values()]) + " |"
    md += "\n"
    metric = ccheck.cluster_diameters()
    md += "| diameter | " + " | ".join([f"{round(m, 3)}" for m in metric.values()]) + " |"
    md += "\n"
    metric = ccheck.cluster_separation()
    md += "| separation | " + " | ".join([f"{round(m, 3)}" for m in metric.values()]) + " |"
    md += "\n\n"

    md += "Metrics details:\n\n"
    md += "  - *within* stands for within cluster distances"
    md += "\n"
    md += "  - *between* stands for between cluster distances"
    md += "\n"
    md += "  - *diameters* stands for cluster diameters, that is the maximum distance between two cells from the same clusters"
    md += "\n"
    md += "  - *separation* stands for cluster separation, that is the minimum distance between two cells from two different clusters"
    md += "\n\n"

    md += "### Global cluster distance"
    md += "\n\n"
    global_dist = ccheck.global_cluster_distances()
    md += f"  - Sum of within cluster distances: {round(global_dist[0], 3)}"
    md += "\n"
    md += f"  - Sum of between cluster distances: {round(global_dist[1], 3)}"
    md += "\n\n"

    md += "### Cluster distances heatmap"
    md += "\n\n"
    ccheck.cluster_distances_heatmap(print_values=True, figname=cdist_fname)
    md += include_figure_html(Path(cdist_fname).name)  # use basename only as absolute path could change
    md += "\n"

    md += "\n"
    md += "## Elements distance to cluster centers"
    md += "\n\n"
    ccheck.plot_element_distance_to_cluster(cmap=colormap, figname=edist_fname)
    md += include_figure_html(Path(edist_fname).name)  # use basename only as absolute path could change
    md += "\n"

    md += "\n"
    md += "## Cell properties distribution by clusters"
    md += "\n\n"
    ccheck.boxplot_properties_by_cluster(cmap=colormap, figname=bxp_fname)
    md += include_figure_html(Path(bxp_fname).name)  # use basename only as absolute path could change
    md += "\n"

    with open(report_fame, 'w') as f:
        f.write(md)

    if kwargs.get("html", False):
        html = ""
        # HTML header:
        html += "<!DOCTYPE html>\n<html>\n"
        # Table styling:
        html += _add_css_styles([HTML_TABLE_STYLE, FIGCAPTION_STYLE])
        # HTML body:
        html += "<body>\n"
        html += markdown.markdown(md, extensions=['tables'])
        # HTML end of body:
        html += "\n</body>\n</html>"
        # Export HTML file:
        with open(html_report_fame, 'w') as f:
            f.write(html)

    return report_fame, cdist_fname, edist_fname, bxp_fname


def known_clustering(clustering_dict, args, **kwargs):
    """Check if we already computed that clustering configuration.

    Parameters
    ----------
    clustering_dict : dict
        Dictionary of previously computed clustering from JSON file.
    args : parser.parse_args
        Parsed args.

    Other Parameters
    ----------------
    logger : logging.Logger
        A logger

    Returns
    -------
    bool
        ``True`` if the clustering is known, ``False`` otherwise.

    """
    from timagetk.bin.logger import get_dummy_logger
    logger = kwargs.get('logger', get_dummy_logger(sys._getframe().f_code.co_name))

    clustering_name = _clustering_naming(args.method, {'n_clusters': args.n_clusters}, args.weights, args.features,
                                         "deleted" if args.delete_outliers else "ignored")

    try:
        # first element is "idx", second is "clustering name"
        known_clusterings = [k[1] for k in clustering_dict.keys()]
    except:
        return False

    if clustering_name in known_clusterings and not args.force:
        logger.info(f"You already computed this clustering: {clustering_name}")
        return True
    else:
        logger.info(f"New clustering to compute: {clustering_name}")
        return False


def main_task(args, clusterer, idx, seg_imgs, trsfs=None, **kwargs):
    """Compute the clustering, export to CSV and generate report.

    Parameters
    ----------
    args : parser.parse_args
        Parsed args.
    clusterer : FeatureClusterer
        The clusterer instance to use.
    idx : str
        Index or name of the clustering. Used as prefix for the generated files.
    clustering_dict : dict
        Dictionary of previously computed clustering from JSON file.

    Other Parameters
    ----------------
    logger : logging.Logger
        A logger

    """
    from timagetk.bin.logger import get_dummy_logger
    logger = kwargs.get('logger', get_dummy_logger(sys._getframe().f_code.co_name))

    # -- Performs the requested clustering:
    clusterer = make_clustering(clusterer, args.method, args.n_clusters, args.features, args.weights,
                                args.standardisation, args.delete_outliers,
                                identities=args.identities, exclude_identities=args.exclude_identities,
                                force=args.force, logger=logger)

    args.out_path.mkdir(exist_ok=True)  # make sure the directory exists
    # -- Export tissue graph CSV files with clustering:
    csv_file = f"{args.xp_id}_clustering"
    logger.info("Exporting clustering to CSV files...")
    csv_files = to_csv(clusterer.graph, str(args.out_path.joinpath(csv_file)))
    logger.info("Updating JSON metadata file with CSV file locations...")
    add2json(args.json, args.out_dataset, dict(zip(CSV_EXPORT_SUFFIXES, csv_files)))

    # -- Clustering figures:
    logger.info("Generating clustering figures...")
    cmap = colors_array(args.colormap, clusterer.get_number_of_clusters())
    cmap_dict = dict(zip(clusterer.list_cluster_ids(), cmap))
    vtk_cmap = multicolor_categorical_colormap(cmap, "clustering")

    figure_fname = join(args.out_path,
                        f"{args.xp_id}-clustering_projection_{idx}_t" + "{}" + f"_L{'L'.join(map(str, args.layers))}.png")
    proj_fnames = clustering_figures(clusterer.graph, clusterer.clustering_name(), seg_imgs, args.layers, trsfs=trsfs,
                                     figname=figure_fname, orientation=args.orientation, colormap=vtk_cmap,
                                     logger=logger)

    # -- Clustering report:
    logger.info("Generating clustering report...")
    report_fame, cdist_fname, edist_fname, bxp_fname = clustering_report(clusterer, args.xp_id, args.out_path,
                                                                         proj_fnames,
                                                                         cmap_dict, idx=idx, html=args.html)
    # Save file locations associated to this clustering in the JSON file:
    cdict = {(idx, clusterer.clustering_name()): [report_fame, cdist_fname, edist_fname, bxp_fname]}
    logger.info("Updating JSON metadata file with report and associated file locations...")
    add2json(args.json, args.out_dataset, cdict)


def trsf_temporal_resampling(trsfs, time_index, reindex=True):
    """Compose registration trsf to selected time indexes.

    Examples
    --------
    >>> from timagetk.bin.feature_clustering import trsf_temporal_resampling
    >>> from timagetk.tasks.json_parser import json_trsf_parsing
    >>> json = "/data/FPS_paper/Data/FM1/FM1.json"
    >>> # Get the dictionary of intensity images indexed by time intervals (len-2 tuples)
    >>> trsf_json = json_trsf_parsing(json)
    >>> trsf_files = trsf_json["rigid"]
    >>> all_time_points = [0, 10, 18, 24, 32, 40, 48, 57, 64, 72, 81, 88, 96, 104, 112, 120, 128, 132]
    >>> time_points = [0, 18, 40, 57, 72, 88, 104, 120, 132]
    >>> time_index = [all_time_points.index(tp) for tp in time_points]
    >>> trsfs = trsf_temporal_resampling(trsf_files, time_index)
    >>> print(list(trsfs.keys()))
    [(0, 1), (1, 2), (2, 3), (3, 4), (4, 5), (5, 6), (6, 7), (7, 8)]
    >>> trsfs = trsf_temporal_resampling(trsf_files, time_index, reindex=False)
    >>> print(list(trsfs.keys()))
    [(0, 2), (2, 5), (5, 7), (7, 9), (9, 11), (11, 13), (13, 15), (15, 17)]
    """
    from timagetk.algorithms.trsf import compose_trsf
    out_trsf = {}
    t_intervals = list(zip(time_index[:-1], time_index[1:]))
    for tp, (ti, tj) in enumerate(t_intervals):
        if reindex:
            key = (tp, tp + 1)
        else:
            key = (ti, tj)
        out_trsf[key] = compose_trsf([read_trsf(trsfs[(t, t + 1)]) for t in range(ti, tj)])
    return out_trsf


def main(args):
    # Get the name of the series
    args.xp_id = json_series_metadata(args.json)['series_name']
    # Get time related info
    time_md = json_time_metadata(args.json)
    # Defines some location variables:
    args.root_path = Path(args.json).parent.absolute()  # the root directory is where the JSON file is...
    args.out_path = args.root_path.joinpath(args.out_dataset)  # but needs absolute path to save files
    args.out_path.mkdir(exist_ok=True)  # make sure the directory exists

    # - Initialize logger
    logger = get_logger(cli_name, args.root_path.joinpath(f'{cli_name}.log'), args.log_level.upper())
    # Log first welcome message
    logger.info(f"Feature clustering for time-series '{args.xp_id}' located under '{args.root_path}'.")

    # ----- CLUSTERING FILES -----
    # - Get the list of existing clustering:
    if args.force:
        clustering_dict = clear_existing_clustering(args, logger)
    else:
        clustering_dict = json_clustering_parsing(args.json, args.out_dataset)
        logger.info(f"Found {len(clustering_dict)} clustering: {list(clustering_dict.keys())}")
    logger.debug(clustering_dict)

    # ----- GRAPH CSV FILES -----
    # - Get the dictionary of graph CSVs indexed by time-point:
    csv_files = json_csv_parsing(args.json, args.features_dataset)
    logger.info(f"Found {len(csv_files)} CSV files under '{args.features_dataset}'.")
    logger.debug(csv_files)

    # ----- SEGMENTED IMAGES FILES -----
    # - Get the dictionary of segmented images indexed by time-point:
    seg_imgs = json_segmentation_parsing(args.json, args.segmentation_dataset)
    logger.info(f"Found {len(seg_imgs)} segmented images under '{args.segmentation_dataset}'.")
    logger.debug(seg_imgs)
    args.n_imgs = len(seg_imgs)

    # ----- CONSECUTIVE TRANSFORMATION FILES -----
    # - Get the dictionary of transformations indexed by time intervals (len-2 tuples)
    trsfs = get_trsf_files(args, logger)

    # ----- TIME INDEXES -----
    # - Try to get the 'selected' time-points from the JSON 'time' metadata:
    all_time_points = time_md.get("points", None)
    sel_time_points = time_md.get("selected", None)
    t_unit = time_md.get("unit", "?")
    if sel_time_points is not None:
        logger.info(f"Got a list of 'selected' time-points from JSON: {sel_time_points}")
        args.time_index = [all_time_points.index(tp) for tp in sel_time_points]
        logger.info(f"Corresponding time-indexes: {args.time_index}")
        # Select corresponding segmented images and re-index segmentation dictionary with contiguous time-indexes:
        seg_imgs = {t: seg_imgs[ti] for t, ti in enumerate(args.time_index)}
        # Select & compose corresponding registration trsfs and relabel with contiguous time-indexes:
        if trsfs is not None:
            trsfs = trsf_temporal_resampling(trsfs, args.time_index)
    else:
        sel_time_points = all_time_points
        args.time_index = list(range(0, len(sel_time_points)))
    logger.info(f"Corresponding time-intervals (in {t_unit}) from JSON: {time_intervals(sel_time_points)}")

    # - Build the (Temporal)TissueGraph from the CSV files:
    logger.info("Re-build the (Temporal)TissueGraph from the CSV files...")
    ttg = from_csv(csv_files)
    logger.info(f"Obtained a TemporalTissueGraph with the following cell features:{sorted(ttg.list_cell_properties())}")

    # Try to load defined identities:
    identity_folder = args.root_path.joinpath(args.identity_dataset)
    identity_folder.mkdir(exist_ok=True)  # make sure the directory exists
    identity_files = identity_folder.iterdir()
    identity_files = [id_file for id_file in identity_files if id_file.suffix == '.txt']
    logger.info(f"Found {len(identity_files)} files defining cell identities...")
    if identity_files != []:
        for id_file in identity_files:
            logger.info(f"Importing '{id_file.stem}' cell identity to the {clean_type(ttg)}.")
            import_identity(ttg, id_file, time_index=args.time_index)

    # Try to get the image/microscope 'orientation' from the JSON metadata:
    orientation = json_orientation_metadata(args.json, None)
    if orientation is not None:
        args.orientation = orientation
        logger.info(f"Got an image/microscope 'orientation' from JSON: `{args.orientation}`")

    scaler = RobustScaler(quantile_range=(10., 90.)).fit_transform

    if args.toml != "":
        logger.info(f"Using provided TOML configuration file: '{args.toml}'")
        # Check the TOML file indeed contain 'clustering' configurations
        config = toml.load(args.toml)
        if 'clustering' not in config:
            logger.critical("Could not find 'clustering' configurations in provided TOML!")
            sys.exit("Check the TOML configuration file.")
        config = config['clustering']
        # Get the 'global clustering' configuration (as a dict):
        global_config = config.pop("global", {})
        # Loop over given "specific clustering configuration" and run clustering task:
        for clust_id, clust_conf in config.items():
            # Copy the global clustering config:
            current_clust_cfg = cp.deepcopy(global_config)
            # Convert weights given as str (useful to represent fractions like `1/3.`):
            if "weights" in clust_conf and isinstance(clust_conf["weights"][0], str):
                from fractions import Fraction
                try:
                    clust_conf["weights"] = [Fraction(v) for v in clust_conf["weights"]]
                except Exception:
                    clust_conf["weights"] = [eval(v) for v in clust_conf["weights"]]
            # Override global clustering config with specific clustering config:
            current_clust_cfg.update(clust_conf)
            # Set the attributes of the parsed argument with the current 'clustering' config:
            for k, v in current_clust_cfg.items():
                setattr(args, k, v)
            # If clustering is not yet known, run the task:
            if not known_clustering(clustering_dict, args, logger=logger):
                # we recreate the `FeatureClusterer` instance each time as we may not have the same list of ids
                current_clusterer = FeatureClusterer(ttg, args.standardisation, outlier_scaler=scaler,
                                                     outliers_cluster_id=args.outliers_cluster_id)
                main_task(args, current_clusterer, clust_id, seg_imgs, trsfs, logger=logger)
    else:
        if not known_clustering(clustering_dict, args, logger=logger):
            current_clusterer = FeatureClusterer(ttg, args.standardisation, outlier_scaler=scaler,
                                                 outliers_cluster_id=args.outliers_cluster_id)
            main_task(args, current_clusterer, None, seg_imgs, trsfs, logger=logger)


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
