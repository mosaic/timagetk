#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
import os
from os.path import exists
from os.path import join
from os.path import split

from timagetk.third_party.ctrl.io import AVAILABLE_LINEAGE_FMT
from timagetk.third_party.ctrl.io import DEFAULT_LINEAGE_FMT

from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.io.graph import from_csv
from timagetk.tasks.features import CELL_SCALAR_FEATURES
from timagetk.tasks.features import TEMPORAL_DIFF_FEATURES
from timagetk.tasks.json_parser import DEFAULT_LINEAGE_DATASET
from timagetk.tasks.json_parser import DEFAULT_OBJ_DATASET
from timagetk.tasks.json_parser import DEFAULT_SEGMENTATION_DATASET
from timagetk.tasks.json_parser import add2json
from timagetk.tasks.json_parser import json_csv_parsing
from timagetk.tasks.json_parser import json_lineage_parsing
from timagetk.tasks.json_parser import json_mesh_parsing
from timagetk.tasks.json_parser import json_segmentation_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.json_parser import json_time_metadata
from timagetk.third_party.morphonet import _test_morphonet
from timagetk.third_party.morphonet import temporal_graph_to_info_file
from timagetk.third_party.morphonet import time_series_to_morphonet_obj

_test_morphonet()

DESCRIPTION = """Convert labelled images to cell-based mesh OBJ files and basic info files ('space' & 'time').
"""


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION,
                                     epilog="This requires a valid MorphoNet account!")

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('-seg', '--segmentation_dataset', type=str, default=DEFAULT_SEGMENTATION_DATASET,
                        help=f"the dataset containing the segmented images, '{DEFAULT_SEGMENTATION_DATASET}' by default.")
    parser.add_argument('-lin', '--lineage_dataset', type=str, default=DEFAULT_LINEAGE_DATASET,
                        help=f"the dataset containing the lineages, '{DEFAULT_LINEAGE_DATASET}' by default.")
    parser.add_argument('-fmt', '--lineage_format', type=str, default=DEFAULT_LINEAGE_FMT,
                        choices=AVAILABLE_LINEAGE_FMT,
                        help=f"the format used to read the lineage file, '{DEFAULT_LINEAGE_FMT}' by default.")

    mesh_arg = parser.add_argument_group('meshing arguments')
    mesh_arg.add_argument('--smoothing_iteration', type=int, default=10,
                          help="The number of smoothing iterations to apply to each cell mesh.")
    mesh_arg.add_argument('--target_reduction', type=float, default=0.5,
                          help="The percentage of reduction to achieve by the decimation step.")
    mesh_arg.add_argument('--voxelsize', type=float, nargs='+', default=0.5,
                          help="use this to resample the labelled image prior to meshing. Can be defined for each axis, ZYX ordered.")

    cells_arg = parser.add_argument_group('cell selection arguments')
    cells_arg.add_argument('--exclude_marginal_cells', action="store_true",
                           help="exclude marginal cells from mesh creation.")
    cells_arg.add_argument('--distance_from_margins', type=int, default=2,
                           help="the voxel distance from the stack margin to consider.")
    cells_arg.add_argument('--background_id', type=int, default=1,
                           help="the id corresponding to the background position.")
    cells_arg.add_argument('--select_cell_groups', type=str, nargs='+',
                           help="cell group names to keep for meshing. Should be defined in cell CSV file.")

    ppty_arg = parser.add_argument_group('cell features arguments')
    ppty_arg.add_argument('--cell_features', type=str, nargs='+', default="none",
                          help="The cell features to export. Use 'all' to select all the possible features")

    out_arg = parser.add_argument_group('output arguments')
    out_arg.add_argument('--out_dataset', type=str, default=DEFAULT_OBJ_DATASET,
                         help=f"export the mesh OBJ files in a '{DEFAULT_OBJ_DATASET}' dataset")
    out_arg.add_argument('--rigid-registration', action="store_true",
                         help="use this to register rigidly all the meshes onto the same geometry (last time-point)."
                              " Rigid transformation is computed based on the lineage between two consecutive time-points.")
    out_arg.add_argument('--force', action="store_true",
                         help="use this to force the computation of mesh and info.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default")

    return parser


def main(args):
    # Get the name of the series
    series_md = json_series_metadata(args.json)
    xp_id = series_md['series_name']
    # Defines some location variables:
    json_root_path, _ = split(args.json)  # the root directory is where the JSON file is...
    abs_out_path = join(json_root_path, args.out_dataset)  # but needs absolute path to save files

    logger = get_logger('create_cell_mesh', join(json_root_path, 'create_cell_mesh.log'), args.log_level.upper())

    # Make sure the destination directory exists:
    try:
        os.mkdir(abs_out_path)
    except:
        pass

    seg_img_files = json_segmentation_parsing(args.json, args.segmentation_dataset)
    n_imgs = len(seg_img_files)
    logger.info(f"Found {n_imgs} segmented image files under '{args.segmentation_dataset}'.")
    logger.debug(seg_img_files)

    time_points = json_time_metadata(args.json)['points']
    logger.info(f"Found {len(time_points)} time-points indexed.")
    logger.debug(time_points)
    try:
        assert n_imgs == len(time_points)
    except AssertionError:
        logger.error(f"Number of segmented images ({n_imgs}) and time-points ({len(time_points)}) differ!")
        exit(1)

    lineage_files = json_lineage_parsing(args.json, args.lineage_dataset)
    logger.info(f"Found {len(lineage_files)} lineage files under '{args.lineage_dataset}'.")
    logger.debug(lineage_files)

    try:
        assert len(lineage_files) == n_imgs - 1
    except AssertionError:
        logger.error("We need `N-1` lineage files, where `N` is the number of segmented images...")
        logger.error(
            f"Found {n_imgs} segmented images and {len(lineage_files)} lineage files.")
        exit(1)

    if ((len(args.cell_features) == 1) & (args.cell_features[0] == "all")) | (args.cell_features == "all"):
        logger.info("Selected ALL cell features!")
        args.cell_features = CELL_SCALAR_FEATURES + TEMPORAL_DIFF_FEATURES
    elif (len(args.cell_features) == 1) & (args.cell_features[0] == "none") | (args.cell_features == "none"):
        logger.info("Selected NO cell features!")
        args.cell_features = []
    else:
        logger.info(f"Selected {len(args.cell_features)} cell features: {args.cell_features}")

    # - Get the existing list of info files as they may have been already extracted
    obj_mesh, obj_info = json_mesh_parsing(args.json, args.out_dataset)

    # Check that all mesh and info files exists:
    all_mesh = len(obj_mesh) == n_imgs and all(exists(mesh) for t, mesh in obj_mesh.items())
    if all_mesh:
        logger.info("All mesh files are already defined!")
    else:
        missing_mesh = set(range(0, n_imgs)) - {t for t, f in obj_mesh.items() if not exists(f)}
        logger.info(f"Missing mesh file at times: {missing_mesh}")

    all_info = all(info in obj_info and exists(obj_info[info]) for info in ['space', 'time'])
    if all_info:
        logger.info("All info files are already defined!")
    else:
        missing_info = [info for info in ['space', 'time'] if info not in obj_info or not exists(obj_info[info])]
        logger.info(f"Missing info files: {missing_info}")

    ttg = None
    selected_cells = []

    # - List of cells:
    if args.select_cell_groups is not None:
        logger.info("Exporting info files about selected cell groups...")
        ttg = get_ttg(args, ttg)
        ppty_list = ttg.list_cell_properties()
        valid_cell_groups = [group for group in args.select_cell_groups if group in ppty_list]
        n_valid_groups = len(valid_cell_groups)
        if n_valid_groups != len(args.select_cell_groups):
            missing_cg = list(set(args.select_cell_groups) - set(valid_cell_groups))
            n = len(missing_cg)
            logger.warning(
                f"Could not find {n} cell group{'s' if n > 1 else ''} in temporal tissue graph from cell CSV!")
            logger.warning(f"Missing cell group{'s' if n > 1 else ''}: {', '.join(missing_cg)}")

        for cell_group in valid_cell_groups:
            selected_cells += ttg.list_cells_with_identity(cell_group, rank=0)

        info_fnames = temporal_graph_to_info_file(ttg, valid_cell_groups, ['group'] * n_valid_groups,
                                                  path=abs_out_path, suffix=f"{xp_id}-")
        logger.info("Updating JSON metadata file...")
        obj_info.update(info_fnames)
        add2json(args.json, args.out_dataset, {"mesh": obj_mesh, "info": obj_info})

    voxelsize = args.voxelsize
    if isinstance(voxelsize, list):
        if len(voxelsize) == 1:  # deal with isotropic case
            voxelsize = voxelsize[0]

    if not all_mesh or not all_info or args.force:
        logger.info("Starting cell-based meshing task")
        seg_img_files = [f for t, f in seg_img_files.items()]
        lineage_files = [f for t, f in lineage_files.items()]
        obj_fnames, info_fnames, process = time_series_to_morphonet_obj(seg_img_files, time_points, lineage_files,
                                                                       path=abs_out_path, suffix=f"{xp_id}-",
                                                                       smoothing_iteration=args.smoothing_iteration,
                                                                       target_reduction=args.target_reduction,
                                                                       voxelsize=voxelsize, cells_to_keep=selected_cells,
                                                                       rigid_registration=args.rigid_registration,
                                                                       exclude_marginal_cells=args.exclude_marginal_cells,
                                                                       distance_from_margins=args.distance_from_margins,
                                                                       background_id=args.background_id,
                                                                       return_process=True)
        logger.info("Updating JSON metadata file...")
        obj_info.update(info_fnames)  # update the info files dictionary
        add2json(args.json, args.out_dataset, {"mesh": obj_fnames, "info": obj_info})
    else:
        logger.info("Found the required mesh and info files, skipping mesh generation!")

    all_cell_info = False
    if args.cell_features != []:
        all_cell_info = all(feat in obj_info and exists(obj_info[feat]) for feat in args.cell_features)
        if all_info:
            logger.info("All cell features info files are already defined!")
        else:
            missing_info = [feat for feat in args.cell_features if feat not in obj_info or not exists(obj_info[feat])]
            logger.info(f"Missing cell features info files: {missing_info}")

    if args.cell_features != [] and (not all_cell_info or args.force):
        n_features = len(args.cell_features)
        ttg = get_ttg(args, ttg)
        # - Export as info files:
        info_fnames = temporal_graph_to_info_file(ttg, args.cell_features, ['float'] * n_features,
                                                  path=abs_out_path, suffix=f"{xp_id}-")
        logger.info("Updating JSON metadata file...")
        obj_info.update(info_fnames)
        add2json(args.json, args.out_dataset, {"mesh": obj_mesh, "info": obj_info})

    # Save the process
    # TODO: fix formatting (filepath are split in the json file)
    add2json(join(abs_out_path, "processing.json"), args.out_dataset, process,
             required_object_metadata=False,
             keep_basename=False)  # Keep complete filepath & no need for `object`

def get_ttg(args, ttg):
    """Return the temporal tissue graph."""
    if ttg is None:
        # - Get CSV files:
        csv_files = json_csv_parsing(args.json)
        # - Make a temporal tissue graph:
        ttg = from_csv(csv_files, background=args.background_id)
        # print(ttg.list_cell_properties())
    return ttg


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
