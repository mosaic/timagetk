#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
import sys
from os.path import join
from pathlib import Path

from timagetk.third_party.ctrl.io import AVAILABLE_LINEAGE_FMT
from timagetk.third_party.ctrl.io import DEFAULT_LINEAGE_FMT
from timagetk.third_party.ctrl.io import read_lineage
from timagetk.third_party.ctrl.lineage import extend_lineage

from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.bin.tasks import load_lineage_from_files
from timagetk.io.graph import CSV_EXPORT_SUFFIXES
from timagetk.io.graph import import_identity
from timagetk.io.graph import to_csv
from timagetk.tasks.json_parser import DEFAULT_FEATURES_DATASET
from timagetk.tasks.json_parser import DEFAULT_IDENTITY_DATASET
from timagetk.tasks.json_parser import DEFAULT_LINEAGE_DATASET
from timagetk.tasks.json_parser import DEFAULT_SEGMENTATION_DATASET
from timagetk.tasks.json_parser import DEFAULT_TRSF_DATASET
from timagetk.tasks.json_parser import add2json
from timagetk.tasks.json_parser import json_lineage_parsing
from timagetk.tasks.json_parser import json_orientation_metadata
from timagetk.tasks.json_parser import json_segmentation_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.json_parser import json_time_metadata
from timagetk.tasks.json_parser import time_intervals
from timagetk.tasks.temporal_tissue_graph import temporal_tissue_graph_from_images
from timagetk.util import clean_type

OUT_DATASET = DEFAULT_FEATURES_DATASET

DESCRIPTION = """Cell feature extraction procedure.

"""
cli_name = Path(__file__).stem


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION)

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('-seg', '--segmentation_dataset', type=str, default=DEFAULT_SEGMENTATION_DATASET,
                        help=f"the dataset containing the segmented images, '{DEFAULT_SEGMENTATION_DATASET}' by default.")
    parser.add_argument('-lin', '--lineage_dataset', type=str, default=DEFAULT_LINEAGE_DATASET,
                        help=f"the dataset containing the lineages, '{DEFAULT_LINEAGE_DATASET}' by default.")
    parser.add_argument('-fmt', '--lineage_format', type=str, default=DEFAULT_LINEAGE_FMT,
                        choices=AVAILABLE_LINEAGE_FMT,
                        help=f"the format used to read the lineage file, '{DEFAULT_LINEAGE_FMT}' by default.")
    parser.add_argument('-trsf', '--trsf_dataset', type=str, default=DEFAULT_TRSF_DATASET,
                        help=f"name of the dataset containing the transformations, '{DEFAULT_TRSF_DATASET}' by default.")
    parser.add_argument('--identity_dataset', type=str, default=DEFAULT_IDENTITY_DATASET,
                        help=f"name of the dataset containing the cell identities as text files, '{DEFAULT_IDENTITY_DATASET}' by default.")

    img_arg = parser.add_argument_group('images arguments')
    img_arg.add_argument('--orientation', type=int, default=1, choices={-1, 1},
                         help="image orientation, use `-1` with an inverted microscope, `1` by default.")

    feat_arg = parser.add_argument_group('features arguments')
    feat_arg.add_argument('--cell_features', type=str, nargs='+', default="all",
                           help="the cell features to export.")
    feat_arg.add_argument('--wall_features', type=str, nargs='+', default="all",
                           help="the cell features to export.")
    feat_arg.add_argument('--temporal_features', type=str, nargs='+', default="all",
                           help="the cell features to export.")
    feat_arg.add_argument('--wall_area_threshold', type=float, default=None,
                           help="use this to defines a minimum contact area between two neighbors for graph creation.")
    feat_arg.add_argument('--epidermis_area_threshold', type=float, nargs=1, default=None,
                           help="use this to defines a minimum contact area with the background for graph creation.")
    feat_arg.add_argument('--exclude_marginal_cells', action="store_true",
                           help="use this to exclude the cells at the margins of the stack from features computation.")
    feat_arg.add_argument('--voxel_distance_from_margin', type=int, nargs=1, default=2,
                           help="the distance, in voxels, from the stack margin to use to defines cells at the stack margins. `5` by default.")

    graph_arg = parser.add_argument_group('graph arguments')
    graph_arg.add_argument('--dual', action="store_true",
                           help="construct the dual graph with cell-edges and cell-vertices.")

    out_arg = parser.add_argument_group('output arguments')
    out_arg.add_argument('-o', '--out_dataset', type=str, default=OUT_DATASET,
                          help=f"the dataset name to use to save lineage files, '{OUT_DATASET}' by default.")
    out_arg.add_argument('--force', action="store_true",
                          help="use this to force the computation of tissue graph and features.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                          help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default.")

    return parser


def main_from_image():
    pass

def main_from_images():
    pass


def main(args):
    # Get the name of the time-series
    args.xp_id = json_series_metadata(args.json)['series_name']
    # Get 'time' metadata
    time_md = json_time_metadata(args.json)
    # Defines some location variables:
    args.root_path = Path(args.json).parent.absolute()  # the root directory is where the JSON file is...
    args.out_path = args.root_path.joinpath(args.out_dataset)  # but needs absolute path to save files
    args.out_path.mkdir(exist_ok=True)  # make sure the directory exists

    # Initialize logger
    logger = get_logger(cli_name, args.root_path.joinpath(f'{cli_name}.log'), args.log_level.upper())
    # Log first welcome message
    logger.info(f"Cell feature computation for time-series '{args.xp_id}' located under '{args.root_path}'.")

    # TODO: create two methods to work with tissue_graph OR temporal_tissue_graph

    # ----- LINEAGE FILES -----
    # - Get the list of lineage indexed by len-2 time-point tuple
    lineage_files = json_lineage_parsing(args.json, args.lineage_dataset)
    logger.info(f"Found {len(lineage_files)} lineage files under '{args.lineage_dataset}'.")
    logger.debug(lineage_files)

    # ----- SEGMENTED IMAGES FILES -----
    # - Get the dictionary of segmented images indexed by time-point:
    seg_imgs = json_segmentation_parsing(args.json, args.segmentation_dataset)
    logger.info(f"Found {len(seg_imgs)} segmented images under '{args.segmentation_dataset}'.")
    logger.debug(seg_imgs)
    # Check the consistency of number of lineage & image files
    try:
        assert len(lineage_files) == len(seg_imgs) - 1
    except AssertionError:
        logger.error("We need `N-1` lineage files, where `N` is the number of segmented images...")
        logger.error(f"Found {len(seg_imgs)} segmented images and {len(lineage_files)} lineage files.")
        exit(1)
    else:
        args.n_imgs = len(seg_imgs)

    # ----- IMAGE ORIENTATION -----
    # Try to get the image/microscope 'orientation' from the JSON metadata:
    orientation = json_orientation_metadata(args.json, None)
    if orientation is not None:
        # If defined in JSON, override input argument:
        args.orientation = orientation
        logger.info(f"Got an image/microscope 'orientation' from JSON: `{args.orientation}`")
    else:
        logger.info(f"Got an image/microscope 'orientation' from input argument: `{args.orientation}`")

    # ----- CELL FEATURES -----
    if args.cell_features == "all":
        logger.info("Selected ALL cell features!")
    elif args.cell_features != []:
        logger.info(f"Selected {len(args.cell_features)} cell features: {args.cell_features}")
    else:
        logger.info("Selected NO cell features!")

    # ----- CELL-WALL FEATURES -----
    if args.wall_features == "all":
        logger.info("Selected ALL wall features!")
    elif args.wall_features != []:
        logger.info(f"Selected {len(args.wall_features)} wall features: {args.wall_features}")
    else:
        logger.info("Selected NO wall features!")

    # ----- TEMPORAL FEATURES -----
    if args.temporal_features == "all":
        logger.info("Selected ALL temporal features!")
    elif args.temporal_features != []:
        logger.info(f"Selected {len(args.temporal_features)} temporal features: {args.temporal_features}")
    else:
        logger.info("Selected NO temporal features!")

    # Try to load defined identities:
    identity_folder = args.root_path.joinpath(args.identity_dataset)
    identity_folder.mkdir(exist_ok=True)  # make sure the directory exists
    identity_files = identity_folder.iterdir()
    identity_files = [id_file for id_file in identity_files if id_file.suffix == '.txt']
    logger.info(f"Found {len(identity_files)} files defining cell identities...")

    # ----- TIME INDEXES -----
    # - Check definition of time-points ('point' entry) in JSON 'time' metadata:
    all_time_points = time_md.get("points", [])
    if all_time_points == []:
        logger.critical("No 'point' entry defined in 'time' metadata!")
        sys.exit("Required 'point' entry in 'time' metadata is missing!")
    # - Try to get the 'selected' time-points from the JSON 'time' metadata:
    sel_time_points = time_md.get("selected", [])
    t_unit = time_md.get("unit", "?")
    if sel_time_points != []:
        logger.info(f"Got a list of 'selected' time-points from JSON: {sel_time_points}")
        time_index = [all_time_points.index(tp) for tp in sel_time_points]
        logger.info(f"This correspond to the following time-indexes: {time_index}")
    else:
        sel_time_points = all_time_points
        time_index = list(range(0, len(sel_time_points)))
    logger.info(f"Corresponding time-intervals (in {t_unit}) from JSON: {time_intervals(sel_time_points)}")

    # -- Load the lineages corresponding to selected time-indexes:
    lineages = []
    t_idx_pairs = list(zip(time_index[:-1], time_index[1:]))
    for (ti, tj) in t_idx_pairs:
        lineages.append(load_lineage_from_files(lineage_files, ti, tj, fmt=args.lineage_format))

    # -- Group some input arguments in keyword argument dict:
    kwargs = {'exclude_marginal_cells': args.exclude_marginal_cells,
              'wall_area_threshold': args.wall_area_threshold,
              'epidermis_area_threshold': args.epidermis_area_threshold,
              'voxel_distance_from_margin': args.voxel_distance_from_margin,
              'orientation': args.orientation,
              'force': args.force}

    tissues = [seg_imgs[i] for i in time_index]
    # -- Create a temporal tissue graph from selected tissues and lineages:
    ttg = temporal_tissue_graph_from_images(tissues, lineages, sel_time_points, time_unit=t_unit,
                                            features=args.cell_features,
                                            wall_features=args.wall_features,
                                            temp_features=args.temporal_features,
                                            extract_dual=args.dual, csv_dir=args.out_path, **kwargs
                                            )

    # Import cell identities to the TPG, if any:
    if identity_files != []:
        for id_file in identity_files:
            logger.info(f"Importing '{id_file.stem}' cell identity to the {clean_type(ttg)}.")
            import_identity(ttg, id_file, time_index=time_index)

    csv_file = f"{args.xp_id}_features"
    csv_files = to_csv(ttg, join(args.out_path, csv_file), export_dual=args.dual)
    logger.info("Updating JSON metadata file...")
    add2json(args.json, args.out_dataset, dict(zip(CSV_EXPORT_SUFFIXES, csv_files)))


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
