#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
import time
from pathlib import Path

import click

from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.io import imsave
# from timagetk.io import imread
from timagetk.io.image import pims_read as imread
from timagetk.tasks.json_parser import DEFAULT_EXT
from timagetk.tasks.json_parser import DEFAULT_INTENSITY_DATASET
from timagetk.tasks.json_parser import add2json
from timagetk.tasks.json_parser import json_channel_names
from timagetk.tasks.json_parser import json_intensity_image_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.json_parser import json_time_metadata
from timagetk.tasks.json_parser import time_intervals
from timagetk.third_party.plantseg import DEF_DEVICE
from timagetk.third_party.plantseg import DEF_RESCALE
from timagetk.third_party.plantseg import DEF_UNET_MODEL
from timagetk.third_party.plantseg import DEVICES
from timagetk.third_party.plantseg import UNET_MODELS
from timagetk.third_party.plantseg import unet_predict
from timagetk.third_party.plantseg import unet_tiled_predict

OUT_DATASET = "unet_predict"
DESCRIPTION = """
Performs cell boundary prediction using convolutional neural network (CNN).

A general recommendation: try to provide an image as close as possible to the one used for training.
See the link below to get those info.
"""
MODELS_URL = "https://kreshuklab.github.io/plant-seg/chapters/plantseg_models/"
EPILOG = f"Visit this URL for more information about pre-trained models: {MODELS_URL}"


@click.command(help=DESCRIPTION, epilog=EPILOG)
@click.argument('json-file', type=click.Path(exists=True))
@click.option('-int', '--intensity-dataset', type=str,
              default=DEFAULT_INTENSITY_DATASET, show_default=True,
              help=f"Name of the dataset that contains the intensity images to segment.")
@click.option('-t', '--time-index', type=int, multiple=True,
              help="A list of time-index to select a subset of images from the time-series, all of them by default.")
@click.option('-r', '--ref-idx', type=int, multiple=True,
              default=[0], show_default=True,
              help="A list of reference image index, in case of multi-angle images, to select from the time-series.")
@click.option('--channel', type=str, default=None,
              help="Channel to perform prediction for, mandatory for multichannel images.")
@click.option('--tiled', is_flag=True,
              help=f"Whether to use tiled method or original method to gather predicted patches.")
@click.option('--model-name', type=click.Choice(UNET_MODELS),
              default=DEF_UNET_MODEL, show_default=True,
              help=f"Pre-trained cell tissue model to use for prediction.")
@click.option('--device', type=click.Choice(DEVICES),
              default=DEF_DEVICE, show_default=True,
              help=f"Device to use to perform prediction.")
@click.option('--rescaling', type=str,
              default=DEF_RESCALE, show_default=True,
              help=f"Rescale the image to given voxelsize prior to performing prediction. "
                   "Can be a single float for isometric resampling or a list of coma separated values. "
                   "For example '0.5' for isometric resampling, or '0.7,0.6,0.6' for a different ZYX voxelsize.")
@click.option('--ext', type=str,
              default=DEFAULT_EXT, show_default=True,
              help=f"File extension used to save predicted images.")
@click.option('--out-dataset', type=str,
              default=OUT_DATASET, show_default=True,
              help=f"Name of the dataset to export the predicted images.")
@click.option('--log-level', type=click.Choice(LOG_LEVELS),
              default=DEFAULT_LOG_LEVEL, show_default=True,
              help=f"Logging level to use.")
def main(json_file, intensity_dataset, time_index, ref_idx, channel, tiled, model_name, device, rescaling, ext,
         out_dataset,
         log_level):
    # Get the name of the series
    series_md = json_series_metadata(json_file)
    xp_id = series_md['series_name']
    # Defines some location variables:
    root_path = Path(json_file).parent.absolute()  # the root directory is where the JSON file is...
    out_path = root_path.joinpath(out_dataset)  # but needs absolute path to save files
    out_path.mkdir(exist_ok=True)  # make sure the directory exists

    # Initialize logger
    logger = get_logger(__name__, root_path / f'{__name__}.log', log_level.upper())
    # Log first welcome message
    logger.info(f"3D U-Net prediction for time-series '{xp_id}' located under '{root_path}'.")

    # --- INTENSITY IMAGES ---
    intensity_imgs = parse_intensity_imgs(json_file, intensity_dataset, ref_idx, logger)

    # --- CHANNEL NAMES ---
    ch_names = parse_channel_names(json_file, logger)

    # --- TIME POINTS ---
    time_index, intensity_imgs = parse_time_index(json_file, time_index, logger, intensity_imgs)

    # - Parse rescaling argument to accept input as single float or list of floats:
    if isinstance(rescaling, str):
        rescaling = [float(r) for r in rescaling.split(',')]
    if isinstance(rescaling, list) and len(rescaling) == 1:
        rescaling = rescaling * 3

    predictions = {}  # prediction file names dictionary, indexed by time-series index
    logger.info("Starting cell tissue prediction task...")
    for im_idx, int_img_fname in intensity_imgs.items():
        # Load the intensity image file
        if channel is not None:
            logger.info(f"Loading channel '{channel}' of image file '{int_img_fname}' ({im_idx})...")
        else:
            logger.info(f"Loading single channel of image file '{int_img_fname}' ({im_idx})...")
        int_img = imread(int_img_fname, channel_names=ch_names, channel=channel)

        pred_start = time.time()
        # Perform prediction
        if tiled:
            pred_img = unet_tiled_predict(int_img, model_name=model_name, device=device,
                                          rescaling=rescaling)
        else:
            pred_img = unet_predict(int_img, model_name=model_name, device=device,
                                    rescaling=rescaling)
        pred_stop = time.time()

        # Save the predictions
        pred_filename = f"{xp_id}_t{im_idx}_predicted.{ext}"
        predictions[im_idx] = pred_filename
        logger.info(f"Saving predicted intensity image in file '{pred_filename}' ({im_idx})...")
        imsave(out_path.joinpath(pred_filename), pred_img)
        add2json(json_file, out_dataset, predictions)

        # Save the processing pipeline and values
        process = {
            'intensity_image': int_img_fname,
            'time_point': im_idx,
            'channel': channel,
            'model_name': model_name,
            'rescaling': rescaling,
            'tiled': tiled,
            'device': device,
            'prediction_time': f"{round(pred_stop - pred_start, 3)}s",
        }
        add2json(out_path.joinpath("processing.json"), f'{im_idx}', process,
                 required_object_metadata=False, keep_basename=False)  # Keep complete filepath & no need for `object`


def parse_time_index(json_file, time_index, logger, intensity_imgs=None, segmented_imgs=None):
    """Parse the JSON file and cli inputs to define the list of selected time-indexes and of intensity images to work with.

    Parameters
    ----------
    json_file : str
        Path to the JSON file containing information about time points and units.
    time_index : list[int]
        List of manually selected time indexes to include from images dictionary.
    logger : logging.Logger
        A logger instance for logging the process details.
    intensity_imgs : dict[int, str]
        A dictionary of intensity images with time indexes as keys.
    segmented_imgs : dict[int, str]
        A dictionary of segmented images with time indexes as keys.

    Returns
    -------
    list[int]
        The list of selected time indexes.
    dict, optional
        If provided, the dictionary of intensity image, filtered and re-indexed by selected time indexes.
    dict, optional
        If provided, the dictionary of segmented image, filtered and re-indexed by selected time indexes.
    """
    time_md = json_time_metadata(json_file)  # Extract time metadata from a JSON file.
    n_imgs = len(intensity_imgs)  # Get the number of intensity images.
    # Get the list of image indexes from either the intensity of segmented image dictionary
    images_tidx = list(intensity_imgs.keys()) if intensity_imgs is not None else list(segmented_imgs.keys())
    # TODO: should we check that the indexes are consecutive and starting from 0 ?

    if time_index != ():
        # - CLI input case: some time-indexes were given to the CLI
        # Identify any invalid time indices.
        wrong_index = [idx for idx in time_index if idx not in images_tidx]
        if wrong_index != []:
            logger.warning(f"Got some manually defined time-index that do not exists: {wrong_index}")
        # Filter out invalid time indices from the provided list.
        time_index = [idx for idx in time_index if idx in images_tidx]
        logger.info(f"Selected a subset of time-points from JSON: {time_index} (out of {n_imgs} original images).")
    else:
        # - JSON parsing case: try to load metadata from JSON else use all time-indexes
        all_time_points = time_md.get("points", None)  # Extract time points from the metadata.
        sel_time_points = time_md.get("selected", None)  # Get any specifically selected time points.
        t_unit = time_md.get("unit", "?")  # Get the time unit, with a default value if not defined.
        # TODO: raise something if time unit is not properly defined ?

        if sel_time_points is not None:
            # Get the time indices of the selected time points.
            logger.info(f"Got a list of 'selected' time-points from JSON: {sel_time_points}")
            time_index = [all_time_points.index(tp) for tp in sel_time_points]
            logger.info(f"Corresponding time-indexes: {time_index}")
        else:
            # If no specific time points are selected, use all time points.
            sel_time_points = all_time_points
            time_index = images_tidx
        logger.info(f"Corresponding time-intervals (in {t_unit}) from JSON: {time_intervals(sel_time_points)}")

    img_dicts = []
    if intensity_imgs is not None:
        # Re-index the intensity images based on the selected time indices.
        intensity_imgs = {idx: img for idx, img in intensity_imgs.items() if idx in time_index}
        img_dicts.append(intensity_imgs)
    if segmented_imgs is not None:
        # Re-index the segmented images based on the selected time indices.
        segmented_imgs = {idx: img for idx, img in segmented_imgs.items() if idx in time_index}
        img_dicts.append(segmented_imgs)

    if len(img_dicts) != 0:
        return time_index, *img_dicts
    else:
        return time_index


def parse_channel_names(json_file, logger):
    """Parse the JSON file to define the list of channel names, if any.

    Parameters
    ----------
    json_file : str
        Path to the JSON file containing channel names.
    logger : logging.Logger
        A logger instance to log information and errors.

    Returns
    -------
    list[str] or None
        List of channel names if found in the JSON file, otherwise None.
    """
    try:
        ch_names = json_channel_names(json_file)
    except:
        ch_names = None
    else:
        logger.info(f"Found channel names in JSON: {ch_names}.")
    return ch_names


def parse_intensity_imgs(json_file, intensity_dataset, ref_idx, logger):
    """Parse the JSON file and cli inputs to define the dict of intensity images to work with.

    Parameters
    ----------
    json_file : str
        The path to a JSON file that holds information about intensity images.
    intensity_dataset : str
        The name or path of the dataset containing intensity images.
    ref_idx : list[int]
        Indices indicating which images should be used as reference images.
    logger : logging.Logger
        A logger instance used to report information and debug messages.

    Returns
    -------
    dict
        The dict of intensity images to work with.
    """
    # Get the dictionary of intensity images indexed by time-point
    intensity_imgs = json_intensity_image_parsing(json_file, intensity_dataset)
    # Duplicate the reference image index in case only one is given, meaning it is the same for all time-point:
    if len(ref_idx) == 1 and len(intensity_imgs) > 1:
        ref_idx = ref_idx * len(intensity_imgs)  # set the right number of reference image index
    # Check if we have multi-angle image, if yes select the one defined as reference:
    for t_idx, images in intensity_imgs.items():
        if isinstance(images, dict):
            try:
                ref_img = Path(images[ref_idx[t_idx]])
            except IndexError:
                pass
            else:
                logger.info(f"Using image '{ref_img.name}' ({ref_idx}), of the multi-angle set, at time-point {t_idx}!")
                intensity_imgs[t_idx] = ref_img
    # Some logging:
    n_imgs = len(intensity_imgs)
    logger.info(f"Found {n_imgs} intensity images under '{intensity_dataset}'.")
    logger.debug(intensity_imgs)
    return intensity_imgs


if __name__ == '__main__':
    main()
