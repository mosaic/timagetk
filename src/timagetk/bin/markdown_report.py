from pathlib import Path

import timagetk


def _add_css_styles(styles):
    styles = '\n'.join(styles)
    return f"""<head>\n<style>{styles}\n</style>\n</head>"""


#: CSS styling of HTML tables:
HTML_TABLE_STYLE = """
table {
  border-collapse: collapse;
}
th, td {
  border: 1px solid;
  padding: 5px 15px;
}
"""

#: CSS styling of figures and caption:
FIGCAPTION_STYLE = """
figure {
  width: auto;
  border: thin #c0c0c0 solid;
  padding: 5px;
}

.row {
  display: flex;
  flex-wrap: wrap;
  padding: 0 4px;
}

/* Create two equal columns that sits next to each other */
.column {
  flex: 2%;
  padding: 0 4px;
}

.column img {
  margin-top: 8px;
  vertical-align: middle;
}

img {
  max-width: 100%;
  min-width: 200px;
  height: auto;
  margin-left: auto;
  margin-right: auto;
  display: block;
}

figcaption {
  background-color: #6f6f6f;
  color: #fff;
  font: italic smaller sans-serif;
  padding: 3px;
  text-align: center;
}
"""


def include_figure_html(fig_path, caption="", end=""):
    """Return the HTML code to include the given figure(s) and caption if any.

    Parameters
    ----------
    fig_path : str or list[str]
        The (list of) paths to the figures to include.
    caption : str, optional
        The caption to add to the figure(s)

    Returns
    -------
    str
        The corresponding HTML code.

    Examples
    --------
    >>> from timagetk.bin.feature_clustering import include_figure_html
    >>> print(include_figure_html('test.jpg', "This is a test!"))
    >>> print(include_figure_html(['testA.jpg', 'testB.jpg'], "This is a more complex test!"))

    """
    if isinstance(fig_path, (str, Path)):
        return _include_figure_html(fig_path, caption) + end
    else:
        return _include_figures_html(fig_path, caption) + end


def _include_figure_html(fig_path, caption=""):
    """Return the HTML code to include the given figure and caption if any.

    Parameters
    ----------
    fig_path : str
        The path to the figures to include.
    caption : str, optional
        The caption to add to the figure(s)

    Returns
    -------
    str
        The corresponding HTML code.
    """
    # Open the 'figure' tag:
    html = "<figure>"
    html += "\n"
    html += f"""  <img src="{fig_path}">\n"""
    # Add a caption, if any:
    if caption != "":
        html += f"  <figcaption>{caption}</figcaption>"
        html += "\n"
    # Close the 'figure' tag:
    html += "</figure>"
    return html


def _include_figures_html(fig_path, caption=""):
    """Return the HTML code to include the given figures and caption if any.

    Parameters
    ----------
    fig_path : list[str]
        The list of paths to the figures to include.
    caption : str, optional
        The caption to add to the figure(s)

    Returns
    -------
    str
        The corresponding HTML code.
    """
    if isinstance(fig_path, str):
        fig_path = [fig_path]
    # Open the 'figure' tag:
    html = "<figure>"
    html += "\n"

    html += """  <div class="row">\n"""
    # Add all listed figure path:
    for f_path in fig_path:
        html += """    <div class="column">\n"""
        html += f"""      <img src="{f_path}">\n"""
        html += "    </div>\n"
    html += "  </div>\n"
    # Add a caption, if any:
    if caption != "":
        html += f"  <figcaption>{caption}</figcaption>"
        html += "\n"
    # Close the 'figure' tag:
    html += "</figure>"
    return html
