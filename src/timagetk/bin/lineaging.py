#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import argparse
from os.path import join
from os.path import splitext
from pathlib import Path

import numpy as np
from timagetk.third_party.ctrl.io import AVAILABLE_LINEAGE_FMT
from timagetk.third_party.ctrl.io import DEFAULT_LINEAGE_FMT
from timagetk.third_party.ctrl.io import read_lineage
from timagetk.third_party.ctrl.io import write_lineage
from timagetk.third_party.ctrl.lineage import Lineage
from timagetk.third_party.ctrl.tracking import CT_METHODS
from timagetk.third_party.ctrl.tracking import CellTracking
from timagetk.third_party.ctrl.tracking import DEF_TRSF_DIRECTION
from timagetk.third_party.ctrl.tracking import IterativeCellTracking
from timagetk.third_party.ctrl.tracking import TRSF_DIRECTIONS

from timagetk import TissueImage3D
from timagetk.algorithms.pointmatching import pointmatching
from timagetk.algorithms.trsf import apply_trsf
from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.components.labelled_image import labels_at_stack_margins
from timagetk.components.multi_channel import combine_channels
from timagetk.io import imread
from timagetk.io import read_trsf
from timagetk.tasks.json_parser import DEFAULT_INTENSITY_DATASET
from timagetk.tasks.json_parser import DEFAULT_LINEAGE_DATASET
from timagetk.tasks.json_parser import DEFAULT_LINEAGE_LANDMARKS_DATASET as LANDMARKS_DATASET
from timagetk.tasks.json_parser import DEFAULT_SEGMENTATION_DATASET
from timagetk.tasks.json_parser import DEFAULT_TRSF_DATASET
from timagetk.tasks.json_parser import add2json
from timagetk.tasks.json_parser import json_intensity_image_parsing
from timagetk.tasks.json_parser import json_landmarks_parsing
from timagetk.tasks.json_parser import json_lineage_parsing
from timagetk.tasks.json_parser import json_segmentation_parsing
from timagetk.tasks.json_parser import json_series_metadata
from timagetk.tasks.json_parser import json_trsf_parsing
from timagetk.visu.stack import orthogonal_view

OUT_DATASET = DEFAULT_LINEAGE_DATASET
DEFAULT_LINEAGE_METHOD = "naive"

DESCRIPTION = """Cell lineaging procedure.

"""
cli_name = Path(__file__).stem


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION)

    parser.add_argument('json', type=str,
                        help="a JSON file containing the metadata and locations of the required dataset.")
    parser.add_argument('-m', '--method', type=str, default=DEFAULT_LINEAGE_METHOD, choices=[CT_METHODS]+['iterative'],
                        help=f"the method to use to compute the lineage, '{DEFAULT_LINEAGE_METHOD}' by default.")
    parser.add_argument('-t', '--time_index', type=int, nargs="+", default=[],
                        help="the time-index of the images to use with this procedure, all of them by default.")
    parser.add_argument('-int', '--intensity_dataset', type=str, default=DEFAULT_INTENSITY_DATASET,
                        help=f"the dataset to use as intensity image input for the lineage algorithm, '{DEFAULT_INTENSITY_DATASET}' by default.")
    parser.add_argument('-seg', '--segmentation_dataset', type=str, default=DEFAULT_SEGMENTATION_DATASET,
                        help=f"the dataset to use as segmented image input for the lineage algorithm, '{DEFAULT_SEGMENTATION_DATASET}' by default.")
    parser.add_argument('-lin', '--lineage_dataset', type=str, default=DEFAULT_LINEAGE_DATASET,
                        help=f"the dataset to use as segmented image input for the lineage algorithm, '{DEFAULT_LINEAGE_DATASET}' by default.")
    parser.add_argument('-fmt', '--lineage_format', type=str, default=DEFAULT_LINEAGE_FMT,
                        choices=AVAILABLE_LINEAGE_FMT,
                        help=f"the format used to read the lineage file, '{DEFAULT_LINEAGE_FMT}' by default.")
    parser.add_argument('-trsf', '--trsf_dataset', type=str, default=DEFAULT_TRSF_DATASET,
                        help=f"name of the dataset containing the transformations, '{DEFAULT_TRSF_DATASET}' by default.")

    init_arg = parser.add_mutually_exclusive_group()
    init_arg.add_argument('--manual_init', action="store_true",
                          help="add this to use manual definition of landmarks, which can be done using `lineage_initialisation.py`.")
    init_arg.add_argument('--lineage_init', action="store_true",
                          help="add this to use previous lineage definition as initialisation method.")

    lin_arg = parser.add_argument_group('lineaging arguments')
    lin_arg.add_argument('--direction', type=str, default=DEF_TRSF_DIRECTION, choices=TRSF_DIRECTIONS,
                         help="defines the temporal direction of initial transformation and estimated registration.\
                              'backward' lineaging (default), resample daughter image into mother reference frame.\
                              'forward' lineaging, resample mother image into daughter reference frame.")

    out_arg = parser.add_argument_group('output arguments')
    out_arg.add_argument('-o', '--out_dataset', type=str, default=OUT_DATASET,
                         help=f"the dataset name to use to save lineage files, '{OUT_DATASET}' by default.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default")

    return parser


def main(args):
    # Get the name of the series
    series_md = json_series_metadata(args.json)
    xp_id = series_md['series_name']
    # Defines some location variables:
    args.root_path = Path(args.json).parent.absolute()  # the root directory is where the JSON file is...
    args.out_path = args.root_path.joinpath(args.out_dataset)  # but needs absolute path to save files
    args.out_path.mkdir(exist_ok=True)  # make sure the directory exists

    # Initialize logger
    logger = get_logger(cli_name, args.root_path.joinpath(f'{cli_name}.log'), args.log_level.upper())
    # Log first welcome message
    logger.info(f"Lineage estimation for time-series '{xp_id}' located under '{args.root_path}'.")

    # Get the dictionary of intensity images indexed by time-point
    int_imgs = json_intensity_image_parsing(args.json, args.intensity_dataset)
    logger.info(f"Found the following intensity images: {int_imgs}")
    # Get the dictionary of segmented images indexed by time-point
    seg_imgs = json_segmentation_parsing(args.json, args.segmentation_dataset)
    logger.info(f"Found the following segmented images: {seg_imgs}")

    # Check the consistency of number of intensity and segmented images:
    try:
        assert len(int_imgs) == len(seg_imgs)
    except AssertionError:
        logger.critical("There should be the same number of intensity and segmented images!")
        logger.info(f"Found {len(int_imgs)} intensity images!")
        logger.info(f"Found {len(seg_imgs)} segmented images!")
        exit(1)

    n_imgs = len(int_imgs)

    ldmks = {}
    if args.manual_init:
        # Try to load existing landmarks
        try:
            ldmk_json = json_landmarks_parsing(args.json, LANDMARKS_DATASET)
        except KeyError:
            logger.error(f"Could not find `{LANDMARKS_DATASET}` entry in JSON file '{args.json}'!")
            exit(1)
        else:
            logger.info("Found existing landmarks files:")
            logger.debug(ldmk_json)
        # Now load the landmarks
        for (t_ref, t_float), _files in ldmk_json.items():
            try:
                mother_pts = [np.loadtxt(jf) for jf in ldmk_json[(t_ref, t_float)] if 'reference' in jf][0]
                daughter_pts = [np.loadtxt(jf) for jf in ldmk_json[(t_ref, t_float)] if 'floating' in jf][0]
                assert len(mother_pts) == len(daughter_pts)  # we need the same number of points!
                ldmks[(t_ref, t_float)] = {'mother': mother_pts, 'daughter': daughter_pts}
            except KeyError:
                pass  # Undefined yet, no need to explicit the caught error here...
            except IOError as e:
                logger.critical(e)  # print the caught error as there might be some files missing...
                import sys
                sys.exit("Error while loading files!")
            else:
                n_pts = len(mother_pts)
                logger.info(f"Loaded reference (t{t_ref}) and floating (t{t_float}) landmarks (n={n_pts})")

    lineage_files = {}
    if args.lineage_init:
        lineage_files = json_lineage_parsing(args.json, args.lineage_dataset)
        logger.info(f"Found {len(lineage_files)} lineage files under '{args.lineage_dataset}'.")
        logger.debug(lineage_files)

    try:
        trsf_json = json_trsf_parsing(args.json, args.trsf_dataset)
        #TODO: modify the trsf database in order to avoid this ugly following line
        trsfs = list(trsf_json.values())[0]

        #trsfs = trsf_json['rigid']
        n_trsfs = len(trsfs)
    except:
        logger.error(f"Could not find 'rigid' temporal transformations for time series {xp_id}!")
        trsfs = None
    else:
        logger.info(f"Found {n_trsfs} 'rigid' transformations under '{args.trsf_dataset}'.")
        logger.debug(trsfs)
        try:
            assert n_trsfs + 1 == n_imgs
        except AssertionError:
            logger.error(f"Found {n_trsfs} temporal transformations for a time series of {n_imgs} images!")

    lineages = {}
    logger.info(f"Starting cell lineage estimation task in '{args.direction}' direction...")
    if args.time_index == []:
        args.time_index = list(range(n_imgs))
    else:
        args.time_index = list(map(int, args.time_index))

    for ti, mother_int_img in int_imgs.items():
        if ti not in args.time_index:
            logger.info(f"Skipping time-index {ti}")
            continue
        if ti + 1 not in int_imgs:
            break  # stop the loop at 'ti-1'

        # Load the reference intensity image (t_n)
        logger.info(f"Loading reference intensity image #{ti}...")
        mother_int_img = imread(mother_int_img)
        # Load the floating intensity image (t_n+1)
        logger.info(f"Loading floating intensity image #{ti + 1}...")
        daughter_int_img = imread(int_imgs[ti + 1])

        logger.info(f"Loading reference segmented image #{ti}...")
        mother_seg_img = TissueImage3D(imread(seg_imgs[ti]), not_a_label=0, background=1)
        logger.info(f"Loading floating segmented image #{ti + 1}...")
        daughter_seg_img = TissueImage3D(imread(seg_imgs[ti + 1]), not_a_label=0, background=1)

        init_trsf = None
        previous_lineage = {}
        if args.manual_init:
            # If a manual initialisation is required, compute the affine trsf and use it as init trsf for registration
            logger.info("Computing affine transformation from manual landmarks definition...")
            if args.direction == 'backward':
                init_trsf = pointmatching(points_flo=ldmks[(ti, ti + 1)]['daughter'],
                                          points_ref=ldmks[(ti, ti + 1)]['mother'],
                                          template_img=daughter_int_img, method='affine')
            else:
                init_trsf = pointmatching(points_flo=ldmks[(ti, ti + 1)]['mother'],
                                          points_ref=ldmks[(ti, ti + 1)]['daughter'],
                                          template_img=mother_int_img, method='affine')
        else:
            # Else, try to load t_n -> t_n+1 trsf
            if trsfs is not None:
                try:
                    if args.direction == 'forward':
                        init_trsf = read_trsf(trsfs[(ti, ti + 1)])
                    elif args.direction == 'backward':
                        init_trsf = read_trsf(trsfs[(ti + 1, ti)])
                        trsfs[(ti, ti + 1)] = trsfs[(ti + 1, ti)] # create new entry (for process)

                except:
                    logger.warning(f"Could not find 'rigid' transformation for {ti}->{ti + 1} interval!")
                else:
                    logger.info(f"Found 'rigid' transformation for {ti}->{ti + 1} interval.")
                    #if args.direction == 'backward':
                        # Need to reverse the Trsf as it is a forward transfo (t_n->t_n+1) and we use backward lineaging
                        #logger.info(f"Inverting transformation to obtain {ti + 1}->{ti} initial transformation.")
                        #
                        #init_trsf = inv_trsf(init_trsf, template_img=mother_int_img)

        if lineage_files:
            try:
                previous_lineage = read_lineage(lineage_files[(ti, ti + 1)], fmt=args.lineage_format)
            except:
                logger.error(f"Could not read lineage file with '{args.lineage_format}' formatting!")
                try:
                    idx = AVAILABLE_LINEAGE_FMT.index(args.lineage_format)
                    from copy import copy
                    lin_formats = copy(AVAILABLE_LINEAGE_FMT)
                    lin_formats.pop(idx)
                    logger.info(f"Try to read lineage file with '{lin_formats[0]}' formatting!")
                    previous_lineage = read_lineage(lineage_files[(ti, ti + 1)], fmt=lin_formats[0])
                except:
                    logger.error(f"Could not read lineage file with '{lin_formats[0]}' formatting!")
                    previous_lineage = {}

        logger.info(f"Computing cell lineage between t{ti} and t{ti + 1}...")

        if args.method == 'iterative':
            logger.info("Initialize IterativeCellTracking instance...")
            tracking = IterativeCellTracking(mother_img=mother_int_img, daughter_img=daughter_int_img,
                                             mother_seg=mother_seg_img, daughter_seg=daughter_seg_img,
                                             init_trsf=init_trsf, trsf_direction=args.direction,
                                             init_lineage_dict=previous_lineage, max_iter=5, pts_highest_level=3)
            lineage = tracking.iterative_naive_tracking()
        else:
            logger.info("Initialize CellTracking instance...")
            tracking = CellTracking(mother_img=mother_int_img, daughter_img=daughter_int_img,
                                    mother_seg=mother_seg_img, daughter_seg=daughter_seg_img,
                                    init_trsf=init_trsf, trsf_direction=args.direction,
                                    init_lineage_dict=previous_lineage)
            lineage = tracking.naive_lineage()

        # Save the process
        # TODO: add2json need to be refactored in order to 1) allow to write other json file than the main one
        # and 2) allow the complete filepath to be stored (only basename is saved right now)
        # A first try has been done, need to be checked
        lin_path = lineage_files[(ti, ti + 1)] if lineage_files else None
        init_trsf_path =  [(ti, ti + 1)] if trsfs is not None else None
        init_ldmks_path = ldmk_json[(ti, ti + 1)] if ldmks else None

        process_lin = {"floating_intensity_image": int_imgs[ti],
                        "reference_intensity_image": int_imgs[ti + 1],
                        "floating_segmented_image": seg_imgs[ti],
                        "reference_segmented_image": seg_imgs[ti + 1],
                        "method": args.method,
                        "direction": args.direction,
                        "init_lineage": lin_path,
                        "init_trsf": init_trsf_path,
                        "init_landmarks": init_ldmks_path}
        add2json(join(args.out_path, "processing.json"), f"{ti}/{ti + 1}", process_lin,
                 required_object_metadata=False, keep_basename=False)  # Keep complete filepath & no need for `object`

        logger.info("Saving cell lineage dictionary to file...")
        lin_file = f"{xp_id}_t{ti}-{ti + 1}_lineage.txt"
        lineage = Lineage(lineage)

        # Remove lineage for cells located at the image stack margins:
        mother_margin_labels = labels_at_stack_margins(mother_seg_img, voxel_distance_from_margin=5,
                                                       labels_to_exclude=[mother_seg_img.background])
        daughter_margin_labels = labels_at_stack_margins(daughter_seg_img, voxel_distance_from_margin=5,
                                                         labels_to_exclude=[daughter_seg_img.background])
        if mother_margin_labels != []:
            lineage.remove_lineage(mother_margin_labels, quiet=True)
        if daughter_margin_labels != []:
            lineage.remove_lineage(descendant=daughter_margin_labels, quiet=True)

        # Save the lineage in a file:
        write_lineage(lineage, join(args.out_path, lin_file))
        lineages[(ti, ti + 1)] = lin_file
        logger.info("Updating JSON metadata file...")
        add2json(args.json, args.out_dataset, lineages)

        base, _ = splitext(lin_file)
        trsf = tracking.trsf
        if trsf.is_linear():
            if args.direction == 'backward':
                reg_float = apply_trsf(daughter_int_img, trsf, template_img=mother_int_img)
            else:
                reg_float = apply_trsf(mother_int_img, trsf, template_img=daughter_int_img)
        else:
            if args.direction == 'backward':
                reg_float = apply_trsf(daughter_int_img, trsf)
            else:
                reg_float = apply_trsf(mother_int_img, trsf)

        if args.direction == 'backward':
            blend = combine_channels([mother_int_img, reg_float])
        else:
            blend = combine_channels([daughter_int_img, reg_float])
        orthogonal_view(blend, suptitle=base, figname=join(args.out_path, base + '.png'), figsize=(15, 15))


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
