#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Meta analysis of independent clustering across multiple time-series."""

import argparse
import copy
import logging
import sys
from pathlib import Path
from shutil import rmtree

import pandas as pd
from visu_core.matplotlib.colormap import multicolor_categorical_colormap

from timagetk.algorithms.cluster_matching import match_cluster_from_cost
from timagetk.algorithms.cluster_matching import wasserstein_distance_matrix_from_properties
from timagetk.bin.feature_clustering import make_clustering
from timagetk.bin.feature_clustering import trsf_temporal_resampling
from timagetk.bin.logger import DEFAULT_LOG_LEVEL
from timagetk.bin.logger import LOG_LEVELS
from timagetk.bin.logger import get_logger
from timagetk.components.clustering import FeatureClusterer
from timagetk.io.graph import from_csv
from timagetk.tasks.json_parser import DEFAULT_SEGMENTATION_DATASET
from timagetk.tasks.json_parser import DEFAULT_TRSF_DATASET
from timagetk.tasks.json_parser import json_clustering_parsing
from timagetk.tasks.json_parser import json_csv_parsing
from timagetk.tasks.json_parser import json_orientation_metadata
from timagetk.tasks.json_parser import json_segmentation_parsing
from timagetk.tasks.json_parser import json_time_metadata
from timagetk.tasks.json_parser import json_trsf_parsing
from timagetk.tasks.json_parser import time_intervals

OUT_DATASET = "meta_clustering"

DESCRIPTION = """Meta analysis of independent clustering across multiple time-series.

"""
cli_name = Path(__file__).stem


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(description=DESCRIPTION)

    parser.add_argument('toml', type=str,
                        help="a TOML configuration file with a 'meta_clustering' entry.")
    parser.add_argument('meta_clustering', type=str, default="meta_clustering", nargs="?",
                        help="the entry in TOML config defining the meta clustering to use.")

    out_arg = parser.add_argument_group('output arguments')
    out_arg.add_argument('-o', '--out_dataset', type=str, default=OUT_DATASET,
                         help=f"the name of the dataset to use to save lineage files, '{OUT_DATASET}' by default.")
    out_arg.add_argument('--force', action="store_true",
                         help="use this to clear any existing previous output dataset.")

    log_arg = parser.add_argument_group('logging arguments')
    log_arg.add_argument('--log_level', type=str, default=DEFAULT_LOG_LEVEL, choices=LOG_LEVELS,
                         help=f"logging level to use, '{DEFAULT_LOG_LEVEL}' by default.")

    return parser


def parse_toml(toml_file, logger) -> dict:
    """Parse the TOML configuration file."""
    import toml
    logger.info(f"Using provided TOML configuration file: '{toml_file}'")
    # Check the TOML file indeed contain 'clustering' configurations
    config = toml.load(toml_file)
    if 'meta_clustering' not in config:
        logger.critical("Could not find 'meta_clustering' configurations in provided TOML!")
        sys.exit("Wrong TOML configuration file!")

    return config


def find_time_series_json(root_path, time_series, **kwargs):
    """Find the JSON associated to each time-series.

    Parameters
    ----------
    root_path : pathlib.Path
        The path to the root folder containing all the time-series as sub-folders.
    time_series : list[str]
        A list of time-series to find, should match sub-folder names.

    Returns
    -------
    list[pathlib.Path]
        The list of path to the JSON files associated to the `time_series`.

    Examples
    --------
    >>> from pathlib import Path
    >>> from timagetk.bin.meta_clustering import find_time_series_json
    >>> root_path = Path("/data/FPS_paper/Old_Data")
    >>> time_series = ['p58', 'p60', 'p194']
    >>> find_time_series_json(root_path, time_series)

    """
    from timagetk.bin.logger import get_dummy_logger
    logger = kwargs.get('logger', get_dummy_logger(sys._getframe().f_code.co_name))

    subdirs = root_path.iterdir()  # list all subdirectories in this root folder
    subdir_stems = {d.stem: d for d in subdirs}

    # Get the directory for each time-series:
    time_series_path = {}
    for ts in time_series:
        # Check it exists:
        if ts not in subdir_stems:
            logger.error(f"Could not find directory associated to selected time-series '{ts}'!")
        else:
            time_series_path[ts] = subdir_stems[ts]

    # Check that there is a JSON file in these directories:
    time_series_json = {}  # dict of JSON path indexed by time-series name
    for ts, path in time_series_path.items():
        json_path = path.joinpath(ts + '.json')
        if not json_path.exists():
            logger.error(f"Could not find JSON file associated to time-series '{ts}'!")
        else:
            time_series_json[ts] = json_path

    if len(time_series_json) == len(time_series):
        logger.info("Found all directories and JSON files for selected time-series!")

    return time_series_json


def cluster_matching_report(cost_df, q_match, out_path, ref_ts, time_series, clustering_cfg, clust_by_ts, **kwargs):
    """Generates the markdown (& html) report(s) for cluster matching.

    Parameters
    ----------
    cost_df : dict
        A dictionary of pairing cost DataFrames indexed by time-series name.
    q_match : dict
        A dictionary of cluster pairing indexed by time-series name.
    out_path : patlib.Path
        The path to the report(s) directory.
    ref_ts : str
        The name of the reference time-series.
    time_series : list[str]
        The list of time-series matched to the reference.
    clustering_cfg : dict
        The clustering configurations indexed by clustering id.
    clust_by_ts : dict
        The dictionary of clustering id and names indexed by time-series name.
    html : bool
        If ``True`` convert the markdown report to an HTML version.

    """
    ref_clust_id, ref_clust_name = clust_by_ts[ref_ts]
    ref_clust_cfg = clustering_cfg[ref_clust_id]

    # -- Generates markdown report for cluster matching:
    report_fame = "cluster_matching_report.{}"
    md_report_fame = out_path.joinpath(report_fame.format('md'))
    md = ""
    md += "# Cluster matching\n\n"
    md += f"Reference time series is `{ref_ts}`.\n\n"
    md += f"Reference clustering is named `{ref_clust_name}` (id=`{ref_clust_id}`).\n"
    md += "Associated configuration is:\n\n - "
    md += "\n - ".join([f"**{k}**: {v}" for k, v in ref_clust_cfg.items()])
    md += "\n\n"
    for ts in set(time_series) - {ref_ts}:
        md += f"## {ref_ts}-{ts} matching\n\n"
        md += "Associated pairing costs:\n\n"
        md += cost_df[ts].to_markdown() + '\n\n'
        md += "Matched clusters (ref, test): " + ", ".join([f"{r}:{t}" for r, t in q_match[ts]])
        md += "\n\n"
    md += "\n\n"

    # - Write markdown report to file:
    with open(md_report_fame, 'w') as f:
        f.write(md)

    # - Export as HTML if required:
    if kwargs.get("html", False):
        import markdown
        from timagetk.bin.markdown_report import HTML_TABLE_STYLE
        html_report_fame = out_path.joinpath(report_fame.format('html'))
        html = ""
        # HTML header:
        html += "<!DOCTYPE html>\n<html>\n"
        # Table styling:
        html += HTML_TABLE_STYLE
        # HTML body:
        html += "<body>\n"
        html += markdown.markdown(md, extensions=['tables'])
        # HTML end of body:
        html += "\n</body>\n</html>"
        # Export HTML file:
        with open(html_report_fame, 'w') as f:
            f.write(html)

    return


def make_figures_and_report(time_series, time_series_json, ttg, config, clust_by_ts, out_path, cmap, ref_ts, **kwargs):
    """Generates the markdown (& html) report(s) for clustering, with matching colors and cluster id.

    Parameters
    ----------
    time_series : str
        Name of the time-series matched against the reference.
    time_series_json : dict
        Time-series indexed dictionary of JSON files location.
    ttg : timagetk.graphs.temporal_graph.TemporalTissueGraph
        The temporal tissue graph to use.
    config : dict
        The configuration dictionary, loaded from TOML.
    clust_by_ts : dict
        Time-series indexed dictionary of clustering id and full name (as a tuple).
    out_path : pathlib.Path
        Common output directory of the meta_analysis.
    cmap : dict
        Cluster indexed dictionary of RGBA colors.

    Other Parameters
    ----------------
    logger : logging.Logger
        A logger
    relabelling : dict
        Clustering relabelling dictionary as {old_id: new_id}

    """
    from timagetk.bin.logger import get_dummy_logger
    logger = kwargs.get('logger', get_dummy_logger(sys._getframe().f_code.co_name))

    time_md = json_time_metadata(time_series_json[time_series])

    meta_cfg = config['meta_clustering']
    # ref_clust_id, ref_clust_name = clust_by_ts[ref_ts]
    # n_clusters = config['clustering'][ref_clust_id]['n_clusters']
    clust_id, clust_name = clust_by_ts[time_series]
    outliers_cluster_id = config['clustering']['outliers_cluster_id']

    from timagetk.bin.feature_clustering import clustering_figures
    from timagetk.bin.feature_clustering import clustering_report
    seg_dataset = meta_cfg[time_series].get('segmentation_dataset', DEFAULT_SEGMENTATION_DATASET)
    trsf_dataset = meta_cfg[time_series].get('trsf_dataset', DEFAULT_TRSF_DATASET)

    # -- Get the dictionary of segmented images indexed by time-point
    seg_imgs = json_segmentation_parsing(time_series_json[time_series], seg_dataset)
    logger.info(f"Found {len(seg_imgs)} segmented images under '{seg_dataset}'.")
    logger.debug(seg_imgs)
    n_imgs = len(seg_imgs)

    # -- Get the dictionary of intensity images indexed by time intervals (len-2 tuples)
    trsf_json = json_trsf_parsing(time_series_json[time_series], trsf_dataset)
    # Check the consistency of number of intensity images and transformations:
    required_regs = [(t, t + 1) for t in range(n_imgs - 1)]
    trsf_type = "rigid"
    trsfs = trsf_json[trsf_type]
    try:
        assert [reg in trsfs for reg in required_regs]
    except AssertionError:
        logger.critical(f"Could not find all the required consecutive {trsf_type} transformations!")
        logger.critical(
            f"Missing consecutive {trsf_type} transformations for intervals: {[reg not in trsfs for reg in required_regs]}")
        logger.info("Run `consecutive_registration.py` prior to using `sequence_registration.py`!")
        exit(1)
    # Now we can define the time-interval indexed dictionary of transformations:
    trsfs = trsf_json[trsf_type]
    logger.info(f"Found the required {trsf_type} transformations under '{trsf_dataset}'.")
    logger.debug(trsfs)

    # -- TIME INDEXES - Try to get the 'selected' time-points from the JSON metadata:
    all_time_points = time_md.get("points", None)
    sel_time_points = time_md.get("selected", None)
    t_unit = time_md.get("unit", "?")
    if sel_time_points is not None:
        logger.info(f"Got a list of 'selected' time-points from JSON: {sel_time_points}")
        time_index = [all_time_points.index(tp) for tp in sel_time_points]
        logger.info(f"Corresponding time-indexes: {time_index}")
        # Select corresponding segmented images and re-index segmentation dictionary with contiguous time-indexes:
        seg_imgs = {t: seg_imgs[ti] for t, ti in enumerate(time_index)}
        # Select & compose corresponding registration trsfs and relabel with contiguous time-indexes:
        trsfs = trsf_temporal_resampling(trsfs, time_index)
    else:
        sel_time_points = all_time_points
        time_index = list(range(0, len(sel_time_points)))
    logger.info(f"Corresponding time-intervals (in {t_unit}) from JSON: {time_intervals(sel_time_points)}")

    # -- Try to get the image/microscope 'orientation' from the JSON metadata:
    orientation = json_orientation_metadata(time_series_json[time_series], 1)

    # -- Create an output directory for the time-series:
    out_path = out_path.joinpath(time_series)
    out_path.mkdir(exist_ok=True)  # make sure it exists

    cfg = config['clustering'][clust_id]
    # FIXME: unfortunately we have to recreate the `FeatureClusterer` instance to be able to generate figures & reports.
    graph_clustering = copy.deepcopy(
        ttg.cell_property_dict(clust_name, default=outliers_cluster_id, only_defined=False))
    clusterer = FeatureClusterer(ttg, cfg['standardisation'])
    # -- Performs the requested clustering:
    clusterer = make_clustering(clusterer, cfg['method'], cfg['n_clusters'], cfg['features'],
                                cfg['weights'], cfg['standardisation'], cfg['delete_outliers'],
                                identities=cfg.get('identities', []),
                                exclude_identities=cfg.get('exclude_identities', []),
                                logger=logger)

    # Fixme: that implies to recompute the clustering, so we check they are the same!
    clust_clustering = copy.deepcopy(clusterer._clustering)

    try:
        assert all(clust_clustering[i] == q_i for i, q_i in graph_clustering.items() if q_i != outliers_cluster_id)
    except AssertionError:
        logger.critical("Clustering are different!!!")
        logger.info("Change clustering to match the one from the graph...")
        for i, q_i in clust_clustering.item():
            clusterer._clustering[i] = graph_clustering[i]

    # -- Now we can relabel the cluster ids based on relabelling dict, if any:
    relabelling = kwargs.get('relabelling', None)
    if relabelling is not None:
        logger.info(f"Got a clusters relabelling dictionary: {relabelling}")
        # Get a copy of the clustering from the temporal tissue graph:
        graph_clustering = copy.deepcopy(
            ttg.cell_property_dict(clust_name, default=outliers_cluster_id, only_defined=True))
        # Apply the clusters relabelling to copy:
        for tcid, q_id in graph_clustering.items():
            graph_clustering[tcid] = relabelling[q_id]
        # Replace in the temporal tissue graph:
        ttg.add_cell_property(clust_name, graph_clustering)

    # VTK colormap is a list:
    lin_cmap = [cmap[q] for q in sorted(cmap.keys())]
    vtk_cmap = multicolor_categorical_colormap(lin_cmap, "clustering")

    # -- Clustering figures:
    logger.info(f"Generating clustering figures for time-series '{time_series}'...")
    proj_fnames = clustering_figures(ttg, clust_name, seg_imgs, meta_cfg['layers'], out_path,
                                     trsfs, idx=clust_id, orientation=orientation, colormap=vtk_cmap,
                                     outliers_cluster_id=outliers_cluster_id, logger=logger)

    # -- Clustering report:
    logger.info("Generating clustering report...")
    report_fame, cdist_fname, edist_fname, bxp_fname = clustering_report(clusterer, time_series, out_path, proj_fnames,
                                                                         colormap=cmap, idx=clust_id,
                                                                         html=kwargs.get('html', True))


def main(args):
    # Defines some location variables:
    args.root_path = Path(args.toml).parent.absolute()  # the root directory is where the TOML config file is!

    # Initialize logger
    logger = get_logger(cli_name, args.root_path.joinpath(f'{cli_name}.log'), args.log_level.upper())

    # Parse the config file:
    config = parse_toml(args.toml, logger)
    logger.info(f"Found {len(config)} entries in TOML config files: {sorted(config.keys())}")
    meta_name = args.meta_clustering
    meta_cfg = config[meta_name]
    # Set new attributes from this config:
    args.out_dataset = meta_cfg.get('out_dataset',
                                    meta_name)  # name of the output dataset, defaults to `args.meta_clustering`
    args.time_series = meta_cfg['time_series']  # list of time-series to use for cluster matching
    args.reference = meta_cfg.get('reference', args.time_series[0])  # reference time-series, defaults to the first one
    args.html = meta_cfg.get("html", False)
    args.colormap = meta_cfg.get("colormap", 'viridis')

    # -- Check if output directory exists & has files:
    args.out_path = args.root_path.joinpath(args.out_dataset)  # full path where to output data
    if args.out_path.exists():
        empty_dir = len(list(args.out_path.iterdir())) == 0
        if not empty_dir:
            if args.force:
                # Clear folder:
                rmtree(args.out_path)
                while not args.out_path.exists():
                    args.out_path.mkdir()
                logger.info("Cleared any existing meta clustering in out dataset directory.")
            else:
                logger.warning("Non empty output directory for meta clustering...")
                # Loop to find an empty directory to store the outputs
                n = 1
                out_path = args.root_path.joinpath(args.out_dataset + f"_{n}")
                empty_dir = len(list(out_path.iterdir())) == 0
                while out_path.exists() and not empty_dir:
                    n += 1
                    out_path = args.root_path.joinpath(args.out_dataset + f"_{n}")
                    empty_dir = len(list(out_path.iterdir())) == 0
                args.out_path = out_path
                args.out_path.mkdir(exist_ok=True)  # make sure it exists
                logger.info(f"Changed the output directory to '{args.out_path}'!")
    else:
        args.out_path.mkdir(exist_ok=True)  # make sure it exists

    # Log first welcome message
    logger.info(f"Meta analysis of clustering for time-series: {', '.join(args.time_series)}.")

    # -- Get the JSON files for selected time-series:
    time_series_json = find_time_series_json(args.root_path, args.time_series)

    # -- Make sure we can find the selected clustering "id" in the JSON files:
    clust_by_ts = {}  # {ts: (clustering id, clustering name)}
    for ts, json_path in time_series_json.items():
        clustering_dict = json_clustering_parsing(json_path, meta_cfg[ts]['clustering_dataset'])
        logger.info(f"Found {len(clustering_dict)} clustering in '{ts}' JSON.")
        logger.debug(clustering_dict)
        clust_id_by_names = {k[1]: k[0] for k in clustering_dict.keys()}
        known_clusterings = list(clust_id_by_names.keys())
        clustering_name = meta_cfg[ts]['clustering_name']
        try:
            assert clustering_name in known_clusterings
        except AssertionError:
            logger.critical(f"Could not find clustering '{clustering_name}' in {ts} JSON!")
            exit(1)
        else:
            logger.info(f"Found requested clustering '{clustering_name}'.")
        clust_by_ts[ts] = (clust_id_by_names[clustering_name], clustering_name)

    # - Get the reference clustering id and full name:
    logger.info(f"Reference time-series for clusters pairing: {args.reference}!")
    ref_clust_id, ref_clust_name = clust_by_ts[args.reference]
    # - Get the reference clustering configuration:
    ref_clust_cfg = config['clustering'][ref_clust_id]
    logger.info(f"Reference clustering config: {ref_clust_cfg}")

    # Get the graph CSV files for the reference time-series:
    ref_csv_files = json_csv_parsing(time_series_json[args.reference], meta_cfg[args.reference]['clustering_dataset'])
    # Build the (Temporal)TissueGraph from the CSV files for the reference time-series:
    logger.info(
        f"Re-build the (Temporal)TissueGraph from the CSV files for the reference time-series '{args.reference}'...")
    ref_ttg = from_csv(ref_csv_files)

    outliers_cluster_id = config['clustering']['outliers_cluster_id']
    ref_clustering = ref_ttg.cell_property_dict(ref_clust_name, default=outliers_cluster_id, only_defined=False)

    n_colors = ref_clust_cfg['n_clusters']  # number of colors is the number of clusters
    cluster_ids = set(ref_clustering.values()) - {outliers_cluster_id}

    # -- Check selected clustering for other time-series against reference:
    for ts in args.time_series:
        if ts == args.reference:
            continue  # skip the reference time-series!
        # Get the clustering id and full name for this time-series:
        clust_id, clust_name = clust_by_ts[ts]
        # Get the clustering configuration for this time-series:
        clust_cfg = config['clustering'][clust_id]
        try:
            assert ref_clust_cfg['n_clusters'] >= clust_cfg['n_clusters']
        except AssertionError:
            logger.warning(
                f"Reference clustering {ref_clust_id} has less clusters than clustering {clust_id} for time-series {ts}!")
            n_colors = clust_cfg['n_clusters']
            cluster_ids = set(range(1, n_colors + 1)) - {outliers_cluster_id}

    # -- Create the colormap dictionary indexed by cluster id:
    from timagetk.visu.util import colors_array
    colors = colors_array(args.colormap, n_colors)
    colors_by_q = {cluster_id: colors[n] for n, cluster_id in enumerate(cluster_ids)}

    # -- Make the clustering figures and report for the reference time-series:
    # We have to redo it as the colors might have changed if another clustering has more clusters!
    make_figures_and_report(args.reference, time_series_json, ref_ttg, config, clust_by_ts, args.out_path, colors_by_q,
                            args.reference, logger=logger)

    cost_df = {}
    q_match = {}
    for ts in args.time_series:
        if ts == args.reference:
            continue  # skip the reference time-series!
        # Get the graph CSV files for the current time-series:
        csv_files = json_csv_parsing(time_series_json[ts], meta_cfg[ts]['clustering_dataset'])
        # Build the (Temporal)TissueGraph from the CSV files for the current time-series:
        logger.info(f"Re-build the (Temporal)TissueGraph from the CSV files for time-series '{ts}'...")
        ttg = from_csv(csv_files)
        # Get the clustering config for this time-series:
        clust_id, clust_name = clust_by_ts[ts]
        clust_cfg = config['clustering'][clust_id]

        try:
            assert ref_clust_cfg['features'] == clust_cfg['features']
        except AssertionError:
            logger.error(f"Selected clustering ids ({ref_clust_id} & {clust_id}) are not based on the same properties!")
            logger.info(f"Clustering {ref_clust_id} for {args.reference} use: {ref_clust_cfg['features']}")
            logger.info(f"Clustering {clust_id} for {ts} use: {clust_cfg['features']}")
            continue  # skip to the next pairing
        else:
            properties = ref_clust_cfg['features']
            ref_weights = ref_clust_cfg['weights']
            weights = clust_cfg['weights']

        # - Compute the cost matrix for pairing clusters based on the cell properties:
        cost_func = wasserstein_distance_matrix_from_properties
        cost, cluster_idx_a, cluster_idx_b = cost_func(ref_ttg, ttg, ref_clust_name, clust_name,
                                                       properties, ref_weights, weights)
        # - Convert to a DataFrame:
        cost_df[ts] = pd.DataFrame(cost, columns=[f'Cluster_{int(qi)}' for _, qi in cluster_idx_b.items()],
                                   index=[f'Ref_Cluster_{int(qi)}' for _, qi in cluster_idx_a.items()])
        cost_df[ts].to_csv(args.out_path.joinpath(f'{args.reference}_{ts}-cost_matrix.csv'))
        # - Find the best cluster pairings:
        cluster_matching = match_cluster_from_cost(cost, cluster_idx_a, cluster_idx_b)
        q_match[ts] = cluster_matching
        # - Create the cluster relabelling dictionary:
        o2n_q_id = {int(idx_b): int(idx_a) for (idx_a, idx_b) in cluster_matching}
        # If the selected clustering for the current time-series has more clusters than the reference,
        # we have to relabel unpaired clusters:
        if clust_cfg['n_clusters'] > ref_clust_cfg['n_clusters']:
            last_q_id = max(list(o2n_q_id.values()))  # last paired cluster id (should increment from there)
            missing_q_id = list(
                set(range(1, clust_cfg['n_clusters'] + 1)) - set(o2n_q_id.keys()))  # list of unpaired cluster ids
            for n, q_id in enumerate(missing_q_id):
                o2n_q_id[q_id] = last_q_id + n + 1
        # Add the relabelling for outliers cluster:
        o2n_q_id[outliers_cluster_id] = outliers_cluster_id
        # - Use the cluster pairings & colors to generate a new clustering report with matching colors and cluster ids:
        make_figures_and_report(ts, time_series_json, ttg, config, clust_by_ts, args.out_path, colors_by_q,
                                args.reference, logger=logger, relabelling=o2n_q_id)

    cluster_matching_report(cost_df, q_match, args.out_path, args.reference, args.time_series, config['clustering'],
                            clust_by_ts, html=args.html)


def run():
    parser = parsing()
    args = parser.parse_args()
    main(args)


if __name__ == '__main__':
    run()
