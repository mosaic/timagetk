#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Image Utilities for the TimageTK Library

This module provides utility functions and classes to work with various image types
defined within the TimageTK library. It supports operations such as checking image
types, retrieving image attributes, and managing metadata. These tools ensure smooth
handling and manipulation of images in various formats and contexts, making them
useful for image processing tasks, especially in tissue analysis and modeling.

Key Features

- **Type Assertions**:
  Functions like `assert_image` and `assert_spatial_image` validate whether an
  object is an image or a `SpatialImage` to ensure compatibility with TimageTK operations.

- **Image Metadata**:
  Retrieve and manage attributes using `IMAGE_MD_TAGS` and the `get_image_attributes`
  function, enabling metadata extraction from image objects.

- **Image Class Identification**:
  Determine and categorize images into the defined types, such as `TissueImage2D`,
  `TissueImage3D`, `LabelledImage`, and `SpatialImage`.

- **Isometric Checks**:
  Functions like `_input_img_check` ensure proper handling of images with voxel-based
  operations by checking their isometric properties.

Usage Examples

Example 1: Verify an object is a valid image type.

```python
from timagetk.components.image import assert_image
from timagetk.components.spatial_image import SpatialImage

# Create a spatial image instance (dummy example).
image = SpatialImage()

# Assert if the object is an image.
try:
    assert_image(image)
    print("The object is a valid image.")
except TypeError as e:
    print(e)
```

Example 2: Retrieve attributes from an image object.

```python
from timagetk.components.image import get_image_attributes
from timagetk.array_util import dummy_labelled_image_3D

# Generate a dummy 3D labeled image.
image = dummy_labelled_image_3D([0.2, 0.5, 0.5])

# Extract attributes.
attributes = get_image_attributes(image)
print(attributes)
```

This module is a core utility for ensuring the integrity and consistency of image
processing operations performed by the TimageTK library.
"""

from timagetk.bin.logger import get_logger
from timagetk.components.labelled_image import LabelledImage
from timagetk.components.metadata import IMAGE_MD_TAGS
from timagetk.components.multi_channel import MultiChannelImage
from timagetk.components.spatial_image import SpatialImage
from timagetk.components.tissue_image import AbstractTissueImage
from timagetk.components.tissue_image import TissueImage2D
from timagetk.components.tissue_image import TissueImage3D
from timagetk.util import clean_type

#: List of known image types from timagetk.
IMAGE_TYPES = (SpatialImage, LabelledImage, TissueImage2D, TissueImage3D, MultiChannelImage)

log = get_logger(__name__)


def assert_image(obj, obj_name=None):
    """Tests whether given object is an `Image`.

    Parameters
    ----------
    obj : instance
        Object to test
    obj_name : str, optional
        If given used as object name for TypeError printing
    """
    if obj_name is None:
        try:
            obj_name = obj.filename
        except AttributeError:
            obj_name = clean_type(obj)

    err = "Input '{}' is not an Image instance."
    try:
        assert isinstance(obj, IMAGE_TYPES)
    except AssertionError:
        raise TypeError(err.format(obj_name))

    return


def _input_img_check(input_image, real=False, var_name='given image'):
    """Used to check `input_image` type and method units.

    If real is False, check that the given input image is isometric.

    Parameters
    ----------
    input_image : timagetk.SpatialImage
        Tested input type
    real : bool, optional
        Indicate if the method works on real or voxel units
    var_name : str, optional
        Variable name, usd for better

    """
    if isinstance(input_image, list) and isinstance(input_image[0], SpatialImage):
        return [_input_img_check(im, real=real) for im in input_image]

    # - Check the ``input_image`` is indeed a ``SpatialImage``
    assert_spatial_image(input_image, obj_name='input_image')

    # - Check the isometry of the image when using voxel units:
    if not real and not input_image.is_isometric():
        msg = "WARNING: The {} is NOT isometric and this method operates on voxels!"
        log.warning(msg.format(var_name))
        return None
    else:
        return None


def get_image_class(image):
    """Return the class corresponding to the given image."""
    if isinstance(image, TissueImage3D):
        return TissueImage3D
    elif isinstance(image, TissueImage2D):
        return TissueImage2D
    elif isinstance(image, MultiChannelImage):
        return MultiChannelImage
    elif isinstance(image, LabelledImage):
        return LabelledImage
    elif isinstance(image, SpatialImage):
        return SpatialImage
    else:
        msg = "Input `image` is not an Image type: {}!"
        raise NotImplementedError(msg.format(clean_type(image)))


def get_image_attributes(image, exclude=None, extra=None):
    """Get a dictionary of image attributes.

    Parameters
    ----------
    image : image-like
        The image to extract attributes from.
        The list of attribute to extract is defined in ``IMAGE_MD_TAGS``, plus some specific to instances.
        See the notes section from more details.
    exclude : list, optional
        A list of attributes names to exclude from the returned dictionary.
    extra : list, optional
        A list of extra attributes names to get from the image.
        Any missing attribute will be set to ``None``.

    Returns
    -------
    dict
        The dictionary of attribute names and values.

    See Also
    --------
    timagetk.components.metadata.IMAGE_MD_TAGS

    Notes
    -----
    ``TissueImage`` will have a default `'background'` value set to ``1``.

    ``LabelledImage`` will have a default `'not_a_label'` value set to ``0``.

    Any missing attribute from ``IMAGE_MD_TAGS`` will be set to ``None``.


    Examples
    --------
    >>> from timagetk.components.image import get_image_attributes
    >>> # Example 1: get the image attributes from a 3D `LabelledImage`:
    >>> from timagetk.array_util import dummy_labelled_image_3D
    >>> img = dummy_labelled_image_3D([0.2, 0.5, 0.5])
    >>> attr = get_image_attributes(img)
    >>> print(attr)
    {'not_a_label': 0, 'shape': (5, 13, 12), 'ndim': 3, 'dtype': dtype('uint8'), 'origin': [0, 0, 0], 'voxelsize': [0.2, 0.5, 0.5], 'unit': 1e-06}
    >>> # Example 2: get the image attributes from a `TissueImage3D`:
    >>> from timagetk import TissueImage3D
    >>> tissue = TissueImage3D(img, background=1)
    >>> attr = get_image_attributes(tissue)
    >>> print(attr)
    {'background': 1, 'not_a_label': 0, 'shape': (5, 13, 12), 'ndim': 3, 'dtype': dtype('uint8'), 'origin': [0, 0, 0], 'voxelsize': [0.2, 0.5, 0.5], 'unit': 1e-06}

    """
    from timagetk.util import get_attributes

    attr_dict = {}
    if isinstance(image, AbstractTissueImage):
        attr_dict.update({'background': getattr(image, '_background_id', 1)})
    if isinstance(image, MultiChannelImage):
        # TODO!
        pass
    if isinstance(image, LabelledImage):
        attr_dict.update({'not_a_label': getattr(image, '_not_a_label', 0)})
    if isinstance(image, SpatialImage):
        attr_dict.update(get_attributes(image, IMAGE_MD_TAGS))

    if extra is not None:
        attr_dict.update(get_attributes(image, extra))

    if exclude is not None:
        for attr in exclude:
            try:
                attr_dict.pop(attr)
            except:
                pass

    return attr_dict


def assert_spatial_image(obj, obj_name=None):
    """Assert that object is a ``SpatialImage`` instance.

    Parameters
    ----------
    obj : instance
        Object to test as ``SpatialImage`` instance.
    obj_name : str, optional
        If given used as object name for ``TypeError`` printing.

    Raises
    ------
    TypeError
        If the input `obj` is not a ``SpatialImage`` instance.

    """
    if obj_name is None:
        try:
            obj_name = obj.filename
        except AttributeError:
            obj_name = clean_type(obj)

    err = "Input '{}' is not a SpatialImage instance."
    try:
        assert isinstance(obj, SpatialImage)
    except AssertionError:
        raise TypeError(err.format(obj_name))

    return


def assert_all_spatial_image(objects):
    """Assert that all objects are ``SpatialImage`` instance.

    Parameters
    ----------
    obj : Iterable
        Objects to test as ``SpatialImage`` instances.

    Raises
    ------
    TypeError
        If any of the input `obj` is not a ``SpatialImage`` instance.

    """
    [assert_spatial_image(obj) for obj in objects]
    return
