#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Module to analyse clustering.

It regroups methods dedicated to clustering analysis and inspection.
You will find various method to assess the clustering quality, generate figures and more.
It leverages the ``clustering`` module from the ``algorithms`` sub-package.
"""

from math import sqrt

import numpy as np
from matplotlib import pyplot as plt
from sklearn import metrics

from timagetk.algorithms.clustering_estimators import between_cluster_distances
from timagetk.algorithms.clustering_estimators import cluster_diameters
from timagetk.algorithms.clustering_estimators import cluster_separation
from timagetk.algorithms.clustering_estimators import element_distance_to_cluster
from timagetk.algorithms.clustering_estimators import global_cluster_distances
from timagetk.algorithms.clustering import n_elements_by_cluster
from timagetk.algorithms.clustering_estimators import within_cluster_distances
from timagetk.bin.logger import get_logger

log = get_logger(__name__)


class ClustererChecker:
    """Analyse and assess the quality of clusters in a ``FeatureClusterer`` object.

    Attributes
    ----------
    clusterer : FeatureClusterer
        The object to analyse.
    ids : list
        The list of time-indexed cell ids tuples ``(tp, cell_id)`` used in the `clusterer` to generates the global pairwise distance matrix.
        This corresponds to `clusterer._global_pw_dist_ids`.
    clustering_name : str
        Auto generated name of the global pairwise distance matrix.
    info_clustering : dict
        Dictionary regrouping several `clusterer` attributes.
    nb_clusters : int
        Number of required (supervised) or obtained (unsupervised) cluster.
    clustering : dict
        Clustering dictionary indexed by time-indexed cell ids tuples ``(tp, cell_id)``.
    distance_matrix : numpy.ndarray
        Global standardized pairwise distance matrix.
        Reduced to the list of ``self.clusterer._global_pw_dist_ids`` ids.
        Used a similarity matrix for clustering.

    Examples
    --------
    >>> from timagetk.components.clustering import example_clusterer
    >>> clust = example_clusterer()
    >>> clust.assemble_matrix(['topological', 'area', "log_relative_value('volume',1)"], [0.2, 0.4, 0.4])
    >>> clust.compute_clustering(method='ward', n_clusters=4)
    >>> from timagetk.components.clustering_analysis import ClustererChecker
    >>> ccheck = ClustererChecker(clust)
    >>> print(ccheck.list_cluster_ids())
    {0, 1, 2, 3}
    >>> ccheck.nb_elements_by_clusters()
    {3: 36, 1: 72, 2: 43, 0: 128}
import timagetk.algorithms.clustering_estimators    >>> timagetk.algorithms.clustering_estimators.within_cluster_distances()
    {3: 1.248493226916846,
     1: 1.5889679763317919,
     2: 1.2771591107757134,
     0: 1.5559765687985145}
import timagetk.algorithms.clustering_estimators    >>> timagetk.algorithms.clustering_estimators.between_cluster_distances()
    {3: 2.0959352865124115,
     1: 1.5019640896409803,
     2: 1.8298356413033143,
     0: 0.993164484784841}
import timagetk.algorithms.clustering_estimators    >>> timagetk.algorithms.clustering_estimators.global_cluster_distances()
    (3.683133842618269, 1.998743664845256)

    """

    def __init__(self, clusterer):
        """Constructor.

        Parameters
        ----------
        clusterer : timagetk.components.clustering.FeatureClusterer
            The object to analyse.
        """
        # - Paranoia:
        assert clusterer.__class__.__name__ == 'FeatureClusterer'
        # - Initialisation
        self.clusterer = clusterer
        self.outliers_cluster_id = clusterer.outliers_cluster_id

        # - Reformat inherited info
        self.ids = clusterer._global_pw_dist_ids
        # Make sure the `outliers_cluster_id` is not in the cell indexed clustering dictionary:
        self.clustering = clusterer.get_clustering_dict(exclude_outlier_cluster=True)
        # TODO: extend the class functionalities to be able to include the outliers in the analysis?
        self.distance_matrix = clusterer._global_pw_dist_mat
        self.nb_clusters = clusterer._nb_clusters
        self.clustering_name = clusterer._global_pw_dist_name
        self.info_clustering = {'method': clusterer._method, 'variables': clusterer._global_pw_dist_variables,
                                'weights': clusterer._global_pw_dist_weights, 'outliers': clusterer._outlier_ids}

        # - Check for undesirable situations: a cluster with only one element (some methods won't like it!)
        non_outliers_clusters = list(set(clusterer.list_cluster_ids()) - {self.outliers_cluster_id})
        n_elt_by_cluster = n_elements_by_cluster(self.clustering)
        for q in non_outliers_clusters:
            if n_elt_by_cluster[q] == 1:
                log.warning(f"Cluster {q} has only one element!")

    def elements_by_clusters(self) -> dict:
        """Get the dictionary containing the list of element ids by the cluster they belong to."""
        return self.clusterer.cell_ids_by_cluster()

    def nb_elements_by_clusters(self) -> dict:
        """Get the dictionary containing the number of elements by cluster."""
        return n_elements_by_cluster(self.clustering)

    def list_cluster_ids(self) -> list:
        """Get the list of valid cluster ids, that is the cluster ids excluding the outlier cluster id."""
        return self.clusterer.list_cluster_ids()

    def element_distance_to_clusters(self):
        """Compute the distance between an element and each cluster.

        Returns
        -------
        dict
            Time indexed cell ids dictionary of cluster indexed distance dictionaries {(tp, cid): {q: D(cid,q)}}

        Notes
        -----
        The distance :math:`D(i,q)` between a element *i* and a cluster *q*:
        if :math:`i \in q`, :math:`D(i,q) = \dfrac{\sum_{j \in q, j \neq i} D(i,j)}{N_q - 1},`
        else, :math:`D(i,q) = \dfrac{\sum_{i \neq j} D(i,j)}{N_q},`
        where :math:`D(i,j)`$` is the distance between element *i* and *j*,
        :math:`N_q` the number of element in cluster *q*.

        Examples
        --------
        >>> import numpy as np
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer()
        >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5])
        >>> clust.compute_clustering(method='ward', n_clusters=4)
        >>> from timagetk.components.clustering_analysis import ClustererChecker
        >>> ccheck = ClustererChecker(clust)
        >>> D_tcid_q = ccheck.element_distance_to_clusters()
        >>> D_tcid_q[(2, 129)]
        {0: 1.123208240009025,
         1: 0.6857721942484629,
         2: 1.3660433957015157,
         3: 1.993538515433321}
        """
        Diq = element_distance_to_cluster(self.distance_matrix, list(self.clustering.values()))

        return {tcid: {q: Diq[(i, q)] for q in self.list_cluster_ids()} for i, tcid in enumerate(self.ids)}

    def element_distance_to_cluster_center(self):
        """Compute the distance between a vertex and the center of its group.

        Returns
        -------
        dict
            Element indexed dictionary of distance to cluster ``{(tp, cid): D(cid,q_cid)}``

        Examples
        --------
        >>> import numpy as np
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer()
        >>> clust.assemble_matrix(['topological', 'area', "log_relative_value('volume',1)"], [0.2, 0.4, 0.4])
        >>> clust.compute_clustering(method='ward', n_clusters=4)
        >>> from timagetk.components.clustering_analysis import ClustererChecker
        >>> ccheck = ClustererChecker(clust)
        >>> ccheck.element_distance_to_cluster_center()
        """
        D_iq = self.element_distance_to_clusters()

        elt_dist2center = {}
        for i in self.ids:
            elt_dist2center[i] = D_iq[i][self.clustering[i]]

        return elt_dist2center

    def plot_element_distance_to_cluster(self, cluster_names=None, figname=None, **kwargs):
        """Plot the distance between a vertex and the center of its group.

        Parameters
        ----------
        cluster_names : dict
            Cluster indexed dictionary of names to give to the clusters.
        figname : str
            File name of the figure to save. Do not forget to add the extension.
            By default, show the matplolib figure instead of saving it.

        Other Parameters
        ----------------
        cmap : str or dict, optional
            Name of the matplotlib colormap to use to color the cluster boxes.
            Or cluster indexed dictionary of RGBA colors.

        Examples
        --------
        >>> import numpy as np
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer()
        >>> clust.assemble_matrix(['topological', 'area', "log_relative_value('volume',1)"], [0.2, 0.4, 0.4])
        >>> clust.compute_clustering(method='ward', n_clusters=4)
        >>> from timagetk.components.clustering_analysis import ClustererChecker
        >>> ccheck = ClustererChecker(clust)
        >>> ccheck.plot_element_distance_to_cluster()
        """
        vtx2center = self.element_distance_to_cluster_center()
        # Get the cluster ids:
        cluster_ids = self.list_cluster_ids()
        # Handling the colormap to use with the clusters:
        color_by_cluster = self.clusterer._check_color_by_cluster(kwargs.get('cmap', 'viridis'), cluster_ids)

        # Initialize the figure:
        fig = plt.figure(figsize=(10, 5))
        index_q = {}
        ids = list(self.ids)
        for q in cluster_ids:
            index_q[q] = [ni for ni, (i, clust_i) in enumerate(self.clustering.items()) if clust_i == q]
            vector = [vtx2center[ids[i]] for i in index_q[q]]
            vector.sort()
            if not cluster_names:
                plt.plot(vector, '.-', label="Cluster " + str(q), figure=fig,
                         color=tuple(color_by_cluster[q]))
            else:
                plt.plot(vector, '.-', label=str(cluster_names[q]), figure=fig, color=tuple(color_by_cluster[q]))
            plt.title(self.clustering_name)
            plt.suptitle("Elements distance to their cluster center", fontweight='semibold')

            plt.xlabel("Distance ranked elements")
            plt.ylabel("Distance to cluster center")
            x_start, x_stop = 0, max(self.nb_elements_by_clusters().values())
            y_start, y_stop = min(vtx2center.values()), max(vtx2center.values())
            plt.axis([x_start, x_stop + 1, y_start - 0.05 * y_start, y_stop + 0.05 * y_stop])

        plt.grid(linestyle='-.')
        plt.legend(ncol=2, framealpha=0.7, fontsize='small')
        plt.tight_layout()
        plt.subplots_adjust(top=0.9)
        if isinstance(figname, str):
            plt.savefig(figname)
            plt.close()
        else:
            plt.show()
        return

    def distance_sorted_ids_by_cluster(self, return_distance=False):
        """Return the distance sorted list of element ids per cluster.

        Parameters
        ----------
        return_distance : bool
            If ``True`` return the distances instead of the element ids.
            By default, return the element ids sorted by distance (by cluster).

        Returns
        -------
        dict
            Cluster indexed dictionary of element ids sorted by cluster

        Examples
        --------
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer()
        >>> clust.assemble_matrix(['topological', 'area', "log_relative_value('volume',1)"], [0.2, 0.4, 0.4])
        >>> clust.compute_clustering(method='ward', n_clusters=4)
        >>> from timagetk.components.clustering_analysis import ClustererChecker
        >>> ccheck = ClustererChecker(clust)
        >>> distance_sorted_ids = ccheck.distance_sorted_ids_by_cluster()
        >>> distance_by_cluster = ccheck.distance_sorted_ids_by_cluster(True)
        """
        vtx2center = self.element_distance_to_cluster_center()
        elt_by_clusters = self.elements_by_clusters()
        cluster_ids = self.list_cluster_ids()

        s_ids = {}
        for q in cluster_ids:
            vtx2center_in_q = {i: d_iqi for i, d_iqi in vtx2center.items() if i in elt_by_clusters[q]}
            sorted_index = np.argsort(list(vtx2center_in_q.values()))
            elt_list = list(vtx2center_in_q.keys())
            sorted_elt = [elt_list[i] for i in sorted_index]
            if return_distance:
                s_ids[q] = [vtx2center_in_q[i] for i in sorted_elt]
            else:
                s_ids[q] = sorted_elt
        return s_ids

    def cluster_distance_matrix(self):
        """Compute the distance between clusters matrix.

        Returns
        -------
        numpy.ndarray
            The cluster distance matrix.

        Notes
        -----
        For :math:` \ell \eq q, D(q,\ell) = \dfrac{ \sum_{i,j \in q; i \neq j} D(i,j) }{(N_{q}-1)N_{q}}`,
        For :math:` \ell \neq q, D(q,\ell) = \dfrac{ \sum_{i \in q} \sum_{j \in \ell} D(i,j) }{N_{q} N_{\ell}}`,
        where :math:`D(i,j)` is the distance matrix,
        :math:`N_{q}` and :math:`N_{\ell}` are the number of elements found in clusters :math:`q` and :math:`\ell`.

        Examples
        --------
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer()
        >>> clust.assemble_matrix(['topological', 'area', "log_relative_value('volume',1)"], [0.2, 0.4, 0.4])
        >>> clust.compute_clustering(method='ward', n_clusters=4)
        >>> from timagetk.components.clustering_analysis import ClustererChecker
        >>> ccheck = ClustererChecker(clust)
        >>> cdm = ccheck.cluster_distance_matrix()
        >>> np.round(cdm, 3)
        """
        n_elt_by_cluster = self.nb_elements_by_clusters()
        cluster_ids = self.list_cluster_ids()

        D = np.zeros(shape=[self.nb_clusters, self.nb_clusters], dtype=float)
        for n, q in enumerate(cluster_ids):
            for m, l in enumerate(cluster_ids):
                if n == m:
                    index_q = [ni for ni, (i, clust_i) in enumerate(self.clustering.items()) if clust_i == q]
                    D[n, m] = sum([self.distance_matrix[i, j] for i in index_q for j in index_q if i != j]) / (
                            (n_elt_by_cluster[q] - 1) * n_elt_by_cluster[q])
                if n > m:
                    index_q = [ni for ni, (i, clust_i) in enumerate(self.clustering.items()) if clust_i == q]
                    index_l = [ni for ni, (i, clust_i) in enumerate(self.clustering.items()) if clust_i == l]
                    D[n, m] = D[m, n] = sum([self.distance_matrix[i, j] for i in index_q for j in index_l]) / (
                            n_elt_by_cluster[q] * n_elt_by_cluster[l])

        return D

    def cluster_distances_heatmap(self, print_values=True, figname=None, decimals=2, **kwargs):
        """Creates the cluster distance heatmap.

        Parameters
        ----------
        print_values : bool
            If ``True`` (default), print the cluster distance values on top of the heatmap.
        figname : None or str
            File name of the figure to save. Do not forget to add the extension.
            By default, show the matplolib figure instead of saving it.
        digits : int, optional
            The number of decimals to display with ``print_values=True``.

        Examples
        --------
        >>> import numpy as np
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer()
        >>> clust.assemble_matrix(['topological', 'area', "log_relative_value('volume',1)"], [0.2, 0.4, 0.4])
        >>> clust.compute_clustering(method='ward', n_clusters=4)
        >>> from timagetk.components.clustering_analysis import ClustererChecker
        >>> ccheck = ClustererChecker(clust)
        >>> ccheck.cluster_distances_heatmap()
        >>> ccheck.cluster_distances_heatmap(True)
        """
        cluster_distances = self.cluster_distance_matrix()
        numrows, numcols = cluster_distances.shape

        fig = plt.figure(figsize=(numcols*1.25+2, numrows*1.25))
        ax1 = fig.add_subplot(111)
        plt.imshow(cluster_distances, cmap='viridis', interpolation=None, origin='upper')
        plt.xticks(range(numcols), self.list_cluster_ids(), fontweight='semibold')
        plt.yticks(range(numrows), self.list_cluster_ids(), fontweight='semibold')
        ax1.axes.tick_params(length=0)
        ax1.xaxis.tick_top()  # move the x-axis to the top

        if print_values:
            font = {'family': 'monospace', 'color': 'white', 'weight': 'semibold', 'size': 16}
            alignment = {'horizontalalignment': 'center', 'verticalalignment': 'center'}
            for nrow in range(numrows):
                for ncol in range(numcols):
                    plt.text(nrow, ncol, np.round(cluster_distances[nrow, ncol], decimals), fontdict=font, **alignment)

        suptitle = kwargs.get('title', None)
        if suptitle is not None:
            plt.suptitle(suptitle, fontweight='semibold')
        else:
            plt.suptitle("Cluster distances heat-map", fontweight='semibold')
        plt.title(self.clustering_name)

        cbar = plt.colorbar()
        cbar.ax.tick_params(labelsize=14)
        plt.tight_layout()
        plt.subplots_adjust(top=0.9)
        if isinstance(figname, str):
            plt.savefig(figname)
            plt.close()
        else:
            plt.show()

    def cluster_diameters(self):
        """Compute the *cluster diameter* metrics, that is the max distance between two vertex from the same cluster.

        Returns
        -------
        dict of float
            The dictionary of cluster diameter.

        See Also
        --------
        timagetk.algorithms.clustering.cluster_diameters

        Examples
        --------
import timagetk.algorithms.clustering_estimators        >>> import numpy as np
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer()
        >>> clust.assemble_matrix(['topological', 'area', "log_relative_value('volume',1)"], [0.2, 0.4, 0.4])
        >>> clust.compute_clustering(method='ward', n_clusters=4)
        >>> from timagetk.components.clustering_analysis import ClustererChecker
        >>> ccheck = ClustererChecker(clust)
        >>> timagetk.algorithms.clustering_estimators.cluster_diameters()
        {3: 3.5335449276534945,
         1: 4.606053437532901,
         2: 3.198027300374117,
         0: 3.2591502290446526}
        """
        return cluster_diameters(self.distance_matrix, list(self.clustering.values()))

    def cluster_separation(self):
        """Compute the *cluster separation* metrics, that is the min distance between two vertex from two different clusters.

        Returns
        -------
        dict of float
            The dictionary of cluster separation.

        Notes
        -----
        The cluster separation is expressed as follows: :math:`\min_{i \in q, j \not\in q} D(j,i)`,
        where :math:`D(i,j)` is the distance matrix and :math:`q` a cluster.

        Examples
        --------
import timagetk.algorithms.clustering_estimators        >>> import numpy as np
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer()
        >>> clust.assemble_matrix(['topological', 'area', "log_relative_value('volume',1)"], [0.2, 0.4, 0.4])
        >>> clust.compute_clustering(method='ward', n_clusters=4)
        >>> from timagetk.components.clustering_analysis import ClustererChecker
        >>> ccheck = ClustererChecker(clust)
        >>> timagetk.algorithms.clustering_estimators.cluster_separation()
        {3: 0.00027347848322563286,
         1: 0.00017934141739168387,
         2: 7.421024167931746e-05,
         0: 7.421024167931746e-05}
        """
        return cluster_separation(self.distance_matrix, list(self.clustering.values()))

    def within_cluster_distances(self):
        """Compute the within cluster distance.

        Returns
        -------
        dict of float
            The dictionary of within cluster distances.

        Examples
        --------
import timagetk.algorithms.clustering_estimators        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer()
        >>> clust.assemble_matrix(['topological', 'area', "log_relative_value('volume',1)"], [0.2, 0.4, 0.4])
        >>> clust.compute_clustering(method='ward', n_clusters=4)
        >>> from timagetk.components.clustering_analysis import ClustererChecker
        >>> ccheck = ClustererChecker(clust)
        >>> timagetk.algorithms.clustering_estimators.within_cluster_distances()
        {3: 1.248493226916846,
         1: 1.5889679763317919,
         2: 1.2771591107757134,
         0: 1.5559765687985145}
        """
        return within_cluster_distances(self.distance_matrix, list(self.clustering.values()))

    def between_cluster_distances(self):
        """Compute the between cluster distance.

        Returns
        -------
        dict of float
            The dictionary of between cluster distances.

        Examples
        --------
import timagetk.algorithms.clustering_estimators        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer()
        >>> clust.assemble_matrix(['topological', 'area', "log_relative_value('volume',1)"], [0.2, 0.4, 0.4])
        >>> clust.compute_clustering(method='ward', n_clusters=4)
        >>> from timagetk.components.clustering_analysis import ClustererChecker
        >>> ccheck = ClustererChecker(clust)
        >>> timagetk.algorithms.clustering_estimators.between_cluster_distances()
        {3: 2.0959352865124115,
         1: 1.5019640896409803,
         2: 1.8298356413033143,
         0: 0.993164484784841}
        """
        return between_cluster_distances(self.distance_matrix, list(self.clustering.values()))

    def global_cluster_distances(self):
        """Function computing global cluster distances.

        Parameters
        ----------
        outliers_cluster_id : int
            Id of the outliers cluster.
            It will be excluded from the sums of *within* and *between* cluster distances.

        Returns
        -------
        float
            The sum of *within* cluster distance
        float
            The sum of *between* cluster distance

        Examples
        --------
import timagetk.algorithms.clustering_estimators        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer()
        >>> clust.assemble_matrix(['topological', 'area', "log_relative_value('volume',1)"], [0.2, 0.4, 0.4])
        >>> clust.compute_clustering(method='ward', n_clusters=4)
        >>> from timagetk.components.clustering_analysis import ClustererChecker
        >>> ccheck = ClustererChecker(clust)
        >>> timagetk.algorithms.clustering_estimators.global_cluster_distances()
        (3.683133842618269, 1.998743664845256)
        """
        return global_cluster_distances(self.distance_matrix, list(self.clustering.values()), self.outliers_cluster_id)

    def cluster_by_time_points(self) -> dict:
        """Return the representativity of each cluster by time points.

        Examples
        --------
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer()
        >>> clust.assemble_matrix(['topological', 'area', "log_relative_value('volume',1)"], [0.2, 0.4, 0.4])
        >>> clust.compute_clustering(method='ward', n_clusters=4)
        >>> from timagetk.components.clustering_analysis import ClustererChecker
        >>> ccheck = ClustererChecker(clust)
        >>> ccheck.cluster_by_time_points()
        {0: {0: 0, 1: 36, 2: 11, 3: 16},
         1: {0: 0, 1: 36, 2: 32, 3: 20},
         2: {0: 128, 1: 0, 2: 0, 3: 0}}
        """
        ids_by_c = self.elements_by_clusters()
        c_tp = {t: {q: 0 for q in self.list_cluster_ids()} for t in range(self.clusterer.graph.nb_time_points)}
        for q, ids in ids_by_c.items():
            for (ti, _i) in ids:
                c_tp[ti][q] += 1

        return c_tp

    def time_point_by_clusters(self) -> dict:
        """Return the representativity of each time points by clusters.

        Examples
        --------
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer()
        >>> clust.assemble_matrix(['topological', 'area', "log_relative_value('volume',1)"], [0.2, 0.4, 0.4])
        >>> clust.compute_clustering(method='ward', n_clusters=4)
        >>> from timagetk.components.clustering_analysis import ClustererChecker
        >>> ccheck = ClustererChecker(clust)
        >>> ccheck.time_point_by_clusters()
        {0: {0: 0, 1: 0, 2: 128},
         1: {0: 36, 1: 36, 2: 0},
         2: {0: 11, 1: 32, 2: 0},
         3: {0: 16, 1: 20, 2: 0}}
        """
        ids_by_c = self.elements_by_clusters()
        tp_c = {q: {t: 0 for t in range(self.clusterer.graph.nb_time_points)} for q in self.list_cluster_ids()}
        for q, ids in ids_by_c.items():
            for (ti, _i) in ids:
                tp_c[q][ti] += 1

        return tp_c

    def boxplot_properties_by_cluster(self, cluster_names=None, figname=None, **kwargs):
        """Creates boxplot of properties (used for clustering) by clusters.

        Parameters
        ----------
        cluster_names : list of str, optional
            List of names to give to the clusters on the figure.
            Defaults to the id of the cluster.
        figname : None or str, optional
            If defined, should be a valid filename, used to save the figure.

        Other Parameters
        ----------------
        rotation : int
            The rotation to apply to given `cluster_names` if any.
            Useful if the specified `cluster_names` are a bit long and could overlap.
        dpi : int
            The number of dot per inch (dpi) to use to create the figure. Defaults to ``96``.
        cmap : str, optional
            Name of the matplotlib colormap to use to color the cluster boxes.

        Examples
        --------
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer()
        >>> clust.assemble_matrix(['topological', 'area', "log_relative_value('volume',1)"], [0.2, 0.4, 0.4])
        >>> clust.compute_clustering(method='ward', n_clusters=4)
        >>> from timagetk.components.clustering_analysis import ClustererChecker
        >>> ccheck = ClustererChecker(clust)
        >>> ccheck.boxplot_properties_by_cluster()
        """
        self.clusterer.boxplot_properties_by_cluster(cluster_names=cluster_names, figname=figname, **kwargs)
        return

    def __score_param(func):
        def wrapped_function(self, dict_labels_expert, groups2compare=None):
            """Wrapped function for clustering score computation according to the knowledge of the ground truth class assignments.

            Parameters
            ----------
            dict_labels_expert: dict
                expert defined domains / clusters in which keys are labels
            groups2compare: list
                pair(s) of groups id to compare, with first the expert id then the predicted id ex. [0,6] or [[0,6],[4,3]]
            """
            if groups2compare is None:
                compare_groups = True
                groups2compare = np.array([], ndmin=2)
            else:
                compare_groups = False

            not_found = []
            labels_true, labels_pred = [], []
            max1 = max(dict_labels_expert.values()) + 1
            max2 = max(self.clustering.values()) + 1
            for k, v in dict_labels_expert.items():
                if k in self.clustering:
                    v2 = self.clustering[k]
                    if compare_groups:
                        labels_true.append(v if (v in groups2compare[:, 0]) else max1)
                        labels_pred.append(v2 if (v2 in groups2compare[:, 1]) else max2)
                        # labels_pred.append(v2)
                    else:
                        labels_true.append(v)
                        labels_pred.append(v2)
                else:
                    not_found.append(k)

            if not_found != []:
                log.warning("These labels were not found in the clustering result: {}".format(not_found))

            return func(labels_true, labels_pred)

        return wrapped_function

    @__score_param
    def adjusted_rand_score(labels_true, labels_pred):
        """The Adjusted Rand Index (ARI) is a function that measures the similarity of the two assignments, ignoring permutations and with chance normalization.

        Parameters
        ----------
        labels_true: list
            Knowledge of the ground truth class assignments
        labels_pred: list
            Clustering algorithm assignments of the same samples

        Notes
        -----
        - Random (uniform) label assignments have a ARI score close to ``0.0``.
        - Bounded range [-1, 1]. Negative values are bad (independent labellings), similar clustering have a positive ARI, ``1.0`` is the perfect match score.
        - No assumption is made on the cluster structure: can be used to compare clustering algorithms such as k-means which assumes isotropic blob shapes with results of spectral clustering algorithms which can find cluster with "folded" shapes.
        """
        return metrics.adjusted_rand_score(labels_true, labels_pred)

    @__score_param
    def adjusted_mutual_info_score(labels_true, labels_pred):
        """The Mutual Information (NMI and AMI) is a function that measures the agreement of the two assignments, ignoring permutations.
        Adjusted Mutual Information (AMI) was proposed more recently than NMI and is normalized against chance.

        Parameters
        ----------
        labels_true: list
            Knowledge of the ground truth class assignments
        labels_pred: list
            Clustering algorithm assignments of the same samples

        Notes
        -----
        Random (uniform) label assignments have a AMI score close to ``0.0``.
        """
        return metrics.adjusted_mutual_info_score(labels_true, labels_pred)

    @__score_param
    def normalized_mutual_info_score(labels_true, labels_pred):
        """The Mutual Information (NMI and AMI) is a function that measures the agreement of the two assignments, ignoring permutations.
        Normalized Mutual Information (NMI) is often used in the literature, but it is NOT normalized against chance.
        """
        return metrics.normalized_mutual_info_score(labels_true, labels_pred)

    @__score_param
    def homogeneity_score(labels_true, labels_pred):
        """Homogeneity: each cluster contains only members of a single class.
        Bounded below by 0.0 and above by 1.0 (higher is better).

        Parameters
        ----------
        labels_true: list
            Knowledge of the ground truth class assignments
        labels_pred: list
            Clustering algorithm assignments of the same samples

        Note
        ----
        ``homogeneity_score(a, b) == completeness_score(b, a)``
        """
        return metrics.homogeneity_score(labels_true, labels_pred)

    @__score_param
    def completeness_score(labels_true, labels_pred):
        """Completeness: all members of a given class are assigned to the same cluster.
        Bounded below by 0.0 and above by 1.0 (higher is better).

        Parameters
        ----------
        labels_true: list
            knowledge of the ground truth class assignments
        labels_pred: list
            Clustering algorithm assignments of the same samples

        Note
        ----
        ``homogeneity_score(a, b) == completeness_score(b, a)``
        """
        return metrics.completeness_score(labels_true, labels_pred)

    @__score_param
    def v_measure_score(labels_true, labels_pred):
        """Harmonic mean of homogeneity and completeness_score is called V-measure.

        Parameters
        ----------
        labels_true: list
            Knowledge of the ground truth class assignments
        labels_pred: list
            Clustering algorithm assignments of the same samples

        Notes
        -----
         ``v_measure_score`` is symmetric, it can be used to evaluate the agreement of two independent assignments on the same dataset.
        """
        return metrics.v_measure_score(labels_true, labels_pred)

    @__score_param
    def homogeneity_completeness_v_measure(labels_true, labels_pred):
        """Homogeneity, completeness and V-measure can be computed at once using homogeneity_completeness_v_measure.

        Parameters
        ----------
        labels_true: list
            Knowledge of the ground truth class assignments
        labels_pred: list
            Clustering algorithm assignments of the same samples
        """
        return metrics.homogeneity_completeness_v_measure(labels_true, labels_pred)

    def forward_projection_match(self, graph):
        """Compute the temporal evolution of ids of each clusters.
        TODO !!!.
        """
        index_time_points = list(set(graph.vertex_property('index').values()))

        forward_projection_cluster = {}
        for t in index_time_points[:-1]:
            for vid in graph.vertex_at_time(t):
                if graph.has_children(vid):
                    children = graph.children(vid)
                    for _vid_children in children:
                        forward_projection_cluster[(t, self.clustering[vid])] = 1

        return

    def update_cluster_labels(self, old2new_labels):
        """Change the cluster labels according to given info in `old2new_labels`.
        TODO !!!.

        Parameters
        ----------
        old2new_labels: dict
            Translation dictionary
        """
        assert isinstance(old2new_labels, dict)
        self.clustering = [old2new_labels[k] for k in self.clustering]

        self.__init__(self.clusterer)
        log.info("Clustering labels have been updated!")


class ClusteringMapping(object):
    """Class to map two clustering based on various metrics.

    Attributes
    ----------
    clusterer_a : timagetk.components.clustering.FeatureClusterer
        The `Clusterer` instance with the clustering to compare.
    clusterer_b : timagetk.components.clustering.FeatureClusterer
        The `Clusterer` instance with the clustering to compare.

    Examples
    --------
    >>> from timagetk.components.clustering import example_clusterer
    >>> clusterer_a = example_clusterer()
    >>> clusterer_a.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5])
    >>> clusterer_a.compute_clustering(method='ward', n_clusters=4)
    >>> clusterer_b = example_clusterer()
    >>> clusterer_b.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5])
    >>> clusterer_b.compute_clustering(method='ward', n_clusters=5)
    >>> # Boxplot of properties by clusters:
    >>> clusterer_a.boxplot_properties_by_cluster()
    >>> clusterer_b.boxplot_properties_by_cluster()

    >>> from timagetk.components.clustering_analysis import ClusteringMapping
    >>> clust_mapping = ClusteringMapping(clusterer_a, clusterer_b)
    >>> cost, cluster_ix_a, cluster_ix_b = clust_mapping.estimate_cost_matrix_from_properties()
    >>> from scipy.optimize import linear_sum_assignment
    >>> list(zip(*linear_sum_assignment(cost)))

    >>> clust_mapping.match_cluster_from_properties()

    """

    def __init__(self, clusterer_a, clusterer_b):
        """Constructor for the clustering mapping class.

        Parameters
        ----------
        clusterer_a : timagetk.components.clustering.FeatureClusterer
            The ``Clusterer`` instance with the clustering to compare.
        clusterer_b : timagetk.components.clustering.FeatureClusterer
            The ``Clusterer`` instance with the clustering to compare.
        """
        # - Paranoia:
        for clusterer in (clusterer_a, clusterer_b):
            assert clusterer.__class__.__name__ == 'FeatureClusterer'

        self.clusterer_a = clusterer_a
        self.clusterer_b = clusterer_b

    def estimate_cost_matrix_from_properties(self):
        """Return the cost matrix between clusters based on the weighted sum of normalized property median values."""
        # Check the ``FeatureClusterer`` instances have been built with the same properties:
        try:
            assert self.clusterer_a._global_pw_dist_variables == self.clusterer_b._global_pw_dist_variables
        except AssertionError:
            log.critical("Given `FeatureClusterer` instances did not use the same properties!")
            log.info(f"Properties for instance A: {self.clusterer_a._global_pw_dist_variables}")
            log.info(f"Properties for instance B: {self.clusterer_b._global_pw_dist_variables}")
            return None
        else:
            ppties = self.clusterer_a.list_gpdm_features()

        # Get the values by cluster and property:
        val_by_q_by_ppty_a = self.clusterer_a._property_values_by_cluster_by_properties()
        val_by_q_by_ppty_b = self.clusterer_b._property_values_by_cluster_by_properties()
        # Compute the unit variance by property:
        std_by_ppty_a = {ppty: np.nanstd(list(self.clusterer_a._property_values(ppty).values())) for ppty in ppties}
        std_by_ppty_b = {ppty: np.nanstd(list(self.clusterer_b._property_values(ppty).values())) for ppty in ppties}
        # Normalize data to the [0, 1] unit range:
        n_val_by_q_by_ppty_a = {ppty: {q: val_by_q[q] / std_by_ppty_a[ppty] for q in val_by_q} for ppty, val_by_q in
                                val_by_q_by_ppty_a.items()}
        n_val_by_q_by_ppty_b = {ppty: {q: val_by_q[q] / std_by_ppty_b[ppty] for q in val_by_q} for ppty, val_by_q in
                                val_by_q_by_ppty_b.items()}

        # Compute the medians by cluster and property:
        medians_by_q_by_ppty_a = {ppty: [np.nanmedian(val_by_q[q]) for q in val_by_q] for ppty, val_by_q in
                                  n_val_by_q_by_ppty_a.items()}
        medians_by_q_by_ppty_b = {ppty: [np.nanmedian(val_by_q[q]) for q in val_by_q] for ppty, val_by_q in
                                  n_val_by_q_by_ppty_b.items()}

        # Get the weights used to combine the cell properties when building the pairwise distance matrix
        weights_a = self.clusterer_a.list_gpdm_weights()
        weights_b = self.clusterer_b.list_gpdm_weights()
        # Compute the "median" for each cluster by computing the weighted sum of medians by property:
        medians_by_q_a = {q: np.sum([weights_a[n] * medians_by_q_by_ppty_a[ppty][nq]
                                     for n, ppty in enumerate(ppties)]) for nq, q in
                          enumerate(self.clusterer_a.list_cluster_ids())}
        medians_by_q_b = {q: np.sum([weights_b[n] * medians_by_q_by_ppty_b[ppty][nq]
                                     for n, ppty in enumerate(ppties)]) for nq, q in
                          enumerate(self.clusterer_b.list_cluster_ids())}

        # Compute the cost as the Euclidean distance between each weighted sum of medians by cluster:
        cost = np.zeros(shape=(self.clusterer_a.get_number_of_clusters(), self.clusterer_b.get_number_of_clusters()))
        for na, qa in enumerate(self.clusterer_a.list_cluster_ids()):
            for nb, qb in enumerate(self.clusterer_b.list_cluster_ids()):
                cost[na, nb] = sqrt(abs(medians_by_q_a[qa] - medians_by_q_b[qb]))

        cluster_ix_a = dict(enumerate(self.clusterer_a.list_cluster_ids()))
        cluster_ix_b = dict(enumerate(self.clusterer_b.list_cluster_ids()))
        return cost, cluster_ix_a, cluster_ix_b

    def match_cluster_from_properties(self):
        """Match clusters of the two ``FeatureClusterer`` instances based on their properties."""
        from scipy.optimize import linear_sum_assignment
        cost, cluster_ix_a, cluster_ix_b = self.estimate_cost_matrix_from_properties()
        match = list(zip(*linear_sum_assignment(cost)))
        return [(cluster_ix_a[q_a], cluster_ix_b[q_b]) for (q_a, q_b) in match]
