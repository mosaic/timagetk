#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Module to cluster physical features of cellular tissues.

It regroups methods dedicated to data standardisation, handling missing values, and of course, clustering.
It leverages the ``clustering`` module from the ``algorithms`` sub-package.
"""

import copy
from math import fsum
from typing import Iterable

import numpy as np
from matplotlib import pyplot as plt
from sklearn.cluster import DBSCAN
from sklearn.cluster import SpectralClustering

from timagetk.algorithms.clustering import cluster_ids
from timagetk.algorithms.clustering import create_weight_matrix
from timagetk.algorithms.clustering import distance_matrix_from_vector
from timagetk.algorithms.clustering import elements_by_cluster
from timagetk.algorithms.clustering import euclidean_distance_matrix
from timagetk.algorithms.clustering import n_elements_by_cluster
from timagetk.algorithms.clustering import standardisation
from timagetk.algorithms.clustering import topological_distance_matrix
from timagetk.bin.logger import get_logger
from timagetk.features.utils import find_missing_values
from timagetk.graphs import TissueGraph, TemporalTissueGraph
from timagetk.util import clean_type

log = get_logger(__name__)

try:
    from hdbscan import HDBSCAN
except ImportError:
    log.warning("HDBSCAN clustering method is not available!")


def _standard_matrix_naming(pw_dist_weights, pw_dist_variables, outliers) -> str:
    """Create pairwise distance matrix name from lists of weights and variables.

    Parameters
    ----------
    pw_dist_weights : list of float
        List of variables weights used to create the global pairwise distance matrix.
    pw_dist_variables : list of str
        List of variables names used to create the global pairwise distance matrix.
    outliers : {None, "", "ignored", "deleted"}
        How outliers were managed to create the global pairwise distance matrix.

    Examples
    --------
    >>> from timagetk.components.clustering import _standard_matrix_naming
    >>> _standard_matrix_naming([0.5, 0.5], ['area', 'volume'], "")
    '0.5*area+0.5*volume'

    """
    if outliers is None or outliers == "":
        out_name = ""
    elif outliers == 'ignored':
        out_name = '-ignored_outliers'
    else:
        out_name = '-deleted_outliers'
    nw = len(pw_dist_weights)
    names = [f"{pw_dist_weights[n]}*{pw_dist_variables[n]}" for n in range(nw)]
    return f"{'+'.join(names)}{out_name}"


def _clustering_naming(clustering_method, clustering_params, pw_dist_weights, pw_dist_variables, outliers) -> str:
    """Create clustering name from clustering parameters.

    Parameters
    ----------
    clustering_method : str
        Name of the clustering method.
    clustering_params : dict
        Diction ary of clustering parameters used with selected method.
    pw_dist_weights : list of float
        List of variables weights used to create the global pairwise distance matrix.
    pw_dist_variables : list of str
        List of variables names used to create the global pairwise distance matrix.
    outliers : {None, "", "ignored", "deleted"}
        How outliers were managed to create the global pairwise distance matrix.

    Examples
    --------
    >>> from timagetk.components.clustering import _clustering_naming
    >>> _clustering_naming("Ward", {'n_clusters': 5}, [0.5, 0.5], ['area', 'volume'], "")
    'ward_5Q_0.5*area+0.5*volume'

    """
    clustering_method = clustering_method.lower()
    params = copy.copy(clustering_params)
    if clustering_method in ("single", "ward", "spectral"):
        params["Q"] = params.pop("n_clusters")

    param_str = "_".join([f"{k}:{v}" for k, v in params.items()])

    return f"{clustering_method}_{param_str}_{_standard_matrix_naming(pw_dist_weights, pw_dist_variables, outliers)}"


def contiguous_ids(dic, starting_id=0):
    """Check that the `dic` dictionary values are contiguous starting from `starting_id`, otherwise make them contiguous."""
    uniq, mini, maxi = list(set(dic.values())), min(dic.values()), max(dic.values())
    N_ids = len(uniq)
    if (N_ids == len(range(mini, maxi + 1))) and (mini == starting_id):
        return dic
    elif (N_ids == len(range(mini, maxi + 1))) and (mini != starting_id):
        diff = mini - starting_id
        return {k: v - diff for k, v in dic.items()}
    else:
        diffs = np.array(uniq) - range(starting_id, N_ids)
        return {k: v - diffs[uniq.index(v)] for k, v in dic.items()}


def example_clusterer(dataset='sphere', pre_compute=True):
    """Create a Clusterer instance from a shared dataset.

    Parameters
    ----------
    dataset : {'sphere', 'p58'}
        A valid shared dataset name.
    pre_compute : bool
        If ``True`` (default), pre-compute the following features: area, volume & log_relative_value('volume').

    Returns
    -------
    FeatureClusterer
        The instance built from the corresponding shared dataset.

    Examples
    --------
    >>> from timagetk.components.clustering import example_clusterer
    >>> clust = example_clusterer()

    """
    from timagetk.io.graph import from_csv
    from timagetk.io.dataset import shared_data
    cell_csv, wall_csv, _, _ = shared_dataset(dataset, dtype='temporal features')
    ttg = from_csv([cell_csv, wall_csv])
    clust = FeatureClusterer(ttg)
    if pre_compute:
        clust.add_spatial_variable('area', 'numeric')
        clust.add_spatial_variable('volume', 'numeric')
        clust.add_temporal_variable("log_relative_value('volume',1)", 'numeric')
    return clust


#: List of supervised clustering methods to use with ``FeatureClusterer``.
SUPERVISED_CLUSTERING_METHODS = ['single', 'complete', 'average', 'weighted', 'centroid', 'median', 'ward', 'spectral']
#: List of unsupervised clustering methods to use with ``FeatureClusterer``.
UNSUPERVISED_CLUSTERING_METHODS = ['dbscan', 'hdbscan']
#: List of clustering method to use with ``FeatureClusterer``.
ALL_CLUSTERING_METHODS = SUPERVISED_CLUSTERING_METHODS + UNSUPERVISED_CLUSTERING_METHODS
#: List of data standardisation methods to use with ``FeatureClusterer``.
STANDARDIZATION_METHODS = ["L1", "L2"]
#: List of suffixes to use to decompose vector features.
DECOMP_SUFFIXES = ['i', 'j', 'k']


class FeatureClusterer:
    """Perform cell clustering based on their feature using a TemporalTissueGraph object.

    Parameters
    ----------
    graph : timagetk.graphs.TemporalTissueGraph
        The temporal tissue graph to use for clustering by providing cell feature data.
    standardisation_method : {"L1", "L2"}, optional
        The standardisation metric to apply to the data, "L1" by default.
    ids : None or 'lineaged' or list or set, optional
        The list of ids to consider, all of them by default.
    outliers_cluster_id : int, optional
        Label to use with the outliers cluster, if any. It will be excluded from the analysis. Defaults to `-1`.
    outlier_scaler : func, optional
        A function to apply to the feature array to transform it, useful for scaling.
        By defaults, no scaling is performed.

    Attributes
    ----------
    graph : timagetk.graphs.TemporalTissueGraph
        The temporal tissue graph containing the cell features to use for clustering.
    standardisation_method : {"L1", "L2"}
        The standardisation metric to apply to the data, "L1" by default.
    ids : set
        The list of time-indexed cell ids tuples (tp, cell_id) from the graph.
        It is used to construct "variable vectors".
    outlier_scaler : func or None
            A function to apply to the feature array to transform it, useful for scaling.
    _variable_vector_info: dict
        Variable vector dictionary by name.
        Used to gather `ids` ordered list of values and compute distance matrix.
    _variable_vector_info: dict
        Variable vector information dictionary by name.
    _distance_matrix_dict: dict
        Pairwise distance matrix dictionary by name.
        Not standardized and with all `ids`.
    _global_pw_dist_mat : numpy.ndarray
        Global standardized pairwise distance matrix.
        Reduced to the list of `_global_pw_dist_ids` ids.
        Used a similarity matrix for clustering.
    _global_pw_dist_ids : list
        Ids used to build the global standardized pairwise distance matrix `_global_pw_dist_mat`.
        This corresponds to the list of ids `self.ids` excluding the outlier ids ``self._outliers``.
    _global_pw_dist_weights : list
        Ordered list of weights used in global distance matrix assembly (call to `assemble_matrix`).
    _global_pw_dist_weight_mat : dict
        Dictionary of weight matrices used in global distance matrix assembly (call to `assemble_matrix`).
    _global_pw_dist_variables : list
        Ordered list of variable names used in global distance matrix assembly (call to `assemble_matrix`).
    _global_pw_dist_name : str
        Auto generated name of the global pairwise distance matrix.
        Built from used weights and variables used during global distance matrix assembly (call to `assemble_matrix`).
    _method : str
        Name of the used clustering method, only defined after clustering was performed (call to `compute_clustering`).
    _nb_clusters : int or str
        Number of required cluster in case of a supervised clustering algorithm.
        In case of unsupervised clustering algorithm should be "auto".
    _clustering : dict
        Clustering dictionary indexed by time-indexed cell ids tuples ``(tp, cell_id)``.
        Do NOT contain outliers excluded by ``self.detect_outlier_cells``.
    _outlier_ids : set
        Cell ids ``(tp, cell_id)`` detected as outliers by ``self.detect_outlier_cells``.
    _lof_score : list
        Outlier scores (local outlier factor), if computed by ``self.detect_outlier_cells``, sorted as ``self.ids``.
    _outliers_mgt : str
        String indicating how outliers were handled prior to clustering.
        If "" (empty string), the detection of outliers was not taken into account during the creation of the global standardized pairwise distance matrix.
        If "ignored", the outliers values were unused during the standardisation process, but are kept to build the global standardized pairwise distance matrix.
        If "deleted", the outliers were removed from the list of ids used to create the global standardized pairwise distance matrix.

    See Also
    --------
    timagetk.components.clustering.SUPERVISED_CLUSTERING_METHODS : List of supervised clustering methods.
    timagetk.components.clustering.UNSUPERVISED_CLUSTERING_METHODS : List of unsupervised clustering methods.
    timagetk.components.clustering.STANDARDIZATION_METHODS : List of data standardisation methods.

    Examples
    --------
    >>> from timagetk.components.clustering import example_clusterer
    >>> clust = example_clusterer("p58")
    >>> clust.assemble_matrix(['topological', 'area', "log_relative_value('volume',1)"], [0.2, 0.4, 0.4])
    >>> clust.plot_feature_distance_matrix('topological')
    >>> clust.plot_feature_distance_matrix('area')
    >>> clust.compute_clustering(method='dbscan', eps=5)
    >>> clust._clustering

    >>> from timagetk.components.clustering import FeatureClusterer
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph
    >>> ttg = example_temporal_tissue_graph("p58", features='all', wall_features='all', temp_features='all')
    >>> clust = FeatureClusterer(ttg, ids='lineaged')
    >>> clust.get_topological_distance_matrix()
    >>> clust.add_spatial_variable('area', 'numeric')
    >>> clust.add_spatial_variable('volume', 'numeric')
    >>> clust.add_temporal_variable("log_relative_value('volume',1)", 'numeric')
    >>> clust.detect_outlier_cells()
    >>> clust.assemble_matrix(['topological', 'area', "log_relative_value('volume',1)"], [0.2, 0.4, 0.4])
    >>> clust.plot_feature_distance_matrix('topological')
    >>> clust.plot_feature_distance_matrix('area')
    >>> clust.compute_clustering(method='dbscan', eps=5)
    >>> clust._clustering
    """

    def __init__(self, graph, standardisation_method="L1", ids=None, outliers_cluster_id=-1, outlier_scaler=None):

        # -- Variables saving information:
        self._variable_vector_info = {}  # dictionary of property indexed variable information (dimension, type, unit)
        self._standardisation_method = None

        # -- Variables for caching information:
        self._ids = None  # ordered list of INITIAL cell ids (used to get feature values)
        # Attributes linked to standardized data:
        self._distance_matrix_dict = {}  # dictionary of property indexed distance matrices, each is ordered as `self.ids`
        self._global_pw_dist_mat = None  # global distance matrix
        self._global_pw_dist_ids = None  # ordered list of cell ids used in global distance matrix assembly
        self._global_pw_dist_weights = None  # ordered list of weights used in global distance matrix assembly
        self._global_pw_dist_weight_mat = {}  # dictionary of weight matrices used in global distance matrix assembly
        self._global_pw_dist_variables = None  # ordered list of variable names used in global distance matrix assembly
        self._global_pw_dist_name = ""  # name of the global pairwise distance matrix
        # Attributes linked to clustering:
        self._method = None  # str
        self._nb_clusters = None  # int
        self._clustering = None  # dict ``{cid: cluster_id}``, without ``self._outlier_ids``
        self._clustering_params = None
        self._outliers_mgt = ""
        # Attributes linked to outliers:
        self._outlier_ids = None  # list of ids detected as outliers (time-indexed cell ids)
        self._lof_score = None  # list of lof scores, ordered as `self.ids`

        # -- Initialisation:
        self.graph = graph
        self.outliers_cluster_id = outliers_cluster_id  # label to use as outlier cluster
        self.outlier_scaler = outlier_scaler  # function applied to data prior to outlier detection
        # Initialize the list of used ids using the setter:
        self.ids = ids
        # Initialize the standardisation method using the setter:
        if standardisation_method is not None:
            self.standardisation_method = standardisation_method

    def get_feature_dimension(self, name):
        """Return the feature dimension, if it has been previously registered to the instance.

        Parameters
        ----------
        name : str
            The name of the cell feature to get.

        Returns
        -------
        str
            The feature dimension.
        """
        try:
            feat_dim = self._variable_vector_info[name][0]
        except IndexError:
            feat_dim = ""
        return feat_dim

    def get_feature_type(self, name):
        """Return the feature type, if it has been previously registered to the instance.

        Parameters
        ----------
        name : str
            The name of the cell feature to get.

        Returns
        -------
        str
            The feature type.
        """
        try:
            feat_type = self._variable_vector_info[name][1]
        except IndexError:
            feat_type = ""
        return feat_type

    def get_feature_unit(self, name):
        """Return the feature unit, if it has been previously registered to the instance.

        Parameters
        ----------
        name : str
            The name of the cell feature to get.

        Returns
        -------
        str
            The feature unit.
        """
        try:
            feat_unit = self._variable_vector_info[name][2]
        except IndexError:
            feat_unit = ""
        return feat_unit

    @property
    def standardisation_method(self):
        """Get the used standardisation method."""
        return copy.copy(self._standardisation_method)

    @standardisation_method.setter
    def standardisation_method(self, value):
        """Set the standardisation method to use.

        Parameters
        ----------
        value : {"L1", "L2"}
            The name of the standardisation method to use.

        Raises
        ------
        ValueError
            If the given `value` is not in the list of valid standardisation methods ``STANDARDIZATION_METHODS``.

        See Also
        --------
        timagetk.components.clustering.STANDARDIZATION_METHODS : List of data standardisation methods.
        """
        try:
            assert value in STANDARDIZATION_METHODS
        except AssertionError:
            raise ValueError(f"Standardisation method should be in {STANDARDIZATION_METHODS}, got '{value}'!")

        # RESET cached attribute linked to standardized pairwise distance matrix and clustering
        if self._standardisation_method is not None and value != self._standardisation_method:
            # Clean cached attribute linked to standardized data:
            self._global_pw_dist_mat = None
            self._global_pw_dist_ids = None
            self._global_pw_dist_weights = None
            self._global_pw_dist_weight_mat = {}
            self._global_pw_dist_variables = None
            self._global_pw_dist_name = ""
            # Clean cached attribute linked to clustering:
            self._method = None
            self._nb_clusters = None
            self._clustering = None
            self._clustering_params = None
            self._full_tree = None

        self._standardisation_method = value
        return

    @property
    def ids(self):
        """Get the list of element ids to use."""
        return copy.copy(self._ids)

    @ids.setter
    def ids(self, ids):
        """Set the list of element ids to use when building the pairwise distance matrix.

        Parameters
        ----------
        ids : set, list, tuple or numpy.ndarray
            If ``None`` or ``'all'``, use all ids available in the graph.
            If a list of string, they are considered to be identities to find in the graph.
            This can also include ``'forward_lineaged'`` to include only cells that have at least one ancestor.
            This can also include ``'layer_*'`` to include only cells belonging to the specified layers.
            For example ``'layer_1_2'`` will include only cells that are in the first two layers.

        Raises
        ------
        ValueError
            If the set of element ids is empty after intersection.

        Examples
        --------
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> clust.n_elements()
        4530
        >>> clust.ids = ['FloralMeristem', 'layer_1_2']
        >>> clust.n_elements()
        1795
        """
        # Get the set of all possible cell ids:
        all_ids = self.graph.cell_ids()

        # Make sure the set of cell ids is defined (i.e. not None):
        if self._ids is None:
            self._ids = all_ids

        # Define or filter the set of cell ids to use:
        if ids is None or ids == "all":
            # Case where we want to use all cell ids:
            self._ids = all_ids
        elif isinstance(ids, (set, list, tuple)) and all(isinstance(i, str) for i in ids):
            # Case where the list of ids is a list of cell identity to find in the graph.
            self._ids = self.graph.cell_ids(identity=ids)
        elif isinstance(ids, (set, list, tuple, np.ndarray)):
            # Case where the list of ids is a list of cell ids to find in the graph.
            unknown_ids = set(ids) - set(all_ids)
            if unknown_ids:
                log.warning(f"Got {len(unknown_ids)} unknown ids, they were excluded from the list of ids.")
            self._ids = set(ids) - unknown_ids
        else:
            raise TypeError(f"Do not know what to do with provided `ids`: {ids}")

        try:
            assert len(self._ids) != 0
        except AssertionError:
            raise ValueError("The set of element ids is EMPTY!")
        else:
            log.info(f"There is now {len(self._ids)} ids selected!")
            # If the list of ids is modified, we have to RESET some of the attributes:
            self._reset_outliers_attributes()
            self._reset_pw_attributes()
            self._reset_clustering_attributes()

        return

    def _reset_outliers_attributes(self):
        """Reset the hidden attributes related to outliers."""
        self._outlier_ids = None
        self._lof_score = None
        return

    def _reset_pw_attributes(self):
        """Reset the hidden attributes related to pairwise distances."""
        self._distance_matrix_dict = {}
        self._global_pw_dist_mat = None
        self._global_pw_dist_ids = None
        self._global_pw_dist_weights = None
        self._global_pw_dist_weight_mat = {}
        self._global_pw_dist_variables = None
        self._global_pw_dist_name = ""
        self._outliers_mgt = ""
        return

    def _reset_clustering_attributes(self):
        """Reset the hidden attributes related to clustering."""
        self._method = None
        self._nb_clusters = None
        self._clustering = None
        self._clustering_params = None
        self._full_tree = None
        return

    def exclude_ids(self, ids):
        """Exclude given element ids from the list of element ids to use when building the pairwise distance matrix.

        Parameters
        ----------
        ids : list
            If ``None``, use all ids available in the graph.
            If a list of string, they are considered to be identities to find in the graph.
            This can also include 'forward_lineaged' to include only cells that have at least one ancestor.
            This cal also include 'layer_*' to include only cells belonging to the specified layers.
            For example 'layer_1_2' will include only cells that are in the first two layers.

        Raises
        ------
        ValueError
            If the set of element ids is empty after exclusion.

        Examples
        --------
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> clust.n_elements()
        4530
        >>> clust.graph.list_identities()
        ['epidermis', 'FloralMeristem']
        >>> clust.exclude_ids('epidermis')
        >>> clust.n_elements()
        2930
        >>> clust.exclude_ids('stack_margin')  # should raise a critical logging message
        """
        if isinstance(ids, str):
            ids = [ids]

        if isinstance(ids, (set, list, tuple)) and all(isinstance(i, str) for i in ids):
            # Case where the list of ids is a list of cell identity to find in the graph.
            exclude_ids = set([])
            for identity in ids:
                i_ids = self.graph.cell_ids(identity=identity)
                if i_ids != {}:
                    log.info(f"Excluding {len(i_ids)} elements with identity: '{identity}'!")
                    exclude_ids |= i_ids
                else:
                    log.warning(f"No elements with identity: '{identity}'!")
            self.ids = self.ids - exclude_ids
        elif isinstance(ids, (set, list, tuple, np.ndarray)):
            # Case where the list of ids is a list of cell ids to find in the graph.
            log.info(f"Excluding {len(ids)} elements using a given list of ids!")
            self.ids = self.ids - set(ids)
        else:
            raise TypeError(f"Do not know what to do with provided `ids`: {ids}")

        try:
            assert len(self.ids) != 0
        except AssertionError:
            raise ValueError("The set of element ids is EMPTY after excluding ids!")
        return

    def n_elements(self) -> int:
        """Return the number of initialized elements."""
        return len(self.ids)

    def _get_feature_values(self, feat_name, ids):
        """Returns the feature values and used ids as two ordered vectors for a given feature name.

        Parameters
        ----------
        feat_name : str
            Name of the feature to get from the temporal tissue graph (in `self.graph.cell_property`).
        ids : list of 2-tuples
            A list of time-indexed cell ids to retrieve the feature values.

        Returns
        -------
        list
            The ordered vector of feature from the temporal tissue graph.

        Raises
        ------
        ValueError
            If the 'spatial' `ppty` is not found in ``self.graph.list_cell_properties()``.
            If the 'temporal' `ppty` is not found in ``self.graph.list_cell_properties()`` or could not be computed.
        """
        feat_dim = self.get_feature_dimension(feat_name)

        default = np.nan  # default value to return when accessing the cell property from the graph
        # Check if it's a decomposed feature:
        col = None
        if feat_name[-2:] in [f"_{d}" for d in DECOMP_SUFFIXES]:
            col = DECOMP_SUFFIXES.index(feat_name[-1])  # get the column index to return
            feat_name = feat_name[:-2]  # remove the suffix
            dim = int(self._check_variable(feat_name)[-1])  # get the vector dimension
            default = [default] * dim  # Change it to match dimension of vector

        if feat_dim == 'spatial':
            if feat_name in self.graph.list_cell_properties():
                if isinstance(self.graph, TissueGraph):
                    variable_vector = [self.graph.cell_property(cid, feat_name, default=default) for cid in ids]
                else:
                    variable_vector = [self.graph.cell_property(tp, cid, feat_name, default=default) for (tp, cid) in ids]
            else:
                raise ValueError(f"Could not find spatial property '{feat_name}' in the graph!")
        elif feat_dim == 'temporal':
            try:
                assert isinstance(self.graph, TemporalTissueGraph)
            except AssertionError:
                raise TypeError(f"Only `TemporalTissueGraph` may have temporal properties, got `{clean_type(self.graph)}`!")
            if feat_name in self.graph.list_cell_properties():
                variable_vector = [self.graph.cell_property(tp, cid, feat_name, default=default) for (tp, cid) in ids]
            else:
                # If the feature name is not found as a cell property on the graph, try to compute it on the fly:
                try:
                    var_dict = eval(f"self.graph.{feat_name}")
                except:
                    raise ValueError(f"Could not find or evaluate temporal property '{feat_name}' in the graph!")
                else:
                    variable_vector = [var_dict[tcid] if tcid in var_dict else default for tcid in ids]
        else:
            raise ValueError(
                f"Unknown dimension '{feat_dim}' for feature '{feat_name}', should be either 'spatial' or 'temporal'!")

        if col is not None:
            # Get the column corresponding to the dimension axis:
            variable_vector = list(np.array(variable_vector)[:, col])

        return variable_vector

    def add_property_from_dictionary(self, ptty_dict, name, var_type='numeric', ppty_type='spatial', unit=""):
        """Register a given cell indexed scalar property dictionary to the graph.

        Parameters
        ----------
        ptty_dict : dict, optional
            Cell indexed dictionary of scalar property values.
        name : str
            Name of a spatial cell property in the graph.
        var_type : {"numeric", "ordinal"}, optional
            Specify the mathematical type of the variable, this is required for the standardisation step.
            Defaults to 'numeric'.
        ppty_type : {'spatial', 'temporal'}, optional
            The type of property, defaults to 'spatial'.
            If a 'spatial' property, `ppty` should be a cell property in the tissue graph.
            If a 'temporal' property, `ppty` can also be a *temporal method* to compute.
        unit : str, optional
            Unit (dimension) to give to the variable, used in properties boxplot.

        Examples
        --------
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> # Add a dummy cell property named 'foo' to the TPG, contains a single value for 'tcid' (0, 25):
        >>> clust.add_property_from_dictionary({(0, 25): 25.}, 'foo', unit="A.U.")
        >>> clust._variable_vector_info
        {'foo': ('spatial', 'numeric', 'A.U.')}
        """
        if name in self._variable_vector_info:
            log.warning(f"You already have a variable vector for '{ppty_type}' property '{name}'!")
        else:
            log.info(f"Importing the dictionary as '{name}' cell property the ...")
            self.graph.add_cell_property(name, ptty_dict, unit=unit)
            self._variable_vector_info[name] = (ppty_type.lower(), var_type.lower(), unit)
        return

    def _check_variable(self, name):
        """Check the variable in the graph."""
        if not name in self.graph.list_cell_properties():
            log.warning(f"The requested variable '{name}' can not be found in the graph!")
            return None

        ppty_dict = self.graph.cell_property_dict(name, default=None, only_defined=True)
        if len(ppty_dict) == 0:
            log.warning(f"The requested variable '{name}' returned an empty dictionary!")
            return None

        ppty_value = list(ppty_dict.values())[0]
        if isinstance(ppty_value, (int, float)):
            return "scalar"
        elif isinstance(ppty_value, (str)):
            return "ordinal"
        elif isinstance(ppty_value, Iterable):
            ppty_value = np.array(ppty_value)
            if ppty_value.ndim == 1 and ppty_value.shape[0] in (2,3):
                return f"vector{ppty_value.shape[0]}"
            elif ppty_value.ndim == 2 and (all([sh == 2 for sh in ppty_value.shape]) or all([sh == 3 for sh in ppty_value.shape])):
                return f"tensor{ppty_value.shape[0]}"

        log.critical(f"Could not make sense of property '{name}' from first non-null value: {ppty_value}!")
        return None

    @staticmethod
    def _decompose_name(name, dim):
        """Decompose a variable name according to dimension by appending a suffix.

        Parameters
        ----------
        name : str
            Name of the variable to decompose.
        dim : {2, 3}
            Dimension of the variable, should be either 2 or 3

        Returns
        -------
        list of str
            The list of names.

        Examples
        --------
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> clust._decompose_name("norm", 3)  # a 3D norm vector
        ['norm_i', 'norm_j', 'norm_k']
        """
        return [f"{name}_{i}" for i in DECOMP_SUFFIXES[:dim]]

    def add_spatial_variable(self, name, var_type='numeric', unit=None):
        """Register selected spatial cell property from the graph to the object.

        Parameters
        ----------
        name : str
            Name of a cell property in the graph.
        var_type : {"numeric", "ordinal"}, optional
            Specify the mathematical type of the variable, this is required for the standardisation step.
            Defaults to 'numeric'.
        unit : str, optional
            Unit (dimension) to give to the variable. Specify it to override the one in the tissue graph.

        Examples
        --------
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> clust.add_spatial_variable('area')
        >>> clust.add_spatial_variable('principal_direction_norms')
        >>> clust.list_features()
        ['area', 'principal_direction_norms_i', 'principal_direction_norms_j', 'principal_direction_norms_k']
        """
        var = self._check_variable(name)
        if var is None:
            return  # nothing to do here, a message have been issued by `_check_variable`...

        # Try to guess the property unit using the tissue graph:
        if unit is None:
            try:
                unit = self.graph._tissue_graph[0]._cell_properties[name]
            except:
                unit = ""

        if var == "scalar":
            self._add_spatial_variable(name, var_type, unit)
        elif var.startswith("vector"):
            dim = int(var[-1])
            var = var[:-1]
            log.info(f"Variable '{name}', has been detected has a {dim}D-{var}.")
            name = self._decompose_name(name, dim)
            [self._add_spatial_variable(vname, var_type, unit) for vname in name]
        else:
            log.error(f"Not sure what to do with variable {name} because we detected it as a {var}...")
        return

    def _add_spatial_variable(self, name, var_type, unit) -> None:
        if name in self._variable_vector_info:
            log.warning(f"You already have a variable vector for spatial property '{name}'!")
        else:
            self._variable_vector_info[name] = ('spatial', var_type.lower(), unit)
        return

    def add_temporal_variable(self, name, var_type='numeric', unit=None):
        """Register selected temporal cell property from the graph to the object.

        Parameters
        ----------
        name : str
            Name of a cell property in the graph. It can also be a *temporal method* to compute.
        var_type : {"numeric", "ordinal"}, optional
            Specify the mathematical type of the variable, this is required for the standardisation step.
            Defaults to 'numeric'.
        unit : str, optional
            Unit (dimension) to give to the variable. Specify it to override the one in the tissue graph.

        Examples
        --------
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> clust.add_temporal_variable("division_rate(1)")  # register an existing temporal cell property
        >>> clust.add_temporal_variable("log_relative_value('volume',1)")  # compute and register
        >>> clust.list_features()
        ['division_rate(1)', "log_relative_value('volume',1)"]
        """
        var = self._check_variable(name)
        if var is None:
            return  # nothing to do here, a message have been issued by `_check_variable`...

        # Try to guess the property unit using the tissue graph:
        if unit is None:
            try:
                unit = self.graph._tissue_graph[0]._cell_properties[name]
            except:
                unit = ""

        if var == "scalar":
            self._add_temporal_variable(name, var_type, unit)
        elif var.startswith("vector"):
            dim = int(var[-1])
            var = var[:-1]
            log.info(f"Variable '{name}', has been detected has a {dim}D-{var}.")
            name = self._decompose_name(name, dim)
            [self._add_temporal_variable(vname, var_type, unit) for vname in name]
        else:
            log.error(f"Not sure what to do with variable {name} because we detected it as a {var}...")
        return

    def _add_temporal_variable(self, name, var_type, unit) -> None:
        if name in self._variable_vector_info:
            log.warning(f"You already have a variable vector for temporal property '{name}'!")
        else:
            self._variable_vector_info[name] = ('temporal', var_type.lower(), unit)
        return

    def remove_feature(self, feature) -> None:
        self._variable_vector_info.pop(feature)
        return

    def list_features(self) -> list:
        """List the features available to the instance, that is those added with the `add_*` methods."""
        return list(self._variable_vector_info.keys())

    def _filter_outliers(self):
        """Return the list of ids without the outliers."""
        ids = self.ids
        if self._outlier_ids is None:
            log.warning("Requested to exclude outliers while not defined yet! Use `detect_outlier_cells()` method.")
        else:
            ids -= self._outlier_ids
        return ids

    def get_topological_distance_matrix(self, ids=None, exclude_outliers=False):
        """Compute the topological distance matrix based on the graph.

        Parameters
        ----------
        ids : list of 2-tuples, optional
            A list of time-indexed cell ids to retrieve the feature values.
            Default to the whole list of ids (``self.ids``), potentially filtered of outliers.
        exclude_outliers : bool, optional
            Whether to exclude the outliers from the list of ids, only if `ids` is `None`.

        Examples
        --------
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> clust.n_elements()
        4530
        >>> dist_arr = clust.get_topological_distance_matrix()
        >>> dist_arr.shape
        (4530, 4530)
        """
        if ids is None:
            if exclude_outliers:
                ids = self._filter_outliers()
            else:
                ids = self.ids

        dist_arr = topological_distance_matrix(self.graph, ids)
        self._variable_vector_info["topological"] = ('spatial', "ordinal", "")
        return dist_arr

    def get_euclidean_distance_matrix(self, ids=None, exclude_outliers=False):
        """Compute the Euclidean distance matrix based on the graph.

        Parameters
        ----------
        ids : list of 2-tuples, optional
            A list of time-indexed cell ids to retrieve the feature values.
            Default to the whole list of ids (``self.ids``), potentially filtered of outliers.
        exclude_outliers : bool, optional
            Whether to exclude the outliers from the list of ids, only if `ids` is `None`.

        Examples
        --------
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> clust.n_elements()
        4530
        >>> dist_arr = clust.get_euclidean_distance_matrix()
        >>> dist_arr.shape
        (4530, 4530)
        """
        if ids is None:
            if exclude_outliers:
                ids = self._filter_outliers()
            else:
                ids = self.ids

        # Try to guess the distance unit using the "barycenter" property from the temporal tissue graph:
        try:
            unit = self.graph._tissue_graph[0]._cell_properties["barycenter"]
        except:
            unit = ""

        dist_arr = euclidean_distance_matrix(self.graph, ids)
        self._variable_vector_info["euclidean"] = ('spatial', "numeric", unit)
        return dist_arr

    def _check_features(self, feature_names, dimension_filter=None):
        """Filter the feature names against those registered with an 'add_*' method.

        Parameters
        ----------
        feature_names : list
            The list of feature names to pre-process.
        dimension_filter : list or None, optional
            If set, should be a list of feature dimensionalities to keep.
            For example, use ``["spatial", "temporal"]`` to keep only those that are either spatial or temporal ones.

        Returns
        -------
        list
            The list of available features, matching the "dimensionality filter" if any.
        """
        # Transform a single feature name into a list of 1 feature name:
        if isinstance(feature_names, str):
            feature_names = [feature_names]

        all_feature_names = set(self.list_features())
        if feature_names is None:
            feature_names = self.list_features()
        else:
            unknown_feature_names = set(feature_names) - all_feature_names
            feature_names = set(feature_names) & all_feature_names
            if len(unknown_feature_names) != 0:
                for fname in unknown_feature_names:
                    log.critical(f"Unknown feature '{fname}', add it with one of the `add_*` method first!")

        if dimension_filter is not None:
            feature_names = [name for name in feature_names if self.get_feature_dimension(name) in dimension_filter]

        return feature_names

    def get_features_array(self, names=None, exclude_outliers=False):
        """Return an array made of selected feature values.

        Parameters
        ----------
        names : list of str
            List of feature names to use to create the array.
            By default, use all known variables (excluding topological and Euclidean distance matrices).
        exclude_outliers : bool, optional
            Whether to exclude the outliers from the list of ids.

        Returns
        -------
        numpy.ndarray
            The array with feature values, shape is `ids` x `names` (lines x columns).
        list of str
            The ordered list of ids used to create the features array (lines).
        list of str
            The ordered list of names used to create the features array (columns).

        Examples
        --------
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> clust.add_spatial_variable('area')
        >>> clust.add_spatial_variable('principal_direction_norms')
        >>> arr, ids, names = clust.get_features_array(['area', 'principal_direction_norms_i'])
        >>> print(arr.shape)  # only the first element of the 'principal_direction_norms' vector was kept!
        (4530, 2)
        """
        # Get the names of the feature to use to build the array:
        names = self._check_features(names, dimension_filter=["spatial", "temporal"])
        log.info(f"Extracting {len(names)} features as array: {', '.join(names)}")

        if exclude_outliers:
            ids = self._filter_outliers()
        else:
            ids = self.ids

        return np.array([self._get_feature_values(name, ids) for name in names]).T, ids, names

    def get_features_df(self, names=None, exclude_outliers=False):
        """Return a DataFrame made of selected feature values.

        Parameters
        ----------
        names : list of str
            List of feature names to use to create the DataFrame.
            By default, use all known variables (excluding topological and Euclidean distance matrices).
        exclude_outliers : bool, optional
            Whether to exclude the outliers from the list of ids.

        Returns
        -------
        pandas.DataFrame
            The table with feature values, shape is `ids` x `names` (lines x columns).

        Examples
        --------
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> clust.add_spatial_variable('area')
        >>> clust.add_spatial_variable('principal_direction_norms')
        >>> df = clust.get_features_df(['area', 'principal_direction_norms_i'])
        >>> print(df.head())
                                  area  principal_direction_norms_i
        time-index cell_id
        2          567       63.892474                     1.867110
        1          751      158.265195                     4.122834
        2          338      176.565790                     3.996902
        0          1726      94.557670                     1.993177
        1          342      103.422019                     2.549807
        """
        import pandas as pd
        arr, ids, names = self.get_features_array(names, exclude_outliers)
        if isinstance(self.graph, TissueGraph):
            df = pd.DataFrame(arr, columns=names,
                            index=pd.Index(list(ids), name="cell_id"))[sorted(names)]
        else:
            df = pd.DataFrame(arr, columns=names,
                            index=pd.MultiIndex.from_tuples(list(ids), names=["time-index", "cell_id"]))[sorted(names)]
        return df

    def find_missing_values(self, names=None, n=0):
        """Find the elements that have missing values for the given list of variables.

        Parameters
        ----------
        names : list of str
            List of property names to use to find missing values.
            By default, use all known variables (excluding topological and Euclidean distance matrices).
        n : int, optional
            The number of allowed missing values.
            Defaults to `0`.

        Returns
        -------
        list
            Elements with a maximum of `n` missing value.

        Examples
        --------
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> print(len(clust.ids))
        4530
        >>> clust.add_spatial_variable("area")
        >>> clust.add_spatial_variable("volume")
        >>> nan_ids = clust.find_missing_values(["area", "volume"])
        >>> print(len(nan_ids))
        0
        >>> clust.add_temporal_variable("log_relative_value('volume',1)")
        >>> nan_ids = clust.find_missing_values(["volume", "log_relative_value('volume',1)"])
        >>> print(len(nan_ids))
        3276
        >>> clust.get_topological_distance_matrix()
        >>> nan_ids = clust.find_missing_values(["volume", "topological"])
        >>> print(len(nan_ids))
        WARNING  [timagetk.components.clustering] Topological and Euclidean distances cannot be used to find NaNs!
        0
        """
        # Create the array with feature values, shape is `ids` x `names` (lines x columns)
        arr, ids, names = self.get_features_array(names)
        return set(find_missing_values(arr, ids, n, axis=1))

    def detect_outlier_cells(self, names=None, scaler=None, **kwargs):
        """Unsupervised detection of outliers using 'Local Outlier Factor' (LOF).

        Parameters
        ----------
        names : list of str, optional
            List of feature names to use to detect outliers.
            By default, use all declared features (excluding topological and Euclidean distance matrices).
        scaler : func, optional
            A function to apply to the feature array to transform it, useful for scaling.
            By defaults, no scaling is performed.

        Returns
        -------
        set
            The set of cell ids ``(tp, cell_id)`` that have been declared 'outliers'.

        See Also
        --------
        sklearn.neighbors.LocalOutlierFactor

        Notes
        -----
        Keyword argument are passed to `LocalOutlierFactor`.
        The ids with missing values are defined as outliers!

        Examples
        --------
        >>> import numpy as np
        >>> from sklearn.preprocessing import RobustScaler
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> clust.add_spatial_variable('volume')
        >>> clust.add_temporal_variable("log_relative_value('volume',1)")
        >>> # Detect the outliers using LOF on quantile scaled and median centered data:
        >>> scaler = RobustScaler(quantile_range=(10., 90.)).fit_transform
        >>> outlier_ids = clust.detect_outlier_cells(scaler=scaler)
        >>> print(f"Detected {len(outlier_ids)} ids as outliers!")
        >>> outliers_score = clust._lof_score  # access predicted outliers scores
        >>> # Plot the LOF scores on a 2D scatter plot made of the "volume" and "log_relative_value('volume',1)"
        >>> # Note that the cells without a lineage will not appear here as they have `np.nan` as value for "log_relative_value('volume',1)"
        >>> from matplotlib import pyplot as plt
        >>> names = list(clust._variable_vector_info.keys())
        >>> X, ids, names = clust.get_features_array(names, exclude_outliers=False)
        >>> bool_outliers = [i in outlier_ids for i in ids]
        >>> plt.title("Outliers detection by LOF")
        >>> plt.scatter(X[:, 0], X[:, 1], c=bool_outliers, s=3., label='Data points')
        >>> plt.xlabel(names[0])
        >>> plt.ylabel(names[1])
        >>> plt.show()
        """
        from timagetk.features.utils import lof_array

        # Create the array with feature values, shape is `ids` x `names` (lines x columns)
        arr, ids, names = self.get_features_array(names, exclude_outliers=False)
        # Standardize the features (scaling & centering) so they are comparable for a density estimator (LOF)
        if scaler is None:
            scaler = self.outlier_scaler  # try to get the "default scaler"
        if scaler is not None:
            arr = scaler(arr)
        # Now detect the outlier elements based on their feature values:
        self._outlier_ids, self._lof_score = lof_array(arr, ids, **kwargs)
        # NOTE that the ids with missing values are defined as outliers by the `lof_array` function!

        return self._outlier_ids

    def _requested_distance_matrix(self, names, ids):
        """Compute the required pairwise distance matrix to build the global pairwise distance matrix.

        Parameters
        ----------
        names : list of str
            List of feature names required to compute the global pairwise distance matrix.
        ids : list of 2-tuples
            A list of time-indexed cell ids to retrieve the feature values.

        Raises
        ------
        KeyError
            If one of the requested feature name is not known.
        """
        for name in names:
            if name not in self._distance_matrix_dict:
                if name == 'topological':
                    self._distance_matrix_dict[name] = self.get_topological_distance_matrix(ids)
                elif name == 'euclidean':
                    self._distance_matrix_dict[name] = self.get_euclidean_distance_matrix(ids)
                elif name in self._variable_vector_info:
                    # Compute the distance matrix from the variable vector now:
                    v_type = self.get_feature_type(name)
                    self._distance_matrix_dict[name] = distance_matrix_from_vector(self._get_feature_values(name, ids),
                                                                                   v_type)
                else:
                    raise KeyError(f"Feature '{name}' is unknown!")

    def assemble_matrix(self, names, weights, ignore_outliers=True, delete_outliers=False, **kwargs):
        """Creating the global weighted distance matrix.

        Provided `names` should exist in ``self._variable_vector_info``.

        Parameters
        ----------
        names: list of str
            List of feature names in ``self._variable_vector_info`` to combine.
        weights: list of float
            List of feature ordered weights used to create the global weighted distance matrix.
        ignore_outliers: bool, optional
            If ``True`` ignore outliers when computing standardisation value.
        delete_outliers: bool, optional
            If ``True`` delete outliers (values set to ``np.nan``) of pairwise distance matrix.

        Other Parameters
        ----------------
        standardisation_method : {"L1", "L2"}
            The standardisation method to use.
        scaler : func
            A scaling function to apply to the data prior to outliers detection.

        Examples
        --------
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> clust.ids = ['FloralMeristem', 'layer_1_2']  # select `FM` cells in L1+L2
        >>> clust.n_elements()
        >>> clust.add_spatial_variable('volume')
        >>> clust.add_temporal_variable("log_relative_value('volume',1)")
        >>> # Example #1 - Assemble a pairwise distance matrix with topological data
        >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5])
        >>> # You can check the re-weighting for missing data went fine with:
        >>> import numpy as np
        >>> arr = np.nansum(list(clust._global_pw_dist_weight_mat.values()), axis=0)
        >>> np.unique(arr)  # should only contain the value `1.`
        >>> # Example #2 - Use `Fractions` as weights:
        >>> from fractions import Fraction
        >>> clust.assemble_matrix(['topological', 'area', "log_relative_value('volume',1)"], [Fraction("1/3")]*3)
        >>> clust.list_gpdm_weights()
        """
        names = [name.lower() for name in names]  # make sure all names are lower cases
        assert len(names) == len(weights)
        assert fsum(weights) == 1.

        # - Check the presence of all requested variables names in the dictionary `self._distance_matrix_dict`:
        self._requested_distance_matrix(names, self.ids)
        # We create the feature indexed distance matrix dictionary with `self.ids`

        if 'standardisation_method' in kwargs:
            self.standardisation_method = kwargs.get('standardisation_method')

        # - Detect if we have 'topological' or 'Euclidean' distance
        spatial_relation_data = False
        if 'topological' in names or 'euclidean' in names:
            spatial_relation_data = True

        # -- Outliers --
        # If both are True, keep `delete_outliers`
        if delete_outliers and ignore_outliers:
            log.warning("Both `ignore_outliers` & `delete_outliers` are `True`!")
            ignore_outliers = False
            log.info("Changing `ignore_outliers` to `False`!")

        outlier_ids = []
        if ignore_outliers or delete_outliers:
            # If outlier management is required...
            if self._outlier_ids is None:
                # Automatically detect the outliers if not done yet:
                outlier_ids = self.detect_outlier_cells(names, scaler=kwargs.get('scaler', None))
            else:
                # Else use previously determined list of outliers
                outlier_ids = self._outlier_ids

        # -- Creating the list of ids used to build the global pairwise distance matrix (GPDM):
        if ignore_outliers:
            # - Case where all known sample ids are used to build the GPDM, but the standardization exclude outliers:
            outliers_management = 'ignored'
            # Set of ids used to create the global pairwise distance matrix:
            ids = self.ids
            # Create the mask of outliers for standardization procedure:
            std_outliers = [i in outlier_ids for i in ids]
        elif delete_outliers:
            # - Case where only non-outlier sample ids are used to build & standardize the GPDM:
            outliers_management = 'deleted'
            # Set of ids used to create the global pairwise distance matrix:
            ids = self.ids - outlier_ids
            # Create the mask of outliers for standardization procedure:
            std_outliers = None  # as they have been removed of the list of ids
        else:
            # - Case where all known sample ids are used to build & standardize the GPDM:
            outliers_management = None
            # Set of ids used to create the global pairwise distance matrix:
            ids = self.ids
            # Create the mask of outliers for standardization procedure:
            std_outliers = None  # as we keep the whole list of ids

        # -- Shortcut when asking for the same result:
        same_weights = weights == self._global_pw_dist_weights
        same_vars = names == self._global_pw_dist_variables
        same_ids = ids == self._global_pw_dist_ids
        same_outliers = self._outliers_mgt == outliers_management
        if same_weights and same_vars and same_ids and same_outliers:
            log.info("The global pairwise distance matrix was already computed!")
            return
        else:  # Otherwise, clean any possible clustering:
            self._reset_clustering_attributes()

        # Ids index used to create the global pairwise distance matrix (with/without outlier):
        ids_index = [list(self.ids).index(v) for v in ids]

        # -- Standardization step:
        log.info(f"Standardizing the following pairwise distance matrix: {', '.join(names)}")
        sp_stand_dist_mat, tp_stand_dist_mat = {}, {}
        sp_weights, tp_weights = [], []
        nb_temp_var, nb_spa_var = 0, 0
        mat_topo_dist_standard = []
        topo_weight = 0
        for n, feat_name in enumerate(names):
            # Reduce the matrix to the list of selected ids `ids_index`
            distance_matrix = self._distance_matrix_dict[feat_name][ids_index, :][:, ids_index]
            # If the variable is topology (ranked or Euclidean):
            if (feat_name == 'topological') or (feat_name == 'euclidean'):
                mat_topo_dist_standard = standardisation(distance_matrix, self.standardisation_method,
                                                         outliers=std_outliers)
                topo_weight = weights[n]
            # If the variable is a temporal one:
            elif self.get_feature_dimension(feat_name) == 'temporal':
                tp_stand_dist_mat[nb_temp_var] = standardisation(distance_matrix, self.standardisation_method,
                                                                 outliers=std_outliers)
                tp_weights.append(weights[n])
                nb_temp_var += 1
            # If the variable is a spatial one:
            else:
                sp_stand_dist_mat[nb_spa_var] = standardisation(distance_matrix, self.standardisation_method,
                                                                outliers=std_outliers)
                sp_weights.append(weights[n])
                nb_spa_var += 1

        # -- Checking for simple cases: no re-weighting to do !
        # - Only 'topological' or 'Euclidean' distance asked:
        if spatial_relation_data and (nb_spa_var + nb_temp_var) == 0:
            log.critical("Only topological or euclidean distance requested!")
            log.info("Add spatial and/or temporal data to the list of variables!")
            return

        # - Creating list of weight, list of weight matrices and replacing nan by zeros for computation in standardized matrix.
        N = len(ids)
        weights_l, weight_mat_l, stand_mat_l = [], [], []
        # Starts with the 'topological' or 'Euclidean' data
        if spatial_relation_data:
            weights_l.append(topo_weight)
            weight_mat_l.append(create_weight_matrix(N, topo_weight, mat_topo_dist_standard))
            stand_mat_l.append(np.nan_to_num(mat_topo_dist_standard))
        # Then append the spatial data:
        for n in range(nb_spa_var):  # if no spatial data, noting will happen here!
            weights_l.append(sp_weights[n])
            weight_mat_l.append(create_weight_matrix(N, sp_weights[n], sp_stand_dist_mat[n]))
            stand_mat_l.append(np.nan_to_num(sp_stand_dist_mat[n]))
        # Then append the temporal data:
        for n in range(nb_temp_var):  # if no temporal data, noting will happen here!
            weights_l.append(tp_weights[n])
            weight_mat_l.append(create_weight_matrix(N, tp_weights[n], tp_stand_dist_mat[n]))
            stand_mat_l.append(np.nan_to_num(tp_stand_dist_mat[n]))

        log.info("Checking the weight matrices for missing values...")
        missing_weights_mat = {}
        for name, w, w_m in zip(names, weights_l, weight_mat_l):
            missing_weights_mat[name] = np.isnan(w_m) * float(w)
        log.info("Re-weighting matrices for missing values...")
        for idx, name in enumerate(names):
            missing_weights_sum = np.zeros(shape=[N, N], dtype=float)
            for w_name, w_mat in missing_weights_mat.items():
                if w_name != name:
                    missing_weights_sum += w_mat
            weight_mat_l[idx] = weight_mat_l[idx] / (np.ones_like(weight_mat_l[idx]) - missing_weights_sum)

        # -- Finally, making the weighted standardized pairwise distance matrix:
        # Defines the name of the global pairwise distance matrix by used weights, variables & outliers definition:
        sm_name = _standard_matrix_naming(weights, names, outliers_management)
        log.info(f"Creating the weighted standardized pairwise distance matrix: '{sm_name}'...")
        global_matrix = np.zeros(shape=[N, N], dtype=float)
        for wei_mat, standard_mat in zip(weight_mat_l, stand_mat_l):
            global_matrix += np.nan_to_num(wei_mat) * standard_mat

        # -- Update hidden attributes:
        self._global_pw_dist_ids = ids
        self._global_pw_dist_mat = global_matrix
        self._global_pw_dist_weight_mat = {names[n]: w_mat for n, w_mat in enumerate(weight_mat_l)}
        self._global_pw_dist_weights = weights_l
        self._global_pw_dist_variables = names
        self._global_pw_dist_name = sm_name
        self._outliers_mgt = outliers_management

        return

    def list_gpdm_weights(self) -> list:
        """List weights used to build the global pairwise distance matrix (GPDM)."""
        return copy.deepcopy(self._global_pw_dist_weights)

    def list_gpdm_features(self) -> list:
        """List features used to build the global pairwise distance matrix (GPDM)."""
        return copy.deepcopy(self._global_pw_dist_variables)

    def list_gpdm_ids(self) -> list:
        """List the ids that have been used to build the global pairwise distance matrix (GPDM)."""
        return copy.deepcopy(self._global_pw_dist_ids)

    def n_gpdm_ids(self) -> int:
        """Get the number of ids that have been used to build the global pairwise distance matrix (GPDM)."""
        return len(self.list_gpdm_ids())

    def _check_gpdm_features(self, feature_names) -> list:
        """Filter the feature names against those used to build the global pairwise distance matrix (GPDM)."""
        # Transform a single feature name into a list of 1 feature name:
        if isinstance(feature_names, str):
            feature_names = [feature_names]

        all_feature_names = set(self.list_gpdm_features())
        if feature_names is None:
            feature_names = all_feature_names
        else:
            unknown_feature_names = set(feature_names) - all_feature_names
            feature_names = set(feature_names) & all_feature_names
            if len(unknown_feature_names) != 0:
                for fname in unknown_feature_names:
                    log.critical(
                        f"Unknown feature '{fname}', it was not used to build the global pairwise distance matrix!")

        return list(feature_names)

    # -------------------------------------------------------------------------
    # Clustering methods
    # -------------------------------------------------------------------------

    def plot_dendrogram(self, linkage_method='ward', n_clusters=10, **kwargs):
        """Plot a dendrogram to visualize the cluster distances according to selected linkage criterion.

        Parameters
        ----------
        linkage_method : {'single', 'complete', 'average', 'weighted', 'centroid', 'median', 'ward'}
            The linkage method for the agglomerative clustering method.
        n_clusters : int
            The number of cluster to "cut" the dendrogram.

        Other Parameters
        ----------------
        figname : None or str
            If defined, should be a valid filename, used to save the figure.
            Defaults to ``None``.
        dpi : int
            The number of dot per inch (dpi) to use to create the figure. Defaults to ``96``.
        subplots_adjust : dict
            The dictionary indicating the adjustment to perform to the margins by ``plt.subplots_adjust``.

        Returns
        -------
        matplotlib.figure.Figure
            The `Figure` object.
        matplotlib.axes.Axes
            The `Axes` object.

        References
        ----------
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html
        """
        from scipy.cluster.hierarchy import dendrogram
        from scipy.cluster.hierarchy import linkage

        figname = kwargs.get('figname', None)
        # Matplotlib keyword arguments:
        dpi = kwargs.get('dpi', 96)
        subplots_adjust = kwargs.get('subplots_adjust', {'top': 0.9, 'wspace': 0.1})

        # Get the upper triangle indexes of the global matrix:
        upper_idx = np.triu_indices(self._global_pw_dist_mat.shape[0], k=1)
        # Apply Ward on condensed distance matrix:
        linkage_matrix = linkage(self._global_pw_dist_mat[upper_idx].flatten(), linkage_method)

        fig, axes = plt.subplots(ncols=2, figsize=(n_clusters * 2 / 3 * 2, 5), dpi=dpi)
        # plot the top levels of the dendrogram
        _ = dendrogram(linkage_matrix, ax=axes[0], truncate_mode="level", p=n_clusters)
        # plot the top levels of the dendrogram
        _ = dendrogram(linkage_matrix, ax=axes[1], truncate_mode="lastp", p=n_clusters)
        axes[0].set_ylabel("Distance")
        axes[0].grid(axis='y', linestyle='-.')
        axes[1].grid(axis='y', linestyle='-.')
        plt.suptitle("Hierarchical Clustering Dendrogram")
        plt.subplots_adjust(**subplots_adjust)

        if isinstance(figname, str):
            plt.savefig(figname)
            plt.close()

        return fig, axes

    @staticmethod
    def _compute_agglomerative(global_matrix, linkage_method, n_clusters, connectivity=None, **kwargs):
        """Agglomerative hierarchical clustering method.

        Parameters
        ----------
        global_matrix : numpy.ndarray
            The global pairwise distance matrix to cluster.
        linkage_method : {'single', 'complete', 'average', 'weighted', 'centroid', 'median', 'ward'}
            The linkage method for the agglomerative clustering method.
        n_clusters : int
            The number of required clusters.
        connectivity : numpy.ndarray, optional
            Connectivity matrix. Defines for each sample the neighboring samples.
            NOT USED IN THIS CASE.

        Returns
        -------
        list
            The list of clustering labels.

        Notes
        -----
        All ``linkage`` parameters, except for ``y`` and ``method``, are accessible using keyword arguments.

        See Also
        --------
        scipy.cluster.hierarchy.linkage
        scipy.cluster.hierarchy.fcluster

        References
        ----------
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html
        https://scikit-learn.org/stable/modules/clustering.html#hierarchical-clustering
        """
        from scipy.cluster.hierarchy import linkage
        from scipy.cluster.hierarchy import fcluster
        if connectivity is not None:
            log.warning("Can not use `connectivity` parameter with agglomerative hierarchical clustering method.")
        # Get the upper triangle indexes of the global matrix:
        upper_idx = np.triu_indices(global_matrix.shape[0], k=1)
        # Apply agglomerative hierarchical clustering on condensed distance matrix:
        linkage_matrix = linkage(global_matrix[upper_idx].flatten(), method=linkage_method, **kwargs)
        return fcluster(linkage_matrix, n_clusters, criterion="maxclust")

    @staticmethod
    def _compute_spectral(global_matrix, n_clusters, connectivity=None, **kwargs):
        """Spectral clustering method.

        Parameters
        ----------
        global_matrix : numpy.ndarray
            The global pairwise distance matrix to cluster.
        n_clusters : int
            The number of required clusters.
        connectivity : numpy.ndarray, optional
            Connectivity matrix. Defines for each sample the neighboring samples.
            NOT USED IN THIS CASE.

        Other Parameters
        ----------------
        n_jobs : int
            The number of parallel jobs to run.
            ``None`` means ``1`` unless in a ``joblib.parallel_backend`` context.
            ``-1`` means using all processors.
            Defaults to ``-1``.

        Returns
        -------
        list
            The list of clustering labels.

        Notes
        -----
        All ``SpectralClustering`` parameters, except for ``n_clusters`` and ``affinity``, are accessible using keyword arguments.

        See Also
        --------
        sklearn.cluster.SpectralClustering

        References
        ----------
        https://scikit-learn.org/stable/modules/clustering.html#spectral-clustering
        https://scikit-learn.org/stable/modules/generated/sklearn.cluster.SpectralClustering.html
        """
        if connectivity is not None:
            log.warning("Can not use `connectivity` parameter with 'Spectral' method.")

        if not 'n_jobs' in kwargs:
            kwargs['n_jobs'] = -1  # set it to use all processors by default
        if 'affinity' in kwargs:
            kwargs.pop('affinity', None)
            log.warning("You cannot change the `affinity` parameter!")
        if 'connectivity' in kwargs:
            kwargs.pop('connectivity', None)

        clustering = SpectralClustering(n_clusters=n_clusters, affinity='precomputed', **kwargs).fit(global_matrix)
        return list(clustering.labels_)

    @staticmethod
    def _compute_dbscan(global_matrix, n_clusters=None, connectivity=None, **kwargs):
        """Density-Based Spatial Clustering of Applications with Noise.

        Parameters
        ----------
        global_matrix : numpy.ndarray
            The global pairwise distance matrix to cluster.
        n_clusters : int, optional
            The number of required clusters.
            NOT USED IN THIS CASE.
        connectivity : numpy.ndarray, optional
            Connectivity matrix. Defines for each sample the neighboring samples.
            NOT USED IN THIS CASE.

        Other Parameters
        ----------------
        eps : float
            The maximum distance between two samples for one to be considered as in the neighborhood of the other.
            Defaults to ``5``.
        min_samples : int
            The number of samples (or total weight) in a neighborhood for a point to be considered as a core point.
            This includes the point itself.
            Defaults to ``5``.
        n_jobs : int
            The number of parallel jobs to run.
            ``None`` means ``1`` unless in a ``joblib.parallel_backend`` context.
            ``-1`` means using all processors.
            Defaults to ``-1``.

        Returns
        -------
        list
            The list of clustering labels.

        Notes
        -----
        All ``DBSCAN`` parameters, except for ``metric``, are accessible using keyword arguments.

        See Also
        --------
        sklearn.cluster.DBSCAN

        References
        ----------
        https://scikit-learn.org/stable/modules/clustering.html#dbscan
        https://scikit-learn.org/stable/modules/generated/sklearn.cluster.DBSCAN.html
        """
        if n_clusters is not None:
            log.warning('Can not use `n_clusters` parameter with DBSCAN clustering.')
        if connectivity is not None:
            log.warning('Can not use `connectivity` matrix parameter with DBSCAN clustering.')

        if not 'n_jobs' in kwargs:
            kwargs['n_jobs'] = -1  # set it to use all processors by default
        if 'metric' in kwargs:
            kwargs.pop('metric', None)
            log.warning("You cannot change the `metric` parameter!")
        if 'connectivity' in kwargs:
            kwargs.pop('connectivity', None)

        clustering = DBSCAN(metric="precomputed", **kwargs).fit(global_matrix)
        return list(clustering.labels_)

    @staticmethod
    def _compute_hdbscan(global_matrix, n_clusters, connectivity, **kwargs):
        """Hierarchical Density-Based Spatial Clustering of Applications with Noise.

        Parameters
        ----------
        global_matrix : numpy.ndarray
            The global pairwise distance matrix to cluster.
        n_clusters : int, optional
            The number of required clusters.
            NOT USED IN THIS CASE.
        connectivity : numpy.ndarray, optional
            Connectivity matrix. Defines for each sample the neighboring samples.
            NOT USED IN THIS CASE.

        Other Parameters
        ----------------
        min_cluster_size : float
            The minimum size of clusters.
            Defaults to `8`.
        min_samples : int
            The number of samples (or total weight) in a neighborhood for a point to be considered as a core point.
            This includes the point itself.
            Defaults to `5`.

        Returns
        -------
        list
            The list of clustering labels.

        Notes
        -----
        Performs DBSCAN over varying epsilon values and integrates the result to find a clustering that gives the best stability over epsilon.
        This allows HDBSCAN to find clusters of varying densities (unlike DBSCAN), and be more robust to parameter selection.
        All ``HDBSCAN`` parameters, except for ``metric``, are accessible using keyword arguments.

        See Also
        --------
        hdbscan.HDBSCAN

        References
        ----------
        https://hdbscan.readthedocs.io/en/latest/index.html
        """
        if n_clusters is not None:
            log.warning('Can not use `n_clusters` parameter with DBSCAN clustering.')
        if connectivity is not None:
            log.warning('Can not use `connectivity` matrix parameter with DBSCAN clustering.')

        if not 'core_dist_n_jobs' in kwargs:
            kwargs['core_dist_n_jobs'] = -1  # set it to use all processors by default
        if 'metric' in kwargs:
            kwargs.pop('metric', None)
            log.warning("You cannot change the `metric` parameter!")
        if 'connectivity' in kwargs:
            kwargs.pop('connectivity', None)

        clustering = HDBSCAN(metric="precomputed", **kwargs).fit(global_matrix)
        return list(clustering.labels_)

    def compute_clustering(self, method="ward", n_clusters=None, connectivity=None, **kwargs):
        """Run the selected clustering method on the pre-computed global distance matrix.

        Parameters
        ----------
        method : {'single', 'complete', 'average', 'weighted', 'centroid', 'median', 'ward', 'spectral', 'dbscan', 'hdbscan'}
            Clustering method to use, see notes and references for details about the methods.
        n_clusters : int, optional
            Number of cluster to create during supervised clustering.
            For unsupervised method, like 'dbscan' and 'hdbscan', this is not required.
        connectivity : None or numpy.ndarray
            Connectivity matrix defining for the neighborhood of the data samples.

        Other Parameters
        ----------------
        eps : float
            The maximum distance between two samples for one to be considered as in the neighborhood of the other.
            To use with the "dbscan" method.

        Notes
        -----
          - 'single', 'complete', 'average', 'weighted', 'centroid', 'median' and 'ward' are hierarchical agglomerative clustering methods, see [hierarchical-clustering] and [scipy-linkage].
          - 'spectral' is a dimensionality reduction method followed by a K-Means clustering, see [spectral-clustering].
          - 'dbscan' is an unsupervised density based clusetring method, see [dbscan].
          - 'hdbscan' is a supervised hierarchical density based clusetring method, see [hdbscan].

        References
        ----------
        .. [hierarchical-clustering] https://scikit-learn.org/stable/modules/clustering.html#hierarchical-clustering
        .. [scipy-linkage] https://docs.scipy.org/doc/scipy/reference/generated/scipy.cluster.hierarchy.linkage.html
        .. [spectral-clustering] https://scikit-learn.org/stable/modules/clustering.html#spectral-clustering
        .. [dbscan] https://scikit-learn.org/stable/modules/clustering.html#dbscan
        .. [hdbscan] https://hdbscan.readthedocs.io/en/latest/index.html

        Examples
        --------
        >>> from timagetk.components.clustering import example_clusterer
        >>> # - Get an example from shared dataset:
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> # - Select cells with 'FloralMeristem' identity and from the first two layers:
        >>> clust.ids = ['FloralMeristem', 'layer_1_2']
        >>> # - Compute and add vector variables for "volume" and "log_relative_value('volume',1)" cell properties:
        >>> clust.add_spatial_variable("volume")
        >>> clust.add_temporal_variable("log_relative_value('volume',1)")
        >>> # - Compute the global pairwise distance matrix combine these two cell properties on a 50/50% basis:
        >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5])
        >>> clust.n_gpdm_ids()
        631
        >>> # - Dimensionality reduction into two dimensions with t-SNE:
        >>> from sklearn.manifold import TSNE
        >>> data = clust._global_pw_dist_mat
        >>> projection = TSNE(n_components=2, metric='precomputed', learning_rate='auto', init='random').fit_transform(data)
        >>> import matplotlib.pyplot as plt
        >>> plt.scatter(*projection.T, alpha=0.75, linewidth=0)

        >>> # - EXAMPLE #1 - Cluster it with a hierarchical agglomerative clustering method with "Ward's" linkage:
        >>> # Performs several clustering and compute their silhouette metrics:
        >>> silhouette, nb_cluster = clust.silhouette_estimator(method="ward")
        >>> print(f"Number of clusters with the best silhouette metrics: {nb_cluster}")
        Number of clusters with the best silhouette metrics: 4
        >>> clust.compute_clustering(method="ward", n_clusters=4)
        >>> clust.n_cells_by_cluster()
        {1: 58, 2: 219, 3: 199, 4: 155}
        >>> # - Visualize obtained clustering on 2D projection by t-SNE:
        >>> import matplotlib.pyplot as plt
        >>> plt.scatter(*projection.T, c=list(clust.get_clustering_dict().values()), alpha=0.75, linewidth=0)
        >>> # - Spatial projection of the obtained clustering on segmented tissues:
        >>> from timagetk.io.dataset import shared_data_path
        >>> from timagetk.visu.clustering import clustering_figures
        >>> tissues = shared_data_path('flower_labelled')
        >>> clustering_figures(clust.graph, clust.clustering_name(), tissues, [1, 2])

        >>> # - EXAMPLE #2 - Cluster it with a hierarchical agglomerative clustering method with "complete" linkage:
        >>> clust.compute_clustering(method="complete", n_clusters=4)
        >>> clust.n_cells_by_cluster()
        {0: 564, 1: 16, 2: 19, 3: 32}
        >>> # - Visualize obtained clustering on 2D projection by t-SNE:
        >>> plt.scatter(*projection.T, c=list(clust.get_clustering_dict().values()), alpha=0.75, linewidth=0)

        >>> # - EXAMPLE #3 - Cluster it with HDBSCAN clustering method:
        >>> clust.compute_clustering(method="HDBSCAN", min_cluster_size=8, min_samples=None)
        >>> clust.n_cells_by_cluster(exclude_outlier_cluster=False)
        {0: 9, 1: 328, 2: 10, -1: 284}
        >>> # - Visualize obtained clustering on 2D projection by t-SNE:
        >>> plt.scatter(*projection.T, c=list(clust.get_clustering_dict(exclude_outlier_cluster=False).values()), alpha=0.75, linewidth=0)
        """
        method = method.lower()
        try:
            assert method in ALL_CLUSTERING_METHODS
        except AssertionError:
            raise ValueError(
                f"The `method` parameter is not valid, got '{method}' and should be in {ALL_CLUSTERING_METHODS}.")

        if self._global_pw_dist_mat is None:
            raise ValueError("No global distance matrix assembled, use `assemble_matrix` first!")
        global_matrix = self._global_pw_dist_mat

        if method in ('single', 'complete', 'average', 'weighted', 'centroid', 'median', 'ward'):
            clustering_labels = self._compute_agglomerative(global_matrix, method, n_clusters, connectivity, **kwargs)
            clustering_params = {'n_clusters': n_clusters}
        elif method == "spectral":
            clustering_labels = self._compute_spectral(global_matrix, n_clusters, connectivity, **kwargs)
            clustering_params = {'n_clusters': n_clusters}
        elif method == "dbscan":
            kwargs['eps'] = kwargs.get('eps', 5)  # set default values
            kwargs['min_samples'] = kwargs.get('min_samples', 5)  # set default values
            clustering_labels = self._compute_dbscan(global_matrix, n_clusters, connectivity, **kwargs)
            clustering_params = kwargs
            n_clusters = "auto"
        elif method == "hdbscan":
            kwargs['min_cluster_size'] = kwargs.get('min_cluster_size', 8)  # set default values
            kwargs['min_samples'] = kwargs.get('min_samples', 5)  # set default values
            clustering_labels = self._compute_hdbscan(global_matrix, n_clusters, connectivity, **kwargs)
            clustering_params = kwargs
            n_clusters = "auto"
        else:
            raise ValueError(f"Unknown clustering method '{method}'!")

        # Create the clustering dictionary:
        clustering_dict = dict(zip(self._global_pw_dist_ids, clustering_labels))

        # Clean the potential outliers detected by unsupervised methods:
        if method in ['dbscan', 'hdbscan']:
            new_outliers = [eid for eid, label in clustering_dict.items() if label == -1]
            clustering_dict = {eid: label for eid, label in clustering_dict.items() if label == -1}
            if self._outlier_ids is None:
                self._outlier_ids = new_outliers
            else:
                self._outlier_ids |= new_outliers

        # Export clustering hidden attributes:
        self._clustering = clustering_dict
        self._clustering_params = clustering_params
        self._method = method
        self._nb_clusters = n_clusters
        # Export clustering to the TissueGraph object 'self.graph':
        self.export_clustering2graph()

        return

    def clustering_name(self):
        """Return the name of the current clustering, if previously computed."""
        if self._global_pw_dist_mat is None:
            log.warning('No clustering have yet been performed!')
            return None

        clustering_params = self._clustering_params.copy()
        return _clustering_naming(self._method, clustering_params, self._global_pw_dist_weights,
                                  self._global_pw_dist_variables, self._outliers_mgt)

    def get_clustering_dict(self, exclude_outlier_cluster=True) -> dict:
        """Get the cell id indexed clustering dictionary.

        Examples
        --------
        >>> from sklearn.preprocessing import RobustScaler
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> clust.add_spatial_variable('volume')
        >>> clust.add_temporal_variable("log_relative_value('volume',1)")
        >>> # Detect the outliers using LOF on quantile scaled and median centered data:
        >>> scaler = RobustScaler(quantile_range=(10., 90.)).fit_transform
        >>> outlier_ids = clust.detect_outlier_cells(scaler=scaler)
        >>> # EXAMPLE #1 - Exclude detected outliers from the standardisation metric and clustering:
        >>> # Assemble a global pairwise distance matrix
        >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5], delete_outliers=True)
        >>> # Cluster it with a hierarchical agglomerative clustering method with a "ward" linkage:
        >>> clust.compute_clustering(method="ward", n_clusters=5)
        >>> # Now you can access the clustering dictionary as follows:
        >>> clustering = clust.get_clustering_dict()
        >>> print(len(clustering))
        4515
        >>> list(clust._outlier_ids)[0] in clustering
        False
        >>> # You can include the excluded outliers in the clustering dictionary as follows:
        >>> clustering_wo = clust.get_clustering_dict(exclude_outlier_cluster=False)
        >>> print(len(clustering_wo))
        4530
        >>> list(clust._outlier_ids)[0] in clustering_wo
        True
        >>> clustering_wo[list(clust._outlier_ids)[0]]
        -1
        >>> # EXAMPLE #2 - Include detected outliers in the clustering, but exclude them from the standardisation metric:
        >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5])
        >>> # Cluster it with a hierarchical agglomerative clustering method with a "ward" linkage:
        >>> clust.compute_clustering(method="ward", n_clusters=5)
        >>> # Now you can access the clustering dictionary as follows:
        >>> clustering = clust.get_clustering_dict()
        >>> print(len(clustering))
        4530
        >>> list(clust._outlier_ids)[0] in clustering
        True
        >>> clustering[list(clust._outlier_ids)[0]]
        1
        """
        if self._clustering is None:
            log.error("No clustering have been computed yet!")
            return {}

        if exclude_outlier_cluster:
            return self._clustering
        else:
            return {tcid: self.outliers_cluster_id for tcid in self._outlier_ids} | self._clustering

    def list_cluster_ids(self, exclude_outlier_cluster=True) -> list:
        """Get the list of valid cluster ids, that is the cluster ids excluding the outlier cluster id.

        Examples
        --------
        >>> from sklearn.preprocessing import RobustScaler
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> clust.add_spatial_variable('volume')
        >>> clust.add_temporal_variable("log_relative_value('volume',1)")
        >>> # Detect the outliers using LOF on quantile scaled and median centered data:
        >>> scaler = RobustScaler(quantile_range=(10., 90.)).fit_transform
        >>> outlier_ids = clust.detect_outlier_cells(scaler=scaler)
        >>> # Assemble a global pairwise distance matrix
        >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5], delete_outliers=True)
        >>> # Cluster it with a hierarchical agglomerative clustering method with a "ward" linkage:
        >>> clust.compute_clustering(method="ward", n_clusters=5)
        >>> # Now you can list the cluster ids as follows:
        >>> print(clust.list_cluster_ids())
        [1, 2, 3, 4, 5]
        >>> # You can also list the cluster ids, including the outliers cluster id as follows:
        >>> print(clust.list_cluster_ids(exclude_outlier_cluster=False))
        [1, 2, 3, 4, 5, -1]
        """
        if self._clustering is None:
            log.error("No clustering have been computed yet!")
            return []

        return cluster_ids(self.get_clustering_dict(exclude_outlier_cluster),
                           self.outliers_cluster_id if exclude_outlier_cluster else None)

    def get_number_of_clusters(self, exclude_outlier_cluster=True) -> int:
        """Return the number of clusters.

        Examples
        --------
        >>> from sklearn.preprocessing import RobustScaler
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> # Register the "volume" as spatial variable and the "log_relative_value('volume',1)" as temporal variable:
        >>> clust.add_spatial_variable('volume')
        >>> clust.add_temporal_variable("log_relative_value('volume',1)")
        >>> # Detect the outliers using LOF on quantile scaled and median centered data:
        >>> scaler = RobustScaler(quantile_range=(10., 90.)).fit_transform
        >>> outlier_ids = clust.detect_outlier_cells(scaler=scaler)
        >>> # Assemble a global pairwise distance matrix, excluding the outliers:
        >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5], delete_outliers=True)
        >>> # Cluster it with a hierarchical agglomerative clustering method with a "ward" linkage:
        >>> clust.compute_clustering(method="ward", n_clusters=5)
        >>> # Now you can get the number of cluster ids as follows:
        >>> print(clust.get_number_of_clusters())
        5
        >>> # You can also get the number of cluster ids, including the outliers cluster id as follows:
        >>> print(clust.get_number_of_clusters(exclude_outlier_cluster=False))
        6
        """
        if self._clustering is None:
            log.error("No clustering have been computed yet!")
            return 0

        return len(self.list_cluster_ids(exclude_outlier_cluster))

    def cell_ids_by_cluster(self, exclude_outlier_cluster=True) -> dict:
        """Return the dictionary of ids indexed by cluster id.

        Examples
        --------
        >>> from sklearn.preprocessing import RobustScaler
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> clust.add_spatial_variable('volume')
        >>> clust.add_temporal_variable("log_relative_value('volume',1)")
        >>> # Detect the outliers using LOF on quantile scaled and median centered data:
        >>> scaler = RobustScaler(quantile_range=(10., 90.)).fit_transform
        >>> outlier_ids = clust.detect_outlier_cells(scaler=scaler)
        >>> # Assemble a global pairwise distance matrix
        >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5], delete_outliers=True)
        >>> # Cluster it with a hierarchical agglomerative clustering method with a "ward" linkage:
        >>> clust.compute_clustering(method="ward", n_clusters=5)
        >>> # Now you can list the cell ids by cluster ids as follows:
        >>> print(len(clust.cell_ids_by_cluster()))
        5
        >>> # You can also list the cell ids by cluster ids, including the outliers cluster id as follows:
        >>> print(len(clust.cell_ids_by_cluster(exclude_outlier_cluster=False)))
        6
        """
        if self._clustering is None:
            log.error("No clustering have been computed yet!")
            return {}

        return elements_by_cluster(self.get_clustering_dict(exclude_outlier_cluster),
                                   self.outliers_cluster_id if exclude_outlier_cluster else None)

    def n_cells_by_cluster(self, exclude_outlier_cluster=True) -> dict:
        """Return the number of cells by cluster.

        Examples
        --------
        >>> from sklearn.preprocessing import RobustScaler
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> clust.add_spatial_variable('volume')
        >>> clust.add_temporal_variable("log_relative_value('volume',1)")
        >>> # Detect the outliers using LOF on quantile scaled and median centered data:
        >>> scaler = RobustScaler(quantile_range=(10., 90.)).fit_transform
        >>> outlier_ids = clust.detect_outlier_cells(scaler=scaler)
        >>> # EXAMPLE #1 - Exclude detected outliers from the standardisation metric and clustering:
        >>> # Assemble a global pairwise distance matrix
        >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5], delete_outliers=True)
        >>> # Cluster it with a hierarchical agglomerative clustering method with a "ward" linkage:
        >>> clust.compute_clustering(method="ward", n_clusters=5)
        >>> # Now you can list the cell ids by cluster ids as follows:
        >>> print(clust.n_cells_by_cluster())
        {1: 1623, 2: 1744, 3: 982, 4: 22, 5: 144}
        >>> # You can also list the cell ids by cluster ids, including the outliers cluster id as follows:
        >>> print(clust.n_cells_by_cluster(exclude_outlier_cluster=False))
        {1: 1623, 2: 1744, 3: 982, 4: 22, 5: 144, -1: 15}
        >>> # EXAMPLE #2 - Include detected outliers in the clustering, but exclude them from the standardisation metric:
        >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5])
        >>> # Cluster it with a hierarchical agglomerative clustering method with a "ward" linkage:
        >>> clust.compute_clustering(method="ward", n_clusters=5)
        >>> print(clust.n_cells_by_cluster())
        {1: 1626, 2: 1751, 3: 985, 4: 22, 5: 146}
        >>> # You can also list the cell ids by cluster ids, including the outliers cluster id as follows:
        >>> print(clust.n_cells_by_cluster(exclude_outlier_cluster=False))
        {1: 1626, 2: 1751, 3: 985, 4: 22, 5: 146}
        """
        if self._clustering is None:
            log.error("No clustering have been computed yet!")
            return {}

        return n_elements_by_cluster(self.get_clustering_dict(exclude_outlier_cluster),
                                     self.outliers_cluster_id if exclude_outlier_cluster else None)

    def export_clustering2graph(self):
        """Export the clustering to the temporal tissue graph."""
        if self._global_pw_dist_mat is None:
            log.warning('No clustering have yet been performed!')
            return

        name = self.clustering_name()
        log.info(f"The clustering '{name}' will be added to the `graph` ...")
        # Export the clustering under its name:
        # self.graph.add_cell_property(name, {k: int(v) for k, v in self.get_clustering_dict().items()})
        self.graph.add_cell_property(name, self.get_clustering_dict(exclude_outlier_cluster=False))

        return

    # -------------------------------------------------------------------------
    # Graphics
    # -------------------------------------------------------------------------

    @staticmethod
    def _histogram(data_dict, color_by_height=False, **kwargs):
        from matplotlib import pyplot as plt
        from matplotlib import colors

        figname = kwargs.pop('figname', None)
        hist_height = kwargs.pop('hist_height', 5.)  # height of the histogram figure
        hist_width = kwargs.pop('hist_width', 2.5 * hist_height)  # width of the histogram figure
        # Matplotlib keyword arguments:
        dpi = kwargs.pop('dpi', 96)

        n_figs = len(data_dict)
        fig, axes = plt.subplots(nrows=n_figs, ncols=1, dpi=dpi)
        fig.set_size_inches(hist_width, n_figs * hist_height + 1)
        for i_fig, (vname, vect_data) in enumerate(data_dict.items()):
            if n_figs == 1:
                ax = axes
            else:
                ax = axes[i_fig]
            N, bins, patches = ax.hist(vect_data, **kwargs)
            if color_by_height:
                # Color code by height
                fracs = N / N.max()
                # we need to normalize the data to 0..1 for the full range of the colormap
                norm = colors.Normalize(fracs.min(), fracs.max())
                # Loop through our objects and set the color of each accordingly
                for thisfrac, thispatch in zip(fracs, patches):
                    color = plt.cm.viridis(norm(thisfrac))
                    thispatch.set_facecolor(color)
            # ax.hlines(0, *ax.get_xlim(), color='black', linewidths=0.8)
            mini = - max(N) * 0.08
            maxi = max(N) * 1.05
            _ = ax.plot(vect_data, [mini / 2] * len(vect_data), '|', color='gray')
            ax.set_ylim(mini, maxi)
            ax.set_xlim(*ax.get_xlim())
            if n_figs == 1:
                ax.set_title(f"Histogram of '{vname}' vector data")
            else:
                ax.set_title(f"{vname}")
        if n_figs != 1:
            plt.suptitle("Vector data histograms.")

        if isinstance(figname, str):
            plt.savefig(figname)
            plt.close()

        return fig, axes

    def plot_vector_histogram(self, var_names=None, exclude_outliers=True, **kwargs):
        """Display an histogram for each variable vector.

        Parameters
        ----------
        var_names : None or list of str
            List of variables names in `self._distance_matrix_dict` to plot.
            If ``None`` (default), plot all of them.
        exclude_outliers : bool, optional
            Wheter to exclude the outliers from the graph.
            Defaults to ``True``, to exclude them.

        Other Parameters
        ----------------
        hist_height : float
            The height of an histogram (per variable), in inches. Defaults to ``5.``.
        hist_width : float
            The width of an histogram, in inches. Defaults to ``2.5 * hist_height``.
        color_by_height : bool
            If ``True``, color the bins of the histogram by height, *i.e.* proportionnaly to the max height of the distribution.
            Defaults to ``False``.
        figname : str or None
            If defined, should be a valid filename, used to save the figure. Defaults to ``False``.
        dpi : int
            The number of dot per inch (dpi) to use to create the figure. Defaults to ``96``.

        Returns
        -------
        matplotlib.figure.Figure
            The `Figure` object.
        matplotlib.axes.Axes
            The `Axes` object.

        Notes
        -----
        It is possible to provides other keyword arguments for ``matplotlib.pyplot.hist()``.

        Examples
        --------
        >>> import matplotlib.pyplot as plt
        >>> from sklearn.preprocessing import RobustScaler
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> clust.add_spatial_variable('volume')
        >>> clust.add_temporal_variable("log_relative_value('volume',1)")
        >>> # Example #1 - Plot the feature histograms for all ids:
        >>> fig, axes = clust.plot_vector_histogram(bins=50)
        >>> plt.show()
        >>> # Example #2 - Plot the feature histograms for non-outlier ids:
        >>> scaler = RobustScaler(quantile_range=(10., 90.)).fit_transform
        >>> outlier_ids = clust.detect_outlier_cells(scaler=scaler)
        >>> fig, axes = clust.plot_vector_histogram(bins=50)
        >>> plt.show()
        """
        var_names = self._check_features(var_names)

        if exclude_outliers:
            ids = self._filter_outliers()
        else:
            ids = self.ids

        color_by_height = kwargs.pop('color_by_height', False)
        return self._histogram({vname: self._get_feature_values(vname, ids) for vname in var_names},
                                    color_by_height=color_by_height, **kwargs)

    @staticmethod
    def _heatmap(data_dict, suptitle, **kwargs):
        figname = kwargs.pop('figname', None)
        # Matplotlib keyword arguments:
        dpi = kwargs.pop('dpi', 96)
        matsize = kwargs.pop('matsize', 5.)  # size of the matrix figure

        n_figs = len(data_dict)
        fig, axes = plt.subplots(nrows=1, ncols=n_figs, dpi=dpi)
        fig.set_size_inches(w=n_figs * matsize, h=matsize + 1.)
        for i_fig, (vname, data) in enumerate(data_dict.items()):
            id_col = i_fig
            if n_figs == 1:
                ax = axes
            else:
                ax = axes[id_col]
            imax = ax.imshow(data, interpolation=None, aspect='equal', **kwargs)
            ax.xaxis.tick_top()  # and move the X-axis ticks to top
            ax.set_title(f"{vname}")
            plt.colorbar(imax, ax=ax, orientation="horizontal", fraction=0.1)
        plt.suptitle(suptitle, fontweight='semibold')

        if isinstance(figname, str):
            plt.savefig(figname)
            plt.close()

        return fig, axes

    def plot_feature_distance_matrix(self, feature_names=None, **kwargs):
        """Plot the pairwise distance matrix as a heatmap for the selected features.

        Parameters
        ----------
        feature_names : None or list of str
            List of features names in `self._distance_matrix_dict` to plot.
            If ``None`` (default), plot all of them.

        Other Parameters
        ----------------
        matsize : float
            Size of the matrix, in inches. Defaults to ``5.``.
        figname : str or None
            If defined, should be a valid filename, used to save the figure.
        dpi : int
            The number of dot per inch (dpi) to use to create the figure. Defaults to ``96``.

        Returns
        -------
        matplotlib.figure.Figure
            The `Figure` object.
        matplotlib.axes.Axes
            The `Axes` object.

        Notes
        -----
        It is possible to provides other keyword arguments for ``matplotlib.pyplot.imshow()``, except for
        ``interpolation`` & ``aspect`` that are set to ``None`` and ``equal``, respectively.

        Examples
        --------
        >>> import matplotlib.pyplot as plt
        >>> from sklearn.preprocessing import RobustScaler
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> # Register the "volume" as spatial variable and the "log_relative_value('volume',1)" as temporal variable:
        >>> clust.add_spatial_variable('volume')
        >>> clust.add_temporal_variable("log_relative_value('volume',1)")
        >>> # Detect the outliers using LOF on quantile scaled and median centered data:
        >>> scaler = RobustScaler(quantile_range=(10., 90.)).fit_transform
        >>> outlier_ids = clust.detect_outlier_cells(scaler=scaler)
        >>> # Assemble a global pairwise distance matrix, excluding the outliers:
        >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5], delete_outliers=True)
        >>> # Cluster it with a hierarchical agglomerative clustering method with a "ward" linkage:
        >>> clust.compute_clustering(method="ward", n_clusters=5)
        >>> # Now you can plot the feature's pairwise distance matrices as follows:
        >>> fig, axes = clust.plot_feature_distance_matrix()
        >>> plt.show()
        """
        feature_names = self._check_gpdm_features(feature_names)
        # We have to filter by used indexes as `self._distance_matrix_dict` contains all `self.ids`:
        ids_index = [list(self.ids).index(v) for v in self._global_pw_dist_ids]
        data_dict = {fname: self._distance_matrix_dict[fname][ids_index, :][:, ids_index] for fname in feature_names}
        return self._heatmap(data_dict, "Feature pairwise distance matrix.", **kwargs)

    def plot_weight_matrix(self, feature_names=None, **kwargs):
        """Plot the pairwise weight matrix as a heatmap for the selected features.

        Parameters
        ----------
        feature_names : None or list of str
            List of features names in `self._global_pw_dist_weight_mat` to plot.
            If ``None`` (default), plot all of them.

        Other Parameters
        ----------------
        matsize : float
            Size of the matrix, in inches. Defaults to ``5.``.
        figname : str or None
            If defined, should be a valid filename, used to save the figure.
        dpi : int
            The number of dot per inch (dpi) to use to create the figure. Defaults to ``96``.

        Returns
        -------
        matplotlib.figure.Figure
            The `Figure` object.
        matplotlib.axes.Axes
            The `Axes` object.

        Notes
        -----
        It is possible to provides other keyword arguments for ``matplotlib.pyplot.imshow()``, except for
        ``interpolation`` & ``aspect`` that are set to ``None`` and ``equal``, respectively.

        Examples
        --------
        >>> import matplotlib.pyplot as plt
        >>> from sklearn.preprocessing import RobustScaler
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> # Register the "volume" as spatial variable and the "log_relative_value('volume',1)" as temporal variable:
        >>> clust.add_spatial_variable('volume')
        >>> clust.add_temporal_variable("log_relative_value('volume',1)")
        >>> # Detect the outliers using LOF on quantile scaled and median centered data:
        >>> scaler = RobustScaler(quantile_range=(10., 90.)).fit_transform
        >>> outlier_ids = clust.detect_outlier_cells(scaler=scaler)
        >>> # Assemble a global pairwise distance matrix, excluding the outliers:
        >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5], delete_outliers=True)
        >>> # Cluster it with a hierarchical agglomerative clustering method with a "ward" linkage:
        >>> clust.compute_clustering(method="ward", n_clusters=5)
        >>> # Now you can plot the feature's pairwise weight matrices as follows:
        >>> fig, axes = clust.plot_weight_matrix()
        >>> plt.show()
        """
        feature_names = self._check_gpdm_features(feature_names)
        weight_dict = {fname: self._global_pw_dist_weight_mat[fname] for fname in feature_names}
        return self._heatmap(weight_dict, "Feature pairwise weight matrix.", **kwargs)

    def plot_global_distance_matrix(self, **kwargs):
        """Plot the global pairwise distance matrix (GPDM) as a heatmap.

        Other Parameters
        ----------------
        matsize : float
            Size of the matrix, in inches. Defaults to ``12.``.
        figname : str or None
            If defined, should be a valid filename, used to save the figure.
        dpi : int
            The number of dot per inch (dpi) to use to create the figure. Defaults to ``96``.

        Returns
        -------
        matplotlib.figure.Figure
            The `Figure` object.
        matplotlib.axes.Axes
            The `Axes` object.

        Notes
        -----
        It is possible to provides other keyword arguments for ``matplotlib.pyplot.imshow()``, except for
        ``interpolation`` & ``aspect`` that are set to ``None`` and ``equal``, respectively.

        Examples
        --------
        >>> import matplotlib.pyplot as plt
        >>> from sklearn.preprocessing import RobustScaler
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> # Register the "volume" as spatial variable and the "log_relative_value('volume',1)" as temporal variable:
        >>> clust.add_spatial_variable('volume')
        >>> clust.add_temporal_variable("log_relative_value('volume',1)")
        >>> # Detect the outliers using LOF on quantile scaled and median centered data:
        >>> scaler = RobustScaler(quantile_range=(10., 90.)).fit_transform
        >>> outlier_ids = clust.detect_outlier_cells(scaler=scaler)
        >>> # Assemble a global pairwise distance matrix, excluding the outliers:
        >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5], delete_outliers=True)
        >>> # Cluster it with a hierarchical agglomerative clustering method with a "ward" linkage:
        >>> clust.compute_clustering(method="ward", n_clusters=5)
        >>> # Now you can plot the global pairwise distance matrix (GPDM) as follows:
        >>> fig, axes = clust.plot_global_distance_matrix()
        >>> plt.show()
        """
        data = {f"{self._standardisation_method} standardized": self._global_pw_dist_mat}
        kwargs['matsize'] = kwargs.get('matsize', 12.)
        return self._heatmap(data, "Global pairwise distance matrix.", **kwargs)

    def silhouette_estimator(self, method, k_min=3, k_max=8, connectivity=None):
        """Compute the silhouette metrics for a range of number cluster with given supervised `method`.

        Parameters
        ----------
        method : {'single', 'complete', 'average', 'weighted', 'centroid', 'median', 'ward', 'spectral'}
            Clustering methods to use.
        k_min : int, optional
            The minimum number of cluster to consider, should be strictly superior to ``1``.
            Defaults to ``3``.
        k_max : int, optional
            The maximum number of cluster to consider.
            Defaults to ``8``.
        connectivity : numpy.ndarray, optional
            Connectivity matrix defining for the neighborhood of the data samples.

        Returns
        -------
        dict
            Dictionary of silhouette values.
        int
            Number of clusters with the best silhouette metrics.

        Raises
        ------
        ValueError
            If the minimum number of cluster is lower or equal to `1`, `k_min <= 1`.
            If the selected clustering `method` is not valid, that is an unsupervised method.

        See Also
        --------
        sklearn.metrics.cluster.silhouette_score

        Notes
        -----
        The closer to ``1``, the better.
        The closer to ``-1``, the worst.
        ``0`` mean overlapping clusters.

        Examples
        --------
        >>> from sklearn.preprocessing import RobustScaler
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> # Register the "volume" as spatial variable and the "log_relative_value('volume',1)" as temporal variable:
        >>> clust.add_spatial_variable('volume')
        >>> clust.add_temporal_variable("log_relative_value('volume',1)")
        >>> # Detect the outliers using LOF on quantile scaled and median centered data:
        >>> scaler = RobustScaler(quantile_range=(10., 90.)).fit_transform
        >>> outlier_ids = clust.detect_outlier_cells(scaler=scaler)
        >>> # Assemble a global pairwise distance matrix, excluding the outliers:
        >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5], delete_outliers=True)
        >>> # Performs several clustering and compute their silhouette metrics:
        >>> silhouette, nb_cluster = clust.silhouette_estimator("ward", k_max=7)
        >>> print(f"Number of clusters with the best silhouette metrics: {nb_cluster}")
        """
        from sklearn import metrics
        # Check minimum of cluster is NOT lower or equal to `1`:
        try:
            assert k_min > 1
        except AssertionError:
            raise ValueError("Parameter `k_min` should be strictly greater than `1`!")
        # Check the selected clustering `method` is valid:
        try:
            assert method in ['single', 'complete', 'average', 'weighted', 'centroid', 'median', 'ward', 'spectral']
        except AssertionError:
            raise ValueError("Parameter `method` should be 'single', 'ward' or 'spectral'!")

        global_matrix = self._global_pw_dist_mat
        clustering_labels = {}  # dict of obtained clustering labels indexed by number of clusters
        sil = {}  # dict of silhouette scores indexed by number of clusters
        for k in range(k_min, k_max + 1):
            if method.lower() == "spectral":
                clustering_labels[k] = self._compute_spectral(global_matrix, k, connectivity)
            else:
                clustering_labels[k] = self._compute_agglomerative(global_matrix, linkage_method=method.lower(),
                                                                   n_clusters=k, connectivity=connectivity)

            # Compute the silhouette score:
            sil[k] = metrics.silhouette_score(global_matrix, clustering_labels[k], metric='precomputed')

        return sil, list(sil.keys())[np.argmax(list(sil.values()))]

    def plot_silhouette_estimator(self, method, k_min=3, k_max=8, connectivity=None, **kwargs):
        """Compute the silhouette metrics for a range of number cluster with given supervised `method`.

        Parameters
        ----------
        method : {'single', 'complete', 'average', 'weighted', 'centroid', 'median', 'ward', 'spectral'}
            Clustering methods to use.
        k_min : int, optional
            The minimum number of cluster to consider, should be strictly superior to ``1``.
            Defaults to ``3``.
        k_max : int, optional
            The maximum number of cluster to consider.
            Defaults to ``8``.
        connectivity : numpy.ndarray, optional
            Connectivity matrix defining for the neighborhood of the data samples.

        Other Parameters
        ----------------
        figname : str or None
            If defined, should be a valid filename, used to save the figure.
        dpi : int
            The number of dot per inch (dpi) to use to create the figure. Defaults to ``96``.

        Returns
        -------
        matplotlib.figure.Figure
            The `Figure` object.
        matplotlib.axes.Axes
            The `Axes` object.

        Raises
        ------
        ValueError
            If the minimum number of cluster is lower or equal to `1`, `k_min <= 1`.
            If the selected clustering `method` is not valid, that is an unsupervised method.

        See Also
        --------
        sklearn.metrics.cluster.silhouette_score

        Notes
        -----
        The closer to ``1``, the better.
        The closer to ``-1``, the worst.
        ``0`` mean overlapping clusters.

        Examples
        --------
        >>> import matplotlib.pyplot as plt
        >>> from sklearn.preprocessing import RobustScaler
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> # Register the "volume" as spatial variable and the "log_relative_value('volume',1)" as temporal variable:
        >>> clust.add_spatial_variable('volume')
        >>> clust.add_temporal_variable("log_relative_value('volume',1)")
        >>> # Detect the outliers using LOF on quantile scaled and median centered data:
        >>> scaler = RobustScaler(quantile_range=(10., 90.)).fit_transform
        >>> outlier_ids = clust.detect_outlier_cells(scaler=scaler)
        >>> # Assemble a global pairwise distance matrix, excluding the outliers:
        >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5], delete_outliers=True)
        >>> # Performs several clustering and compute their silhouette metrics:
        >>> fig, ax = clust.plot_silhouette_estimator("ward", k_max=7)
        >>> plt.show()
        """
        figname = kwargs.get('figname', None)
        # Matplotlib keyword arguments:
        dpi = kwargs.get('dpi', 96)

        silhouette_scores, _ = self.silhouette_estimator(method, k_min=k_min, k_max=k_max, connectivity=connectivity)
        fig, ax = plt.subplots(figsize=(len(silhouette_scores) * 2 / 3., 4), dpi=dpi)
        _ = ax.bar(silhouette_scores.keys(), silhouette_scores.values())
        _ = ax.set_xlabel("Number of clusters")
        _ = ax.set_ylabel("Silhouette score")

        if isinstance(figname, str):
            plt.savefig(figname)
            plt.close()

        return fig, ax

    def clustering_estimators(self, clustering_method, k_min=4, k_max=15, plot_estimator=True):
        """Compute various estimators based on supervised clustering results.

        Parameters
        ----------
        clustering_method: {'single', 'complete', 'average', 'weighted', 'centroid', 'median', 'ward', 'spectral'}
            A supervised clustering method to get the estimators for.
        k_min : int, optional
            The minimum number of clusters to consider, should be strictly gereater than ``1``.
        k_max : int, optional
            The maximum number of clusters to consider.
        plot_estimator : bool, optional
            If ``True`` (default), plot a graph of the obtained silhouette values.

        Returns
        -------
        dict
            Dictionary of 'Calinski and Harabasz index' indexed by 'number of clusters'.
        dict
            Dictionary of 'Hartigan index' indexed by 'number of clusters'.
        dict
            Dictionary of 'silhouette score' indexed by 'number of clusters'.
        dict
            Dictionary of 'global within cluster distance' indexed by 'number of clusters'.
        dict
            Dictionary of 'global between cluster distance' indexed by 'number of clusters'.

        Raises
        ------
        ValueError
            If `clustering_method` is not taken in ``SUPERVISED_CLUSTERING_METHODS``.
            If `k_min` is not strictly greater than ``1``.
            If `k_max` is not strictly greater than ``k_min+1``.

        See Also
        --------
        timagetk.components.clustering.SUPERVISED_CLUSTERING_METHODS
        timagetk.algorithms.clustering.global_cluster_distances
        timagetk.algorithms.clustering.calinski_harabasz_estimator
        timagetk.algorithms.clustering.within_cluster_distances
        timagetk.algorithms.clustering.hartigan_estimator

        Examples
        --------
        >>> from sklearn.preprocessing import RobustScaler
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> # Register the "volume" as spatial variable and the "log_relative_value('volume',1)" as temporal variable:
        >>> clust.add_spatial_variable('volume')
        >>> clust.add_temporal_variable("log_relative_value('volume',1)")
        >>> # Detect the outliers using LOF on quantile scaled and median centered data:
        >>> scaler = RobustScaler(quantile_range=(10., 90.)).fit_transform
        >>> outlier_ids = clust.detect_outlier_cells(scaler=scaler)
        >>> # Assemble a global pairwise distance matrix, excluding the outliers:
        >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5], delete_outliers=True)
        >>> # Performs several clustering and compute their silhouette metrics:
        >>> ch, hart, sil, gwcd, gbcd = clust.clustering_estimators("ward", k_max=8)
        """
        from sklearn import metrics
        from timagetk.algorithms.clustering_estimators import global_cluster_distances
        from timagetk.algorithms.clustering_estimators import calinski_harabasz_estimator
        from timagetk.algorithms.clustering_estimators import within_cluster_distances
        from timagetk.algorithms.clustering_estimators import hartigan_estimator

        try:
            assert clustering_method in SUPERVISED_CLUSTERING_METHODS
        except AssertionError:
            raise ValueError(
                f"The `clustering_method` parameter should be taken in `SUPERVISED_CLUSTERING_METHODS`, got '{clustering_method}'!")
        try:
            assert k_min > 1
        except AssertionError:
            raise ValueError(f"The `k_min` parameter should be strictly greater than `1`, got 'k_min={k_min}'!")
        try:
            assert k_max >= k_min + 1
        except AssertionError:
            raise ValueError(
                f"The `k_max` parameter should be strictly greater than `k_min+1`, got 'k_min={k_min}' and 'k_max={k_max}'!")

        clustering_labels, N = {}, {}
        gwithin_cdist, gbetween_cdist = {}, {}
        CH, sil = {}, {}

        for k in range(k_min, k_max + 1):
            self.compute_clustering(method=clustering_method, n_clusters=k)
            clustering_labels[k] = list(self.get_clustering_dict(exclude_outlier_cluster=True).values())
            N[k] = len(clustering_labels[k])
            if k > 1:
                w, b = global_cluster_distances(self._global_pw_dist_mat, clustering_labels[k])
                gwithin_cdist[k], gbetween_cdist[k] = w, b
                if None in (w, b):
                    continue  # skip this as there is a cluster with only one element!
                CH[k] = calinski_harabasz_estimator(k, gwithin_cdist[k], gbetween_cdist[k], N[k])
                sil[k] = metrics.silhouette_score(self._global_pw_dist_mat, clustering_labels[k], metric='euclidean')
            else:
                gwithin_cdist[k] = within_cluster_distances(self._global_pw_dist_mat, clustering_labels[k])

        Hartigan = {k: hartigan_estimator(k, gwithin_cdist, N[k]) for k in range(k_min, k_max)}

        if plot_estimator:
            fig = plt.figure(figsize=(12, 4), dpi=100)
            fig.add_subplot(131)
            plt.plot(range(k_min if k_min > 1 else 2, k_max + 1), CH.values())
            plt.title("Calinski and Harabasz estimator")
            fig.add_subplot(132)
            plt.plot(range(k_min, k_max), Hartigan.values(), color='green')
            plt.title("Hartigan estimator")
            fig.add_subplot(133)
            plt.plot(range(k_min if k_min > 1 else 2, k_max + 1), sil.values(), color='red')
            plt.title("Silhouette estimator")

        return CH, Hartigan, sil, gwithin_cdist, gbetween_cdist

    def _property_values(self, ppty):
        """Return the dictionary of values for given `property` indexed by cell id.

        Parameters
        ----------
        ppty : str
            Name of the property.

        Returns
        -------
        dict
            Dictionary of values for given `property` indexed by cell id.
        """
        return dict(zip(self._ids, self._variable_vector_info[ppty]))

    def _property_values_by_cluster(self, ppty, default=np.nan):
        """Return the dictionary of values for given `property` indexed by cluster id.

        Parameters
        ----------
        ppty : str
            Name of the property.
        default : any, optional
            The default property value. In case a cell has no value for this property.
            Defaults to ``np.nan``.

        Returns
        -------
        dict
            Dictionary of values for given `property` indexed by cluster id.

        Examples
        --------
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer()
        >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5])
        >>> clust.compute_clustering(method='ward',n_clusters=4)
        >>> values_by_q = clust._property_values_by_cluster('volume')
        >>> # Show the distribution of 'volume' values as a boxplot:
        >>> import matplotlib.pyplot as plt
        >>> plt.boxplot(values_by_q.values())
        """
        if self._clustering is None:
            log.error("No clustering have been computed yet!")
            return []

        cid_ppty_dict = self._property_values(ppty)
        ids_by_q = self.cell_ids_by_cluster()

        return {q: [cid_ppty_dict.get(cid, default) for cid in ids_by_q[q]] for q in self.list_cluster_ids()}

    def _property_values_by_cluster_by_properties(self, properties=None, default=np.nan):
        """Return the dictionary of values indexed by cluster id and properties.

        Parameters
        ----------
        properties : list or None, optional
            The list of cell property to use.
        default : any, optional
            The default property value. In case a cell has no value for this property.
            Defaults to ``np.nan``.

        Returns
        -------
        dict
            Dictionary of values for given `property` indexed by cluster id.

        Examples
        --------
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer()
        >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5])
        >>> clust.compute_clustering(method='ward',n_clusters=4)
        >>> values_by_q_by_ppty = clust._property_values_by_cluster_by_properties()
        >>> print(values_by_q_by_ppty.keys())
        dict_keys(['volume', "log_relative_value('volume',1)"])
        >>> # Show the distribution of 'volume' values as a boxplot:
        >>> import matplotlib.pyplot as plt
        >>> plt.boxplot(values_by_q_by_ppty['volume'].values())
        """
        if properties is None:
            properties = self._global_pw_dist_variables
        return {ppty: self._property_values_by_cluster(ppty, default) for ppty in properties}

    def _check_color_by_cluster(self, cmap, cluster_ids):
        """Check the correct definition of the colormap to use with the clusters.

        Parameters
        ----------
        cmap : dict
            Cluster indexed dictionary of RGBA colors.
        cluster_ids : list
            List of cluster ids to use.

        Returns
        -------
        dict
            Cluster indexed colormap dictionary.
        """
        default_cmap = self.get_color_by_cluster('viridis')  # default colormap
        if isinstance(cmap, str):
            color_by_cluster = self.get_color_by_cluster(cmap)
        elif isinstance(cmap, dict):
            try:
                assert all(q in cmap for q in cluster_ids)
            except AssertionError:
                log.error(f"Missing color for clusters: {[q not in cmap for q in cluster_ids]}")
                log.info("Falling back to default with a 'viridis' colormap!")
                color_by_cluster = default_cmap
            else:
                color_by_cluster = copy.deepcopy(cmap)
        else:
            log.error(f"Wrong type of colormap: {clean_type(cmap)}!")
            log.info("Falling back to default with a 'viridis' colormap!")
            color_by_cluster = default_cmap
        return color_by_cluster

    def get_color_by_cluster(self, cmap='viridis'):
        """Create a dictionary of RGBA colors associated to each cluster.

        Parameters
        ----------
        cmap : str, optional
            Name of the matplotlib colormap to use.

        Returns
        -------
        dict
            The dictionary of RGBA colors associated to each cluster.

        Examples
        --------
        >>> from sklearn.preprocessing import RobustScaler
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> # Register the "volume" as spatial variable and the "log_relative_value('volume',1)" as temporal variable:
        >>> clust.add_spatial_variable('volume')
        >>> clust.add_temporal_variable("log_relative_value('volume',1)")
        >>> # Detect the outliers using LOF on quantile scaled and median centered data:
        >>> scaler = RobustScaler(quantile_range=(10., 90.)).fit_transform
        >>> outlier_ids = clust.detect_outlier_cells(scaler=scaler)
        >>> # Assemble a global pairwise distance matrix, excluding the outliers:
        >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5], delete_outliers=True)
        >>> # Cluster it with a hierarchical agglomerative clustering method with a "ward" linkage:
        >>> clust.compute_clustering(method="ward", n_clusters=5)
        >>> # Now you can get a cluster indexed RGBA color dictionary as follows:
        >>> clust.get_color_by_cluster()
        {1: [0.267004, 0.004874, 0.329415, 1.0],
         2: [0.229739, 0.322361, 0.545706, 1.0],
         3: [0.127568, 0.566949, 0.550556, 1.0],
         4: [0.369214, 0.788888, 0.382914, 1.0],
         5: [0.993248, 0.906157, 0.143936, 1.0],
         -1: [0.3411764705882353, 0.3411764705882353, 0.3411764705882353, 1.0]}
        """
        if self._clustering is None:
            log.error("No clustering have been computed yet!")
            return {}

        import matplotlib.colors as colors
        from timagetk.visu.util import colors_array
        # Take the colors of the clusters in the given `cmap`:
        cm_rgba = colors_array(cmap, self._nb_clusters).tolist()
        cmap_dict = dict(zip(range(1, self._nb_clusters + 2), cm_rgba))
        # Set the color of the outlier cluster (#575757 = dark gray)
        cmap_dict[self.outliers_cluster_id] = list(colors.hex2color('#575757'))+[1.0]
        return cmap_dict

    def boxplot_properties_by_cluster(self, exclude_outlier_cluster=True, cmap="Set1", **kwargs):
        """Creates boxplot of properties (used for clustering) by clusters.

        Parameters
        ----------
        exclude_outlier_cluster : bool, optional
            Wheter to exclude the outliers cluster from the representation.
            Defaults to ``True``.
        cmap : str or dict, optional
            Name of the matplotlib colormap to use to color the cluster boxes.
            Or cluster indexed dictionary of RGBA colors.

        Other Parameters
        ----------------
        figname : str or None
            If defined, should be a valid filename, used to save the figure.
        include_outlier_cluster : bool
            If ``True`` include the oulier cluster to the boxplots.
            Defaults to ``False``.
        dpi : int
            The number of dot per inch (dpi) to use to create the figure. Defaults to ``96``.
        subplots_adjust : dict
            The dictionary indicating the adjustement to perform to the margins by ``plt.subplots_adjust``.

        Returns
        -------
        matplotlib.figure.Figure
            The `Figure` object.
        matplotlib.axes.Axes
            The `Axes` object.

        Examples
        --------
        >>> import matplotlib.pyplot as plt
        >>> from sklearn.preprocessing import RobustScaler
        >>> from timagetk.components.clustering import example_clusterer
        >>> clust = example_clusterer("p58", pre_compute=False)
        >>> # Register the "volume" as spatial variable and the "log_relative_value('volume',1)" as temporal variable:
        >>> clust.add_spatial_variable('volume')
        >>> clust.add_temporal_variable("log_relative_value('volume',1)")
        >>> # Detect the outliers using LOF on quantile scaled and median centered data:
        >>> scaler = RobustScaler(quantile_range=(10., 90.)).fit_transform
        >>> outlier_ids = clust.detect_outlier_cells(scaler=scaler)
        >>> # Assemble a global pairwise distance matrix, excluding the outliers:
        >>> clust.assemble_matrix(['volume', "log_relative_value('volume',1)"], [0.5, 0.5], delete_outliers=True)
        >>> # Cluster it with a hierarchical agglomerative clustering method with a "ward" linkage:
        >>> clust.compute_clustering(method="ward", n_clusters=5)
        >>> # Now you can plot the feature boxplots per cluster as follows:
        >>> fig, axes = clust.boxplot_properties_by_cluster()
        >>> plt.show()
        """
        from timagetk.visu.features import _boxplot_property_by_group
        include_outlier_cluster = kwargs.get('include_outlier_cluster', False)
        figname = kwargs.get('figname', None)
        # Matplotlib keyword arguments:
        dpi = kwargs.get('dpi', 96)
        subplots_adjust = kwargs.get('subplots_adjust', {'top': 0.9, 'wspace': 0.3})

        if self._clustering is None:
            log.error("No clustering have been computed yet!")
            return

        cluster_ids = self.list_cluster_ids(exclude_outlier_cluster)
        if include_outlier_cluster:
            cluster_ids |= self.outliers_cluster_id

        if isinstance(cmap, str):
            cmap = self.get_color_by_cluster(cmap)

        n_ppties = len(self.list_gpdm_features())
        fig, axes = plt.subplots(ncols=n_ppties, figsize=(5 / 3. * self._nb_clusters * n_ppties, 7), dpi=dpi)

        for n, ppty in enumerate(self.list_gpdm_features()):
            axes[n] = _boxplot_property_by_group(axes[n], self.graph, self.clustering_name(), ppty, cmap,
                                                 default_group_id=self.outliers_cluster_id,
                                                 group_id_subset=cluster_ids)
            axes[n].set_xlabel('')
        plt.suptitle(self.clustering_name())

        plt.subplots_adjust(**subplots_adjust)

        if isinstance(figname, str):
            plt.savefig(figname)
            plt.close()

        return fig, axes

    # -------------------------------------------------------------------------
    # Post-clustering modifications
    # -------------------------------------------------------------------------

    def group_clusters(self, domains2group, domain_names=None, make_contiguous_ids=True):
        # FIXME!
        """
        """
        return self.group_domains(domains2group, domain_names, make_contiguous_ids)

    def group_domains(self, domains2group, domain_names=None, make_contiguous_ids=True):
        # FIXME!
        """Function modifying clusterer._clustering by grouping ids.

        Parameters
        ----------
        domains2group : list
            List of strings or ids naming domains (at least 2) to group, should be referenced in self.graph.graph_property()
        """
        assert len(domains2group) >= 2
        if isinstance(domains2group[0], str):
            assert domain_names is not None
            domains2group_index = [domain_names.index(r_name) for r_name in domains2group]
        else:
            domains2group_index = domains2group

        # - Now we replace the domains2group ids with the min of their ids
        cid = min(domains2group_index)
        domains_left = list(set(domains2group_index) - {cid})
        clust = {}
        for k, v in self._clustering.items():
            clust[k] = cid if v in domains_left else v

        if make_contiguous_ids:
            clust = contiguous_ids(clust, 0)

        self._clustering = clust
        self._nb_clusters = len(np.unique(self._clustering.values()))

        return 'Done grouping domain!'

    def clusters_from_domains(self, domain_names, tcids=None, method_name='expert'):
        # FIXME!
        """Function creating a clusterer-like object from defined domains (in `domain_names`).
        The str in `domain_names` should be in tg.graph_property().

        Parameters
        ----------
        domain_names : list
            List of strings referenced in self.graph.graph_property()
        tcids : list
            (Optional) list of ids to use when creating the clustering from domains
        """
        # -- Creating the list of vertices:
        if not tcids:
            tcids = self.ids
        elif tcids == 'lineaged':
            tcids = [k for k in self.ids if k in self.graph.lineaged_vertex(False)]
        elif tcids == 'fully lineaged':
            tcids = [k for k in self.ids if k in self.graph.lineaged_vertex(True)]
        else:
            assert isinstance(tcids, list)

        self._method = method_name
        self._clustering = {}
        for n, r_name in enumerate(domain_names):
            r_labels = self.graph.graph_property(r_name)
            self._clustering.update({vid: n for vid in tcids if vid in r_labels})

        self._nb_clusters = len(np.unique(self._clustering.values()))
