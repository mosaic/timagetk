#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Base image data structure."""

import copy as cp
from collections.abc import Iterable

import numpy as np
import vt
from pint import UnitRegistry
from vt.image import Image

from timagetk.bin.logger import get_logger
from timagetk.components.metadata import IMAGE_MD_TAGS
from timagetk.components.metadata import ImageMetadata
from timagetk.components.metadata import Metadata
from timagetk.components.metadata import ProcessMetadata
from timagetk.util import _to_list
from timagetk.util import check_dimensionality
from timagetk.util import check_type
from timagetk.util import compute_extent
from timagetk.util import compute_voxelsize
from timagetk.util import dimensionality_test

#: Default axes order for 2D image.
DEFAULT_AXES_2D = "YX"
#: Default axes order for 3D image.
DEFAULT_AXES_3D = "ZYX"
#: Dictionary of default axes order for 2D image.
DEFAULT_AXIS_ORDER_2D = {ax: ax_id for ax_id, ax in enumerate(DEFAULT_AXES_2D)}
#: Dictionary of default axes order for 3D image.
DEFAULT_AXIS_ORDER_3D = {ax: ax_id for ax_id, ax in enumerate(DEFAULT_AXES_3D)}
#: Default voxels size for 2D image.
DEFAULT_VXS_2D = [1.0, 1.0]
#: Default voxels size for 3D image.
DEFAULT_VXS_3D = [1.0, 1.0, 1.0]
#: Default origin for 2D image.
DEFAULT_ORIG_2D = [0, 0]
#: Default origin for 3D image.
DEFAULT_ORIG_3D = [0, 0, 0]
#: Default image size unit is micrometer (1e-6).
DEFAULT_SIZE_UNIT = 1e-6

#: List of protected image attribute or property.
PROTECT_PPTY = ['shape', 'min', 'max', 'mean']

DEFAULT_VALUE_MSG = "Set '{}' to default value: {}"

log = get_logger(__name__)


def default_origin(input_array):
    """Returns the default origin depending on the array dimensionality.

    Parameters
    ----------
    input_array : numpy.ndarray
        2D or 3D array defining an image, *e.g.* intensity or labelled image.

    Returns
    -------
    list
        Default origin coordinates.
    """
    if input_array.ndim == 2:
        orig = DEFAULT_ORIG_2D
    else:
        orig = DEFAULT_ORIG_3D
    log.debug(DEFAULT_VALUE_MSG.format("origin", orig))

    return orig


def default_voxelsize(input_array):
    """Returns the default pixel or voxel size depending on the array dimensionality.

    Parameters
    ----------
    input_array : numpy.ndarray
        2D or 3D array defining an image, *e.g.* intensity or labelled image.

    Returns
    -------
    list
        Default voxel size.
    """
    if input_array.ndim == 2:
        vxs = DEFAULT_VXS_2D
    else:
        vxs = DEFAULT_VXS_3D
    log.debug(DEFAULT_VALUE_MSG.format("voxelsize", vxs))

    return vxs


def default_axes_order(input_array):
    """Returns the default physical axes order depending on the array dimensionality.

    Parameters
    ----------
    input_array : numpy.ndarray
        2D or 3D array defining an image, *e.g.* intensity or labelled image.

    Returns
    -------
    str
        Default physical axes ordering.
    """
    if input_array.ndim == 2:
        axo = DEFAULT_AXES_2D
    else:
        axo = DEFAULT_AXES_3D
    log.debug(DEFAULT_VALUE_MSG.format("axes order", axo))

    return axo


def compare_kwargs_metadata(kwd_val, md_val, dim, kwd_name):
    """Compare two values, usually keyword or attribute against metadata.

    Parameters
    ----------
    kwd_val : any
        Keyword or attribute value.
    md_val : any
        Metadata value.
    dim : int
        Dimensionality of the array.
    kwd_name : str
        Name of the compared keyword argument and metadata.

    Returns
    -------
    any|None
        value
    """
    if kwd_val != md_val:
        msg = f"Keyword argument '{kwd_name}' ({kwd_val}) differ from metadata ({md_val})"
        msg += "Using keyword argument by default!"
        log.warning(msg)

    return check_dimensionality(dim, kwd_val)


class SpatialImage(np.ndarray):
    """Data structure for 2D and 3D images.

    A ``SpatialImage`` combines a NumPy array with metadata, such as voxel size, physical extent, origin, type, etc.
    Inheriting from ``numpy.ndarray`` allows all standard operations possible on
    `numpy.ndarray <http://docs.scipy.org/doc/numpy-1.10.1/reference/generated/numpy.ndarray.html>`_
    objects (like sum, product, transposition, etc.) to be utilized. All image processing operations
    are executed on this structure, which also handles input (read) and output (write) operations.

    We `subclass <https://docs.scipy.org/doc/numpy-1.14.0/user/basics.subclassing.html>`_ ndarray
    using view casting in the ``__new__`` section. View casting is the standard ``ndarray`` mechanism
    by which you take an ``ndarray`` of any subclass and return a view as another (specified) subclass,
    here, a ``SpatialImage``.

    .. warning::
      * Modifying a property, when possible, changes the object instead of returning a new one.
      * The attributes ``shape`` and ``ndim`` are determined by the input array.
      * Specifying ``dtype`` at object instantiation changes the array dtype to the specified one.
      * Image tags can be set or updated using a metadata dictionary.

    Attributes
    ----------
    _extent : list of float
        The physical extent of the image, sorted as (planes,) rows & columns [(Z,) Y, X].
    _origin : list of int
        The coordinates of the origin, sorted as (planes,) rows & columns [(Z,) Y, X].
    _metadata : dict
        A dictionary containing metadata.
    _voxelsize : list of float
        The size of the pixels or voxels (2D/3D), sorted as (planes,) rows & columns [(Z,) Y, X].
    axes_order : dict
        The order of the axes, for example, {'x': 2, 'y': 1, 'z': 0} by default for a 3D image.
    filepath : str
        The directory location of the image, if applicable.
    filename : str
        The file name of the image.
    metadata_image : ImageMetadata
        A self-generated list of attributes containing basic image information such as 'shape', 'ndim', 'dtype',
        'origin', 'voxelsize', and 'extent'. Refer to the ``ImageMetadata`` class documentation in
        ``timagetk.components.metadata``.
    metadata_acquisition : Metadata
        Acquisition metadata, a list of attributes related to the measurement settings, machine setup, or even
        the equipment and parameters used. Use the 'scan_info' key in the ``metadata`` dictionary to pass this to the constructor.
    metadata_processing : ProcessMetadata
        A self-generated hierarchical dictionary of algorithms applied to the image. It includes process names,
        the parameters used and their values, and any sub-processes called with the same level of information.

    .. todo::
        * Add a ``metric_unit`` attribute and associated methods.
        * Define a ``personal_metadata`` attribute to store additional metadata.
    """

    def __new__(cls, array, origin=None, voxelsize=None, dtype=None, metadata=None, **kwargs):
        """Data structure for 2D and 3D images.

        Parameters
        ----------
        array : numpy.ndarray or timagetk.SpatialImage
            A 2D (YX) or 3D (ZYX) array defining an image, such as an intensity or labelled image.
        origin : list, optional
            The (Z)YX coordinates of the origin in the image. Defaults to ``[0, 0]`` for 2D images or ``[0, 0, 0]`` for 3D images.
        voxelsize : list, optional
            Specifies (Z)YX image voxel sizes. Defaults to ``[1.0, 1.0]`` for 2D images or ``[1.0, 1.0, 1.0]`` for 3D images.
        dtype : str, optional
            If specified, should be one of the types in ``AVAIL_TYPES`` and will change the input `array` type.
        metadata : dict, optional
            A dictionary of image metadata. Defaults to an empty dictionary.

        Other Parameters
        ----------------
        unit : float, optional
            The size unit in International System of Units (meters) linked to the image.
        axes_order : dict, optional
            A dictionary defining axes order, such as {'x': 2, 'y': 1, 'z': 0}, which is the default for 3D images.

        Returns
        -------
        timagetk.SpatialImage
            An image that includes metadata.

        See Also
        --------
        timagetk.components.spatial_image.AVAIL_TYPES

        Notes
        -----
        By default, NumPy arrays are C-contiguous and indexed starting at ``0``.
        This convention is followed, meaning 2D images are sorted by column and then by row.
        For 3D images, planes are indexed first, meaning to access plane 3 of a ``SpatialImage`` named ``img``, use: ``plane = img[2]``.
        To use the array's dtype from NumPy, leave ``dtype`` as ``None``.

        Example
        -------
        >>> from timagetk.io.dataset import shared_data
        >>> from timagetk import SpatialImage
        >>> spim = shared_data('flower_confocal', 0)
        >>> im = SpatialImage(spim, voxelsize=[1., 1., 1.])

        >>> from timagetk.array_util import random_spatial_image
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], unit=1e-6)
        >>> isinstance(img, SpatialImage)  # Test `SpatialImage` type
        True
        >>> isinstance(img, np.ndarray)  # Test `np.ndarray` type => INHERITANCE
        True
        >>> print(img.voxelsize)  # Access the voxel size information
        [1., 0.5, 0.5]
        >>> print(img.unit)  # Access the unit information
        micrometer
        >>> print(img.shape)  # Access the plane information
        (3, 5, 5)
        >>> img[0]  # Access the first plane
        """
        from timagetk.third_party.vt_converter import _new_from_vtimage
        from timagetk.third_party.vt_converter import attr_from_spatialimage

        # - Accept ``SpatialImage`` or ``numpy.ndarray`` instances:
        log.debug(f"``SpatialImage`` got a ``{type(array)}`` as input!")
        kwargs['origin'] = origin
        kwargs['voxelsize'] = voxelsize
        kwargs['metadata'] = metadata
        if isinstance(array, SpatialImage):
            # - Override keyword arguments if defined as attributes in `array` instance:
            kwargs |= attr_from_spatialimage(array, **kwargs)
        elif isinstance(array, vt.vtImage):
            # Do NOT assign to `origin` & `metadata` variables to avoid overriding input!
            # Furthermore, vtImage does not (yet?) have those attributes.
            array, voxelsize, _, _ = _new_from_vtimage(array, copy=False)
            kwargs['voxelsize'] = voxelsize
        else:
            # - Test input array type, should be a numpy array :
            check_type(array, 'array', np.ndarray)

        # - Take care of image attributes:
        from timagetk.io.image import default_image_attributes
        kwargs |= default_image_attributes(array, log_undef=log.warning, **kwargs)

        # ----------------------------------------------------------------------
        # ARRAY:
        # ----------------------------------------------------------------------
        # - Test input array dimensionality, should be of dimension 2 or 3:
        try:
            assert array.ndim in [2, 3]
        except AssertionError:
            msg = "Input 'array' must be 2D or 3D!\n"
            for attr in ['ndim', 'shape', 'dtype']:
                try:
                    msg += "Got '{}': {}\n".format(attr, getattr(array, attr))
                except:
                    pass
            raise ValueError(msg)
        # - View casting (see class documentation) of the array as a SpatialImage subclass:
        # NOTE: it converts the array to given dtype if not None:
        # This call ``__array_finalize__``!
        if array.flags.c_contiguous:
            obj = np.asarray(array, dtype=dtype).view(cls)
        else:
            obj = np.ascontiguousarray(array, dtype=dtype).view(cls)

        # ----------------------------------------------------------------------
        # METADATA:
        # ----------------------------------------------------------------------
        metadata = kwargs['metadata']
        # - Get acquisition metadata using 'scan_info' key:
        scan_info = metadata.pop('scan_info', {})
        obj.metadata_acquisition = Metadata(scan_info)
        if obj.metadata_acquisition is None:
            msg = "Could not set 'metadata_acquisition' attribute!"
            raise AttributeError(msg)

        # - Get ImageMetadata:
        image_metadata = ImageMetadata(array, **kwargs)
        # - Update the metadata dictionary for image IMAGE_MD_TAGS & extent:
        metadata = image_metadata.update_metadata(metadata)
        if 'acquisition_date' in metadata:
            image_metadata.update({'acquisition_date': metadata['acquisition_date']})
        obj.metadata_image = image_metadata
        obj._metadata = {}

        # - Set 'axes_order', 'voxelsize', 'origin', 'extent' & 'unit' hidden attributes:
        obj._axes_order = image_metadata['axes_order']
        obj._voxelsize = _to_list(image_metadata['voxelsize'])
        obj._origin = _to_list(image_metadata['origin'])
        obj._extent = _to_list(image_metadata['extent'])
        obj._unit = image_metadata['unit']
        obj._unit_reg = UnitRegistry()  # initialize unit registry

        # - Get processing metadata :
        obj.metadata_processing = {}

        # -- Set the 'class' metadata if not defined:
        try:
            assert 'class' in obj.metadata_processing
        except AssertionError:
            obj.metadata_processing.update({'class': 'SpatialImage'})
        else:
            # DO NOT try to change returned class, eg. 'LabelledImage', ...
            pass

        # - Defines some attributes:
        # -- File name and path, if known:
        obj.filename = kwargs.get('filename', metadata.get('filename', '?'))
        obj.filepath = kwargs.get('filepath', metadata.get('filepath', '?'))

        return obj

    def __array_finalize__(self, obj):
        """Mechanism provided by NumPy to allow subclasses instanciation.

        Parameters
        ----------
        obj : object
            The object returned by the ``__new__`` method.

        Examples
        --------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> type(img)
        timagetk.SpatialImage
        >>> # Taking the first z-slice automatically return a SpatialImage instance:
        >>> img2d = img[0]
        >>> type(img2d)
        timagetk.SpatialImage
        """
        self._axes_order = getattr(obj, '_axes_order', None)
        self._origin = getattr(obj, '_origin', None)
        self._voxelsize = getattr(obj, '_voxelsize', None)
        self._extent = getattr(obj, '_extent', None)
        self._unit_reg = getattr(obj, '_unit_reg', None)
        self._unit = getattr(obj, '_unit', None)

        self.filename = getattr(obj, 'filename', None)
        self.filepath = getattr(obj, 'filepath', None)

        self._metadata = getattr(obj, '_metadata', None)
        self.metadata_image = getattr(obj, 'metadata_image', None)
        self.metadata_acquisition = getattr(obj, 'metadata_acquisition', None)
        self.metadata_processing = getattr(obj, 'metadata_processing', None)

        # Case where we are doing view casting of a SpatialImage and we changed the array:
        # md_img = self.metadata_image

        # View Casting DEBUG:
        # if md_img is not None:
        #     print("Got image metadata: {}".format(md_img.get_dict()))
        #     print("With md_img['shape']: {}".format(md_img['shape']))
        #     print("With obj.shape: {}".format(obj.shape))
        # -----

        # if md_img is not None and md_img['shape'] != obj.shape:
        #     # Update image metadata:
        #     self.metadata_image.get_from_image(obj)
        #     self.metadata = self.metadata_image.update_metadata(self.metadata)
        #     # Reset some attributes
        #     for attr in ['min', 'max', 'mean']:
        #         setattr(self, '_' + attr, None)

    def __str__(self):
        """Print information about the object."""
        msg = "SpatialImage object with following metadata:\n"
        md = self.metadata_image.get_dict()
        msg += '\n'.join(['   - {}: {}'.format(k, v) for k, v in md.items()])
        return msg

    def __setstate__(self, state, /):
        """Restore the state of the object for deserialization with pickle"""
        sp_state, array_state = state
        super().__setstate__(array_state)

        self.__dict__.update(sp_state[0])

        self.metadata_image = ImageMetadata(self)
        self.metadata_image.update(sp_state[1])

        self.metadata_acquisition = Metadata()
        self.metadata_acquisition.update(sp_state[2])

        self._unit_reg = UnitRegistry()

    def __reduce_ex__(self, __protocol):
        """Returns reconstruction info for pickle serialization"""
        reconstructor, reconstructor_args, state = super().__reduce_ex__(__protocol)
        state_dict = {
            key: val for key, val in self.__dict__.items()
            if key not in ["metadata_image", "metadata_acquisition", "_unit_reg"]
        }
        sp_state = (state_dict, self.metadata_image.get_dict(), self.metadata_acquisition.get_dict())
        new_state = (sp_state, state)
        return reconstructor, reconstructor_args, new_state

    # --------------------------------------------------------------------------
    #
    # Conversion:
    #
    # --------------------------------------------------------------------------

    def to_vt_image(self):
        """Return the same object converted to a ``vt.image.Image`` instance.

        Returns
        -------
        vt.image.Image
            A `vt.image.Image` object.

        Examples
        --------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 4, 5), voxelsize=[1., 0.51, 0.5], dtype='uint8')
        >>> vt_im = img.to_vt_image()
        >>> from vt import vtImage
        >>> isinstance(vt_im, vtImage)
        True
        >>> vt_im.shape()
        >>> vt_im.spacing()

        >>> # Initialize a random (uint8) 2D SpatialImage:
        >>> img = random_spatial_image((4, 5), voxelsize=[0.51, 0.5], dtype='uint8')
        >>> vt_im = img.to_vt_image()
        >>> from vt import vtImage
        >>> isinstance(vt_im, vtImage)
        True
        >>> vt_im.shape()
        >>> vt_im.spacing()
        """
        return Image(self, self.voxelsize)

    def get_array(self, dtype=None):
        """Returns a copy of the image as a NumPy array.

        Parameters
        ----------
        dtype : str or numpy.dtype, optional
            Specifies the desired data type for the returned array. The default is ``None``.

        Returns
        -------
        numpy.ndarray
            A NumPy array linked to the object, sorted in (Z)YX order.

        Notes
        -----
        Changing the `dtype` of the array forces the function to return a copy.

        Examples
        --------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> from timagetk import SpatialImage
        >>> isinstance(img, SpatialImage)
        True
        >>> # Get the array linked to the SpatialImage:
        >>> array = img.get_array()
        >>> isinstance(array, SpatialImage)
        False
        >>> import numpy as np
        >>> isinstance(array, np.ndarray)
        True

        >>> # Return the pointer:
        >>> array.fill(0)
        >>> print(array)
        >>> print(img.get_array())
        """
        return np.asarray(self, dtype=dtype).copy()

    def get_xyz_array(self, dtype=None):
        """Retrieve a copy of the image as a XY(Z) sorted NumPy array.

        Parameters
        ----------
        dtype : str or numpy.dtype, optional
            If specified, it changes the data type of the returned array.
            The default value is ``None``.

        Returns
        -------
        numpy.ndarray
            A NumPy array associated with the SpatialImage, sorted in XY(Z) order.

        .. deprecated::
            This method will be removed in the next major release.

        Examples
        --------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> from timagetk import SpatialImage
        >>> isinstance(img, SpatialImage)
        True
        >>> # Get the array linked to the SpatialImage:
        >>> array = img.get_xyz_array()
        >>> print(array.shape)
        """
        import warnings
        warnings.warn("This method maintain legacy compatibility with timagetk version anterior to 3!",
                      DeprecationWarning)
        ax = self.axes_order_dict
        if self.is2D():
            return self.get_array(dtype).transpose((ax['X'], ax['Y']))
        else:
            return self.get_array(dtype).transpose((ax['X'], ax['Y'], ax['Z']))

    # --------------------------------------------------------------------------
    #
    # SpatialImage properties:
    #
    # --------------------------------------------------------------------------

    @property
    def axes_order(self):
        """Retrieves the ordering of the physical axes of the image.

        Returns
        -------
        str
            The ordering of the image's physical axes.

        Example
        -------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> print(img.axes_order)
        ZYX
        """
        return cp.copy(self._axes_order)

    @property
    def axes_order_dict(self):
        """Retrieves the ordering of the physical axes of the image.

        Returns
        -------
        dict
            The ordering of the image's physical axes, with axes names as keys and axes index as values.

        Example
        -------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> print(img.axes_order_dict)
        {'Z': 0, 'Y': 1, 'X': 2}
        """
        return {ax: axi for axi, ax in enumerate(self._axes_order)}

    @property
    def extent(self):
        """Retrieves the physical extent of the image.

        Returns
        -------
        list
            The physical extent of the image origin, ordered as ZYX (planes, columns, and rows).

        Notes
        -----
        It is related to the image shape and voxel size.

        Example
        -------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> print(img.extent)
        [3.0, 2.5, 2.5]
        """
        return cp.copy(self._extent)

    @extent.setter
    def extent(self, img_extent):
        """Set the physical extent of the image.

        Parameters
        ----------
        img_extent : list
            The physical extent of the image origin, ordered as ZYX (planes, columns, and rows).

        Notes
        -----
        Do NOT use this method unless you are CERTAIN of what you are doing!
        This will modify the voxel size based on the array shape.
        The metadata are updated according to the new physical extent and voxel size.

        Example
        -------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5),voxelsize=[1., 0.5, 0.5],dtype='uint8')
        >>> print(img.voxelsize)
        [1.0, 0.5, 0.5]
        >>> img.extent = [10., 10., 10.]
        >>> print(img.extent)
        [10., 10., 10.]
        >>> print(img.voxelsize)
        [3.3333333333333335, 2.0, 2.0]
        """
        log.warning("You are changing a physical property of the image!")
        log.warning("This should not happen!")
        log.warning("You better know what you are doing!")

        dimensionality_test(self.ndim, img_extent)
        # - Update 'extent' hidden attribute:
        self._extent = _to_list(img_extent)
        # - Update 'voxelsize' hidden attribute:
        self._voxelsize = compute_voxelsize(self.shape, self.extent)
        # - Update 'extent' & 'voxelsize' metadata:
        self.metadata_image['extent'] = self.extent
        self.metadata_image['voxelsize'] = self.voxelsize
        log.info("Set extent to '{}'".format(self.extent))
        log.info("Changed voxelsize to '{}'".format(self.voxelsize))
        return

    @property
    def origin(self):
        """Retrieve coordinates of the image origin.

        Returns
        -------
        list
            The coordinates of the image origin, sorted as ZYX (planes, columns, and rows).

        Example
        -------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5),voxelsize=[1., 0.5, 0.5],dtype='uint8')
        >>> print(img.origin)
        [0, 0, 0]
        """
        return cp.copy(self._origin)

    @origin.setter
    def origin(self, img_origin):
        """Set the image origin.

        The given list should have the same length as the image's dimensionality.

        Parameters
        ----------
        img_origin : list
            The coordinates of the image origin, sorted as ZYX (planes, columns, and rows).

        Example
        -------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5),voxelsize=[1., 0.5, 0.5],dtype='uint8')
        >>> print(img.origin)
        [0, 0, 0]
        >>> img.origin = [2, 2, 2]
        >>> print(img.origin)
        [2, 2, 2]
        """
        log.warning("You are changing a physical property of the image!")
        log.warning("This should not happen!")
        log.warning("You better know what you are doing!")

        dimensionality_test(self.ndim, img_origin)
        # - Update hidden attribute 'origin':
        self._origin = _to_list(img_origin)
        # - Update hidden attribute metadata key 'origin':
        self.metadata_image['origin'] = self.origin
        log.info("Set origin to '{}'".format(self.origin))
        return

    @property
    def voxelsize(self):
        """Retrieve the voxel size of an image.

        Returns
        -------
        list
            The voxel size of the image, sorted as ZYX (planes, columns, and rows).

        Example
        -------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5),voxelsize=[1., 0.5, 0.5],dtype='uint8')
        >>> print(img.voxelsize)
        [1., 0.5, 0.5]
        """
        return cp.copy(self._voxelsize)

    @voxelsize.setter
    def voxelsize(self, img_vxs):
        """Change the image voxel size while preserving the 'extent'.

        Parameters
        ----------
        img_vxs : list
            The new voxel size for the image, sorted as ZYX (planes, columns, and rows).

        Notes
        -----
        Do NOT use this method unless you know EXACTLY what you are doing!
        This will change the voxel size based on the array shape.
        The metadata is updated according to the new physical extent and voxel size.

        Example
        -------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> print(img.voxelsize)
        [1., 0.5, 0.5]
        >>> print(img.extent)
        [3.0, 2.5, 2.5]
        >>> print(img.shape)
        (3, 5, 5)
        >>> # - Change img voxelsize:
        >>> img.voxelsize = [0.5, 0.5, 0.5]
        >>> print(img.voxelsize)
        [0.5, 0.5, 0.5]
        >>> print(img.extent)
        [1.5, 2.5, 2.5]
        >>> print(img.shape)
        (3, 5, 5)
        """
        log.warning("You are changing a physical property of the image!")
        log.warning("This should not happen!")
        log.warning("You better know what you are doing!")

        dimensionality_test(self.ndim, img_vxs)
        # - Update 'voxelsize' hidden attribute:
        self._voxelsize = _to_list(img_vxs)
        # - Update 'extent' hidden attribute:
        self._extent = compute_extent(self.voxelsize, self.shape)
        # - Update 'extent' & 'voxelsize' metadata:
        self.metadata_image['voxelsize'] = self.voxelsize
        self.metadata_image['extent'] = self.extent
        log.info("Set voxelsize to '{}'".format(self.voxelsize))
        log.info("Changed extent to '{}'".format(self.extent))
        return

    @property
    def unit(self):
        """Returns the unit in the International System of Units (SI).

        Returns
        -------
        float
            The unit in the International System of Units (SI).
        """
        return cp.copy(self._unit)

    @unit.setter
    def unit(self, unit):
        """Set the unit of the image size in the International System of Units (SI).

        Parameters
        ----------
        unit : int or float
            The unit, in meters, associated with the image.

        Examples
        --------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> print(img.get_unit())  # default unit is micrometer
        'micrometer'
        >>> img.unit = 1e-3  # change to millimeter
        >>> print(img.get_unit())
        'millimeter'
        """
        self._unit = unit

    @property
    def metadata(self):
        """Retrieves the image metadata attribute.

        Returns
        -------
        dict
            A dictionary containing image metadata.

        Example
        -------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5),voxelsize=[1., 0.5, 0.5],dtype='uint8',metadata={'info': "test OK"})
        >>> print(img.metadata['info'])
        'test OK'
        >>> print(img.metadata)
        {'info': 'test OK',
         'shape': (3, 5, 5),
         'ndim': 3,
         'dtype': dtype('uint8'),
         'origin': [0, 0, 0],
         'voxelsize': [1.0, 0.5, 0.5],
         'get_extent': [3.0, 2.5, 2.5]}
        """
        md = {}
        if isinstance(self.metadata_image, ImageMetadata):
            md.update(self.metadata_image.get_dict())
        if isinstance(self.metadata_acquisition, Metadata):
            acq = self.metadata_acquisition.get_dict()
            if acq != {}:
                md['acquisition'] = acq
        if isinstance(self.metadata_processing, ProcessMetadata):
            proc = self.metadata_processing.get_dict()
            if proc != {}:
                md['processing'] = proc

        self._metadata.update(md)
        return cp.copy(self._metadata)

    @metadata.setter
    def metadata(self, img_md):
        """Updates the metadata attribute with the given image metadata.

        Parameters
        ----------
        img_md : dict
            A dictionary containing image metadata to be merged with the existing `_metadata` attribute.

        Example
        -------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5),voxelsize=[1., 0.5, 0.5],dtype='uint8')
        >>> print(img.metadata)
        {'shape': (3, 5, 5),
         'ndim': 3,
         'dtype': dtype('uint8'),
         'origin': [0, 0, 0],
         'voxelsize': [1.0, 0.5, 0.5],
         'get_extent': [3.0, 2.5, 2.5]}
        >>> img.metadata = {'info': "test OK"}
        >>> print(img.metadata)
        {'shape': (3, 5, 5),
         'ndim': 3,
         'dtype': dtype('uint8'),
         'origin': [0, 0, 0],
         'voxelsize': [1.0, 0.5, 0.5],
         'get_extent': [3.0, 2.5, 2.5],
         'info': 'test OK'}
        """
        self._metadata.update(img_md)

    # --------------------------------------------------------------------------
    #
    # SpatialImage methods :
    #
    # --------------------------------------------------------------------------

    def is_isometric(self):
        """Determines whether the image is isometric.

        Tests if the voxelsize values are the same for every axis or not.

        Returns
        -------
        bool
            ``True`` if the image is isometric, otherwise ``False``.

        Examples
        --------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5),voxelsize=[1., 0.5, 0.5],dtype='uint8')
        >>> img.is_isometric()
        False
        >>> from timagetk.algorithms.resample import isometric_resampling
        >>> # Transform to small isometric 3D SpatialImage:
        >>> img_iso = isometric_resampling(img, value='max')
        >>> img_iso.is_isometric()
        True
        >>> img_iso.voxelsize
        [1.0, 1.0, 1.0]
        """
        vxs = [self.voxelsize[0]] * self.ndim
        return np.allclose(vxs, self.voxelsize, atol=1.e-9)

    def is2D(self):
        """Determines whether the image has two dimensions.

        Returns
        -------
        bool
            ``True`` if the image is 2-dimensional, otherwise ``False``.


        Examples
        --------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> img.is2D()
        False
        >>> # Take the first z-slice:
        >>> img2d = img.get_slice(0, 'z')  # Equivalent to img[0]
        >>> img2d.is2D()
        True
        """
        return self.ndim == 2

    def is3D(self):
        """Determines whether the image has three dimensions.

        Returns
        -------
        bool
            `True` if the image is 3-dimensional, otherwise `False`.

        Examples
        --------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> img.is3D()
        True        """
        return self.ndim == 3

    # --------------------------------------------------------------------------
    #
    # GETTERs & SETTERs:
    #
    # --------------------------------------------------------------------------

    def _get_unit(self):
        return (self._unit * self._unit_reg.meter).to_compact()

    def get_unit(self, short=False):
        """Returns the unit of measurement associated with this object in a string format.

        Parameters
        ----------
        short : bool, optional
            Determines the format of the returned unit string. If ``True``,
            the unit is returned in a short form. Default is ``False``.

        Returns
        -------
        str
            The unit of measurement as a string. The format may be short
            or long depending on the `short` parameter.

        Examples
        --------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5])
        >>> print(img.get_unit())
        micrometer
        >>> print(img.get_unit(short=True))
        µm
        """
        if short:
            return f"{self._get_unit().u:P~}"
        else:
            return f"{self._get_unit().u}"

    def get_dtype(self):
        """Returns the name of the data type.

        This method provides the string representation of the data type's name.
        It is useful for retrieving and manipulating the type information
        associated with a particular data instance.

        Returns
        -------
        str
            The name of the data type.
        """
        return self.dtype.name

    def get_extent(self, axis=None):
        """Returns the extent of the image or the specified axis, if provided.

        Parameters
        ----------
        axis : {'x', 'y', 'z'}, optional
            Specifies the axis for which the extent should be returned.

        Returns
        -------
        list of float or float
            The extent of the image or the specified axis, if applicable.
            The list is sorted according to the order of the physical axes.

        Notes
        -----
        The extent of an image is related to its shape and voxel size.

        See Also
        --------
        timagetk.util.compute_extent

        Examples
        --------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5])
        >>> img.get_extent()
        [2., 2., 2.]
        >>> img.get_extent('x')
        2.0
        >>> img.get_extent('y')
        2.0
        >>> img.get_extent('z')
        2.0
        """
        if axis is None:
            return cp.copy(self.extent)
        else:
            return cp.copy(self.extent[self.axes_order_dict[axis.upper()]])

    def get_shape(self, axis=None):
        """Return the shape of the image or the shape of the specified axis, if provided.

        Parameters
        ----------
        axis : {'x', 'y', 'z'}, optional
            The axis for which the shape should be returned, if specified.

        Returns
        -------
        list of int or int
            The shape of the image or the shape of the specified axis,
            if provided. The list is sorted according to the order of the
            physical axes.

        Examples
        --------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> img.get_shape()
        [3, 5, 5]
        >>> img.get_shape('z')
        3
        """
        if axis is None:
            return list(cp.copy(self.shape))
        else:
            try:
                assert axis.lower() in ['x', 'y', 'z']
            except AssertionError:
                raise ValueError("Parameter `axis` take values in {'x', 'y', 'z'}!")
            except AttributeError:
                raise ValueError("Parameter `axis` should be string!")

            return cp.copy(self.shape[self.axes_order_dict[axis.upper()]])

    def get_voxelsize(self, axis=None):
        """Return the voxel size of the image, or of the specified axis, if any.

        Parameters
        ----------
        axis : {'x', 'y', 'z'}, optional
            If specified, the axis for which the voxel size should be returned.

        Returns
        -------
        list of float or float
            The voxel size of the image, or of the selected axis, if specified.
            The list is sorted according to the order of the physical axes.

        Examples
        --------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> img.get_voxelsize()
        [1., 0.5, 0.5]
        >>> img.get_voxelsize('z')
        1.0
        """
        if axis is None:
            return list(cp.copy(self.voxelsize))
        else:
            try:
                assert axis.lower() in ['x', 'y', 'z']
            except AssertionError:
                raise ValueError(
                    "Parameter `axis` take values in {'x', 'y', 'z'}!")
            except AttributeError:
                raise ValueError("Parameter `axis` should be string!")

            return cp.copy(self.voxelsize[self.axes_order_dict[axis.upper()]])

    def set_slice(self, slice_id, array, axis='z'):
        """Set the values of a slice along a specified axis to the given array.

        Parameters
        ----------
        slice_id : int
            The index of the slice to modify.
        array : numpy.ndarray or int
            The values to replace those at the specified slice index. If an integer is provided, an array
            of this value will be created.
        axis : {'x', 'y', 'z'}, optional
            The axis along which to slice. Defaults to 'z'.

        Examples
        --------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> sl = img.get_slice(0, axis='z')
        >>> print(sl.get_array())
        >>> new_sl = random_spatial_image((5, 5), voxelsize=[0.5, 0.5], dtype='uint8')
        >>> img.set_slice(0, new_sl, axis='z')
        >>> print(new_sl)
        >>> print(img.get_slice(0, 'z'))
        """
        sl = self.get_slice(slice_id, axis)
        if isinstance(array, int):
            array = np.ones_like(sl) * array
        try:
            assert sl.shape == array.shape
        except AssertionError:
            msg = "Given array {} has a wrong shape for {}-slice {} {}!"
            raise ValueError(msg.format(array.shape, axis, slice_id, sl.shape))

        ax_id = self.axes_order_dict[axis.upper()]
        if ax_id == 2:
            self[:, :, slice_id] = array
        elif ax_id == 1:
            self[:, slice_id, :] = array
        elif ax_id == 0:
            self[slice_id, :, :] = array
        else:
            raise ValueError("Parameter `axis` take values in {'x', 'y', 'z'}!")

    def _get_slice(self, slice_id, axis):
        """Take one or more slices along the given axis.

        Parameters
        ----------
        slice_id : int or Iterable
            The slice to return.
        axis : int or str in {'x', 'y', 'z'}
            The axis to use for slicing, default is 'z'.

        Returns
        -------
        numpy.ndarray
            A 2D or 3D array containing only the required slice(s).
        dict
            A dictionary containing the new axes ordering for use with the slice(s).
        numpy.ndarray
            The origin of the slice(s).
        numpy.ndarray
            The voxel size of the slice(s).
        dict
            A dictionary containing metadata for the slice(s).

        Raises
        ------
        ValueError
            Raised if the image is not 3D and `axis='z'`.
            Raised if `slice_id` does not exist, which means it should satisfy: `0 < slice_id < max(len(axis))`.
        """
        # -- Correspondence between `axis` string and `axis_idx` index:
        if isinstance(axis, str):
            axis_idx = self.axes_order_dict[axis.upper()]
        else:
            axis_idx = cp.copy(axis)
            axis = {v: k for k, v in self.axes_order_dict.items()}[axis]
        # -- Check `axis` is defined for this image:
        try:
            assert axis.upper() in set(self.axes_order_dict.keys())
        except AssertionError:
            raise ValueError(f"Could not find required {axis.upper()}-axis in this image!")

        # -- If a list with just one integer, ``slice_id`` should be an integer:
        if isinstance(slice_id, Iterable) and len(slice_id) == 1:
            slice_id = slice_id[0]
        # -- Make sure this slice exists:
        max_slice = self.shape[axis_idx]
        if not isinstance(slice_id, Iterable):
            try:
                assert slice_id <= max_slice
            except AssertionError:
                msg = "Required {}-slice ({}) does not exists (max: {})"
                raise ValueError(msg.format(axis, slice_id, max_slice))
        else:
            try:
                assert np.max(slice_id) <= max_slice
            except AssertionError:
                msg = "Required {}-slice range ({}-{}) does not exists (max: {})"
                raise ValueError(msg.format(axis, np.min(slice_id), np.max(slice_id), max_slice))

        # -- Take the slice(s) in the array:
        arr = np.take(self.get_array(), slice_id, axis=axis_idx)

        vxs = self.voxelsize
        ori = self.origin
        md = self.metadata
        if not isinstance(slice_id, Iterable):
            # If ``slice_id`` is an integer, one axis will "disappear"...
            # its info should thus be removed:
            new_axes_order = ''.join([ax for ax, _ in self.axes_order_dict.items() if ax.upper() != axis.upper()])
            # Remove value corresponding to returned slice axis:
            vxs.pop(axis_idx)
            ori.pop(axis_idx)
            # Clear image metadata tags, they will be recomputed by SpatialImage.__new__
            for attr in IMAGE_MD_TAGS + ['extent']:
                md.pop(attr)
        else:
            new_axes_order = self.axes_order.copy()
            # Assume we have a constant compression ratio along the selected axis:
            ratio = self.get_shape(axis) / arr.shape[axis_idx]
            vxs[axis_idx] = vxs[axis_idx] * ratio
            for attr in ['voxelsize', 'extent']:
                md.pop(attr)

        log.debug("Re-slicing image with:")
        log.debug(f"New array shape: {arr.shape}")
        log.debug(f"New ``axes_order``: {new_axes_order}")
        log.debug(f"New ``ori``: {ori}")
        log.debug(f"New ``vxs``: {vxs}")
        log.debug(f"New ``md``: {md}")
        return arr, new_axes_order, ori, vxs, md

    def get_slice(self, slice_id, axis='z'):
        """Return a SpatialImage with only one slice for the given axis.

        Parameters
        ----------
        slice_id : int
            The slice to return.
        axis : int or str in {'x', 'y', 'z'}, optional
            The axis to use for slicing, defaults to 'z'.

        Returns
        -------
        timagetk.SpatialImage
            A 2D SpatialImage containing only the specified slice.

        Raises
        ------
        ValueError
            If the image is not 3D when the axis is 'z'.
            If the slice_id does not exist, i.e., it must satisfy: 0 <= slice_id < max(axis_length).

        Examples
        --------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> print(img.axes_order_dict)
        {'Z': 0, 'Y': 1, 'X': 2}
        >>> print(img.get_shape())
        [3, 5, 5]
        >>> # Taking an existing z-slice from a 3D image works fine:
        >>> img_z = img.get_slice(1, 'z')
        >>> print(img_z.axes_order_dict)
        {'Y': 0, 'X': 1}
        >>> print(img_z.get_shape())
        [5, 5]
        >>> # Taking an existing x-slice from a 3D image works fine:
        >>> img_x = img.get_slice(3, 'x')
        >>> print(img_x.axes_order_dict)
        {'Z': 0, 'Y': 1}
        >>> print(img_x.get_shape())
        [3, 5]

        >>> # Down-sampling x-axis of a 3D image:
        >>> nx = img.get_shape('x')
        >>> img_ds_x2 = img.get_slice(range(0, nx, 2), 'x')

        >>> # Taking an NON-existing z-slice from a 3D image raises an error:
        >>> img.get_slice(50, 'z')

        >>> # Taking a z-slice from a 2D image raises an error:
        >>> img_z.get_slice(5, 'z')
        """
        arr, axes_order, ori, vxs, md = self._get_slice(slice_id, axis)
        return SpatialImage(arr, origin=ori, voxelsize=vxs, metadata=md, unit=self.unit, axes_order=axes_order)

    def get_pixel(self, idx):
        """Retrieve the pixel value of a `SpatialImage` at the specified array coordinates.

        Parameters
        ----------
        idx : list
            A list of integers representing the indices.

        Returns
        -------
        value
            The value of the pixel at the specified indices.

        Raises
        ------
        TypeError
            If the provided `idx` is not a list.
        ValueError
            If the number of indices in `idx` is incorrect; it must match the image's dimensionality.
            If the `idx` coordinates are out of the image's boundaries.

        Example
        -------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> img.get_pixel([1,1])
        """
        check_type(idx, 'idx', list)
        # Check we have the right number of `idx`:
        ndim = self.ndim
        try:
            assert len(idx) == ndim
        except AssertionError:
            msg = "Input `idx` must have a length equal to the image dimensionality!"
            raise ValueError(msg)
        # Check the `ids` are within the image boundaries:
        sh = self.shape
        ids = range(0, ndim)
        try:
            assert all((idx[i] >= 0) & (idx[i] < sh[i] + 1) for i in ids)
        except AssertionError:
            raise ValueError(f"Input `idx` {idx} are not within the image shape {self.shape}!")

        if ndim == 2:
            pix_val = self[idx[0], idx[1]]
        else:
            pix_val = self[idx[0], idx[1], idx[2]]

        return pix_val

    def set_pixel(self, idx, value):
        """Change the pixel or voxel value of the `SpatialImage` at the specified array coordinates.

        Parameters
        ----------
        idx : list
            The array coordinates, provided as a list of integers.
        value : array.dtype
            The new value for the pixel or voxel, which should be compatible with the array's `dtype`.

        Raises
        ------
        TypeError
            Raised if `idx` is not a list.
        ValueError
            Raised if the number of elements in `idx` does not match the image dimensionality.
            Also raised if the `idx` coordinates are not within the image boundaries.

        Example
        -------
        >>> import numpy as np
        >>> from timagetk import SpatialImage
        >>> test_array = np.ones((3,3), dtype=np.uint8)
        >>> image = SpatialImage(test_array)
        >>> image.set_pixel([1, 1], 2)
        >>> image.get_array()
        array([[1, 1, 1],
               [1, 2, 1],
               [1, 1, 1]], dtype=uint8)
        >>> # Trying to set a value not compatible with the array.dtype:
        >>> image.set_pixel([2, 2], 0.5)
        >>> image.get_array()
        array([[1, 1, 1],
               [1, 2, 1],
               [1, 1, 0]], dtype=uint8)
        >>> # Trying to set a value not compatible with the array.dtype:
        >>> image.set_pixel([0, 0], -6)
        >>> image.get_array()
        array([[250,   1,   1],
               [  1,   2,   1],
               [  1,   1,   0]], dtype=uint8)
        >>> image.set_pixel([0, 0], -6)
        """
        check_type(idx, 'idx', list)
        # Check we have the right number of `idx`:
        ndim = self.ndim
        try:
            assert len(idx) == ndim
        except AssertionError:
            msg = "Input `idx` must have a length equal to the image dimensionality!"
            raise ValueError(msg)
        # Check the `ids` are within the image boundaries:
        sh = self.shape
        ids = range(0, ndim)
        try:
            assert all((idx[i] >= 0) & (idx[i] < sh[i] + 1) for i in ids)
        except AssertionError:
            raise ValueError(f"Input `idx` {idx} are not within the image shape {self.shape}!")

        if ndim == 2:
            self[idx[0], idx[1]] = value
        else:
            self[idx[0], idx[1], idx[2]] = value
        return

    def get_region(self, region):
        """Extract a region using a list of start and stop indexes.

        Parameters
        ----------
        region : list
            Indexes provided as a list of integers, for example, `[y_start, y_stop, x_start, x_stop]` for a 2D image.

        Returns
        -------
        timagetk.SpatialImage
            The output image.

        Notes
        -----
        There should be two values per dimension in `region`, for example, `region=[5, 8, 5, 8]` for a 2D image.
        If the image is 3D and the start and stop indexes differ by only one in any dimension (indicating one layer of voxels),
        the returned image will be transformed to 2D.

        Raises
        ------
        TypeError
            If the provided `region` is not a list.
        ValueError
            If the number of indexes in `region` is incorrect, it should be twice the image dimensionality.
            If the `region` coordinates are outside the array boundaries.

        Example
        -------
        >>> from timagetk import SpatialImage
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 2D SpatialImage:
        >>> img = random_spatial_image((5, 5), voxelsize=[0.5, 0.5], dtype='uint8')
        >>> region = [1, 3, 1, -1]  # y-start, y-stop, x-start, x-stop
        >>> out_img = img.get_region(region)
        >>> isinstance(out_img, SpatialImage)
        True
        >>> out_img.get_array()
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((5, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> region = [1, 2, 1, 3, 1, 3]  # z-start, z-stop, y-start, y-stop, x-start, x-stop
        >>> out_img = img.get_region(region)
        >>> isinstance(out_img, SpatialImage)
        True
        >>> out_img.is3D()
        True
        >>> region = [2, 2, 1, 3, 1, 3]  # z-start, z-stop, y-start, y-stop, x-start, x-stop
        >>> out_img = img.get_region(region)
        >>> out_img.is2D()
        True
        """

        def _region_idx_to_slice(region):
            # Check we have the right number of `idx`:
            try:
                assert len(region) == 2 * self.ndim
            except AssertionError:
                msg = "Input `region` must have a length equal to twice the number of dimension of the image!"
                raise ValueError(msg)
            region_start = region[::2]
            region_stop = region[1::2]
            sh = self.shape
            # Check the "stop" values and replace negative ones:
            for n, r in enumerate(region_stop):
                if r < 0:
                    region_stop[n] = sh[n] + r
                try:
                    assert 0 < region_stop[n] <= sh[n] - 1
                except AssertionError:
                    raise ValueError(f"Input `region` {region} are not within the image shape {self.shape}!")
            # Check the "start" values:
            for n, r in enumerate(region_start):
                try:
                    assert 0 <= r < sh[n] - 1
                except AssertionError:
                    raise ValueError(f"Input `region` {region} are not within the image shape {self.shape}!")

            return tuple([slice(region_start[i], region_stop[i] + 1) for i in range(self.ndim)])

        from timagetk.components.image import get_image_class
        from timagetk.components.image import get_image_attributes
        if isinstance(region, (list, tuple)) and all([isinstance(reg, slice) for reg in region]):
            bbox = region
        else:
            check_type(region, 'idx', list)
            bbox = _region_idx_to_slice(region)

        Image = get_image_class(self)
        # - Get the `image` object attributes:
        attrs = get_image_attributes(self, exclude=['shape'], extra=['filename'])
        out_sp_img = Image(self[bbox], **attrs)
        if 1 in out_sp_img.shape:
            out_sp_img = out_sp_img.to_2d()

        return out_sp_img

    def set_region(self, region, value):
        """Replace a region of the image with the given value.

        Parameters
        ----------
        region : list
            An index list consisting of integers.
        value : array-like or ndarray
            The new value for the selected pixels, which should be of the same type as the `SpatialImage` array.

        Returns
        -------
        timagetk.SpatialImage
            An instance of `SpatialImage`.

        Raises
        ------
        TypeError
            Raised if the provided `region` is not a list.
        ValueError
            Raised if the number of indices in `region` is incorrect; it should be twice the image's dimensionality.
            Raised if the coordinates in `region` are not within the array's boundaries.
            Raised if there is a shape mismatch between `region` and `value`.
            Raised if `value` is not of the same data type as the array or is not an ndarray.

        Example
        -------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((5, 5), voxelsize=[0.5, 0.5], dtype='uint8')
        >>> region = [1, 3, 1, 3]
        >>> img.set_region(region, 3)
        >>> img.get_array()
        """
        check_type(region, 'idx', list)
        # Check we have the right number of `idx`:
        ndim = self.ndim
        try:
            assert len(region) == 2 * ndim
        except AssertionError:
            msg = "Input 'region' must have a length equal to twice the number of dimension of the image!"
            raise ValueError(msg)
        # Check the `region` is within the image boundaries:
        sh = self.shape
        ids = range(0, 2 * ndim, 2)
        try:
            assert all((region[i] >= 0) & (region[i + 1] < sh[i // 2] + 1) for i in ids)
        except AssertionError:
            err = "Input 'idx' {} are not within the image shape {}!"
            raise ValueError(err.format(region, self.shape))

        if isinstance(value, np.ndarray):
            region_shape = [region[i + 1] - region[i] for i in ids]
            try:
                np.testing.assert_equal(region_shape, value.shape)
            except AssertionError:
                msg = "Given region shape ({}) and value shape ({}) mismatch!"
                raise ValueError(msg.format(region_shape, value.shape))

        dtype = self.dtype
        if isinstance(value, np.ndarray):
            if ndim == 2:
                self[region[0]: region[1], region[2]: region[3]] = value[:, :].astype(dtype)
            else:
                self[region[0]: region[1], region[2]: region[3], region[4]: region[5]] = value[:, :, :].astype(dtype)
        elif isinstance(value, dtype):
            if ndim == 2:
                self[region[0]: region[1], region[2]: region[3]] = value
            else:
                self[region[0]: region[1], region[2]: region[3], region[4]: region[5]] = value
        else:
            msg = "The given `value` is not of the same dtype than the array or not a ndarray"
            raise ValueError(msg)

        return

    # --------------------------------------------------------------------------
    #
    # SpatialImage transformation functions:
    #
    # --------------------------------------------------------------------------

    def astype(self, dtype, **kwargs):
        """Return a copy of the SpatialImage with updated data type.

        Parameters
        ----------
        dtype : str
            The new data type to apply.

        Returns
        -------
        timagetk.SpatialImage
            The image with the new data type.

        Notes
        -----
        Keyword arguments are passed to the `numpy.astype()` method.

        Examples
        --------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((5, 5), voxelsize=[0.5, 0.5], dtype='uint8')
        >>> # Check the type:
        >>> img.dtype
        dtype('uint8')
        >>> print(img)
        SpatialImage object with following metadata:
           - shape: (5, 5)
           - ndim: 2
           - dtype: uint8
           - axes_order: YX
           - voxelsize: [0.5, 0.5]
           - unit: 1e-06
           - origin: [0, 0]
           - extent: [2.0, 2.0]
           - acquisition_date: None
        >>> # Convert it to 16bits unsigned integers:
        >>> img16 = img.astype('uint16')
        >>> # Check the type:
        >>> img16.dtype
        dtype('uint16')
        >>> print(img16)
        SpatialImage object with following metadata:
           - shape: (5, 5)
           - ndim: 2
           - dtype: uint16
           - axes_order: YX
           - voxelsize: [0.5, 0.5]
           - unit: 1e-06
           - origin: [0, 0]
           - extent: [2.0, 2.0]
           - acquisition_date: None
        """
        # Get the list of attribute to preserve:
        img_attr = ['axes_order', 'origin', 'voxelsize', 'metadata', 'not_a_label', 'background', 'unit', 'filename']
        img_kwargs = {attr: getattr(self, attr, None) for attr in img_attr}
        # - Convert the numpy array:
        array = self.get_array().astype(dtype, **kwargs)

        return SpatialImage(array, **img_kwargs)

    def crop(self, start, stop, axis='z'):
        """Crop the specified axis and keep data between the provided start and stop slice indices.

        Parameters
        ----------
        start : int
            The slice index to start cropping from.
        stop : int
            The slice index to stop cropping at.
        axis : {'x', 'y', 'z'} or int, optional
            The name of the axis to crop, or its index. Defaults to the 'z' axis.

        Returns
        -------
        timagetk.SpatialImage
            The cropped image.

        Raises
        ------
        ValueError
            If the image is not 3D and `axis='z'`.
            If the `start` and/or `stop` indices do not exist on this axis.

        Examples
        --------
        >>> from timagetk.io.dataset import shared_data
        >>> from timagetk.visu.stack import orthogonal_view
        >>> img = shared_data('flower_confocal', 0)
        >>> # Example - Get the first half of the image on the x-axis
        >>> cropped_x = img.crop(0, img.get_shape('x')//2, 'x' )
        >>> print(img.get_extent('x'))
        91.74656248092651
        >>> print(cropped_x.get_extent('x'))
        45.67296123504639
        >>> print(img)
        SpatialImage object with following metadata:
       - shape: (160, 230, 230)
       - ndim: 3
       - dtype: uint8
       - axes_order: ZYX
       - voxelsize: [0.40064001083374023, 0.40064001083374023, 0.40064001083374023]
       - unit: 1e-06
       - origin: [0, 0, 0]
       - extent: [63.7017617225647, 91.74656248092651, 91.74656248092651]
       - acquisition_date: None
        >>> print(cropped_x)
         SpatialImage object with following metadata:
           - shape: (160, 230, 115)
           - ndim: 3
           - dtype: uint8
           - axes_order: ZYX
           - voxelsize: [0.40064001083374023, 0.40064001083374023, 0.40064001083374023]
           - unit: 1e-06
           - origin: [0, 0, 0]
           - extent: [63.7017617225647, 91.74656248092651, 45.67296123504639]
           - acquisition_date: None
        >>> orthogonal_view(img)
        >>> orthogonal_view(cropped_x)
        """
        axis_shape = self.get_shape(axis=axis)
        try:
            assert 0 <= start <= stop - 1 < stop <= axis_shape - 1
        except AssertionError:
            raise ValueError("Check given copping limits!")

        if stop - start == 1:
            return self.get_slice(start, axis=axis)

        if isinstance(axis, str):
            axis_id = self.axes_order_dict[axis.upper()]
        else:
            axis_id = cp.copy(axis)

        if axis_id > self.ndim:
            raise ValueError(f"Can not crop axis {axis_id} on a {self.ndim}D image!")

        arr = np.take(self.get_array(), range(start, stop, 1), axis=axis_id)
        # Get the list of attribute to preserve:
        img_attr = ['axes_order', 'origin', 'voxelsize', 'metadata', 'not_a_label', 'background', 'unit', 'filename']
        img_kwargs = {attr: getattr(self, attr, None) for attr in img_attr}
        log.debug(f"Array shape: {arr.shape}")
        log.debug(f"Used image attributes: {img_kwargs}")

        return SpatialImage(arr, **img_kwargs)

    def to_2d(self):
        """Convert a 3D SpatialImage to a 2D SpatialImage.

        Conversion is possible only if there is a "flat" axis (i.e., an axis with only one slice).

        Returns
        -------
        timagetk.SpatialImage
            The 2D SpatialImage.

        Examples
        --------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage with one z-slice:
        >>> img = random_spatial_image((1, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> img.to_2d()
        >>> # Initialize a random (uint8) 3D SpatialImage with one y-slice:
        >>> img = random_spatial_image((5, 1, 5), voxelsize=[0.5, 1., 0.5], dtype='uint8')
        >>> img.to_2d()
        >>> # Initialize a random (uint8) 3D SpatialImage with one x-slice:
        >>> img = random_spatial_image((5, 5, 1), voxelsize=[0.5, 0.5, 1.], dtype='uint8')
        >>> img.to_2d()
        """
        from timagetk.io.image import default_image_attributes

        if self.is3D() and 1 in self.shape:
            # Get the array and its shape:
            shape, array = self.get_shape(), self.get_array()
            # Get attribute 'voxelsize':
            voxelsize = self.voxelsize
            # Get attributes 'origin' & 'metadata':
            ori, md = self.origin, self.metadata
            # Search axe to squeeze:
            if self.get_shape('x') == 1:
                ax2squeeze = self.axes_order_dict['X']
            elif self.get_shape('y') == 1:
                ax2squeeze = self.axes_order_dict['Y']
            else:
                ax2squeeze = self.axes_order_dict['Z']
            # Squeeze found axe:
            new_arr = np.squeeze(array, axis=(ax2squeeze,))
            # Edit attributes 'voxelsize' & 'origin':
            voxelsize.pop(ax2squeeze)
            ori.pop(ax2squeeze)
            return SpatialImage(new_arr,
                                **default_image_attributes(new_arr, voxelsize=voxelsize, origin=ori, metadata=md))
        elif self.is2D():
            log.error("This image is already a 2D image!")
            return None
        else:
            log.error('This 3D SpatialImage can not be reshaped to 2D.')
            return None

    def to_3d(self, flat_axis='z'):
        """Convert a 2D SpatialImage to a 3D SpatialImage.

        Parameters
        ----------
        flat_axis : {'x', 'y', 'z'}, optional
            The axis to add to the 2D image as a flat axis. Defaults to the 'z' axis.

        Returns
        -------
        timagetk.SpatialImage
            The 3D image containing a flat axis.

        Notes
        -----
        The resulting 3D SpatialImage includes a "flat" axis, which contains only one slice along this axis.
        The voxel size of this axis is set to `1.0`.
        The origin of this axis is set to `0`.

        Examples
        --------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 2D SpatialImage:
        >>> img = random_spatial_image((5, 5), voxelsize=[0.5, 0.5], dtype='uint8')
        >>> z_flat = img.to_3d(flat_axis='z')
        >>> z_flat.get_shape()
        >>> y_flat = img.to_3d(flat_axis='y')
        >>> y_flat.get_shape()
        >>> x_flat = img.to_3d(flat_axis='x')
        >>> x_flat.get_shape()
        >>> x_flat.origin
        >>> x_flat.voxelsize
        """
        from timagetk.io.image import default_image_attributes

        if not self.is2D():
            log.error('This SpatialImage is not 2D.')
            return None

        # Get the 'id' of the axis to reverse:
        axis_id = DEFAULT_AXIS_ORDER_3D[flat_axis.upper()]
        # Get the array and its shape:
        shape, array = self.get_shape(), self.get_array()
        # Get attribute 'voxelsize':
        voxelsize = self.voxelsize
        # Get attributes 'origin' & 'metadata':
        ori, md = self.origin, self.metadata

        # Update attributes:
        voxelsize.insert(axis_id, 1.)
        ori.insert(axis_id, 0)
        shape.insert(axis_id, 1)
        # Add new flat "axe":
        new_arr = np.reshape(array, shape)

        return SpatialImage(new_arr, **default_image_attributes(new_arr, voxelsize=voxelsize, origin=ori, metadata=md))

    def _new_order(self, *axes) -> list:
        """Return the list of integers indexing axes to re-order."""
        if len(axes) == 1 and isinstance(axes[0], str) and len(axes[0]) == self.ndim:
            # If axes are given as str, translate them to "axe ids":
            return [self.axes_order_dict[axe.upper()] for i, axe in enumerate(axes[0])]
        elif isinstance(axes, (list, tuple)) and all(isinstance(axe, str) for axe in axes):
            # If axes are given as list of str, translate them to "axe ids":
            return [self.axes_order_dict[axe.upper()] for axe in axes]
        else:
            raise ValueError(f"Could not make sense of given `axes`: {axes}")

    def transpose(self, *axes):
        """Permute the image axes to a specified order; by default, reverse the order.

        Parameters
        ----------
        axes : list of int or list of str, optional
            By default, the dimensions are reversed. However, if a specific order is provided, the axes are permuted accordingly.

        Returns
        -------
        timagetk.SpatialImage
            The image with permuted axes.

        Examples
        --------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 4, 5), voxelsize=[0.51, 0.52, 0.53], dtype='uint8')
        >>> img.metadata
        {'shape': (3, 4, 5),
         'ndim': 3,
         'dtype': dtype('uint8'),
         'axes_order': 'ZYX',
         'voxelsize': [0.51, 0.52, 0.53],
         'unit': 1e-06,
         'origin': (0.0, 0.0, 0.0),
         'extent': [1.02, 1.56, 2.12],
         'acquisition_date': None}
        >>> img_t = img.transpose()
        >>> img_t.metadata
        {'shape': (5, 4, 3),
         'ndim': 3,
         'dtype': dtype('uint8'),
         'axes_order': 'XYZ',
         'voxelsize': [0.53, 0.52, 0.51],
         'unit': 1e-06,
         'origin': [0.0, 0.0, 0.0],
         'extent': [2.12, 1.56, 1.02],
         'acquisition_date': None}
        >>> print(img_t.shape)  # `shape` attribute has been updated
        (5, 4, 3)
        >>> print(img_t.voxelsize)  # `voxelsize` attribute has been updated
        [0.53, 0.52, 0.51]
        >>> # Method `transpose` accept axes names as input
        >>> img_t = img.transpose('xzy')
        >>> print(img_t.shape)
        (5, 3, 4)
        >>> # Method `transpose` accept a tuple of axes names as input
        >>> img_t = img.transpose('x', 'z', 'y')
        >>> print(img_t.shape)
        (5, 3, 4)
        >>> # Initialize a random (uint8) 2D SpatialImage:
        >>> img = random_spatial_image((4, 5), voxelsize=[0.52, 0.53], dtype='uint8')
        >>> img_t = img.transpose()
        >>> # Transpose update the shape attribute of the image (here reversed):
        >>> print(img_t.shape)
        (5, 4)
        """
        if axes == ():
            # If no axes order is specified, reverse the axes.
            axes = list(range(self.ndim))[::-1]
        elif isinstance(axes, (list, tuple)) and all(isinstance(ax, int) for ax in axes):
            pass
        else:
            axes = self._new_order(*axes)

        # Uses ``axes`` to reorder the `axes_order`, `origin` and `voxelsize` attributes:
        ori = [self.origin[axe] for axe in axes]
        vxs = [self.voxelsize[axe] for axe in axes]
        axo = ''.join([self.axes_order[axe] for axe in axes])
        # Get the list of attribute to preserve:
        img_attr = ['metadata', 'not_a_label', 'background', 'unit', 'filename']
        img_kwargs = {attr: getattr(self, attr, None) for attr in img_attr}
        # Transpose the array:
        arr = np.transpose(self.get_array(), axes)
        return SpatialImage(arr, origin=ori, voxelsize=vxs, axes_order=axo, **img_kwargs)

    def invert_axis(self, axis):
        """Invert the specified axis of the spatial image.

        Parameters
        ----------
        axis : {'x', 'y', 'z'}
            Axis to invert. Can be either 'x', 'y', or 'z' (if the image is 3D).

        Returns
        -------
        timagetk.SpatialImage
            The image with the array inverted along the specified axis.

        Raises
        ------
        ValueError
            Raised if the provided `axis` is not one of {'x', 'y', 'z'} for 3D images or {'x', 'y'} for 2D images.

        Example
        -------
        >>> from timagetk.array_util import random_spatial_image
        >>> # Initialize a random (uint8) 3D SpatialImage:
        >>> img = random_spatial_image((3, 5, 5), voxelsize=[1., 0.5, 0.5], dtype='uint8')
        >>> print(img.get_slice(0, "z").get_array())
        >>> inv_img = img.invert_axis(axis='z')
        >>> print(inv_img.get_slice(0, "z").get_array())
        """
        # Get the list of attribute to preserve:
        img_attr = ['axes_order', 'origin', 'voxelsize', 'metadata', 'not_a_label', 'background', 'unit', 'filename']
        img_kwargs = {attr: getattr(self, attr, None) for attr in img_attr}

        if self.is2D():
            arr = self._invert_2d(axis)
        else:
            arr = self._invert_3d(axis)

        return SpatialImage(arr, **img_kwargs)

    def _invert_3d(self, axis):
        """Reverse the x, y, or z-axis of a given 3D array.

        Parameters
        ----------
        axis : str, one of {'x', 'y', 'z'}
            The array axis to reverse.

        Returns
        -------
        numpy.ndarray
            The array with the selected axis reversed.

        Raises
        ------
        ValueError
            If the provided `axis` is not one of {'x', 'y', 'z'}.
        """
        if self.axes_order_dict[axis.upper()] == 2:
            return self.get_array()[:, :, ::-1]
        elif self.axes_order_dict[axis.upper()] == 1:
            return self.get_array()[:, ::-1, :]
        elif self.axes_order_dict[axis.upper()] == 0:
            return self.get_array()[::-1, :, :]
        else:
            raise ValueError("Unknown axis '{}' for a 3D array.".format(axis))

    def _invert_2d(self, axis):
        """Reverse the x or y-axis of a given 2D array.

        Parameters
        ----------
        axis : str, one of {'x', 'y'}
            The array axis to reverse.

        Returns
        -------
        numpy.ndarray
            The array with the selected axis reversed.

        Raises
        ------
        ValueError
            If the provided `axis` is not one of {'x', 'y'}.
        """
        if self.axes_order_dict[axis.upper()] == 1:
            return self.get_array()[:, ::-1]
        elif self.axes_order_dict[axis.upper()] == 0:
            return self.get_array()[::-1, :]
        else:
            raise ValueError("Unknown axis '{}' for a 2D array.".format(axis))
