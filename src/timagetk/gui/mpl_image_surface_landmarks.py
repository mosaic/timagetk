#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Graphical user interface to manually place landmarks on projected object surface."""

import math

import matplotlib.pyplot as plt
import numpy as np
import scipy.ndimage as nd
from timagetk.algorithms.blockmatching import blockmatching
from timagetk.algorithms.exposure import global_contrast_stretch
from timagetk.algorithms.pointmatching import pointmatching
from timagetk.algorithms.reconstruction import image_surface_projection
from timagetk.algorithms.resample import isometric_resampling
from timagetk.algorithms.resample import new_voxelsize_from_max_shape
from timagetk.algorithms.trsf import apply_trsf
from timagetk.algorithms.trsf import compose_trsf
from timagetk.algorithms.trsf import create_trsf
from timagetk.algorithms.trsf import inv_trsf
from timagetk.array_util import guess_image_orientation
from timagetk.array_util import test_array_n_percent_range
from timagetk.array_util import to_uint8
from timagetk.bin.logger import get_dummy_logger
from timagetk.components.multi_channel import combine_channels
from timagetk.components.spatial_image import SpatialImage
from timagetk.visu import register_glasbey
from timagetk.visu.util import colors_array

try:
    register_glasbey()
except:
    pass  # should already be loaded!

DEFAULT_LDMK_3D = "XYZ"
PANELS = ['left', 'right']

logger = get_dummy_logger('SurfaceLandmarks')


class SurfaceLandmarks(object):
    """Matplotlib based GUI to manually define landmarks on projected intensity images.

    Attributes
    ----------
    figure : matplotlib.pyplot.figure
        The matplotlib figure used to display the projected intensity images.
    images : dict of timagetk.SpatialImage
        Dictionary with 'left' and 'right' keys and associated SpatialImage.
    rotations :  dict
        Dictionary indicating 'left' and/or 'right' image clockwise rotation
    orientation :  dict
        Dictionary indicating 'left' and/or 'right" image orientation with `-1` or `1`.
        If `-1` we consider the z-axis starts at the bottom of the object and goes upward.
        If `1` we consider the z-axis starts at the top of the object and goes downward.
    surface_threshold : int
        Intensity threshold to detect the objects outer surfaces. 
    surface_sigma : float
        Smoothing factor, divided by voxel-sizes.
    projected_images : dict of timagetk.SpatialImage
        Dictionary with 'left' & 'right' keys and associated intensity projection images, YX sorted.
    altitude_maps : dict of timagetk.SpatialImage
        Dictionary with 'left' & 'right' keys and associated altitude images, YX sorted.
    colormap : str
        Matplotlib colormap name to use with intensity projections figures.
    value_range : list
        Length-2 list of integer defining the used range of values with intensity projections figures.
    points : dict
        Dictionary with 'left' and 'right' keys and associated 2D points coordinates.
    artists : dict
        Dictionary with 'left' and 'right' keys and associated colored point.
    panels : dict
        Dictionary with 'left' and 'right' keys and associated 2D points coordinates.
    add : bool
        Boolean variable used to enable the 'add point' action when maintaining the 'a' key pressed.
    drag
    connects
    """

    def __init__(self, img_left, img_right, **kwargs):
        """Constructor.

        Parameters
        ----------
        img_left : timagetk.SpatialImage
            The intensity image to display on the left panel.
        img_right : timagetk.SpatialImage
            The intensity image to display on the right panel.

        Other Parameters
        ----------------
        orientation : dict of int
            Dictionary indicating 'left' and/or 'right' image oritentation with `-1` or `1`.
            If `-1` we consider the z-axis starts at the bottom of the object and goes upward.
            If `1` we consider the z-axis starts at the top of the object and goes downward.
        rotate : dict
            Dictionary indicating 'left' and/or 'right' image clockwise rotation in degrees.
            To rotate the right image by 90°, do {'left': 0, 'right': 90}.
            Using only one key is possible.
        voxelsize : float
            The voxel-size to use to resample the panel images.
            By default it is set to `max(img_left.xy_voxelsize, img_right.xy_voxelsize, 0.3)`.
        points : dict
            A 'left'/'right' dictionary of landmarks to add to the firgure.
            Must be XY(Z) sorted and in REAL units (not voxel)!
        surface_sigma : float, optional
            Smoothing factor, divided by voxel-sizes.
        colormap : str, optional
            Matplotlib colormap name to use with intensity projections figures.
        figure : matplotlib.pyplot.figure
            The matplotlib figure used to display the projected intensity images.

        Examples
        --------
        >>> import matplotlib.pyplot as plt
        >>> import numpy as np
        >>> from timagetk.gui.mpl_image_surface_landmarks import SurfaceLandmarks
        >>> from timagetk.io.dataset import shared_data
        >>> # Using shared data as example, images are multi-angle view of the same floral meristem:
        >>> ref_img = shared_data('flower_multiangle', 0)
        >>> flo_img = shared_data('flower_multiangle', 1)

        >>> # Example 1: Initialize the GUI:
        >>> gui = SurfaceLandmarks(ref_img, flo_img, surface_threshold=45, orientation={'left': 1, 'right': 1})
        >>> gui.figure.show()
        >>> # Press & maintain key `a` to ADD points by left-clicking (mouse) on any panel, release `a` add drag any point to move it around
        >>> # Press key `r` to rotate the right panel by 90° clockwise
        >>> # Press key `u` to update the app figures
        >>> # Press key `q` to quit the app
        >>> # AFTER PLACING POINTS: Get XYZ landmarks coordinates (in real units):
        >>> xyz_ldmks = gui.points_3d_coordinates()
        >>> l_pts_01 = xyz_ldmks['left']
        >>> r_pts_01 = xyz_ldmks['right']
        >>> gui = SurfaceLandmarks(ref_img, flo_img, surface_threshold=45, orientation={'left': 1, 'right': 1}, rotate={'left': 0, 'right': 90}, points={'left': l_pts_01, 'right': r_pts_01})

        >>> gui = SurfaceLandmarks(ref_img, flo_img, surface_threshold=45, orientation={'left': 1, 'right': 1}, axis={'left': 'x', 'right': 'x'})

        >>> # Example 2: Initialize the GUI with shared landmarks:
        >>> ref_pts = np.loadtxt(shared_data('p58_t0_reference_ldmk-01.txt', 'p58'))
        >>> flo_pts = np.loadtxt(shared_data('p58_t0_floating_ldmk-01.txt', 'p58'))
        >>> gui = SurfaceLandmarks(ref_img, flo_img, surface_threshold=45, orientation={'left': 1, 'right': 1}, points={'left': ref_pts, 'right': flo_pts})
        >>> # - Returned XYZ landmarks coordinates are in real units:
        >>> xyz_ldmks = gui.points_3d_coordinates()
        >>> l_pts_01 = xyz_ldmks['left']
        >>> r_pts_01 = xyz_ldmks['right']
        >>> print(l_pts_01)
        [(43.04263323598073, 54.64057612359009, 9.61533808708191), (39.64462096493574, 47.57271059981649, 11.418213978409767), (36.92621114809974, 41.59220900277729, 11.418213978409767), (50.92602170480514, 50.15519992581069, 11.418213978409767), (60.44045606373113, 53.145450724330296, 10.015977174043655), (56.09100035679354, 38.46603771341589, 11.418213978409767), (42.49895127261354, 36.42723035078889, 11.61853352189064), (46.71248648870933, 44.85430078298049, 11.418213978409767), (54.867715939217334, 42.67957292951169, 11.418213978409767), (57.586125756053335, 61.300680174838284, 5.007988587021828), (60.84821753625654, 45.66982372803129, 11.418213978409767)]
        >>> print(r_pts_01)
        [(25.137649211007727, 39.57082038592395, 9.61533808708191), (29.623025408787136, 33.862159770568354, 9.815657630562782), (35.467606514984546, 28.969022100263544, 9.61533808708191), (30.982230317205143, 44.599878547070546, 9.815657630562782), (34.380242588250134, 54.92983585104736, 9.815657630562782), (44.16651792885974, 45.14356051043774, 9.61533808708191), (41.58402860286554, 33.04663682551754, 9.61533808708191), (34.78800406077554, 39.163058913398544, 9.61533808708191), (38.865618786029536, 45.82316296464674, 9.61533808708191), (26.76869510110933, 55.47351781441455, 9.61533808708191), (39.81706222192214, 53.02694897926216, 9.61533808708191)]

        >>> # - Apply manual registration to right image panel and display both overlayed z-stacks
        >>> from timagetk.algorithms.trsf import apply_trsf
        >>> reg_img = apply_trsf(list_img[1], inv_trsf(trsf_01), template_img=list_img[0], interpolation=self.interp)
        >>> from timagetk.visu.stack import channels_stack_browser
        >>> channels_stack_browser([list_img[0], reg_img], ['ref', 'reg'], ['red', 'green'], title="Manual registration")

        >>> figure = plt.figure(0)
        >>> figure.clf()
        >>> landmarks_02 = SurfaceLandmarks(figure, img_left=list_img[0], img_right=list_img[2], surface_threshold=45, rotate={'left': 0, 'right': 180})
        >>> plt.show(block=True)
        >>> # - Returned XYZ landmarks coordinates are in the original reference geometry for the right image:
        >>> l_pts_02 = landmarks_02.points_3d_coordinates()['left']

        >>> r_pts_02 = landmarks_02.points_3d_coordinates()['right']
        >>> np.savetxt('p58_t0_reference_ldmk_02.txt', np.array(l_pts_02))
        >>> np.savetxt('p58_t0_floating_ldmk_02.txt', np.array(r_pts_02))
        >>> trsf_02 = pointmatching(r_pts_02, l_pts_02)
        """
        default_vxs = kwargs.get('voxelsize', None)
        default_interp = kwargs.pop('interpolation', 'linear')
        default_proj_axis = kwargs.get('axis', {'left': 'z', 'right': 'z'})
        default_orientation = kwargs.get('orientation', {'left': None, 'right': None})
        default_surface_sigma = kwargs.get('surface_sigma', 1.)
        default_colormap = kwargs.get('colormap', 1.)
        default_figure = kwargs.get('figure', plt.figure(0))
        # Initialize attributes
        ## Images params
        self.interp = default_interp
        self.main_vxs = self._init_main_voxelsize(default_vxs, img_left, img_right)
        self.thumb_vxs = self._init_thumb_voxelsize(img_left, img_right)
        self.proj_axis = default_proj_axis
        ## Images
        self.images = self._init_images({'left': img_left, 'right': img_right}, self.main_vxs, self.interp)
        self.orientation = self._init_image_orientation(default_orientation, self.images)
        self.value_range = self._init_value_range(self.images)
        self.lowres_images = self._init_lowres_images(self.images, self.thumb_vxs, self.interp)
        # Projection params
        self.surface_threshold = self._init_surface_threshold(self.images)
        self.surface_sigma = default_surface_sigma
        # Projection images
        self.projected_images = {'left': None, 'right': None}
        self.altitude_maps = {'left': None, 'right': None}
        ## Points & transfo
        self.ldmks_2d = {'left': np.array([]), 'right': np.array([])}  # row/col sorted!
        self.ldmk_trsf = create_trsf('identity')  # Identity transformation
        self.rigid_trsf = None
        self.ldmk_registered_image = None
        self.rigid_registered_image = None
        ## Matplotlib
        self.colormap = default_colormap
        self.figure = default_figure
        self.artists = {'left': {}, 'right': {}}
        self.panels = {'left': None, 'right': None}
        self.rotations = {'left': 0, 'right': 0}
        self.axes_2d = {'left': None, 'right': None}
        ## GUI stuffs
        self.add = False
        self.delete = False
        self.drag = {'left': None, 'right': None}
        self.connects = {}

        # Matplotlib figure
        if 'figure' not in kwargs:
            self.figure.clf()
            self.figure.set_size_inches(15, 8)

        ## Create altitude maps & 2D projections:
        self._update_projections_altimaps()
        ## Get the physical axes ordering from the projections:
        self.axes_2d = {panel: self.projected_images[panel].axes_order for panel in PANELS}

        # Initialize 2D landmarks:
        ## Check the 3D landmarks dictionary
        ldmks_3d = kwargs.get('points', None)
        if ldmks_3d is None:
            ldmks_3d = {
                'left': np.array([(5., 10., 15.)]),
                'right': np.array([(5., 10., 15.)])
            }  # XYZ (col/row/alti) sorted!
        try:
            assert len(ldmks_3d['left']) == len(ldmks_3d['right'])
        except KeyError:
            raise KeyError("Both 'left' and 'right' entries are required for 'points' keyword arguments!")
        except AssertionError as e:
            raise AssertionError("You have to provide the same number of points in both 'left' & 'right' entries!")
        ## Convert 3D landmarks to a dict of 2D numpy arrays, sorted as physical axes for panel:
        for panel in PANELS:
            current_axo = self.axes_2d[panel]
            axes_idx = [DEFAULT_LDMK_3D.index(ax) for ax in current_axo]
            ldmks_2d = np.array(ldmks_3d[panel])[:, axes_idx]
            self.ldmks_2d[panel] = ldmks_2d
            logger.info(f"Initialized {panel} 2D landmarks ({current_axo}) to: {self.ldmks_2d[panel]}")

        # Initialize GUI stuffs
        ## Initialize the panels:
        self.init_panels()
        ## Connect GUI events:
        self.connect()
        ## Populate left & right main panels with projection images and 2D landmarks.
        self.update_main_panels()
        ## Apply rotations to main panel images & 2D landmarks (if any)
        rotations = kwargs.get('rotate', self.rotations)
        for panel in PANELS:
            if rotations[panel] != 0:
                self.rotate_panel(panel, rotations[panel])
        logger.info(self.ldmks_2d)

        ## Populate lower panel with thumbnails:
        #    - reference image (left)
        #    - 'landmarks' registered right image
        #    - 'landmarks o rigid' registered right image
        self.update_lower_panel()

    def __del__(self):
        """Clear the GUI & disconnect all events."""
        self.clear()

    @staticmethod
    def _init_main_voxelsize(voxelsize, img_left, img_right):
        _unit = img_left._get_unit()
        ## Get the "main voxel-size" to use for the images to display in the main panels (left & right)
        if voxelsize is not None:
            main_vxs = voxelsize
            logger.info(f"Got a main voxel-size of {main_vxs * _unit:~P}...")
        else:
            main_vxs = max([round(max(new_voxelsize_from_max_shape(img, 512)), 2) for img in [img_left, img_right]])
            logger.info(f"Obtained a main voxel-size of {main_vxs * _unit:~P}...")
        return main_vxs

    @staticmethod
    def _init_thumb_voxelsize(img_left, img_right):
        _unit = img_left._get_unit()
        thumb_vxs = max([round(max(new_voxelsize_from_max_shape(img, 128)), 2) for img in [img_left, img_right]])
        logger.info(f"Obtained a thumbnail voxel-size of {thumb_vxs * _unit:~P}...")
        return thumb_vxs

    @staticmethod
    def _init_images(images, main_vxs, interpolation):
        ## Left and right panel images have array values that cover decent range of their dtype's range
        for panel, img in images.items():
            if not test_array_n_percent_range(img.get_array(), pc_range_threshold=75):
                logger.info(f"Global contrast stretching for '{panel}' main image...")
                images[panel] = global_contrast_stretch(img, pc_min=2., pc_max=98.)

        # Conversion to 8bits unsigned integers
        logger.info(f"Conversion to 8bits unsigned integers of main intensity images...")
        for panel, img in images.items():
            if img.dtype != np.uint8:
                images[panel] = to_uint8(img)

        ## Images are finally resampled to ISOMETRIC voxel-size
        logger.info(f"Isometric resampling of main intensity images...")
        for panel, img in images.items():
            if not img.is_isometric() or img.get_voxelsize('z') != main_vxs:
                images[panel] = isometric_resampling(img, value=main_vxs, interpolation=interpolation)

        return images

    @staticmethod
    def _init_value_range(images):
        return {panel: (np.percentile(images[panel], 1.), np.percentile(images[panel], 99.5)) for
                panel in PANELS}

    @staticmethod
    def _init_lowres_images(images, thumb_vxs, interpolation):
        logger.info(f"Isometric resampling of low resolution intensity images...")
        lowres_images = {panel: isometric_resampling(images[panel], value=thumb_vxs, interpolation=interpolation) for
                         panel in PANELS}
        return lowres_images

    @staticmethod
    def _init_surface_threshold(images):
        return {panel: np.percentile(images[panel], 85) for panel in PANELS}

    @staticmethod
    def _init_image_orientation(orientation, images):
        ## Get images orientation and guess them if undefined
        for panel in PANELS:
            if not orientation[panel] in [-1, 1]:
                logger.info(f"Guessing image orientation for '{panel}' main image...")
                orientation[panel] = guess_image_orientation(images[panel])
        return orientation

    def clear(self):
        """Disconnect all events."""
        for c in self.connects.keys():
            self.figure.canvas.mpl_disconnect(self.connects[c])

    def connect(self):
        """Connect events."""
        self.connects['press'] = self.figure.canvas.mpl_connect('button_press_event', self.press)
        self.connects['move'] = self.figure.canvas.mpl_connect('motion_notify_event', self.move)
        self.connects['release'] = self.figure.canvas.mpl_connect('button_release_event', self.release)
        self.connects['scroll'] = self.figure.canvas.mpl_connect('scroll_event', self.scroll)
        self.connects['key_press'] = self.figure.canvas.mpl_connect('key_press_event', self.on_key_press)
        self.connects['key_release'] = self.figure.canvas.mpl_connect('key_release_event', self.on_key_release)

    def press(self, event):
        """Define what happen when a button is pressed."""
        for panel in ['left', 'right']:
            if event.inaxes == self.panels[panel]:
                if self.add:
                    logger.info(f"Added a point to {panel} panel at {(event.xdata, event.ydata)}!")
                    self.add_point(panel, col=event.xdata, row=event.ydata)
                elif self.delete:
                    for i_p, _p in enumerate(self.ldmks_2d[panel]):
                        try:
                            picked, ind = self.artists[panel][i_p].contains(event)
                        except KeyError:
                            picked = False
                        if picked:
                            self.delete_point(i_p)
                            logger.info(f"Deleted landmark point {i_p}!")
                            self.delete = False
                else:
                    for i_p, _p in enumerate(self.ldmks_2d[panel]):
                        picked, ind = self.artists[panel][i_p].contains(event)
                        if picked:
                            self.drag[panel] = i_p

    def on_key_press(self, event):
        """Define action to associate to pressing a keyboard key."""
        # Enable adding a point on both panels
        if event.key == 'a':
            self.add = True
        if event.key == 'd':
            self.delete = True
        # Rotate the image & points on the right panel by 90 degrees
        if event.key == 'r':
            self.rotate_panel('right', 90)
        if event.key == 'u':
            self.update_panels()

    def on_key_release(self, event):
        """Define action to associate to releasing a keyboard key."""
        # Disable adding a point on both panels
        if event.key == 'a':
            self.add = False
        if event.key == 'd':
            self.delete = False
        if event.key == 'q':
            # TODO: gracefully exit the GUI
            pass

    def move(self, event):
        """Define how to move the image in each panel."""
        for panel in ['left', 'right']:
            if event.inaxes == self.panels[panel]:
                if self.drag[panel] is not None:
                    col = event.xdata
                    row = event.ydata
                    self.ldmks_2d[panel][self.drag[panel]] = [row, col]
                    self.update_main_panels_points()
                    self.figure.canvas.draw()
            else:
                self.drag[panel] = None

    def release(self, event):
        """Define how to stop dragging the image around."""
        for panel in ['left', 'right']:
            self.drag[panel] = None

    def scroll(self, event):
        """Define how to scroll the image, that is zooming in and out."""
        if event.inaxes is not None:
            step = 20 * event.step
            point = [event.xdata, event.ydata]
            xlim = list(event.inaxes.get_xlim())
            point_x = (point[0] - xlim[0]) / (xlim[1] - xlim[0])
            x_size = xlim[1] - xlim[0]
            xlim[0] += step * point_x * x_size / 100.
            xlim[1] -= step * (1 - point_x) * x_size / 100.
            ylim = list(event.inaxes.get_ylim())
            point_y = (point[1] - ylim[0]) / (ylim[1] - ylim[0])
            y_size = ylim[1] - ylim[0]
            ylim[0] += step * point_y * y_size / 100.
            ylim[1] -= step * (1 - point_y) * y_size / 100.
            event.inaxes.set_ylim(*ylim)
            event.inaxes.set_xlim(*xlim)
            self.figure.canvas.draw()

    def init_panels(self):
        """Initialize the figure object and its layout with a gridspec.

        Two main panels, left & right, are created to host the projected images.
        The lower panel will receive a series of thumbnails showing slices of the resulting registration with a multi-color blending images.
        """
        self.figure.clf()
        left_col = self.images['left'].get_extent(self.axes_2d['left'][1])
        right_col = self.images['right'].get_extent(self.axes_2d['right'][1])

        gridspec = self.figure.add_gridspec(2, 2, width_ratios=[left_col, right_col], height_ratios=[2, 1])
        self.panels['left'] = self.figure.add_subplot(gridspec[0, 0])
        self.panels['right'] = self.figure.add_subplot(gridspec[0, 1])
        self.panels['lower'] = gridspec[1, :].subgridspec(1, 5)
        self.figure.subplots_adjust(wspace=0, hspace=0)
        self.figure.tight_layout()

    def update_panels(self):
        """Define the action to take when updating the panels."""
        # self.update_main_panels()
        self.update_lower_panel()

    def delete_point(self, i_p):
        """Delete an existing point from both panels.

        Parameters
        ----------
        panel : {'left', 'right'}
            The panel where the point was placed.
        xy : tuple
            A length-2 tuple of XY coordinates.
        """
        for panel in PANELS:
            self.ldmks_2d[panel] = np.delete(self.ldmks_2d[panel], i_p, axis=0)
        self.update_main_panels_points()

    def add_point(self, panel, row, col):
        """Add a new points to both panels.

        Add 2D coordinates to the `points` attribute and update the scatter figure.

        Parameters
        ----------
        panel : {'left', 'right'}
            The panel where the point was placed.
        xy : tuple
            A length-2 tuple of XY coordinates.
        """
        n_points = len(self.ldmks_2d[panel])
        # Add the XY point to the panel where it was clicked
        self.ldmks_2d[panel] = np.vstack([self.ldmks_2d[panel], [row, col]])
        # Add the XY point to the opposite panel...
        for p in set(PANELS) - {panel}:
            self.ldmks_2d[p] = np.vstack([self.ldmks_2d[p], [row, col]])
            # TODO: next lines are an attempt to apply the known trsf to the added point to place it on the opposite panel
            # if n_points <= 3:
            #     self.points[p] += [xy]
            # else:
            #     self._update_trsf()
            #     pex = self.images[panel].get_extent('x') / 2.
            #     pey = self.images[panel].get_extent('y') / 2.
            #     o_pex = self.images[p].get_extent('x') / 2.
            #     o_pey = self.images[p].get_extent('y') / 2.
            #     # REMINDER: the 'left' panel is the "reference image" & the 'right' panel is the floating image
            #     if p == 'right':
            #         # if we added the point in the 'right' panel (floating image)...
            #         # to place the landmarks on the 'left' panel we need to apply the INVERT trsf to the unrotated 3D point
            #         # (that allow to go from the 'right' to the 'left' panel)
            #         trsf = inv_trsf(self.ldmk_trsf)
            #     else:
            #         # if we added the point in the 'left' panel (reference image)...
            #         # to place the landmarks on the 'right' panel we need to apply the trsf to the unrotated 3D point
            #         # (that allow to go from the 'left' to the 'left' panel)
            #         trsf = self.ldmk_trsf
            #     # Add the point to the opposite panel with the transformation
            #     print(f"{panel} panel 2D coordinates: {xy}")
            #     xy = rotate_around_point(np.array([xy]), self.rotations[panel], (pex, pey))[0]
            #     print(f"{panel} panel true 2D coordinates (rotated): {xy}")
            #     xyz = self.get_coord_3d(panel, xy)
            #     print(f"{panel} panel 3D coordinates: {xyz}")
            #     opposite_xyz = apply_trsf_to_points([[xyz]], trsf)[0]
            #     # opposite_xyz = apply_trsf_to_points(self.points_3d_coordinates()[panel], trsf)[-1]
            #     print(f"{p} panel 3D coordinates: {opposite_xyz}")
            #     opposite_xy = opposite_xyz[:2]
            #     print(f"{p} panel true 2D coordinates: {opposite_xy}")
            #     opposite_xy = rotate_around_point(np.array([opposite_xy]), -self.rotations[p], (o_pex, o_pey))[0]
            #     print(f"{p} panel rotated opposite 2D point: {opposite_xy}")
            #     self.points[p] += [list(opposite_xy)]
            #     opposite_xy = rotate_around_point(np.array([opposite_xyz[:2]]), self.rotations[p], (o_pex, o_pey))[0]
            #     print(f"{p} panel rotated opposite 2D point: {opposite_xy}")

        self.update_main_panels_points()
        # self.update_lower_panel()

    def _update_projections_altimaps(self, panels=PANELS):
        """Update the projections and altitude maps."""
        for panel in panels:
            logger.info(f"Updating {panel} 2D projections and altitude map...")
            img = self.images[panel]
            if self.projected_images[panel] is None:
                proj_img, alti_img = image_surface_projection(img, gaussian_sigma=self.surface_sigma, height=3.,
                                                              threshold=self.surface_threshold[panel],
                                                              axis=self.proj_axis[panel],
                                                              orientation=self.orientation[panel], iso_upsampling=False)
                # Save the 2D intensity projection image
                self.projected_images[panel] = proj_img
                # Convert the altitude map to real units
                self.altitude_maps[panel] = alti_img * img.get_voxelsize(self.proj_axis[panel])
        return

    def update_main_panels(self):
        """Update left & right panels with 2D projections & 2D landmarks.

        Notes
        -----
        Inside the left & right panels, the coordinates are in real units!
        """
        for panel in ['left', 'right']:
            self.figure.sca(self.panels[panel])  # set the current figure to given panel
            # Set the projections and altitude maps if not done yet
            if self.projected_images[panel] is None:
                self._update_projections_altimaps()
            self.figure.gca().cla()  # clear the current axes
            ax = self.figure.gca()  # get current axis
            # Plot the projected images:
            self.imshow(ax, self.projected_images[panel], title=self.images[panel].filename,
                        value_range=self.value_range[panel], extent=self._mpl_panel_extent(panel))

        self.update_main_panels_points()
        self.figure.canvas.draw()
        return

    def update_main_panels_points(self):
        """Update the landmarks representation on the main panels."""
        for panel in ['left', 'right']:
            # Select the axes corresponding to the panel
            self.figure.sca(self.panels[panel])
            # Clean the points from the panel figure:
            for i_p, _p in enumerate(self.ldmks_2d[panel]):
                if i_p in self.artists[panel]:
                    self.artists[panel][i_p].remove()
                    del self.artists[panel][i_p]
            self.artists[panel] = {}
            # Get the current axes:
            ax = self.figure.gca()
            # Get the number of points to plot:
            n_pts = len(self.ldmks_2d[panel])
            # Get an array of RGB colors:
            gcolors = colors_array('glasbey', 200, alpha=False)[1:]  # exclude first color (white)
            # Add the point to the panel figure
            for i_p, p in enumerate(self.ldmks_2d[panel]):
                # row, col = p
                # self.artists[panel][i_p] = ax.scatter(col, row, ec='k', s=80, color=gcolors[i_p])
                self.artists[panel][i_p] = ax.scatter(*p[::-1], ec='k', s=80, color=gcolors[i_p])

        self.figure.canvas.draw()
        return

    def update_lower_panel(self):
        """Update the panel with the slices showing the result of the manual registration."""
        logger.info("Updating the thumbnails...")

        # If at least 4 2D landmarks exists, update the "landmark transformation":
        n_points = len(self.ldmks_2d['left'])
        if n_points >= 4:
            self._update_ldmk_trsf()
            self._update_rigid_trsf()

        logger.info("Applying landmark transformation to right thumbnail image...")
        # Required even if `self.ldmk_trsf` is the identity as right & left lowres_images may be of different shape!
        self.ldmk_registered_image = apply_trsf(self.lowres_images['right'], trsf=self.ldmk_trsf,
                                                template_img=self.lowres_images['left'], interpolation=self.interp)

        if self.rigid_trsf is not None:
            comp_trsf = compose_trsf([self.ldmk_trsf, self.rigid_trsf], template_img=self.lowres_images['left'])
            self.rigid_registered_image = apply_trsf(self.lowres_images['right'], trsf=comp_trsf,
                                                     template_img=self.lowres_images['left'], interpolation=self.interp)

        proj_axis = self.proj_axis['left'].upper()  # as the reference image is the left one
        # Get the list of slices to represent in the lowres_images:
        cuts = [1 / 6., 1 / 4., 1 / 2., 2 / 3., 3 / 4.]
        axis_sh = self.ldmk_registered_image.get_shape(proj_axis)
        slices = [int(np.floor(c * axis_sh)) for c in cuts]

        logger.info("Updating blend image for thumbnails...")
        if self.rigid_trsf is not None:
            # Blend the reference (red), the landmark registered image (green) and improved registered image (blue)
            blend = combine_channels(
                [self.lowres_images['left'], self.ldmk_registered_image, self.rigid_registered_image])
        else:
            # Blend the reference (red) and the landmark registered image (green)
            blend = combine_channels([self.lowres_images['left'], self.ldmk_registered_image])

        # Transpose blend axes to match projection axis when slicing:
        new_axo = proj_axis + blend.axes_order.replace(proj_axis, '')
        new_axo = new_axo.replace('B', '')
        blend = blend.transpose(new_axo)

        logger.info("Updating lower panel thumbnails...")
        # Show the selected slices
        plot_kwargs = {}
        for n, sl in enumerate(slices):
            subpanel = self.figure.add_subplot(self.panels['lower'][0, n])
            plot_kwargs['no_ytick'] = True if n > 0 else False
            plot_kwargs['no_ylab'] = True if n > 0 else False
            self.imshow(subpanel, blend.get_slice(sl, axis=proj_axis), title=f"{proj_axis}-slice {sl}/{axis_sh}",
                        extent=self._mpl_panel_extent('left'), value_range=None, **plot_kwargs)
        self.figure.subplots_adjust(wspace=0, hspace=0)
        self.figure.tight_layout()
        self.figure.canvas.draw()
        logger.info("Done.")

        return

    def _update_ldmk_trsf(self):
        """Compute the rigid transformation using 3D landmarks.

        Notes
        -----
        We use the reference (left) image as template to resample prior to blending (requires identical array shapes).
        """
        logger.info("Updating rigid transformation from landmarks...")
        # Get the points real coordinates for each panel:
        ldmks_3d = self.points_3d_coordinates()
        ref_pts = ldmks_3d['left']
        flo_pts = ldmks_3d['right']
        self.ldmk_trsf = pointmatching(flo_pts, ref_pts, template_img=self.images['left'], method='rigid', real=True)
        return

    def _update_rigid_trsf(self):
        """Compute the rigid transformation using transformation from 3D landmarks as initialization.

        Notes
        -----
        The obtained transformation do NOT contain the initial transformation from 3D landmarks!
        """
        logger.info("Updating rigid transformation initialized by landmarks transformation...")
        # Compute the transformation and registered image to blend with reference & display in lower panel
        # We use the reference (left) image to resample prior to blending (it requires identical array shapes)
        self.rigid_trsf = blockmatching(self.lowres_images['right'], self.lowres_images['left'],
                                        method='rigid', left_trsf=self.ldmk_trsf, pyramid_lowest_level=2)
        return

    @staticmethod
    def imshow(ax, spim, extent, title="", value_range=None, **kwargs):
        """Plot the images."""
        # Use the "value range" or guess it:
        if value_range is None:
            vmin = spim.min()
            vmax = spim.max()
        else:
            vmin, vmax = value_range

        row_lab = f"{spim.axes_order[0]} ({spim.get_unit()})"
        col_label = f"{spim.axes_order[1]} ({spim.get_unit()})"
        # Plot this image
        fig_img = ax.imshow(spim, cmap='gray', extent=extent, vmin=vmin, vmax=vmax, interpolation='none',
                            origin='upper')
        ax.xaxis.tick_top()  # move the tick for the X-axis to the top

        if title != "":
            ax.set_title(title)
        else:
            try:
                ax.set_title(f"{spim.filename}")
            except:
                pass

        if kwargs.get('no_xtick', False):
            fig_img.axes.xaxis.set_ticklabels([])
        if kwargs.get('no_ytick', False):
            fig_img.axes.yaxis.set_ticklabels([])

        if kwargs.get('no_xlab', False):
            ax.set_xlabel("")
        else:
            ax.set_xlabel(col_label)  # add the axis label

        if kwargs.get('no_ylab', False):
            ax.set_ylabel("")
        else:
            ax.set_ylabel(row_lab)  # add the axis label

        return fig_img

    def _mpl_panel_extent(self, panel):
        """Compute the panel extent based on the image to plot."""
        row_extent, col_extent = self.projected_images[panel].get_extent()
        row_vxs, col_vxs = np.array(self.projected_images[panel].get_voxelsize()) / 2.
        return -col_vxs, col_extent - col_vxs, row_extent - row_vxs, -row_vxs

    def points_3d_coordinates(self):
        """Return a left/right dictionary with 3D landmarks in real units, column/row/alti (XYZ) sorted."""
        points_3d = {}
        for panel in ['left', 'right']:
            points_3d[panel] = []
            gui_points = self.ldmks_2d[panel]  # row/column (YX) sorted!!
            row_sh, col_sh = self.projected_images[panel].get_shape()
            row_vxs, col_vxs = self.projected_images[panel].get_voxelsize()
            # If a rotation is applied to the panel, "rotate back" the points
            if self.rotations[panel] != 0:
                deg = 360 - self.rotations[panel]
                _, gui_points = self._iter_rotate90(panel, deg)

            for (p_row, p_col) in gui_points:
                # Convert real-world point coordinates to image coordinates
                row = int(np.round(np.maximum(0, np.minimum(row_sh - 1, p_row / row_vxs))))
                col = int(np.round(np.maximum(0, np.minimum(col_sh - 1, p_col / col_vxs))))
                # Get the corresponding altitude value (real units)
                alti = self.altitude_maps[panel][row, col]  # row/column (YX) sorted!!
                points_3d[panel] += [(p_row, p_col, alti)]  # row/column/alti

            points_3d[panel] = np.array(points_3d[panel])
            # Check if axes needs re-ordering to get an "XYZ" sorted array of coordinates:
            current_axo = self.axes_2d[panel] + self.proj_axis[panel].upper()
            if current_axo != DEFAULT_LDMK_3D:
                transpose = [current_axo.index(ax) for ax in DEFAULT_LDMK_3D]
                points_3d[panel] = points_3d[panel][:, transpose]

        return points_3d

    def _iter_rotate90(self, panel, deg):
        # Get the rotation angle in ]-360, 360[:
        if deg > 0:
            deg = deg % 360
        else:
            deg = -(-deg % 360)
        # Number of 90° rotations to apply:
        n_iter = int(abs(deg / 90))
        img, pts = self.projected_images[panel], self.ldmks_2d[panel]
        for _iter in range(n_iter):
            img, pts = clockwise_rotate90_image_and_points(img, pts)
        return img, pts

    def rotate_panel(self, panel, deg):
        """Rotate selected panel (projected image & 2D landmarks) around the image center."""
        try:
            assert deg % 90 == 0
        except AssertionError:
            raise ValueError(f"Only multiples of 90° are accepted, got {deg}!")

        logger.info(f"Rotating {panel} panel by {deg}° (was {self.rotations[panel]}°)...")
        # Start by getting rotated image and points:
        self.projected_images[panel], self.ldmks_2d[panel] = self._iter_rotate90(panel, deg)

        # Update the rotation value of panel
        self.rotations[panel] += deg
        # Try to keep the rotation values in the [0, 360[ range
        self.rotations[panel] = self.rotations[panel] % 360

        self.update_main_panels()  # update panels & points
        return

    @staticmethod
    def save_landmarks_figure(fname):
        """Save the figure under given file name."""
        plt.savefig(fname)
        return


def clockwise_rotate90_image_and_points(image, pts, decimals=8):
    """Rotate a 2D image and points around the image center.

    Parameters
    ----------
    image : timagetk.SpatialImage
        The 2D image to rotate.
    pts : numpy.ndarray
        An ``Mx2`` array representing a set of point coordinates to rotate.

    Returns
    -------
    timagetk.SpatialImage
        The rotated image.
    numpy.ndarray
        The array of rotated coordinates.

    Raises
    ------
    AssertionError
        If the image is not 2D.
        If the given numpy array is not an ``Mx2`` set of points.

    Examples
    --------
    >>> import numpy as np
    >>> import matplotlib.pyplot as plt
    >>> from timagetk import SpatialImage
    >>> from timagetk.array_util import handmade_2d
    >>> from timagetk.gui.mpl_image_surface_landmarks import clockwise_rotate90_image_and_points
    >>> from timagetk.visu.mplt import image_plot
    >>> img = SpatialImage(handmade_2d())
    >>> row, col = [0, 9]
    >>> img[row, col] = 200
    >>> pts = np.array([[row, col], [1, 2]])
    >>> r_img, r_pts = clockwise_rotate90_image_and_points(img, pts)
    >>> fig, axes = plt.subplots(1, 2, figsize=(10, 5))
    >>> titles=["Original", f"Rotated by 90°"]
    >>> [image_plot(im, axe=ax) for im, ax in zip([img, r_img], axes)]
    >>> [ax.scatter(*p[:, ::-1].T, c=['red', 'blue']) for p, ax in zip([pts, r_pts], axes)]

    """
    try:
        assert image.is2D()
    except AssertionError:
        raise ValueError("Given `image` is not 2D!")
    try:
        assert pts.shape[1] == 2
    except AssertionError:
        raise ValueError("Given `pts` is not 2D!")

    def _rotate90_img(image, deg):
        axo = image.axes_order
        vxs = image.voxelsize
        arr = nd.rotate(image, -deg, axes=(1, 0))
        axo = axo[::-1]
        vxs = vxs[::-1]
        return SpatialImage(arr, axes_order=axo, origin=image.origin, voxelsize=vxs, metadata=image.metadata,
                            unit=image.unit)

    def _rotate90_pts(pts, deg, rotation_point):
        radians = np.deg2rad(deg)
        offset_x, offset_y = rotation_point

        if deg > 0:
            pts = np.vstack([pts, [rotation_point[0] * 2, 0]])
        else:
            pts = np.vstack([pts, [0, rotation_point[1] * 2]])

        x, y = pts.T
        adjusted_x = (x - offset_x)
        adjusted_y = (y - offset_y)
        cos_rad = math.cos(radians)
        sin_rad = math.sin(radians)
        qx = offset_x + cos_rad * adjusted_x + sin_rad * adjusted_y
        qy = offset_y + -sin_rad * adjusted_x + cos_rad * adjusted_y
        rotated_pts = np.array([qx, qy]).T

        if abs(offset_x - offset_y) != 0:
            translate = rotated_pts[-1, :]
            rotated_pts = rotated_pts - translate

        return rotated_pts[:-1, :]

    deg = 90
    rotation_point = np.array(image.get_extent()) / 2

    return _rotate90_img(image, deg), np.round(_rotate90_pts(pts, deg, rotation_point), decimals)
