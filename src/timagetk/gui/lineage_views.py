#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Module to project cell lineage on meshed labelled image with VTK."""
import sys

import numpy as np
import pyvista as pv

from timagetk import LabelledImage
from timagetk.components.tissue_image import cell_layers_from_image
from timagetk.features.utils import flatten
from timagetk.io import imread
from timagetk.visu.pyvista import tissue_image_cell_polydatas
from timagetk.visu.pyvista import tissue_image_unstructured_grid


def _load_image(seg_img, orientation, not_a_label, background):
    from timagetk import TissueImage3D

    if isinstance(seg_img, str):
        seg_img = TissueImage3D(imread(seg_img), not_a_label=not_a_label, background=background).transpose('xyz')
        # `.transpose('xyz')` as VTK works with XYZ sorted arrays:
    elif isinstance(seg_img, LabelledImage):
        seg_img = TissueImage3D(seg_img, not_a_label=seg_img.not_a_label, background=background).transpose('xyz')
        # `.transpose('xyz')` as VTK works with XYZ sorted arrays:
    elif isinstance(seg_img, TissueImage3D):
        pass
    else:
        raise TypeError("Parameter `seg_img` should either be a filename, a LabelledImage or a TissueImage3D instance!")

    # -- Revert the Z-axis if specified:
    if orientation == -1:
        return seg_img.invert_axis('z')
    else:
        return seg_img


def lineage_viewer(labelled_images, lineage, layers=None, exclusion_list=None, **kwargs):
    """Create VTK views of cells lineage, by layers, projected on two meshed labelled images.

    Parameters
    ----------
    labelled_images : list of str or list[timagetk.LabelledImage] or list of TissueImage3D
        A time-sorted list of labelled image used to create the lineage.
    lineage : ctrl.lineage.Lineage
        The lineage object to view.
    layers : list, optional
        A list of layers to render. By default, returns the first layer.
    exclusion_list : list, optional
        List of labels to exclude from visualisation.
        The labelled image background is automatically added to this list!

    Other Parameters
    ----------------
    orientation : {-1, 1}
        Image stack orientation, use `-1` with an inverted microscope.
        Defaults to `1`.
    background_color : list
        The color of the background as a len-3 list of RGB values in the `[0, 1]` range.
        Defaults to `[1., 1., 1.]` (white).
    filename : str
        A file name (and path) used to save the figure instead of creating an interactive GUI.
    figsize : list or tuple
        A len-2 list of values, in pixels, defining the window/figure size.
    not_a_label : int
        If specified, it defines the "unknown label" (*i.e.* a value that is not a label).
        Defaults to ``0``.
    background : int
        The id referring to the background, if any.
        Defaults to ``1``.
    trsf : numpy.ndarray
        A linear transformation to apply to the first mesh.
        Defaults to ``None``.
    estimate_trsf : bool
        If ``True``, and `trsf` is ``None``, register the first image onto the second using cells' barycenter and lineage.
        Defaults to ``False``.
    image_scale : int
        Scale factor when saving screenshots.
        Image sizes will be the ``figsize`` multiplied by this scale factor.
        Defaults to `2`.
    cmap : str
        The name of a matplotlib colormap to use.
        Defaults to `'Set2'`.
    nan_color : tuple of int
        A 4-tuple of int, each in `[0, 255]`, representing the RGBA value of the nan-color to use.
        Defaults to white `(255, 255, 255, 255)`.

    Returns
    -------
    pyvista.Plotter
        Plotting object to display vtk meshes or numpy arrays.

    Examples
    --------
    >>> from timagetk.bin.lineage_viewer import lineage_viewer
    >>> from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image
    >>> from timagetk import TissueImage3D
    >>> from timagetk.third_party.ctrl.lineage import Lineage
    >>> # Create synthetic spherical labelled images as TissueImage3D:
    >>> tissue0 = TissueImage3D(example_layered_sphere_labelled_image(extent=30), background=1, not_a_label=0)
    >>> tissue1 = TissueImage3D(example_layered_sphere_labelled_image(extent=60), background=1, not_a_label=0)
    >>> # Lineages are simples as there is no division:
    >>> lineage = Lineage({i: [i] for i in tissue0.cells.ids()})

    >>> plotter = lineage_viewer([tissue0, tissue1], lineage, layers=[1, 2], cmap='plasma')
    >>> plotter.show()

    >>> from timagetk.io import imread
    >>> tissue0 = imread('data/p58_down_interp_2x/watershed_segmentation/p58-t0-SEG_down_interp_2x.inr.gz', TissueImage3D, background=1, not_a_label=0)
    >>> tissue1 = imread('data/p58_down_interp_2x/watershed_segmentation/p58-t1-SEG_down_interp_2x.inr.gz', TissueImage3D, background=1, not_a_label=0)
    >>> from timagetk.third_party.ctrl.io import read_lineage
    >>> lineage = read_lineage('data/p58_down_interp_2x/lineage/p58_t0-1_lineage.txt')

    >>> plotter = lineage_viewer([tissue0, tissue1], lineage, layers=[1, 2], cmap='Set2')
    >>> plotter.show()

    >>> plotter = lineage_viewer([tissue0, tissue1], lineage, layers=[1, 2], filename='p58_t0-1_lineage.png')
    >>> plotter = lineage_viewer([tissue0, tissue1], lineage, layers=[1, 2], estimate_trsf=True, filename='p58_t0-1_lineage_reg.png')

    """
    from timagetk.bin.logger import get_dummy_logger
    logger = kwargs.get('logger', get_dummy_logger(sys._getframe().f_code.co_name))

    if layers is None:
        # Set the layers to the first one if none where specified:
        layers = [1]
    elif isinstance(layers, int):
        layers = [layers]
    else:
        layers = sorted(layers)
    n_layers = len(layers)

    not_a_label = kwargs.get('not_a_label', 0)
    background = kwargs.get('background', 1)
    orientation = kwargs.get("orientation", 1)
    cell_polydatas = kwargs.get('cell_polydatas', {0: None, 1: None})  # time-indexed dict of cell polydata
    cell_layers = kwargs.get('cell_layers', {0: None, 1: None})  # time-indexed dict of layer indexed list of cells
    background_color = kwargs.get('background_color', 'white')
    figsize = kwargs.get('figsize', (640 * 2, 640 * n_layers))
    fname = kwargs.get('filename', '')
    trsf = kwargs.get('trsf', None)
    estimate_trsf = kwargs.get('estimate_trsf', False)
    register = estimate_trsf or trsf is not None

    # Set/check the list of labels excluded from visu
    if exclusion_list is None:
        exclusion_list = set()
    else:
        exclusion_list = set(exclusion_list)

    cell_labels = {0: {}, 1: {}}  # time-indexed dict of cell labels
    bary = {0: {}, 1: {}}  # time-indexed dict of cell barycenters
    img_grids = {0: {}, 1: {}}  # time-indexed dict of cell unstructured grids
    # -- Pre-compute mesh & cell layers prior to display
    for t, label_img in enumerate(labelled_images):
        logger.info(f"# Processing t{t} image...")
        label_img = _load_image(label_img, orientation, not_a_label, background)
        if label_img.axes_order != "XYZ":
            label_img = label_img.transpose("XYZ")
        # - Add the background to the exclusion list
        exclusion_list |= {label_img.background}
        # - List cell labels from the image, excluding those from the `exclusion_list`:
        cell_labels[t] = list(set(label_img.labels()) - exclusion_list)

        # - Add lineage dictionary to project:
        if t == 0:
            from timagetk.visu.util import greedy_labelling
            greedy_label = greedy_labelling(label_img, background=background)
            lin_cell_labels = list(set(cell_labels[t]) & set(lineage.keys()))
            label_img.cells.set_feature('lineage', {label: label for label in lin_cell_labels})
            label_img.cells.set_feature('greedy_lineage', {label: greedy_label.get(label, np.nan) for label in lin_cell_labels})
        else:
            lin_cell_labels = list(set(cell_labels[t]) & set(flatten(lineage.values())))
            label_img.cells.set_feature('lineage', {label: lineage.ancestor(label) for label in lin_cell_labels})
            label_img.cells.set_feature('greedy_lineage', {label: greedy_label.get(lineage.ancestor(label), np.nan) for label in lin_cell_labels})

        # - Pre-compute layers-indexed dictionary of cells, if required:
        if cell_layers[t] is None:
            logger.info("# Computing cell layers...")
            cell_layers[t] = cell_layers_from_image(label_img, layers, labels=cell_labels[t])

        # - Pre-compute cells' mesh polydata, if required:
        if cell_polydatas[t] is None:
            logger.info("# Converting labelled image to PolyData...")
            cell_polydatas[t] = tissue_image_cell_polydatas(label_img, labels=cell_labels[t])

        for layer in layers:
            logger.info(f"# Computing unstructured grid for layer {layer}...")
            img_grids[t][layer] = tissue_image_unstructured_grid(label_img,
                                                                 labels=cell_layers[t][layer],
                                                                 resampling_voxelsize=1,
                                                                 cell_polydatas=cell_polydatas[t])
        if estimate_trsf:
            if t == 1:
                # Relabel the second image with lineage to compute daughter cells' barycenter:
                label_img = label_img.relabelling_cells_from_mapping(lineage.revert(), clear_unmapped=True)
            # Get the cells' barycenter:
            bary[t] = label_img.cells.barycenter(cell_labels[0], real=True)

    # Compute the rigid trsf with cell barycenter as landmarks (using cell lineage)
    if estimate_trsf:
        logger.info("Computing rigid registration using lineaged cells' barycenter...")
        from timagetk.algorithms.pointmatching import pointmatching
        common_labels = set(bary[0].keys()) & set(bary[1].keys())
        ldmk_0 = np.array([bary[0][c] for c in common_labels])
        ldmk_1 = np.array([bary[1][c] for c in common_labels])
        trsf = pointmatching(ldmk_0, ldmk_1, method='rigid').get_array()

    # Apply trsf to t0 mesh with `mesh.transform()`
    if register:
        for layer in layers:
            img_grids[0][layer] = img_grids[0][layer].transform(trsf)

    # Center the mesh to the origin:
    origin = np.array([0, 0, 0])
    for col, t in enumerate([0, 1]):
        for row, layer in enumerate(layers):
            mesh = img_grids[t][layer]
            img_grids[t][layer] = mesh.translate(origin - mesh.center)

    logger.debug("# Initializing window...")
    off_screen = fname != ""  # off-screen rendering if a filename is given
    plotter = pv.Plotter(shape=(n_layers, 2), window_size=figsize, off_screen=off_screen,
                         image_scale=kwargs.get('image_scale', 2))
    plotter.set_background(background_color)
    for col, t in enumerate([0, 1]):
        for row, layer in enumerate(layers):
            plotter.subplot(row, col)
            # Add the mesh to the plotter:
            plotter.add_mesh(img_grids[t][layer], name=f"t{t} L{layer}",
                             scalars='greedy_lineage', show_scalar_bar=False,
                             cmap=kwargs.get("cmap", "Set2"),
                             nan_color=kwargs.get("nan_color", (255, 255, 255, 255)))
            plotter.set_focus(origin)

    plotter.link_views()
    plotter.view_yx(negative=orientation == -1, render=False)
    plotter.zoom_camera(1.3)  # zoom in to fill more space with the actors

    if fname != "":
        plotter.show(screenshot=fname)

    return plotter
