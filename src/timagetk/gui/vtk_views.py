#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Module to project cell properties on meshed labelled image with VTK."""

import sys
from os.path import splitext

import numpy as np
import vtk
from timagetk.third_party.ctrl.io import read_lineage
from visu_core.vtk.actor import vtk_actor
from visu_core.vtk.actor import vtk_scalar_bar_actor
from visu_core.vtk.display import vtk_display_actors
from visu_core.vtk.utils.image_tools import image_to_vtk_cell_polydatas
from visu_core.vtk.utils.polydata_tools import vtk_combine_polydatas

from timagetk import TissueImage3D
from timagetk.algorithms.resample import resample
from timagetk.components.labelled_image import LabelledImage
from timagetk.components.tissue_image import cell_layers_from_image
from timagetk.features.utils import flatten
from timagetk.io import imread


def get_screensize():
    """Get the size of the current screen."""
    from PIL import ImageGrab
    img = ImageGrab.grab()  # grab a screenshot with pillow and return its size!
    return img.size


def _lineaged_cells_dict(lineage_files):
    """Return a dict of lineaged labels per time-point from a list of lineaged files.txt.

    It combines the label lineaged as ancestors and descendants when not the first or last time-point.
    This is mostly used for display purposes.

    Parameters
    ----------
    lineage_files : list
        List of lineage files.

    Returns
    -------
    dict
        Time-point based dictionary of lineaged labels.

    """
    lineaged_cells = {}
    for (t0, t1), lineage_file in lineage_files.items():
        lineage = read_lineage(lineage_file, fmt='marsalt')
        if t0 in lineaged_cells:
            lineaged_cells[t0].extend(lineage.get_ancestors())
        else:
            lineaged_cells[t0] = lineage.get_ancestors()
        if t1 in lineaged_cells:
            lineaged_cells[t1].extend(lineage.get_descendants())
        else:
            lineaged_cells[t1] = lineage.get_descendants()
    return lineaged_cells


def get_cell_labels(seg_img, exclusion_list=None):
    """Return a list of labels.

    Parameters
    ----------
    seg_img : timagetk.LabelledImage
        The labelled image containing the cells.
    exclusion_list : list, optional
        List of cells to exclude from returned array.

    Returns
    -------
    list
        The list of selected cells.

    Examples
    --------
    >>> from timagetk.gui.vtk_views import get_cell_labels
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk import TissueImage3D
    >>> im = TissueImage3D(shared_data('flower_labelled', 0), background=1, not_a_label=0)
    >>> cell_labels = get_cell_labels(im, exclusion_list=[1])
    >>> cell_labels

    """
    if exclusion_list is None:
        exclusion_list = set()
    else:
        exclusion_list = set(exclusion_list)

    return list(set(seg_img.labels()) - exclusion_list)


def vtk_viewer(labelled_image, data, name, layers=None, exclusion_list=None, value_range=None, **kwargs):
    """Create a vtk rendering for meshed labelled image and project given data dictionary.

    Parameters
    ----------
    labelled_image : str or timagetk.LabelledImage or timagetk.TissueImage3D
        A labelled image used to project the data.
    data : dict
        A label indexed dictionary of cell data to project on the labelled image.
    name : str
        The name of the data, used as scalar bar title.
    layers : list or dict, optional
        A list of layers to render. By default, returns the first layer.
        Can be a layer indexed dictionary of cell ids.
    exclusion_list : list, optional
        List of labels to exclude from visualisation.
        The labelled image background is automatically added to this list!
    value_range : list, optional
        Len-2 list of values indicating the range of the colormap.

    Other Parameters
    ----------------
    orientation : {-1, 1}
        Image stack orientation, use `-1` with an inverted microscope, `1` by default.
    colormap : str
        The colormap used to represent data. Scaled by `value_range`. Defaults to 'viridis'.
    n_labels : int
        The number of labels to represent on the color bar.
    background_color : list
        The color of the background as a len-3 list of RGB values in the [0, 1] range.
        Defaults to `[1., 1., 1.]` (white).
    filename : str
        A file name (and path) used to save the figure instead of creating an interactive GUI.
    figsize : list or tuple
        A len-2 list of values, in pixels, defining the window/figure size.
    not_a_label : int, optional
        If specified, it defines the "unknown label" (*i.e.* a value that is not a label).
        Defaults to ``0``.
    background : int, optional
        The id referring to the background, if any.
        Defaults to ``1``.
    labels : list
        List of labels to use. By default, use gather the ids in selected `layers` of layer indexed dict of cell ids


    Examples
    --------
    >>> from timagetk.gui.vtk_views import vtk_viewer
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk import TissueImage3D
    >>> im = TissueImage3D(shared_data('flower_labelled', 0), background=1, not_a_label=0)
    >>> volumes = im.cells.volume(real=True)  # compute cell volumes
    >>> vtk_viewer(im, volumes, 'Volume', layers=[1, 2, 3])  # interactive viewer
    >>> vtk_viewer(im, volumes, 'Volume', layers=[1, 2], filename='/tmp/test_vtk_viewer.png')  # save a snapshot & close

    """
    from timagetk.bin.logger import get_dummy_logger
    from timagetk.gui.lineage_views import _load_image
    logger = kwargs.get('logger', get_dummy_logger(sys._getframe().f_code.co_name))

    not_a_label = kwargs.get('not_a_label', 0)
    background = kwargs.get('background', 1)
    categorical = kwargs.get('categorical', False)
    orientation = kwargs.get("orientation", 1)


    # - Set/check the list of labels excluded from visu
    if exclusion_list is None:
        exclusion_list = set()
    else:
        exclusion_list = set(exclusion_list)

    labelled_image = _load_image(labelled_image, orientation, not_a_label, background)
    # - Add the background to the exclusion list
    exclusion_list |= {labelled_image.background}

    # -- Take care of hte
    # - Set the layers to the first one if none where specified:
    if layers is None:
        layers = [1]
    # - Get the layer indexes dictionary of cell ids:
    if isinstance(layers, dict):
        # Assume a dict of cell by layers:
        from copy import copy
        cells_by_layer = copy(layers)
        layers = sorted(cells_by_layer.keys())
    else:
        # Compute dict of cell by layers:
        cells_by_layer = cell_layers_from_image(labelled_image, layers)

    n_layers = len(layers)

    # -- Get list of cells, either from kwarg or gather the ids in selected `layers` of layer indexed dict of cell ids:
    labels = kwargs.get('labels', list(flatten(list(cells_by_layer.values()))))

    # -- Make sure we have the data we need, no more, no less:
    extra_labels = set(data.keys()) - set(labels)
    missing_labels = set(labels) - set(data.keys())
    # - Remove extra labels from data (for automatic value range)
    for label in extra_labels:
        try:
            data.pop(label)
        except KeyError:
            pass
    # - Add missing labels to data (to avoid crash from `image_to_vtk_cell_polydatas`)
    for label in missing_labels:
        data[label] = np.nan

    # -- Define/check colormap value range
    if value_range is None:
        values = np.array(list(data.values()))
        values = values[~np.isnan(values)]
        if len(values) == 0:
            value_range = [0, 1]
            logger.warning(f"All {name} data are `np.nan`!")
        else:
            value_range = min(values), max(values)
        logger.info(f"Computed value range: '{value_range}'.")
        del values
    else:
        try:
            assert len(value_range) == 2
        except AssertionError:
            raise ValueError(f"Parameter 'value_range' should be a len-2 list, got {value_range}!")
        try:
            all(isinstance(v, (int, float)) for v in value_range)
        except AssertionError:
            raise ValueError(f"Parameter 'value_range' should be a len-2 list of int or float, got {value_range} with type {[type(v) for v in value_range]}!")

    # logger.info("# Converting to VTK PolyData...")
    cell_polydata = image_to_vtk_cell_polydatas(labelled_image, labels=labels, cell_property=data, smoothing=10)

    renderers = []
    cam = None
    for i_l, layer in enumerate(layers):
        logger.debug(f"# Combining cell PolyData for layer {layer}...")
        missing_cells = []  # gather the cell missing in VTK PolyData
        cpd = []
        for cell in cells_by_layer[layer]:
            if cell in cell_polydata:
                cpd.append(cell_polydata[cell])
            else:
                missing_cells += [cell]
        # Warn for missing cells in VTK PolyData
        if missing_cells != []:
            n_miss = len(missing_cells)
            n_tot = len(cells_by_layer[layer])
            logger.warning(f"Could not find {n_miss} cells from layer {layer} (out of {n_tot}) in VTK PolyData!")
        vtk_polydata = vtk_combine_polydatas(cpd)

        actors = []
        cell_actor = vtk_actor(vtk_polydata, colormap=kwargs.get('colormap', 'viridis'),
                               value_range=value_range, categorical=categorical)
        actors += [cell_actor]

        # Add scalar bar actor to last sub-figure:
        if i_l == (len(layers) - 1):
            n_labels = kwargs.get('n_labels', 5)
            if categorical:
                ticks=range(1, n_labels+1)
            else:
                ticks=None
            scalar_bar = vtk_scalar_bar_actor(cell_actor, title=name, color='k', n_labels=n_labels, ticks=ticks)
            actors += [scalar_bar]
        ren, _, _ = vtk_display_actors(actors, background=kwargs.get('background_color', 'w'), show=False)

        # Get the camera from the first sub-figure and use it in the other ones ("lock" the views)
        if i_l == 0:
            cam = ren.GetActiveCamera()
        else:
            ren.SetActiveCamera(cam)
        renderers.append(ren)

    render_window = vtk.vtkRenderWindow()
    render_window_interactor = vtk.vtkRenderWindowInteractor()
    render_window_interactor.SetRenderWindow(render_window)

    x_start = 0.
    increment = 1. / n_layers
    for n, layer in enumerate(layers):
        if n != 0:
            x_start = x_stop
        x_stop = x_start + increment
        renderers[n].SetViewport(x_start, 0., x_stop, 1.)
        render_window.AddRenderer(renderers[n])

    figsize = kwargs.get('figsize', (640 * n_layers, 640))
    render_window.SetSize(figsize)  # width, height

    fname = kwargs.get('filename', '')
    if fname != '':
        render_window.SetOffScreenRendering(1)
        render_window.SetAlphaBitPlanes(1)
        render_window.Render()
        save_vtk_figure(render_window, fname)
        del render_window, render_window_interactor
    else:
        render_window.Render()
        render_window_interactor.Initialize()
        render_window_interactor.SetInteractorStyle(vtk.vtkInteractorStyleTrackballCamera())
        render_window_interactor.Start()

    return


def save_vtk_figure(render_window, fname):
    """Save a snapshot of a VTK render window.

    Parameters
    ----------
    render_window : vtk.vtkRenderWindow
        The VTK render window to save as in a file.
    fname : str
        The filename of the snapshot.
        Extensions can be in {'png', 'jpg', 'svg', 'eps'}.

    """
    prefix, ext = splitext(fname)
    if ext in ['.png', '.jpg']:
        window_to_image = vtk.vtkWindowToImageFilter()
        window_to_image.SetInput(render_window)
        window_to_image.SetInputBufferTypeToRGBA()
        window_to_image.ReadFrontBufferOff()
        window_to_image.Update()
        if ext == '.png':
            writer = vtk.vtkPNGWriter()
        elif ext == '.jpg':
            writer = vtk.vtkJPEGWriter()
        writer.SetInputConnection(window_to_image.GetOutputPort())
        writer.SetFileName(fname)
        writer.Write()
    elif ext in ['.svg', '.eps']:
        exporter = vtk.vtkGL2PSExporter()
        exporter.SetInput(render_window)
        exporter.Write3DPropsAsRasterImageOff()
        exporter.OcclusionCullOn()
        exporter.PS3ShadingOn()
        if ext == '.svg':
            exporter.SetFileFormatToSVG()
        elif ext == '.eps':
            exporter.SetFileFormatToEPS()
        exporter.SetFilePrefix(prefix)
        exporter.CompressOff()
        exporter.Write()
