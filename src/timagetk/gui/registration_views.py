#!/usr/bin/env python
# *- coding: utf-8 -*-
# -----------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# -----------------------------------------------------------------------------
import vtk
import time
import numpy as np
import pyvista as pv

from timagetk.components.spatial_image import SpatialImage
from timagetk.components.trsf import Trsf
from timagetk.visu.pyvista import intensity_image_volume
from timagetk.algorithms.trsf import inv_trsf
from timagetk.algorithms.resample import isometric_resampling
from timagetk.algorithms.blockmatching import blockmatching

# Distinct color maps for each floating image (reference is "red/Reds")
COLOR_MAPS = ["Blues", "Greens", "Oranges", "Purples"]
COLORS = ["tab:blue", "tab:green", "tab:orange", "tab:purple"]

def _get_state_checkbox(pv_checkbox):
    """
    Wrapper to get the state of a given Pyvista checkbox.

    Parameters
    ----------
    pv_checkbox: vtk.vtkButtonWidget
        The checkbox widget from Pyvista.
    """
    return pv_checkbox.GetRepresentation().GetState()

def _update_state_checkbox(pv_checkbox, state=None):
    """
    Wrapper to update the state of a given Pyvista checkbox. If state is None, switch the state.

    Parameters
    ----------
    pv_checkbox: vtk.vtkButtonWidget
        The checkbox widget from Pyvista.
    """
    previous_state = _get_state_checkbox(pv_checkbox)
    if state is None:
        # switch state if no state is given
        pv_checkbox.GetRepresentation().SetState(1 - previous_state)
    elif previous_state != state:
        # update state if needed
        pv_checkbox.GetRepresentation().SetState(state)

class Rigid3DRegistrationGUI:
    """
    A GUI class for rigidly registering 3D images onto a chosen reference image using PyVista.

    Usage Example (high-level):
    ---------------------------
    # 1) Load images externally
    images = [imread('path_to_img1'), imread('path_to_img2'), ...]

    # 2) Create the GUI object and show
    gui = Rigid3DRegistrationGUI(images, reference_index=0)
    gui.show()  # Start the interactive PyVista session

    # 3) Activate the "transformation" mode using the 'Rotation/Translation' button. Use widget to set some
    #    initial rotation & translation

    # 4) Use "Register" button to perform rigid registration.

    # 5) Export the rigid transformations by clicking on "Export" button

    # Optional behaviors:
    # * Use the "Reset trsf." to reset the initial rotation, translation or origin changes.
    # * Use the "Move origin" to move the center where the transformation are applied.
    # * Use colored checkbox to change the visibility of each image.
    # * Use the slider bar "Alpha" to change the opacity of each image.

    # Note that some interactions are not allowed if "transformation mode" is activated or deactivated. Look at
    # the log console if something goes wrong.

    Attributes
    ----------
    reference_index : int
        Index of the image to use as the reference image.
    images : list[SpatialImage]
        List of 3D SpatialImage objects, potentially resampled to isometric voxels. Will be 'xyz' oriented !
    rigid_trsfs : dict
        Dictionary storing rigid transformations for each floating image. Use export_trsf() to gather them.
    init_trsfs : dict
        Dictionary storing initial transformations (manual rotation/translation) for each floating image.
        Warning: the index of the init_trsfs does not correspond to the image indices. Please use export_initial_trsf()
        to gather the initial transformations.
    p : pv.Plotter
        PyVista Plotter instance used for visualization.
    pyramid_lowest_level : int
        The lowest pyramid level used for the blockmatching algorithm (rigid registration). Default is 3.

    Methods
    -------
    show()
        Start the GUI.
    export_trsf()
        Checks that all rigid transformations have been computed for each floating image and returns them as a dictionary.
        Dictionnary keys are organized as tuple (reference_index, floating_index) and values are the Trsf.

    export_initial_trsf()
        Return initial transformations (manual rotation/translation) for each floating image as a dictionary.
        Dictionnary keys are organized as tuple (reference_index, floating_index) and values are the Trsf.

    Known issues
    ------------
    1) After clicking on "Export", the pyvista.Plotter will be empty but not close automatically. Manually close the
    window to resume normal operation.
    2) Some problems can happen with the registration procedure. In such case, an identity matrix is returned and will
    be indicated in the console. Make sure that the images voxel size (original or from resampling) is sufficiently large
    according to the `pyramid_lowest_level` parameter. Try to decrease the `iso_voxelsize` or `pyramid_lowest_level`
    parameter in such cases.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.gui.registration_views import Rigid3DRegistrationGUI
    >>> from timagetk.algorithms.trsf import apply_trsf
    >>> print("Example 1: Register rigidly multi-angle images")
    >>> ref_img = shared_data('flower_multiangle', 1)
    >>> flo_img = shared_data('flower_multiangle', 2)
    >>>
    >>> gui = Rigid3DRegistrationGUI([ref_img, flo_img], reference_index=0, iso_voxelsize=0.5) # Initialize the GUI
    >>> gui.show() # Launch the GUI
    >>>
    >>> trsfs = gui.export_trsf() # Gather the resulting transformations
    >>> print('Rigid trsf: ', trsfs) # dictionnary of the transformations indexed by the input image indices.
    >>> print('Initial trsf: ', gui.export_initial_trsf()) # if needed, you can get the initial transformations manually set
    >>> reg_img = apply_trsf(flo_img, trsfs[(0, 1)], template_img=ref_img) # [(reference_index, floating_index)]
    >>> from timagetk.visu.stack import channels_stack_browser
    >>> channels_stack_browser([ref_img, reg_img], ['ref', 'reg'], ['red', 'green'], title="Manual registration")

    """
    def __init__(self, images, reference_index=0, iso_voxelsize=None, **kwargs):
        """
        Constructor for the Rigid3DRegistrationGUI.

        Parameters
        ----------
        images: list[timagetk.SpatialImages])
            List of 3D SpatialImage objects.
        reference_index: int, optional
            Index of the image to use as reference image. Default is 0.
        iso_voxelsize: float
            Image voxelsize if isometric resampling is used. Default is None.
            Notice that isometric images are required, if not, a default 0.75 is used.
        
        Optional Parameters
        -------------------
        window_size: tuple
            Initial window size (width, height) for the PyVista plotter. Default is (2000, 1000).
        pyramid_lowest_level: int
            Lowest pyramid level used for the blockmatching algorithm (rigid registration). Default is 3.
            This can be decreases (min: 0) if more accurate registration is required.
        """
        assert isinstance(images, list), "Images should be a list of SpatialImage objects."
        num_images = len(images)

        assert 2 <= num_images <= 5, "Number of images must be between 2 and 5."
        assert 0 <= reference_index <= num_images - 1, "Reference index must be between 0 and number of images - 1."

        # Fill images. Resample if needed.
        self.reference_index = reference_index
        self.images = []

        for img in images:
            assert isinstance(img, SpatialImage), "Images should be SpatialImage objects."
            assert img.ndim == 3, "Only 3D images are supported for registration."
            assert img.axes_order == 'ZYX', "Axis order should be zyx for all images"

            if iso_voxelsize is not None:
                img = isometric_resampling(img, value=iso_voxelsize, interpolation='linear')
            elif not img.is_isometric():
                print("Image is not isometric, iso-metric resampling will be applied with voxelsize of 0.75.")
                img = isometric_resampling(img, value=0.75, interpolation='linear')

            self.images.append(img.transpose()) # transpose to get XYZ order as it is in Pyvista

        # Gather the optional arguments
        window_size = kwargs.get('window_size', (2000, 1000))
        assert isinstance(window_size, tuple) and len(window_size) == 2, "Window size should be a tuple (width, height)."
        
        self.pyramid_lowest_level = kwargs.get('pyramid_lowest_level', 3)
        assert isinstance(self.pyramid_lowest_level, int), "Pyramid lowest level should be an integer."

        # Construct a mapping between the image index & the actor index (reference actor will always be last)
        flo_idx = [i for i in range(num_images) if i != self.reference_index]
        self._idx2img = {actor_idx: img_idx for actor_idx, img_idx in enumerate(flo_idx)}
        self._idx2img[num_images - 1] = self.reference_index

        self.init_trsfs = {}
        self.rigid_trsfs = {}

        self.p = pv.Plotter(window_size=window_size)
        self.p.iren.add_observer("WindowResizeEvent", self._on_window_resize)

        self._affine_widget = None
        self._sphere_widget = None
        self._floating_activated_idx = None
        self._checkboxes = {'visibility': {},
                            'transformation': {}}

        # Add all volumes and their widgets
        self._actors = []
        self._add_volumes_and_widgets()

        # Add registration & export checkboxes
        self._add_registration_and_export_checkbox()

        # Add reset & move origin checkboxes
        self._add_reset_and_move_origin_checkbox()

        # Add axes and bounds
        self.p.add_axes()
        self.p.show_bounds(location='origin', grid='back')

        # Set camera to get origin foreground & z-axis down
        self.p.view_vector(self.p.camera.GetDirectionOfProjection(), viewup=(0, 0, -1))

    #######################################################################################
    ### Methods related to creation of actors & widgets
    #######################################################################################
    def _add_volumes_and_widgets(self):
        """
        Add the images as volumes into the Plotter & associated widgets (visibility, opacity, transformation).
        """
        widget_spacing = 0.09

        # Add informative texts (Image, Rotation, Translation, etc...)
        self._add_text("Image", x_position=0.04, y_position=0.95)
        self._add_text("Alpha", x_position=0.16, y_position=0.95)
        self._add_text("  Rotation/\nTranslation", x_position=0.23, y_position=0.92)

        for actor_idx, img_idx in self._idx2img.items():
            if img_idx == self.reference_index:
                cmap, color = "Reds", "red"
                text_title = f"Reference:"
            else:
                cmap = COLOR_MAPS[actor_idx % len(COLOR_MAPS)] # e.g. ["Blues", "Greens", "Oranges", "Purples"]
                color = COLORS[actor_idx % len(COLORS)] # e.g. ["tab:blue", "tab:green", "tab:orange", ...]
                text_title = f"Floating {actor_idx}:"

            # Construct the actor of the current image
            volume = intensity_image_volume(self.images[img_idx])
            actor = self.p.add_volume(volume, opacity="sigmoid", cmap=cmap, show_scalar_bar=False, pickable=False)
            self._actors.append(actor)

            # Set up the y-position
            y_position = 0.87 - actor_idx * widget_spacing

            # Add a text for the floating image
            self._add_text(text_title, x_position=0.01, y_position=y_position-0.015, fontsize=14)

            # Add a checkbox for visibility & translation
            self._add_visibility_checkbox(y_position-0.02, actor_idx, color)

            # Add an opacity slider
            self._add_opacity_slider(y_position, actor_idx)

            if img_idx != self.reference_index:
                # Add a "push-button" checkbox to allow transformation on floating image
                self._add_transformation_checkbox(y_position - 0.02, actor_idx)
                self.init_trsfs[actor_idx] = np.eye(4)

    def _add_text(self, text, x_position, y_position, color="black", fontsize=15):
        """
        Helper to add a text in the Plotter.
        """
        self.p.add_text(
            text,
            position=(x_position, y_position),
            font_size=fontsize,
            color=color,
            font='arial',
            viewport=True
        )

    def _add_opacity_slider(self, y_position, actor_idx):
        """
        Helper to add an opacity slider widget for a given actor.
        """
        self.p.add_slider_widget(
            callback=lambda value, idx=actor_idx: self._update_opacity(value, idx),
            rng=[0, 2],
            value=0.5,
            style='modern',
            pointa=(0.13, y_position),
            pointb=(0.23, y_position)
        )

    def _add_visibility_checkbox(self, y_position, actor_idx, color_on):
        """
        Helper to add a checkbox button widget for toggling the actor's visibility.
        """
        x_position = 0.09
        window_width, window_height = self.p.window_size
        position = (x_position * window_width, y_position * window_height)
        size = min(int(0.04 * window_width), int(0.04 * window_height))

        c = self.p.add_checkbox_button_widget(
            callback=lambda state, idx=actor_idx: self._toggle_visibility(state, idx),
            value=True,
            position=position,
            size=size,
            color_on=color_on,
            color_off="white",
        )

        self._checkboxes['visibility'][actor_idx] = {'xy_position': (x_position, y_position),
                                                     'actor': c}

    def _add_transformation_checkbox(self, y_position, actor_idx):
        """
        Helper to add a checkbox button widget to enable/disable the transformation mode.
        """
        x_position = 0.26
        window_width, window_height = self.p.window_size
        position_t = (x_position * window_width, y_position * window_height)
        size = min(int(0.04 * window_width), int(0.04 * window_height))

        # Add a checkbox to activate or deactivate the transformation mode
        c = self.p.add_checkbox_button_widget(
            callback=lambda state, idx=actor_idx: self._toggle_transformation_mode(state, idx),
            value=False,
            position=position_t,
            size=size,
            color_on="orange",
            color_off="gray",
        )

        self._checkboxes['transformation'][actor_idx] = {'xy_position': (x_position, y_position),
                                                         'actor': c}

    def _add_reset_and_move_origin_checkbox(self):
        """
        Add a "reset" checkbox & "Move origin" that acts like push-buttons.
        """
        x_position = 0.94
        y_position = 0.87

        # Add a text & checkbox button for the "Reset" behavior
        self._add_text("Reset trsf:", x_position=x_position-0.075, y_position=y_position+0.005)

        # Add a checkbox to reset the transformation to identity
        window_width, window_height = self.p.window_size
        position_t = (x_position * window_width, y_position * window_height)
        size = min(int(0.04 * window_width), int(0.04 * window_height))

        c = self.p.add_checkbox_button_widget(
            callback= self._reset_transformation,
            value=False,
            position=position_t,
            size=size,
            color_on="orange",
            color_off="gray",
        )

        self._checkboxes['reset'] = {'xy_position': (x_position, y_position),
                                     'actor': c}

        # Add a text & checkbox button for the "Move origin" behavior
        y_position -= 0.065
        self._add_text("Move origin:", x_position=x_position-0.095, y_position=y_position+0.005)

        # Add a checkbox to reset the transformation to identity
        position_t = (x_position * window_width, y_position * window_height)
        size = min(int(0.04 * window_width), int(0.04 * window_height))

        c = self.p.add_checkbox_button_widget(
            callback= self._toggle_move_mode,
            value=False,
            position=position_t,
            size=size,
            color_on="orange",
            color_off="gray",
        )

        self._checkboxes['origin'] = {'xy_position': (x_position, y_position),
                                      'actor': c}

    def _add_registration_and_export_checkbox(self):
        """
        Add a "Register" checkbox & "Export" checkbox that acts like push-buttons.
        """
        x_position = 0.94
        y_position = 0.08
        window_width, window_height = self.p.window_size

        # Put the button in the bottom-right corner
        position = (x_position * window_width, y_position * window_height)
        size = min(int(0.04 * window_width), int(0.04 * window_height))

        # Put a registration checkbox & "Register" text
        c = self.p.add_checkbox_button_widget(
            callback=self._register_callback,
            value=False,
            position=position,
            size=size,
            color_on="orange",
            color_off="gray",
        )
        self._checkboxes['registration'] = {'xy_position': (x_position, y_position),
                                            'actor': c}

        self._add_text("Register:", x_position=x_position-0.068, y_position=y_position + 0.005)

        # Put an export checkbox & "Export" text
        y_position -= 0.065
        position = (x_position * window_width, y_position * window_height)

        c = self.p.add_checkbox_button_widget(
            callback=self._close_and_export_callback,
            value=False,
            position=position,
            size=size,
            color_on="orange",
            color_off="gray",
        )
        self._checkboxes['export'] = {'xy_position': (x_position, y_position),
                                      'actor': c}

        self._add_text("Export:", x_position=x_position-0.055, y_position=y_position + 0.005)

    #######################################################################################
    ### Methods related to visibility/opacity behavior
    #######################################################################################
    def _toggle_visibility(self, state, actor_idx):
        """
        Toggle visibility for the actor at index actor_idx.
        """
        self._actors[actor_idx].SetVisibility(state)
        self.p.render()

    def _update_opacity(self, value, actor_idx):
        """
        Update the opacity of the given actor at index actor_idx.
        Uses a scalar opacity unit distance approach.
        """
        self._actors[actor_idx].prop.SetScalarOpacityUnitDistance(value)
        self.p.render()

    #######################################################################################
    ### Methods related to transformation widgets behavior
    #######################################################################################
    def _toggle_transformation_mode(self, state, floating_idx):
        """
        Enable or disable the transformation mode for the floating actor at index floating_idx.
        """
        if state:
            self._enable_transformation_mode(floating_idx)
        else:
            self._disable_transformation_mode()

    def _toggle_move_mode(self, state):
        """
        Enable or disable the "move origin" mode.
        """
        # Assert that the transformation mode is activated
        if self._affine_widget is not None:
            if state:
                self._enable_move_mode()
            else:
                self._disable_move_mode()
        else:
            print('You need to activate the transformation mode to move the origin!')
            checkbox = self._checkboxes['origin']['actor']
            _update_state_checkbox(checkbox, state=0) # reset state to 0

    def _enable_transformation_mode(self, floating_idx):
        """
        Enable the transformation mode for the floating actor at index floating_idx.
        """
        # Assert that the user is not trying to change the transformation in "registration on" mode.
        reg_checkbox = self._checkboxes['registration']['actor']
        if _get_state_checkbox(reg_checkbox):
            print('Using the transformation mode on a registered image is forbidden! '
                  'Deactivate the "Register" checkbox first!')
            trans_checkbox = self._checkboxes['transformation'][floating_idx]['actor']
            _update_state_checkbox(trans_checkbox, state=0)  # deactivate the transformation checkbox
            return

        # Make sure that any previous affine widget has been removed
        self._disable_transformation_mode()

        # Assert that any other checkbox for "transformation" is deactivated (like a radio button)
        for actor_idx, info_checkbox in self._checkboxes['transformation'].items():
            if actor_idx != floating_idx:
                checkbox = info_checkbox['actor']
                _update_state_checkbox(checkbox, state=0)

        # Create an affine widget for the current floating actor
        img_center = self._get_img_center(self._idx2img[floating_idx])
        widget = self.p.add_affine_transform_widget(self._actors[floating_idx],
                                                    origin=list(img_center),
                                                    release_callback=lambda
                                                        user_matrix: self._on_release_affine_widget(user_matrix,
                                                                                                    floating_idx)
                                                    )
        self._affine_widget = widget

        # Store the current activated floating actor
        self._floating_activated_idx = floating_idx

        # Assert that the actor is visible, make it visible else
        if not self._actors[floating_idx].GetVisibility():
            self._actors[floating_idx].SetVisibility(True)
            checkbox = self._checkboxes['visibility'][floating_idx]['actor']
            _update_state_checkbox(checkbox, state=1)

        self.p.render()

    def _enable_move_mode(self):
        """
        Enable to move the origin of the AffineWidget using a sphere widget.
        """
        # Disable the AffineWidget
        self._affine_widget.disable()

        # Create a sphere widget that can move the origin of the AffineWidget
        widget = self.p.add_sphere_widget(
            callback= self._move_origin,
            center=self._affine_widget.origin,  # set at the origin of the AffineWidget
            radius=5.0,
            color='black',
            theta_resolution=16,
            phi_resolution=16,
            style='surface',
            test_callback=False,
        )
        self._sphere_widget = widget

    def _disable_move_mode(self):
        """
        Disable the "move origin" mode and remove the sphere widget.
        """
        # Remove the sphere widget & enable the AffineWidget
        if self._sphere_widget is not None:
            self.p.clear_sphere_widgets()
            self._sphere_widget = None

        # Make sure that the checkbox is deactivated
        checkbox = self._checkboxes['origin']['actor']
        _update_state_checkbox(checkbox, state=0)

        if self._affine_widget is not None:
            self._affine_widget.enable()

    def _disable_transformation_mode(self):
        """
        Disable the transformation mode.
        """
        # Remove the current affine widget
        if self._affine_widget is not None:
            self._affine_widget.remove()
            self._affine_widget = None

        # Deactivate the sphere widget if any
        self._disable_move_mode()

        self._floating_activated_idx = None
        self.p.render()

    def _on_release_affine_widget(self, user_matrix, floating_idx):
        """
        Keep track of the changes applied with the affine widget
        """
        self.init_trsfs[floating_idx] = user_matrix

    def _reset_transformation(self, _):
        """
        Reset the transformation for the activated floating actor in "transformation mode".
        """
        if self._affine_widget is not None:
            # Find the activated floating actor
            floating_idx = self._floating_activated_idx

            # Disable the sphere widget if any
            self._disable_move_mode()

            # Reset the origin of the AffineWidget
            img_center = self._get_img_center(self._idx2img[floating_idx])
            self._affine_widget.origin = img_center

            # Reset the AffineWidget & reset the corresponding initial transformation
            self._affine_widget._reset() # call the private method in the AffineWidget

            # Update the initial transformation
            self.init_trsfs[floating_idx] = self._actors[floating_idx].user_matrix
        else:
            print('You need to activate the transformation mode to reset the transformation!')

        # deactivate the checkbox (act as a "push-button")
        reset_checkbox = self._checkboxes['reset']['actor']
        _update_state_checkbox(reset_checkbox, state=0)

        self.p.render()

    def _move_origin(self, new_center):
        """
        Move the origin of the AffineWidget to the new_center.
        """
        self._affine_widget.origin = new_center
        self.p.render()

    #######################################################################################
    ### Methods related to transformation computation & export
    #######################################################################################
    def _get_img_center(self, img_idx):
        """
        Compute the center of the image at index img_idx. Axis order XYZ !
        """
        extent = self.images[img_idx].get_extent()
        return np.array(extent) / 2.0

    def _register_callback(self, state):
        """
        Trigger the rigid blockmatching registration or reset transformations
        depending on the state of the 'Register' checkbox.
        """
        # For each floating actor
        for actor_idx in range(len(self._actors)):
            floating_img_idx = self._idx2img[actor_idx]

            if floating_img_idx == self.reference_index:
                continue # floating image can not be the reference image

            if not self._actors[actor_idx].GetVisibility():
                continue # Do not register non-visible actors

            if state:
                # Disable the "transformation" mode & corresponding checkbox
                self._disable_transformation_mode()
                checkbox = self._checkboxes['transformation'][actor_idx]['actor']
                _update_state_checkbox(checkbox, state=0)

                # If "Register" is toggled on, run blockmatching
                floating_img = self.images[floating_img_idx].transpose() # transpose XYZ -> ZYX
                reference_img = self.images[self.reference_index].transpose() # transpose XYZ -> ZYX

                # Apply blockmatching using the inverse of the initial transformation (vt convention)
                init_trsf = Trsf(self.init_trsfs[actor_idx])  # convert in Trsf for blockmatching
                rigid_trsf = blockmatching(floating_img, reference_img, method="rigid",
                                               init_trsf=inv_trsf(init_trsf),
                                               pyramid_lowest_level=self.pyramid_lowest_level)
                self.rigid_trsfs[(self.reference_index, floating_img_idx)] = rigid_trsf
                
                # We have a transformation from reference -> floating (vt convention)
                # but we need to visualize in PyVista => we inverse the rigid transformation
                rigid_trsf_inv = inv_trsf(rigid_trsf).get_array()
                self._actors[actor_idx].user_matrix = rigid_trsf_inv # XYZ ordered

                if np.allclose(rigid_trsf_inv, np.identity(4)):
                    print("Something wrong happens, an exact identity matrix have been computed. Verify that there is"
                          " not a problem with the input images or that the images voxel size (original or from resampling) "
                          "is not too large!.")

                # Example: How to visualize the resulting registered floating images in the Plotter ?
                # img_reg = apply_trsf(floating_img, rigid_trsf, template_img=reference_img)
                # volume = intensity_image_volume(image=img_reg.transpose())
                # cmap = COLOR_MAPS[actor_idx % len(COLOR_MAPS)]
                # p.add_volume(volume, opacity="sigmoid", cmap=cmap, show_scalar_bar=False)
            else:
                # If "Register" is toggled off, reset to the original rotation states
                self._actors[actor_idx].user_matrix = self.init_trsfs[actor_idx]

        self.p.render()

    def export_initial_trsf(self):
        """
        Export the initial transformations ie. the transformations (rotation/translation) that have been set
         manually in the Plotter. Initial transformations will be Trsf with vt-convention (use it with apply_trsf).

        Return
        ------
        dict
            The dictionary of the initial transformations. Keys are tuple (reference_img_idx, floating_img_idx) and
            values are the Trsf initial transformations.
        """
        ref_idx = self.reference_index

        init_trsfs = {}
        for actor_idx, img_idx in self._idx2img.items():
            if img_idx != ref_idx:
                init_trsf = Trsf(self.init_trsfs[actor_idx])
                init_trsfs[(ref_idx, img_idx)] = inv_trsf(init_trsf) # store the inv_trsf (vt_convention)

        return init_trsfs

    def export_trsf(self):
        """
        Return rigid transformations if all have been computed. Transformations are Trsf object with vt-convention
        ie. can be used with apply_trsf on floating images.

        Return
        ------
        dict
            The dictionary of the rigid transformations. Keys are tuple (reference_img_idx, floating_img_idx) and
            values are the rigid transformations.
        """
        ref_idx = self.reference_index
        missing_floating_idx = []
        for actor_idx, img_idx in self._idx2img.items():
            if img_idx == ref_idx:
                continue
            elif (ref_idx, img_idx) not in self.rigid_trsfs:
                missing_floating_idx += [actor_idx]

        if len(missing_floating_idx) > 0:
            print(f"No rigid transformation found for Floating image {missing_floating_idx}. Apply all the registration first.")
            return None
        else:
            return self.rigid_trsfs

    #######################################################################################
    ### Methods related to plotter behavior
    #######################################################################################
    def _on_window_resize(self, interactor, event):
        """
        When the window is resized or the user clicks, re-position
        the checkbox button widgets to keep them in the correct place.
        """
        for ele in self._checkboxes.values():
            # Check if nested dictionnary or not (depends on the checkboxes)
            if 'actor' in ele.keys():
                actor, xy_position = ele['actor'], ele['xy_position']
                self._resize_checkboxes(actor, xy_position)
            else:
                for actor_idx, sub_ele in ele.items():
                    actor, xy_position = sub_ele['actor'], sub_ele['xy_position']
                    self._resize_checkboxes(actor, xy_position)

    def _resize_checkboxes(self, vtk_checkbox, xy_position):
        """
        Resize a given checkbox according to the window size and relative position
        """
        try:
            window_width, window_height = self.p.window_size
            x_position, y_position = xy_position

            vtk_representation = vtk_checkbox.GetRepresentation()
            new_position = (x_position * window_width, y_position * window_height)
            size = min(int(0.04 * window_width), int(0.04 * window_height))

            # Update the position of the checkbox
            xmin, xmax = new_position[0], new_position[0] + size
            ymin, ymax = new_position[1], new_position[1] + size
            vtk_representation.PlaceWidget((xmin, xmax, ymin, ymax, 0, 0))
        except Exception as e:
            print(f"Error updating checkbox at xy={xy_position}: {e}")

    def _close_and_export_callback(self, _):
        """
        Close the GUI. Assert that all the transformations have been computed.
        """
        # Gather the transformations
        trsfs = self.export_trsf()

        # Close the Plotter if OK
        if trsfs is not None:
            self.p.clear()
            # self.p.close() # This produce an error...for some reason. The plotter need to be close manually !
        else:
            # Reset the state of the checkbox after 0.5s
            time.sleep(0.5)
            _update_state_checkbox(self._checkboxes['export']['actor'], state=0)

    def show(self):
        """
        Start the GUI.
        """
        self.p.show()
