#!/usr/bin/env python
# *- coding: utf-8 -*-
# -----------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# -----------------------------------------------------------------------------
from __future__ import annotations

import json
import os
import re
import shutil
import tkinter as tk
import tkinter.font as tkfont
import unicodedata
from collections import defaultdict
from tkinter import filedialog
from tkinter import messagebox
from tkinter import ttk
from typing import Any

import numpy as np
from PIL import Image
from PIL import ImageTk

from timagetk.algorithms.exposure import global_contrast_stretch
from timagetk.array_util import to_uint8
from timagetk.bin.logger import get_dummy_logger
from timagetk.components.multi_channel import MultiChannelImage
from timagetk.io import imread

logger = get_dummy_logger('DatasetCreationManager')

# Path to the icons
ICONS_DIR = os.path.join(os.path.dirname(__file__), "icons")
ICON_OK_PATH = os.path.join(ICONS_DIR, "green_ok.png")
ICON_NOK_PATH = os.path.join(ICONS_DIR, "red_cross.png")
DEFAULT_FNAME_PATTERN = "%dataset_%species_%sample"


#############################################################################
### Main application: window, file management, drag&drop logic and cell class
#############################################################################
class DatasetCreationManager(tk.Tk):
    """
    DatasetCreationManager is the main application window for the TimageTK - Dataset Creation Tool.

    This application provides a complete graphical interface to import, manage, and export image datasets.
    It allows users to:

      - Import individual image files or entire folders.
      - Dynamically arrange images on a grid, ensuring a minimum number of rows and columns.
      - View image metadata and a larger preview of the selected image.
      - Drag and drop images to rearrange their positions, with visual feedback and smart grid
        adjustments (including automatic row/column addition or removal).
      - Remove individual images or clear the entire grid.
      - Export the assembled dataset using a separate ExportManager, which organizes the output
        into a predefined folder structure.

    The interface is divided into three main parts:

      1. **Top Controls:**
         - Contains buttons for importing files or folders.
         - Provides options to remove a single file or clear all files from the grid.

      2. **Paned Panels:**
         - The left panel displays the grid of image cells where each cell is an instance of ImageCell.
         - The right panel shows detailed metadata (such as filename and image properties)
           and a larger preview of the currently selected image.

      3. **Export Controls (Bottom Panel):**
         - Allows the user to choose between moving or copying images during the export process.
         - Contains the "Export Dataset" button that launches the export process.

    Grid Management:
      - The grid is initialized with a minimum of MIN_ROWS (3) and MIN_COLS (4).
      - The grid expands dynamically: new rows or columns are added as needed when importing new images.
      - Empty rows or columns are removed automatically if all cells become empty.
      - Users can select any cell to view its metadata, and dragging a cell enables a visual “ghost”
        label to indicate potential drop targets.

    Drag and Drop:
      - The app supports drag-and-drop to swap images between cells.
      - It computes valid drop targets (including outside-the-grid drops which trigger grid expansion).
      - Visual cues (using icons) indicate whether a drop is allowed, and additional logic handles
        row/column removal if a cell is moved leaving its original row or column empty.

    Export Functionality:
      - When the user triggers export, the current grid (with all its image cells) is handed off to an
        ExportManager instance.
      - The ExportManager collects export metadata via a separate ExportWindow dialog, validates the images,
        builds JSON structures, and writes the images and JSON files to disk in a specific folder layout:

            {dataset_name}
                  ├── raw
                  │      ├── image files (renamed)
                  │      └── processing.json
                  └── {dataset_name}.json

    Methods
    -------
    run()
        Run the application.

    Examples
    --------
    >>> from timagetk.gui.dataset_creation import DatasetCreationManager
    >>> manager = DatasetCreationManager()
    >>> manager.run()
    """
    # Minimal number of rows and columns in the grid
    MIN_ROWS = 3
    MIN_COLS = 4

    def __init__(self):
        super().__init__()

        self.title("TimageTK - Dataset Creation Tool")
        self.geometry("1200x530")
        self.tk_image = None

        self._create_top_controls()
        self._create_export_controls()
        self._create_paned_panels()
        self._create_grid()
        self._create_icon()
        self._bind_events()

        self.selected_cell_id = None
        self.dragging_cell_id = None
        self.drag_enabled = False

        self.ghost_label = None
        self.ghost_delay_id = None
        self.ghost_icon_current = self.icon_ok

    ### ----------- Layout Creation & Initialization ----------- ###
    def _create_top_controls(self):
        frame_top = tk.Frame(self)
        frame_top.pack(side="top", fill="x", padx=5, pady=5)
        self._create_import_buttons(frame_top)
        self._create_remove_buttons(frame_top)

    def _create_import_buttons(self, parent):
        frame_left = tk.Frame(parent)
        frame_left.pack(side="left", anchor="w", padx=10, pady=5)
        btn_file = tk.Button(frame_left, text="Import File(s)", command=self.import_files)
        btn_file.pack(side="left", padx=5)
        btn_folder = tk.Button(frame_left, text="Import Folder", command=self.import_folder)
        btn_folder.pack(side="left", padx=5)

    def _create_remove_buttons(self, parent):
        frame_right = tk.Frame(parent)
        frame_right.pack(side="right", anchor="w", padx=10, pady=5)
        btn_remove = tk.Button(frame_right, text="Remove", command=self.remove_selected_file)
        btn_remove.pack(padx=5, side="left")
        btn_remove_all = tk.Button(frame_right, text="Remove All", command=self.remove_all_files)
        btn_remove_all.pack(padx=5, side="left")

    def _create_export_controls(self):
        frame_bottom = tk.Frame(self)
        frame_bottom.pack(side="bottom", fill="x", padx=5, pady=5, anchor="e")
        frame_bottom_right = tk.Frame(frame_bottom)
        frame_bottom_right.pack(side="right", anchor="e", padx=10, pady=5)
        lbl_export_title = tk.Label(frame_bottom_right, text="Move or Copy Images?")
        lbl_export_title.grid(row=0, column=0, columnspan=3, sticky="w", pady=(0, 5))
        self.operation_mode = tk.StringVar(value="copy")
        rb_move = tk.Radiobutton(frame_bottom_right, text="Move", variable=self.operation_mode, value="move")
        rb_move.grid(row=1, column=0, padx=5, sticky="w")
        rb_copy = tk.Radiobutton(frame_bottom_right, text="Copy", variable=self.operation_mode, value="copy")
        rb_copy.grid(row=1, column=1, padx=5, sticky="w")
        btn_export = tk.Button(frame_bottom_right, text="Export Dataset", command=self.export_dataset)
        btn_export.grid(row=1, column=2, padx=(10, 0), sticky="w")

    def _create_paned_panels(self):
        self.paned = tk.PanedWindow(self, orient="horizontal")
        self.paned.pack(fill="both", expand=True)
        self._create_left_panel()
        self._create_right_panel()

    def _create_left_panel(self):
        self.left_panel = tk.Frame(self.paned)
        self.left_panel.grid_rowconfigure(0, weight=1)
        self.left_panel.grid_columnconfigure(0, weight=1)
        canvas = tk.Canvas(self.left_panel, bg="white", height=150)
        canvas.grid(row=0, column=0, sticky="nsew")
        h_scrollbar = tk.Scrollbar(self.left_panel, orient="horizontal", command=canvas.xview)
        h_scrollbar.grid(row=1, column=0, sticky="ew")
        v_scrollbar = tk.Scrollbar(self.left_panel, orient="vertical", command=canvas.yview)
        v_scrollbar.grid(row=0, column=1, sticky="ns")
        canvas.configure(xscrollcommand=h_scrollbar.set, yscrollcommand=v_scrollbar.set)
        self.frame_grid = tk.Frame(canvas)
        canvas.create_window((0, 0), window=self.frame_grid, anchor="nw")
        self.frame_grid.bind("<Configure>", lambda e: canvas.configure(scrollregion=canvas.bbox("all")))
        self.paned.add(self.left_panel)

    def _create_right_panel(self):
        self.right_panel = tk.Frame(self.paned, bd=2, relief="sunken")
        self.right_panel.pack_propagate(False)
        self.label_info_title = tk.Label(self.right_panel, text="", justify="left")
        self.label_info_title.pack(fill="x", padx=5, pady=(5, 0))
        title_font = tkfont.Font(family="Helvetica", size=12, weight="bold", underline=True)
        self.label_info_title.configure(font=title_font)
        self.label_info_data = tk.Label(self.right_panel, text="", justify="left", anchor="w")
        self.label_info_data.pack(fill="x", padx=5, pady=(0, 5))
        self.label_preview = tk.Label(self.right_panel, bg="#222")
        self.label_preview.pack(fill="both", expand=True, padx=5, pady=5)
        self.tk_image = None
        self.paned.add(self.right_panel)
        self.paned.bind("<Configure>", self.update_paned_sizes)

    def _create_grid(self):
        self.cells = {}
        start_rows = self.MIN_ROWS
        start_cols = self.MIN_COLS
        for r in range(start_rows):
            self.frame_grid.rowconfigure(r, weight=1)
            for c in range(start_cols):
                self.frame_grid.columnconfigure(c, weight=1)
                cell_id = (r, c)
                cell = ImageCell(self.frame_grid, cell_id)
                cell.grid(row=r, column=c, padx=2, pady=2, sticky="nsew")
                self.cells[cell_id] = cell

    def _create_icon(self):
        icon_ok = Image.open(ICON_OK_PATH)
        icon_ok = ImageUtils.resize_image_to_fit(icon_ok, (20, 20))
        self.icon_ok = ImageTk.PhotoImage(icon_ok)

        icon_nok = Image.open(ICON_NOK_PATH)
        icon_nok = ImageUtils.resize_image_to_fit(icon_nok, (20, 20))
        self.icon_nok = ImageTk.PhotoImage(icon_nok)

    def _bind_events(self):
        self.bind("<Button-1>", self.on_mouse_down)
        self.bind("<B1-Motion>", self.on_mouse_move)
        self.bind("<ButtonRelease-1>", self.on_mouse_up)

    ### ----------- File Management Methods ----------- ###
    @staticmethod
    def _alphanum_key(s: str):
        """
        Split string s into a list of strings and integers for natural sorting.
        """
        return [int(text) if text.isdigit() else text.lower() for text in re.split('([0-9]+)', s)]

    def import_files(self) -> None:
        """
        Select one or several image files, validate them, and place them in the grid.
        """
        valid_formats = [f"*{ext}" for ext in ImageUtils.get_valid_image_formats()]
        file_paths = filedialog.askopenfilenames(
            title="Select one or several images",
            filetypes=[("Images", " ".join(valid_formats))]
        )
        # Sort the file paths by the base filename in alphanumeric order.
        for filepath in sorted(file_paths, key=lambda f: self._alphanum_key(os.path.basename(f))):
            self.process_file(filepath)

    def import_folder(self) -> None:
        """
        Select a folder, filter for valid image files, and process each one.
        """
        folder_path = filedialog.askdirectory(title="Select folder containing images")
        if not folder_path:
            return

        # Sort the files in alphanumeric order.
        for file in sorted(os.listdir(folder_path), key=self._alphanum_key):
            self.process_file(os.path.join(folder_path, file))

    def process_file(self, filepath: str) -> bool:
        """
        Process a single file: validate, check for duplicates, add it in the grid.

        Parameters
        ----------
        filepath: str
            The path to the file to process.

        Returns
        -------
        bool
            True if was processed, False otherwise
        """
        if not ImageUtils.is_valid_image(filepath):
            logger.info(f"File {filepath} not recognized as a valid image format. Skipping.")
            return False

        if any(cell.filepath == filepath for cell in self.cells.values()):
            logger.info(f"File {filepath} already imported. Skipping.")
            return False

        self.add_file_in_grid(filepath)
        return True

    def add_file_in_grid(self, filepath: str) -> None:
        """
        Load a file from a filepath and place the file in the first free cell at row=0.
        If none is free, extend the grid by adding a new column. Then place the file.

        Parameters
        ----------
        filepath: str
            The path to the file to process.

        """
        # Load the metadata of the file
        preview_array, meta = ImageUtils.load_image_metadata(filepath)

        # Get the latest column index of the row 0 with a non-empty cell
        cells = self.get_row(0, ignore_empty=True)
        latest_non_empty = max((cell.cell_id[1] for cell in cells), default=-1)

        # The new file should be placed in the column immediately after the last non-empty cell.
        new_index = latest_non_empty + 1

        # If the new index is outside the current columns, add a new column.
        col_count = self.get_col_count()
        if new_index >= col_count:
            self.add_column()

        cell_id = (0, new_index)
        cell = self.cells[cell_id]
        cell.add_image_data(filepath, meta, preview_array, thumb_size=(500, 250))

        if self.selected_cell_id is None:
            self.select_cell(cell_id)

    def remove_selected_file(self) -> None:
        """
        Remove the selected file from the grid.
        """
        if self.selected_cell_id is None:
            return

        cell = self.cells.get(self.selected_cell_id)
        if not cell.is_empty():
            # Clear image data & update displayed metadata
            cell.clear_image_data()
            self.show_metadata(self.selected_cell_id)

            # Remove the row/column if needed.
            row, col = cell.cell_id
            if self.is_row_empty(row):
                self.remove_row(row)
            if self.is_col_empty(col):
                self.remove_col(col)

            self.ensure_min_grid()

    def remove_all_files(self) -> None:
        """
        Removes every file from all cells, leaving the grid empty.
        Then remove useless rows/columns.
        """
        for cell in self.cells.values():
            cell.clear_image_data()

        # Remove useless empty rows/cols
        for row in reversed(range(self.MIN_ROWS, self.get_row_count())):
            self.remove_row(row)
        for col in reversed(range(self.MIN_COLS, self.get_col_count())):
            self.remove_col(col)

        # Select the first cell
        self.select_cell((0, 0))

    ### ----------- Grid Management Methods ----------- ###
    def get_row_count(self) -> int:
        """
        Return the number of row in the grid of cells
        """
        return max((r for r, _ in self.cells.keys()), default=-1) + 1

    def get_col_count(self) -> int:
        """
        Return the number of column in the grid of cells
        """
        return max((c for _, c in self.cells.keys()), default=-1) + 1

    def get_row(self, row_index: int, ignore_empty: bool = True) -> list:
        """
        Returns the cells in the given row.

        Parameters
        ----------
        row_index: int
            The row index from which the cells should be returned.
        ignore_empty: bool, optional
            Ignore the empty cells. If True, return an empty list if the row is empty.

        Returns
        -------
        list
            The list of the cells in the given row.
        """
        return [
            cell for (r, c), cell in self.cells.items()
            if r == row_index and not (ignore_empty and cell.is_empty())
        ]

    def get_column(self, col_index: int, ignore_empty: bool = True) -> list:
        """
        Returns the cells in the given column.

        Parameters
        ----------
        col_index: int
            The column index from which the cells should be returned.
        ignore_empty: bool, optional
            Ignore the empty cells. If True, return an empty list if the column is empty.

        Returns
        -------
        list
            The list of the cells in the given column.
        """
        return [
            cell for (r, c), cell in self.cells.items()
            if c == col_index and not (ignore_empty and cell.is_empty())
        ]

    def add_row(self) -> None:
        """
        Append a new row at the bottom of the grid
        """
        num_rows = self.get_row_count()
        num_cols = self.get_col_count()

        self.frame_grid.rowconfigure(num_rows, weight=1)
        for col in range(num_cols):
            self.frame_grid.columnconfigure(col, weight=1)
            new_cell = ImageCell(self.frame_grid, (num_rows, col))
            new_cell.grid(row=num_rows, column=col, padx=2, pady=2, sticky="nsew")
            self.cells[(num_rows, col)] = new_cell

    def add_column(self) -> None:
        """
        Append a new column at the right of the grid
        """
        num_cols = self.get_col_count()
        num_rows = self.get_row_count()

        self.frame_grid.columnconfigure(num_cols, weight=1)
        for row in range(num_rows):
            self.frame_grid.rowconfigure(row, weight=1)
            new_cell = ImageCell(self.frame_grid, (row, num_cols))
            new_cell.grid(row=row, column=num_cols, padx=2, pady=2, sticky="nsew")
            self.cells[(row, num_cols)] = new_cell

    def remove_row(self, row_index: int) -> None:
        """
        Remove the row at the given index.
        It removes every cell in that row and shifts all subsequent rows upward.

        Parameters
        ----------
        row_index: int
            The row index to remove.
        """
        row_cells = self.get_row(row_index, ignore_empty=False)
        if not row_cells:
            return

        # Destroy cells in the row.
        for cell in row_cells:
            cell.grid_forget()
            cell.destroy()
            del self.cells[cell.cell_id]

        # Shift cells in rows below upward.
        for (r, c) in sorted(list(self.cells.keys())):
            if r > row_index:
                curr_cell = self.cells.pop((r, c))
                new_id = (r - 1, c)
                curr_cell.set_cell_id(new_id)
                curr_cell.grid_configure(row=r - 1, column=c)
                self.cells[new_id] = curr_cell

        self.ensure_min_grid()

    def remove_col(self, col_index: int) -> None:
        """
        Remove the column at the given index.
        It removes every cell in that column and shifts all subsequent columns to the left.


        Parameters
        ----------
        col_index: int
            The column index to remove.
        """
        col_cells = self.get_column(col_index, ignore_empty=False)
        if not col_cells:
            return

        # Destroy cells in the column.
        for cell in col_cells:
            cell.grid_forget()
            cell.destroy()
            del self.cells[cell.cell_id]

        # Shift cells in columns to the right leftward.
        for (r, c) in sorted(list(self.cells.keys())):
            if c > col_index:
                curr_cell = self.cells.pop((r, c))
                new_id = (r, c - 1)
                curr_cell.set_cell_id(new_id)
                curr_cell.grid_configure(row=r, column=c - 1)
                self.cells[new_id] = curr_cell

        self.ensure_min_grid()

    def ensure_min_grid(self) -> None:
        """
        Ensures the grid has at least MIN_ROWS and MIN_COLS. Add rows & columns if needed.
        """
        num_rows = self.get_row_count()
        num_cols = self.get_col_count()

        while num_rows < self.MIN_ROWS:
            self.add_row()
            num_rows += 1

        while num_cols < self.MIN_COLS:
            self.add_column()
            num_cols += 1

    def is_row_empty(self, row_index: int) -> bool:
        """
        Return True if every cell in the given row is empty.

        Parameters
        ----------
        row_index: int
            The row index to check.
        """
        row_cells = self.get_row(row_index, ignore_empty=False)
        return bool(row_cells) and all(cell.is_empty() for cell in row_cells)

    def is_col_empty(self, col_index: int) -> bool:
        """
        Return True if every cell in the given column is empty.

        Parameters
        ----------
        col_index: int
            The column index to check.
        """
        col_cells = self.get_column(col_index, ignore_empty=False)
        return bool(col_cells) and all(cell.is_empty() for cell in col_cells)

    ### ----------- Selection Methods ------------- ###
    def show_metadata(self, cell_id: tuple[int, int]) -> None:
        """
        Displays metadata in the right panel and shows the thumbnail from the selected cell.

        Parameters
        ----------
        cell_id: tuple[int, int]
            The cell index (row, column) from which the metadata should be displayed.
        """
        cell = self.cells[cell_id]
        if cell.is_empty():
            self.label_info_title.config(text="(Empty)")
            self.label_info_data.config(text="", anchor="w", justify="left")
            self.label_preview.config(image="", bg="#222")
        else:
            filepath, meta, thumbnail = cell.get_data()

            self.label_info_title.config(text="Image Information")
            lines = [f"Filename:  {os.path.basename(filepath)}"] + [f"{k}:  {v}" for k, v in meta.items()]
            text_info = "\n".join(lines)
            self.label_info_data.config(text=text_info, anchor="w", justify="left")

            self.tk_image = ImageTk.PhotoImage(thumbnail)
            self.label_preview.config(image=self.tk_image, bg="#222")

    def select_cell(self, cell_id: tuple[int, int]) -> None:
        """
        Called when a cell is clicked or after a drop. We can select empty or non-empty cells.

        Parameters
        ----------
        cell_id: tuple[int, int]
            The index (row, column) of the cell to select.
        """
        if self.selected_cell_id is not None:
            old_cell = self.cells.get(self.selected_cell_id)
            if old_cell:
                old_cell.set_selected(False)
        self.selected_cell_id = cell_id

        self.cells[cell_id].set_selected(True)
        self.show_metadata(cell_id)

    ### ----------- Drag & Drop Methods ----------- ###
    def on_mouse_down(self, event: tk.Event) -> None:
        """
        Identify if a ImageCell was clicked. If yes, select it and start drag if it's non-empty.
        """
        cell = event.widget
        if not isinstance(cell, ImageCell):
            parent = cell.master
            if isinstance(parent, ImageCell):
                cell = parent
            else:
                return

        if isinstance(cell, ImageCell):
            cell_id = cell.cell_id
            self.select_cell(cell_id)
            if not cell.is_empty():
                self.start_drag(cell_id)

    def start_drag(self, cell_id: tuple[int, int]) -> None:
        """
        Called when user presses on a non-empty cell.
        Create a ghost label after 0.2s to enable and visualize the drag operation.

        Parameters
        ----------
        cell_id: tuple[int, int]
            The index (row, column) of the cell to drag.
        """
        cell = self.cells[cell_id]
        if cell.is_empty():
            return

        self.dragging_cell_id = cell_id
        self.drag_enabled = False

        # Schedule the ghost creation after 0.2s : will enable drag
        self.ghost_delay_id = self.after(200, self.create_ghost_label, cell_id)

    def create_ghost_label(self, cell_id: tuple[int, int]) -> None:
        """
        Actually create the ghost label after the 0.2 s delay,
        placing it exactly where the mouse currently is, even if the window moved.

        Parameters
        ----------
        cell_id: tuple[int, int]
            The index (row, column) of the cell from which the ghost label should be created.
        """
        # If the user has released or canceled, do nothing
        if self.dragging_cell_id is None or self.dragging_cell_id != cell_id:
            return

        cell = self.cells[cell_id]
        text_for_ghost = cell.get_display_text()

        self.ghost_icon_current = self.icon_ok
        self.ghost_label = tk.Label(
            self,
            text=text_for_ghost,
            image=self.ghost_icon_current,
            compound="left",
            bg="#aaaaaa",
            fg="white",
            padx=4,
            pady=2
        )
        self.ghost_label.lift()

        # Get the current absolute pointer position in screen coords
        mx, my = self.winfo_pointerxy()  # global coords of pointer
        local_x = mx - self.winfo_rootx() + 110
        local_y = my - self.winfo_rooty() + 30

        self.ghost_label.place(x=local_x, y=local_y, anchor="center")
        self.drag_enabled = True

    def on_mouse_move(self, event: tk.Event) -> None:
        """
        Update the position of the ghost label & its associated icon that indicates to the user
        if a drop is possible.
        """
        if self.ghost_label is not None and self.dragging_cell_id is not None:
            local_x = event.x_root - self.winfo_rootx() + 110
            local_y = event.y_root - self.winfo_rooty() + 30
            self.ghost_label.place(x=local_x, y=local_y, anchor="center")

            # Use compute_drop_behavior to decide which icon to show.
            target, drop_behavior = self.compute_drop_behavior(event)

            new_icon = self.icon_ok if "not-allowed" not in drop_behavior else self.icon_nok
            if new_icon != self.ghost_icon_current:
                self.ghost_icon_current = new_icon
                self.ghost_label.config(image=new_icon)

    def compute_drop_behavior(self, event: tk.Event) -> tuple[(None, tuple[int, int]), str]:
        """
        Returns a tuple (target_cell, drop_behavior) where drop_behavior is one of:
          - "non-empty-swap": dropping a non-empty cell onto another non-empty cell.
          - "empty-swap": dropping onto an empty cell that is allowed.
          - "swap-outside-bottom": dropping outside the grid at the bottom.
          - "swap-outside-right": dropping outside the grid to the right.
          - "same-cell": dropping on the original cell.
          - "not-allowed": drop not permitted.

        After the base behavior is determined, an independent check is performed for
        row/col removal: if the dragged cell is moved to a different row (or col) and its
        original row (or col) has no other non-empty cells, "-with-row-removal" (or
        "-with-col-removal") is appended.

        For outside drops:
          - Bottom drop: target is (max_row+1, closest_col), where closest_col is based on the drop's x‑position.
          - Right drop: target is (0, max_col+1).
        """
        mouse_x, mouse_y = event.x_root, event.y_root
        grid_x = self.frame_grid.winfo_rootx()
        grid_y = self.frame_grid.winfo_rooty()
        grid_w = self.frame_grid.winfo_width()
        grid_h = self.frame_grid.winfo_height()

        # Disallow drops if the pointer is above or to the left of the grid.
        if mouse_x < grid_x or mouse_y < grid_y:
            return None, "not-allowed (outside top/left grid)"

        inside_x = (mouse_x <= grid_x + grid_w)
        inside_y = (mouse_y <= grid_y + grid_h)

        if inside_x and inside_y:
            # Pointer is inside the grid.
            drop_widget = self.frame_grid.winfo_containing(mouse_x, mouse_y)
            if drop_widget:
                # Try to get the ImageCell
                if not isinstance(drop_widget, ImageCell):
                    parent = drop_widget.master
                    if isinstance(parent, ImageCell):
                        drop_widget = parent
                    else:
                        return None, "not-allowed (not a cell)"
                target_id = drop_widget.cell_id

                # If dropping on the same cell.
                if target_id == self.dragging_cell_id:
                    return target_id, "same-cell"

                # If target cell is non-empty, allow a swap.
                if drop_widget.filepath is not None:
                    return target_id, "non-empty-swap"
                else:
                    # For empty cells, require at least one adjacent non-empty neighbor that is not the dragged cell
                    r, c = target_id
                    has_neighbor = any((nr, nc) in self.cells
                                       and (not self.cells[(nr, nc)].is_empty())
                                       and (nr, nc) != self.dragging_cell_id
                                       for nr, nc in [(r - 1, c), (r + 1, c), (r, c - 1), (r, c + 1)])
                    if not has_neighbor:
                        return target_id, "not-allowed (empty-no-neighbor)"
                    base_behavior = "empty-swap"
            else:
                return None, "not-allowed (no parent)"
        else:
            # Pointer is outside the grid.
            delta_right = mouse_x - (grid_x + grid_w)
            delta_bottom = mouse_y - (grid_y + grid_h)

            # Is bottom or right ? Else not allowed.
            if (delta_right > 0) ^ (delta_bottom > 0):
                drop_type = "swap-outside-right" if delta_right > 0 else "swap-outside-bottom"
            else:
                return None, "not-allowed (outside top/left)"

            if drop_type == "swap-outside-bottom":
                # Target is (max_row+1, closest_col).
                max_row = self.get_row_count() - 1

                if self.is_row_empty(max_row):
                    return None, "not-allowed (empty-last-row)"

                # Put it at the closest column (i.e. Time) & next row
                sample_cell = next(iter(self.cells.values()))
                cell_w = sample_cell.winfo_width() or 100
                rel_x = mouse_x - grid_x
                closest_col = round(rel_x / cell_w)
                num_cols = self.get_col_count()
                closest_col = max(0, min(closest_col, num_cols - 1))

                target_id = (max_row + 1, closest_col)
                base_behavior = "swap-outside-bottom"
            else:
                # Target is (0, max_col+1).
                max_col = self.get_col_count() - 1

                if self.is_col_empty(max_col):
                    return None, "not-allowed (empty-last-col)"

                # Put it at the row 0 (i.e. View 0) & next column
                target_id = (0, max_col + 1)
                base_behavior = "swap-outside-right"

        source_r, source_c = self.dragging_cell_id

        # Check if the dragged cell is leaving its original row
        if target_id[0] != source_r:
            nonempty_cols = self.get_row(source_r, ignore_empty=True)
            # Remove the row if the dragged cell is the only cell of the row
            if len(nonempty_cols) == 1:
                base_behavior += "-with-row-removal"

        # Check if the dragged cell is leaving its original column.
        if target_id[1] != source_c:
            nonempty_rows = self.get_column(source_c, ignore_empty=True)
            # Remove the column if the dragged cell is the only cell of the column
            if len(nonempty_rows) == 1:
                base_behavior += "-with-col-removal"

        return target_id, base_behavior

    def on_mouse_up(self, event: tk.Event):
        """
        Validate (or not) the current drop according to the drop behavior.
        Apply specific behavior (row removal, column removal, etc.) if needed.
        """
        # Cancel any pending ghost creation.
        if hasattr(self, "ghost_delay_id") and self.ghost_delay_id is not None:
            self.after_cancel(self.ghost_delay_id)
            self.ghost_delay_id = None

        if self.dragging_cell_id is None:
            return

        if self.ghost_label is not None:
            self.ghost_label.destroy()
            self.ghost_label = None

        if not self.drag_enabled:
            return

        target_id, drop_behavior = self.compute_drop_behavior(event)
        logger.info(f"Drag&Drop: {drop_behavior}: {self.dragging_cell_id} to {target_id}")

        # Proceed if the drop was held long enough & the drop behavior is valid
        if "not-allowed" not in drop_behavior and "same-cell" not in drop_behavior:
            if "swap-outside-bottom" in drop_behavior:
                self.add_row()
            elif "swap-outside-right" in drop_behavior:
                self.add_column()

            self.swap_cells(self.dragging_cell_id, target_id)
            self.select_cell(target_id)

            source_r, source_c = self.dragging_cell_id
            if "-with-row-removal" in drop_behavior and self.is_row_empty(source_r):
                self.remove_row(source_r)
            if "-with-col-removal" in drop_behavior and self.is_col_empty(source_c):
                self.remove_col(source_c)

        self.dragging_cell_id = None
        self.drag_enabled = False

    def swap_cells(self, cell_id1: tuple[int, int], cell_id2: tuple[int, int]):
        """
        Apply a swap of two cells.

        Parameters
        ----------
        cell_id1: tuple[int, int]
            The index (row, column) of the first cell.
        cell_id2: tuple[int, int]
            The index (row, column) of the second cell.
        """
        c1 = self.cells[cell_id1]
        c2 = self.cells[cell_id2]
        c1.swap_with(c2)

    ### ----------- Window Management Methods ----------- ###
    def run(self) -> None:
        """
        Run the GUI.
        """
        self.mainloop()

    def update_paned_sizes(self, event: tk.Event) -> None:
        """
        Resize the left and right panel if the window is resized. Also resize the thumbnail is needed.
        """
        total_width = event.width
        left_width = int(total_width * 0.6)
        self.paned.paneconfig(self.left_panel, width=left_width)
        self.paned.paneconfig(self.right_panel, width=total_width - left_width)

        # Check if a non-empty cell is selected
        if self.selected_cell_id is None:
            return
        cell = self.cells[self.selected_cell_id]
        if cell.is_empty() or cell.thumbnail is None:
            return

        # If needed, resize the thumbnail
        avail_w = self.label_preview.winfo_width()
        avail_h = self.label_preview.winfo_height()
        if avail_w <= 1 or avail_h <= 1:
            return

        resized = ImageUtils.resize_image_to_fit(cell.thumbnail, (avail_w, avail_h))
        self.tk_image = ImageTk.PhotoImage(resized)
        self.label_preview.config(image=self.tk_image, bg="#222")

    ### ----------- Export Management Methods ----------- ###
    def export_dataset(self) -> None:
        """
        Export the dataset. All the logic is moved to ExportManager to ensure modularity.
        It is required that the output dataset will have the following organization:

              {dataset_name}
                    |---- raw
                    |      |---- image1.tif, image2.tif, etc...
                    |      |----processing.json
                    |---- {dataset_name}.json

        where {dataset_name}.json is the main JSON containing all the dataset metadata and processing.json is a JSON
        that contains specific information about the raw dataset (ex: original filepath of the images).
        """
        export_manager = ExportManager(self.cells, self.operation_mode.get())
        export_manager.export_dataset(self)


class ImageCell(tk.Frame):
    """
    A cell widget for displaying image metadata and a thumbnail preview.

    Each cell is identified by a (row, col) tuple and can store an image file path,
    associated metadata, and a pre-generated thumbnail.
    """
    # Constants for default colors.
    DEFAULT_HEADER_FG = "#333"
    DEFAULT_HEADER_BG = "#ddd"
    DEFAULT_CONTENT_FG = "#2F4F4F"
    DEFAULT_CONTENT_BG = "#eee"
    NONEMPTY_CONTENT_BG = "#ccc"

    def __init__(self, parent: tk.Frame, cell_id: tuple[int, int], width: int = 200, height: int = 120, **kwargs):
        """
        Initialize an ImageCell.

        Parameters
        ----------
        parent: tk.Frame
            The parent widget.
        cell_id: tuple
            A (row, col) identifier.
        width: int
            The cell width.
        height: int
            The cell height.
        """
        super().__init__(parent, width=width, height=height, bd=1, relief="groove", **kwargs)
        self.cell_id = cell_id
        self.grid_propagate(False)

        # Internal storage for image data.
        self.filepath = None
        self.metadata = {}
        self.thumbnail = None

        # Create header label.
        self.header_label = tk.Label(self, text="",
                                     font=("Helvetica", 12, "bold"),
                                     bg=self.DEFAULT_HEADER_BG,
                                     fg=self.DEFAULT_HEADER_FG,
                                     anchor="center")
        self.header_label.place(x=2, y=2, width=width - 4, height=20)

        # Create content label.
        self.content_label = tk.Label(self, text="",
                                      font=("Helvetica", 14, "bold"),
                                      bg=self.DEFAULT_CONTENT_BG,
                                      fg=self.DEFAULT_CONTENT_FG,
                                      anchor="center",
                                      wraplength=width - 8,  # Leave small margin on sides
                                      justify="center")
        self.content_label.place(x=2, y=24, width=width - 4, height=height - 30)

    def is_empty(self) -> bool:
        """
        Return True if the cell contains no image data.
        """
        return self.filepath is None

    def get_data(self) -> tuple:
        """
        Return a tuple (filepath, metadata, thumbnail).
        """
        return self.filepath, self.metadata, self.thumbnail

    def set_data(self, filepath: str = None, metadata: dict = None,
                 preview_array: list = None, thumb_size: tuple = (500, 300)) -> None:
        """
        Set or clear the cell's data.

        If a filepath is provided, a composite thumbnail is generated from the preview_array.
        Otherwise, the cell is cleared.

        Parameters
        ----------
        filepath: str
            Path to the image file.
        metadata: dict
            Associated metadata.
        preview_array: list
            Raw image slices for thumbnail generation.
        thumb_size: tuple
            Desired size for the composite thumbnail.
        """
        if filepath is None:
            self.filepath = None
            self.metadata = {}
            self.thumbnail = None
        else:
            self.filepath = filepath
            self.metadata = metadata if metadata is not None else {}
            try:
                self.thumbnail = ImagePreviewGenerator.create_composite_thumbnail(
                    preview_array, thumb_size=thumb_size)
            except Exception as e:
                logger.warning(f"Error generating thumbnail for {filepath}: {e}")
                self.thumbnail = None
        self.update_display()

    def set_cell_id(self, cell_id) -> None:
        """
        Update the cell's identifier and refresh its display.
        """
        self.cell_id = cell_id
        self.update_display()

    def add_image_data(self, filepath, metadata, preview_array, thumb_size=(500, 300)) -> None:
        """
        Public method to add image data (delegates to set_data).

        Parameters
        ----------
        filepath: str
            Path to the image file.
        metadata: dict
            Associated metadata.
        preview_array: list
            Raw image slices.
        thumb_size: tuple
            Desired thumbnail size.
        """
        self.set_data(filepath, metadata, preview_array, thumb_size)

    def clear_image_data(self) -> None:
        """
        Clear the cell's image data.
        """
        self.set_data()

    def update_display(self) -> None:
        """
        Refresh the header and content labels based on the current cell data.
        """
        row, col = self.cell_id
        if self.is_empty():
            self.header_label.config(text="")
            self.content_label.config(text="")
        else:
            header = f"Time: {col} | View: {row}"
            self.header_label.config(text=header)
            basename = os.path.basename(self.filepath)
            self.content_label.config(text=basename)

    def swap_with(self, other: ImageCell) -> None:
        """
        Swap the image data with another cell and update both displays.

        Parameters
        ----------
        other: ImageCell
            The cell to swap data with.
        """
        self.filepath, other.filepath = other.filepath, self.filepath
        self.metadata, other.metadata = other.metadata, self.metadata
        self.thumbnail, other.thumbnail = other.thumbnail, self.thumbnail
        self.update_display()
        other.update_display()

    def get_display_text(self) -> str:
        """
        Return a string combining header and content text for display.
        """
        return self.header_label.cget("text") + "\n" + self.content_label.cget("text")

    def set_selected(self, selected: bool) -> None:
        """
        Update the cell's appearance based on whether it is selected.

        Parameters
        ----------
        selected: bool
            True if the cell is selected, False otherwise.
        """
        if selected:
            self.header_label.config(bg="lightblue")
            self.content_label.config(bg="lightblue")
        else:
            self.header_label.config(bg=self.DEFAULT_HEADER_BG)
            self.content_label.config(bg=self.DEFAULT_CONTENT_BG)


#########################################################################################
### Some useful methods to import images & process them (valid format, metadata, preview)
#########################################################################################
class ImageUtils:
    VALID_EXTENSIONS = (".inr", ".inr.gz", ".tif", ".nd2", ".lsm", ".czi")

    @staticmethod
    def is_valid_image(filepath: str) -> bool:
        """
        Check if the file extension is one of the valid image formats.
        """
        lower_fp = filepath.lower()
        return any(lower_fp.endswith(ext) for ext in ImageUtils.VALID_EXTENSIONS)

    @staticmethod
    def get_valid_image_formats() -> list[str]:
        """
        Return a list of unique, latest (innermost) file extensions from VALID_EXTENSIONS.
        For compressed formats (e.g., '.inr.gz', '.tar.gz'), only the latest extension (e.g., '.gz') is included.
        """
        latest_extensions = set()

        for ext in ImageUtils.VALID_EXTENSIONS:
            latest_ext = ImageUtils.extract_latest_extension(ext)
            latest_extensions.add(latest_ext)

        return sorted(latest_extensions)

    @staticmethod
    def extract_latest_extension(extension: str) -> str:
        """
        Extract the latest (innermost) extension from a possibly multipart extension.
        """
        root, ext = os.path.splitext(extension.lower())
        if ext:
            return ext
        else:
            return root

    @staticmethod
    def get_extension(filename: str) -> str:
        """
        Returns the file extension, taking into account special cases like '.inr.gz'.
        """
        base, ext = os.path.splitext(filename)
        if ext.lower() == ".gz":
            # The real extension might be ".inr.gz" or something similar.
            base2, ext2 = os.path.splitext(base)
            ext = ext2 + ".gz"
        return ext

    @staticmethod
    def load_image_metadata(img_path: str, n_slices_mono: int = 8, n_slices_multi: int = 4) -> tuple:
        """
        Reads the image from file_path and returns a tuple:
          (preview_array, metadata)
        The preview_array is organized based on:
             • 2D/MonoChannel: [slice]
             • 2D/MultiChannel: [[slice_per_channel]]
             • 3D/MonoChannel: [n_slices_mono slices]
             • 3D/MultiChannel: [[n_slices_multi slices per channel]]
        Metadata is a dictionary with the following keys:
            "Shape", "Voxel size", "Extent", "Type", "Axes order" and optionally "Channels"
        """
        meta = {}
        preview_array = None
        try:
            img = imread(img_path)
            meta["Shape"] = img.get_shape()
            meta["Voxel size"] = np.around(img.get_voxelsize(), 3)
            meta["Extent"] = np.around(img.get_extent(), 3)
            meta["Type"] = img.dtype
            meta["Axes order"] = img.axes_order

            def process_slice(slice_obj):
                """Apply global contrast stretch and conversion to uint8, returning a copy of the array."""
                processed = global_contrast_stretch(slice_obj, pc_min=2, pc_max=99)
                processed = to_uint8(processed)
                return processed.get_array().copy()

            if img.is3D():
                total_slices = img.get_shape('z')
                if isinstance(img, MultiChannelImage):
                    logger.info(f"Load 3D MultiChannel: {img_path}")
                    ch_names = img.get_channel_names()
                    meta["Channels"] = ch_names
                    indices = np.linspace(0, total_slices - 1, n_slices_multi, dtype=int)
                    # For each channel, process the slices from the selected indices.
                    channel_slices = {ch: [] for ch in ch_names}
                    for idx in indices:
                        slice_2d = img.get_slice(idx, axis='z')
                        for ch in ch_names:
                            channel_slices[ch].append(process_slice(slice_2d.get_channel(ch)))
                    # Return a list where each element is the list of processed slices for one channel.
                    preview_array = [channel_slices[ch] for ch in ch_names]
                else:
                    logger.info(f"Load 3D MonoChannel: {img_path}")
                    indices = np.linspace(0, total_slices - 1, n_slices_mono, dtype=int)
                    preview_array = [process_slice(img.get_slice(idx, axis='z')) for idx in indices]
            else:
                if isinstance(img, MultiChannelImage):
                    logger.info(f"Load 2D MultiChannel: {img_path}")
                    ch_names = img.get_channel_names()
                    meta["Channels"] = ch_names
                    # For 2D multichannel, process each channel and wrap the result in a list.
                    preview_array = [[process_slice(img.get_channel(ch))] for ch in ch_names]
                else:
                    logger.info(f"Load 2D MonoChannel: {img_path}")
                    preview_array = [process_slice(img)]

        except Exception as e:
            logger.error(f"Error with {img_path}: {e}")
            meta["error"] = str(e)
        return preview_array, meta

    @staticmethod
    def resize_image_to_fit(img: Image.Image, container_size: tuple[int, int]) -> Image.Image:
        """
        Resize 'img' to fit within 'container_size' while preserving its aspect ratio.
        The resulting image will be as large as possible without exceeding the container's dimensions.
        """
        container_w, container_h = container_size
        img_w, img_h = img.size
        scale = min(container_w / img_w, container_h / img_h)
        new_w = int(img_w * scale)
        new_h = int(img_h * scale)
        return img.resize((new_w, new_h), Image.Resampling.LANCZOS)


class ImagePreviewGenerator:
    @staticmethod
    def process_array(arr: np.ndarray, thumb_size: tuple[int, int]) -> Image.Image:
        """
        Process a single image slice:
          - Convert grayscale to RGB if necessary.
          - Clamp to 3 channels if more.
          - Convert to uint8 if needed.
          - Return a thumbnail of the specified size.

        Parameters
        ----------
        arr: np.ndarray
            The input image slice.
        thumb_size: tuple
            Desired thumbnail size (width, height).

        Returns
        -------
        Image.Image
            The processed thumbnail.
        """
        # Convert grayscale (2D) to RGB by duplicating the channel 3 times
        if arr.ndim == 2:
            arr = np.stack([arr, arr, arr], axis=-1)
        # Limit to 3 channels (RGB) if more channels exist
        elif arr.ndim == 3 and arr.shape[2] > 3:
            arr = arr[..., :3]

        # Convert array to uint8 format (0-255 range)
        if arr.dtype != np.uint8:
            arr_min, arr_max = arr.min(), arr.max()
            # Normalize only if there's a range to prevent division by zero
            if arr_max - arr_min > 0:
                arr = (255 * (arr - arr_min) / (arr_max - arr_min)).astype(np.uint8)
            else:
                # If array is constant, set all values to 0
                arr = np.zeros_like(arr, dtype=np.uint8)

        # Convert numpy array to PIL Image and resize to thumbnail size
        pil_img = Image.fromarray(arr)
        pil_img.thumbnail(thumb_size)
        return pil_img

    @staticmethod
    def create_composite_thumbnail(preview_array: list, thumb_size: tuple = (500, 300)) -> Image.Image:
        """
        Create a composite thumbnail from a preview array.

        For mono-channel images, preview_array is a flat list of slices.
        For multichannel images, preview_array is a list of lists, each sublist containing slices for one channel.

        The method arranges the slices into a grid and returns a composite image.

        Parameters
        ----------
        preview_array: list
            The list of image slices (or list of lists for multi-channel).
        thumb_size: tuple
            The composite thumbnail size (width, height).

        Returns
        -------
        Image.Image
            The composite thumbnail.
        """
        composite_width, composite_height = thumb_size

        # Determine grid dimensions.
        # Handle both multi-channel (list of lists) and single-channel (flat list) cases
        if isinstance(preview_array[0], list):
            # Multi-channel case: each channel gets its own row
            num_channels = len(preview_array)
            num_slices_per_channel = len(preview_array[0])
            grid_cols = 1 if num_slices_per_channel == 1 else 4
            grid_rows = num_channels
        else:
            # Single-channel case: arrange slices in a grid
            num_images = len(preview_array)
            grid_cols = 1 if num_images == 1 else 4
            grid_rows = int(np.ceil(num_images / grid_cols))

        # Calculate individual thumbnail dimensions
        thumb_w = composite_width // grid_cols
        thumb_h = composite_height // grid_rows

        # Store processed thumbnails in a 2D grid
        grid_thumbs: list[list[Image.Image]] = []

        if isinstance(preview_array[0], list):
            # Process multi-channel images
            for channel_slices in preview_array:
                row_thumbs = []
                for idx in range(grid_cols):
                    if idx < len(channel_slices):
                        # Process actual slice data
                        thumb = ImagePreviewGenerator.process_array(channel_slices[idx], (thumb_w, thumb_h))
                    else:
                        # Fill empty slots with black thumbnails
                        thumb = Image.new('RGB', (thumb_w, thumb_h), color='black')
                    row_thumbs.append(thumb)
                grid_thumbs.append(row_thumbs)
        else:
            # Process single-channel images
            idx = 0
            for r in range(grid_rows):
                row_thumbs = []
                for c in range(grid_cols):
                    if idx < len(preview_array):
                        # Process actual slice data
                        thumb = ImagePreviewGenerator.process_array(preview_array[idx], (thumb_w, thumb_h))
                        idx += 1
                    else:
                        # Fill empty slots with black thumbnails
                        thumb = Image.new('RGB', (thumb_w, thumb_h), color='black')
                    row_thumbs.append(thumb)
                grid_thumbs.append(row_thumbs)

        # Create the final composite image
        composite_img = Image.new('RGB', (composite_width, composite_height), color='black')

        # Paste individual thumbnails into the composite image
        for r, row in enumerate(grid_thumbs):
            for c, thumb in enumerate(row):
                composite_img.paste(thumb, (c * thumb_w, r * thumb_h))

        return composite_img


#########################################################################################
### All the logic related to the export of the dataset
#########################################################################################
class ExportManager:
    """
    ExportManager centralizes all logic related to exporting a dataset.

    It is responsible for:
      - Validating image consistency and grid structure.
      - Building the export JSON structures in a modular fashion.
      - Invoking the ExportWindow to obtain export metadata from the user.
      - Writing images and JSON files to disk with the required folder organization.

    The expected folder structure after export is:

         {dataset_name}
              ├── raw
              │     ├── image files (renamed)
              │     └── processing.json
              └── {dataset_name}.json
    """

    def __init__(self, cells: dict[tuple[int, int], ImageCell], operation_mode: str = "copy") -> None:
        """
        Initialize the ExportManager with the grid of image cells.

        Parameters
        ----------
        cells : dict
            A dictionary mapping (row, col) -> ImageCell (for non-empty cells).
        operation_mode : str, optional
            Operation mode for file handling: 'copy' or 'move' (default is 'copy').
        """
        self.cells = cells
        self.rc_to_cell = {(r, c): cell for (r, c), cell in cells.items() if not cell.is_empty()}
        self.operation_mode = operation_mode
        self.channel_names = None  # will be fill if needed

    def validate_image_consistency(self) -> bool:
        """
        Check that all non-empty cells have consistent image dimensions,
        channel configuration (mono vs multi) and, if multi, matching channel names.

        Returns
        -------
        bool
            True if consistent.
        """
        # Get first cell as reference for comparison
        first_cell = next(iter(self.rc_to_cell.values()))
        # Get shape from reference cell metadata
        ref_shape = first_cell.metadata.get("Shape")
        if not ref_shape:
            raise ValueError("First cell missing shape info.")

        # Determine if reference is 2D or 3D based on shape dimensions
        ref_dim = "2D" if len(ref_shape) == 2 else "3D"
        # Check if reference image has multiple channels
        ref_multi = ("Channels" in first_cell.metadata)
        ref_channels = first_cell.metadata.get("Channels", None)

        # Store channel names if multichannel
        if ref_multi:
            self.channel_names = ref_channels

        # Validate each cell against reference
        for (r, c), cell in self.rc_to_cell.items():
            # Check shape exists and matches dimension
            shape = cell.metadata.get("Shape")
            if not shape:
                raise ValueError(f"Cell {(r, c)} missing shape info.")
            is_2d = len(shape) == 2
            if (ref_dim == "2D" and not is_2d) or (ref_dim == "3D" and is_2d):
                raise ValueError(f"Inconsistent dimension at Time {c} | View {r}. Expected {ref_dim}, got {shape}.")

            # Verify channel configuration consistency
            is_multi = ("Channels" in cell.metadata)
            if is_multi != ref_multi:
                raise ValueError(
                    f"Inconsistent channel configuration at Time {c} | View {r}. Do not import a mix of mono and multichannel images.")
            # For multichannel images, verify channel names match
            if ref_multi and (cell.metadata["Channels"] != ref_channels):
                raise ValueError(
                    f"Inconsistent channel names at Time {c} | View {r}. Expected {ref_channels}, got {cell.metadata['Channels']}.")

        return True

    def validate_grid_structure(self) -> bool:
        """
        Verify that the grid has continuous time points and, when multiple views exist,
        no holes in view indexing.

        Returns
        -------
        bool
            True if valid.
        """
        non_empty_cells = list(self.rc_to_cell.keys())
        if not non_empty_cells:
            raise ValueError("No non-empty cells found.")

        # Validate time points (columns)
        used_cols = set(c for (r, c) in non_empty_cells)
        max_col = max(used_cols)
        for col in range(max_col + 1):
            if col not in used_cols:
                raise ValueError(f"Missing Time {col} in the grid.")

        # Validate views per time point (rows)
        col_to_rows = defaultdict(list)
        for (r, c) in self.rc_to_cell.keys():
            col_to_rows[c].append(r)
        for c, rows in col_to_rows.items():
            rows = sorted(rows)
            if len(rows) > 1:
                for expected_row in range(rows[0], rows[-1] + 1):
                    if expected_row not in rows:
                        raise ValueError(f"At Time {c}, missing View {expected_row}.")
        return True

    @staticmethod
    def build_new_filename(pattern: str, time: int, ext: str, view: (int, None) = None) -> str:
        """
        Construct a new filename for an image based on the provided pattern.

        Parameters
        ----------
        pattern : str
            The base filename pattern.
        time : int
            The time point index.
        ext : str
            The file extension (.tif, .inr, .inr.gz, etc.).
        view : int, optional
            The view index. If None, the view is omitted in the new filename.

        Returns
        -------
        str
            The new filename, either in the form "{pattern}_t{time}{ext}" (if view is None) or
             "{pattern}_t{time}_view{view}{ext}".
        """

        if view is None:
            return f"{pattern}_t{time}{ext}"
        else:
            return f"{pattern}_t{time}_view{view}{ext}"

    def build_raw_dict(self, filename_pattern: str) -> tuple[dict, dict]:
        """
        Build the mapping for the raw images.

        The structure is a dictionary with keys as time points. If there is more than one view
        at a time point, the value is a sub-dictionary mapping view indices to filenames.
        Otherwise, the value is directly the filename.

        Also returns a mapping between the new filename and the original file path.

        Parameters
        ----------
        filename_pattern : str
            The base filename pattern.

        Returns
        -------
        (dict, dict)
            A tuple containing:
                - The raw images mapping.
                - A dictionary mapping new filenames to the original file paths.

        """
        raw_dict = {}  # Mapping: time point -> (filename or dict of view -> filename)
        fname_dict_mapping = {} # Mapping: new_filename -> original file path
        col_to_rows = defaultdict(list)  # Map column (time point) to list of rows (views)

        # Group rows (views) by column (time point)
        for (r, c), cell in self.rc_to_cell.items():
            col_to_rows[c].append(r)

        max_col = max(col_to_rows.keys())

        # Build dictionary structure for each time point
        for c in range(max_col + 1):
            rows = sorted(col_to_rows.get(c, []))
            sub_dict = {}
            for r in rows:
                cell = self.rc_to_cell[(r, c)]
                # If there is only one view, pass view=None; otherwise pass the view index.
                ext = ImageUtils.get_extension(cell.filepath)
                view_param = r if len(rows) > 1 else None
                new_fname = self.build_new_filename(filename_pattern, c, ext, view=view_param)

                sub_dict[str(r)] = new_fname
                fname_dict_mapping[new_fname] = cell.filepath

            # If there's a single view, store the filename directly; otherwise keep the dict of views.
            raw_dict[str(c)] = sub_dict[next(iter(sub_dict))] if len(rows) == 1 else sub_dict

        return raw_dict, fname_dict_mapping

    @staticmethod
    def build_processing_json(raw_dict: dict, fname_dict_mapping: dict) -> dict:
        """
        Build the processing mapping that includes both the new filename and the original file path.

        This function reuses the output from build_raw_dict. For time points with a single view,
        it creates an entry with keys "filename" and "original_path". For time points with multiple views,
        it creates a sub-dictionary mapping view indices to dictionaries containing "filename" and "original_path".

        Parameters
        ----------
        raw_dict : dict
            The raw images mapping (time point -> filename or view mapping) produced by build_raw_dict.
        fname_dict_mapping : dict
            A dictionary mapping new filenames to their original file paths.

        Returns
        -------
        dict
            The processing mapping including both new filenames and original file paths.
        """
        processing_json = {}

        for time_key, value in raw_dict.items():
            if isinstance(value, dict):
                # Multiple views: value is a dictionary mapping view indices to new filenames.
                processing_json[time_key] = {}
                for view_key, filename in value.items():
                    processing_json[time_key][view_key] = {
                        "filename": filename,
                        "original_path": fname_dict_mapping.get(filename)
                    }
            else:
                # Single view: value is the new filename.
                processing_json[time_key] = {
                    "filename": value,
                    "original_path": fname_dict_mapping.get(value)
                }

        return processing_json

    def build_dataset_json(self, raw_dict: dict, user_info: dict) -> dict:
        """
        Build the main dataset JSON combining the object, time, raw image mapping,
        and optionally the channel names.

        Parameters
        ----------
        raw_dict : dict
            The raw images mapping.
        user_info : dict
            The export metadata provided by the user.

        Returns
        -------
        dict
            The complete dataset JSON.
        """
        dataset_object = {
            "series_name": user_info["dataset_name"],
            "species": user_info["species"],
            "observation_sample": user_info["observation_sample"],
            "NCBI id": user_info["ncbi_id"]
        }
        dataset_time = {
            "points": user_info["time_points"],
            "unit": user_info["time_unit"]
        }
        dataset_json = {
            "object": dataset_object,
            "time": dataset_time,
            "raw": raw_dict
        }
        if self.channel_names:
            dataset_json["channels"] = self.channel_names
        return dataset_json

    def write_dataset(self, dataset_name: str, dataset_json: dict, processing_json: dict) -> bool:
        """
        Write the dataset to disk with the following organization:

            {dataset_name}
                 ├── raw
                 │     ├── image files
                 │     └── processing.json
                 └── {dataset_name}.json

        Parameters
        ----------
        dataset_name : str
            The name of the dataset.
        dataset_json : dict
            The main dataset JSON.
        processing_json : dict
            The processing metadata JSON.

        Returns
        -------
        bool
            True if writing was successful.

        Raises
        ------
        ValueError
            If output directory is not selected or files cannot be written.
        """
        # Ask for output directory.
        root_outdir = filedialog.askdirectory(title="Select output folder for dataset")
        if not root_outdir:
            raise ValueError("No output folder selected.")

        dataset_folder_path = os.path.join(root_outdir, dataset_name)
        if os.path.exists(dataset_folder_path):
            raise ValueError(f"Folder '{dataset_name}' already exists in\n'{root_outdir}'.\nExport cancelled.")

        try:
            os.makedirs(dataset_folder_path)
            raw_dir = os.path.join(dataset_folder_path, "raw")
            os.makedirs(raw_dir)
        except Exception as e:
            raise ValueError(f"Could not create dataset folder:\n{e}")

        # Write images.
        for entry in processing_json.values():
            # Flatten the entry: if it's a single-view (contains "filename"), wrap it in a list;
            # otherwise, assume it's a dict mapping view keys to info and use its values.
            infos = [entry] if "filename" in entry else list(entry.values())
            for info in infos:
                old_fp = info["original_path"]
                new_fname = info["filename"]
                new_path = os.path.join(raw_dir, new_fname)
                try:
                    if self.operation_mode == "copy":
                        shutil.copy2(old_fp, new_path)
                    else:
                        shutil.move(old_fp, new_path)
                except Exception as e:
                    raise ValueError(f"Failed to {self.operation_mode} file {old_fp} to {new_path}:\n{e}")

        # Write JSON files.
        try:
            with open(os.path.join(raw_dir, "processing.json"), "w", encoding="utf-8") as f:
                json.dump(processing_json, f, indent=2)
            dataset_json_filename = f"{dataset_name}.json"
            with open(os.path.join(dataset_folder_path, dataset_json_filename), "w", encoding="utf-8") as f:
                json.dump(dataset_json, f, indent=2)
        except Exception as e:
            raise ValueError(f"Failed to write JSON files:\n{e}")

        return True

    def export_dataset(self, parent: tk.Tk) -> None:
        """
        Master method to export the dataset.

        It performs the following steps:
          1. Validates that there are non-empty image cells.
          2. Checks image consistency and grid structure.
          3. Invokes an ExportWindow dialog to obtain export metadata.
          4. Builds the JSON structures for the dataset and processing.
          5. Writes images and JSON files to disk.
          6. Displays success or error messages.

        Parameters
        ----------
        parent : tk.Tk
            The parent Tk window (used to position dialogs).
        """
        try:
            if not self.rc_to_cell:
                raise ValueError("No images to export.")

            # Validate image consistency and grid structure.
            self.validate_image_consistency()
            self.validate_grid_structure()

            # Determine required time points (columns).
            required_times = max(c for (r, c) in self.rc_to_cell.keys()) + 1

            # Obtain export metadata from the user.
            export_win = ExportWindow()
            user_info = export_win.open_info_dialog(parent, required_times)
            if not user_info:
                return
            # Compute the filename pattern from the dialog.
            filename_pattern = export_win.build_filename_pattern()

            # Build export JSON structures.
            raw_dict, fname_mapping_dict = self.build_raw_dict(filename_pattern)

            processing_json = self.build_processing_json(raw_dict, fname_mapping_dict)
            dataset_json = self.build_dataset_json(raw_dict, user_info)

            # Write the dataset to disk.
            dataset_name = user_info["dataset_name"]
            self.write_dataset(dataset_name, dataset_json, processing_json)

            messagebox.showinfo("Export Successful", f"Dataset created in:\n{dataset_name}")
        except Exception as e:
            messagebox.showwarning("Export Error", str(e))


class ExportWindow:
    """
    ExportWindow is responsible for gathering export metadata from the user via a GUI dialog.

    It provides methods to create and display an information dialog where the user enters
    dataset parameters (such as dataset name, species, observation sample, NCBI id, time unit, and time points)
    and shows a filename preview. This class does not perform any export operations.
    """

    TIME_UNIT_MAP: dict[str, str] = {
        "day": "d",
        "hours": "h",
        "minutes": "min",
        "seconds": "s"
    }

    def __init__(self) -> None:
        """Initialize the ExportWindow."""
        self.dialog: tk.Toplevel | None = None
        self.result: dict | None = None

        self.dataset_name_var: tk.StringVar | None = None
        self.species_var: tk.StringVar | None = None
        self.obs_sample_var: tk.StringVar | None = None
        self.ncbi_var: tk.StringVar | None = None
        self.time_unit_var: tk.StringVar | None = None
        self.time_entries: list[tk.StringVar] = []
        self.filename_pattern_var: tk.StringVar | None = None

        self.filename_preview_label: tk.Label | None = None

    def create_labeled_entry(self, parent: tk.Toplevel, label_text: str, row: int,
                             bind_preview_update: bool = False) -> tk.StringVar:
        """
        Create a labeled entry widget.

        Parameters
        ----------
        parent : tk.Toplevel
            The parent dialog.
        label_text : str
            The text for the label.
        row : int
            The grid row index.
        bind_preview_update : bool, optional
            If True, bind key releases to update the filename preview.

        Returns
        -------
        tk.StringVar
            The variable associated with the entry widget.
        """
        # Create and position the label with right alignment
        tk.Label(parent, text=label_text).grid(row=row, column=0, sticky="e", padx=5, pady=5)
        # Create StringVar to store and track entry content
        var = tk.StringVar()
        # Create entry widget with fixed width and bind it to StringVar
        entry = tk.Entry(parent, textvariable=var, width=30)
        entry.grid(row=row, column=1, padx=5, pady=5, sticky="w")  # Left-align entry
        # Optionally bind key release events to update filename preview
        if bind_preview_update:
            entry.bind("<KeyRelease>", self.update_filename_preview)
        return var

    @staticmethod
    def sanitize_dataset_name(name: str) -> str:
        """
        Sanitize the dataset name: trim, remove accents, replace spaces with underscores,
        and remove non-alphanumeric characters (except underscores).

        Parameters
        ----------
        name : str
            The original dataset name.

        Returns
        -------
        str
            The sanitized dataset name.
        """
        # Return empty string for None or empty input
        if not name:
            return ""
        # Remove leading/trailing whitespace
        text = name.strip()
        # Convert accented characters to their ASCII equivalents
        # e.g., 'é' -> 'e', 'ñ' -> 'n'
        text = unicodedata.normalize('NFD', text).encode('ascii', 'ignore').decode('ascii')
        # Replace one or more whitespace characters with single underscore
        text = re.sub(r'\s+', '_', text)
        # Remove all characters except letters, numbers and underscores
        return re.sub(r'[^A-Za-z0-9_]', '', text)

    @staticmethod
    def species_code(species_str: str) -> str:
        """
        Generate a species code from the species name by taking the uppercase first letter of each word.

        Parameters
        ----------
        species_str : str
            The species name.

        Returns
        -------
        str
            The generated species code.
        """
        return "".join(word[0].upper() for word in species_str.strip().split() if word)

    def create_filename_pattern_entry(self, parent: tk.Toplevel, row: int) -> tk.StringVar:
        """
        Create a labeled entry to allow users to specify a filename pattern.

        Parameters
        ----------
        parent : tk.Toplevel
            The parent dialog.
        row : int
            The grid row index.

        Returns
        -------
        tk.StringVar
            The variable associated with the pattern entry widget.
        """
        # Create and position the label for the filename pattern field
        tk.Label(parent, text="Filename Pattern:").grid(row=row, column=0, sticky="e", padx=5, pady=5)
        # Create StringVar with default pattern using placeholders
        var = tk.StringVar(value=DEFAULT_FNAME_PATTERN)
        # Create entry widget bound to the StringVar
        entry = tk.Entry(parent, textvariable=var, width=30)
        entry.grid(row=row, column=1, padx=5, pady=5, sticky="w")
        # Bind key release event to update preview in real-time
        entry.bind("<KeyRelease>", self.update_filename_preview)
        return var

    def build_filename_pattern(self) -> str:
        """
        Build a filename pattern based on current export parameters.

        The pattern is in the form "{species_code}_{observation_sample}_{dataset_name}".

        Returns
        -------
        str
            The filename pattern.
        """
        ds_name = (self.dataset_name_var.get() or "???").strip().replace(" ", "_")
        sp_code = self.species_code(self.species_var.get() or "???")
        obs = (self.obs_sample_var.get() or "???").strip().replace(" ", "_")

        # Get the user-defined pattern and replace placeholders
        pattern = self.filename_pattern_var.get() or DEFAULT_FNAME_PATTERN
        pattern = pattern.replace("%dataset", ds_name)
        pattern = pattern.replace("%species", sp_code)
        pattern = pattern.replace("%sample", obs)

        return pattern

    def update_filename_preview(self, event: tk.Event) -> None:
        """
        Update the filename preview label based on current parameters.
        """
        pattern = self.build_filename_pattern()
        sample = f"{pattern}_t0_view0.tif"
        if self.filename_preview_label:
            self.filename_preview_label.config(text=sample)

    def open_info_dialog(self, parent: tk.Tk, required_times_count: int) -> dict | None:
        """
        Open a modal dialog for export information.

        Parameters
        ----------
        parent : tk.Tk
            The parent application window.
        required_times_count : int
            The exact number of time points required.

        Returns
        -------
        dict or None
            The export parameters if OK was pressed; otherwise, None.
        """
        # Create modal dialog window
        self.dialog = tk.Toplevel(parent)
        self.dialog.title("Export Dataset Info")
        self.dialog.geometry("460x470")
        self.dialog.resizable(False, False)
        self.dialog.grab_set()  # Make dialog modal

        # Configure grid columns with equal weight
        self.dialog.grid_columnconfigure(0, weight=1)
        self.dialog.grid_columnconfigure(1, weight=1)

        row_y = 0  # Track current row position

        # Create input fields with labels
        self.dataset_name_var = self.create_labeled_entry(self.dialog, "Dataset Name:", row_y,
                                                          bind_preview_update=True)
        row_y += 1
        self.species_var = self.create_labeled_entry(self.dialog, "Species:", row_y,
                                                     bind_preview_update=True)
        row_y += 1
        self.obs_sample_var = self.create_labeled_entry(self.dialog, "Observation Sample:", row_y,
                                                        bind_preview_update=True)
        row_y += 1
        self.ncbi_var = self.create_labeled_entry(self.dialog, "NCBI ID (digits only):", row_y)
        row_y += 1

        # Create time unit dropdown
        tk.Label(self.dialog, text="Time Unit:").grid(row=row_y, column=0, sticky="e", padx=5, pady=5)
        self.time_unit_var = tk.StringVar()
        time_unit_cb = ttk.Combobox(self.dialog, textvariable=self.time_unit_var, state="readonly")
        time_unit_cb["values"] = ["day", "hours", "minutes", "seconds"]
        time_unit_cb.current(1)  # Set default to "hours"
        time_unit_cb.grid(row=row_y, column=1, padx=5, pady=5, sticky="w")
        row_y += 1

        # Create scrollable time points table
        tk.Label(self.dialog, text="Time Points:").grid(row=row_y, column=0, sticky="ne", padx=5, pady=5)
        table_canvas = tk.Canvas(self.dialog, bd=1, relief="sunken", height=80, width=250)
        table_canvas.grid(row=row_y, column=1, sticky="w", padx=5, pady=5)

        # Add horizontal scrollbar
        h_scroll = tk.Scrollbar(self.dialog, orient="horizontal", command=table_canvas.xview)
        h_scroll.grid(row=row_y + 1, column=1, sticky="ew", padx=5)
        table_canvas.configure(xscrollcommand=h_scroll.set)

        # Create frame for time point entries
        table_frame = tk.Frame(table_canvas)
        table_canvas.create_window((0, 0), window=table_frame, anchor="nw")

        # Add time point labels and entry fields
        self.time_entries = []
        for i in range(required_times_count):
            tk.Label(table_frame, text=f"T{i}", width=6, anchor="center").grid(row=0, column=i, padx=1, pady=1)
        for i in range(required_times_count):
            var = tk.StringVar()
            tk.Entry(table_frame, textvariable=var, width=6, justify="center").grid(row=1, column=i, padx=1, pady=1)
            self.time_entries.append(var)

        # Configure scrolling behavior
        table_frame.bind("<Configure>", lambda e: table_canvas.configure(scrollregion=table_canvas.bbox("all")))
        row_y += 2

        # Add separator lines and filename pattern preview section
        line_above = tk.Frame(self.dialog, height=1, background="white")
        line_above.grid(row=row_y, column=0, columnspan=2, sticky="ew", pady=(10, 0))
        row_y += 1

        self.filename_pattern_var = self.create_filename_pattern_entry(self.dialog, row_y)
        row_y += 1

        # Add filename preview section
        tk.Label(self.dialog,
                 text="Image filename will be changed according to the above info.\n"
                      "Example (time=0, view=0, .tif):",
                 justify="left", anchor="w", font=("Helvetica", 10)
                 ).grid(row=row_y, column=0, columnspan=2, sticky="w", padx=5, pady=(5, 0))
        row_y += 1

        # Create preview box with black background
        tk.Label(self.dialog, text="Preview:").grid(row=row_y, column=0, sticky="e", padx=5, pady=5)
        preview_box = tk.Frame(self.dialog, bd=2, relief="sunken", background="black", height=25, width=280)
        preview_box.grid(row=row_y, column=1, sticky="w", padx=5, pady=5)
        preview_box.pack_propagate(False)

        # Add preview label
        pattern = self.build_filename_pattern()
        self.filename_preview_label = tk.Label(preview_box, text=f"{pattern}_t0_view0.tif", bg="black", fg="white")
        self.filename_preview_label.pack(padx=5, pady=2, anchor="w")
        row_y += 1

        # Add bottom separator and buttons
        line_below = tk.Frame(self.dialog, height=1, background="white")
        line_below.grid(row=row_y, column=0, columnspan=2, sticky="ew")
        row_y += 1

        # Add OK and Cancel buttons
        btn_frame = tk.Frame(self.dialog)
        btn_frame.grid(row=row_y, column=0, columnspan=2, pady=10)
        tk.Button(btn_frame, text="OK", command=self.on_ok).pack(side="left", padx=5)
        tk.Button(btn_frame, text="Cancel", command=self.on_cancel).pack(side="left", padx=5)

        # Center dialog on parent window
        self.dialog.update_idletasks()
        if parent is not None:
            x = parent.winfo_rootx() + (parent.winfo_width() // 2) - (self.dialog.winfo_width() // 2)
            y = parent.winfo_rooty() + (parent.winfo_height() // 2) - (self.dialog.winfo_height() // 2)
            self.dialog.geometry(f"+{x}+{y}")

        # Wait for dialog to close and return result
        self.dialog.wait_window()

        return self.result

    def on_ok(self) -> None:
        """
        Validate export inputs; if valid, store the export parameters in self.result and close the dialog.
        """
        # Get and validate dataset name
        dataset_name = self.dataset_name_var.get()
        if not dataset_name:
            messagebox.showwarning("Invalid Dataset Name", "Dataset name cannot be empty.", parent=self.dialog)
            return

        # Sanitize dataset name (allow only letters/digits/underscores)
        ds_name = self.sanitize_dataset_name(dataset_name)
        if dataset_name != ds_name:
            messagebox.showwarning(
                "Dataset Name Changed",
                f"Renamed '{dataset_name}' to '{ds_name}' (only letters/digits/underscores).",
                parent=self.dialog
            )
            self.dataset_name_var.set(ds_name)
            return
        self.dataset_name_var.set(ds_name)

        # Validate species field
        sp = self.species_var.get().strip()
        if not sp:
            messagebox.showwarning("Invalid Species", "Species cannot be empty.", parent=self.dialog)
            return

        # Validate observation sample field
        obs = self.obs_sample_var.get().strip()
        if not obs:
            messagebox.showwarning("Invalid Observation Sample", "Observation sample cannot be empty.",
                                   parent=self.dialog)
            return

        # Validate NCBI ID (must be numeric)
        ncbi = self.ncbi_var.get().strip()
        if not ncbi.isdigit():
            messagebox.showwarning("Invalid NCBI ID", "NCBI ID must be digits only.", parent=self.dialog)
            return

        # Validate time unit selection
        tu = self.time_unit_var.get()
        if tu not in ["day", "hours", "minutes", "seconds"]:
            messagebox.showwarning("Invalid Time Unit", "Time unit must be day/hours/minutes/seconds.",
                                   parent=self.dialog)
            return
        short_tu = self.TIME_UNIT_MAP[tu]  # Convert to abbreviated form (d/h/min/s)

        # Validate time points - must be numeric and strictly increasing
        times: list[float] = []
        for idx, var in enumerate(self.time_entries):
            txt = var.get().strip()
            if not txt:
                messagebox.showwarning("Invalid Time Value", f"Time point T{idx} is empty.", parent=self.dialog)
                return
            if not txt.isdigit():
                messagebox.showwarning("Invalid Time Value", f"'{txt}' (T{idx}) is not a number.", parent=self.dialog)
                return
            times.append(float(txt))

        # Check if time points are strictly increasing
        for i in range(len(times) - 1):
            if times[i] >= times[i + 1]:
                messagebox.showwarning("Invalid Time Order",
                                       f"Time points must be strictly increasing (T{i}={times[i]} >= T{i + 1}={times[i + 1]}).",
                                       parent=self.dialog)
                return

        # Store validated parameters and close dialog
        self.result = {
            "dataset_name": ds_name,
            "species": sp,
            "observation_sample": obs,
            "ncbi_id": ncbi,
            "time_unit": short_tu,
            "time_points": times
        }
        self.dialog.destroy()

    def on_cancel(self) -> None:
        """
        Cancel export and close the dialog.
        """
        self.result = None
        self.dialog.destroy()
