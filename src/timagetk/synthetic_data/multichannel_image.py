#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------
from timagetk import MultiChannelImage
from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image
from timagetk.synthetic_data.nuclei_image import nuclei_signal_images_from_point_position
from timagetk.synthetic_data.wall_image import wall_image_from_labelled_image


def example_layered_sphere_multichannel_image(n_points=100, n_layers=3,
                                              extent=50., voxelsize=(0.5, 0.25, 0.25),
                                              wall_sigma=None, wall_intensity=None,
                                              nuclei_radius=1.5, nuclei_intensity=240,
                                              signal_type='random', signal_intensity=240,
                                              **kwargs):
    """Generate a synthetic multichannel 3D image with 3 channels.

    Parameters
    ----------
    n_points : int, optional
        The number of cells on innermost spherical layer. Defaults to ``100``.
    n_layers : int, optional
        The number of layer around the central cell. Defaults to ``3``.
    extent : float, optional
        The extent of the output image (in µm). Defaults to ``50.``.
    voxelsize : tuple of float, optional
        Length-3 tuple of image Z,Y,X voxelsize (in µm). Defaults to ``(0.5, 0.25, 0.25)``.
    wall_sigma : float, optional
        Sigma value determining the wall width, in real units. Defaults to ``None``.
    wall_intensity : float, optional
        Signal intensity of the wall interfaces. Defaults to ``None``.
    nuclei_radius : float, optional
        Radius of the spherical blobs representing nuclei (in µm). Defaults to ``1.5``.
    nuclei_intensity : int, optional
        Intensity value of the nuclei blobs. Defaults to ``240``.
    signal_type : {'random', 'center'}, optional
        Whether to use a radially decreasing ('center') or a random signal ('random'). Defaults to ``random``.
    signal_intensity : float, optional
        The intensity value of the signal blobs. Defaults to ``240``.

    Returns
    -------
    MultiChannelImage
        A MultiChannelImage object containing three channels: wall, ref, and signal.

    Examples
    --------
    >>> from timagetk.synthetic_data.multichannel_image import example_layered_sphere_multichannel_image
    >>> multichannel_image = example_layered_sphere_multichannel_image(n_points=120, n_layers=4)
    >>> print(multichannel_image)

    """
    return_points = kwargs.get('return_points', False)
    seg_img, pts = example_layered_sphere_labelled_image(n_points=n_points, n_layers=n_layers,
                                                         extent=extent, voxelsize=voxelsize,
                                                         return_points=True)
    kwargs.update({"return_points": False})
    wall_img = wall_image_from_labelled_image(seg_img, wall_sigma=wall_sigma, wall_intensity=wall_intensity)
    ref_nuclei, signal_nuclei, _ = nuclei_signal_images_from_point_position(pts, extent=extent, voxelsize=voxelsize,
                                                                            nuclei_radius=nuclei_radius,
                                                                            nuclei_intensity=nuclei_intensity,
                                                                            signal_type=signal_type,
                                                                            signal_intensity=signal_intensity)
    if return_points:
        return MultiChannelImage([wall_img, ref_nuclei, signal_nuclei], ['wall', 'ref', 'signal']), pts
    else:
        return MultiChannelImage([wall_img, ref_nuclei, signal_nuclei], ['wall', 'ref', 'signal'])
