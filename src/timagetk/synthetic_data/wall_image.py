#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------


from timagetk.components.tissue_image import TissueImage3D
from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image
from timagetk.synthetic_data.util import pseudo_gradient_norm


def wall_image_from_labelled_image(labelled_image, wall_sigma=None, wall_intensity=None, **kwargs):
    """Generate a 3D image representing the walls of a labelled image.

    Parameters
    ----------
    labelled_image : timagetk.LabelledImage
        The cell image on which to compute the wall signal.
    wall_sigma : float, optional
        Sigma value determining the wall width, in real units.
        Defaults to the image voxelsize ``seg_img.voxelsize``.
    wall_intensity : float, optional
        Signal intensity of the wall interfaces.
        Defaults to 2/3 of the maximum value based on `dtype`.

    Other Parameters
    ----------------
    dtype : {"uint8", "uint16"}
        The bit-depth of the intensity image.
        Defaults to ``'uint8'``.
    count : bool, optional
        Whether to have higher signal at wall junctions.
        Defaults to ``True``.

    Returns
    -------
    timagetk.SpatialImage
        The generated cell wall intensity image.

    See Also
    --------
    timagetk.synthetic_data.util.pseudo_gradient_norm

    Examples
    --------
    >>> from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image
    >>> from timagetk.synthetic_data.wall_image import wall_image_from_labelled_image
    >>> from timagetk.visu.mplt import image_plot
    >>> seg_img = example_layered_sphere_labelled_image()
    >>> wall_img = wall_image_from_labelled_image(seg_img)
    >>> z_sl = seg_img.get_shape("z") // 2
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> v = fig = grayscale_imshow([seg_img, wall_img], slice_id=z_sl, cmap=['glasbey', 'gray'], val_range='auto')

    """
    seg_img = TissueImage3D(labelled_image, not_a_label=0, background=1)
    return pseudo_gradient_norm(seg_img, dtype=kwargs.get('dtype', "uint8"), wall_sigma=wall_sigma,
                                intensity=wall_intensity, count=kwargs.get('count', True))


def example_layered_sphere_wall_image(extent=60., voxelsize=(0.6, 0.6, 0.6), n_points=12, n_layers=1,
                                      wall_sigma=None, wall_intensity=None, return_points=False, **kwargs):
    """Generate a synthetic 3D image representing the walls of a spherical tissue.

    The function creates a 3D image over a cubic volume of a given extent.
    It consists of successive spherical cell layers of equal height around a central spherical cell.
    The number of cells in each layer is set so that cells in every layer have similar volumes.

    Parameters
    ----------
    extent : float, optional
        The extent of the output image (in µm).
        Defaults to ``60.``.
    voxelsize : list(float), optional
        Length-3 list of image voxelsize (in µm), ZYX sorted.
        Defaults to ``(0.6, 0.6, 0.6)``.
    n_points : int, optional
        The number of cells on innermost spherical layer.
        Defaults to ``12``.
    n_layers : int, optional
        The number of layer around the central cell.
        Defaults to ``1``.
    wall_sigma : float, optional
        Sigma value determining the wall width, in real units.
        Defaults to the image voxelsize ``seg_img.voxelsize``.
    intensity : float, optional
        Signal intensity of the wall interfaces.
        Defaults to 2/3 of the maximum value based on `dtype`.
    return_points : bool, optional
        Whether to return the points representing the cell centers.

    Other Parameters
    ----------------
    dtype : {"uint8", "uint16"}
        The bit-depth of the intensity image.
        Defaults to ``'uint8'``.
    count : bool
        Whether to have higher signal at wall junctions.
        Defaults to ``True``.

    Returns
    -------
    timagetk.SpatialImage
        The generated cell wall reference image.
    dict, optional
        The 3D point positions, XYZ sorted, used to generate the cells.

    See Also
    --------
    timagetk.synthetic_data.labelled_image.example_layered_sphere_labelled_image
    timagetk.synthetic_data.util.pseudo_gradient_norm

    Examples
    --------
    >>> from timagetk.synthetic_data.wall_image import example_layered_sphere_wall_image
    >>> from timagetk.visu.stack import orthogonal_view
    >>> wall_img = example_layered_sphere_wall_image()
    >>> v = orthogonal_view(wall_img, suptitle="Synthetic 3D wall intensity image", cmap='gray', val_range='auto')

    """
    seg_img, points = example_layered_sphere_labelled_image(extent, voxelsize, n_points, n_layers, return_points=True)
    seg_img = TissueImage3D(seg_img, not_a_label=0, background=1)
    img = pseudo_gradient_norm(seg_img, dtype=kwargs.get('dtype', "uint8"), wall_sigma=wall_sigma,
                               intensity=wall_intensity, count=kwargs.get('count', True))

    if return_points:
        return img, points
    else:
        return img
