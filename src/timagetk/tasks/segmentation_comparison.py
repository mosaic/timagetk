#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Segmentation Comparison Module

This module provides tools to measure, interpret, and compare segmentation results from various sources, enabling a deeper understanding of segmentation correctness and potential errors.
It is especially useful for post-processing tasks and quality control in image analysis pipelines.

Key Features
------------
In-depth overlap analysis between candidate and reference segmentations.
Automated classification of segmentation states (_e.g._, over-segmentation, under-segmentation).
Computation of statistics to evaluate segmentation quality.

Usage Examples
-------------

>>> from timagetk.tasks.segmentation_comparison import overlap_interpretation
>>> from timagetk.tasks.segmentation_comparison import overlap_analysis
>>> from timagetk.tasks.segmentation_comparison import statistics_segmentation
>>> from timagetk.io.dataset import shared_data
>>> from timagetk.third_party.plantseg import unet_tiled_predict
>>> from timagetk.third_party.plantseg import multicut_segmentation
>>> from timagetk.tasks.segmentation import seeded_watershed
>>> # Example compare the seeded watershed segmentation algorithm against the multicut segmentation algorithm
>>> int_img = shared_data('flower_confocal', 0)
>>> pred_img = unet_tiled_predict(int_img)  # improve intensity image using 3D-UNET
>>> seg_img_A = multicut_segmentation(pred_img)  # cell segmentation using 3D-MCUT
>>> seg_img_B, _, _ = seeded_watershed(pred_img, h_min=5)  # cell segmentation using 3D-WS
>>> candidate_overlap, reference_overlap = overlap_analysis(seg_img_A, seg_img_B)  # cell overlap analysis
>>> # Interpret the results of the cell overlap to evaluate the seeded watershed segmentation performance against the multicut segmentation algorithm
>>> out_df = overlap_interpretation(candidate_overlap, reference_overlap)
>>> print(out_df)
           candidate reference segmentation_state
0    [295, 480, 514]       [7]  over-segmentation
1              [314]     [932]         one-to-one
2         [315, 316]     [644]  over-segmentation
3              [430]     [935]         one-to-one
4              [431]       [2]         one-to-one
..               ...       ...                ...
929            [461]     [816]         one-to-one
930            [280]     [808]         one-to-one
931            [320]     [813]         one-to-one
932            [367]     [804]         one-to-one
933           [8, 4]     [905]  over-segmentation
>>> # Compute the statistics of the cell segmentation
>>> cell_statistics = statistics_segmentation(out_df)
Cell statistics:
{'one-to-one': 47.75, 'over-segmentation': 51.76, 'under-segmentation': 0.14, 'misc.': 0.35, 'missing': 0.0}

"""

import networkx as nx
import numpy as np
import pandas as pd
from tqdm import tqdm

from timagetk import LabelledImage
from timagetk.algorithms.resample import resample
from timagetk.bin.logger import get_logger

log = get_logger(__name__)

POSS_CRITERIA = ['jaccard', 'target_reference', 'target_candidate', 'intersection']


def cell_overlap(reference_img, candidate_img, method='target_reference', ds=1,
                 reference_label=None, candidate_label=None, verbose=False,
                 decimal=5):
    """Calculate the overlap between labelled cells in two segmented images.

    This function computes the overlap metrics between cells in two labelled images.
    The overlap can be calculated in several ways depending on the selected method,
    including by targeting the reference or candidate image, using the Jaccard index,
    or by directly measuring intersection size.

    Available criterion are:

      - **jaccard**: J(A, B) = |A n B|/|A u B| [1]
      - **target_(reference/candidate)**: T(A, B) = |A n B|/|B| OR T(B, A) = |A n B|/|A|
      - **intersection**: I(A, B) = |A n B|

    Parameters
    ----------
    reference_img : timagetk.LabelledImage
        Segmented image containing reference cells with distinct labels.
    candidate_img : timagetk.LabelledImage
        Segmented image containing candidate cells with distinct labels.
    method : {'target_reference', 'target_candidate', 'jaccard', 'intersection'}, optional
        The method to calculate the overlap metric:
            - 'target_reference': Fraction of overlapping volume with respect to the reference cell volume.
            - 'target_candidate': Fraction of overlapping volume with respect to the candidate cell volume.
            - 'jaccard': Jaccard index, calculated as intersection volume divided by union volume.
            - 'intersection': Raw volume of the intersection region between cells.
        Default is ``'target_reference'``.
    ds : int, optional
        Downsample factor to reduce image resolution and improve computation 
        speed. Default is ``1`` (no downsampling).
    reference_label : list of int, optional
        List of labels to specify the reference cells for overlap computation.
        If ``None``, all reference labels in `reference_seg` are used. Default is ``None``.
    candidate_label : list of int, optional
        List of labels to specify the candidate cells for overlap computation. 
        If ``None``, all candidate labels in `candidate_seg` are used. Default is ``None``.
    verbose : bool, optional
        If ``True``, prints progress and intermediate results during computation. 
        Default is ``False``.
    decimal : int, optional
        Number of decimals to round the resulting overlap values. Default is ``5``.

    Returns
    -------
    dict
        Dictionary where keys are tuples representing reference-candidate label pairs 
        (reference_label, candidate_label) and values are the corresponding overlap 
        values calculated using the specified method.

    Example
    -------
    >>> from timagetk.components.labelled_image import LabelledImage
    >>> from timagetk.tasks.segmentation_comparison import cell_overlap
    >>> import numpy as np
    >>> I = np.array([[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],[1, 1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 3, 3, 3, 3, 1, 1, 1], [1, 1, 1, 3, 3, 3, 3, 1, 1, 1], [1, 1, 1, 3, 3, 3, 3, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]])
    >>> J = np.array([[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],[1, 1, 1, 1, 2, 2, 2, 1, 1, 1], [1, 1, 1, 1, 2, 2, 2, 1, 1, 1], [1, 1, 1, 1, 2, 2, 2, 1, 1, 1], [1, 1, 1, 1, 2, 2, 2, 1, 1, 1], [1, 1, 1, 1, 2, 2, 2, 1, 1, 1], [1, 1, 1, 1, 2, 2, 2, 1, 1, 1], [1, 1, 1, 1, 2, 2, 2, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]])
    >>> I = LabelledImage(I, not_a_label = 0)
    >>> J = LabelledImage(J, not_a_label = 0)
    >>> cov = cell_overlap(I, J, method = 'target_reference')
    >>> print(cov)

    """
    # Validate input types
    try:
        assert isinstance(reference_img, LabelledImage)
    except:
        raise ValueError(f"Input `reference_img` must be a LabelledImage, got {type(reference_img)} instead.")
    try:
        assert isinstance(candidate_img, LabelledImage)
    except:
        raise ValueError(f"Input `candidate_img` must be a LabelledImage, got {type(candidate_img)} instead.")

    # Validate overlap calculation method
    try:
        assert method in {'target_reference', 'target_candidate', 'jaccard', 'intersection'}
    except:
        raise ValueError(f"Unknown `overlap` method '{method}', choose from: {', '.join(POSS_CRITERIA)}.")

    try:
        np.testing.assert_allclose(reference_img.voxelsize, candidate_img.voxelsize, rtol=1e-05, atol=1e-08,
                                   equal_nan=False,
                                   err_msg='Voxel size mismatch between reference and candidate images.')
    except AssertionError as e:
        log.info(f"Reference image voxel size: {reference_img.voxelsize}.")
        log.info(f"Candidate image voxel size: {candidate_img.voxelsize}.")
        raise ValueError(e)

    # Downsample images if requested to improve performance
    if ds != 1:
        reference_img = resample(reference_img, voxelsize=[vox * ds for vox in reference_img.voxelsize],
                                 interpolation='cellbased', cell_based_sigma=1)
        candidate_img = resample(candidate_img, voxelsize=[vox * ds for vox in candidate_img.voxelsize],
                                 interpolation='cellbased', cell_based_sigma=1)

    # Check given labels against those found in the images:
    if reference_label is not None:
        reference_label = list(set(reference_label) & set(reference_img.labels()))
    if candidate_label is not None:
        candidate_label = list(set(candidate_label) & set(candidate_img.labels()))

    # Get bounding boxes for each cell label:
    reference_bboxes = reference_img.boundingbox(labels=reference_label)
    candidate_bboxes = candidate_img.boundingbox(labels=candidate_label)

    # Pre-calculate cell volumes (in voxels):
    ref_voxel_counts = {lab: np.sum(reference_img[mbbox].get_array() == lab) for lab, mbbox in
                        reference_bboxes.items()}
    can_voxel_counts = {lab: np.sum(candidate_img[mbbox].get_array() == lab) for lab, mbbox in
                        candidate_bboxes.items()}

    overlap_metric = {}
    # Calculate overlap between each reference-candidate cell pair
    for ref_label, ref_bbox in tqdm(reference_bboxes.items(), unit='cell', desc=f'Overlap calculation ({method})'):
        # Get the list of potential candidate labels in the selected region:
        poss_can_labels = np.unique(candidate_img[ref_bbox].get_array())
        for can_label in poss_can_labels:
            # Get the bounding box for the corresponding candidate label:
            can_bbox = candidate_bboxes[can_label]
            # Check if bounding boxes intersect
            coord_min = [max(ref_b.start, can_b.start) for ref_b, can_b in zip(ref_bbox, can_bbox)]
            coord_max = [min(ref_b.stop, can_b.stop) for ref_b, can_b in zip(ref_bbox, can_bbox)]

            # Get the intersection region between cells
            bbox_intersection = tuple(
                [slice(dim_min, dim_max, None) for dim_min, dim_max in zip(coord_min, coord_max)])

            # Extract overlapping regions
            reference_label_mask = np.array(reference_img[bbox_intersection] == ref_label)
            candidate_label_mask = np.array(candidate_img[bbox_intersection] == can_label)

            # Compute intersection volume (in voxels)
            overlapping_voxels = (reference_label_mask & candidate_label_mask).sum()

            # Calculate overlap metric based on selected method
            if method == 'target_reference':
                val = overlapping_voxels / ref_voxel_counts[ref_label]
            elif method == 'target_candidate':
                val = overlapping_voxels / can_voxel_counts[can_label]
            elif method == 'jaccard':  # Jaccard index = intersection / union
                val = overlapping_voxels / (
                            ref_voxel_counts[ref_label] + can_voxel_counts[can_label] - overlapping_voxels)
            else:
                val = overlapping_voxels

            # Round result and store
            overlap_metric[(ref_label, can_label)] = np.around(val, decimals=decimal)
            log.debug(f"--> Couple ({ref_label}, {can_label}) : {overlap_metric[(ref_label, can_label)]}")

    return overlap_metric


def overlap_analysis(candidate_img, reference_img):
    """Compute the overlap between candidate segmentation and reference segmentation.

    Overlap values indicate how much of a tested cell is contained within another (target):

    - Overlap is 1 if the tested cell is completely contained in the target.
    - Overlap is 0.5 if 50% of the tested cell is contained in the target.
    - Other values indicate varying degrees of overlap.

    Parameters
    ----------
    candidate_img : timagetk.LabelledImage
        The candidate segmentation image.
    reference_img : timagetk.LabelledImage
        The reference segmentation image.

    Returns
    -------
    dict
        Overlap metrics for candidate cells compared with reference cells (targets).
    dict
        Overlap metrics for reference cells compared with candidate cells (targets).

    Examples
    --------
    >>> from timagetk.tasks.segmentation_comparison import overlap_interpretation
    >>> from timagetk.tasks.segmentation_comparison import overlap_analysis
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.third_party.plantseg import unet_tiled_predict
    >>> from timagetk.third_party.plantseg import multicut_segmentation
    >>> from timagetk.tasks.segmentation import seeded_watershed
    >>> int_img = shared_data('flower_confocal', 0)
    >>> pred_img = unet_tiled_predict(int_img)  # improve intensity image using 3D-UNET
    >>> seg_img_A = multicut_segmentation(pred_img)  # cell segmentation using 3D-MCUT
    >>> seg_img_B, _, _ = seeded_watershed(pred_img, h_min=5)  # cell segmentation using 3D-WS
    >>> candidate_overlap, reference_overlap = overlap_analysis(seg_img_A, seg_img_B)
    """
    # - candidate --> reference
    candidate = cell_overlap(candidate_img, reference_img, method='target_reference', ds=1, verbose=False)
    # - reference --> candidate
    reference = cell_overlap(candidate_img, reference_img, method='target_candidate', ds=1, verbose=False)
    return candidate, reference


def overlap_interpretation(candidate_overlap, reference_overlap, background=1, thresh_background=0.5):
    """Interpret the overlaps between candidate and reference segmentation.

    Associates predicted cells from a candidate segmentation to reference ground-truth cells based
    on their inclusion relationships, determining segmentation types such as bijection, over-
    segmentation, under-segmentation, or background association.

    Background associations are resolved first based on a specified inclusion threshold. If a
    predicted cell's overlap with the background surpasses the threshold, it is associated with
    the background.

    Parameters
    ----------
    candidate_overlap : dict
        Dictionary containing the overlap information of a candidate cell with reference cells.
    reference_overlap : dict
        Dictionary containing reference segmentation results with 'candidate', 'reference',
        and 'reference_in_candidate' columns. Each row should provide the overlap information
        of a reference cell with candidate cells.
    background : int, optional
        Label indicating the background in both the candidate and reference segmentations.
        Defaults to ``1``.
    thresh_background : float, optional
        Minimum threshold for the inclusion of a cell in the background. If a cell's background
        inclusion is higher than this threshold, it will be associated with the background.
        Defaults to ``0.5``.

    Returns
    -------
    pandas.DataFrame
        DataFrame containing the results of the cell segmentation analysis. Each row represents
        a unique association between predicted and ground-truth cells:
            - 'candidate': List of predicted cell labels
            - 'reference': List of corresponding reference cell labels
            - 'segmentation_state': State of segmentation (bijection, over-segmentation,
              under-segmentation, or background)

    Examples
    --------
    >>> from timagetk.tasks.segmentation_comparison import overlap_interpretation
    >>> from timagetk.tasks.segmentation_comparison import overlap_analysis
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.third_party.plantseg import unet_tiled_predict
    >>> from timagetk.third_party.plantseg import multicut_segmentation
    >>> from timagetk.tasks.segmentation import seeded_watershed
    >>> # Example compare the seeded watershed segmentation algorithm against the multicut segmentation algorithm
    >>> int_img = shared_data('flower_confocal', 0)
    >>> pred_img = unet_tiled_predict(int_img)  # improve intensity image using 3D-UNET
    >>> seg_img_A = multicut_segmentation(pred_img)  # cell segmentation using 3D-MCUT
    >>> seg_img_B, _, _ = seeded_watershed(pred_img, h_min=5)  # cell segmentation using 3D-WS
    >>> candidate_overlap, reference_overlap = overlap_analysis(seg_img_A, seg_img_B)  # cell overlap analysis
    >>> # Interpret the results of the cell overlap to evaluate the seeded watershed segmentation performance against the multicut segmentation algorithm
    >>> out_df = overlap_interpretation(candidate_overlap, reference_overlap)
    >>> print(out_df)
               candidate reference segmentation_state
    0    [295, 480, 514]       [7]  over-segmentation
    1              [314]     [932]         one-to-one
    2         [315, 316]     [644]  over-segmentation
    3              [430]     [935]         one-to-one
    4              [431]       [2]         one-to-one
    ..               ...       ...                ...
    929            [461]     [816]         one-to-one
    930            [280]     [808]         one-to-one
    931            [320]     [813]         one-to-one
    932            [367]     [804]         one-to-one
    933           [8, 4]     [905]  over-segmentation
    """
    # Handle background associations first
    # Find candidate cells that overlap significantly with reference background
    candidate_background = [can for (ref, can), ov_val in candidate_overlap.items()
                            if ref == background and ov_val >= thresh_background]
    # Find reference cells that overlap significantly with candidate background
    reference_background = [ref for (ref, can), ov_val in reference_overlap.items()
                            if can == background and ov_val >= thresh_background]
    # Keep only actual background labels
    candidate_background = set(candidate_background) & {background}
    reference_background = set(reference_background) & {background}

    # Remove background-associated cells from overlap dictionaries
    candidate_overlap = {(ref, can): ov_val for (ref, can), ov_val in candidate_overlap.items()
                         if ref != background and can not in candidate_background}
    reference_overlap = {(ref, can): ov_val for (ref, can), ov_val in reference_overlap.items()
                         if can != background and ref not in reference_background}

    # Group overlaps by candidate cell
    grouped_candidate_overlap = {}
    for (ref, can), ov_val in candidate_overlap.items():
        grouped_candidate_overlap[can] = grouped_candidate_overlap.get(can, {}) | {ref: ov_val}
    # Group overlaps by reference cell
    grouped_reference_overlap = {}
    for (ref, can), ov_val in reference_overlap.items():
        grouped_reference_overlap[ref] = grouped_reference_overlap.get(ref, {}) | {can: ov_val}

    # Find 1:1 associations based on maximum overlap
    candidate_in_reference = {can: max(overlap_dict, key=overlap_dict.get)
                              for can, overlap_dict in grouped_candidate_overlap.items()}
    reference_in_candidate = {ref: max(overlap_dict, key=overlap_dict.get)
                              for ref, overlap_dict in grouped_reference_overlap.items()}

    # Build bipartite graph for relationship analysis
    candidate_labels = list(candidate_in_reference.keys())
    reference_labels = list(reference_in_candidate.keys())
    label_tp_list = [(can_label, 'candidate') for can_label in candidate_labels] + \
                    [(ref_label, 'reference') for ref_label in reference_labels]
    lg2nid = dict(zip(label_tp_list, range(len(label_tp_list))))

    # Create graph with nodes for each cell and edges for associations
    G = nx.Graph()
    G.add_nodes_from([(nid, {'label': lab, 'group': g}) for (lab, g), nid in lg2nid.items()])

    # Add edges for candidate->reference associations
    candidate_to_ref_list = [(lg2nid[(i, 'candidate')], lg2nid[(j, 'reference')])
                             for i, j in candidate_in_reference.items()]
    G.add_edges_from(candidate_to_ref_list)
    # Add edges for reference->candidate associations
    ref_to_candidate_list = [(lg2nid[(i, 'reference')], lg2nid[(j, 'candidate')])
                             for i, j in reference_in_candidate.items()]
    G.add_edges_from(ref_to_candidate_list)

    # Find connected components to identify cell relationships
    connected_subgraph = [list(G.subgraph(c)) for c in nx.connected_components(G)]
    nid2lg = {v: k for k, v in lg2nid.items()}

    # Process connected components to build results
    out_results = []
    for c in connected_subgraph:
        if len(c) > 1:  # Skip single nodes
            candidate, reference = [], []
            for nid in c:
                if nid2lg[nid][1] == 'candidate':
                    candidate.append(nid2lg[nid][0])
                else:
                    reference.append(nid2lg[nid][0])
            out_results.append({'candidate': candidate, 'reference': reference})

    # Add background associations to results
    for lab in candidate_background:
        if lab != background:
            out_results.append({'candidate': [lab], 'reference': []})
    for lab in reference_background:
        if lab != background:
            out_results.append({'candidate': [], 'reference': [lab]})

    # Create final dataframe and classify segmentation states
    out_df = pd.DataFrame(out_results)
    out_df['segmentation_state'] = out_df.apply(_classify_segmentation_state, axis=1)
    return out_df


def _classify_segmentation_state(row):
    """Classifies the segmentation state of a single row in a DataFrame.

    Determines the segmentation state based on the overlap relation between reference
    cells from a ground-truth segmentation and candidate cells from a predicted segmentation.
    The function evaluates different types of overlap relations such as:

    - 'background': when there are no reference cells.
    - 'missing': when there are no candidate cells.
    - 'one-to-one': when there is exactly one reference cell and one candidate cell.
    - 'under-segmentation': when multiple reference cells match with a single candidate cell.
    - 'over-segmentation': when one reference cell matches with multiple candidate cells.
    - 'misc.': when multiple reference and candidate cells are associated.

    Parameters
    ----------
    row : pd.Series
        A single row from a DataFrame containing two columns:
        - `reference`: a list representing ground-truth segmentation cells.
        - `candidate`: a list representing predicted segmentation cells.

    Returns
    -------
    str
        A string categorizing the segmentation state.
    """
    # Case 1: No reference cells - background case
    if len(row.reference) == 0:
        return 'background'
    # Case 2: No candidate cells - missing detection
    elif len(row.candidate) == 0:
        return 'missing'
    # Case 3: Single candidate cell
    elif len(row.candidate) == 1:
        # Perfect match: one reference to one candidate
        if len(row.reference) == 1:
            return 'one-to-one'
        # Multiple reference cells match single candidate - under-segmentation
        else:
            return 'under-segmentation'
    # Case 4: Multiple candidate cells
    else:
        # Single reference matches multiple candidates - over-segmentation
        if len(row.reference) == 1:
            return 'over-segmentation'
        # Multiple references and multiple candidates - miscellaneous case
        else:
            return 'misc.'


def statistics_segmentation(out_results, verbose=True, ignore_background=True):
    """Calculates segmentation statistics based on predicted vs. ground truth data.

    This function computes different segmentation statistics like one-to-one matches,
    over-segmentation, under-segmentation, and miscellaneous cases, between predicted
    segmentation and ground-truth segmentation data. It returns the percentage distribution
    of these categories with an option to ignore the background or include it in the
    analysis. Furthermore, it computes the percentage of missing cells in the predicted
    segmentation relative to the ground truth.

    If `ignore_background` is set to True, the total number of cells analyzed
    excludes those candidate cells associated with the background.

    Parameters
    ----------
    out_results : pandas.DataFrame
        A DataFrame object containing segmentation results. It must include columns:
        `candidate` (list of candidate cell labels per instance), `segmentation_state`
        (state of segmentation for each candidate, e.g., one-to-one, over-segmentation),
        and optionally `reference` (ground truth reference cell labels per instance).
    verbose : bool, optional
        If ``True``, prints the segmentation statistics. Default is ``True``.
    ignore_background : bool, optional
        If ``True``, ignores background-associated candidate cells in the analysis. Default is ``True``.

    Returns
    -------
    dict of str, float
        A dictionary where keys are segmentation states (e.g., 'one-to-one',
        'over-segmentation', 'under-segmentation', 'missing', etc.) and values are
        their respective percentages (rounded to two decimal places) with respect
        to the total number of cells analyzed.

    Examples
    --------
    >>> from timagetk.tasks.segmentation_comparison import overlap_interpretation
    >>> from timagetk.tasks.segmentation_comparison import overlap_analysis
    >>> from timagetk.tasks.segmentation_comparison import statistics_segmentation
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.third_party.plantseg import unet_tiled_predict
    >>> from timagetk.third_party.plantseg import multicut_segmentation
    >>> from timagetk.tasks.segmentation import seeded_watershed
    >>> # Example compare the seeded watershed segmentation algorithm against the multicut segmentation algorithm
    >>> int_img = shared_data('flower_confocal', 0)
    >>> pred_img = unet_tiled_predict(int_img)  # improve intensity image using 3D-UNET
    >>> seg_img_A = multicut_segmentation(pred_img)  # cell segmentation using 3D-MCUT
    >>> seg_img_B, _, _ = seeded_watershed(pred_img, h_min=5)  # cell segmentation using 3D-WS
    >>> candidate_overlap, reference_overlap = overlap_analysis(seg_img_A, seg_img_B)  # cell overlap analysis
    >>> # Interpret the results of the cell overlap to evaluate the seeded watershed segmentation performance against the multicut segmentation algorithm
    >>> out_df = overlap_interpretation(candidate_overlap, reference_overlap)
    >>> # Compute the statistics of the cell segmentation
    >>> cell_statistics = statistics_segmentation(out_df)
    Cell statistics:
    {'one-to-one': 47.75, 'over-segmentation': 51.76, 'under-segmentation': 0.14, 'misc.': 0.35, 'missing': 0.0}
    """
    # Initialize dictionary to store counts for different segmentation categories
    cell_statistics = {'one-to-one': 0, 'over-segmentation': 0, 'under-segmentation': 0, 'misc.': 0}

    # Add background category if we're not ignoring it
    if not ignore_background:
        cell_statistics['background'] = 0

    # Create a mapping of cell labels to their segmentation states
    # Flattens the candidate labels and their states into a single dictionary
    state_candidate = {lab: state for list_lab, state
                       in zip(out_results.candidate.values, out_results.segmentation_state.values)
                       for lab in list_lab if state in cell_statistics}

    # Calculate percentages for each segmentation category
    total_cells = len(state_candidate)
    for lab, state in state_candidate.items():
        cell_statistics[state] += 1
    # Convert raw counts to percentages, rounded to 2 decimal places
    cell_statistics = {state: np.around(val / total_cells * 100, 2) for state, val in cell_statistics.items()}

    # Calculate percentage of missing cells in predicted segmentation
    # First, get total number of unique reference cells
    total_reference_cells = len(np.unique([item for sublist in out_results.reference.values for item in sublist]))

    # Get list of cells marked as missing in the segmentation
    missing_cells = [item for sublist in out_results.reference.loc[out_results.segmentation_state == 'missing'].values
                     for item in sublist]
    total_missing = len(missing_cells)

    # Add missing cells percentage to statistics
    cell_statistics['missing'] = np.around(total_missing / total_reference_cells * 100, 2)

    # Print results if verbose mode is enabled
    if verbose:
        print("Cell statistics:")
        print(cell_statistics)

    return cell_statistics
