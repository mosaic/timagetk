#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Nuclei detection module.

Nuclei detection is done using detection of local signal peaks.

"""

import numpy as np

from timagetk.algorithms.exposure import equalize_adapthist
from timagetk.algorithms.exposure import global_contrast_stretch
from timagetk.algorithms.peak_detection import detect_nuclei
from timagetk.algorithms.signal_quantification import quantify_nuclei_signal_intensity
from timagetk.bin.logger import get_logger
from timagetk.components.multi_channel import MultiChannelImage
from timagetk.components.spatial_image import SpatialImage

log = get_logger(__name__)


def nuclei_detection(intensity_image, threshold=3000., radius_range=(0.8, 1.4), step=0.2,
                     equalize_hist=True, contrast_stretch=False):
    """Detect nuclei positions in a (16-bit) nuclei marker SpatialImage.

    Parameters
    ----------
    intensity_image : timagetk.SpatialImage
        Intensity image with nuclei targeted signal.
    threshold : float, optional
        Response to Gaussian filter threshold.
    radius_range : tuple, optional
        Range of nuclei size to detect.
    step : float, optional
        Step of radius range.
    equalize_hist : bool, optional
        If ``True`` (default), performs 'adaptive histogram equalization' to image prior to any other step
    contrast_stretch : bool, optional
        If ``True`` (default is ``False``), performs 'contrast stretching' to image prior to any other step

    Returns
    -------
    numpy.ndarray
        Nuclei coordinates, sorted as XYZ.
    dict
        A proccess dictionary with the parameters of each algorithm

    Examples
    --------
    >>> import numpy as np
    >>> from scipy.cluster.vq import vq
    >>> from timagetk.tasks.nuclei_detection import nuclei_detection
    >>> from timagetk.io import imread
    >>> from timagetk.synthetic_data.nuclei_image import example_nuclei_image
    >>> # Let's start by creating a synthetic nuclei intensity image:
    >>> nuclei_img, points = example_nuclei_image(n_points=10, extent=15., nuclei_radius=1.5, nuclei_intensity=10000, return_points=True)
    >>> # Now we can detect nuclei positions on this synthetic image:
    >>> nuclei_arr, _ = nuclei_detection(nuclei_img, radius_range=(1.2, 1.6))
    >>> n_nuclei = nuclei_arr.shape[0]  # Number of detected nuclei
    >>> # It is now possible to compare detected nuclei positions & known position (ground-truth):
    >>> true_points = np.array(list(points.values()))  # convert the dictionary into an XYZ sorted array of points coordinates
    >>> matching = vq(true_points, nuclei_arr)
    >>> # Let's display the detected nuclei as a 2D top-view projection:
    >>> import matplotlib.pyplot as plt
    >>> plt.figure(figsize=(10, 10))
    >>> plt.scatter(true_points[:, 0], true_points[:, 1], color='red', label='Ground-truth')  # Show X & Y coordinates of ground truth coordinates in red
    >>> plt.scatter(nuclei_arr[:, 0], nuclei_arr[:, 1], color='green', marker="x", label='Prediction')  # Show X & Y coordinates of predicted coordinates in green
    >>> plt.imshow(nuclei_img.max(axis=0), cmap='gray', extent=(0, nuclei_img.extent[2], nuclei_img.extent[1], 0))  # Add maximum intensity projection of the nuclei signal
    >>> plt.title("Example of nuclei detection on synthetic data")
    >>> plt.legend()
    >>> plt.show()

    >>> image = imread("https://zenodo.org/record/3737795/files/qDII-CLV3-PIN1-PI-E35-LD-SAM4.czi")
    >>> print(image.channel_names)
    ['ChS1-T1_EYFP', 'ChS1-T2_EGFP', 'Ch2-T2_PI', 'Ch1-T3_EBFP', 'Ch2-T3_mCherry']
    >>> nuclei_img = image.get_channel('Ch1-T3_EBFP')
    >>> nuclei_arr, _ = nuclei_detection(nuclei_img)
    >>> n_nuclei = nuclei_arr.shape[0]  # Number of detected nuclei
    >>> import matplotlib.pyplot as plt
    >>> plt.figure(figsize=(15, 15))
    >>> plt.scatter(nuclei_arr[:, 0], nuclei_arr[:, 1], c=range(1, n_nuclei+1), cmap='glasbey')  # Show X & Y coordinates
    >>> plt.imshow(nuclei_img.max(axis=0), cmap='gray', extent=(0, nuclei_img.extent[2], nuclei_img.extent[1], 0))  # Add maximum intensity projection of the nuclei signal
    >>> plt.show()

    """
    process = {}

    # - Performs histogram equalization of intensity image if required
    if equalize_hist:
        log.info("Pre-processing: Adaptive Histogram Equalisation...")
        intensity_image = equalize_adapthist(intensity_image)
        process["equalized_hist"] = True

    # - Performs contrasts stretching of intensity images if required
    if contrast_stretch:
        log.info("Pre-processing: Global Contrast Stretching...")
        intensity_image = global_contrast_stretch(intensity_image)
        process["contrast_stretch"] = True

    nuclei_coordinates = detect_nuclei(intensity_image, threshold=threshold, radius_range=radius_range, step=step)
    process["detect_nuclei"] = {'threshold': threshold, 'radius_range': radius_range, 'step': step}

    return nuclei_coordinates, process


def compute_nuclei_channel_intensities(int_img, nuclei_points, channel_names=None, nuclei_sigma=0.5,
                                       reference_channel=None, ratio_channels={}, negative_ratios={}):
    """Compute nuclei signal intensities from a MultiChannelImage.

    Parameters
    ----------
    int_img : MultiChannelImage
        Multichannel intensity image with nuclei targeted signals to measure.
    nuclei_points : numpy.ndarray
        Nuclei coordinates, sorted as X, Y, Z.
    channel_names : list(str), optional
        Channels on which to measure nuclei signal intensity.
    nuclei_sigma : float, optional
        A real unit sigma to use as radius for intensity measurement.
    reference_channel : str, optional
        Channel to use as reference for ratiometric measurements.
    ratio_channels : dict, optional
        Channels to use to compute ratios, with the associated ratio name.
    negative_ratios : dict, optional
        Whether each ratio is to be computed negatively (1 - ratio) or not.

    Returns
    -------
    dict
        A dictionary of intensity values for each measured channel or ratio;
    dict
        A process dictionary with the parameters of each algorithm.


    Examples
    --------
    >>> import matplotlib.pyplot as plt
    >>> from timagetk import MultiChannelImage

    >>> from timagetk.tasks.nuclei_detection import nuclei_detection, compute_nuclei_channel_intensities
    >>> from timagetk.synthetic_data.nuclei_image import example_nuclei_signal_images
    >>> from timagetk.visu.mplt import image_plot
    >>> # Example 1 - Quick example on synthetic data:
    >>> nuclei_img, signal_img = example_nuclei_signal_images(n_points=30, extent=20., signal_type='random')
    >>> nuclei_arr, _ = nuclei_detection(nuclei_img)
    >>> img = MultiChannelImage([nuclei_img, signal_img], channel_names=['tag', 'signal'])
    >>> nuclei_sig, _ = compute_nuclei_channel_intensities(img, nuclei_arr, ['signal'])
    >>> fig = plt.figure(figsize=(20, 10))
    >>> gs = fig.add_gridspec(1, 2)
    >>> ax_img = fig.add_subplot(gs[0, 0])
    >>> image_plot(signal_img.max(axis=0),ax_img,cmap='gray',val_range='auto')
    >>> ax_img.scatter(nuclei_arr[:, 0], nuclei_arr[:, 1], c='r', s=40)
    >>> ax_sc = fig.add_subplot(gs[0, 1])
    >>> image_plot(signal_img.max(axis=0),ax_sc,alpha=0)
    >>> ax_sc.scatter(nuclei_arr[:, 0], nuclei_arr[:, 1], c=nuclei_sig['signal'], cmap='gray', ec='k', s=1000)
    >>> plt.show()
    >>> # Example 2 - using real data:
    >>> from timagetk.io import imread
    >>> from timagetk.tasks.nuclei_detection import detect_nuclei
    >>> from timagetk.visu.nuclei import nuclei_signals
    >>> from timagetk.visu.nuclei import channel_signal
    >>> url = "https://zenodo.org/record/3737795/files/qDII-CLV3-PIN1-PI-E35-LD-SAM4.czi"
    >>> ch_names = ["DII-VENUS-N7", "pPIN1:PIN1-GFP", "Propidium Iodide", "pRPS5a:TagBFP-SV40", "pCLV3:mCherry-N7"]
    >>> image = imread(url, channel_names=ch_names)
    >>> # Detect nuclei positions:
    >>> nuclei_arr, _ = nuclei_detection(image.get_channel('pRPS5a:TagBFP-SV40'))
    >>> # Quantify "pCLV3:mCherry-N7", "pRPS5a:TagBFP-SV40", "DII-VENUS-N7" and "Auxin" (as negative ratio of DII/TagBFP)
    >>> nuclei_sig, _ = compute_nuclei_channel_intensities(image, nuclei_arr, ["pCLV3:mCherry-N7"], reference_channel="pRPS5a:TagBFP-SV40", ratio_channels={'Auxin':'DII-VENUS-N7'}, negative_ratios={'Auxin': True})
    >>> nuclei_signals(nuclei_arr, nuclei_sig, image.get_extent('x'), image.get_extent('y'))
    >>> channel_signal(nuclei_arr, nuclei_sig["DII-VENUS-N7"], image, "DII-VENUS-N7")
    >>> channel_signal(nuclei_arr, nuclei_sig["Auxin"], image.get_channel("DII-VENUS-N7"), "Auxin")

    """
    # Make a MultiChannelImage out of a SpatialImage
    if not isinstance(int_img, MultiChannelImage):
        if isinstance(int_img, SpatialImage):
            try:
                assert channel_names is not None
            except AssertionError:
                raise ValueError("A channel name is required when providing a SpatialImage!")
            int_img = MultiChannelImage([int_img], channel_names=channel_names)

    # Make sure channel_names is a list:
    if channel_names is None:
        channel_names = []
    elif isinstance(channel_names, str):
        channel_names = [channel_names]
    # Add the reference channel to the list of channel to analyze if missing:
    if reference_channel is not None and reference_channel not in channel_names:
        channel_names += [reference_channel]

    # Check the channels to quantify exists:
    channel_exists = [c in int_img.get_channel_names() for c in channel_names]
    try:
        assert np.all(channel_exists)
    except AssertionError:
        missing_channels = channel_names[channel_exists == 0]
        raise ValueError(f"Missing the following channels in the MultiChannelImage: {missing_channels}")

    # Check the ratiometric channels to quantify exists:
    if len(ratio_channels) > 0:
        # Add the ratiometric channels to the list of channels to quantify:
        channel_names = list(set(channel_names + list(ratio_channels.values())))
        ratio_channel_exists = [c in int_img.get_channel_names() for c in ratio_channels.values()]
        try:
            assert np.all(ratio_channel_exists)
        except AssertionError:
            missing_ratio_channels = channel_names[ratio_channel_exists == 0]
            raise ValueError(f"Missing the following channels in the MultiChannelImage: {missing_ratio_channels}")
        # Complete the `negative_ratios` dict for non-negative ratiometric channels:
        negative_ratios.update({r: False for r in ratio_channels if r not in negative_ratios})

    # Quantify the signal intensity at nuclei positions for selected channels:
    channel_intensities = {}
    for channel_name in channel_names:
        channel_img = int_img.get_channel(channel_name)
        intensities = quantify_nuclei_signal_intensity(channel_img, nuclei_points, nuclei_sigma)
        channel_intensities[channel_name] = intensities
    # Transform to ratiometric measurements for selected ratiometric channels:
    for ratio_name in ratio_channels:
        ratios = channel_intensities[ratio_channels[ratio_name]] / channel_intensities[reference_channel]
        if negative_ratios[ratio_name]:
            channel_intensities[ratio_name] = 1. - ratios
        else:
            channel_intensities[ratio_name] = ratios

    process = {}
    process["quantify_nuclei_signal"] = {'nuclei_sigma': nuclei_sigma}
    if len(ratio_channels) > 0:
        process["signal_ratios"] = {'reference_channel': reference_channel,
                                    'ratio_channels': ratio_channels,
                                    'negative_ratios': negative_ratios}

    return channel_intensities, process
