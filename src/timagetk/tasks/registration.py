#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""This module regroups a set of methods dedicated to temporal registration of time-series."""

from timagetk.algorithms.blockmatching import blockmatching
from timagetk.algorithms.trsf import apply_trsf
from timagetk.algorithms.trsf import compose_trsf
from timagetk.bin.logger import get_logger
from timagetk.tasks.util import check_registrations
from timagetk.third_party.vt_parser import BLOCKMATCHING_METHODS

log = get_logger(__name__)

REG_MSG = "\nPerforms {} registration of t{} on t{}:"


def consecutive_registration(list_images, method='rigid', **kwargs):
    """Consecutive registration plugin register each image on the list onto the next one.

    Valid `method` values are:

      - **rigid**
      - **affine**
      - **vectorfield**

    Parameters
    ----------
    list_images : list of SpatialImage or list of MultiChannelImage
        List of images to register, should be time-sorted with the oldest (initial time-point) as the first of the list!
    method : {'rigid', 'affine', 'vectorfield'}, optional
        Registration method to use, 'rigid' by default

    Other Parameters
    ----------------
    trsf : dict
        Type and intervals (2-tuples) sorted dictionary of ``Trsf``
        Can be incomplete, only missing will be computed
    reference_channel : str
        Use it with ``MultiChannelImage`` to defines the channel to use to compute registration.

    Returns
    -------
    dict
        Time-intervals (2-tuples) sorted dictionary of temporally consecutive ``Trsf`` registering each image on the next one of the time-series.
    dict
        Time-intervals (2-tuples) sorted dictionary of ``SpatialImage`` registered in the frame of the next one.

    Notes
    -----
    **Warning**, with a large list of images, using 'vectorfield' as `method` might be problematic memory-wise!

    Example
    -------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.io import imread
    >>> from timagetk.tasks.registration import consecutive_registration
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> times = [0, 1, 2]
    >>> fnames = ['p58-t{}_INT_down_interp_2x.inr.gz'.format(time) for time in times]
    >>> images = [imread(shared_data(fname, 'p58')) for fname in fnames]

    >>> # Example #1 - Consecutive rigid registration:
    >>> trsfs, reg_imgs = consecutive_registration(images, method='rigid')
    >>> # Display results with 2D projections:
    >>> imgs = [images[0], reg_imgs[(0, 1)], images[1], images[1], reg_imgs[(1, 2)], images[2]]
    >>> titles = ["Original t0", "Registered t0 on t1", "Original t1", "Original t1", "Registered t1 on t2", "Original t2"]
    >>> fig = grayscale_imshow(imgs, suptitle="Consecutive RIGID registration", title=titles, val_range=[0, 255], method='contour', max_per_line=3)

    >>> # Example #2 - Consecutive non-linear registration:
    >>> # Use previously computed consecutive rigid transformations with `trsf` kwarg:
    >>> trsfs, reg_imgs = consecutive_registration(images, method='vectorfield', trsf={'rigid': trsfs})
    >>> # Display results with 2D projections:
    >>> imgs = [images[0], reg_imgs[(0, 1)], images[1], images[1], reg_imgs[(1, 2)], images[2]]
    >>> titles = ["Original t0", "Registered t0 on t1", "Original t1", "Original t1", "Registered t1 on t2", "Original t2"]
    >>> fig = grayscale_imshow(imgs, suptitle="Consecutive DEFORMABLE registration", title=titles, val_range=[0, 255], method='contour', max_per_line=3)

    """
    from timagetk.io import imread
    list_images, method = check_registrations(list_images, method)

    # Handle keyword argument trsf
    default_trsfs = {trsf_type: {} for trsf_type in BLOCKMATCHING_METHODS}
    trsfs = kwargs.pop('trsf', default_trsfs)
    for trsf_type in BLOCKMATCHING_METHODS:
        if trsf_type not in trsfs:
            trsfs[trsf_type] = default_trsfs[trsf_type]

    res_imgs = {}
    for flo_idx, flo_img in enumerate(list_images[:-1]):
        ref_img = list_images[flo_idx + 1]
        if isinstance(flo_img, str):
            flo_img = imread(flo_img)
        if isinstance(ref_img, str):
            ref_img = imread(ref_img)
        # Try to get the rigid transformation or compute it
        try:
            trsf_rig = trsfs['rigid'][(flo_idx, flo_idx + 1)]
        except KeyError:
            # Performs RIGID block-matching registration:
            trsf_rig = blockmatching(flo_img, ref_img, method='rigid', **kwargs)
            trsfs['rigid'][(flo_idx, flo_idx + 1)] = trsf_rig
        if method != 'rigid':
            # If other than rigid was requested:
            # 1. try to load or compute it with rigid trsf as left init
            # 2. compose the two trsfs with reference as template image if linear trsf
            try:
                trsf = trsfs[method][(flo_idx, flo_idx + 1)]
            except KeyError:
                # Performs requested block-matching registration:
                ref_img = list_images[flo_idx + 1]
                trsf = blockmatching(flo_img, ref_img, method=method, left_trsf=trsf_rig, **kwargs)
                if method == 'vectorfield':
                    trsf = compose_trsf([trsf_rig, trsf])
                else:
                    trsf = compose_trsf([trsf_rig, trsf], template_img=ref_img)
                trsfs[method][(flo_idx, flo_idx + 1)] = trsf
        else:
            # The trsf to apply to the floating image is the rigid one!
            trsf = trsf_rig

        # Apply the trsf to the floating image, if non-linear, no-need for template
        if method == 'vectorfield':
            res_imgs[(flo_idx, flo_idx + 1)] = apply_trsf(flo_img, trsf)
        else:
            res_imgs[(flo_idx, flo_idx + 1)] = apply_trsf(flo_img, trsf, template_img=ref_img)

    # Add last image of temporal sequence to the list of resulting images to returns
    res_imgs[(flo_idx + 1, flo_idx + 1)] = list_images[-1]

    return trsfs[method], res_imgs


def sequence_registration(list_images, method=None, **kwargs):
    """Images sequence registration on last time-point.

    First compute T(n,n+1) transformations, *i.e.* T(t_n -> t_n+1), for all n in
    [1, N], where N=length(list_images), then compose them to obtain the list of
    transformations registering all images on the last one:
    [T(0,N); T(1,N); ...; T(N-1,N)].

    Finally, apply the list of transformations to the images.

    Valid `method` values are:

      - **rigid**
      - **affine**
      - **vectorfield**

    Parameters
    ----------
    list_images : list of SpatialImage or list of MultiChannelImage
        List of images to register
    method : {'rigid', 'affine', 'vectorfield'}, optional
        Registration method to use, 'rigid' by default

    Other Parameters
    ----------------
    trsf : dict
        Type and intervals (2-tuples) sorted dictionary of ``Trsf``
        Can be incomplete, only missing will be computed
    reference_channel : str
        Channel to register, mandatory for multichannel images.

    Returns
    -------
    dict
        Time-intervals (2-tuples) sorted dictionary of ``Trsf`` registering each image on the last one of the time-series
    dict
        Time-intervals (2-tuples) sorted dictionary of ``SpatialImage`` registered in the frame of the last time-point

    Example
    -------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.io import imread
    >>> from timagetk.tasks.registration import sequence_registration
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> times = [0, 1, 2]
    >>> fnames = ['p58-t{}_INT_down_interp_2x.inr.gz'.format(time) for time in times]
    >>> images = [imread(shared_data(fname, 'p58')) for fname in fnames]

    >>> # Example #1 - Sequence rigid registration:
    >>> trsfs, reg_imgs = sequence_registration(images, method='rigid')
    >>> print(f"Computed rigid sequence registrations for time intervals: {', '.join([f'{tf}/{tr}' for tf, tr in trsfs.keys()])}")
    >>> # Display results with 2D projections:
    >>> imgs = [images[0], images[1], images[2], reg_imgs[(0, 2)], reg_imgs[(1, 2)], reg_imgs[(2, 2)]]
    >>> titles = ["Original t0", "Original t1", "Original t2", "Registered t0 on t2", "Registered t1 on t2", "Original t2"]
    >>> fig = grayscale_imshow(imgs, suptitle="Sequence RIGID registration", title=titles, val_range=[0, 255], method='contour', max_per_line=3)

    >>> # - Performs sequence vectorfield registration:
    >>> trsfs, res_imgs = sequence_registration(list_images, method='vectorfield')
    >>> print(f"Computed non-linear sequence registrations for time intervals: {', '.join([f'{tf}/{tr}' for tf, tr in trsfs.keys()])}")
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> fig = grayscale_imshow(res_imgs, suptitle="Sequence DEFORMABLE registration", title=fnames, val_range=[0, 255])

    """
    list_images, method = check_registrations(list_images, method)

    # - Compute the consecutive transformations, *i.e.* from one time-point to the next:
    log.info("Performing consecutive image registration...")
    trsfs, _ = consecutive_registration(list_images, method, **kwargs)
    # - Compose the consecutive transformations to all register to the last time-point:
    log.info("Composing consecutive transformations...")
    seq_trsfs = compose_to_last(trsfs, list_images[-1])

    log.info("Applying composed transformations...")
    res_imgs = {}
    for (flo_idx, ref_idx), trsf in seq_trsfs.items():
        log.info(f"Applying t{flo_idx}/{ref_idx} composed transformation to t{flo_idx}...")
        tmp_img = apply_trsf(list_images[flo_idx], trsf, template_img=list_images[-1])
        res_imgs[(flo_idx, ref_idx)] = tmp_img

    # Add last image of temporal sequence to the list of resulting images to returns
    res_imgs[(ref_idx, ref_idx)] = list_images[-1]

    return seq_trsfs, res_imgs


def compose_to_last(consecutive_trsf, template_img):
    """Compose a sequence of consecutive transformations.

    Parameters
    ----------
    consecutive_trsf : dict of Trsf
        Temporal sequence of consecutive transformations, *e.g.* [T(0, 1), T(1, 2), ..., T(N-1,N)]
    template_img : timagetk.SpatialImage
        Template image used as reference to give the shape and voxel-size of the returned transformation.
        Should be the last image used when computing consecutive registration.

    Returns
    -------
    list of Trsf
        List of composed transformations, *e.g.* [T(0, N), T(1, N), ..., T(N-1,N)]

    See Also
    --------
    timagetk.algorithms.trsf.compose_trsf

    Example
    -------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.io import imread
    >>> from timagetk.tasks.registration import consecutive_registration
    >>> from timagetk.tasks.registration import compose_to_last
    >>> times = [0, 1, 2]
    >>> list_fnames = ['p58-t{}_INT_down_interp_2x.inr.gz'.format(time) for time in times]
    >>> images = [imread(shared_data(fname, 'p58')) for fname in list_fnames]
    >>> # - Performs consecutive rigid registration:
    >>> trsfs, _ = consecutive_registration(images, method='rigid')
    >>> print(trsfs.keys())
    >>> sequence_trsf = compose_to_last(trsfs, template_img=images[-1])
    >>> print(sequence_trsf.keys())

    """
    n_imgs = len(consecutive_trsf) + 1

    # Browse dict of consecutive ``Trsf`` to find reference image index (max reference image)
    ref_idx = 0
    for (_tf, tr), _trsf in consecutive_trsf.items():
        if tr > ref_idx:
            ref_idx = tr
    # Create the list of required consecutive trsf indexes:
    required_trsf = list(zip(range(0, ref_idx - 1), range(1, ref_idx)))
    # Check we have all of them
    known_trsfs = [idx in consecutive_trsf for idx in required_trsf]
    try:
        assert all(known_trsfs)
    except AssertionError:
        missing_trsf = list(set(required_trsf) - set(known_trsfs))
        raise ValueError(
            f"Missing required consecutive transformation for interval{'s' if len(missing_trsf) > 1 else ''}: {missing_trsf}")

    # Transformations composition to obtain T(t_n -> t_N):
    seq_trsf = {}
    for (tf, tr), trsf in consecutive_trsf.items():
        if tr < n_imgs - 1:
            log.info(f"Composing consecutive transformations to get {tf}/{n_imgs - 1}...")
            trsfs_idx = list(zip(range(tf, ref_idx - 1), range(tf + 1, ref_idx)))
            comp_trsf = compose_trsf([consecutive_trsf[idx] for idx in trsfs_idx],
                                     template_img=template_img)
            seq_trsf[(tf, ref_idx)] = comp_trsf
        else:
            # 't_N-1'to't_N' transformation does not need composition!
            seq_trsf[(tf, ref_idx)] = trsf

    return seq_trsf
