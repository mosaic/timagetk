#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Module for TemporalTissueGraph creation from a series of TissueImage and Lineage."""

import pathlib
import time
from os.path import exists
from os.path import join

import numpy as np

from timagetk import TissueImage3D
from timagetk.bin.logger import get_logger
from timagetk.components.labelled_image import labels_at_stack_margins
from timagetk.features.graph import add_division_rate
from timagetk.features.graph import cell_deformation
from timagetk.features.graph import cell_growth
from timagetk.features.graph import epidermis_deformation
from timagetk.features.graph import epidermis_growth
from timagetk.graphs.temporal_tissue_graph import TemporalTissueGraph
from timagetk.io import imread
from timagetk.io.graph import CSV_EXPORT_SUFFIXES
from timagetk.io.graph import tissue_graph_from_csv
from timagetk.io.util import get_pne
from timagetk.tasks.features import TEMPORAL_DIFF_FEATURES
from timagetk.tasks.tissue_graph import tissue_graph_from_image
from timagetk.util import stuple

log = get_logger(__name__)


def temporal_tissue_graph_from_images(tissues, lineages, time_points, cells=None, coordinates=True, features=None,
                                      wall_features=None, temp_features=None, **kwargs):
    """Extracts a ``TemporalTissueGraph`` object from a series of ``TissueImage`` and ``Lineage``.

    Parameters
    ----------
    tissues : list of str or list[timagetk.TissueImage3D]
        The list of segmented tissues to transform.
    lineages : list of str or list[ctrl.lineage.Lineage]
        The list of lineages to use, should be of length ``len(tissues)-1``.
    time_points : list of int
        The list of time-points, should be of same length than `tissues`.
    cells : list of list of int, optional
        If given, allow to filter the labels used within the `tissue`. By default, all labels are used.
    coordinates : bool, optional
        If ``True`` add the topologocal element coordinates to the graph at construction time.
        See notes for more information.
    features : list, optional
        List of spatial cell features to extract from the segmented tissue.
        See ``timagetk.tasks.features.CELL_FEATURES``.
    wall_features : list, optional
        List of spatial wall features to extract from the segmented tissue.
        See ``timagetk.tasks.features.WALL_FEATURES``.
    temp_features : list, optional
        List of temporal features to extract from the segmented tissue.
        See ``timagetk.tasks.features.TEMPORAL_FEATURES``.

    Other Parameters
    ----------------
    max_missing_percent : float
        Percentage, in ``[0, 1]``, of missing values allowed on cell-edges and cell-vertices pairings. ``0.2`` by default.
    orientation : {1, -1}
        Image orientation, use ``-1`` with an inverted microscope, ``1`` by default.
    real : bool, optional
        If ``True`` (default), extract the features in real units, else in voxels.
    extract_dual : bool, optional
        If ``True`` construct the dual graph with cell edges and vertices.
        Else, construct only primal graph, that is the neighborhood graph.
    wall_area_threshold : float
        The minimum real contact area with two cells to be defined as neighbors. No minimum by default.
        Defining this value and leaving `epidermis_area_threshold` to ``None`` will lead to using this value for both.
    epidermis_area_threshold : float
        The minimum real contact area with the background necessary to be defined as epidermal cell.
        No minimum by default.
    voxel_distance_from_margin : int
        The distance, in voxels, from the stack margin to use to define cells at the stack margins. ``2`` by default.
    csv_dir : str
        The path to the directory where to load/export intermediate spatial CSV files.
    force : bool
        If ``True`` (default ``False``) force the computation of the static TissueGraph even if found in the ``csv_dir``.

    Returns
    -------
    timagetk.graphs.TemporalTissueGraph
        The temporal tissue graph.

    See Also
    --------
    timagetk.tasks.tissue_graph.tissue_graph_from_image
    timagetk.tasks.features.CELL_FEATURES
    timagetk.tasks.features.WALL_FEATURES
    timagetk.tasks.features.TEMPORAL_FEATURES

    Notes
    -----
    Have a look at ``timagetk.tasks.tissue_graph.tissue_graph_from_image`` for additional keyword arguments!
    The topologocal element coordinates are:
      - the cells' barycenter (node of primal graph)
      - the cell walls geometric median coordinates (edge of primal graph)
      - the cell vertices coordinates (node of dual graph)
      - the cell edge geometric median coordinates (edge of dual graph)

    Examples
    --------
    >>> from timagetk.tasks.temporal_tissue_graph import temporal_tissue_graph_from_images
    >>> from timagetk.tasks.temporal_tissue_graph import fused_tissue_graph
    >>> from timagetk.io.util import shared_folder
    >>> from timagetk.io.dataset import shared_data
    >>> # Example #1 - Create a TemporalTissueGraph from pre-computed TissueGraphs saved as CSVs:
    >>> tissues = shared_dataset("p58",'segmented')
    >>> lineages = shared_dataset("p58",'lineage')
    >>> time_points = shared_dataset("p58",'time-points')
    >>> ttg = temporal_tissue_graph_from_images(tissues,lineages,time_points,temp_features=None,csv_dir=shared_folder("p58"))
    >>> ttg.log_relative_value('volume',1)

    >>> from timagetk.tasks.temporal_tissue_graph import temporal_tissue_graph_from_images
    >>> from timagetk.tasks.temporal_tissue_graph import fused_tissue_graph
    >>> from timagetk.io.dataset import shared_data
    >>> tissues = shared_dataset("p58",'segmented')[:2]
    >>> lineages = shared_dataset("p58",'lineage')[:1]
    >>> time_points = shared_dataset("p58",'time-points')[:2]
    >>> ttg = temporal_tissue_graph_from_images(tissues,lineages,time_points,features=['area', 'volume', 'epidermis_growth', 'cell_growth'])

    >>> from timagetk.tasks.temporal_tissue_graph import epidermis_deformation
    >>> ttg = epidermis_deformation(ttg)
    >>> ep_ldmks = tg.cell_property_dict('epidermis_landmarks')

    >>> from timagetk.tasks.temporal_tissue_graph import cell_deformation
    >>> ttg = cell_deformation(ttg)
    >>> ldmks = tg.cell_property_dict('landmarks')
    >>> ldmks[4][0]

    >>> from timagetk import TissueImage3D
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.io import imread
    >>> tissues.append(TissueImage3D(imread(shared_data('sphere_membrane_0.0_seg.inr.gz', 'sphere')), background=1, not_a_label=0))
    >>> tissues.append(apply_trsf())
    >>> pc = tissue.pointel_coordinates()
    >>> list(pointel_unique_coordinate(pc[(23, 37, 93, 933)]))
    >>> list(pointel_unique_coordinate(pc[(20, 253, 426, 748)]))

    """
    from timagetk.io.graph import to_csv
    from timagetk.third_party.ctrl.io import read_lineage

    real = kwargs.get('real', True)
    csv_dir = kwargs.get('csv_dir', None)
    force = kwargs.get('force', False)
    extract_dual = kwargs.get('extract_dual', False)
    kwargs['extract_dual'] = extract_dual  # for ``tissue_graph_from_image``
    t_unit = kwargs.get('time_unit', "h")

    n_tissues = len(tissues)
    try:
        assert n_tissues - 1 == len(lineages)
    except AssertionError:
        raise ValueError(f"Not the correct number of tissues ({n_tissues}) and lineage ({len(time_points)})!")
    try:
        assert n_tissues == len(time_points)
    except AssertionError:
        raise ValueError(f"Not the same number of tissues ({n_tissues}) and time-points ({len(time_points)})!")

    if cells is None:
        cells = [None] * n_tissues
    else:
        try:
            assert n_tissues == len(cells)
        except AssertionError:
            raise ValueError(f"Not the same number of tissues ({n_tissues}) and cell lists ({len(cells)})!")

    # Initialize an EMPTY temporal tissue graph:
    ttg = kwargs.get('graph', TemporalTissueGraph())
    log.info(f"Starting TemporalTissueGraph generation from a time-series of {n_tissues} tissue image...")
    t_start = time.time()  # create a timer

    csv_dict = {}
    # Build TissueGraphs from TissueImages & add them to the TemporalTissueGraph:
    for t_idx, tissue in enumerate(tissues):
        tg = None
        tp = time_points[t_idx]

        # Check if the TTG has the TissueGraph from corresponding time-point:
        if tp in ttg._tissue_graph:
            log.info(f"Found existing graph corresponding to tissue from time-index {t_idx} (t{tp}h).")
            continue

        # Try to load the TissueGraph from the CSV dir (except it is required to 'force computation'):
        if csv_dir is not None and not force:
            if isinstance(tissue, (str, pathlib.Path)):
                fname = tissue
            else:
                fname = tissue.filename
            _, basename, ext = get_pne(fname)
            basename = basename[:-len(ext)]  # remove extension
            # Check all required CSV exists prior to loading attempt:
            csv_suffixes = CSV_EXPORT_SUFFIXES if extract_dual else CSV_EXPORT_SUFFIXES[:2]
            csv_names = [join(csv_dir, basename + f'_{suf}.csv') for suf in csv_suffixes]
            if all(exists(csv_name) for csv_name in csv_names):
                log.info(f"Trying to load t{t_idx} (+{tp}{t_unit}) TissueGraph from saved CSVs...")
                tg = tissue_graph_from_csv(csv_names)
            else:
                log.warning(f"Could not find all required CSV, TissueGraph will be created!")
                for csv_name in csv_names:
                    if exists(csv_name):
                        log.info(f"Found '{csv_name}'")
                    else:
                        log.info(f"Missing '{csv_name}'")

        # If we could not load the TissueGraph, or it is required to 'force computation'
        if tg is None or force:
            # Load the image if necessary
            if isinstance(tissue, (str, pathlib.Path)):
                log.info(f"Loading file '{tissue}'...")
                bkgd_id = kwargs.get('background', 1)
                nal = kwargs.get('not_a_label', 0)
                tissue = imread(tissue, TissueImage3D, background=bkgd_id, not_a_label=nal)
            # Create the static tissue graph from the image:
            tg = tissue_graph_from_image(tissue, cells[t_idx], coordinates, features, wall_features, **kwargs)
            # Export the static tissue graph:
            if csv_dir is not None:
                _, basename, _ = get_pne(tissue.filename)
                csv_name = join(csv_dir, basename)
                log.info(f"Exporting t{t_idx} (+{tp}{t_unit}) TissueGraph as CSV to directory `{csv_dir}`!")
                csv_dict[t_idx] = to_csv(tg, csv_name, export_dual=extract_dual)

        if tg is None:
            raise ValueError(f"Could not load nor creates the tissue graph from image {t_idx} (+{tp}{t_unit})!")
        else:
            # Add the static tissue graph to the temporal tissue graph:
            ttg.add_tissue_graph(tg, time_points[t_idx])

    # Add lineages to "temporally connect" TissueGraphs cells:
    log.info("Adding cell lineages between the TissueGraphs...")
    for n, lin in enumerate(lineages):
        log.info(f"Adding t{n} to t{n + 1} cell lineages...")
        if isinstance(lin, (str, pathlib.Path)):
            try:
                lin = read_lineage(lin, fmt=kwargs.get('lineage_format', 'basic'))
            except ValueError:
                lin = read_lineage(lin, fmt='marsalt')
            lineages[n] = lin
        missing_anc_nodes, missing_des_nodes = ttg.add_lineage(n, lin)
        if missing_anc_nodes != []:
            log.warning(f"{len(missing_anc_nodes)} lineaged ancestor cells where not found in the TissueGraph!")
        if missing_des_nodes != []:
            log.warning(f"{len(missing_des_nodes)} lineaged descendant cells where not found in the TissueGraph!")

    from timagetk.tasks.features import TEMPORAL_FEATURES
    if temp_features is None:
        temp_features = []
    elif temp_features == 'all':
        temp_features = TEMPORAL_FEATURES
    else:
        temp_features_set = set(temp_features) & set(TEMPORAL_FEATURES)
        if len(temp_features_set) == 0:
            log.warning("None of the specified temporal features were found in the list of available features!")
            log.info(f"Specified features: {temp_features}")
        temp_features = temp_features_set

    if len(temp_features) != 0:
        log.info(f"Starting computation of {len(temp_features)} temporal features:")
        log.info(f"{', '.join(temp_features)}")

    # - Temporal differentiation features:
    for feat in TEMPORAL_DIFF_FEATURES:
        if feat not in temp_features:
            continue
        log.info(f"Compute & add {feat} feature...")
        try:
            _ = eval(f"ttg.{feat}")
        except:
            log.error(f"Could not find or evaluate temporal property '{feat}' in the graph!")
        temp_features.remove(feat)

    if 'division_rate' in temp_features:
        ttg = add_division_rate(ttg)
        temp_features.remove('division_rate')

    if len(temp_features) != 0:
        log.info("Extracting fused cells coordinates features...")
        ttg = fused_tissue_graph(ttg, tissues, lineages, real=real)

    if 'epidermis_growth' in temp_features:
        log.info("Extracting landmarks for epidermal cell-wall deformation computation...")
        ttg = epidermis_deformation(ttg)
        ttg = epidermis_growth(ttg, **kwargs)
        temp_features.remove('epidermis_growth')

    if 'cell_growth' in temp_features:
        log.info("Extracting landmarks for cell deformation computation...")
        ttg = cell_deformation(ttg)
        ttg = cell_growth(ttg, **kwargs)
        temp_features.remove('cell_growth')

    elapsed_time = time.time() - t_start
    if elapsed_time > 3600:
        elapsed_time = time.strftime('%H hour %M min %S sec', time.gmtime(elapsed_time))
    else:
        elapsed_time = time.strftime('%M min %S sec', time.gmtime(elapsed_time))
    log.info(f"Done extracting the TemporalTissueGraph in {elapsed_time}!")
    return ttg


def fused_tissue_graph(temp_tissue_graph, tissues, lineages, **kwargs):
    """Extract "fused cell" features required for deformation features computation.

    Parameters
    ----------
    temp_tissue_graph : timagetk.graphs.TemporalTissueGraph
        The temporal tissue graph to populate with "fused cell" features required for deformation features computation.
    tissues : list of str or list[timagetk.TissueImage3D]
        The list of segmented tissues to transform.
    lineages : list of str or list[timagetk.third_party.ctrl.lineage.Lineage]
        The list of lineages to use, should be of length ``len(tissues)-1``.

    Other Parameters
    ----------------
    real : bool, optional
        If ``True`` (default), extract the features in real units, else in voxels.

    Returns
    -------
    timagetk.graphs.TemporalTissueGraph
        The populated temporal tissue graph.

    """
    from timagetk.third_party.ctrl.io import read_lineage

    ttg = temp_tissue_graph
    real = kwargs.get('real', True)
    wall_area_threshold = kwargs.get('wall_area_threshold', None)
    epidermis_area_threshold = kwargs.get('epidermis_area_threshold', wall_area_threshold)
    exclude_marginal_cells = kwargs.get('exclude_marginal_cells', False)
    v_dist_from_margin = kwargs.get('voxel_distance_from_margin', 2)

    for t, tissue in enumerate(tissues[1:]):
        # Make sure the provided tissues are indeed TissueImage3D instances
        if isinstance(tissue, (str, pathlib.Path)):
            bkgd_id = kwargs.get('background', 1)
            nal = kwargs.get('not_a_label', 0)
            tissue = imread(tissue, TissueImage3D, background=bkgd_id, not_a_label=nal)
        log.info(f"Working on {tissue.filename}...")
        # Make sure the provided lineages are indeed Lineage instances
        lin = lineages[t]
        if isinstance(lin, (str, pathlib.Path)):
            lin = read_lineage(lin)
        mapping = lin.revert()  # revert the lineage to get the relabelling mapping
        n_mapped = len(mapping)
        mapping[1] = 1  # add 'background mapping' (to avoid clearing it)
        log.info(f"Relabelling {n_mapped} lineaged daughter cells & keep background...")
        tissue = tissue.relabelling_cells_from_mapping(mapping, clear_unmapped=True)
        tg = ttg._tissue_graph[t]
        # - ADD CELLS
        # Get cell ids and add them to the tissue graph:
        log.info("Detecting cells...")
        cids = tissue.cell_ids(tg.cell_ids())
        log.info("Detecting cells at stack margins...")
        marginal_cells = labels_at_stack_margins(tissue, v_dist_from_margin)
        marginal_cells = list(set(marginal_cells) - {tissue.background})
        log.info(
            f"Detected {len(marginal_cells)} cells at less than {v_dist_from_margin} voxels from the stack margins!")
        if exclude_marginal_cells:
            cell_ids = list(set(cell_ids) - set(marginal_cells))

        # - Compute & save the fused cell barycenter coordinates:
        bary = tissue.cells.barycenter(cids, real=real)
        log.info(f"Adding {len(bary)} fused cell barycenter coordinates to t{t} TissueGraph.")
        tg.add_cell_property('fused_barycenter', bary, "µm")

        # - Compute & save the fused cell-wall geometric median coordinates:
        neighborhood = tissue.cells.neighbors(cids, min_area=wall_area_threshold, real=real)
        wall_ids = list({stuple((cid, nid)) for cid, nids in neighborhood.items() for nid in nids})
        geom_med = tissue.walls.geometric_median(wall_ids, real=real)
        log.info(f"Adding {len(geom_med)} fused cell-wall geometric median coordinates to t{t} TissueGraph.")
        tg.add_cell_wall_property('fused_median', geom_med, "µm")
        ep_cell_ids = tg.epidermal_cell_ids()
        if ep_cell_ids != []:
            ep_walls = [(tissue.background, cid) for cid in ep_cell_ids]
            if epidermis_area_threshold is not None:
                ep_walls = [ep_wall for ep_wall in ep_walls if
                            tissue.walls.area([ep_wall], real=real)[ep_wall] >= epidermis_area_threshold]
            ep_medians = tissue.walls.geometric_median(ep_walls, real=real)
            ep_medians = {cid: v for (_, cid), v in ep_medians.items()}
            log.info(
                f"Adding {len(ep_medians)} fused epidermal cell-wall geometric median coordinates to t{t} TissueGraph.")
            tg.add_cell_property('fused_epidermis_median', ep_medians, unit='µm')
        # - Compute & save the fused cell-vertices coordinates:
        v_coords = tissue.vertices.coordinates(real=real)
        log.info(f"Adding {len(v_coords)} fused cells vertex coordinate to t{t} TissueGraph.")
        tg.add_cell_vertex_property('fused_coordinate', v_coords, "µm")
        # - Compute & save the fused cell-edge geometric medians:
        ceids = tg.cell_edge_ids()
        pp2linel = tg.cell_edge_property_dict('linel_id')
        ceids = [pp2linel[k] for k in ceids if pp2linel[k] is not None]
        ce_medians = tissue.edges.geometric_median(ceids, real=real)
        # Create the dictionary allowing to transform lined ids into their corresponding pair of pointels
        linel2pp = {v: k for k, v in pp2linel.items() if v is not None}
        # Use it to be able to add cell-edge geometric medians to the graph:
        ce_ppty = {linel2pp[lid]: med for lid, med in ce_medians.items() if lid in linel2pp}
        log.info(f"Adding {len(ce_ppty)} fused cells edge coordinate to t{t} TissueGraph.")
        tg.add_cell_edge_property('fused_median', ce_ppty, "µm")

    deformation_features_summary(ttg)
    return ttg


def deformation_features_summary(ttg):
    for t in range(ttg.nb_time_points - 1):
        tg = ttg._tissue_graph[t]
        marginal_cells = []
        if "stack_margin" in tg.list_cell_properties():
            marginal_cells = [mc for mc, v in tg.cell_property_dict("stack_margin").items() if v]
            log.info(
                f"Found {len(marginal_cells)} marginal cells to exclude from epidermis landmarks computation at time {t}!")

        # Check that the cell is not a "marginal cell" and has at least one descendant:
        cids = [cid for cid in tg.cell_ids() if cid not in marginal_cells and ttg.has_descendant(t, cid)]
        if len(cids) > 0:
            v = tg.cell_property_dict('barycenter', cids, only_defined=True)
            fv = tg.cell_property_dict('fused_barycenter', cids, only_defined=True)
            n_common = len(set(v) & set(fv))
            log.info(
                f"Cell 'barycenter' (n={len(v)}) & 'fused_barycenter' (n={len(fv)}) have {n_common} common ids over {len(cids)} ({np.round(n_common / float(len(cids)) * 100, 2)}%) at time {t}!")
        else:
            log.warning(f"No cell ids at time {t} ?!")

        ep_ids = {c for c, v in tg.cell_property_dict('epidermis').items() if v} & set(cids)
        if len(ep_ids) > 0:
            v = tg.cell_property_dict('epidermis_median', ep_ids, only_defined=True)
            fv = tg.cell_property_dict('fused_epidermis_median', ep_ids, only_defined=True)
            n_common = len(set(v) & set(fv))
            log.info(
                f"Epidermal cell 'epidermis_median' (n={len(v)}) & 'fused_epidermis_median' (n={len(fv)}) have {n_common} common ids over {len(ep_ids)} ({np.round(n_common / float(len(ep_ids)) * 100, 2)}%) at time {t}!")
        else:
            log.warning(f"No epidermal cell ids at time {t} ?!")

        cw_ids = []
        for cid in cids:
            cw_ids.extend(tg.cell_wall_ids(cid))
        cw_ids = set(cw_ids)
        if len(cw_ids) > 0:
            v = tg.cell_wall_property_dict('median', cw_ids, only_defined=True)
            fv = tg.cell_wall_property_dict('fused_median', cw_ids, only_defined=True)
            n_common = len(set(v) & set(fv))
            log.info(
                f"Cell-wall 'median' (n={len(v)}) & 'fused_median' (n={len(fv)}) have {n_common} common ids over {len(cw_ids)} ({np.round(n_common / float(len(cw_ids)) * 100, 2)}%) at time {t}!")
        else:
            log.warning(f"No cell-wall ids at time {t} ?!")

        ce_ids = []
        for cid in cids:
            ce_ids.extend(tg.cell_edge_ids(cid))
        ce_ids = set(ce_ids)
        if len(ce_ids) > 0:
            v = tg.cell_edge_property_dict('median', ce_ids, only_defined=True)
            fv = tg.cell_edge_property_dict('fused_median', ce_ids, only_defined=True)
            n_common = len(set(v) & set(fv))
            log.info(
                f"Cell-edge 'median' (n={len(v)}) & 'fused_median' (n={len(fv)}) have {n_common} common ids over {len(ce_ids)} ({np.round(n_common / float(len(ce_ids)) * 100, 2)}%) at time {t}!")
        else:
            log.warning(f"No cell-edge ids at time {t} ?!")

        cv_ids = []
        for cid in cids:
            cv_ids.extend(tg.cell_vertex_ids(cid))
        cv_ids = set(cv_ids)
        if len(cv_ids) > 0:
            v = tg.cell_vertex_property_dict('coordinate', cv_ids, only_defined=True)
            fv = tg.cell_vertex_property_dict('fused_coordinate', cv_ids, only_defined=True)
            n_common = len(set(v) & set(fv))
            log.info(
                f"Cell-vertex 'coordinate' (n={len(v)}) & 'fused_coordinate' (n={len(fv)}) have {n_common} common ids over {len(cv_ids)} ({np.round(n_common / float(len(cv_ids)) * 100, 2)}%) at time {t}!")
        else:
            log.warning(f"No cell-vertex ids at time {t} ?!")
