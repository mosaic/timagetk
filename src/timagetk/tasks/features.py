#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Tasks module dedicated to physical features extraction of cellular tissues."""

import pandas as pd

from timagetk.bin.logger import get_logger
from timagetk.features.graph import DEFORMATION_FEATURES

log = get_logger(__name__)

# CELL_SEMANTIC_FEATURES = ['epidermis']
CELL_SCALAR_FEATURES = ['area', 'number_of_neighbors', 'shape_anisotropy', 'volume']
CELL_VECTOR_FEATURES = ['barycenter', 'principal_direction_norms']
CELL_TENSOR_FEATURES = ['inertia_axis']
CELL_FEATURES = CELL_SCALAR_FEATURES + CELL_VECTOR_FEATURES + CELL_TENSOR_FEATURES

WALL_SCALAR_FEATURES = ['area']
# WALL_SCALAR_FEATURES += ['gaussian_curvature', 'mean_curvature']  # TODO !
# WALL_TENSOR_FEATURES = ['curvature']  # TODO !
WALL_FEATURES = WALL_SCALAR_FEATURES

TEMPORAL_FUNC = ['division_rate']
TEMPORAL_DIFF_FUNC = ["log_relative_value", "relative_value", "rate_of_change", "relative_rate_of_change"]


def _temporal_diff_features_str(temporal_diff_func, scalar_feature, rank=1):
    from typing import Iterable
    from timagetk.features.utils import flatten

    if not isinstance(rank, int) and not isinstance(rank, Iterable):
        raise ValueError(f"Parameter rank should be an integer or an iterable, got {rank}")

    if temporal_diff_func == "division_rate":
        if isinstance(rank, int):
            return f"{temporal_diff_func}({rank})"
        else:
            return list(flatten([_temporal_diff_features_str(temporal_diff_func, None, r) for r in rank]))
    else:
        if isinstance(rank, int):
            return f"{temporal_diff_func}('{scalar_feature}',{rank})"
        else:
            return list(flatten([_temporal_diff_features_str(temporal_diff_func, scalar_feature, r) for r in rank]))


def _default_or_list(param, default):
    if param is None:
        param = default
    elif isinstance(param, str):
        param = [param]
    else:
        param = list(param)
    return list(set(param) & set(default))

def list_temporal_features(temporal_diff_func=None, scalar_feature=None, rank=1):
    """List the temporal features for given temporal ranked distance.

    Parameters
    ----------
    temporal_diff_func : list, optional
        The list of temporal feature to use.
    scalar_feature : list, optional
        The list of scalar feature to temporally differentiate.
    rank : int or list, optional
        The (list of) temporal rank(s) to temporally differentiate for.

    Examples
    --------
    >>> from timagetk.tasks.features import list_temporal_features
    >>> list_temporal_features(scalar_feature='volume')
    ["relative_value('volume',1)", "relative_rate_of_change('volume',1)", "log_relative_value('volume',1)", "rate_of_change('volume',1)", 'division_rate(1)']
    >>> list_temporal_features(temporal_diff_func='log_relative_value', scalar_feature='volume', rank=[1, 2])
    ["log_relative_value('volume',1)", "log_relative_value('volume',2)", 'division_rate(1)', 'division_rate(2)']

    """
    from timagetk.features.utils import flatten
    temporal_diff_func = _default_or_list(temporal_diff_func, TEMPORAL_DIFF_FUNC)
    scalar_feature = _default_or_list(scalar_feature, CELL_SCALAR_FEATURES)

    temporal_features = []
    temporal_features.extend(list(flatten([_temporal_diff_features_str(func, sc, rank) for func in temporal_diff_func for sc in scalar_feature])))
    temporal_features.extend(list(flatten([_temporal_diff_features_str(func, None, rank) for func in TEMPORAL_FUNC])))

    return temporal_features

TEMPORAL_DIFF_FEATURES = list_temporal_features(rank=1)
TEMPORAL_FEATURES = DEFORMATION_FEATURES + TEMPORAL_DIFF_FEATURES


def spatial_features_from_tissue(tissue, features=None):
    """Extract physical features of cellular tissues.

    Parameters
    ----------
    tissue : timagetk.TissueImage2D or timagetk.TissueImage3D
        The labelled image with a cellular tissue.
    features : list, optional
        List of features to extract. By default, all of them.

    Returns
    -------
    pandas.DataFrame
        The data frame with all the extracted features.

    Examples
    --------
    >>> from timagetk import TissueImage3D
    >>> from timagetk.tasks.features import spatial_features_from_tissue
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> img = TissueImage3D(shared_data('flower_labelled', 0), background=1, not_a_label=0)
    >>> df = spatial_features_from_tissue(tissue, ['area', 'volume'])
    >>> df.columns
    Index(['area', 'volume'], dtype='object')
    >>> df = spatial_features_from_tissue(tissue)
    >>> df.columns
    Index(['area', 'barycenter', 'inertia_axis', 'number_of_neighbors', 'shape_anisotropy', 'volume'], dtype='object')

    """
    if features is None:
        features = CELL_FEATURES

    f_dict = {}
    log.info("Computing spatial features...")
    if "area" in features:
        log.info("Computing cell area...")
        f_dict.update({'area': tissue.cells.area()})
    if "barycenter" in features:
        log.info("Computing cell barycenter...")
        f_dict.update({'barycenter': tissue.cells.barycenter()})
    if "epidermis" in features:
        log.info("Computing cell epidermis...")
        f_dict.update({'epidermis': tissue.epidermal_cell_ids()})
    if "inertia_axis" in features:
        log.info("Computing cell inertia axis...")
        f_dict.update({'inertia_axis': tissue.cells.inertia_axis()})
    if "number_of_neighbors" in features:
        log.info("Computing cell number of neighbors...")
        f_dict.update({'number_of_neighbors': tissue.cells.number_of_neighbors()})
    if "shape_anisotropy" in features:
        log.info("Computing cell shape anisotropy...")
        f_dict.update({'shape_anisotropy': tissue.cells.shape_anisotropy()})

    if "volume" in features and tissue.is3D():
        log.info("Computing cell volume...")
        f_dict.update({'volume': tissue.cells.volume()})

    df = pd.DataFrame()
    return df.from_dict(f_dict)


def features_from_graph(graph, features=None, wall_features=None):
    """Export features from a tissue graph to a pandas ``DataFrame``.

    Parameters
    ----------
    tissue : timagetk.graphs.temporal_tissue_graph.TissueGraph
        A temporal tissue graph with features to export to DataFrame.
    features : list, optional
        List of cell features to export. By default, all of them.
    wall_features : list, optional
        List of cell-wall features to export. By default, all of them.

    Returns
    -------
    pandas.DataFrame
        The data frame with all the extracted features.

    Examples
    --------
    >>> from timagetk.graphs.tissue_graph import TissueGraph
    >>> from timagetk import TissueImage3D
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> img = imread(shared_dataset("p58", "segmented")[0])
    >>> tissue = TissueImage3D(img, background=1, not_a_label=0)
    >>> tg = TissueGraph(tissue)
    >>> from timagetk.tasks.features import features_from_graph
    >>> df = features_from_graph(tg)
    >>> df.columns
    Index(['division_rate', 'log_relative_value('volume')'], dtype='object')

    """
    if features is None:
        features = graph.list_cell_properties()
    if wall_features is None:
        features = graph.list_cell_wall_properties()

    f_dict = {}
    log.info("Gathering cell features...")

    for feat in features:
        if feat in graph.list_cell_properties():
            f_dict.update({feat: graph.cell_property_dict(feat)})
        else:
            log.error(f"Don't know what to do with '{feat}'!")

    df = pd.DataFrame()
    return df.from_dict(f_dict)


def features_from_temporal_graph(temporal_graph, features=None, wall_features=None):
    """Export features from a temporal tissue graph to a pandas ``DataFrame``.

    Parameters
    ----------
    tissue : timagetk.graphs.TemporalTissueGraph
        A temporal tissue graph with features to export to DataFrame.
    features : list, optional
        List of cell features to export. By default, all of them.
    wall_features : list, optional
        List of cell-wall features to export. By default, all of them.

    Returns
    -------
    pandas.DataFrame
        The data frame with all the extracted features.

    Examples
    --------
    >>> from timagetk.tasks.temporal_tissue_graph import temporal_tissue_graph_from_images
    >>> from timagetk.tasks.temporal_tissue_graph import fused_tissue_graph
    >>> from timagetk.io.dataset import shared_data
    >>> tissues = shared_dataset("p58",'segmented')[:2]
    >>> lineages = shared_dataset("p58",'lineage')[:1]
    >>> tp = shared_dataset("p58",'time-points')[:2]
    >>> ttg = temporal_tissue_graph_from_images(tissues,lineages,tp,features=['area', 'volume', 'epidermis_growth', 'cell_growth'])
    >>> from timagetk.tasks.features import features_from_temporal_graph
    >>> cdf, wdf = features_from_temporal_graph(ttg, features='all')
    >>> cdf.columns
    Index(['division_rate', 'log_relative_value('volume')'], dtype='object')

    """
    all_features = temporal_graph.list_cell_properties()
    all_wall_features = temporal_graph.list_cell_wall_properties()
    if features == 'all':
        features = all_features
    if wall_features == 'all':
        wall_features = all_wall_features
    log.info(f"Got a temporal graph with {len(all_features)} cell features: {all_features}")
    log.info(f"Got a temporal graph with {len(all_wall_features)} cell-wall features: {all_wall_features}")

    cf_dict, wf_dict = {}, {}
    cdf, wdf = None, None
    if features is not None:
        log.info(f"Gathering {len(features)} cell features...")
        cf_dict.update({'neighbors': {(t, cid): nei for t in range(temporal_graph.nb_time_points) for cid, nei in
                                      temporal_graph._tissue_graph[t].cell_neighbors_dict().items()}})
        for feat in features:
            if feat in temporal_graph.list_cell_properties():
                cf_dict.update({feat: temporal_graph.cell_property_dict(feat)})
            elif any(feat.startswith(f) for f in TEMPORAL_DIFF_FUNC):
                log.info(f"Computing {feat}...")
                cf_dict.update({feat: eval(f"temporal_graph.{feat}")})
            else:
                try:
                    cf_dict.update({feat: eval(f"temporal_graph.{feat}")})
                except:
                    log.error(f"Don't know what to do with cell feature: '{feat}'!")
                else:
                    log.info(f"Computing {feat}...")
        # Make a pandas.DataFrame out of the cell features dictionary:
        cdf = pd.DataFrame()
        cdf = cdf.from_dict(cf_dict)

    if wall_features is not None:
        log.info(f"Gathering {len(wall_features)} wall features...")
        for feat in features:
            if feat in temporal_graph.list_cell_wall_properties():
                wf_dict.update({feat: temporal_graph.cell_wall_property_dict(feat)})
            elif any(feat.startswith(f) for f in TEMPORAL_DIFF_FUNC):
                log.info(f"Computing {feat}...")
                wf_dict.update({feat: eval(f"temporal_graph.{feat}")})
            else:
                log.error(f"Don't know what to do with wall feature: '{feat}'!")
        # Make a pandas.DataFrame out of the wall features dictionary:
        wdf = pd.DataFrame()
        wdf = wdf.from_dict(wf_dict)

    return cdf, wdf
