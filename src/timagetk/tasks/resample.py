#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

def resampling(image, template_img=None, voxelsize=None, shape=None, interpolation=None, **kwargs):
    """Same than timagetk.algorithms.resample.resample but for SpatialImage or LabelledImage or MultiChannelImage

    Parameters
    ----------
    image
    template_img
    voxelsize
    shape
    interpolation
    kwargs

    Returns
    -------

    """
    # TODO ??
    pass
