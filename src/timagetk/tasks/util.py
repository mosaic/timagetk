#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

from timagetk.bin.logger import get_logger
from timagetk.components.multi_channel import MultiChannelImage
from timagetk.components.spatial_image import SpatialImage
from timagetk.third_party.vt_parser import BLOCKMATCHING_METHODS
from timagetk.third_party.vt_parser import DEF_BM_METHOD
from timagetk.util import _method_check

log = get_logger(__name__)


def check_list_images(list_images):
    # - Check list_images type:
    try:
        assert isinstance(list_images, list)
    except AssertionError:
        raise TypeError(f"Parameter 'list_images' should be of type 'list', but is: {type(list_images)}")

    # - Check SpatialImage consecutive:
    try:
        assert all(isinstance(img, (SpatialImage, MultiChannelImage)) for img in list_images)
    except AssertionError:
        msg = "Parameter 'list_images' should be a list of SpatialImages!"
        raise TypeError(msg)


def check_multi_channels(list_images, **kwargs):
    # If one MultiChannelImage is given
    # Assert all of them are MultiChannelImage
    try:
        assert all(isinstance(img, MultiChannelImage) for img in list_images)
    except AssertionError:
        raise ValueError("All images should be of same type!")
    # Check we have the 'reference_channel' kwargs
    try:
        channel = kwargs.get('reference_channel', '')
        assert channel != ''
    except AssertionError:
        raise ValueError("Unset 'reference_channel' keyword argument, mandatory with `MultiChannelImage` instances!")
    # Check all MultiChannelImage have the requested reference channel
    try:
        assert all(channel in img.get_channel_names() for img in list_images)
    except AssertionError:
        log.info(f"Found channel names: {[(img.filename, img.get_channel_names()) for img in list_images]}")
        raise ValueError("Could not find requested channel in all MultiChannelImage!")
    list_images = [img.get_channel(channel) for img in list_images]
    return list_images


def check_registrations(list_images, method):
    check_list_images(list_images)

    # - Check SpatialImage consecutive length, this function is useless if length < 3!
    try:
        assert len(list_images) >= 3
    except AssertionError:
        msg = "Parameter 'list_images' should have a minimum length of 3!"
        raise ValueError(msg)

    if any(isinstance(img, MultiChannelImage) for img in list_images):
        list_images = check_multi_channels(list_images)

    # - Set method if None or check it is a valid method:
    method = _method_check(method, BLOCKMATCHING_METHODS, DEF_BM_METHOD)

    return list_images, method
