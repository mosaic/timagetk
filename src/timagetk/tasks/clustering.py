#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Tasks module dedicated to clustering of physical features of cellular tissues."""

from timagetk.bin.logger import get_logger
from timagetk.gui.vtk_views import vtk_viewer

log = get_logger(__name__)


def cluster_label_dict(lclustering):
    """Create a cluster-based clustering dictionary from a label-based clustering dictionary.

    Parameters
    ----------
    lclustering : dict
        A label-based clustering dictionary as {label: cluster}

    Returns
    -------
    dict
        A cluster-based clustering dictionary as {cluster: [labels]}

    Examples
    --------
    >>> from timagetk.tasks.clustering import cluster_label_dict
    >>> clustering = {1:0, 2:1, 3:0, 4:0, 5:1, 6:1, 7:0}
    >>> cluster_label_dict(clustering)
    {0: [1, 3, 4, 7], 1: [2, 5, 6]}

    """
    cclustering = {}
    for k, v in lclustering.items():
        try:
            cclustering[v].append(k)
        except:
            cclustering[v] = [k]

    return cclustering


def cost_function(ids_1, ids_2, similarity=False):
    """The matching elements cost function (dissimilarity):

    Parameters
    ----------
    ids_1, ids2 : list or set
        List or set of id-like elements belonging to a cluster
    similarity : bool
        If ``True``, return similarity function instead of dissimilarity.

    Returns
    -------
    float
        The dissimilarity or similarity of the two sets.

    Notes
    -----
    The cost function is defined as follows: :math:`D(a,b) = 1 - (2* intersect(a,b)) / (N_a + N_b)`,
    where :math:`N_a` and :math:`N_b` are the number of elements in ensembles :math:`a` and :math:`b`.

    Examples
    --------
    >>> from timagetk.tasks.clustering import cost_function
    >>> ids_1 = [2, 3, 5, 8, 9]
    >>> ids_2 = [2, 4, 5, 7, 9]
    >>> cost_function(ids_1, ids_2)
    0.4
    >>> cost_function(ids_1, ids_2, similarity=True)
    0.6
    >>> cost_function(ids_1, [1])
    1.

    """
    if isinstance(ids_1, list):
        ids_1 = set(ids_1)
    if isinstance(ids_2, list):
        ids_2 = set(ids_2)
    if similarity:
        return float(2 * len(ids_1 & ids_2)) / (len(ids_1) + len(ids_2))
    else:
        return 1 - float(2 * len(ids_1 & ids_2)) / (len(ids_1) + len(ids_2))


def ensemble_cost_function(cclustering_1, cclustering_2, similarity=False):
    """Cost function between two clusters relating to the number of common ids in them.

    Parameters
    ----------
    cclustering_1 : dict
        A cluster-based clustering dictionary, ``{cluster_id: [labels]}``.
    cclustering_2 : dict
        A cluster-based clustering dictionary, ``{cluster_id: [labels]}``.
    similarity : bool
        If ``True``, return similarity function instead of dissimilarity.

    Returns
    -------
    dict of float
        Cost dictionary by pair of overlapping clusters

    Notes
    -----
    Costs are returned only for cluster with overlapping set of ids.

    Examples
    --------
    >>> from timagetk.tasks.clustering import ensemble_cost_function
    >>> clustering1 = {0: [1, 2, 3, 5, 8], 1: [4, 6, 7, 9, 10], 2: [11, 12, 13, 14, 15]}
    >>> clustering2 = {0: [1, 2, 4, 5, 7], 1: [3, 6, 8, 9, 10], 2: [11, 12], 3: [13, 14, 15]}
    >>> ensemble_cost_function(clustering1, clustering2)
    {(0, 0): 0.4, (0, 1): 0.6, (1, 0): 0.6, (1, 1): 0.4, (2, 2): 0.4285714285714286, (2, 3): 0.25}

    """
    cost = {}
    for clusters_1, ids_1 in cclustering_1.items():
        for clusters_2, ids_2 in cclustering_2.items():
            if set(ids_1) & set(ids_2) != set():
                cost[clusters_1, clusters_2] = cost_function(ids_1, ids_2, similarity)

    return cost


def cluster_matching(clustering_1, clustering_2, return_all=False):
    """Bipartite matching of clusters based on a Minimum cost flow algorithm over their ids composition.

    Parameters
    ----------
    clustering_1, clustering_2 : dict
        A label-based clustering dictionary, {label: cluster}
    return_all : bool
        If ``True``, return score and unassociated groups

    Examples
    --------
    >>> from timagetk.tasks.clustering import ensemble_cost_function
    >>> from timagetk.tasks.clustering import cluster_label_dict
    >>> clustering_1 = {1:0, 2:0, 3:0, 4:1, 5:0, 6:1, 7:1, 8:0, 9:1, 10:1, 11:2, 12:2, 13:2, 14:2, 15:2}
    >>> clustering_2 = {1:0, 2:0, 3:1, 4:0, 5:0, 6:1, 7:0, 8:1, 9:1, 10:1, 11:2, 12:2, 13:3, 14:3, 15:3}
    >>> ensemble_cost_function(clustering_1, clustering_2)

    """
    from timagetk.third_party.ctrl.matching_flow_graph import MatchingFlowGraph
    cclustering_1 = cluster_label_dict(clustering_1)
    cclustering_2 = cluster_label_dict(clustering_2)

    mfg = MatchingFlowGraph(ensemble_cost_function(cclustering_1, cclustering_2), method='padfield', max_cost=1,
                            appear_disappear_edge=True)
    match = mfg.optimize_flow()
    mfg.get_max_flow_dict()

    if return_all:
        return match
    else:
        return match[1]


def clustering_projection(segmented_images, clustering, filename=None, **kwargs):
    """Creates 3D projection of clustering onto segemented images.

    Parameters
    ----------
    segmented_images : list
        The list of segemented image (loaded in memory or file names).
    clustering : dict
        The clustering dictionary to project.
    filename : list of str
        If defined, should be a list of valid filemanes to save figures.

    Examples
    --------
    >>> from timagetk.components.clustering import example_clusterer
    >>> from timagetk.tasks.clustering import clustering_projection
    >>> from timagetk.io.dataset import shared_data
    >>> ds_name = "p58"
    >>> clust = example_clusterer(ds_name)
    >>> clust.assemble_matrix(['topological', 'area', "log_relative_value('volume',1)"], [0.2, 0.4, 0.4])
    >>> clust.compute_clustering(method="dbscan", eps=10)
    >>> print(f"Found {clust._nb_clusters} clusters!")
    >>> clustering_projection(shared_dataset(ds_name,'segmented'), clust._clustering)

    """
    for n, seg_img in enumerate(segmented_images):
        vtk_viewer(seg_img, clustering[n], filename=filename[n], **kwargs)


class ClustererComparison:
    """Class used to compare different clustering, notably to find matching cluster based on their ids composition."""

    def __init__(self, clustering_1, clustering_2):
        # -- Initialisation:
        if clustering_1.__class__.__name__ == 'ClustererChecker':
            self.clustering_1 = dict(zip(clustering_1.ids, clustering_1._clustering))
        else:
            assert isinstance(clustering_1, dict)
            self.clustering_1 = clustering_1
        if clustering_2.__class__.__name__ == 'ClustererChecker':
            self.clustering_2 = dict(zip(clustering_2.ids, clustering_2._clustering))
        else:
            assert isinstance(clustering_2, dict)
            self.clustering_2 = clustering_2

        # -- Matching clusters:
        log.info("Matching clusters by vertex-ids composition using a minimum-cost flow algorithm...")
        matching = cluster_matching(self.clustering_1, self.clustering_2, True)
        self.matching_score = matching[0]
        log.info(f"Total cost of the flow: {self.matching_score}")
        self.matched_clusters = matching[1]
        log.info(f"Matched clusters: {self.matched_clusters}")
        self.unmatched_clusters_1 = matching[2]
        self.unmatched_clusters_2 = matching[3]
        if self.unmatched_clusters_1 != []:
            self.all_matched_1 = False
            log.info(f"Unmatched clusters from `clustering_1`: {self.unmatched_clusters_1}")
        else:
            self.all_matched_1 = True
            log.info("All clusters from `clustering_1` have been matched !")
        if self.unmatched_clusters_2 != []:
            self.all_matched_2 = False
            log.info(f"Unmatched clusters from `clustering_2`: {self.unmatched_clusters_2}")
        else:
            self.all_matched_2 = True
            log.info("All clusters from `clustering_2` have been matched !")

    def relabelling_dictionary(self, clustering_id=2):
        """Compute a relabeling dictionary, to be further used to enumerate clusters ids.

        Parameters
        ----------
        clustering_id: {1, 2}
            Gives the "id" of the clustering to relabel from cluster element matching to the other one. 
        """
        if clustering_id == 1:
            relabelling_dict = dict(self.matched_clusters)
            all_matched = self.all_matched_1
            if not all_matched:
                left_id = set(range(len(self.clustering_1.values()))) - set(self.clustering_2.values())
                unmatched_clusters = self.unmatched_clusters_1
        else:
            relabelling_dict = {id2: id1 for id1, id2 in self.matched_clusters}
            all_matched = self.all_matched_2
            if not all_matched:
                left_id = set(range(len(self.clustering_2.values()))) - set(self.clustering_1.values())
                unmatched_clusters = self.unmatched_clusters_2

        if not all_matched:
            for k, n in zip(unmatched_clusters, left_id):
                relabelling_dict.update({k: n})

        return relabelling_dict

    def relabel_clustering_1(self):
        """Return a relabelled version of the 1st clustering dictionary (self.clustering_1)."""
        relabelling_dict = self.relabelling_dictionary(1)
        return {vid: relabelling_dict[cid] for vid, cid in self.clustering_1.items()}

    def relabel_clustering_2(self):
        """Return a relabelled version of the 2nd clustering dictionary (self.clustering_2)."""
        relabelling_dict = self.relabelling_dictionary(2)
        return {vid: relabelling_dict[cid] for vid, cid in self.clustering_2.items()}
