#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""This module regroups decorators modifying the behavior of algorithms to support multiple input types and not just SpatialImage."""
from functools import wraps

import numpy as np
from timagetk.bin.logger import get_logger
from timagetk.components.labelled_image import LabelledImage
from timagetk.components.multi_channel import MultiChannelImage
from timagetk.components.spatial_image import SpatialImage
from timagetk.util import clean_type

log = get_logger(__name__)

def int16_wrapper(func):
    """This decorator convert int16 image to uint16 for processing then convert back to int16."""

    @wraps(func)
    def wrapper(image, *args, **kwargs):
        if image.dtype == np.int16:
            from timagetk.array_util import uint16_to_int16
            from timagetk.array_util import int16_to_uint16
            from timagetk.components.image import get_image_class
            from timagetk.components.image import get_image_attributes
            Image = get_image_class(image)
            log.warning(f"Using '{image.dtype.name}' data with '{func.__name__}' is not supported by `vt` library!'")
            log.info("Attempting temporary conversion to 'uint16' during processing.")
            image_attr = get_image_attributes(image, exclude=['dtype'])
            img_uint16, offset = int16_to_uint16(image)
            return Image(uint16_to_int16(func(Image(img_uint16, **image_attr), *args, **kwargs), offset), **image_attr)
        else:
            return func(image, *args, **kwargs)

    return wrapper


def multichannel_wrapper(func):
    """This decorator apply the function to a given `channel` or to all channels of a MultiChannelImage."""

    @wraps(func)
    def wrapper(image, *args, **kwargs):
        if isinstance(image, (SpatialImage, LabelledImage)):
            return func(image, *args, **kwargs)
        elif isinstance(image, MultiChannelImage) and 'channel' in kwargs:
            return func(image.get_channel(kwargs.pop('channel')), *args, **kwargs)
        elif isinstance(image, MultiChannelImage):
            channel_names = image.get_channel_names()
            for channel in channel_names:
                image[channel] = func(image.get_channel(channel), *args, **kwargs)
            image.update_attributes(image, channel_names)
            return image
        else:
            raise TypeError(f"Unknown type of image ({clean_type(image)}) to use with `{func.__name__}`!")

    return wrapper


def singlechannel_wrapper(func):
    """This decorator apply the function to a single channel of a MultiChannelImage."""

    @wraps(func)
    def wrapper(image, *args, **kwargs):
        if isinstance(image, (SpatialImage, LabelledImage)):
            return func(image, *args, **kwargs)
        elif isinstance(image, MultiChannelImage):
            try:
                assert 'channel' in kwargs
            except AssertionError:
                raise ValueError("You have to specify the `channel` you want to use!")
            else:
                return func(image.get_channel(kwargs.pop('channel')), *args, **kwargs)
        else:
            raise TypeError(f"Unknown type of image ({clean_type(image)}) to use with `{func.__name__}`!")

    return wrapper


def dual_singlechannel_wrapper(func):
    """This decorator apply the function to a single channel of two MultiChannelImage."""

    @wraps(func)
    def wrapper(floating_image, reference_image, *args, **kwargs):
        if isinstance(floating_image, SpatialImage) and isinstance(reference_image, SpatialImage):
            return func(floating_image, reference_image, *args, **kwargs)
        elif isinstance(floating_image, MultiChannelImage) and isinstance(reference_image, MultiChannelImage):
            try:
                assert 'channel' in kwargs
            except AssertionError:
                raise ValueError("You have to specify the `channel` you want to use!")
            else:
                channel = kwargs.pop('channel')
                return func(floating_image.get_channel(channel),
                            reference_image.get_channel(channel), *args, **kwargs)
        else:
            raise TypeError(
                f"Unknown type of floating ({clean_type(floating_image)}) and reference image ({clean_type(reference_image)}) to use with `{func.__name__}`!")

    return wrapper
