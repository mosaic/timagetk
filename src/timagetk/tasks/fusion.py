#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Images Fusion Module.

This module contains functionality for image fusion by aligning and processing multi-angle or intensity images.
It leverages rigid and non-linear registrations to propose advanced image fusion techniques.
Transformations are computed, and applied to register various images on a chosen reference image.

The primary features include initial transformation graph creation, iterative and non-iterative image fusion,
and transformation averaging for image alignment.

Core functionalities include:
 - Image Registration and Fusion: Align and fuse multiple images on a reference image using methods like 'mean', with options for affine and vector-field-based non-linear transformations.
 - Iterative Fusion: Perform multistep iterative alignment and fusion for improved accuracy.
 - Transformation Management: Construction and maintenance of transformation graphs with support for linear and non-linear transformations.
 - Voxel Manipulation: Support for isometric resampling with specified voxel sizes and super-resolution reconstruction.
 - Masking and Averaging: Advanced averaging methods, including global and local averaging based on defined image masks.

This module is particularly suited for tasks requiring precise multi-angle imaging alignment, such as time-series
or multi-view image analysis. It focuses on supporting customized pipelines with a balance of performance and accuracy.
"""

import time
from copy import copy

import numpy as np

from timagetk.algorithms.averaging import images_averaging
from timagetk.algorithms.blockmatching import blockmatching
from timagetk.algorithms.resample import isometric_resampling
from timagetk.algorithms.resample import resample
from timagetk.algorithms.template import iso_template
from timagetk.algorithms.trsf import apply_trsf
from timagetk.algorithms.trsf import compose_trsf
from timagetk.algorithms.trsf import create_trsf
from timagetk.algorithms.trsf import inv_trsf
from timagetk.algorithms.trsf import trsfs_averaging
from timagetk.bin.logger import get_logger
from timagetk.components.image import get_image_attributes
from timagetk.components.spatial_image import SpatialImage
from timagetk.io.util import find_common_segments
from timagetk.io.util import guess_delimiter
from timagetk.util import elapsed_time

log = get_logger(__name__)


def fusion_on_reference(images, method="mean", init_trsfs=None, ref_img_idx=0, **kwargs):
    """Fuse images by registering them on the reference image.

    All floating image are registered with the affine method
    If requested, a non-linear registration (vectorfield) may also be performed.

    Parameters
    ----------
    images : dict[int, timagetk.SpatialImage] or list[timagetk.SpatialImage]
        A dictionary (or a list) of intensity image to fuse together.
    method : str, optional
        An image fusion method to use, 'mean' by default, see ``AVERAGING_METHODS``
    init_trsfs : dict[(int, int), timagetk.Trsf], optional
        A 2-tuple indexed dictionary of initial linear transformations.
        It should be indexed by pairs of reference and floating images indexes, that is `(ref_img_idx, flo_img_idx)`.
    ref_img_idx : int, optional
        An index of the reference image, by default `0`.

    Other Parameters
    ----------------
    vectorfield : bool
        If ``True`` (default is ``False``), also compute a non-linear deformation to match the images.
    fuse_reference : bool
        If ``True`` (default), add the reference image to the list of images to fuse.
    final_vxs : float
        If specified, the template image will be the isometric resampling of th reference image to this voxel size float value
    super_resolution : bool
        If ``True`` (default), compute a reference frame with minimum voxel size.
    global_averaging : bool
        If ``True`` (default is ``False``), perform global averaging of each voxel by the total number of images.
        Else performs local averaging based on the number of defined imaged per voxels after registrations.

    Returns
    -------
    timagetk.SpatialImage
        The fused image.
    dict[(int, int), timagetk.Trsf]
        The dictionary of transformations to register the images on the reference image.
        The dictionary is indexed by pairs of reference and floating images indexes, that is `(ref_img_idx, flo_img_idx)`.

    See Also
    --------
    timagetk.third_party.vt_parser.AVERAGING_METHODS

    Notes
    -----
    Prior to averaging, masks are generated to average each voxel by the number of locally defined images and not the total number of images.

    Examples
    --------
    >>> from timagetk.tasks.fusion import fusion_on_reference
    >>> from timagetk.algorithms.pointmatching import pointmatching
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.stack import orthogonal_view
    >>> # Load shared multi-angle images for the first time point (t0) of 'p58' shared time-series
    >>> ma_images = {ma_im_idx: shared_data('flower_multiangle', ma_im_idx) for ma_im_idx in range(3)}
    >>> ref_idx=0
    >>> # Load shared multi-angle landmarks for the first time point (t0) of 'p58' shared time-series
    >>> ref_pts_01, flo_pts_01 = shared_data('flower_multiangle_landmarks', (0, 1))
    >>> ref_pts_02, flo_pts_02 = shared_data('flower_multiangle_landmarks', (0, 2))
    >>> # Creates manual initialization transformations with `pointmatching` algorithm:
    >>> trsf_01 = pointmatching(flo_pts_01, ref_pts_01, template_img=ma_images[ref_idx], method='rigid')
    >>> trsf_02 = pointmatching(flo_pts_02, ref_pts_02, template_img=ma_images[ref_idx], method='rigid')
    >>> init_trsfs = {(0, 1): trsf_01, (0, 2): trsf_02}

    >>> # Example 1 - Fuse the multi-angle images, to super-resolution, after affine registration without manual initialization
    >>> fused_img, _ = fusion_on_reference(ma_images, super_resolution=True)
    >>> orthogonal_view(fused_img,suptitle="Fusion: centered local average image after affine registration without manual initialization")
    >>> print(fused_img.filename)
    090223-p58-flo-mean-fused

    >>> # Example 2 - Fuse the multi-angle images, to super-resolution, after affine registration with manual initialization
    >>> fused_img, _ = fusion_on_reference(ma_images, init_trsfs=init_trsfs, super_resolution=True)
    >>> orthogonal_view(fused_img,suptitle="Fusion: centered local average image after affine registration with manual initialization")

    >>> # Example 3 - Fuse the multi-angle images, to super-resolution, after affine & non-linear registration with manual initialization
    >>> fused_img, _ = fusion_on_reference(ma_images, init_trsfs=init_trsfs, super_resolution=True, vectorfield=True)
    >>> orthogonal_view(fused_img,suptitle="Fusion: centered local average image after affine & non-linear registration with manual initialization")

    """
    # Convert the list of images as a dictionary
    if isinstance(images, list):
        images = {idx: img for idx, img in enumerate(images)}

    # Make sure the reference image is in the provided images dictionary
    if ref_img_idx not in images:
        raise ValueError(f"Reference image index {ref_img_idx} not found in the provided images dictionary!")

    # If initial transformations are defined, make sure they are a dictionary
    if init_trsfs is not None:
        if not isinstance(init_trsfs, dict):
            raise TypeError(f"Initial transformations should be a dictionary, but got {type(init_trsfs)}!")

    # Create the transformation graph
    trsf_graph = initial_transformation_graph(images, init_trsfs, ref_img_idx=ref_img_idx)
    # Check if all nodes are connected to the reference image
    connected = check_all_nodes_connected_to_reference(trsf_graph)

    refine_init_trsf = kwargs.pop('refine_init_trsf', False)  # boolean flag to refine initial transformation
    # If not all nodes are connected to the reference image, compute the initial transformations
    # If specifically requested, refine the initial transformations with a rigid registration
    if not connected or refine_init_trsf:
        trsf_graph = compute_rigid_initial_trsf(images, trsf_graph)

    # Extract the init_trsfs dictionary from the trsf_graph
    init_trsfs = {}
    for src, trg, data in trsf_graph.edges(data=True):
        trsf = data.get('trsf', None)
        if trsf is not None:
            init_trsfs[(trg, src)] = trsf

    # Process keyword arguments (remove them from kwargs as it is later passed to `blockmatching`)
    final_vxs = kwargs.pop('final_vxs', None)
    super_resolution = kwargs.pop('super_resolution', True)  # boolean flag to perform super-resolution reconstruction
    interpolation = kwargs.pop('interpolation', 'linear')
    vectorfield = kwargs.pop('vectorfield', False)  # boolean flag to perform non-linear registration
    fuse_reference = kwargs.pop('fuse_reference', True)  # boolean flag to fuse the reference image
    global_averaging = kwargs.pop('global_averaging',
                                  False)  # boolean flag to switch between local and global averaging

    fused_img = _fusion_on_reference(images, method=method, init_trsfs=init_trsfs, ref_img_idx=0, final_vxs=final_vxs,
                                     super_resolution=super_resolution, interpolation=interpolation,
                                     vectorfield=vectorfield,
                                     fuse_reference=fuse_reference, global_averaging=global_averaging, **kwargs)

    return fused_img


def iterative_fusion(images, method="mean", init_trsfs=None, n_iter=3, ref_img_idx=0, **kwargs):
    """Iterative fusion of (multi-angle) images.

    Parameters
    ----------
    images : dict[int, timagetk.SpatialImage] or list[timagetk.SpatialImage]
        A dictionary (or a list) of intensity image to fuse together.
    method : str, optional
        An image fusion method to use, 'mean' by default, see ``AVERAGING_METHODS``
    init_trsfs : dict[(int, int), timagetk.Trsf], optional
        A 2-tuple indexed dictionary of initial linear transformations.
        It should be indexed by pairs of reference and floating images indexes, that is `(ref_img_idx, flo_img_idx)` .
    n_iter : int, optional
        The number of iterations to perform, including the initial registration on first image.
    ref_img_idx : int, optional
        An index of the reference image, by default `0`.

    Other Parameters
    ----------------
    final_vxs : float
        The voxel size of the final isometric image, override the voxelsize computed by `super_resolution`.
    super_resolution : bool
        Defines the voxelsize of the returned fused image.
        If ``True`` (default is ``False``), use the smallest voxelsize of the reference image as isometric voxelsize.
        Else, use the largest voxelsize of the reference image as isometric voxelsize.
    vectorfield_at_last : bool
        If ``True`` (default), perform non-linear registration only at last iteration step.
        Else, perform non-linear registration at each iteration step. This will result in a slower fusion algorithm.
    interpolation : {'linear', 'cspline'}
        The type of interpolation to performs when resampling and applying transformation to intensity images.
        Defaults to 'linear'.
    refine_init_trsf : bool
        If `True` (default), refine any existing initial transformation by performing a rigid registration.
    global_averaging : bool
        If ``True`` (default is ``False``), perform global averaging of each voxel by the total number of images.
        Else performs local averaging based on the number of defined image per voxels after registrations.

    Returns
    -------
    timagetk.SpatialImage
        The fused image after iterative registration.

    See Also
    --------
    timagetk.third_party.vt_parser.AVERAGING_METHODS

    Notes
    -----
    Only the last iteration will use the maximum resolution, the previous steps are performed on 2x down-sampled images.

    Examples
    --------
    >>> from timagetk.tasks.fusion import iterative_fusion
    >>> from timagetk.algorithms.pointmatching import pointmatching
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.stack import orthogonal_view
    >>> # Load shared multi-angle images for the first time point (t0) of 'p58' shared time-series
    >>> ma_images = {ma_im_idx: shared_data('flower_multiangle', ma_im_idx) for ma_im_idx in range(3)}
    >>> ref_idx = 0
    >>> # Load shared multi-angle landmarks for the first time point (t0) of 'p58' shared time-series
    >>> ref_pts_01, flo_pts_01 = shared_data('flower_multiangle_landmarks', (0, 1))
    >>> ref_pts_02, flo_pts_02 = shared_data('flower_multiangle_landmarks', (0, 2))
    >>> # Creates manual initial transformations with `pointmatching` algorithm:
    >>> trsf_01 = pointmatching(flo_pts_01, ref_pts_01, template_img=ma_images[ref_idx], method='rigid')
    >>> trsf_02 = pointmatching(flo_pts_02, ref_pts_02, template_img=ma_images[ref_idx], method='rigid')
    >>> init_trsfs = {(0, 1): trsf_01, (0, 2): trsf_02}

    >>> # Example #1 - Fast iterative fusion: non-linear registration only at last iteration
    >>> fused_img = iterative_fusion(ma_images, init_trsfs=init_trsfs, vectorfield_at_last=True)
    >>> orthogonal_view(fused_img,suptitle="p58-t0 multi-angle fast low-rez iterative fusion with manual initialization")
    >>> print(fused_img.filename)
    090223-p58-flo-mean-fused-iter3

    >>> # Example #2 - Fast iterative fusion: non-linear registration only at last iteration
    >>> # Reloads the shared multi-angle images as they have been modified by the iterative fusion algorithm
    >>> ma_images = {ma_im_idx: shared_data('flower_multiangle', ma_im_idx) for ma_im_idx in range(3)}
    >>> # Redefine the shared manual initial transformations as they have been modified by the iterative fusion algorithm
    >>> init_trsfs = {(0, 1): trsf_01, (0, 2): trsf_02}
    >>> fused_img = iterative_fusion(ma_images, init_trsfs=init_trsfs, vectorfield_at_last=True, super_resolution=True)
    >>> orthogonal_view(fused_img,suptitle="p58-t0 multi-angle fast iterative fusion with manual initialization")

    >>> # Example #3 - Slower iterative fusion: non-linear registration at every iteration
    >>> # Reloads the shared multi-angle images as they have been modified by the iterative fusion algorithm
    >>> ma_images = {ma_im_idx: shared_data('flower_multiangle', ma_im_idx) for ma_im_idx in range(3)}
    >>> # Redefine the shared manual initial transformations as they have been modified by the iterative fusion algorithm
    >>> init_trsfs = {(0, 1): trsf_01, (0, 2): trsf_02}
    >>> fused_img = iterative_fusion(ma_images, init_trsfs=init_trsfs, vectorfield_at_last=False, super_resolution=True)
    >>> orthogonal_view(fused_img,suptitle="p58-t0 multi-angle slow iterative fusion with manual initialization")

    >>> from timagetk.components.multi_channel import combine_channels
    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> cuts = [1 / 6., 1 / 4., 1 / 2., 2 / 3., 3 / 4.]
    >>> z_sh = fused_img.get_shape('z')
    >>> z_slices = [int(round(c * z_sh, 0)) for c in cuts]
    >>> blend = combine_channels([fused_img, vf_fused_img], colors=["red", "green"])
    >>> thumbs = [blend.get_slice(zsl) for zsl in z_slices]
    >>> titles = [f"z-slice {zsl}/{z_sh}" for zsl in z_slices]
    >>> fig = grayscale_imshow(thumbs, titles=titles)

    """
    try:
        assert n_iter >= 2
    except AssertionError:
        raise ValueError("You need a minimum of 2 iterations, otherwise use the `fusion_on_reference` method!")

    # Convert the list of images as a dictionary
    if isinstance(images, list):
        images = {idx: img for idx, img in enumerate(images)}
    n_img = len(images)  # Number of images in the dictionary
    delim = guess_delimiter(images[ref_img_idx].filename)  # guess the delimiter from the reference image filename

    # If initial transformations are defined, make sure they are a dictionary
    if init_trsfs is not None:
        if not isinstance(init_trsfs, dict):
            raise TypeError(f"Initial transformations should be a dictionary, but got {type(init_trsfs)}!")

    # Defines the voxel size of the isometric templates using the reference image
    min_vxs = min(images[ref_img_idx].get_voxelsize())  # minimum desirable voxel size of the template
    final_vxs = kwargs.pop('final_vxs', None)  # voxel size of the template for the last fusion step
    super_resolution = kwargs.pop('super_resolution', False)  # boolean flag to perform super-resolution reconstruction
    if final_vxs is None:
        final_vxs = max(images[ref_img_idx].get_voxelsize())  # default to the largest voxel size of the reference image
        if super_resolution:  # with super resolution requested...
            final_vxs = min_vxs  # ... use the smallest voxel size of the reference image
    else:
        if not isinstance(final_vxs, (int, float)):
            raise TypeError(f"Final voxel size should be a number, but got {type(final_vxs)}!")
        elif final_vxs < min_vxs:
            log.warning(f"Final voxel size ({final_vxs}) is smaller than the min of the reference image ({min_vxs})!")
        elif final_vxs <= 0:
            raise ValueError(f"Final voxel size should be a positive number, but got {final_vxs}!")
    template_vxs = final_vxs * 2.  # voxel size of the template for the fusion steps, except the last one

    # Resample reference image to isometric voxel size
    # Important to get a good initial rigid registration in next step
    images[ref_img_idx] = isometric_resampling(images[ref_img_idx], final_vxs, interpolation="linear")

    # Create the transformation graph
    trsf_graph = initial_transformation_graph(images, init_trsfs, ref_img_idx=ref_img_idx)
    # Check the transformation graph contains the necessary info (all linear trsf to reference)
    refine_init_trsf = kwargs.pop('refine_init_trsf', False)  # boolean flag to refine initial transformation
    trsf_graph = compute_rigid_initial_trsf(images, trsf_graph, refine_trsf=refine_init_trsf)

    # Extract the init_trsfs dictionary from the trsf_graph
    init_trsfs = {}
    for src, trg, data in trsf_graph.edges(data=True):
        trsf = data.get('trsf', None)
        if trsf is not None:
            init_trsfs[(trg, src)] = trsf

    # Process keyword arguments (remove from kwargs as it is passed to `blockmatching` in `_fusion_on_reference`)
    interpolation = kwargs.pop('interpolation', 'linear')
    global_averaging = kwargs.pop('global_averaging',
                                  False)  # boolean flag to switch between local and global averaging
    # Boolean flag to decide whether to perform non-linear registration only at last iteration or at each iteration step.
    if kwargs.get('vectorfield_at_last', False):
        vf_reg = False  # non-linear registration only at last iteration
    else:
        vf_reg = True  # non-linear registration at each iteration

    # Initial fusion to isometric template including the reference image
    it = 1  # Initialize fusion iteration counter
    log.info(f"Fusion - Initial registration (iteration {it})...")
    fused_img, _ = _fusion_on_reference(images, method, init_trsfs, ref_img_idx=ref_img_idx, vectorfield=vf_reg,
                                        fuse_reference=True, final_vxs=template_vxs, super_resolution=False,
                                        interpolation=interpolation, global_averaging=global_averaging,
                                        **kwargs)
    fused_img.filename += f"{delim}iter{it}"  # update the filename with the fusion iteration number

    # The reference image will now be the fused image
    prev_ref_idx = copy(ref_img_idx)  # previous reference image index
    ref_img_idx = n_img + 1  # new reference image index
    images.update({ref_img_idx: fused_img})  # update the dictionary with the fused image as reference
    # As we will now register the reference image on the previously fused image we have to
    # 1. replace the previous reference image index by the new one
    init_trsfs = {(ref_img_idx, src): trsf for (trg, src), trsf in init_trsfs.items()}
    # 2. add the identity trsf to register the previous reference image on the fused image in `init_trsfs`
    init_trsfs.update({(ref_img_idx, prev_ref_idx): create_trsf('identity')})

    # Now we can iteratively perform the registration of the images on the previously fused image
    for it in range(2, n_iter + 1):
        if it == n_iter:
            # maximum resolution only at last iteration to save processing time & memory
            template_vxs = final_vxs
            # if required, perform vectorfield registration at last iteration
            if kwargs.get('vectorfield_at_last', False):
                vf_reg = True

        log.info(f"Fusion - Registration iteration {it}...")
        fused_img, _ = _fusion_on_reference(images, method, init_trsfs, ref_img_idx=ref_img_idx, vectorfield=vf_reg,
                                            fuse_reference=False, final_vxs=template_vxs, super_resolution=False,
                                            interpolation=interpolation, global_averaging=global_averaging,
                                            **kwargs)
        fused_img.filename += f"{delim}iter{it}"  # update the filename with the fusion iteration number
        images[ref_img_idx] = fused_img  # update the reference image with the new fused image

    return fused_img


def initial_transformation_graph(images, init_trsfs=None, ref_img_idx=0):
    """Create the graph of initial transformations.

    Images are nodes and transformations are edges, if any.
    Transformations should be indexed from reference to floating, that is `(ref_img_idx, flo_img_idx)`.

    Parameters
    ----------
    images : dict[int, timagetk.SpatialImage]
        An indexed dictionary of (multi-angle) intensity image data.
    init_trsfs : dict[(int, int), timagetk.Trsf], optional
        A 2-tuple indexed dictionary of initial linear transformations.
        It should be indexed by pairs of reference and floating images indexes, that is `(ref_img_idx, flo_img_idx)`.
    ref_img_idx : int, optional
        An index of the reference image, by default `0`.

    Returns
    -------
    networkx.DiGraph
        The transformation graph with nodes representing images and edges representing transformations.

    Examples
    --------
    >>> from timagetk.tasks.fusion import initial_transformation_graph
    >>> from timagetk.io.dataset import shared_data
    >>> ma_images = {ma_im_idx: shared_data('flower_multiangle', ma_im_idx) for ma_im_idx in range(3)}
    >>> init_trsfs = {(0, 1): None, (0, 2): None}
    >>> trsf_graph = initial_transformation_graph(images, init_trsfs)
    >>> print(trsf_graph.nodes)
    [0, 1, 2]
    >>> print(trsf_graph.edges)
    []
    >>> print({im_idx: trsf_graph.nodes[im_idx] for im_idx in range(3)})
    {'name': '090223-p58-flo-top.lsm', 'reference': True}

    """
    from networkx import DiGraph

    # Create a directed graph to represent transformation relationships between images
    trsf_graph = DiGraph()

    # Add nodes to the graph for each image
    for trsf_idx, img in images.items():
        trsf_graph.add_node(
            trsf_idx,
            # Use image filename if available, otherwise generate a default name
            name=getattr(img, 'filename', f"Image {trsf_idx}"),
            # Mark whether the current image is the reference image
            reference=trsf_idx == ref_img_idx
        )

    # If no initial transformations are defined, there will be no edges, so we return the transformation graph
    if init_trsfs is None:
        return trsf_graph

    # Iterate through the initial transformations and add valid transformations as edges in the graph
    for trsf_idx, trsf in init_trsfs.items():
        if trsf is None:
            # Log a warning and skip if the transformation is missing (None)
            log.warning(f"Null {trsf_idx[0]} <- {trsf_idx[1]} transformation provided...")
            continue
        elif not trsf.is_linear():
            # Log a warning and skip if the transformation is not linear
            log.warning(f"Non-linear {trsf_idx[0]} <- {trsf_idx[1]} transformation provided, skipping it...")
            continue
        else:
            # Add the valid transformation as a directed edge between two images, storing the transformation object
            trsf_graph.add_edge(trsf_idx[1], trsf_idx[0], trsf=trsf)

    # Return the constructed transformation graph
    return trsf_graph


def check_all_nodes_connected_to_reference(trsf_graph):
    """Check if all 'non-reference' nodes are connected to the 'reference' node.

    Parameters
    ----------
    trsf_graph : networkx.DiGraph
        The directed graph representing images and their transformations.

    Returns
    -------
    bool
        `True` if all non-reference nodes are connected to the reference node, `False` otherwise.

    Examples
    --------
    >>> from timagetk.tasks.fusion import check_all_nodes_connected_to_reference
    >>> from timagetk.tasks.fusion import initial_transformation_graph
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.algorithms.trsf import create_trsf
    >>> # Example 1 - Create a graph with 3 images and 2 null transformations:
    >>> ma_images = {ma_im_idx: shared_data('flower_multiangle', ma_im_idx) for ma_im_idx in range(3)}
    >>> init_trsfs = {(0, 1): None, (0, 2): None}
    >>> trsf_graph = initial_transformation_graph(ma_images, init_trsfs)
    >>> connected = check_all_nodes_connected_to_reference(trsf_graph)
    >>> print(connected)
    False
    >>> # Example 2 - Create a graph with 3 images and 2 identity transformations:
    >>> id_trsf = create_trsf('identity')
    >>> init_trsfs = {(0, 1): id_trsf, (0, 2): id_trsf}
    >>> trsf_graph = initial_transformation_graph(ma_images, init_trsfs)
    >>> connected = check_all_nodes_connected_to_reference(trsf_graph)
    >>> print(connected)
    True

    """
    from networkx import has_path

    # Dynamically find the reference node by looking for a node with the 'reference' attribute set to True
    ref_node = next(
        (node for node, attrs in trsf_graph.nodes(data=True) if attrs.get('reference') is True),
        None  # Default value if no node has the 'reference' attribute
    )

    # Raise an error if no reference node is found, as the function requires this to proceed
    if ref_node is None:
        raise ValueError("No reference node found in the graph!")

    # Check if every non-reference node is accessible (connected) from the reference node
    for node in trsf_graph.nodes:
        # Skip the reference node itself, as it doesn't need to connect to itself
        if node != ref_node and not has_path(trsf_graph, node, ref_node):
            # If any non-reference node is not connected, return False
            return False

    # If all nodes are connected to the reference node, return True
    return True


def compute_rigid_initial_trsf(images, trsf_graph, refine_trsf=False, iso_vxs=None):
    """Compute the initial rigid transformations between images in a transformation graph.

    This function calculates the rigid initial transformations between multiple images
    represented as nodes in a transformation graph. The graph edges store transformation
    mappings between two connected nodes. The computed transformations are saved as edge
    attributes in the graph. Optionally, existing transformations can be refined, if needed.
    The reference node in the graph is automatically identified via its 'reference' attribute.

    Parameters
    ----------
    images : dict[int, timagetk.SpatialImage]
        A dictionary containing image data. Each key corresponds to a node in the graph,
        and the value is the image associated with that node.
    trsf_graph : networkx.DiGraph
        A directed or undirected graph in which nodes represent images, and edges will store
        transformation data between those nodes.
    refine_trsf : bool, optional
        If True, existing transformations will be refined. Refine initial transformations
        using the 'blockmatching' method. By default, no refinement is performed.
    iso_vxs : float, optional
        A voxelsize to use for isometric resampling prior to registration.
        If not provided (`None` by default), no resampling is performed.

    Returns
    -------
    networkx.DiGraph
        The transformation graph with updated edges containing computed or refined
        rigid transformations as edge attributes.

    Examples
    --------
    >>> from timagetk.tasks.fusion import compute_rigid_initial_trsf
    >>> from timagetk.tasks.fusion import initial_transformation_graph
    >>> from timagetk.io.dataset import shared_data
    >>> # Example 1 - Create a graph with 3 images and 2 null transformations:
    >>> images = {ma_im_idx: shared_data('flower_multiangle', ma_im_idx) for ma_im_idx in range(3)}
    >>> init_trsfs = {(0, 1): None, (0, 2): None}
    >>> trsf_graph = initial_transformation_graph(images, init_trsfs)
    >>> trsf_graph = compute_rigid_initial_trsf(images, trsf_graph)

    >>> # Example 2 - Create a graph with 3 images and 2 null transformations:
    >>> from timagetk.tasks.fusion import rigid_iso_blockmatching
    >>> trsf_01 = rigid_iso_blockmatching(images[1], images[0])
    >>> trsf_12 = rigid_iso_blockmatching(images[2], images[1])
    >>> init_trsfs = {(0, 1): trsf_01, (1, 2): trsf_12}
    >>> trsf_graph = initial_transformation_graph(images, init_trsfs)
    >>> trsf_graph.has_edge(2, 0)  # No direct path from 2 to 0
    False
    >>> trsf_graph = compute_rigid_initial_trsf(images, trsf_graph)
    >>> trsf_graph.has_edge(2, 0)  # Now there is a direct path from 2 to 0 after composing the transformations
    True
    """
    from networkx import shortest_path
    from networkx.exception import NetworkXNoPath

    # Dynamically find the reference node by looking for a node with the 'reference' attribute set to True
    ref_node = next(
        (node for node, attrs in trsf_graph.nodes(data=True) if attrs.get('reference') is True),
        None  # Default value if no node has the 'reference' attribute
    )

    # Raise an error if no reference node is found, as the function requires this to proceed
    if ref_node is None:
        raise ValueError("No reference node found in the graph!")

    # Iterate over all nodes in the graph to compute or refine transformations
    for src_node in trsf_graph.nodes:
        # Skip the reference node itself, as it doesn't need to connect to itself
        if src_node == ref_node:
            continue

        # Try to determine the shortest path between the source node and the reference node
        try:
            shortest_trsf_path = shortest_path(trsf_graph, source=src_node, target=ref_node)
        except NetworkXNoPath:
            # No path exists between the nodes
            shortest_trsf_path = None
            shortest_path_length = 0
        else:
            shortest_path_length = len(shortest_trsf_path) - 1

        # Check if there is no pre-existing path from the node to the reference node
        # If no path exists, compute a new rigid transformation
        if shortest_trsf_path is None:
            log.info(f"Computing initial rigid transformation T(i_{src_node}->i_{ref_node})...")
            # Compute the rigid transformation using the blockmatching method
            new_trsf = rigid_iso_blockmatching(
                images[src_node],
                images[ref_node],
                iso_vxs=iso_vxs,
            )
            # Add the newly computed transformation as an edge attribute to the graph
            trsf_graph.add_edge(src_node, ref_node, trsf=new_trsf)
            refine_trsf = False  # Disable transformation refinement as we just computed it!
        elif shortest_path_length > 1:
            # We need to compose the transformations to get a direct trsf from the source node to the reference one
            trsf_pairs = list(zip(shortest_trsf_path[:-1], shortest_trsf_path[1:]))
            log.info(f"Composing initial rigid transformations: {trsf_pairs}...")
            new_trsf = compose_trsf([trsf_graph[src][trg]['trsf'] for src, trg in trsf_pairs])
            # Add the newly computed transformation as an edge attribute to the graph
            trsf_graph.add_edge(src_node, ref_node, trsf=new_trsf)
        else:
            log.info(f"Using provided initial rigid transformation T(i_{src_node}->i_{ref_node})...")

        if refine_trsf:
            log.info(f"Updating initial rigid transformation T(i_{src_node}->i_{ref_node})...")
            # Refine the existing transformation using blockmatching with the past transformation as input
            updated_trsf = rigid_iso_blockmatching(
                images[src_node],
                images[ref_node],
                iso_vxs=iso_vxs,
                left_trsf=trsf_graph[src_node][ref_node]['trsf'],  # Use existing transformation as initialization
            )
            # Update the graph edge with refined transformation data.
            trsf_graph.add_edge(src_node, ref_node, trsf=updated_trsf)

    return trsf_graph


def rigid_iso_blockmatching(flo_img, ref_img, iso_vxs=None, left_trsf=None, **kwargs):
    """Performs rigid isometric block matching to estimate a transformation between images.

    The function resamples the input images to an isotropic voxel size and uses
    block matching to align the images rigidly. Optionally, an existing transformation
    can be provided to initialize the process.

    Parameters
    ----------
    flo_img : Image
        The floating (source) image for registration.
    ref_img : Image
        The reference (target) image for registration.
    iso_vxs : float, optional
        Isotropic voxel size to which the images are resampled. If not provided,
        it is calculated based on the voxel sizes of the input images.
    left_trsf : Transformation, optional
        An existing transformation used as initialization for the block matching process.

    Returns
    -------
    timagetk.Trsf
        The resulting rigid transformation obtained from the block matching process.

    Notes
    -----
    The "ideal voxel sizes" is computed using a heuristic formula: 2x min voxel size or 0.5x max voxel size,
    whichever is smaller.

    Any blockmatching parameters can be passed as keyword arguments.
    The following parameters are defined in this function:
      - `pyramid_highest_level` : 5
      - `pyramid_lowest_level` : 3

    Examples
    --------
    >>> from timagetk.tasks.fusion import rigid_iso_blockmatching
    >>> from timagetk.io.dataset import shared_data
    >>> images = {ma_im_idx: shared_data('flower_multiangle', ma_im_idx) for ma_im_idx in range(3)}
    >>> trsf_01 = rigid_iso_blockmatching(images[1], images[0])

    """
    py_hl = kwargs.get('pyramid_highest_level', 5)  # Define pyramid largest block settings for the refinement
    py_ll = kwargs.get('pyramid_lowest_level', 3)  # Define pyramid smallest block settings for the refinement
    quiet = kwargs.get('quiet', True)  # Suppress verbose output from blockmatching

    # Retrieve the minimum and maximum voxel size from the floating and reference images.
    mini_vxs = min(min(flo_img.get_voxelsize()), min(ref_img.get_voxelsize()))
    max_vxs = max(max(flo_img.get_voxelsize()), max(ref_img.get_voxelsize()))

    # Compute the "ideal" isotropic voxel size for resampling the images,
    # using a heuristic formula: 2x min voxel size or 0.5x max voxel size, whichever is smaller.
    ideal_vxs = round(min(mini_vxs * 2., max_vxs / 2.), 3)

    # If no isotropic voxel size is explicitly provided, default to the ideal size.
    if iso_vxs is None:
        iso_vxs = ideal_vxs
    else:
        # Validate if the provided iso_vxs is of type float.
        try:
            assert isinstance(iso_vxs, float)
        except AssertionError:
            # Log an error for invalid voxel size and default to the ideal value.
            log.error(f"Invalid isotropic voxel size provided, got '{iso_vxs}', instead of float!")
            iso_vxs = ideal_vxs
        # Ensure iso_vxs is within the range [mini_vxs, max_vxs].
        try:
            assert mini_vxs < iso_vxs < max_vxs
        except AssertionError:
            # Log a warning if the provided size is out of range, and revert to the ideal size.
            log.warning(
                f"Given isotropic voxel size ({iso_vxs}) is outside the range of the input images [{mini_vxs}, {max_vxs}]!]")
            iso_vxs = ideal_vxs
        iso_vxs = round(iso_vxs, 3)

    log.debug(f"Using isotropic voxel size of {iso_vxs} for registration.")
    trsf = blockmatching(
        # Floating (source) image
        isometric_resampling(flo_img, iso_vxs, 'linear'),
        # Reference (target) image
        isometric_resampling(ref_img, iso_vxs, 'linear'),
        method='rigid',  # Use rigid registration.
        left_trsf=left_trsf,  # Use existing transformation as initialization, if any
        pyramid_highest_level=py_hl,
        pyramid_lowest_level=py_ll,
        quiet=quiet
    )
    # Compose the obtained linear transformation with the initial 'left_trsf':
    if left_trsf is not None:
        trsf = compose_trsf([left_trsf, trsf])

    return trsf


def _fusion_on_reference(images, method, init_trsfs, ref_img_idx, vectorfield=False, fuse_reference=True,
                         final_vxs=None, super_resolution=True, global_averaging=False, interpolation="linear",
                         **kwargs):
    """Fuse images by registering them on the reference image.

    All floating image are registered with the affine method
    If requested, a non-linear registration (vectorfield) may also be performed.
    This(ese) transformation(s), and the initial one, if any, are composed to be applied to their respective floating image.
    The registered images are then 'averaged' with the selected `method`.
    The reference image is included in this list by default.
    Finally, to "center" the transformation, we:
      1. compose the estimated affine and vectorfield transformation of each floating image,
      2. invert them,
      3. average them with the same image averaging method
      4. apply this *averaged transformation* to the *averaged image*

    Parameters
    ----------
    images : dict[int, timagetk.SpatialImage]
        A dictionary of intensity image to fuse together.
    method : str
        An image fusion method to use, 'mean' by default, see ``AVERAGING_METHODS``
    init_trsfs : dict[(int, int), Trsf]
        A 2-tuple indexed dictionary of initial linear transformations.
        It should be indexed by pairs of reference and floating images indexes, that is `(ref_img_idx, flo_img_idx)` .
    ref_img_idx : int
        An index of the reference image.
    vectorfield : bool, optional
        If ``True`` (default is ``False``), also compute a non-linear deformation to match the images.
    fuse_reference : bool, optional
        If ``True`` (default), add the reference image to the list of images to fuse.
    final_vxs : float, optional
        If specified, the template image will be the isometric resampling of the reference image to this voxel size float value
    super_resolution : bool, optional
        If ``True`` (default), compute a reference frame with minimum voxelsize.
    global_averaging : bool, optional
        If ``True`` (default is ``False``), perform global averaging of each voxel by the total number of images.
        Else performs local averaging based on the number of defined imaged per voxels after registrations.
    interpolation : {'linear', 'cspline'}, optional
        The type of interpolation to performs when resampling and applying transformation to intensity images.
        Defaults to 'linear'.

    Returns
    -------
    timagetk.SpatialImage
        The fused image after registration, averaging and frame centering.
    dict[(int, int), timagetk.Trsf]
        The dictionary of transformations to register the images on the reference image.
        The dictionary is indexed by pairs of reference and floating images indexes, that is `(ref_img_idx, flo_img_idx)`.

    See Also
    --------
    timagetk.third_party.vt_parser.AVERAGING_METHODS : A list of available averaging methods.

    Notes
    -----
    The rigid part of the registration should be excluded from the averaged transformations, so the initial transformations should be processed by ``_check_init_rigid_trsf`` prior to using this method!

    Prior to averaging, masks are generated to average each voxel by the number of locally defined images and not the total number of images.

    Examples
    --------
    >>> from timagetk.tasks.fusion import _fusion_on_reference
    >>> from timagetk.algorithms.pointmatching import pointmatching
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.stack import orthogonal_view
    >>> # Load shared multi-angle images for the first time point (t0) of 'p58' shared time-series
    >>> ma_images = {ma_im_idx: shared_data('flower_multiangle', ma_im_idx) for ma_im_idx in range(3)}
    >>> ref_idx=0
    >>> # Load shared multi-angle landmarks for the first time point (t0) of 'p58' shared time-series
    >>> ref_pts_01, flo_pts_01 = shared_data('flower_multiangle_landmarks', (0, 1))
    >>> ref_pts_02, flo_pts_02 = shared_data('flower_multiangle_landmarks', (0, 2))
    >>> # Creates manual initialization transformations with `pointmatching` algorithm:
    >>> trsf_01 = pointmatching(flo_pts_01, ref_pts_01, template_img=ma_images[ref_idx], method='rigid')
    >>> trsf_02 = pointmatching(flo_pts_02, ref_pts_02, template_img=ma_images[ref_idx], method='rigid')
    >>> init_trsfs = {(0, 1): trsf_01, (0, 2): trsf_02}

    >>> # Example 1 - Fuse the multi-angle images, to super-resolution, after affine registration without manual initialization
    >>> fused_img, _ = _fusion_on_reference(ma_images, super_resolution=True)
    >>> orthogonal_view(fused_img,suptitle="Fusion: centered local average image after affine registration without manual initialization")

    >>> # Example 2 - Fuse the multi-angle images, to super-resolution, after affine registration with manual initialization
    >>> fused_img, _ = _fusion_on_reference(ma_images, init_trsfs=init_trsfs, super_resolution=True)
    >>> orthogonal_view(fused_img,suptitle="Fusion: centered local average image after affine registration with manual initialization")

    >>> # Example 3 - Fuse the multi-angle images, to super-resolution, after affine & non-linear registration with manual initialization
    >>> fused_img, _ = _fusion_on_reference(ma_images, init_trsfs=init_trsfs, super_resolution=True, vectorfield=True)
    >>> orthogonal_view(fused_img,suptitle="Fusion: centered local average image after affine & non-linear registration with manual initialization")

    """
    # Create the set of float image indexes
    img_idx = set(images.keys())
    float_imgs_idx = img_idx - {ref_img_idx}

    log.debug(f"Reference image shape: {images[ref_img_idx].get_shape()}")
    log.debug(f"Reference image voxel-sizes: {np.round(np.array(images[ref_img_idx].get_voxelsize()), 3)}")
    for flo_idx in float_imgs_idx:
        log.debug(f"Floating image #{flo_idx} shape: {images[flo_idx].get_shape()}")
        log.debug(f"Floating image #{flo_idx} voxel-sizes: {np.round(np.array(images[flo_idx].get_voxelsize()), 3)}")

    # Defines the template image used for the final resampling:
    if final_vxs is not None:
        # Resample the reference image in the template image:
        template = iso_template(images[ref_img_idx], value=final_vxs)
    elif super_resolution:
        # If Super-resolution is requested, the template image is isometric to the smallest voxel size:
        template = iso_template(images[ref_img_idx], value="min")
    else:
        template = images[ref_img_idx]
    log.debug(f"Template image shape: {template.get_shape()}")
    log.debug(f"Template image voxel-sizes: {np.round(np.array(template.get_voxelsize()), 3)}")

    # Define the reference image template for non-linear transformation
    vf_ref_template = None
    if vectorfield:
        vf_ref_template = resample(images[ref_img_idx], voxelsize=template.get_voxelsize())

    # Timer:
    log.info(f"Starting the fusion of {len(float_imgs_idx)} images on the reference...")
    fusion_time = time.time()

    # --- Block-matching registration of each floating images on the reference:
    iden_trsf = create_trsf('identity')
    lin_trsfs = {(ref_img_idx, flo_idx): iden_trsf for flo_idx in float_imgs_idx}  # dict of linear transformations
    vf_trsfs = {(ref_img_idx, flo_idx): iden_trsf for flo_idx in float_imgs_idx}  # dict of non-linear transformations
    for flo_idx in float_imgs_idx:
        # Estimate the AFFINE transformation, from floating image to reference image:
        lin_trsf = blockmatching(images[flo_idx], images[ref_img_idx],
                                 method='affine', left_trsf=init_trsfs[(ref_img_idx, flo_idx)], **kwargs)
        lin_trsfs[(ref_img_idx, flo_idx)] = lin_trsf
        # If required, also estimate the VECTORFIELD transformation, from floating image to reference image:
        if vectorfield:
            init_vf = compose_trsf([init_trsfs[(ref_img_idx, flo_idx)], lin_trsf])
            vf_trsf = blockmatching(images[flo_idx], vf_ref_template,
                                    method='vectorfield', left_trsf=init_vf, **kwargs)
            vf_trsfs[(ref_img_idx, flo_idx)] = vf_trsf

    # --- Registered images and mask creation:
    res_imgs = []  # images list (to average)
    res_masks = []  # masks list (to average if local averaging)
    if fuse_reference:
        attr = get_image_attributes(images[ref_img_idx], exclude=["dtype"])
        ref_mask = SpatialImage(np.ones_like(images[ref_img_idx], np.uint8), **attr)
        ref_id_aff_trsf = create_trsf('identity', template_img=template, trsf_type='affine')
        # Add the reference image to the list of image to be "fused"
        res_imgs.append(
            apply_trsf(images[ref_img_idx], ref_id_aff_trsf, template_img=template, interpolation=interpolation))
        if not global_averaging:
            # Need a mask if we are performing local averaging (not global)
            res_masks.append(apply_trsf(ref_mask, ref_id_aff_trsf, template_img=template, interpolation='nearest'))

    for flo_idx in float_imgs_idx:
        attr = get_image_attributes(images[ref_img_idx], exclude=["dtype"])
        flo_mask = SpatialImage(np.ones_like(images[flo_idx], np.uint8), **attr)
        if vectorfield:
            # Compose the linear and non-linear transformations (initial + affine + vectorfield)
            comp_trsf = compose_trsf([
                init_trsfs[(ref_img_idx, flo_idx)], lin_trsfs[(ref_img_idx, flo_idx)], vf_trsfs[(ref_img_idx, flo_idx)]
            ])
            # Apply the composed transformation to the floating image & create associated mask
            res_imgs.append(apply_trsf(images[flo_idx], comp_trsf, interpolation=interpolation))
            if not global_averaging:
                # Need a mask if we are performing local averaging (not global)
                res_masks.append(apply_trsf(flo_mask, comp_trsf, interpolation='nearest'))
        else:
            # Compose the linear transformations (initial + affine)
            comp_trsf = compose_trsf([init_trsfs[(ref_img_idx, flo_idx)], lin_trsfs[(ref_img_idx, flo_idx)]])
            # Apply the composed transformation to the floating image & create associated mask
            res_imgs.append(apply_trsf(images[flo_idx], comp_trsf, template_img=template, interpolation=interpolation))
            if not global_averaging:
                # Need a mask if we are performing local averaging (not global)
                res_masks.append(apply_trsf(flo_mask, comp_trsf, template_img=template, interpolation='nearest'))

    # --- Average registered images with specified fusion method:
    if global_averaging:
        # Global averaging => no masks !
        mean_image = images_averaging(res_imgs, masks=None, method=method)
    else:
        # Local averaging
        mean_image = images_averaging(res_imgs, masks=res_masks, method=method)
    log.debug(f"Average image shape: {mean_image.get_shape()}")
    log.debug(f"Average image voxel-sizes: {np.round(np.array(mean_image.get_voxelsize()), 3)}")

    # --- Centering the mean image in the reference frame is achieved by:
    # 1. averaging the computed transformation (excluding potential manual rigid initialization)
    # 2. inverting it
    # 3. apply the inverted mean transformation to the averaged image
    log.debug("# --- Centering the mean image in the reference frame:")

    # 0. Create the dictionary of (composed) transformation to average, it needs one trsf per image to fuse
    trsfs2average = {}
    # If the reference image is fused, we need to create an identity transformation
    log.debug("0. Create the list of (composed) transformation to average")
    if fuse_reference:
        if vectorfield:
            # Add an identity vectorfield transformation if the reference image is "fused"
            iden_trsf = create_trsf('identity', template_img=template, trsf_type='vectorfield')
        else:
            # Add an identity affine transformation if the reference image is "fused"
            iden_trsf = create_trsf('identity', template_img=template, trsf_type='affine')
        trsfs2average[(ref_img_idx, ref_img_idx)] = iden_trsf
    # Add the estimated AFFINE transformations (composed with VECTORFIELD if required)
    for flo_idx in float_imgs_idx:
        if vectorfield:
            trsf = compose_trsf(
                [lin_trsfs[(ref_img_idx, flo_idx)], vf_trsfs[(ref_img_idx, flo_idx)]],
                template_img=template
            )
        else:
            trsf = lin_trsfs[(ref_img_idx, flo_idx)]
        trsfs2average[(ref_img_idx, flo_idx)] = trsf

    # 1. average the (composed) transformations
    if vectorfield:
        log.debug("1. average the composed linear transformations")
        mean_trsf = trsfs_averaging(list(trsfs2average.values()), method='mean', trsf_type="vectorfield")
    else:
        log.debug("1. average the composed non-linear transformations")
        mean_trsf = trsfs_averaging(list(trsfs2average.values()), method='mean', trsf_type="affine")

    # 2. invert the average transformation
    if vectorfield:
        log.debug("2. invert the average transformation")
        inv_mean_trsf = inv_trsf(mean_trsf, template_img=template)
    else:
        log.debug("2. invert the average transformation")
        inv_mean_trsf = inv_trsf(mean_trsf)

    # 3. finally, apply the inverted average transformation to the average image
    if vectorfield:
        log.debug("3. finally, apply the non-linear inverted average transformation to the average image")
        fused_img = apply_trsf(mean_image, inv_mean_trsf, interpolation=interpolation)
    else:
        log.debug("3. finally, apply the linear inverted average transformation to the average image")
        fused_img = apply_trsf(mean_image, inv_mean_trsf, template_img=template, interpolation=interpolation)
    log.debug(f"Fused image shape: {fused_img.get_shape()}")
    log.debug(f"Fused image voxel-sizes: {np.round(np.array(fused_img.get_voxelsize()), 3)}")

    # Timer:
    log.info(f"Multi-angle fusion {elapsed_time(fusion_time)}")

    delim = guess_delimiter(images[ref_img_idx].filename)  # guess the delimiter from the reference image filename
    # Create a filename for the fused image using common segments from the fused image filenames
    fused_img.filename = delim.join([find_common_segments([img.filename for img in images.values()]), method, "fused"])

    if fuse_reference:
        trsfs2average.pop((ref_img_idx, ref_img_idx))  # remove the identity transformation

    return fused_img, trsfs2average
