#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""This submodule implement the mini data model to follow for easy batch processing.

Semantic and file hierarchy to use:
```
Experiment/
└── Observation/
    ├── raw/                     <- put your raw data there
    ├── <dataset_name>/          <- other dataset will be created automatically as you performs tasks
    ├── ...
    └── <observation_name>.json  <- define the basic metadata to perform tasks
```

An _experiment_ is a collection of _observations_.
Each _observation_ should have a RAW _dataset_ that contains a (multichannel/multi-angle) image or a temporal set of images.
An observation should thus relate to a **single biological sample**.

The minimal JSON file to create should describe the `object`, the `time` (if temporal observation) and RAW data.
For example, with the `p58` multi-angle time-series:
```json
{
    "object": {
        "series_name": "p58",
        "species": "Arabidopsis thaliana",
        "observation_sample": "Floral Meristem",
        "NCBI id": "3702",
        "genotype": "wt"
    },
    "time": {
        "points": [
            0,
            26,
            49
        ],
        "unit": "h"
    },
    "raw": {
        "0": [
            "090223-p58-flo-top.lsm",
            "090223-p58-flo-tilt1.lsm",
            "090223-p58-flo-tilt2.lsm"
        ],
        "1": [
            "090224-p58-flo-top.lsm",
            "090224-p58-flo-tilt1.lsm",
            "090224-p58-flo-tilt2.lsm"
        ],
        "2": [
            "090225-p58-flo-top.lsm",
            "090225-p58-flo-tilt1.lsm",
            "090225-p58-flo-tilt2.lsm"
        ]
    }
}
```
Note that all file paths are relative to the JSON file and thus do not need to be added.
Also, `'object'` description is not mandatory except for `'series_name'`.
However, it is highly recommended to improve the metadata associated to the experiments.

"""

import collections.abc
import json
import os
from os.path import exists
from os.path import join
from os.path import split
from pathlib import Path
from sys import exit

import pandas as pd

from timagetk.bin.logger import get_logger

DEFAULT_EXT = "tif"

# -- Default names of the dataset:
DEFAULT_INTENSITY_DATASET = "raw"
DEFAULT_TRSF_DATASET = "trsf"
DEFAULT_FUSION_DATASET = "fusion"
DEFAULT_SEGMENTATION_DATASET = "watershed_segmentation"
DEFAULT_LINEAGE_DATASET = "lineage"
DEFAULT_MULTIANGLE_LANDMARKS_DATASET = "multiangle_landmarks"
DEFAULT_MULTIANGLE_INIT_DATASET = "multiangle_init"
DEFAULT_LINEAGE_LANDMARKS_DATASET = "lineage_landmarks"
DEFAULT_REGISTRATION_DATASET = "temporal_registration"
DEFAULT_SEED_DATASET = "seed_images"
DEFAULT_NUCLEI_DATASET = "nuclei_detection"
DEFAULT_FEATURES_DATASET = "features"
DEFAULT_CLUSTERING_DATASET = "clustering"
DEFAULT_OBJ_DATASET = "mesh"
DEFAULT_IDENTITY_DATASET = "identities"

# Default registration method:
DEFAULT_REGISTRATION = "rigid"
DEFAULT_INIT_TRSF_DATASET = f"init-{DEFAULT_TRSF_DATASET}-{DEFAULT_REGISTRATION}"

logger = get_logger('json_parser', '/tmp/json_parser.log')


def sorted_dict(my_dict):
    return dict(sorted(my_dict.items()))

def save2json(json_file: str, json_dict: dict):
    """Save a jsonyfied dictionary to a JSON file."""
    with open(json_file, 'w') as f:
        json.dump(json_dict, f, indent=4)
    return


def json_has_dataset(json_file: str, dataset: str) -> bool:
    """Test if the dataset name is present in the JSON file."""
    json_ds = json_metadata(json_file)
    return dataset in json_ds


def json_metadata(json_file, **kwargs) -> dict:
    """Parse metadata from JSON.

    Parameters
    ----------
    json_file : str, os.PathLike
        Path to the JSON file with dataset metadata.

    Optional parameters
    -------------------
    required_object_metadata : bool, optional
        Required an `object` field in the json file. Required for the main json file of an experiment. Default is True.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.json_parser import json_metadata
    >>> json_metadata(shared_data("p58/p58_full.json"))
    {'object': {'series_name': 'p58',
      'species': 'Arabidopsis thaliana',
      'observation_sample': 'Floral Meristem',
      'NCBI id': '3702',
      'genotype': 'wt'},
     'time': {'points': [0, 26, 49], 'unit': 'h'},
     'threshold': [20, 30, 15],
     'raw': {'0': ['090223-p58-flo-top.lsm',
       '090223-p58-flo-tilt1.lsm',
       '090223-p58-flo-tilt2.lsm'],
      '1': ['090224-p58-flo-top.lsm',
       '090224-p58-flo-tilt1.lsm',
       '090224-p58-flo-tilt2.lsm'],
      '2': ['090225-p58-flo-top.lsm',
       '090225-p58-flo-tilt1.lsm',
       '090225-p58-flo-tilt2.lsm']},
     'multiangle_landmarks': {'0': {'0/1': ['p58_t0_reference_ldmk-01.txt',
        'p58_t0_floating_ldmk-01.txt'],
       '0/2': ['p58_t0_reference_ldmk-02.txt', 'p58_t0_floating_ldmk-02.txt']},
      '1': {'0/1': ['p58_t1_reference_ldmk-01.txt', 'p58_t1_floating_ldmk-01.txt'],
       '0/2': ['p58_t1_reference_ldmk-02.txt', 'p58_t1_floating_ldmk-02.txt']},
      '2': {'0/1': ['p58_t2_reference_ldmk-01.txt', 'p58_t2_floating_ldmk-01.txt'],
       '0/2': ['p58_t2_reference_ldmk-02.txt', 'p58_t2_floating_ldmk-02.txt']}},
     'fusion': {'1': 'p58_t1_fused.tif',
      '0': 'p58_t0_fused.tif',
      '2': 'p58_t2_fused.tif'}}

    """
    with open(json_file, 'r') as f:
        ds = json.load(f)

    required_object_metadata = kwargs.get('required_object_metadata', True)

    if ('object' not in ds) and required_object_metadata:
        logger.critical("Missing mandatory 'object' entry in JSON metadata!")
        exit(1)
    return ds


def json_series_metadata(json_file) -> dict:
    """Parse 'object' metadata from JSON.

    Parameters
    ----------
    json_file : str, os.PathLike
        Path to the JSON file with dataset metadata.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.json_parser import json_series_metadata
    >>> json_series_metadata(shared_data("p58/p58_full.json"))
    {'series_name': 'p58',
     'species': 'Arabidopsis thaliana',
     'observation_sample': 'Floral Meristem',
     'NCBI id': '3702',
     'genotype': 'wt'}

    """
    json_ds = json_metadata(json_file)
    return json_ds.get('object', {})


def json_orientation_metadata(json_file, default_orientation=1) -> int:
    """Parse 'orientation' metadata from JSON, related to upright (1) or inverted (-1) microscope orientation.

    Parameters
    ----------
    json_file : str, os.PathLike
        Path to the JSON file with dataset metadata.
    default_orientation : int, optional
        Default image/microscope orientation to use.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.json_parser import json_orientation_metadata
    >>> json_orientation_metadata(shared_data("p58/p58_full.json"))
    1

    """
    json_ds = json_metadata(json_file)
    return json_ds.get('orientation', default_orientation)


def json_time_metadata(json_file) -> dict:
    """Parse 'time' metadata from JSON.

    Parameters
    ----------
    json_file : str, os.PathLike
        Path to the JSON file with dataset metadata.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.json_parser import json_time_metadata
    >>> json_time_metadata(shared_data("p58/p58_full.json"))
    {'points': [0, 26, 49], 'unit': 'h'}

    """
    json_ds = json_metadata(json_file)
    return json_ds.get('time', {})


def json_threshold_metadata(json_file) -> dict:
    """Parse 'threshold' metadata from JSON.

    Parameters
    ----------
    json_file : str, os.PathLike
        Path to the JSON file with dataset metadata.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.json_parser import json_threshold_metadata
    >>> json_threshold_metadata(shared_data("p58/p58_full.json"))
    [20, 30, 15]

    """
    json_ds = json_metadata(json_file)
    return json_ds.get('threshold', None)


def time_intervals(time_points:list) -> list:
    """Compute the time-intervals between given time-points."""
    return [tp - time_points[i] for i, tp in enumerate(time_points[1:])]


def json_time_intervals(json_file, src="points") -> list:
    """Parse 'time' metadata from JSON and print time intervals.

    Parameters
    ----------
    json_file : str, os.PathLike
        Path to the JSON file with dataset metadata.
    src : {"points", "selected"}
        Source of time point values, can be the list of all "points", or "selected" ones.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.json_parser import json_time_intervals
    >>> json_time_intervals(shared_data("p58/p58_full.json"))
    [26, 23]
    >>> json_time_intervals("/data/FPS_paper/wt/FM1/FM1.json","selected")

    """
    time_points = json_time_metadata(json_file)[src]
    return time_intervals(time_points)


def json_channel_names(json_file):
    """Identifies and retrieves the names of channels from a JSON file's metadata.

    Parameters
    ----------
    json_file : str
        The path to the input JSON file containing the metadata.

    Returns
    -------
    list or None
        A list of channel names extracted from the metadata.
        If channel names are not found, ``None`` is returned.
    """
    json_ds = json_metadata(json_file)
    return json_ds.get('channels', None)


def _integer_indexed_json(json_file: str, key) -> dict:
    """
    Reconstructs a JSON object by converting string-indexed keys to integer indices and updating
    file paths to absolute paths based on the data's location. Additionally, it validates the file
    paths to ensure they are not empty and logs warnings for missing or incomplete files.

    Parameters
    ----------
    json_file : str
        Path to the JSON metadata file that contains information to be processed.
    key : Any
        The key in the JSON structure that identifies a nested dictionary to transform
        and process.

    Returns
    -------
    dict
        A sorted dictionary with integer-indexed keys where the values have been transformed
        appropriately. Values may consist of new absolute file paths, lists of such file paths, or
        nested dictionaries with the same update applied recursively.

    Raises
    ------
    None
        This function does not explicitly raise errors. However, it assumes that valid input is
        provided and relies on auxiliary functions to handle improper cases.
    """
    json_ds = json_metadata(json_file)
    data_location, _ = split(json_file)

    jdict = {}
    for k, v in json_ds[key].items():
        if isinstance(v, list):
            jdict[int(k)] = [join(data_location, key, vv) for vv in v]
        elif isinstance(v, dict):
            jdict[int(k)] = {int(k): join(data_location, key, vv) for k, vv in v.items()}
        elif isinstance(v, str):
            jdict[int(k)] = join(data_location, key, v)

    # Check for potential missing files:
    missing_files = []
    for _i, file in jdict.items():
        if isinstance(file, list):
            for f in file:
                if is_empty_file(f):
                    missing_files.append(f)
        elif isinstance(file, dict):
            for k, f in file.items():
                if is_empty_file(f):
                    missing_files.append(f)
        else:
            if is_empty_file(file):
                missing_files.append(file)

    if len(missing_files) != 0:
        files = '\n'.join(missing_files)
        logger.warning(f"Found some missing files:\n{files}")

    return sorted_dict(jdict)


def json_intensity_image_parsing(json_file: str, dataset=DEFAULT_FUSION_DATASET) -> dict:
    """Parse a JSON file to return the time-interval indexed dictionary of intensity image files.

    An example of the required JSON file structure when dealing with multiangle raw data:
    ```json
    "raw": {
        "0": {
            "0": "090223-p58-flo-top.lsm",
            "1": "090223-p58-flo-tilt1.lsm",
            "2": "090223-p58-flo-tilt2.lsm"
            },
        "1": {
            "0": "090224-p58-flo-top.lsm",
            "1": "090224-p58-flo-tilt1.lsm",
            "2": "090224-p58-flo-tilt2.lsm"
            },
        "2": {
            "0": "090225-p58-flo-top.lsm",
            "1": "090225-p58-flo-tilt1.lsm",
            "2": "090225-p58-flo-tilt2.lsm"
            }
    }
    ```

    An example of the required JSON file structure for "segmentation" file list definition:
    ```json
    "fusion": {
        "0": "p58-t0-imgFus.inr.gz",
        "1": "p58-t1-imgFus.inr.gz",
        "2": "p58-t2-imgFus.inr.gz"
    },
    ```

    The files are indexed by time.
    These indexes are matched in the "segmentation" section.
    The file paths are relative to the JSON file location and dataset name.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.json_parser import json_intensity_image_parsing
    >>> json_intensity_image_parsing(shared_data("p58/p58_full.json"), "raw")
    {0: {0: '**/p58/raw/090223-p58-flo-top.lsm',
         1: '**/p58/raw/090223-p58-flo-tilt1.lsm',
         2: '**/p58/raw/090223-p58-flo-tilt2.lsm'},
     1: {0: '**/p58/raw/090224-p58-flo-top.lsm',
         1: '**/p58/raw/090224-p58-flo-tilt1.lsm',
         2: '**/p58/raw/090224-p58-flo-tilt2.lsm'},
     2: {0: '**/p58/raw/090225-p58-flo-top.lsm',
         1: '**/p58/raw/090225-p58-flo-tilt1.lsm',
         2: '**/p58/raw/090225-p58-flo-tilt2.lsm'}
    }
    >>> json_intensity_image_parsing(shared_data("p58/p58_full.json"), "fusion")
    {1: '**/p58/fusion/p58_t1_fused.tif',
     0: '**/p58/fusion/p58_t0_fused.tif',
     2: '**/p58/fusion/p58_t2_fused.tif'}

    """
    return _integer_indexed_json(json_file, dataset)


def json_nuclei_segmentation_parsing(json_file: str, dataset=DEFAULT_NUCLEI_DATASET) -> dict:
    """Parse a JSON file to return the time-interval indexed dictionary of nuclei segmentation files.

    An example of the required JSON file structure for "segmentation" file list definition:
    ```json
    {
        "nuclei_segmentation": {
            "0": "E89-LD-SAM2_t0_nuclei_coordinates.txt",
            "1": "E89-LD-SAM2_t1_nuclei_coordinates.txt",
            "2": "E89-LD-SAM2_t2_nuclei_coordinates.txt",
            "3": "E89-LD-SAM2_t3_nuclei_coordinates.txt"
        },
    }
    ```
    The files are indexed by time.
    These indexes are matched in the "segmentation" section.
    The file paths are relative to the JSON file location and dataset name.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.json_parser import json_nuclei_segmentation_parsing
    >>> json_nuclei_segmentation_parsing(shared_data("p58/p58_full.json"))
    {0: '/data/Meristems/Carlos/Nuclei_registration/qDII-CLV3-AHP6-E89-LD-SAM2/nuclei_segmentation/E89-LD-SAM2_t0_nuclei_coordinates.txt',
     1: '/data/Meristems/Carlos/Nuclei_registration/qDII-CLV3-AHP6-E89-LD-SAM2/nuclei_segmentation/E89-LD-SAM2_t1_nuclei_coordinates.txt',
     2: '/data/Meristems/Carlos/Nuclei_registration/qDII-CLV3-AHP6-E89-LD-SAM2/nuclei_segmentation/E89-LD-SAM2_t2_nuclei_coordinates.txt',
     3: '/data/Meristems/Carlos/Nuclei_registration/qDII-CLV3-AHP6-E89-LD-SAM2/nuclei_segmentation/E89-LD-SAM2_t3_nuclei_coordinates.txt'}

    """
    return _integer_indexed_json(json_file, dataset)


def json_segmentation_parsing(json_file: str, dataset=DEFAULT_SEGMENTATION_DATASET) -> dict:
    """Parse a JSON file to return the time indexed dictionary of segmentation files.

    An example of the required JSON file structure for "segmentation" file list definition:
    ```json
    {
        "watershed_segmentation": {
            "0": "p58-t0-imgSeg.inr.gz",
            "1": "p58-t1-imgSeg.inr.gz",
            "2": "p58-t2-imgSeg.inr.gz"
        },
    }
    ```
    The files are indexed by time.
    These indexes are matched in the "lineage" section.
    The file paths are relative to the JSON file location and dataset name.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.json_parser import json_segmentation_parsing
    >>> json_segmentation_parsing(shared_data("p58/p58_full.json"))
    {0: '**/p58/watershed_segmentation/p58-t0-imgSeg.inr.gz',
     1: '**/p58/watershed_segmentation/p58-t1-imgSeg.inr.gz',
     2: '**/p58/watershed_segmentation/p58-t2-imgSeg.inr.gz'}

    """
    return _integer_indexed_json(json_file, dataset)


def json_landmarks_parsing(json_file: str, dataset=DEFAULT_MULTIANGLE_LANDMARKS_DATASET) -> dict:
    """Parse a JSON file to return the time-interval indexed dictionary of lineage files.

    An example of the required JSON file structure for "multiangle_landmarks" file list definition:
    ```json
    {
        "multiangle_landmarks": {
            "0": {
                "0/1": ["ref_points_t0_view0.txt", "flo_points_t0_view1.txt"]
                "0/2": ["ref_points_t0_view0.txt", "flo_points_t0_view2.txt"]
                }
        }
    }
    ```
    An example of the required JSON file structure for "lineage_landmarks" file list definition:
    ```json
    {
        "lineage_landmarks": {
            "0/1": ["ref_points_t0.txt", "flo_points_t1.txt"]
            "1/2": ["ref_points_t1.txt", "flo_points_t2.txt"]
        }
    }
    ```

    The files are indexed by a key with both time-index defining the lineage.
    These time index should be matched in a "segmentation" file list.
    The file paths are relative to the JSON file location and dataset name.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.json_parser import json_landmarks_parsing
    >>> json_landmarks_parsing(shared_data("p58/p58_full.json"))
    {0: {(0,1): ['**/p58/multiangle_landmarks/p58_t0_reference_ldmk-01.txt', '**/p58/multiangle_landmarks/p58_t0_floating_ldmk-01.txt'],
      (0, 2): ['**/p58/multiangle_landmarks/p58_t0_reference_ldmk-02.txt', '**/p58/multiangle_landmarks/p58_t0_floating_ldmk-02.txt']},
     1: {(0, 1): ['**/p58/multiangle_landmarks/p58_t1_reference_ldmk-01.txt', '**/p58/multiangle_landmarks/p58_t1_floating_ldmk-01.txt'],
      (0, 2): ['**/p58/multiangle_landmarks/p58_t1_reference_ldmk-02.txt', '**/p58/multiangle_landmarks/p58_t1_floating_ldmk-02.txt']},
     2: {(0, 1): ['**/p58/multiangle_landmarks/p58_t2_reference_ldmk-01.txt', '**/p58/multiangle_landmarks/p58_t2_floating_ldmk-01.txt'],
      (0, 2): ['**/p58/multiangle_landmarks/p58_t2_reference_ldmk-02.txt', '**/p58/multiangle_landmarks/p58_t2_floating_ldmk-02.txt']}}

    """
    json_ds = json_metadata(json_file)

    data_location, _ = split(json_file)
    ldmk_json = {}
    for ti, ldmk_dict in json_ds[dataset].items():
        if "multiangle" in dataset:
            ldmk_json[int(ti)] = {
                tuple(map(int, k.split('/'))): [vv if (vv.startswith(data_location) and data_location) else join(data_location, dataset, vv)
                                                for vv in v] for k, v in
                ldmk_dict.items()}
        else:
            ldmk_json[tuple(map(int, ti.split('/')))] = [
                v if (v.startswith(data_location) and data_location) else join(data_location, dataset, v) for v in ldmk_dict]
    return sorted_dict(ldmk_json)


def json_multiangle_rotation_parsing(json_file: str, dataset='initial_rotation') -> dict:
    """Parse a JSON file to return the time indexed dictionary of multi-angle rotation initialisaton values.

    An example of the required JSON file structure for "multiangle_landmarks" file list definition:
    ```json
    {
        "initial_rotation": {
            "0": {
                "0/1": ["y, -70", "z, 40"],
                "0/2": ["y, 45", "z, -25"],
                "0/3": ["x, 90", "y, 180"]
            }
        }
    }
    ```

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.json_parser import json_multiangle_rotation_parsing
    >>> json_multiangle_rotation_parsing('/data/anther_morpho/multiangle_anther/multiangle_anther.json')

    """
    # Obtain the JSON metadata from the given file
    json_ds = json_metadata(json_file)

    # Initialize an empty dictionary to store the rotated JSON data
    rot_json = {}
    # Iterate over each item in the dataset
    for ti, ldmk_dict in json_ds[dataset].items():
        ti = int(ti)
        rot_json[ti] = {}

        # Iterate over each landmark dictionary item
        for angle_pairs, rot_list in ldmk_dict.items():
            # Convert angle_pairs string to a tuple of integers
            angle_pairs = tuple(map(int, angle_pairs.split('/')))
            rot_json[ti][angle_pairs] = []
            # Iterate over each rotation in the rotation list
            for rot in rot_list:
                # Split the rotation string into axis and angle after removing spaces
                axis, angle = rot.replace(' ', '').split(',')
                rot_json[ti][angle_pairs].append((axis, float(angle)))

    # Return the sorted dictionary of the rotated JSON data
    return sorted_dict(rot_json)


def json_multiangle_trsf_parsing(json_file: str, dataset=DEFAULT_MULTIANGLE_INIT_DATASET) -> dict:
    """Parse a JSON file to return the time indexed dictionary of multi-angle rotation initialisaton values.

    An example of the required JSON file structure for "multiangle_landmarks" file list definition:
    ```json
    {
        "multiangle_init": {
            "0": {
                "0/1": "multiangle_anther_t0_0-1_rigid.trsf",
                "0/2": "multiangle_anther_t0_0-2_rigid.trsf",
                "0/3": "multiangle_anther_t0_0-3_rigid.trsf"
            }
        }
    }
    ```

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.json_parser import json_multiangle_trsf_parsing
    >>> json_multiangle_trsf_parsing('/data/anther_morpho/multiangle_anther/multiangle_anther.json')

    """
    # Obtain the JSON metadata from the given file
    json_ds = json_metadata(json_file)

    # Split the file path to get the data location
    data_location, _ = split(json_file)

    # Initialize an empty dictionary to store the rotated JSON data
    rot_json = {}
    # Iterate over each item in the dataset
    for ti, trsf_dict in json_ds[dataset].items():
        ti = int(ti)
        rot_json[ti] = {}

        # Iterate over each landmark dictionary item
        for angle_pairs, trsf_fname in trsf_dict.items():
            # Convert angle_pairs string to a tuple of integers
            angle_pairs = tuple(map(int, angle_pairs.split('/')))
            rot_json[ti][angle_pairs] = join(data_location, dataset, trsf_fname)

    # Return the sorted dictionary of the rotated JSON data
    return sorted_dict(rot_json)


def json_lineage_parsing(json_file: str, dataset=DEFAULT_LINEAGE_DATASET) -> dict:
    """Parse a JSON file to return the time-interval indexed dictionary of lineage files.

    An example of the required JSON file structure for "lineage" file list definition:
    ```json
    {
        "lineage": {
            "0/1": "lineage-t0_t1-L1L2.txt",
            "1/2": "lineage-t1_t2-L1L2.txt"
        },
    }
    ```
    The files are indexed by a key with both time-index defining the lineage.
    These time index should be matched in a "segmentation" file list.
    The file paths are relative to the JSON file location and dataset name.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.json_parser import json_lineage_parsing
    >>> json_lineage_parsing(shared_data("p58/p58_full.json"))
    {(0, 1): '**/p58/lineage/p58_t0-1_lineage.txt',
     (1, 2): '**/p58/lineage/p58_t1-2_lineage.txt'}

    """
    # Load the JSON file as a dictionary:
    json_ds = json_metadata(json_file)

    # If the `dataset` is not defined or empty, return an empty dictionary:
    if dataset not in json_ds or len(json_ds[dataset]) == 0:
        return {}
    else:
        ds = json_ds[dataset]

    data_location, _ = split(json_file)

    # Parse lineage files by transitions:
    lineage_files = {
        tuple(map(int, k.split('/'))): v if (v.startswith(data_location) and data_location)
        else join(data_location, dataset, v)
        for k, v in ds.items()}

    removed_lineage = []
    # Make sure all files exists and are not empty:
    for lin, lin_file in lineage_files.items():
        if is_empty_file(lin_file):
            logger.info(f"Could not find lineage files for 't{lin[0]}->t{lin[1]}'")
            # Remove the entry if at least a file is missing or empty:
            ds.pop(lin)
            removed_lineage.append(lin)
            # Remove orphan files:
            try:
                os.remove(lin_file)
            except FileNotFoundError:
                pass
            # Update the dataset definition in JSON file:
            add2json(json_file, dataset, ds, replace=True)

    # Clear deleted lineage_files from the dictionary:
    [lineage_files.pop(lin) for lin in removed_lineage]

    # If no lineage_files remains, delete the whole dataset:
    if len(lineage_files) == 0:
        logger.info(f"No entries left in the '{dataset}' dataset, clearing it from the JSON file!")
        delete_dataset(json_file, dataset)

    return sorted_dict(lineage_files)


def json_trsf_parsing(json_file: str, dataset=DEFAULT_TRSF_DATASET) -> dict:
    """Parse a JSON file to return the type & time-interval indexed dictionary of trsf files.

    An example of the required JSON file structure for "trsf" file list definition:
    ```json
    {
        "trsf": {
            {'rigid': {
                "0/1": "XPID-t0_t1-rigid.txt",
                "1/2": "XPID-t1_t2-rigid.txt"
                },
            'vectorfield': {
                "0/1": "XPID-t0_t1-vectorfield.txt",
                "1/2": "XPID-t1_t2-vectorfield.txt"
                }
            }
        },
    }
    ```
    The files are indexed by their type and a key with the two time-index for which the transformation was computed.
    These time index should be matched in a "raw" file list (or any other valid intensity image list).
    The file paths are relative to the JSON file location and dataset name.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.json_parser import json_trsf_parsing
    >>> json_trsf_parsing(shared_data("p58/p58_full.json"))

    """
    # Load the JSON file as a dictionary:
    json_ds = json_metadata(json_file)

    # If the `dataset` is not defined or empty, return an empty dictionary:
    if dataset not in json_ds or len(json_ds[dataset]) == 0:
        return {}
    else:
        ds = json_ds[dataset]

    data_location, _ = split(json_file)
    # Parse transformation files by type
    trsf_files_by_type = {
        trsf_type: {tuple(map(int, k.split('/'))): v if (v.startswith(data_location) and data_location)
                    else join(data_location, dataset, v)
                    for
                    k, v in ds[trsf_type].items()} for trsf_type in ds}

    removed_trsf_type = []
    # Make sure all files exists and are not empty:
    for trsf_type in trsf_files_by_type:
        files = [file for _, file in trsf_files_by_type[trsf_type].items()]
        if any(is_empty_file(file) for file in files):
            if trsf_type == 'rigid':
                logger.critical("Some of the rigid transformation were missing, we have to recompute it all!")
                removed_trsf_type = list(trsf_files_by_type.keys())  # add all trsf types to the list to remove
                break

            logger.info(f"Could not find all files associated to trsf type '{trsf_type}'")
            # Remove the entry if at least a file is missing or empty:
            ds.pop(trsf_type)
            removed_trsf_type.append(trsf_type)
            # Remove orphan files:
            [os.remove(file) for file in files if exists(file)]
            # Update the dataset definition in JSON file:
            add2json(json_file, dataset, ds, replace=True)

    # Clear deleted trsf types from the dictionary:
    [trsf_files_by_type.pop(trsf_type) for trsf_type in removed_trsf_type]

    # If no transformation remains, delete the whole dataset:
    if len(trsf_files_by_type) == 0:
        logger.info(f"No entries left in the '{dataset}' dataset, clearing it from the JSON file!")
        delete_dataset(json_file, dataset)

    return trsf_files_by_type


def json_registration_parsing(json_file: str, dataset=DEFAULT_REGISTRATION_DATASET) -> dict:
    """Parse a JSON file to return the type & time-interval indexed dictionary of registered image files.

    An example of the required JSON file structure for "temporal_registration" file list definition:
    ```json
    {
        "temporal_registration": {
            {'rigid': {
                "0/1": "XPID-t0_t1-rigid.tif",
                "0/2": "XPID-t0_t2-rigid.tif",
                "1/2": "XPID-t1_t2-rigid.tif"
                },
            'vectorfield': {
                "0/1": "XPID-t0_t1-vectorfield.tif",
                "0/2": "XPID-t0_t2-vectorfield.tif",
                "1/2": "XPID-t1_t2-vectorfield.tif"
                }
            }
        },
    }
    ```
    The files are indexed by they type and a key with the two time-index for which the ransformation was computed.
    These time index should be matched in a "raw" file list (or any other valid intensity image list).
    The file paths are relative to the JSON file location and dataset name.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.json_parser import json_registration_parsing
    >>> json_registration_parsing(shared_data("p58/p58_full.json"))

    """
    return json_trsf_parsing(json_file, dataset)


def json_mesh_parsing(json_file: str, dataset=DEFAULT_OBJ_DATASET):
    """Parse a JSON file to return the time indexed dictionary of mesh files and type indexed info files.

    An example of the required JSON file structure for "mesh" file list definition:
    ```json
    {
        "mesh": {
            "mesh": {
                "0": "p58-t0.obj",
                "1": "p58-t1.obj",
                "2": "p58-t2.obj"
            },
            "info": {
                "space": "p58-t0_t49_space_infos.txt",
                "time": "p58-t0_t49_time_infos.txt"
            }
        },
    }
    ```
    The mesh files are indexed by time.
    These indexes are matched in the "lineage" section.
    The file paths are relative to the JSON file location and dataset name.

    Returns
    -------
    dict
        Time-indexed dictionary of mesh objects.
    dict
        Property indexed dictionary of info objects.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.json_parser import json_mesh_parsing
    >>> json_mesh_parsing(shared_data("p58/p58_full.json"))
    ({0: '**/p58/mesh/p58-t0.obj',
      1: '**/p58/mesh/p58-t1.obj',
      2: '**/p58/mesh/p58-t2.obj'},
     {'space': '**/p58/mesh/p58-t0_t49_space_infos.txt',
      'time': '**/p58/mesh/p58-t0_t49_time_infos.txt',
      'area': '**/p58/mesh/p58-t0_t49_area_infos.txt',
      'number_of_neighbors': '**/p58/mesh/p58-t0_t49_number_of_neighbors_infos.txt',
      'shape_anisotropy': '**/p58/mesh/p58-t0_t49_shape_anisotropy_infos.txt',
      'volume': '**/p58/mesh/p58-t0_t49_volume_infos.txt',
      "log_relative_value('area')": "**/p58/mesh/p58-t0_t49_log_relative_value('area')_infos.txt",
      "log_relative_value('number_of_neighbors')": "**/p58/mesh/p58-t0_t49_log_relative_value('number_of_neighbors')_infos.txt",
      "log_relative_value('shape_anisotropy')": "**/p58/mesh/p58-t0_t49_log_relative_value('shape_anisotropy')_infos.txt",
      "log_relative_value('volume',1)": "**/p58/mesh/p58-t0_t49_log_relative_value('volume')_infos.txt",
      "relative_value('area')": "**/p58/mesh/p58-t0_t49_relative_value('area')_infos.txt",
      "relative_value('number_of_neighbors')": "**/p58/mesh/p58-t0_t49_relative_value('number_of_neighbors')_infos.txt",
      "relative_value('shape_anisotropy')": "**/p58/mesh/p58-t0_t49_relative_value('shape_anisotropy')_infos.txt",
      "relative_value('volume')": "**/p58/mesh/p58-t0_t49_relative_value('volume')_infos.txt",
      "rate_of_change('area')": "**/p58/mesh/p58-t0_t49_rate_of_change('area')_infos.txt",
      "rate_of_change('number_of_neighbors')": "**/p58/mesh/p58-t0_t49_rate_of_change('number_of_neighbors')_infos.txt",
      "rate_of_change('shape_anisotropy')": "**/p58/mesh/p58-t0_t49_rate_of_change('shape_anisotropy')_infos.txt",
      "rate_of_change('volume')": "**/p58/mesh/p58-t0_t49_rate_of_change('volume')_infos.txt",
      "relative_rate_of_change('area')": "**/p58/mesh/p58-t0_t49_relative_rate_of_change('area')_infos.txt",
      "relative_rate_of_change('number_of_neighbors')": "**/p58/mesh/p58-t0_t49_relative_rate_of_change('number_of_neighbors')_infos.txt",
      "relative_rate_of_change('shape_anisotropy')": "**/p58/mesh/p58-t0_t49_relative_rate_of_change('shape_anisotropy')_infos.txt",
      "relative_rate_of_change('volume')": "**/p58/mesh/p58-t0_t49_relative_rate_of_change('volume')_infos.txt",
      'FloralMeristem': '**/p58/mesh/p58-t0_t49_FloralMeristem_infos.txt',
      'epidermis': '**/p58/mesh/p58-t0_t49_epidermis_infos.txt'})

    """
    # Load the JSON file as a dictionary:
    json_ds = json_metadata(json_file)

    # If the `dataset` is not defined or empty, return now:
    if dataset not in json_ds:
        return {}, {}
    else:
        ds = json_ds[dataset]

    data_location, _ = split(json_file)

    mesh_json = {}
    info_json = {}
    for name, path_dict in ds.items():
        if name == "mesh":
            for t, path in path_dict.items():
                mesh_json[int(t)] = join(data_location, dataset, path)
        elif name == 'info':
            for ppty, path in path_dict.items():
                info_json[ppty] = join(data_location, dataset, path)

    return mesh_json, info_json


def json_csv_parsing(json_file: str or Path, dataset=DEFAULT_FEATURES_DATASET) -> list:
    """Parse a JSON file to return the cell and wall CSVs files.

    An example of the required JSON file structure for "features" file list definition:
    ```json
    {
        "features": {
            "cell": "p58_features_cell.csv",
            "wall": "p58_features_wall.csv"
        },
    }
    ```
    The file paths are relative to the JSON file location and dataset name.

    Returns
    -------
    list
        The cell and cell-wall CSVs files. If available, also returns the cell-vertex and cell-edge CSVs files.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.json_parser import json_csv_parsing
    >>> json_csv_parsing(shared_data("p58/p58_full.json"), "features")
    >>> json_csv_parsing(shared_data("p58/p58_full.json"), "clustering")

    """
    # Load the JSON file as a dictionary:
    json_ds = json_metadata(json_file)

    # If the `dataset` is not defined or empty:
    if dataset not in json_ds or len(json_ds[dataset]) == 0:
        return [None, None]
    else:
        ds = json_ds[dataset]

    if "cell" not in ds or "wall" not in ds:
        return [None, None]

    data_location, _ = split(json_file)
    # Parse list of CSV files:
    cell_csv = join(data_location, dataset, ds["cell"])
    wall_csv = join(data_location, dataset, ds["wall"])

    # Optional files
    cell_vertex_csv = join(data_location, dataset, ds.get("cell_vertex", ""))
    cell_edge_csv = join(data_location, dataset, ds.get("cell_edge", ""))

    # Clear the JSON entries if files cannot be found:
    if not exists(cell_csv) or not exists(wall_csv):
        logger.info(f"Missing some files in the '{dataset}' dataset, clearing it from the JSON file!")
        delete_dataset(json_file, dataset)
        return [None, None]

    csv_files = [cell_csv, wall_csv]

    # Add optional files if they exist
    if ds.get("cell_vertex") and ds.get("cell_edge"):
        if exists(cell_vertex_csv) and exists(cell_edge_csv):
            csv_files.extend([cell_vertex_csv, cell_edge_csv])

    return csv_files


def json_clustering_parsing(json_file: str, dataset=DEFAULT_CLUSTERING_DATASET) -> dict:
    """Parse a JSON file to return the clustering dictionary with associated files.

    An example of the required JSON file structure for "clustering" file list definition:
    ```json
    {
        "clustering": {
            "cell": "FM1_clustering_cell.csv",
            "wall": "FM1_clustering_wall.csv",
            ('1', "ward_Q5_0.5*volume+0.5*log_relative_value('volume')-deleted_outliers"): [
                "clustering_report_1.md",
                "cluster_distances_heatmap_1.png",
                "element_distance_to_cluster_1.png",
                "properties_boxplot_by_cluster_1.png"
            ]
        },
    }
    ```
    The file paths are relative to the JSON file location and dataset name.

    Returns
    -------
    dict
        A dictionary with *condensed clustering name* as key and the list of associated files (report and figures) as value.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.json_parser import json_clustering_parsing
    >>> json_clustering_parsing(shared_data("p58/p58_full.json"))

    """
    # Load the JSON file as a dictionary:
    json_ds = json_metadata(json_file)

    # If the `dataset` is not defined or empty, return an empty dictionary:
    if dataset not in json_ds or len(json_ds[dataset]) == 0:
        return {}
    else:
        ds = json_ds[dataset]

    data_location, _ = split(json_file)
    # Parse list of clustering files:
    clusterings = {clust: [join(data_location, dataset, cf) for cf in ds[clust]] for clust in ds if
                   clust not in ('cell', 'wall')}
    # Parse list of clustering CSV files:
    clusterings_csv = json_csv_parsing(json_file, dataset)

    # If the CSV are missing, remove everything!
    if None in clusterings_csv:
        logger.info("Could not find CSV files associated to clustering...")
        delete_dataset(json_file, dataset)
        return {}

    removed_files = []
    # Make sure all files exists and are not empty:
    for clust_name, files in clusterings.items():
        if any(is_empty_file(file) for file in files):
            logger.info(f"Could not find all files associated to clustering '{clust_name}'")
            # Remove the entry if at least a file is missing or empty:
            ds.pop(clust_name)
            removed_files.append(clust_name)
            # Remove orphan files:
            [os.remove(file) for file in files if exists(file)]
            # Update the dataset definition in JSON file:
            add2json(json_file, dataset, ds, replace=True)

    # Clear deleted clustering from the dictionary:
    [clusterings.pop(c) for c in removed_files]
    # Clear deleted clustering from the cell CSV file:
    df = pd.read_csv(clusterings_csv[0])
    for c in removed_files:
        try:
            df.drop(c)
        except KeyError:
            pass
    df.to_csv(clusterings_csv[0])

    # If no clustering remains, delete the whole dataset:
    if len(clusterings) == 0:
        logger.info(f"No entries left in the '{dataset}' dataset, clearing it from the JSON file!")
        delete_dataset(json_file, dataset)

    return {tuple(k.split("/")): v for k, v in clusterings.items()}


def delete_dataset(json_file: str, dataset: str):
    """Delete the dataset from the JSON file and folder structure."""
    if dataset == DEFAULT_INTENSITY_DATASET:
        logger.error("You just tried to delete the RAW data folder! We prevented it!")
        return

    # Load the JSON file as a dictionary:
    json_ds = json_metadata(json_file)

    if dataset in json_ds:
        # Remove the dataset from the JSON file:
        json_ds.pop(dataset)  # remove the 'key' entry from the JSON dict
        json_ds = jsonify_tuple_keys(json_ds)
        save2json(json_file, json_ds)

    data_location, _ = split(json_file)
    dataset_location = join(data_location, dataset)
    if exists(data_location):
        # Remove the dataset folder:
        from shutil import rmtree
        rmtree(dataset_location, ignore_errors=True)
        logger.critical(f"Removed the whole '{dataset}' dataset!")
    return


def is_empty_file(file: str) -> bool:
    """Use the stat module to check if the file exists or is empty.

    Examples
    --------
    >>> from tempfile import NamedTemporaryFile
    >>> from timagetk.tasks.json_parser import is_empty_file
    >>> f = NamedTemporaryFile()
    >>> is_empty_file(f.name)
    True

    """
    from os import stat
    # Check if file do not exist or is empty (file size is 0):
    if not exists(file) or stat(file).st_size == 0:
        return True
    return False


def json_file_exists(json_dict: dict, json_key: any) -> bool:
    """Test if given key is found in JSON dictionary and test if the associated file exists."""
    return exists(json_dict[json_key]) if json_key in json_dict else False


def jsonify_tuple_keys(d: dict, sep='/', keep_basename=True) -> dict:
    """Some types conversion to meet JSON requirements.

    Transforms:
      - tuple dict keys as a string joined by a separator ('/' by default).
      - Absolute paths (dict values), given as ``str`` or ``pathlib.Path`` into ``'filename.ext'`` string.

    Parameters:
    ----------
    d : dict
        dictionnary input corresponding to the json file
    sep : str, optional
        the separator for the tuple dict keys
    keep_basename : bool, optional
        keep (default) or not the basename of filepath

    Examples
    --------
    >>> from pathlib import Path
    >>> from timagetk.tasks.json_parser import jsonify_tuple_keys
    >>> jsonify_tuple_keys({0: {(0,1): "foo"}, ("a", "b"): ['a', 'list'], 1: '/path/to/file.ext'})
    {'0': {'0/1': 'foo'}, 'a/b': ['a', 'list'], '1': 'file.ext'}
    >>> jsonify_tuple_keys({0: {(0,1): "foo"}, ("a", "b"): ['a', 'list'], 1: Path('/path/to/file.ext')})
    {'0': {'0/1': 'foo'}, 'a/b': ['a', 'list'], '1': 'file.ext'}
    """
    jd = {}
    for k, v in d.items():
        if isinstance(v, collections.abc.Mapping):
            # Explore nested dictionaries:
            jd[str(k)] = jsonify_tuple_keys(d.get(k, {}), sep=sep)
        else:
            # ----- Convert keys -----
            if isinstance(k, tuple):
                # Convert tuple dict keys as a string joined by a separator:
                k = sep.join(map(str, k))
            else:
                # Convert as a string:
                k = str(k)
            # ----- Convert values -----
            # If value `v` is a string or a Path indicating an absolute path, keep only the file name:
            if keep_basename:
                jd[k] = Path(v).name if isinstance(v, Path) or (isinstance(v, str) and v.startswith(Path(v).root)) else v
            else:
                jd[k] = str(v)
    return jd


def add2json(json_file, dataset, ds_dict, replace=False, **kwargs):
    """Update the `dataset` entry of the JSON file with given dictionary.

    Parameters
    ----------
    json_file : str or pathlib.Path
        Path to JSON file to populate with dictionary values.
    dataset : str
        Name to use as reference to the dictionary, correspond to a dataset.
    ds_dict : dict
        Dictionary corresponding to the dataset.
    replace : bool, optional
        If ``True`` replace the JSON `dataset` values with `ds_dict`.
        Otherwise, merge the common keys if any.

    Optional Parameters
    -------------------
    required_object_metadata : bool, optional
        Required an `object` field in the json file. Required for the main json file of an experiment. Default is True.
    sep : str, optional
        the separator for the tuple dict keys
    keep_basename : bool, optional
        keep (default) or not the basename of filepath

    Examples
    --------
    >>> import json
    >>> from pathlib import Path
    >>> from tempfile import NamedTemporaryFile
    >>> from timagetk.tasks.json_parser import add2json
    >>> tmp_json = NamedTemporaryFile()
    >>> add2json(tmp_json.name, 'raw', {0: {0: "here"}})
    >>> with open(tmp_json.name, 'r') as jf: print(json.load(jf))
    {'raw': {'0': {'0': 'here'}}}
    >>> tmp_json = NamedTemporaryFile()
    >>> add2json(tmp_json.name, 'raw', {0: {1: "there"}})
    >>> with open(tmp_json.name, 'r') as jf: print(json.load(jf))
    {'raw': {'0': {'1': 'there'}}}
    >>> tmp_json = NamedTemporaryFile()
    >>> add2json(tmp_json.name, 'raw', {1: "foo", "f": Path('/path/to/file.ext')})
    >>> with open(tmp_json.name, 'r') as jf: print(json.load(jf))
    {'raw': {'1': 'foo', 'f': 'file.ext'}}
    """
    required_object_metadata = kwargs.pop('required_object_metadata', True)

    json_ds = {}
    if not is_empty_file(json_file):
        json_ds = json_metadata(json_file, required_object_metadata=required_object_metadata)

    ds_dict = jsonify_tuple_keys(ds_dict, **kwargs)

    if dataset in json_ds.keys() and not replace:
        json_ds[dataset] = merge_dict(json_ds[dataset], ds_dict)
    else:
        json_ds[dataset] = ds_dict

    with open(json_file, 'w') as f:
        json.dump(json_ds, f, indent=4)


def merge_dict(d1, d2):
    """Merge dictionaries.

    Parameters
    ----------
    d1, d2 : dict
        Dictionary to merge.

    Returns
    -------
    dict
        Merged dictionary.

    Examples
    --------
    >>> from timagetk.tasks.json_parser import merge_dict
    >>> d1 = {0: {0: "here"}}
    >>> d2 = {0: {1: "there"}}
    >>> merge_dict(d1, d2)  # merge dictionaries for common key `0`
    {0: {0: 'here', 1: 'there'}}
    >>> d1 = {0: {0: "here"}}
    >>> d2 = {1: "foo"}
    >>> merge_dict(d1, d2)  # keep key `0` from `d1` and add key `1` from `d2`
    {0: {0: 'here'}, 1: 'foo'}
    >>> d1 = {0: [1, 2, 3]}
    >>> d2 = {0: [4, 5]}
    >>> merge_dict(d1, d2)  # merge lists for common key `0`
    {0: [1, 2, 3, 4, 5]}
    >>> d1 = {0: [1, 2, 3], 1: "foo"}
    >>> d2 = {0: [4, 5], 2: "bar"}
    >>> merge_dict(d1, d2)  # merge lists for common key `0`, keep key `1` from `d1` and add key `2` from `d2`
    {0: [1, 2, 3, 4, 5], 1: 'foo', 2: 'bar'}
    >>> d1 = {0: [1, 2, 3], 1: "foo"}
    >>> d2 = {0: [4, 5], 1: "bar"}
    >>> merge_dict(d1, d2)  # merge lists for common key `0` but use d2 for common key `1`!
    {0: [1, 2, 3, 4, 5], 1: 'bar'}

    """
    d = {}
    for key in set(list(d1.keys()) + list(d2.keys())):

        if isinstance(d1.get(key, None), dict) or isinstance(d2.get(key, None), dict):
            # Update with d1 value:
            try:
                d.setdefault(key, {}).update(d1[key])
            except KeyError:
                pass
            except ValueError:
                d.update(d1)
            # Update with d2 value:
            try:
                d.setdefault(key, {}).update(d2[key])
            except KeyError:
                pass
            except ValueError:
                d.update(d2)

        elif isinstance(d1.get(key, None), list) or isinstance(d2.get(key, None), list):
            # Update with d1 value:
            try:
                if isinstance(d1[key], list):
                    d.setdefault(key, []).extend(d1[key])
                else:
                    d.setdefault(key, []).append(d1[key])
            except KeyError:
                pass
            # Update with d2 value:
            try:
                if isinstance(d2[key], list):
                    d.setdefault(key, []).extend(d2[key])
                else:
                    d.setdefault(key, []).append(d2[key])
            except:
                pass
        else:
            try:
                d[key] = d1[key]
            except KeyError:
                pass
            try:
                d[key] = d2[key]
            except KeyError:
                pass

    return d
