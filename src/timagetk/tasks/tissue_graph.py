#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Module for TissueGraph creation from TissueImage."""
import os
import tempfile
import time

import numpy as np
from joblib import Parallel
from joblib import delayed
from joblib import dump
from joblib import load
from tqdm import tqdm

from timagetk import TissueImage2D
from timagetk import TissueImage3D
from timagetk.bin.logger import get_logger
from timagetk.components.labelled_image import labels_at_stack_margins
from timagetk.features.graph import add_cell_layers
from timagetk.graphs.tissue_graph import TissueGraph
from timagetk.tasks.features import CELL_FEATURES
from timagetk.tasks.features import WALL_FEATURES
from timagetk.util import stuple

log = get_logger(__name__)


def dual_graph_extraction_old(tissue, **kwargs):
    """Extract and pairs the cell-vertices & cell-edges to construct a dual graph.

    Parameters
    ----------
    tissue : timagetk.TissueImage3D
        The tissue image to extract the cell-vertices & cell-edges from.

    Returns
    -------
    list
        List of cell-vertices, to use as dual graph nodes.
    dict
        Dictionary matching cell-vertices pairs to cell-edge ids, to use as dual graph edges.

    Examples
    --------
    >>> from timagetk import TissueImage3D
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.tissue_graph import dual_graph_extraction
    >>> img = imread(shared_dataset("sphere", dtype='segmented')[0], TissueImage3D, background=1, not_a_label=0)
    >>> pointels, pointel_pairs_to_linel = dual_graph_extraction(img)
    >>> img = imread(shared_dataset("p58", dtype='segmented')[0], TissueImage3D, background=1, not_a_label=0)
    >>> pointels, pointel_pairs_to_linel = dual_graph_extraction(img)
    """

    verbose = kwargs.get('verbose', True)
    # FIXME: how to use area threshold in case it has been used in `tissue_graph_from_image` ?!
    if verbose:
        log.info("Detecting cell-vertices...")
    pointels = tissue.pointels()
    if verbose:
        log.info(f"Found {len(pointels)} cell-vertices.")
        log.info("Detecting cell-edges...")
    linels = tissue.linels()
    if verbose:
        log.info(f"Found {len(linels)} cell-edges.")
        log.info("Searching associations between cell-edges and pairs of cell-vertices...")
    pointel_pairs_to_linel = {}
    no_pointel, one_pointel, too_many = [], {}, {}

    for linel in linels:
        pointel_pair = tuple({p for p in pointels if linel[0] in p and linel[1] in p and linel[2] in p})
        if len(pointel_pair) == 0:
            no_pointel.append(linel)
        elif len(pointel_pair) == 1:
            one_pointel[pointel_pair] = linel
        elif len(pointel_pair) == 2:
            pointel_pairs_to_linel[pointel_pair] = linel
        else:
            too_many[pointel_pair] = linel

    npp = len(pointel_pairs_to_linel)
    nl = len(linels)
    if verbose:
        log.info(
            f"Matched {round(npp / float(nl) * 100, 3)}% ({npp}/{nl}) of detected cell-edges with cell-vertices pairs!")
    if no_pointel != []:
        log.warning(f"Found {len(no_pointel)} cell-edges without associated cell-vertices!")
        log.debug(no_pointel)
    if one_pointel != {}:
        log.warning(f"Found {len(one_pointel)} cell-edges with only one associated cell-vertex!")
        log.debug(one_pointel)
    if too_many != {}:
        log.warning(f"Found {len(too_many)} cell-edges with more than two associated cell-vertices!")
        log.debug(too_many)

    return pointels, pointel_pairs_to_linel


def dual_graph_extraction(tissue, **kwargs):
    """Extract and pairs the cell-vertices & cell-edges to construct a dual graph.

    Parameters
    ----------
    tissue : timagetk.TissueImage3D
        The tissue image to extract the cell-vertices & cell-edges from.

    Returns
    -------
    list
        List of cell-vertices, to use as dual graph nodes.
    dict
        Dictionary matching cell-vertices pairs to cell-edge ids, to use as dual graph edges.

    Examples
    --------
    >>> from timagetk import TissueImage3D
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.tissue_graph import dual_graph_extraction
    >>> img = imread(shared_dataset("sphere", dtype='segmented')[0], TissueImage3D, background=1, not_a_label=0)
    >>> pointels, pointel_pairs_to_linel = dual_graph_extraction(img)
    >>> img = imread(shared_dataset("p58", dtype='segmented')[0], TissueImage3D, background=1, not_a_label=0)
    >>> pointels, pointel_pairs_to_linel = dual_graph_extraction(img)
    """
    verbose = kwargs.get('verbose', True)
    # FIXME: how to use area threshold in case it has been used in `tissue_graph_from_image` ?!
    if verbose:
        log.info("Detecting cell-vertices...")
    pointels = np.array(tissue.pointels())
    if verbose:
        log.info(f"Found {len(pointels)} cell-vertices.")
        log.info("Detecting cell-edges...")
    linels = tissue.linels()
    if verbose:
        log.info(f"Found {len(linels)} cell-edges.")
        log.info("Searching associations between cell-edges and pairs of cell-vertices...")
    pointel_pairs_to_linel = {}
    no_pointel, one_pointel, too_many = [], {}, {}

    for linel in tqdm(linels, unit="linels"):
        test_arr = np.any([pointels == np.ones_like(pointels) * linel[0],
                           pointels == np.ones_like(pointels) * linel[1],
                           pointels == np.ones_like(pointels) * linel[2]], axis=0)
        pointel_pair = tuple(map(stuple, pointels[(np.where(np.sum(test_arr, axis=1) == 3)[0])]))
        if len(pointel_pair) == 0:
            no_pointel.append(linel)
        elif len(pointel_pair) == 1:
            one_pointel[pointel_pair] = linel
        elif len(pointel_pair) == 2:
            pointel_pairs_to_linel[pointel_pair] = linel
        else:
            too_many[pointel_pair] = linel

    npp = len(pointel_pairs_to_linel)
    nl = len(linels)
    if verbose:
        log.info(
            f"Matched {round(npp / float(nl) * 100, 3)}% ({npp}/{nl}) of detected cell-edges with cell-vertices pairs!")
    if no_pointel != []:
        log.warning(f"Found {len(no_pointel)} cell-edges without associated cell-vertices!")
        log.debug(no_pointel)
    if one_pointel != {}:
        log.warning(f"Found {len(one_pointel)} cell-edges with only one associated cell-vertex!")
        log.debug(one_pointel)
    if too_many != {}:
        log.warning(f"Found {len(too_many)} cell-edges with more than two associated cell-vertices!")
        log.debug(too_many)

    return list(map(stuple, pointels)), pointel_pairs_to_linel


def dual_graph_extraction_v3(tissue, **kwargs):
    """Extract and pairs the cell-vertices & cell-edges to construct a dual graph.

    Parameters
    ----------
    tissue : timagetk.TissueImage3D
        The tissue image to extract the cell-vertices & cell-edges from.

    Returns
    -------
    list
        List of cell-vertices, to use as dual graph nodes.
    dict
        Dictionary matching cell-vertices pairs to cell-edge ids, to use as dual graph edges.

    Examples
    --------
    >>> from timagetk import TissueImage3D
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.tissue_graph import dual_graph_extraction_v3
    >>> img = imread(shared_dataset("sphere", dtype='segmented')[0], TissueImage3D, background=1, not_a_label=0)
    >>> pointels, pointel_pairs_to_linel = dual_graph_extraction_v3(img)
    >>> img = imread(shared_dataset("p58", dtype='segmented')[0], TissueImage3D, background=1, not_a_label=0)
    >>> pointels, pointel_pairs_to_linel = dual_graph_extraction_v3(img)
    """

    verbose = kwargs.get('verbose', True)
    # FIXME: how to use area threshold in case it has been used in `tissue_graph_from_image` ?!
    if verbose:
        log.info("Detecting cell-vertices...")
    pointels = np.array(tissue.pointels())
    if verbose:
        log.info(f"Found {len(pointels)} cell-vertices.")
        log.info("Detecting cell-edges...")
    linels = tissue.linels()
    if verbose:
        log.info(f"Found {len(linels)} cell-edges.")
        log.info("Searching associations between cell-edges and pairs of cell-vertices...")
    pointel_pairs_to_linel = {}
    no_pointel, one_pointel, too_many = [], {}, {}

    if len(pointels) > 1e6:
        temp_folder = tempfile.mkdtemp()
        filename = os.path.join(temp_folder, 'joblib_test.mmap')
        if os.path.exists(filename):
            os.unlink(filename)
        _ = dump(pointels, filename)
        pointels = load(filename, mmap_mode='r+')

    def _pair_linel_to_pointels(linel):
        return tuple(map(tuple, pointels[(np.where(np.sum(np.any([pointels == np.ones_like(pointels) * linel[0],
                                                                  pointels == np.ones_like(pointels) * linel[1],
                                                                  pointels == np.ones_like(pointels) * linel[2]],
                                                                 axis=0), axis=1) == 3)[0])]))

    paired_pointels = [r for r in tqdm(
        Parallel(return_as="generator", n_jobs=-1)(delayed(_pair_linel_to_pointels)(linel) for linel in linels),
        unit="linels", total=len(linels))]

    if isinstance(pointels, np.memmap):
        import shutil
        try:
            shutil.rmtree(temp_folder)
        except OSError:
            pass

    for linel, pointel_pair in zip(linels, paired_pointels):
        if len(pointel_pair) == 0:
            no_pointel.append(linel)
        elif len(pointel_pair) == 1:
            one_pointel[pointel_pair] = linel
        elif len(pointel_pair) == 2:
            pointel_pairs_to_linel[pointel_pair] = linel
        else:
            too_many[pointel_pair] = linel

    npp = len(pointel_pairs_to_linel)
    nl = len(linels)
    if verbose:
        log.info(
            f"Matched {round(npp / float(nl) * 100, 3)}% ({npp}/{nl}) of detected cell-edges with cell-vertices pairs!")
    if no_pointel != []:
        log.warning(f"Found {len(no_pointel)} cell-edges without associated cell-vertices!")
        log.debug(no_pointel)
    if one_pointel != {}:
        log.warning(f"Found {len(one_pointel)} cell-edges with only one associated cell-vertex!")
        log.debug(one_pointel)
    if too_many != {}:
        log.warning(f"Found {len(too_many)} cell-edges with more than two associated cell-vertices!")
        log.debug(too_many)

    return pointels, pointel_pairs_to_linel


def dual_graph_extraction2D(tissue, **kwargs):
    """Extract and pairs the cell-vertices & cell-edges to construct a dual graph.

    Parameters
    ----------
    tissue : timagetk.TissueImage2D
        The tissue image to extract the cell-vertices & cell-edges from.

    Returns
    -------
    list
        List of cell-vertices, to use as dual graph nodes.
    dict
        Dictionary matching cell-vertices pairs to cell-edge ids, to use as dual graph edges.

    Examples
    --------
    >>> from timagetk.tasks.tissue_graph import dual_graph_extraction2D
    >>> from timagetk.array_util import dummy_labelled_image_2D
    >>> from timagetk import TissueImage2D
    >>> im = TissueImage2D(dummy_labelled_image_2D(), background=1)
    >>> p, p2l = dual_graph_extraction2D(im)

    """
    verbose = kwargs.get('verbose', True)
    # FIXME: how to use area threshold in case it has been used in `tissue_graph_from_image` ?!
    if verbose:
        log.info("Detecting cell-vertices...")
    pointels = tissue.linels()
    if verbose:
        log.info(f"Found {len(pointels)} cell-vertices.")
        log.info("Detecting cell-edges...")
    linels = tissue.surfels()
    if verbose:
        log.info(f"Found {len(linels)} cell-edges.")
        log.info("Searching associations between cell-edges and pairs of cell-vertices...")
    pointel_pairs_to_linel = {}
    no_pointel, one_pointel, too_many = [], {}, {}
    for l in linels:
        pointel_pair = tuple({p for p in pointels if l[0] in p and l[1] in p})
        if len(pointel_pair) == 0:
            no_pointel.append(l)
        elif len(pointel_pair) == 1:
            one_pointel[pointel_pair] = l
        elif len(pointel_pair) == 2:
            pointel_pairs_to_linel[pointel_pair] = l
        else:
            too_many[pointel_pair] = l
    npp = len(pointel_pairs_to_linel)
    nl = len(linels)
    if verbose:
        log.info(
            f"Matched {round(npp / float(nl) * 100, 3)}% ({npp}/{nl}) of detected cell-edges with cell-vertices pairs!")
    if no_pointel != []:
        log.warning(f"Found {len(no_pointel)} cell-edges without associated cell-vertices!")
        log.debug(no_pointel)
    if one_pointel != {}:
        log.warning(f"Found {len(one_pointel)} cell-edges with only one associated cell-vertex!")
        log.debug(one_pointel)
    if too_many != {}:
        log.warning(f"Found {len(too_many)} cell-edges with more than two associated cell-vertices!")
        log.debug(too_many)

    return pointels, pointel_pairs_to_linel


def tissue_graph_from_image(tissue, cell_ids=None, coordinates=True, features=None, wall_features=None, **kwargs):
    """Extracts a ``TissueGraph`` object from the image.

    Parameters
    ----------
    tissue : timagetk.TissueImage3D
        The segmented tissue to transform.
    cell_ids : list, optional
        If given, allow to filter the labels used within the `tissue`. By default all labels are used.
    coordinates : bool, optional
        If ``True`` add the topologocal element coordinates to the graph at construction time.
        See notes for more information.
    features : list, optional
        List of spatial cell features to extract from the segmented tissue.
        See ``timagetk.tasks.features.CELL_FEATURES``.
    wall_features : list, optional
        List of spatial wall features to extract from the segmented tissue.
        See ``timagetk.tasks.features.WALL_FEATURES``.

    Other Parameters
    ----------------
    graph : timagetk.graphs.TissueGraph
        The tissue graph to populate, if not defined, return a new one.
    real : bool
        If ``True`` (default), extract the features in real units, else in voxels.
    orientation : {1, -1}
        Image orientation, use `-1` with an inverted microscope, `1` by default.
    extract_dual : bool
        If ``True`` (default) construct the dual graph with cell edges and vertices.
        Else, construct only primal graph, that is the neighborhood graph.
    wall_area_threshold : float
        The minimum real contact area with two cells to be defined as neighbors. No minimum by default.
        Defining this value and leaving `epidermis_area_threshold` to ``None`` will lead to using this value for both.
    epidermis_area_threshold : float
        The minimum real contact area with the background necessary to be defined as epidermal cell.
        No minimum by default.
    voxel_distance_from_margin : int
        The distance, in voxels, from the stack margin to use to defines cells at the stack margins. `2` by default.
    exclude_marginal_cells : bool
        Set this to ``True`` to exclude marginal cells from cell feature computation.

    Returns
    -------
    timagetk.graphs.TissueGraph
        The primal (and dual) graph(s) from the segmented image.

    See Also
    --------
    timagetk.tasks.features.CELL_FEATURES
    timagetk.tasks.features.WALL_FEATURES

    Notes
    -----
    The topologocal element coordinates are:
      - the cells barycenter (node of primal graph)
      - the cell wall_ids geometric median coordinates (edge of primal graph)
      - the cell vertices coordinates (node of dual graph)
      - the cell edge geometric median coordinates (edge of dual graph)

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.tasks.tissue_graph import tissue_graph_from_image
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk import TissueImage3D
    >>> tissue = imread(shared_dataset("sphere", "intensity")[0], TissueImage3D, background=1, not_a_label=0)
    >>> tissue.cells.set_feature('random', {cid: np.random.randint(0, 100) for cid in tissue.cells.ids()})
    >>> tg = tissue_graph_from_image(tissue, features=['volume', 'random'], wall_features='all', extract_dual=False)
    >>> # List obtained features by types:
    >>> print(tg.list_cell_properties())
    >>> print(tg.list_cell_wall_properties())
    >>> print(tg.list_cell_edge_properties())
    >>> print(tg.list_cell_vertex_properties())
    >>> # Overview of the features by types and names:
    >>> {cf: len(tg.cell_property_dict(cf, only_defined=True)) for cf in tg.list_cell_properties()}
    >>> {wf: len(tg.cell_wall_property_dict(wf, only_defined=True)) for wf in tg.list_cell_wall_properties()}
    >>> {ef: len(tg.cell_edge_property_dict(ef, only_defined=True)) for ef in tg.list_cell_edge_properties()}
    >>> {vf: len(tg.cell_vertex_property_dict(vf, only_defined=True)) for vf in tg.list_cell_vertex_properties()}

    >>> tg = tissue_graph_from_image(tissue, wall_area_threshold=5.0, features=['area', 'volume'])

    >>> tg.add_cell_property('number_of_neighbors', tissue.cells.number_of_neighbors(), "")  # add the cell number of neighbors property
    >>> tg.add_cell_wall_property('area', tissue.walls.area(), "µm²")  # add the cell wall area property
    >>> tg.cell_properties()
    >>> cell_barycenters = tg.cell_property_dict('barycenter')
    >>> cell_wall_medians = tg.cell_wall_property_dict('medians')
    >>> tg.cell_property_dict('volume',)
    >>> tg.primal.adj[1]
    >>> list(tg.primal.neighbors(1))
    >>> from networkx.linalg import adjacency_matrix
    >>> adjacency_matrix(tg.primal, [1])

    """
    assert isinstance(tissue, (TissueImage2D, TissueImage3D))
    # KWARGS processing:
    real = kwargs.get('real', True)
    extract_dual = kwargs.get('extract_dual', True)
    verbose = kwargs.get('verbose', True)
    wall_area_threshold = kwargs.get('wall_area_threshold', None)
    epidermis_area_threshold = kwargs.get('epidermis_area_threshold', wall_area_threshold)
    exclude_marginal_cells = kwargs.get('exclude_marginal_cells', False)
    v_dist_from_margin = kwargs.get('voxel_distance_from_margin', 2)

    # Invert image if not an upright microscope:
    if kwargs.get('orientation', 1) == -1:
        tissue.invert_axis("z")

    # Get or create an empty TissueGraph
    tg = kwargs.get('graph', TissueGraph(verbose=verbose))

    if verbose:
        log.info(f"Starting TissueGraph generation from '{tissue.filename}' tissue image...")
    t_start = time.time()  # create a timer

    # Set the background id if defined in the image
    if tissue.background is not None:
        tg.background = tissue.background

    # -- PRIMAL GRAPH:
    # - ADD CELLS
    # Get cell ids and add them to the tissue graph:
    if verbose:
        log.info("Detecting cells...")
    cell_ids = tissue.cells.ids(cell_ids)  # The list of cell to compute property for...
    if verbose:
        log.info(f"Background in list of cell_ids: {tissue.background in cell_ids}")
        log.info(f"Found {len(cell_ids)} cells in the tissue!")
    tg.add_cells(cell_ids)

    if verbose:
        log.info("Detecting cells at stack margins...")
    marginal_cells = labels_at_stack_margins(tissue, v_dist_from_margin)
    marginal_cells = list(set(marginal_cells) - {tissue.background})
    if len(marginal_cells) > 0:
        tg.add_cell_identity('stack_margin', marginal_cells)
    if verbose:
        log.info(f"Found {len(marginal_cells)} cells at less than {v_dist_from_margin} voxels from the stack margins!")
    if exclude_marginal_cells:
        cell_ids = list(set(cell_ids) - set(marginal_cells))
        if verbose:
            log.info(
                f"Excluded the cells at stack margins from property computation, continuing with {len(cell_ids)} cells...")
    n_graph_cids = len(tg.cell_ids())

    # - ADD CELL-WALLS
    # Get wall ids and add them to the tissue graph as edges between cells:
    if verbose:
        if wall_area_threshold is None:
            log.info("Detecting cell-walls without any area threshold...")
        else:
            log.info(f"Detecting cell-walls with a minimum area threshold of {wall_area_threshold}µm²...")
            # TODO: need to save this `wall_area_threshold` somewhere in the graph...

    if isinstance(tissue, TissueImage3D):
        neighborhood = tissue.cells.neighbors(cell_ids, min_area=wall_area_threshold, real=real)
    else:
        neighborhood = tissue.cells.neighbors(cell_ids)

    wall_ids = list({stuple((cid, nid)) for cid, nids in neighborhood.items() for nid in nids})
    log.debug(f"Adding cell-wall_ids: {wall_ids}")
    tg.add_cell_walls(wall_ids)
    # Track potential changes to the list of primal nodes due to edge update
    n_new = len(tg.cell_ids()) - n_graph_cids
    if n_new > 0:
        log.warning(f"This added {n_new} new cell{'s' if n_new > 1 else ''}!")
        n_graph_cids = len(tg.cell_ids())

    if tissue.background is not None:
        tg = add_cell_layers(tg, **kwargs)

    # -- DUAL GRAPH:
    if extract_dual:
        if verbose:
            log.info("Extraction of the dual graph...")
        # Extract the dual graph:
        if isinstance(tissue, TissueImage3D):
            cell_vertex_ids, vtx_pairs_to_edges = dual_graph_extraction(tissue, verbose=verbose)
        else:
            cell_vertex_ids, vtx_pairs_to_edges = dual_graph_extraction2D(tissue, verbose=verbose)
        vtx_pairs = list(vtx_pairs_to_edges.keys())  # list of cell-edges as pairs of cell-vertex (edges extremities)
        # - ADD CELL-VERTICES
        tg.add_cell_vertices(cell_vertex_ids)
        n_cvids = len(tg.cell_vertex_ids())
        # - ADD CELL-EDGES
        tg.add_cell_edges(vtx_pairs)
        # Track potential changes to the list of dual nodes due to edge update
        if len(tg.cell_vertex_ids()) != n_cvids:
            n_new = len(tg.cell_vertex_ids()) - n_cvids
            log.warning(f"This added {n_new} new cell-vert{'ex' if n_new > 1 else 'ices'}!")
        tg.add_cell_edge_property('linel_id', vtx_pairs_to_edges)
    else:
        if verbose:
            log.info("Extraction of the dual graph was NOT required!")

    unit_str = tissue.get_unit(short=True) if real else 'voxel'
    # Pre-compute topological elements coordinates:
    if verbose:
        log.info("Computing the coordinates of the topological elements...")
    if coordinates and extract_dual:
        tissue.topological_elements([2, 1, 0], verbose=verbose)
    else:
        tissue.topological_elements([2], verbose=verbose)
    # Add required topological elements coordinates to TissueGraph:
    if coordinates:
        # - Compute the cell barycenter:
        bary = tissue.cells.barycenter(cell_ids, real=real)
        tg.add_cell_property('barycenter', bary, unit_str)
        # - Compute the cell-wall geometric medians:
        med_coords = tissue.walls.geometric_median(wall_ids, real=real)
        tg.add_cell_wall_property('median', med_coords, unit_str)
    if coordinates and extract_dual:
        # - Compute the cell-vertices coordinates:
        cell_vertex_ids = tg.cell_vertex_ids()
        cell_vertex_coords = tissue.vertices.coordinates(cell_vertex_ids, real=real)
        tg.add_cell_vertex_property('coordinate', cell_vertex_coords, unit_str)
        # - Compute the cell-edge geometric medians:
        cell_edge_ids = tg.cell_edge_ids()
        pp2linel = tg.cell_edge_property_dict('linel_id')
        cell_edge_ids = [pp2linel[k] for k in cell_edge_ids if pp2linel[k] is not None]
        ce_medians = tissue.edges.geometric_median(cell_edge_ids, real=real)
        linel2pp = {v: k for k, v in pp2linel.items() if v is not None}
        ce_ppty = {linel2pp[lid]: med for lid, med in ce_medians.items() if lid in linel2pp}
        tg.add_cell_edge_property('median', ce_ppty, unit_str)

    if tissue.background is not None:
        # Detect epidermal cells:
        ep_cell_ids = tissue.epidermal_cell_ids(epidermis_area_threshold, real=real)
        tg.add_cell_identity('epidermis', ep_cell_ids)
        if coordinates:
            ep_walls = [(tg.background, cid) for cid in ep_cell_ids]
            ep_medians = tissue.walls.geometric_median(ep_walls, real=real)
            ep_medians = {cid: v for (_, cid), v in ep_medians.items()}
            tg.add_cell_property('epidermis_median', ep_medians, unit=unit_str)

    # Compute cell features, if any:
    if features is None:
        features = []
    elif features == 'all':
        features = list(set(CELL_FEATURES) | set(tissue.cells.feature_names()))
    else:
        features_set = set(features) & (set(CELL_FEATURES) | set(tissue.cells.feature_names()))
        if len(features_set) == 0 and verbose:
            log.warning("None of the specified cell features were found in the list of available features!")
            log.info(f"Specified features: {features}")
        features = list(features_set)

    if features != [] and verbose:
        log.info(f"Starting computation of {len(features)} cell features:")
        log.info(f"{', '.join(features)}")

    if 'area' in features:
        tg.add_cell_property('area',
                             tissue.cells.area(cell_ids, real=real), unit=f'{unit_str}²')
        features.remove('area')
    if 'barycenter' in features and 'barycenter' not in tg.list_cell_properties():
        tg.add_cell_property('barycenter',
                             tissue.cells.barycenter(cell_ids, real=real), unit=unit_str)
        features.remove('barycenter')
    if 'inertia_axis' in features:
        tg.add_cell_property('inertia_axis',
                             tissue.cells.inertia_axis(cell_ids, real=real), unit='')
        features.remove('inertia_axis')
    if 'number_of_neighbors' in features:
        tg.add_cell_property('number_of_neighbors',
                             tissue.cells.number_of_neighbors(cell_ids), unit='')
        features.remove('number_of_neighbors')
    if 'principal_direction_norms' in features:
        tg.add_cell_property('principal_direction_norms',
                             tissue.cells.principal_direction_norms(cell_ids, real=real), unit=unit_str)
        features.remove('principal_direction_norms')
    if 'shape_anisotropy' in features:
        tg.add_cell_property('shape_anisotropy',
                             tissue.cells.shape_anisotropy(cell_ids, real=real), unit='')
        features.remove('shape_anisotropy')
    if 'volume' in features:
        tg.add_cell_property('volume',
                             tissue.cells.volume(cell_ids, real=real), unit=f'{unit_str}³')
        features.remove('volume')

    if len(features) != 0:
        for feature in features:
            try:
                tg.add_cell_property(feature, tissue.cells.feature(feature, copy=True))
            except KeyError:
                log.error(f"Could not find feature '{feature}' in the computed cell features!")

    # Compute cell-wall features, if any:
    if wall_features is None:
        wall_features = []
    elif wall_features == 'all':
        wall_features = WALL_FEATURES
    else:
        wall_features_set = set(wall_features) & set(WALL_FEATURES)
        if len(wall_features_set) == 0 and verbose:
            log.warning("None of the specified cell-wall features were found in the list of available features!")
            log.info(f"Specified features: {wall_features}")
        wall_features = wall_features_set

    if wall_features != [] and verbose:
        log.info(f"Starting computation of {len(wall_features)} cell-wall features:")
        log.info(f"{', '.join(wall_features)}")

    if 'area' in wall_features:
        tg.add_cell_wall_property('area', tissue.walls.area(wall_ids, real=real), unit=f'{unit_str}²')
        # TODO: stop duplicating data...
        # Select epidermal cells:
        ep_walls = [(tg.background, cid) for cid in ep_cell_ids]
        ep_area = tissue.walls.area(ep_walls, real=real)
        ep_area = {cid: v for (_, cid), v in ep_area.items()}
        tg.add_cell_property('epidermis_area', ep_area, unit=f'{unit_str}²')

    elapsed_time = time.time() - t_start
    if elapsed_time > 3600:
        elapsed_time = time.strftime('%H hour %M min %S sec', time.gmtime(elapsed_time))
    else:
        elapsed_time = time.strftime('%M min %S sec', time.gmtime(elapsed_time))
    if verbose:
        log.info(f"Done extracting the TissueGraph from '{tissue.filename}' in {elapsed_time}!")
    return tg
