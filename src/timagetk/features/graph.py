#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Graph based algorithms to compute & add features from/to graphs."""

import numpy as np

from timagetk.bin.logger import get_logger
from timagetk.features.array_tools import affine_deformation_tensor
from timagetk.features.array_tools import pointset_subspace_reduction
from timagetk.features.array_tools import strain_anisotropy_2d
from timagetk.features.array_tools import stretch_tensors
from timagetk.util import stuple

log = get_logger(__name__)

DEFORMATION_FEATURES = ['epidermis_growth', 'cell_growth']

DEP_FEATURES = {
    'landmarks': ['affine_deformation_matrix', 'affine_deformation_r2',
                  'growth_tensor_before', 'growth_tensor_after', 'growth_rates',
                  'volumetric_strain_rates'],
    'epidermis_landmarks': ['epidermis_affine_deformation_matrix', 'epidermis_affine_deformation_r2',
                            'epidermis_growth_tensor_before', 'epidermis_growth_tensor_after', 'epidermis_growth_rates',
                            'epidermis_areal_strain_rates', 'epidermis_strain_anisotropy'],
}


def _ldmk_pair_values(ids, values1, values2, max_missing_pc=0.):
    missing = 0
    pairs = [[], []]
    for i in ids:
        if values1[i] is not None and values2[i] is not None:
            pairs[0].append(list(values1[i]))
            pairs[1].append(list(values2[i]))
        else:
            missing += 1
        if missing / float(len(ids)) > max_missing_pc:
            return None
    return pairs


def epidermis_deformation(temp_tissue_graph):
    """Creates "epidermis_landmarks" cell feature required for epidermis deformation computation.

    Parameters
    ----------
    temp_tissue_graph : timagetk.graphs.TemporalTissueGraph
        The temporal tissue graph to populate with "epidermis_landmarks" cell feature.
        This feature is required for epidermis deformation computation.

    Returns
    -------
    timagetk.graphs.TemporalTissueGraph
        The populated temporal tissue graph.

    """
    for t in range(temp_tissue_graph.nb_time_points - 1):
        tg = temp_tissue_graph._tissue_graph[t]
        # Get the list of epidermal cell ids at that time:
        ep_cids = set(tg.epidermal_cell_ids())
        if len(ep_cids) > 0:
            log.info(f"Found {len(ep_cids)} epidermal cell at time t{t}.")
        else:
            log.critical(f"No epidermal cell found at time t{t}!")
            return None
        # See if it should be filtered by a list of marginal cells:
        if "stack_margin" in tg.list_cell_properties():
            marginal_cells = {mc for mc, v in tg.cell_property_dict("stack_margin").items() if v}
            marginal_cells = ep_cids & marginal_cells
            ep_cids = ep_cids - marginal_cells
            log.info(
                f"Found {len(marginal_cells)} marginal cells to exclude from epidermal cell-walls landmarks computation at time {t}!")
        # Keep only those with at least a descendant:
        ep_cids = [cid for cid in ep_cids if temp_tissue_graph.has_descendant(t, cid)]
        if len(ep_cids) > 0:
            log.info(f"Found {len(ep_cids)} epidermal cell at time t{t} with a descendant.")
        else:
            log.critical(f"No epidermal cell with a descendant found at time t{t}!")
            return None
        expected_ldmks = len(ep_cids)
        # Gather landmarks for each epidermal cell-wall:
        ldmks = {}
        for ep_cid in ep_cids:
            ldmks[ep_cid] = epidermal_cell_landmarks(temp_tissue_graph, (t, ep_cid), verbose=False)
        # Filter returned landmarks to keep only those with coordinates:
        ldmks = {cid: ldmk for cid, ldmk in ldmks.items() if all(len(ldmk[i]) != 0 for i in [0, 1])}
        # Print a summary of this step:
        log.info(f"Obtained {len(ldmks)} epidermal cell-walls with landmarks at time {t}!")
        pc = len(ldmks) / float(expected_ldmks) * 100
        log.info(f"Expected to obtain {expected_ldmks}, so we have a {np.round(pc, 2)}% completion rate!")
        # Add this dictionary as cell property 'epidermis_landmarks':
        tg.add_cell_property('epidermis_landmarks', ldmks)

    return temp_tissue_graph


def cell_deformation(temp_tissue_graph):
    """Creates "landmarks" cell feature required for cell deformation computation.

    Parameters
    ----------
    temp_tissue_graph : timagetk.graphs.TemporalTissueGraph
        The temporal tissue graph to populate with "landmarks" cell feature.
        This feature is required for cell deformation computation.

    Returns
    -------
    timagetk.graphs.TemporalTissueGraph
        The populated temporal tissue graph.

    """
    for t in range(temp_tissue_graph.nb_time_points - 1):
        tg = temp_tissue_graph._tissue_graph[t]
        # Get the list of cell ids at that time:
        cids = set(tg.cell_ids())
        # See if it should be filtered by a list of marginal cells:
        if "stack_margin" in tg.list_cell_properties():
            marginal_cells = {mc for mc, v in tg.cell_property_dict("stack_margin").items() if v}
            marginal_cells = cids & marginal_cells
            cids = cids - marginal_cells
            log.info(
                f"Found {len(marginal_cells)} marginal cells to exclude from cell landmarks computation at time {t}!")
        # Keep only those with at least a descendant:
        cids = [cid for cid in cids if temp_tissue_graph.has_descendant(t, cid)]
        expected_ldmks = len(cids)
        # Gather landmarks for each cell:
        ldmks = {}
        for cid in cids:
            ldmks[cid] = cell_landmarks(temp_tissue_graph, (t, cid), verbose=False)
        # Filter returned landmarks to keep only those with coordinates:
        ldmks = {cid: ldmk for cid, ldmk in ldmks.items() if all(len(ldmk[i]) != 0 for i in [0, 1])}
        # Print a summary of this step:
        log.info(f"Obtained {len(ldmks)} cells with landmarks at time {t}!")
        pc = len(ldmks) / float(expected_ldmks) * 100
        log.info(f"Expected to obtain {expected_ldmks}, so we have a {np.round(pc, 2)}% completion rate!")
        # Add this dictionary as cell property 'landmarks':
        tg.add_cell_property('landmarks', ldmks)

    return temp_tissue_graph


def epidermis_growth(temporal_graph, time_points=None, **kwargs):
    """Compute the epidermis growth rates and directions for all epidermal cells with an 'epidermis_landmarks' property.

    Parameters
    ----------
    temporal_graph : timagetk.graphs.TemporalTissueGraph
        The temporal tissue graph to populate with new features, require an 'epidermis_landmarks' cell property.
    time_points : int or list, optional
        If ``None`` (default) compute the epidermis growth for all time-point.
        Else, should be a list of valid time-points to select for computation.

    Other Parameters
    ----------------
    max_missing_percent : float
        Percentage, in [0, 1], of missing values allowed on cell-edges and cell-vertices pairings. ``0.2`` by default.

    Returns
    -------
    timagetk.graphs.TemporalTissueGraph
        The temporal tissue graph augmented with epidermal cell-wall growth related features.

    See Also
    --------
    timagetk.tasks.temporal_tissue_graph.temporal_tissue_graph
    timagetk.tasks.temporal_tissue_graph.deformation_features
    timagetk.features.graph.epidermis_deformation

    Notes
    -----
    Use the ``temporal_tissue_graph`` method with 'epidermis_deformation' in ``features`` parameter or
    ``deformation_features`` and ``epidermis_deformation`` method prior to this one to get a temporal tissue graph
    with an 'epidermis_landmarks' property.

    Examples
    --------
    >>> from timagetk.tasks.temporal_tissue_graph import temporal_tissue_graph_from_images
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.temporal_tissue_graph import fused_tissue_graph
    >>> from timagetk.features.graph import epidermis_growth
    >>> from timagetk.features.graph import epidermis_deformation
    >>> tissues = shared_dataset("p58", 'segmented')
    >>> lineages = shared_dataset("p58", 'lineage')
    >>> ts = shared_dataset("p58", 'time-points')
    >>> ttg = temporal_tissue_graph_from_images(tissues,lineages,ts,features=['area', 'volume'], extract_dual=True)
    >>> ttg = fused_tissue_graph(ttg, tissues, lineages, real=True)
    >>> ttg = epidermis_deformation(ttg)
    >>> ttg = epidermis_growth(ttg)
    >>> ep_gr = ttg._tissue_graph[0].cell_property_dict('epidermis_growth_rates')
    >>> {cid: gr for cid, gr in ep_gr.items() if gr is not None}

    """
    mmpc = kwargs.get('max_missing_percent', 0.2)
    # Make sure specified time-points are valid:
    valid_time_points = list(range(temporal_graph.nb_time_points - 1))
    if time_points is None:
        time_points = valid_time_points
    elif isinstance(time_points, int):
        try:
            assert time_points != temporal_graph.nb_time_points
        except AssertionError:
            log.critical("The requested time-point is the last one, this is not possible!")
            return temporal_graph
        else:
            time_points = [time_points]
    elif isinstance(time_points, list):
        time_points = list(set(time_points) & set(valid_time_points))
    else:
        log.critical(f"Could not make sense of given `time-points`: {time_points}!")
        return temporal_graph

    aff_def, aff_r2 = {}, {}
    stretch_before, stretch_after = {}, {}
    strain_rate = {}
    for tp in time_points:
        log.info(f"Computing epidermal cell-wall growth from t{tp} to t{tp + 1}...")
        tg = temporal_graph._tissue_graph[tp]
        try:
            assert 'epidermis_landmarks' in tg.list_cell_properties()
        except AssertionError:
            log.error(f"The tissue graph at t{tp} does not have the required 'epidermis_landmarks' cell property!")
            continue
        interval = temporal_graph.time_interval(tp, tp + 1)
        low_score = []
        for ep_cid in tg.epidermal_cell_ids():
            ldmk_before, ldmk_after = tg.cell_property(ep_cid, 'epidermis_landmarks', default=[[], []])
            if len(ldmk_before) <= 3:
                continue  # SKIP this epidermal cell as not enough landmarks has been found!
            score = landmarks_score(ldmk_before, ldmk_after)
            if 1 - score > mmpc:
                low_score += [ep_cid]
                continue  # SKIP this epidermal cell as it's landmarks definition score is too low!
            ldmk_before, ldmk_after = clean_landmarks(ldmk_before, ldmk_after)
            ldmk_before, ldmk_after = ldmk_before - ldmk_before[0], ldmk_after - ldmk_after[0]
            _, flat_ldmk_before = pointset_subspace_reduction(ldmk_before, 2, ldmk_before)
            _, flat_ldmk_after = pointset_subspace_reduction(ldmk_after, 2, ldmk_after)
            aff_def[ep_cid], aff_r2[ep_cid] = affine_deformation_tensor(flat_ldmk_before, flat_ldmk_after)
            stretch_before[ep_cid], stretch_norm, stretch_after[ep_cid] = stretch_tensors(aff_def[ep_cid])
            strain_rate[ep_cid] = [np.log(dk) / float(interval) for dk in stretch_norm]
        tg.add_cell_property('epidermis_affine_deformation_matrix', aff_def)
        tg.add_cell_property('epidermis_affine_deformation_r2', aff_r2)
        tg.add_cell_property('epidermis_growth_tensor_before', stretch_before)
        tg.add_cell_property('epidermis_growth_tensor_after', stretch_after)
        tg.add_cell_property('epidermis_growth_rates', strain_rate, 'µm.h⁻¹')
        areal_strain_rates = {i: np.sum(r) for i, r in strain_rate.items()}
        tg.add_cell_property('epidermis_areal_strain_rates', areal_strain_rates, 'µm².h⁻¹')
        strain_anisotropy = {i: strain_anisotropy_2d(r) for i, r in strain_rate.items()}
        tg.add_cell_property('epidermis_strain_anisotropy', strain_anisotropy, 'h⁻¹')

        if low_score != []:
            total = len(aff_def) + len(low_score)
            pc_low = len(low_score) / float(total) * 100
            log.warning(
                f"Got {len(low_score)} epidermal cell-walls (out of {total}, so {round(pc_low, 1)}%) with more than {round(mmpc * 100, 1)}% of missing landmarks at t{tp}!")
            log.warning(f"{low_score}")

    return temporal_graph


def cell_growth(temporal_graph, time_points=None, **kwargs):
    """Compute the cell growth rates and directions for all cells with a 'cell_landmarks' property.

    Parameters
    ----------
    temporal_graph : timagetk.graphs.TemporalTissueGraph
        The temporal tissue graph to populate with new features, require an 'cell_landmarks' cell property.
    time_points : int or list, optional
        If ``None`` (default) compute the cell growth for all time-point.
        Else, should be a list of valid time-points to select for computation.

    Other Parameters
    ----------------
    max_missing_percent : float
        Percentage, in [0, 1], of missing values allowed on cell-edges and cell-vertices pairings. ``0.2`` by default.

    Returns
    -------
    timagetk.graphs.TemporalTissueGraph
        the temporal tissue graph augmented with cell growth related features.

    See Also
    --------
    timagetk.tasks.temporal_tissue_graph.temporal_tissue_graph
    timagetk.tasks.temporal_tissue_graph.deformation_features
    timagetk.features.graph.cell_deformation

    Notes
    -----
    Use the ``temporal_tissue_graph`` method with 'deformation' in ``features`` parameter or
    ``deformation_features`` and ``cell_deformation`` methods prior to this one to get a temporal tissue graph
    with a 'cell_landmarks' property.

    Examples
    --------
    >>> from timagetk.tasks.temporal_tissue_graph import temporal_tissue_graph_from_images
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.features.graph import cell_growth
    >>> tissues = shared_dataset("p58", 'segmented')[:2]
    >>> lineages = shared_dataset("p58", 'lineage')[:1]
    >>> ts = shared_dataset("p58", 'time-points')[:2]
    >>> ttg = temporal_tissue_graph_from_images(tissues,lineages,ts,features=['area', 'volume', 'epidermis_deformation'])
    >>> ttg = cell_growth(ttg)
    >>> cgr = ttg._tissue_graph[0].cell_property_dict('growth_rates')
    >>> {cid: gr for cid, gr in cgr.items() if gr is not None}

    """
    mmpc = kwargs.get('max_missing_percent', 1 / 5.)
    # Make sure specified time-points are valid:
    valid_time_points = list(range(temporal_graph.nb_time_points - 1))
    if time_points is None:
        time_points = valid_time_points
    elif isinstance(time_points, int):
        try:
            assert time_points != temporal_graph.nb_time_points
        except AssertionError:
            log.critical("The requested time-point is the last one, this is not possible!")
            return temporal_graph
        else:
            time_points = [time_points]
    elif isinstance(time_points, list):
        time_points = list(set(time_points) & set(valid_time_points))
    else:
        log.critical(f"Could not make sense of given `time-points`: {time_points}!")
        return temporal_graph

    aff_def, aff_r2 = {}, {}
    stretch_before, stretch_after = {}, {}
    strain_rate = {}
    for tp in time_points:
        log.info(f"Computing cell growth from t{tp} to t{tp + 1}...")
        tg = temporal_graph._tissue_graph[tp]
        try:
            assert 'landmarks' in tg.list_cell_properties()
        except AssertionError:
            log.error(f"The tissue graph at t{tp} does not have the required 'landmarks' cell property!")
            continue
        interval = temporal_graph.time_interval(tp, tp + 1)
        low_score = []
        for cid in tg.cell_ids():
            ldmk_before, ldmk_after = tg.cell_property(cid, 'landmarks', default=[[], []])
            if len(ldmk_before) <= 3:
                continue  # SKIP this cell as not enough landmarks has been found!
            score = landmarks_score(ldmk_before, ldmk_after)
            if 1 - score > mmpc:
                low_score += [cid]
                continue  # SKIP this cell as it's landmarks definition score is too low!
            ldmk_before, ldmk_after = clean_landmarks(ldmk_before, ldmk_after)
            ldmk_before, ldmk_after = ldmk_before - ldmk_before[0], ldmk_after - ldmk_after[0]
            aff_def[cid], aff_r2[cid] = affine_deformation_tensor(ldmk_before, ldmk_after)
            stretch_before[cid], stretch_norm, stretch_after[cid] = stretch_tensors(aff_def[cid])
            strain_rate[cid] = [np.log(dk) / float(interval) for dk in stretch_norm]
        tg.add_cell_property('affine_deformation_matrix', aff_def)
        tg.add_cell_property('affine_deformation_r2', aff_r2)
        tg.add_cell_property('growth_tensor_before', stretch_before)
        tg.add_cell_property('growth_tensor_after', stretch_after)
        tg.add_cell_property('growth_rates', strain_rate, 'µm.h⁻¹')
        volumetric_strain_rates = {i: np.sum(r) for i, r in strain_rate.items()}
        tg.add_cell_property('volumetric_strain_rates', volumetric_strain_rates, 'µm³.h⁻¹')

        if low_score != []:
            total = len(aff_def) + len(low_score)
            pc_low = len(low_score) / float(total) * 100
            log.warning(
                f"Got {len(low_score)} cells (out of {total}, so {round(pc_low, 1)}%) with more than {round(mmpc * 100, 1)}% of missing landmarks at t{tp}!")

    return temporal_graph


def epidermal_cell_landmarks(ttg, tcid, verbose=True):
    """Gather the landmarks of a cell using a temporal tissue graph with required properties.

    Parameters
    ----------
    ttg : timagetk.graphs.TemporalTissueGraph
        A temporal tissue graph with the required properties.
    tcid : tuple
        The (time-point, cell_id) tuple indicating the cell of the temporal tissue graph to work with.
    verbose : bool, optional
        Control the method verbosity level, ``True`` by default.

    Returns
    -------
    numpy.ndarray
        The array of epidermal landmarks before deformation.
    numpy.ndarray
        The array of epidermal landmarks after deformation.

    Notes
    -----
    The returned array can be empty if:
      - the cell belong to the last time point
      - the cell has no descendant
      - the cell belong to the stack margin (require prior definition of a 'stack_margin' cell property)
      - the cell does not belong to the epidermis (require prior definition of an 'epidermis' cell property)
      - the cell is missing value(s) for any of the cell properties 'epidermis_median' & 'fused_epidermis_median'
    The required properties are:
      - cell properties 'epidermis_median' & 'fused_epidermis_median'
      - cell-edge properties 'median' & 'fused_median'
      - cell-vertex properties 'coordinate' & 'fused_coordinate'

    Examples
    --------
    >>> from timagetk.tasks.temporal_tissue_graph import temporal_tissue_graph_from_images
    >>> from timagetk.tasks.temporal_tissue_graph import fused_tissue_graph
    >>> from timagetk.features.graph import epidermal_cell_landmarks
    >>> from timagetk.io.dataset import shared_data
    >>> tissues = shared_dataset("p58", 'segmented')[:2]
    >>> lineages = shared_dataset("p58", 'lineage')[:1]
    >>> tp = shared_dataset("p58", 'time-points')[:2]
    >>> ttg = temporal_tissue_graph_from_images(tissues,lineages,tp)
    >>> ttg = fused_tissue_graph(ttg, tissues, lineages, real=True)
    >>> epidermal_cell_landmarks(ttg, (0, 4))

    """
    t, cid = tcid
    ldmks = []
    if t == ttg.nb_time_points:
        if verbose:
            log.error(f"Time {t} is the last time-point, so no landmarks can be defined!")
        return np.array([]), np.array([])
    if not ttg.has_descendant(t, cid):
        if verbose:
            log.error(f"Cell {cid} from time {t} has no lineage, so no landmarks can be defined!")
        return np.array([]), np.array([])
    if ttg.cell_property(t, cid, "stack_margin", False):
        if verbose:
            log.error(f"Cell {cid} from time {t} is at the stack margins, so no landmarks can be defined!")
        return np.array([]), np.array([])
    if not ttg.cell_property(t, cid, "epidermis", False):
        if verbose:
            log.error(f"Cell {cid} from time {t} does not belong to the epidermis, so no landmarks can be defined!")
        return np.array([]), np.array([])

    # The 'epidermis_median' & 'fused_epidermis_median' cell properties are required to define landmarks as they are use to center the points!
    ep_med = ttg.cell_property(t, cid, 'epidermis_median', default=None)
    fep_med = ttg.cell_property(t, cid, 'fused_epidermis_median', default=None)
    if ep_med is None:
        if verbose:
            log.warning(f"Missing epidermal cell-wall median coordinate for cell {cid} at time {t}!")
    if fep_med is None:
        if verbose:
            log.warning(f"Missing fused epidermal cell-wall median coordinate for cell {cid} at time {t}!")
    if ep_med is None or fep_med is None:
        return np.array([]), np.array([])
    else:
        ldmks.append([ep_med, fep_med])

    ce_ids = ttg._tissue_graph[t].epidermal_cell_edge_ids(cid)
    for ceid in ce_ids:
        med = ttg.cell_edge_property(t, ceid, 'median', [np.nan] * 3)
        fmed = ttg.cell_edge_property(t, ceid, 'fused_median', [np.nan] * 3)
        ldmks.append([med, fmed])

    cv_ids = ttg._tissue_graph[t].epidermal_cell_vertex_ids(cid)
    for cvid in cv_ids:
        coords = ttg.cell_vertex_property(t, cvid, 'coordinate', [np.nan] * 3)
        fcoords = ttg.cell_vertex_property(t, cvid, 'fused_coordinate', [np.nan] * 3)
        ldmks.append([coords, fcoords])

    ldmks = np.array(ldmks)
    return ldmks[:, 0], ldmks[:, 1]


def cell_landmarks(ttg, tcid, verbose=True):
    """Gather the landmarks of a cell using a temporal tissue graph with required properties.

    Parameters
    ----------
    ttg : timagetk.graphs.TemporalTissueGraph
        A temporal tissue graph with the required properties.
    tcid : tuple
        The (time-point, cell_id) tuple indicating the cell of the temporal tissue graph to work with.
    verbose : bool, optional
        Control the method verbosity level, ``True`` by default.

    Returns
    -------
    numpy.ndarray
        The array of landmarks before deformation.
    numpy.ndarray
        The array of landmarks after deformation.

    Notes
    -----
    The returned array can be empty if:
      - the cell belong to the last time point
      - the cell has no descendant
      - the cell belong to the stack margin (require prior definition of a 'stack_margin' cell property)
      - the cell is missing value(s) for any of the cell properties 'barycenter' & 'fused_barycenter'
    The required properties are:
      - cell properties 'barycenter' & 'fused_barycenter'
      - cell properties 'epidermis_median' & 'fused_epidermis_median' if 'epidermis' is defined
      - cell-wall properties 'median' & 'fused_median' if 'epidermis' is defined
      - cell-edge properties 'median' & 'fused_median'
      - cell-vertex properties 'coordinate' & 'fused_coordinate'

    Examples
    --------
    >>> from timagetk.tasks.temporal_tissue_graph import temporal_tissue_graph_from_images
    >>> from timagetk.tasks.temporal_tissue_graph import fused_tissue_graph
    >>> from timagetk.features.graph import epidermal_cell_landmarks
    >>> from timagetk.io.dataset import shared_data
    >>> tissues = shared_dataset("p58", 'segmented')[:2]
    >>> lineages = shared_dataset("p58", 'lineage')[:1]
    >>> tp = shared_dataset("p58", 'time-points')[:2]
    >>> ttg = temporal_tissue_graph_from_images(tissues,lineages,tp)
    >>> ttg = fused_tissue_graph(ttg, tissues, lineages, real=True)
    >>> cell_landmarks(ttg, (0, 4))

    """
    t, cid = tcid
    ldmks = []

    if t == ttg.nb_time_points:
        if verbose:
            log.error(f"Time {t} is the last time-point, so no landmarks can be defined!")
        return np.array([]), np.array([])
    if not ttg.has_descendant(t, cid):
        if verbose:
            log.error(f"Cell {cid} from time {t} has no lineage, so no landmarks can be defined!")
        return np.array([]), np.array([])
    if ttg.cell_property(t, cid, "stack_margin", False):
        if verbose:
            log.error(f"Cell {cid} from time {t} is at the stack margins, so no landmarks can be defined!")
        return np.array([]), np.array([])

    # The 'barycenter' & 'fused_barycenter' cell properties are required to define landmarks as they are use to center the points!
    bary = ttg.cell_property(t, cid, 'barycenter', default=None)
    fbary = ttg.cell_property(t, cid, 'fused_barycenter', default=None)
    if bary is None:
        if verbose:
            log.warning(f"Missing barycenter coordinate for cell {cid} at time {t}!")
    if fbary is None:
        if verbose:
            log.warning(f"Missing fused cell barycenter coordinate for cell {cid} at time {t}!")
    if bary is None or fbary is None:
        return np.array([]), np.array([])
    else:
        ldmks.append([bary, fbary])

    if ttg.cell_property(t, cid, "epidermis", False):
        ep_med = ttg.cell_property(t, cid, 'epidermis_median', default=None)
        fep_med = ttg.cell_property(t, cid, 'fused_epidermis_median', default=None)
        if ep_med is None:
            if verbose:
                log.warning(f"Missing epidermal cell-wall median coordinate for cell {cid} at time {t}!")
            ep_med = [np.nan] * 3
        if fep_med is None:
            if verbose:
                log.warning(f"Missing fused epidermal cell-wall median coordinate for cell {cid} at time {t}!")
            fep_med = [np.nan] * 3
        ldmks.append([ep_med, fep_med])

    cw_ids = ttg._tissue_graph[t].cell_wall_ids(cid)
    if ttg.cell_property(t, cid, "epidermis", False):
        try:
            cw_ids.remove(stuple((ttg._tissue_graph[t].background, cid)))
        except:
            pass
    for cwid in cw_ids:
        med = ttg.cell_wall_property(t, cwid, 'median', [np.nan] * 3)
        fmed = ttg.cell_wall_property(t, cwid, 'fused_median', [np.nan] * 3)
        ldmks.append([med, fmed])

    ce_ids = ttg._tissue_graph[t].cell_edge_ids(cid)
    for ceid in ce_ids:
        med = ttg.cell_edge_property(t, ceid, 'median', [np.nan] * 3)
        fmed = ttg.cell_edge_property(t, ceid, 'fused_median', [np.nan] * 3)
        ldmks.append([med, fmed])

    cv_ids = ttg._tissue_graph[t].cell_vertex_ids(cid)
    for cvid in cv_ids:
        coords = ttg.cell_vertex_property(t, cvid, 'coordinate', [np.nan] * 3)
        fcoords = ttg.cell_vertex_property(t, cvid, 'fused_coordinate', [np.nan] * 3)
        ldmks.append([coords, fcoords])

    ldmks = np.array(ldmks)
    return ldmks[:, 0], ldmks[:, 1]


def landmarks_score(ldmk_before, ldmk_after):
    """Score for landmarks definition.

    Parameters
    ----------
    ldmk_before, ldmk_after : numpy.ndarray
        The arrays of landmarks to evaluate.

    Returns
    -------
    float
        The landmarks' definition score, in [0, 1].

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.features.graph import landmarks_score
    >>> ldmk_before = np.array([[1.1, 0., 0.], [0., 1.2, 0.], [0., 0., 1.], [0., 5., 1.]])
    >>> ldmk_after = np.array([[2.2, 0., 0.], [0., 2.4, 0.], [0., 0., 2.], [np.nan] * 3])
    >>> landmarks_score(ldmk_before, ldmk_after)
    0.75

    """
    n = len(ldmk_after)
    if n == 0:
        return 0.
    else:
        miss_before = np.array([np.isnan(k[0]) for k in ldmk_before])
        miss_after = np.array([np.isnan(k[0]) for k in ldmk_after])
        return (n - sum(miss_before | miss_after)) / float(n)


def clean_landmarks(ldmk_before, ldmk_after):
    """Clean landmarks from any ``numpy.nan`` occurence.

    Parameters
    ----------
    ldmk_before, ldmk_after : numpy.ndarray
        The arrays of landmarks to clean.

    Returns
    -------
    numpy.ndarray
        The cleaned array of landmarks before deformation.
    numpy.ndarray
        The cleaned array of landmarks after deformation.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.features.graph import clean_landmarks
    >>> ldmk_before = np.array([[1.1, 0., 0.], [0., 1.2, 0.], [0., 0., 1.], [0., 5., 1.]])
    >>> ldmk_after = np.array([[2.2, 0., 0.], [0., 2.4, 0.], [0., 0., 2.], [np.nan] * 3])
    >>> clean_landmarks(ldmk_before, ldmk_after)
    (array([[1.1, 0. , 0. ],
            [0. , 1.2, 0. ],
            [0. , 0. , 1. ]]),
     array([[2.2, 0. , 0. ],
            [0. , 2.4, 0. ],
            [0. , 0. , 2. ]]))
    """
    cldmk_before = []
    cldmk_after = []
    n, d = ldmk_before.shape
    for i in range(n):
        if np.isnan(ldmk_before[i][0]) or np.isnan(ldmk_after[i][0]):
            continue
        cldmk_before += [ldmk_before[i]]
        cldmk_after += [ldmk_after[i]]

    return np.array(cldmk_before), np.array(cldmk_after)


def division_rate(temporal_graph, time_points=None):
    """Compute the division rates for all cells.

    Parameters
    ----------
    temporal_graph : timagetk.graphs.TemporalTissueGraph
        The temporal tissue graph to populate with new features.
    time_points : int or list, optional
        If ``None`` (default) compute the temporal differentiation function for all time-point.
        Else, should be a list of valid time-points to select for computation.

    Returns
    -------
    dict
        Cell indexed dictionary of 'division_rate'.

    See Also
    --------
    timagetk.graphs.utils.division_rate

    Examples
    --------
    >>> from timagetk.features.graph import division_rate
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
    >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
    >>> div_r = division_rate(ttg)

    """
    valid_time_points = list(range(temporal_graph.nb_time_points - 1))
    if time_points is None:
        time_points = valid_time_points
    elif isinstance(time_points, int):
        try:
            assert time_points != temporal_graph.nb_time_points
        except AssertionError:
            log.critical("The requested time-point is the last one, this is not possible!")
            return temporal_graph
        else:
            time_points = [time_points]
    elif isinstance(time_points, list):
        time_points = list(set(time_points) & set(valid_time_points))
    else:
        log.critical(f"Could not make sense of given `time-points`: {time_points}!")
        return temporal_graph

    from timagetk.graphs.utils import division_rate as div_rate
    dr = {(t, cid): div_rate(len(temporal_graph.child(t, cid)), temporal_graph.time_interval(t, t + 1)) for t, cid in
          temporal_graph.cell_ids(time_points)}

    return dr


def add_division_rate(temporal_graph, time_points=None):
    """Compute the division rates for all cells and add it to the temporal tissue graph.

    Parameters
    ----------
    temporal_graph : timagetk.graphs.TemporalTissueGraph
        The temporal tissue graph to populate with new features.
    time_points : int or list, optional
        If ``None`` (default) compute the temporal differentiation function for all time-point.
        Else, should be a list of valid time-points to select for computation.

    Returns
    -------
    timagetk.graphs.TemporalTissueGraph
        The temporal tissue graph augmented with cell 'division_rate' feature.

    Examples
    --------
    >>> from timagetk.features.graph import division_rate
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
    >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
    >>> ttg = add_division_rate(ttg)

    """
    div_rate = division_rate(temporal_graph, time_points)
    temporal_graph.add_cell_property('division_rate', div_rate)
    return temporal_graph


def cell_layers(tissue_graph, max_depth=None, **kwargs):
    """Get the cell indexed dictionary of layer index.

    Parameters
    ----------
    tissue_graph : timagetk.graphs.TissueGraph or timagetk.graphs.TemporalTissueGraph
        The (temporal) tissue graph to use for cell layer computation.
    max_depth : int, optional
        The maximum search distance. No limit by default.

    Returns
    -------
    dict
        Cell indexed dictionary of layer index.

    See Also
    --------
    timagetk.graphs.tissue_graph.topological_distance

    Examples
    --------
    >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv    >>> from timagetk.features.graph import cell_layers
    >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
    >>> layers = cell_layers(tg)

    >>> from timagetk.features.graph import cell_layers
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
    >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
    >>> layers = cell_layers(ttg)

    """
    from timagetk.graphs.temporal_tissue_graph import TemporalTissueGraph

    if isinstance(tissue_graph, TemporalTissueGraph):
        return {(tp, cid): dist for tp in range(tissue_graph.nb_time_points) for cid, dist in cell_layers(
            tissue_graph._tissue_graph[tp], max_depth=max_depth).items()}

    bkgd_id = tissue_graph.background
    layers = topological_distance(tissue_graph, bkgd_id, max_depth=max_depth)
    if kwargs.get('verbose', True):
        log.info(f"Found {len(np.unique(list(layers.values())))} cell layers in the tissue graph!")

    return layers


def add_cell_layers(tissue_graph, max_depth=None, **kwargs):
    """Compute the cell layer for all cells and add it to the (temporal) tissue graph.

    Parameters
    ----------
    tissue_graph : timagetk.graphs.TissueGraph or timagetk.graphs.TemporalTissueGraph
        The (temporal) tissue graph to populate with new features.
    max_depth : int, optional
        The maximum search distance. No limit by default.

    Returns
    -------
    timagetk.graphs.TissueGraph, timagetkgraphs.temporal_tissue_graph.TemporalTissueGraph
        The temporal tissue graph augmented with cell 'layers' feature.

    See Also
    --------
    timagetk.features.graph.cell_layers

    Examples
    --------
    >>> from timagetk.features.graph import division_rate
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
    >>> ttg = example_temporal_tissue_graph_from_csv('sphere')
    >>> ttg = add_cell_layers(ttg)

    """
    layers = cell_layers(tissue_graph, max_depth=max_depth, **kwargs)
    tissue_graph.add_cell_property("layers", layers)
    return tissue_graph


def search_isolated_nodes(graph, background_id=None):
    """List the nodes isolated from the largest part of the tissue graph.

    Parameters
    ----------
    graph : timagetk.graphs.TissueGraph
        The graph to use to search for isolated nodes.
    background_id : int, optional
        Remove this node (and associated edges) as it connect many cells, but is not relevant here.
        By default, `None`, try to use the attribute `background` from the `graph`.

    See Also
    --------
    networkx.algorithms.connected_components

    Examples
    --------
    >>> from timagetk.features.graph import search_isolated_nodes
    >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
    >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
    >>> tg.add_cells([100, 101])  # add isolated nodes
    >>> search_isolated_nodes(tg)
    [100, 101]
    >>> tg.primal.remove_node(1)
    >>> search_isolated_nodes(tg)

    """
    import copy
    from networkx.algorithms import connected_components

    if background_id is None:
        background_id = graph.background

    G = copy.deepcopy(graph.primal)
    if background_id is not None:
        try:
            G.remove_node(background_id)  # exclude the "background id"
        except:
            pass

    # Search connected components:
    cc_graphs = connected_components(G)
    cc_graphs = dict(enumerate(cc_graphs))
    # Compute their sizes:
    cc_graph_sizes = {}
    for n, cc_nodes in cc_graphs.items():
        cc_graph_sizes[n] = len(cc_nodes)
    log.debug(f"Connected components graph sizes: {cc_graph_sizes}")
    # Find the biggest one:
    biggest_cc_graph = max(cc_graph_sizes, key=lambda key: cc_graph_sizes[key])
    # List nodes in small connected sub-graphs:
    unconnected_nodes = []
    for n, cc_nodes in cc_graphs.items():
        if n == biggest_cc_graph:
            continue
        unconnected_nodes.extend(list(cc_nodes))
    return unconnected_nodes


def topological_distance(tissue_graph, source_cell_id, max_depth=None):
    """Return the topological distance of all cells to the selected source.

    Parameters
    ----------
    tissue_graph : timagetk.graphs.TissueGraph
        The tissue graph with the cell topology.
    source_cell_id : int
        The source cell id to use.
    max_depth : int, optional
        The maximum search distance. No limit by default.

    Returns
    -------
    dict
        Cell indexed dictionary of topological distances to source id.

    See Also
    --------
    networkx.single_source_dijkstra

    Examples
    --------
    >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
    >>> from timagetk.features.graph import topological_distance
    >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
    >>> topo_dist = topological_distance(tg, 1)  # distance from the background == cell layers!
    >>> topo_dist

    >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
    >>> from timagetk.features.graph import topological_distance
    >>> tg = example_tissue_graph_from_csv('p58', time_point=0)
    >>> # Limit search to L1 layer with ``max_depth=1`` (rest is set to `inf` by default)
    >>> topo_dist = topological_distance(tg, 1, max_depth=1)  # distance from the background == cell layers!
    >>> topo_dist

    """
    from networkx import single_source_dijkstra
    topo_dist, _ = single_source_dijkstra(tissue_graph.primal, source_cell_id, cutoff=max_depth,
                                          weight=lambda u, v, d: 1)
    topo_dist.pop(source_cell_id)
    return topo_dist


def list_cells_in_layer(tissue_graph, layer):
    """Return the list of cells belonging to the selected layer(s).

    Parameters
    ----------
    tissue_graph : timagetk.graphs.TissueGraph
        The tissue graph with the cells' topology.
    layer : int or list
        The cell layer to return. Can be a len-2 list indicating a range of layers.

    Returns
    -------
    list
        The list of cells belonging to the selected cell layer(s).

    Notes
    -----
    This requires the specification of the background id.
    This can be achieved using the tissue image (with a defined background id) during graph initialization.
    Or you can define it manually with the background property.

    See Also
    --------
    timagetk.graphs.tissue_graph.topological_distance

    Examples
    --------
    >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
    >>> from timagetk.features.graph import list_cells_in_layer
    >>> # Use the 'sphere' synthetic data at t0 as it is made of 3 layers of cells:
    >>> #  - one (#2) round in the center (layer 3);
    >>> #  - 13 (#3 - #15) in the layer 2;
    >>> #  - 49 (#16 - #64) in the layer 1;
    >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
    >>> layer1 = list_cells_in_layer(tg,1)
    >>> print(len(layer1))
    49
    >>> layer2 = list_cells_in_layer(tg, 2)
    >>> print(layer2)
    [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
    >>> layer3 = list_cells_in_layer(tg, 3)
    >>> print(layer3)
    [2]
    >>> layer2_3 = list_cells_in_layer(tg, [2, 3])
    >>> print(layer2_3)
    [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]

    """
    if isinstance(layer, list):
        min_layer = min(layer)
        max_layer = max(layer)
    else:
        min_layer = layer
        max_layer = layer
    try:
        assert tissue_graph.background is not None
    except AssertionError:
        raise ValueError("You have to assign a value to the `background` property first!")
    try:
        assert min_layer >= 1
    except AssertionError:
        raise ValueError(f"Argument `layer` should be strictly greater than 1, got {min_layer}!")

    topo_dist = topological_distance(tissue_graph, tissue_graph.background, max_depth=max_layer)

    if isinstance(layer, list):
        return sorted([cid for cid, dist in topo_dist.items() if dist in range(min_layer, max_layer + 1)])
    else:
        return sorted([cid for cid, dist in topo_dist.items() if dist == layer])


def cell_by_layers(tissue_graph, layers):
    """Return the layer indexed dictionary of cells.

    Parameters
    ----------
    tissue_graph : timagetk.graphs.TissueGraph
        The tissue graph with the cells' topology.
    layers : list
        The list of layers to returns.

    Returns
    -------
    dict
        The layer indexed dictionary of cells.

    Notes
    -----
    This requires the specification of the background id.
    This can be achieved using the tissue image (with a defined background id) during graph initialization.
    Or you can define it manually with the background property.

    See Also
    --------
    timagetk.graphs.tissue_graph.topological_distance

    Examples
    --------
    >>> from timagetk.io.graph_dataset import example_tissue_graph_from_csv
    >>> from timagetk.features.graph import cell_by_layers
    >>> # Use the 'sphere' synthetic data at t0 as it is made of 3 layers of cells:
    >>> #  - one (#2) round in the center (layer 3);
    >>> #  - 13 (#3 - #15) in the layer 2;
    >>> #  - 49 (#16 - #64) in the layer 1;
    >>> tg = example_tissue_graph_from_csv('sphere', time_point=0)
    >>> cids_by_layers = cell_by_layers(tg, [1])
    >>> print(list(cids_by_layers.keys()))
    [1]
    >>> print(len(cids_by_layers[1]))
    49
    >>> cids_by_layers = cell_by_layers(tg, [1, 2, 3])
    >>> print(list(cids_by_layers.keys()))
    [1, 2, 3]
    >>> print(cids_by_layers[2])
    [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
    >>> print(cids_by_layers[3])
    [2]

    """
    min_layer = min(layers)
    max_layer = max(layers)

    try:
        assert tissue_graph.background is not None
    except AssertionError:
        raise ValueError("You have to assign a value to the `background` property first!")
    try:
        assert min_layer >= 1
    except AssertionError:
        raise ValueError(f"Argument `layer` should be strictly greater than 1, got {min_layer}!")

    # Compute the topological distance to the background:
    topo_dist = topological_distance(tissue_graph, tissue_graph.background, max_depth=max_layer)

    # Transform the cell indexed dictionary into a layer indexed dictionary:
    cell_layers = {}
    for cid, layer in topo_dist.items():
        if layer not in layers:
            continue
        cell_layers[layer] = [cid] if layer not in cell_layers else cell_layers[layer] + [cid]

    return cell_layers
