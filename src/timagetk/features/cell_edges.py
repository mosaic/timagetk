#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Module for cell-edge feature extraction methods."""

from tqdm.autonotebook import tqdm

from timagetk.bin.logger import get_logger
from timagetk.features.array_tools import covariance_matrix
from timagetk.features.array_tools import geometric_median
from timagetk.features.cells import assert_dimensionality
from timagetk.features.features import Features

log = get_logger(__name__)


def median_coordinate(image, edge_ids=None, real=True):
    """Compute the edge property geometric *median coordinate*.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Labelled image to use for property computation.
    edge_ids : int or list of int or str, optional
        If ``None`` (default) returns the dictionary for all cell-edge ids found in the image.
        If a len-3 tuple of int, or a list of them, filter returned dictionary keys with those ids, but make sure
        they are defined in the list of cell-edge ids.
    real : bool, optional
        If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

    Returns
    -------
    dict
        Cell-edge geometric median coordinate dictionary, ``{eid: median(eid)}`` for ``eid`` in `edge_ids`.

    Raises
    ------
    ValueError
        If the image is not 3D.

    Examples
    --------
    >>> from timagetk import LabelledImage
    >>> from timagetk.io.dataset import shared_data
    >>> image = shared_data('flower_labelled', 0)
    >>> from timagetk.features.cell_edges import median_coordinate
    >>> median_coordinate(image, [(1, 64, 1001)])  # Get the epidermis median for cell 1001
    {(1, 64, 1001): array([32.25152087, 68.50944185, 41.46624112])}
    >>> median_coordinate(image, [(1, 1001, 64)])  # remember, to define an edge-id, cell-ids order is irrelevant
    {(1, 64, 1001): array([32.25152087, 68.50944185, 41.46624112])}
    >>> median_coordinate(image)  # Get the area for every -detected- edges
    {(1, 96, 605): array([47.87648129, 85.73696232, 62.7001617 ]),
     (14, 106, 930): array([87.53984237, 63.30112171, 33.05280089]),
     (31, 312, 905): array([37.45984101, 38.46144104, 29.04640079]),
    ...}

    """
    assert_dimensionality(image, 'cell edge geometric median', 3)
    edge_ids = image.linels(edge_ids)  # Check the provided edge_ids
    geom_median = {}
    for edge_id in tqdm(edge_ids, unit="edge"):
        geom_median[edge_id] = geometric_median(image.linel_coordinates(edge_id, real=real)[edge_id])

    return geom_median


def inertia_axis(image, edge_ids=None, real=True):
    """Compute the edge property *inertia axis*.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Labelled image to use for property computation.
    edge_ids : int or list of int or str, optional
        If ``None`` (default) returns the dictionary for all cell-edge ids found in the image.
        If a len-3 tuple of int, or a list of them, filter returned dictionary keys with those ids, but make sure
        they are defined in the list of cell-edge ids.
    real : bool, optional
        If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

    Returns
    -------
    dict
        Cell-edge inertia axes dictionary, ``{eid: inertia_axis(eid)}`` for ``eid`` in `edge_ids`.

    Raises
    ------
    ValueError
        If the image is not 3D.

    Notes
    -----
    If a cell-edge is made of less than four coordinates (voxels), it is filtered out.

    Examples
    --------
    >>> from timagetk.features.cell_edges import inertia_axis
    >>> from timagetk.io.dataset import shared_data
    >>> image = shared_data('flower_labelled', 0)
    >>> inertia_axis(image, [(1001, 64, 1)])  # access the feature value for one edge
    {(1, 64, 1001): array([[ 0.47016763,  0.45578839, -0.35279292],
                           [ 0.45578839,  0.45729319, -0.35764173],
                           [-0.35279292, -0.35764173,  0.37837459]])}
    >>> i_axis = inertia_axis(image)  # get the feature dictionary for all edges

    """
    assert_dimensionality(image, 'cell edge inertia axis', 3)
    edge_ids = image.linels(edge_ids)  # Check the provided edge_ids
    coords = image.linel_coordinates(edge_ids, real=real)
    return {edge: covariance_matrix(coord) for edge, coord in tqdm(coords.items(), unit="edge") if coord.shape[0] >= 4}


class Edges(Features):

    def __init__(self, image):
        super().__init__()
        self.image = image

    def ids(self, edge_ids=None):
        """Return the list of cell-edge ids found in the image.

        Parameters
        ----------
        edge_ids : tuple of int or list of tuple of int, optional
            If ``None`` (default) returns all cell-edge ids found in the image.
            If a len-3 tuple of int, or a list of them, make sure it is or they are in the image.

        Returns
        -------
        list
            List of cell-edge ids found in the image.
        """
        return self.image.linels(edge_ids)

    def coordinates(self, edge_ids=None, real=True):
        """Return the real or voxel coordinates of each voxels representing a cell-edge.

        Parameters
        ----------
        edge_ids : int or list of int or str, optional
            If ``None`` (default) returns the dictionary for all cell-edge ids found in the image.
            If a len-3 tuple of int, or a list of them, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell-edge ids.
        real : bool, optional
            If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

        Returns
        -------
        dict
            Cell-edge coordinates dictionary, ``{eid: coordinates(eid)}`` for ``eid`` in `edge_ids`.
        """
        return self.image.linel_coordinates(edge_ids, real)


class Edges3D(Edges):

    def __init__(self, image):
        Edges.__init__(self, image)
        self.add_feature("geometric_median")
        self.add_feature("inertia_axis")

    def geometric_median(self, edge_ids=None, real=True):
        """Return the dictionary of edge geometric median.

        Parameters
        ----------
        edge_ids : int or list of int or str, optional
            If ``None`` (default) returns the dictionary for all cell-edge ids found in the image.
            If a len-3 tuple of int, or a list of them, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell-edge ids.
        real : bool, optional
            If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

        Returns
        -------
        dict
            Cell-edge geometric median dictionary, ``{eid: geometric_median(eid)}`` for ``eid`` in `edge_ids`.
        """
        edge_ids = self.ids(edge_ids)
        eids_no_ppty = set(edge_ids) - set(self.feature('geometric_median').keys())
        if eids_no_ppty != set():
            log.info(f"Computing {len(eids_no_ppty)} cell-edge geometric median coordinates:")
            self.set_feature('geometric_median', median_coordinate(self.image, edge_ids=eids_no_ppty, real=real),
                             update=True)

        return {eid: self.feature('geometric_median')[eid] for eid in edge_ids}

    def inertia_axis(self, edge_ids=None, real=True):
        """Return the dictionary of edge inertia axis.

        Parameters
        ----------
        edge_ids : int or list of int or str, optional
            If ``None`` (default) returns the dictionary for all cell-edge ids found in the image.
            If a len-3 tuple of int, or a list of them, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell-edge ids.
        real : bool, optional
            If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

        Returns
        -------
        dict
            Cell-edge inertia axis dictionary, ``{eid: inertia_axis(eid)}`` for ``eid`` in `edge_ids`.
        """
        edge_ids = self.ids(edge_ids)
        eids_no_ppty = set(edge_ids) - set(self.feature('inertia_axis').keys())
        if eids_no_ppty != set():
            log.info(f"Computing {len(eids_no_ppty)} cell-edge inertia axis matrices:")
            self.set_feature('inertia_axis', inertia_axis(self.image, edge_ids=eids_no_ppty, real=real), update=True)

        return {eid: self.feature('inertia_axis')[eid] for eid in edge_ids}
