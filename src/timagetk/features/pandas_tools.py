# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import numpy as np
import pandas as pd

from timagetk.features.cell_walls import Wall
from timagetk.features.cells import Cell


def cells_to_dataframe(cells: Cell, cell_ids=None) -> pd.DataFrame:
    """Export the Cell object to a data frame.

    Parameters
    ----------
    cells : Cell
        The cell object to export.
    cell_ids : list of int, optional
        If defined, used to filter the list of cell ids saved under cell related CSV.

    Returns
    -------
    pandas.DataFrame
        Cell related data frame

    Examples
    --------
    >>> from timagetk.features.pandas_tools import cells_to_dataframe
    >>> from timagetk import TissueImage3D
    >>> from timagetk.io.dataset import shared_data
    >>> img = TissueImage3D(shared_data('flower_labelled', 0), background=1, not_a_label=0)
    >>> img.cells.volume()
    >>> df = cells_to_dataframe(img.cells)
    >>> df.columns
    Index(['label', 'volume'], dtype='object')

    """
    if cell_ids is None:
        cell_ids = cells.ids()

    cell_df = pd.DataFrame({"label": cell_ids })
    for property_name in cells.feature_names():
        if cells.feature(property_name) != {}:
            cell_df[property_name] = [cells.feature(property_name)[l]
                                      if l in cells.feature(property_name)
                                      else np.nan
                                      for l in cell_ids]
    return cell_df


def cell_walls_to_dataframe(cell_walls: Wall, cell_ids=None) -> pd.DataFrame:
    """Export the Wall object to a data frame.

    Parameters
    ----------
    cell_walls : Wall
        The wall object to export.
    cell_ids : list of int, optional
        If defined, used to filter the list of cell ids saved under cell-wall related CSV.

    Returns
    -------
    pandas.DataFrame
        Cell-wall related data frame

    Examples
    --------
    >>> from timagetk.features.pandas_tools import cell_walls_to_dataframe
    >>> from timagetk import TissueImage3D
    >>> from timagetk.io.dataset import shared_data
    >>> img = TissueImage3D(shared_data('flower_labelled', 0), background=1, not_a_label=0)
    >>> img.walls.area()
    >>> df = cell_walls_to_dataframe(img.walls)
    >>> df.columns
    Index(['left_label', 'right_label', 'area'], dtype='object')

    """
    if cell_ids is None:
        wall_ids = cell_walls.ids()
    else:
        wall_ids = [w for w in cell_walls.ids() if np.all([c in cell_ids for c in w])]
    wall_ids = np.array(wall_ids)

    wall_df = pd.DataFrame({"left_label": wall_ids[:, 0], "right_label": wall_ids[:, 1] })
    for property_name in cell_walls.feature_names():
        if cell_walls.feature(property_name) != {}:
            wall_df[property_name] = [cell_walls.feature(property_name)[tuple(w)]
                                      if tuple(w) in cell_walls.feature(property_name)
                                      else np.nan
                                      for w in wall_ids]
    return wall_df