#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Module for cell feature extraction methods."""

from copy import deepcopy
from itertools import permutations

import numpy as np
import scipy.ndimage as nd
from tqdm.autonotebook import tqdm
from vt.cellproperties import CellProperties

from timagetk import LabelledImage
from timagetk.bin.logger import get_logger
from timagetk.features.array_tools import covariance_matrix
from timagetk.features.array_tools import eigen_values_vectors
from timagetk.features.array_tools import fractional_anisotropy_eigenval
from timagetk.features.features import Features
from timagetk.util import stuple

log = get_logger(__name__)


def assert_dimensionality(image, algo_name, dim):
    """Assert given image dimensionality, raises a specific message if not.

    Parameters
    ----------
    image : timagetk.LabelledImage
        An labelled image to test.
    algo_name : str
        The algorithm name, used for message printing.
    dim : {2, 3}
        The required `image` dimensionality.

    Raises
    ------
    ValueError
        If the `image` dimensionality is not the required one.
    TypeError
        If the `image` is not a ``LabelledImage`` instance.

    """
    if dim == 2:
        dim_test = image.is2D
    else:
        dim_test = image.is3D

    try:
        assert dim_test()
    except AssertionError:
        msg = f"Algorithm '{algo_name}' requires a {dim}D image... "
        msg += f"Image '{image.filename}' is NOT {dim}D!"
        raise ValueError(msg)
    except AttributeError:
        msg = f"Algorithm '{algo_name}' requires a `LabelledImage` instance... "
        msg += f"But got {type(image)}"
        raise TypeError(msg)


def volume(image, cell_ids=None, real=True):
    """Compute the cell property *volume*, require a 3D image.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Labelled image to use for property computation.
    cell_ids : int or list of int, optional
        If ``None`` (default), returns values for all cell ids found in `image`.
        Else, should be a cell id of a list of cell ids.
        Note that given cell ids are checked for existence within the array.
    real : bool, optional
        If ``True`` (default), returns the values in real world units.
        Else returns the values in voxel units.

    Returns
    -------
    dict
        Cell volumes dictionary, ``{cid: volume(cid)}`` for ``cid`` in `cell_ids`.

    Raises
    ------
    ValueError
        If the image is not 3D.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> image = shared_data('flower_labelled', 0)
    >>> from timagetk.features.cells import volume
    >>> volume(image, 7)  # access the feature value for one cell
    {7: 269.89940430477526}
    >>> volume(image, 7, real=False)  # access the feature value in voxels unit
    {7: 4197.0}
    >>> vol = volume(image)  # get the feature dictionary for all cells

    """
    assert_dimensionality(image, 'volume', 3)
    # Check the provided cell_ids:
    cell_ids = image.labels(cell_ids)
    log.info(f"Computing 'volume' feature for {len(cell_ids)} cells...")

    # - Compute the volumes using scipy.ndimage.sum()
    volume = list(nd.sum(np.ones_like(image), image, index=np.uint16(cell_ids)))
    volume = dict(zip(cell_ids, volume))

    # - Convert to real-world units if asked:
    if real:
        vxs = image.voxelsize
        volume = {l: np.multiply(v, np.prod(vxs)) for l, v in volume.items()}

    return volume


def center_of_mass(image, cell_ids=None, real=True):
    """Compute the cell property *center of mass*.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Labelled image to use for property computation.
    cell_ids : int or list of int, optional
        If ``None`` (default), returns values for all cell ids found in `image`.
        Else, should be a cell id of a list of cell ids.
        Note that given cell ids are checked for existence within the array.
    real : bool, optional
        If ``True`` (default), returns the values in real world units.
        Else returns the values in voxel units.

    Returns
    -------
    dict
        Cell indexed dictionary of centers of mass coordinates, ``{cid: center_of_mass(cid)}`` for ``cid`` in `cell_ids`.
        Coordinates are XY(Z) ordered.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> image = shared_data('flower_labelled', 0)
    >>> from timagetk.features.cells import center_of_mass
    >>> center_of_mass(image, 7)  # access the feature value for one cell
    {7: array([46.40598831, 72.0760639 , 79.14907357])}
    >>> center_of_mass(image, 7, real=False)  # access the feature value in voxels unit
    {7: [115.8296402192042, 179.90231117464856, 197.55658803907554]}
    >>> com = center_of_mass(image)  # get the feature dictionary for all cells

    """
    # Check the provided cell_ids:
    cell_ids = image.labels(cell_ids)
    # - Check we have all necessary bounding-boxes...
    bboxes = image.boundingbox(cell_ids, real=False)

    log.info(f"Computing 'center of mass' feature for {len(cell_ids)} cells:")

    center = {}
    for _n, cid in tqdm(enumerate(cell_ids), unit='cell'):
        try:
            crop = bboxes[cid]
            crop_im = image.get_array()[crop]
            com = np.array(nd.center_of_mass(crop_im, crop_im, index=cid))
            com = [com[ax] + sl.start for ax, sl in enumerate(crop)]
        except ValueError:
            crop_im = image.get_array()
            com = np.array(nd.center_of_mass(crop_im, crop_im, index=cid))
        center[cid] = com

    if real:
        vxs = image.voxelsize
        center = {cid: np.multiply(center[cid], vxs) for cid in cell_ids}

    return center


def inertia_axis(image, cell_ids=None, real=True):
    """Compute the cell property *inertia axis*.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Labelled image to use for property computation.
    cell_ids : int or list of int, optional
        If ``None`` (default), returns values for all cell ids found in `image`.
        Else, should be a cell id of a list of cell ids.
        Note that given cell ids are checked for existence within the array.
    real : bool, optional
        If ``True`` (default), returns the values in real world units.
        Else returns the values in voxel units.

    Returns
    -------
    dict
        Cell inertia axes dictionary, ``{cid: inertia_axis(cid)}`` for ``cid`` in `cell_ids`.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> image = shared_data('flower_labelled', 0)
    >>> from timagetk.features.cells import inertia_axis
    >>> inertia_axis(image, 7)  # access the feature value for one cell
    {7: array([[12.82581413, -0.62548   ,  2.19449703],
        [-0.62548   ,  2.16523248, -1.0341804 ],
        [ 2.19449703, -1.0341804 ,  2.25045642]])}
    >>> inertia_axis(image, 7, real=False)  # access the feature value in voxels unit
    {7: array([[79.90543202, -3.89677014, 13.67182089],
            [-3.89677014, 13.48950133, -6.44299307],
            [13.67182089, -6.44299307, 14.02045051]])}
    >>> i_axis = inertia_axis(image)  # get the feature dictionary for all cells

    """
    coords = image.label_coordinates(labels=cell_ids, real=real)
    # Check the provided cell_ids:
    log.info(f"Computing 'inertia axis' feature for {len(coords)} cells:")
    return {cell: covariance_matrix(coord) for cell, coord in tqdm(coords.items(), unit="cell")}


def principal_direction_norms(image, cell_ids=None, inertia_dict=None, real=True):
    """Compute the cell property *principal direction norms*.

    Parameters
    ----------
    image : timagetk.TissueImage3D
        Labelled image to use for property computation.
    cell_ids : int or list of int, optional
        If ``None`` (default), returns values for all cell ids found in `image`.
        Else, should be a cell id of a list of cell ids.
        Note that given cell ids are checked for existence within the array.
    inertia_dict : dict, optional
        Cell indexed dictionary of inertia matrices.
        Note that in that case, the `real` parameter will have no effect.
    real : bool, optional
        If ``True`` (default), returns the values in real world units.
        Else returns the values in voxel units.

    Returns
    -------
    dict
        Cell principal direction norms dictionary, ``{cid: principal_direction_norms(cid)}`` for ``cid`` in `cell_ids`.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> image = shared_data('flower_labelled', 0)
    >>> from timagetk.features.cells import principal_direction_norms
    >>> principal_direction_norms(image, 7)  # access the feature value for one cell
    {7: array([13.32309103,  2.86078965,  1.05762235])}
    >>> pdn = principal_direction_norms(image)  # get the feature dictionary for all cells

    """
    if inertia_dict is None:
        inertia_dict = inertia_axis(image, cell_ids, real)
    else:
        if cell_ids is None:
            cell_ids = image.cell_ids()
        missing_cids = set(cell_ids) - set(inertia_dict.keys())
        if len(missing_cids) != 0:
            inertia_dict.update(inertia_axis(image, missing_cids, real))

    log.info(f"Computing 'principal direction norms' feature for {len(inertia_dict)} cells:")
    pdn = {}
    for cell, cov in tqdm(inertia_dict.items(), unit="cell"):
        eig_val, _ = eigen_values_vectors(cov)
        pdn[cell] = eig_val
    return pdn


def shape_anisotropy(image, cell_ids=None, inertia_dict=None, real=True):
    """Compute the cell property *shape anisotropy*.

    Parameters
    ----------
    image : timagetk.TissueImage3D
        Labelled image to use for property computation.
    cell_ids : int or list of int, optional
        If ``None`` (default), returns values for all cell ids found in `image`.
        Else, should be a cell id of a list of cell ids.
        Note that given cell ids are checked for existence within the array.
    real : bool, optional
        If ``True`` (default), returns the values in real world units.
        Else returns the values in voxel units.

    Returns
    -------
    dict
        Cell shape anisotropy dictionary, ``{cid: shape_anisotropy(cid)}`` for ``cid`` in `cell_ids`.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> image = shared_data('flower_labelled', 0)
    >>> from timagetk.features.cells import shape_anisotropy
    >>> shape_anisotropy(image, 7)  # access the feature value for one cell
    {7: 0.8392512339371002}
    >>> sa = shape_anisotropy(image)  # get the feature dictionary for all cells

    """
    if inertia_dict is None:
        inertia_dict = inertia_axis(image, cell_ids, real)
    else:
        if cell_ids is None:
            cell_ids = image.cell_ids()
        missing_cids = set(cell_ids) - set(inertia_dict.keys())
        if len(missing_cids) != 0:
            inertia_dict.update(inertia_axis(image, missing_cids, real))

    log.info(f"Computing 'shape anisotropy' feature for {len(inertia_dict)} cells:")
    sa = {}
    for cell, cov in tqdm(inertia_dict.items(), unit="cell"):
        eig_val, _ = eigen_values_vectors(cov)
        sa[cell] = fractional_anisotropy_eigenval(eig_val)
    return sa


def number_of_neighbors(image, cell_ids=None):
    """Compute the cell property *number of neighbors*.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Labelled image to use for property computation.
    cell_ids : int or list of int, optional
        If ``None`` (default), returns values for all cell ids found in `image`.
        Else, should be a cell id of a list of cell ids.
        Note that given cell ids are checked for existence within the array.

    Returns
    -------
    dict
        Cell-based dictionary of number of neighbors, ``{cid: number_of_neighbors(cid)}`` for ``cid`` in `cell_ids`.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> image = shared_data('flower_labelled', 0)
    >>> from timagetk.features.cells import number_of_neighbors
    >>> number_of_neighbors(image, 7)  # access the feature value for one cell
    {7: 15}
    >>> number_of_neighbors(image, 7, 15., True)  # access the feature value for one cell
    >>> n_neighbor = number_of_neighbors(image)  # get the feature dictionary for all cells

    """
    neigh = image.neighbors(labels=cell_ids)
    return {cell: len(nei) for cell, nei in neigh.items()}


class Cell(Features):
    """Class dedicated to computation of cell features from labelled image.

    Attributes
    ----------
    image : timagetk.LabelledImage
        Labelled image to use for property computation.
    _feature : dict of str: dict
        Dictionary of computed cell features.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> image = shared_data('synthetic', 'labelled')
    >>> from timagetk.features.cells import Cell
    >>> c = Cell(image)
    >>> c.ids()
    {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14}
    >>> c.barycenter(2)
    {2: array([30.00393275, 30.00254943, 29.99614244])}
    >>> c.shape_anisotropy(2)
    {2: 3.1489654141427686e-16}
    >>> c.number_of_neighbors(2)
    {2: 12}
    >>> c.neighbors(3)
    {3: [1, 2, 4, 7, 8, 11, 12]}

    """

    def __init__(self, image):
        """Constructor.

        Parameters
        ----------
        image : timagetk.LabelledImage or timagetk.TissueImage3D
            Labelled image to use for property computation.
        """
        super().__init__()
        self.image = image
        self.vxs = self.image.get_voxelsize()[::-1]  # should be XYZ sorted
        self._ppties = []
        self.vt_ppty = None
        # Initialize some features names in the dictionary
        self.add_feature("barycenter")
        self.add_feature("inertia_axis")
        self.add_feature("principal_direction_norms")
        self.add_feature("shape_anisotropy")

    def __deepcopy__(self, memo):
        obj = self.__class__(self.image)
        obj._feature = deepcopy(self._feature)
        return obj

    def _init_vt_ppty(self, ppty_list, filter_neighbors=False):
        """Hidden magic to get the right image for correct estimation by `vtCellProperties`.

        Parameters
        ----------
        ppty_list : list
            A list of properties to be initialized for the VT cell properties.
        filter_neighbors : bool, optional
            A flag indicating whether to filter neighbors during initialization of VT cell properties
            Defaults to `False`.
        """
        # Add "contact-surface" to list of ppty if `filter_neighbors` is True to be able to effectively do that filtering
        if filter_neighbors and "contact-surface" not in ppty_list:
            ppty_list.append("contact-surface")

        if any("surface" in ppty for ppty in ppty_list) and not self.image.is_isometric():
            from timagetk.algorithms.resample import isometric_resampling
            # surface estimation methods from `CellProperties` requires an isometric image
            ppty_image = isometric_resampling(LabelledImage(self.image), value='min', interpolation='cellbased',
                                              cell_based_sigma=2)
            self.vxs = ppty_image.get_voxelsize()[::-1]  # should be XYZ sorted
        else:
            ppty_image = self.image
            self.vxs = ppty_image.get_voxelsize()[::-1]  # should be XYZ sorted

        self.vt_ppty = CellProperties(ppty_image.to_vt_image(), ppty_list, filter_neighbors)
        self._ppties = ppty_list

    def check_properties(self, ppty_list, filter_neighbors=False):
        """Use this method to check the required properties are accessible from the `vtCellProperties` instance.

        Parameters
        ----------
        ppty_list : list
            A list of properties to be checked.
        filter_neighbors : bool, optional
            A flag that determines whether to filter neighbors.
            Defaults to `False`.
        """
        if any(ppty not in self._ppties for ppty in ppty_list):
            self._init_vt_ppty(ppty_list, filter_neighbors)

    def ids(self, cell_ids=None):
        """Return the set of cell ids found in the image.

        Parameters
        ----------
        cell_ids : int or list of int, optional
            If ``None`` (default) returns all cell ids found in the image.
            If an integer or a list of integers, make sure they are in the image.

        Returns
        -------
        set
            Set of cell ids found in the image.
        """
        return set(self.image.labels(cell_ids)) - {getattr(self.image, "background", None)}

    def coordinates(self, cell_ids=None, real=True):
        """Return the real or voxel coordinates of each voxel representing a label.

        Parameters
        ----------
        cell_ids : int or list of int, optional
            If ``None`` (default) returns the dictionary for all cell ids.
            If an integer, or a list of them, filter returned dictionary keys with those ids, but make sure they are
            defined in the list of cell ids.
        real : bool, optional
            If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

        Returns
        -------
        dict
            Cell coordinates dictionary, ``{cid: coordinates(cid)}`` for ``cid`` in `cell_ids`.

        Example
        -------
        >>> from timagetk.io.dataset import shared_data
        >>> image = shared_data('flower_labelled', 0)
        >>> from timagetk.features.cells import Cell
        >>> c = Cell(image)
        >>> cell_coords = c.coordinates(2)
        >>> cell_coords[2].shape
        (326, 3)
        """
        return self.image.label_coordinates(cell_ids, real)

    def bounding_boxes(self, cell_ids=None, real=True):
        """Return the dictionary of cell bounding-boxes.

        Parameters
        ----------
        cell_ids : int or list of int, optional
            If ``None`` (default) returns the dictionary for all cell ids.
            If an integer or a list of integers, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell ids.
        real : bool, optional
            If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

        Returns
        -------
        dict
            Cell volumes dictionary, ``{cid: bounding-box(cid)}`` for ``cid`` in `cell_ids`.
        """
        return self.image.boundingbox(labels=cell_ids, real=real)

    def barycenter(self, cell_ids=None, real=True):
        """Return the dictionary of cell barycenter.

        Parameters
        ----------
        cell_ids : int or list of int, optional
            If ``None`` (default) returns the dictionary for all cell ids.
            If an integer or a list of integers, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell ids.
        real : bool, optional
            If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

        Other Parameters
        ----------------
        verbose : bool
            If ``True``, increase code verbosity, default is ``False``.

        Returns
        -------
        dict
            Cell barycenter XYZ coordinate dictionary, ``{cid: barycenter(cid)}`` for ``cid`` in `cell_ids`.
        """
        from timagetk.third_party.vt_features import _ppty_get_barycenter
        cell_ids = self.ids(cell_ids)
        cids_no_ppty = set(cell_ids) - set(self.feature("barycenter").keys())
        if cids_no_ppty != set():
            log.info(f"Computing {len(cids_no_ppty)} cells barycenter...")
            if self.vt_ppty is None:
                self._init_vt_ppty(['volume'], filter_neighbors=False)
            self.set_feature(
                "barycenter",
                _ppty_get_barycenter(self.vt_ppty, labels=cids_no_ppty, real=real, vxs=self.vxs),
                update=True
            )

        return {cid: self.feature("barycenter")[cid] for cid in cell_ids}

    def inertia_axis(self, cell_ids=None, real=True):
        """Return the dictionary of cell inertia axis.

        Parameters
        ----------
        cell_ids : int or list of int, optional
            If ``None`` (default) returns the dictionary for all cell ids.
            If an integer or a list of integers, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell ids.
        real : bool, optional
            If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

        Returns
        -------
        dict
            Cell inertia axis dictionary, ``{cid: inertia_axis(cid)}`` for ``cid`` in `cell_ids`.
        """
        cell_ids = self.ids(cell_ids)
        cids_no_ppty = set(cell_ids) - set(self.feature("inertia_axis").keys())
        if cids_no_ppty != set():
            self.set_feature(
                "inertia_axis",
                inertia_axis(self.image, cell_ids=cids_no_ppty, real=real),
                update=True
            )

        return {cid: self.feature("inertia_axis")[cid] for cid in cell_ids}

    def principal_direction_norms(self, cell_ids=None, real=True):
        """Return the dictionary of cell principal direction norms.

        Parameters
        ----------
        cell_ids : int or list of int, optional
            If ``None`` (default) returns the dictionary for all cell ids.
            If an integer or a list of integers, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell ids.
        real : bool, optional
            If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

        Returns
        -------
        dict
            Cell inertia axis dictionary, ``{cid: principal_direction_norms(cid)}`` for ``cid`` in `cell_ids`.
        """
        cell_ids = self.ids(cell_ids)
        cids_no_ppty = set(cell_ids) - set(self.feature("principal_direction_norms").keys())
        if cids_no_ppty != set():
            self.set_feature(
                "principal_direction_norms",
                principal_direction_norms(self.image,
                                          cell_ids=cids_no_ppty,
                                          inertia_dict=self.feature('inertia_axis'),
                                          real=real),
                update=True
            )

        return {cid: self.feature("principal_direction_norms")[cid] for cid in cell_ids}

    def shape_anisotropy(self, cell_ids=None, real=True):
        """Return the dictionary of cell shape anisotropy.

        Parameters
        ----------
        cell_ids : int or list of int, optional
            If ``None`` (default) returns the dictionary for all cell ids.
            If an integer or a list of integers, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell ids.
        real : bool, optional
            If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

        Returns
        -------
        dict
            Cell shape anisotropy dictionary, ``{cid: shape_anisotropy(cid)}`` for ``cid`` in `cell_ids`.
        """
        cell_ids = self.ids(cell_ids)
        cids_no_ppty = set(cell_ids) - set(self.feature("shape_anisotropy").keys())
        if cids_no_ppty != set():
            self.set_feature(
                "shape_anisotropy",
                shape_anisotropy(self.image,
                                 cell_ids=cids_no_ppty,
                                 inertia_dict=self.feature('inertia_axis'),
                                 real=real),
                update=True
            )

        return {cid: self.feature("shape_anisotropy")[cid] for cid in cell_ids}

    def neighbors(self, cell_ids=None):
        """Return the dictionary of cell neighbors.

        Parameters
        ----------
        cell_ids : int or list of int, optional
            If ``None`` (default) returns the dictionary for all cell ids.
            If an integer or a list of integers, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell ids.

        Returns
        -------
        dict
            Cell neighbors dictionary, ``{cid: neighbors(cid)}`` for ``cid`` in `cell_ids`.
        """
        return self.image.neighbors(labels=cell_ids)

    def number_of_neighbors(self, cell_ids=None):
        """Return the dictionary of cell neighbors.

        Parameters
        ----------
        cell_ids : int or list of int, optional
            If ``None`` (default) returns the dictionary for all cell ids.
            If an integer or a list of integers, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell ids.

        Returns
        -------
        dict
            Cell neighbors dictionary, ``{cid: neighbors(cid)}`` for ``cid`` in `cell_ids`.
        """
        return number_of_neighbors(self.image, cell_ids=cell_ids)

    def wall_ids(self, cell_ids=None):
        """Return the dictionary of cell-wall ids per cell.

        Parameters
        ----------
        cell_ids : int or list of int, optional
            If ``None`` (default) returns the dictionary for all cell ids.
            If an integer or a list of integers, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell ids.

        Returns
        -------
        dict
            Cell-wall ids dictionary, ``{cid: wall_ids(cid)}`` for ``cid`` in `cell_ids`.
        """
        return {cid: self.image.surfels([stuple((cid, nid)) for nid in neighbors]) for cid, neighbors in
                self.neighbors(cell_ids=cell_ids).items()}

    def edge_ids(self, cell_ids=None):
        """Return the dictionary of cell-edge ids per cell.

        Parameters
        ----------
        cell_ids : int or list of int, optional
            If ``None`` (default) returns the dictionary for all cell ids.
            If an integer or a list of integers, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell ids.

        Returns
        -------
        dict
            Cell-edge ids dictionary, ``{cid: edge_ids(cid)}`` for ``cid`` in `cell_ids`.
        """
        return {cid: self.image.linels([stuple(p) for p in permutations(neighbors + [cid])]) for cid, neighbors in
                self.neighbors(cell_ids=cell_ids).items()}

    def vertex_ids(self, cell_ids=None):
        """Return the dictionary of cell-vertex ids per cell.

        Parameters
        ----------
        cell_ids : int or list of int, optional
            If ``None`` (default) returns the dictionary for all cell ids.
            If an integer or a list of integers, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell ids.

        Returns
        -------
        dict
            Cell-vertex ids dictionary, ``{cid: vertex_ids(cid)}`` for ``cid`` in `cell_ids`.
        """
        return {cid: self.image.surfels([stuple((cid, nid)) for nid in neighbors]) for cid, neighbors in
                self.neighbors(cell_ids=cell_ids).items()}


class Cell2D(Cell):
    """Class dedicated to computation of cell features from 2D labelled image.

    Attributes
    ----------
    image : timagetk.LabelledImage
        Labelled image to use for property computation.
    _feature : dict of str: dict
        Dictionary of computed cell features.

    """

    def __init__(self, image):
        """Constructor.

        Parameters
        ----------
        image : timagetk.LabelledImage
            2D labelled image to use for property computation.
        """
        Cell.__init__(self, image)
        self.add_feature("area")

    def barycenter(self, cell_ids=None, real=True):
        """Return the dictionary of cell barycenter.

        Parameters
        ----------
        cell_ids : int or list of int, optional
            If ``None`` (default) returns the dictionary for all cell ids.
            If an integer or a list of integers, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell ids.
        real : bool, optional
            If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

        Other Parameters
        ----------------
        verbose : bool
            If ``True``, increase code verbosity, default is ``False``.

        Returns
        -------
        dict
            Cell barycenter dictionary, ``{cid: barycenter(cid)}`` for ``cid`` in `cell_ids`.
        """
        cell_ids = self.ids(cell_ids)
        cids_no_ppty = set(cell_ids) - set(self.feature("barycenter").keys())
        if cids_no_ppty != set():
            log.info(f"Computing {len(cids_no_ppty)} cells barycenter...")
            self.set_feature(
                "barycenter",
                center_of_mass(self.image, cell_ids=cids_no_ppty, real=real),
                update=True
            )

        return {cid: self.feature("barycenter")[cid] for cid in cell_ids}

    def area(self, cell_ids=None, real=True):
        """Return the dictionary of cell areas.

        Parameters
        ----------
        cell_ids : int or list of int, optional
            If ``None`` (default) returns the dictionary for all cell ids.
            If an integer or a list of integers, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell ids.
        real : bool, optional
            If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

        Returns
        -------
        dict
            Cell area dictionary, ``{cid: area(cid)}`` for ``cid`` in `cell_ids`.
        """
        from timagetk.third_party.vt_features import _ppty_get_area
        cell_ids = self.ids(cell_ids)
        cids_no_ppty = set(cell_ids) - set(self.feature("area").keys())
        if cids_no_ppty != set():
            log.info(f"Computing {len(cids_no_ppty)} cells area (2D)...")
            # Geometric average of voxelsize
            vxs = np.power(np.prod(self.vxs), 1/2)
            self.set_feature(
                "area",
                _ppty_get_area(self.vt_ppty, labels=cids_no_ppty, real=real, vxs=vxs),
                update=True
            )

        return {cid: self.feature("area")[cid] for cid in cell_ids}


class Cell3D(Cell):
    """Class dedicated to computation of cell features from 3D labelled image.

    Attributes
    ----------
    image : timagetk.LabelledImage
        Labelled image to use for property computation.
    _feature : dict of str: dict
        Dictionary of computed cell features.

    Examples
    --------
    >>> from timagetk.io.dataset import shared_data
    >>> image = shared_data('synthetic', 'labelled')
    >>> from timagetk.features.cells import Cell3D
    >>> c = Cell3D(image)
    >>> c.ids()
    {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14}
    >>> c.volume(2)
    {2: 1780.0562121992195}
    >>> c.neighbors(3, min_area=30, real=True)

    """

    def __init__(self, image):
        """Constructor.

        Parameters
        ----------
        image : timagetk.LabelledImage
            3D labelled image to use for property computation.
        """
        Cell.__init__(self, image)
        self.add_feature("area")
        self.add_feature("volume")
        self.add_feature("neighbor_areas")

    def area(self, cell_ids=None, real=True):
        """Return the dictionary of cell areas.

        Parameters
        ----------
        cell_ids : int or list of int, optional
            If ``None`` (default) returns the dictionary for all cell ids.
            If an integer or a list of integers, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell ids.
        real : bool, optional
            If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

        Returns
        -------
        dict
            Cell area dictionary, ``{cid: area(cid)}`` for ``cid`` in `cell_ids`.

        Notes
        -----
        This should be similar to the sum of all wall areas for a given cell.
        """
        from timagetk.third_party.vt_features import _ppty_get_area
        cell_ids = self.ids(cell_ids)
        cids_no_ppty = set(cell_ids) - set(self.feature("area").keys())
        if cids_no_ppty != set():
            log.info(f"Computing {len(cids_no_ppty)} cells area...")
            # Geometric average of voxelsize
            vxs = np.power(np.prod(self.vxs), 1/3)
            if self.vt_ppty is None:
                self._init_vt_ppty(['contact-surface'], filter_neighbors=False)
            self.set_feature(
                "area",
                _ppty_get_area(self.vt_ppty, labels=cids_no_ppty, real=real, vxs=vxs),
                update=True
            )

        return {cid: self.feature("area")[cid] for cid in cell_ids}

    def volume(self, cell_ids=None, real=True):
        """Return the dictionary of cell volumes.

        Parameters
        ----------
        cell_ids : int or list of int, optional
            If ``None`` (default) returns the dictionary for all cell ids.
            If an integer or a list of integers, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell ids.
        real : bool, optional
            If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

        Returns
        -------
        dict
            Cell volumes dictionary, ``{cid: volume(cid)}`` for ``cid`` in `cell_ids`.
        """
        from timagetk.third_party.vt_features import _ppty_get_volume
        cell_ids = self.ids(cell_ids)
        cids_no_ppty = set(cell_ids) - set(self.feature("volume").keys())
        if cids_no_ppty != set():
            log.info(f"Computing {len(cids_no_ppty)} cells volume...")
            self.check_properties(['barycenter', 'volume'], filter_neighbors=False)
            self.set_feature(
                "volume",
                _ppty_get_volume(self.vt_ppty, labels=cids_no_ppty, real=real, vxs=self.vxs),
                update=True
            )
        return {cid: self.feature("volume")[cid] for cid in cell_ids}

    def neighbors(self, cell_ids=None, min_area=None, real=True):
        """Return the dictionary of cell neighbors.

        Parameters
        ----------
        cell_ids : int or list of int, optional
            If ``None`` (default) returns the dictionary for all cell ids.
            If an integer or a list of integers, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell ids.
        min_area : float, optional
            The minimum real contact area between two cells to be defined as neighbors. No minimum by default.
        real : bool, optional
            If ``True`` (default), the `min_area` value is in real world units, else in voxel units.

        Returns
        -------
        dict
            Cell neighbors dictionary, ``{cid: neighbors(cid)}`` for ``cid`` in `cell_ids`.
        """
        from timagetk.third_party.vt_features import _ppty_get_neighbor_areas

        if isinstance(cell_ids, list) and len(cell_ids) == 1:
            cell_ids = cell_ids[0]

        # Make a special case if only background is given
        if isinstance(cell_ids, int) and cell_ids == self.image.background:
            return self.image.neighbors(labels=cell_ids)

        cell_ids = self.ids(cell_ids)

        neighbors = self.image.neighbors(labels=cell_ids)
        if min_area is not None:
            log.info(f"Computing {len(cell_ids)} cells volume...")
            self.check_properties(['barycenter', 'volume', 'neighbors', 'contact-surface'],
                                  filter_neighbors=False)
            cids_no_ppty = set(cell_ids) - set(self.feature("neighbor_areas").keys())
            if cids_no_ppty != set():
                contact_area = _ppty_get_neighbor_areas(self.vt_ppty, labels=self.ids(), real=real,
                                                        vxs=self.vxs[0])  # DO NOT reduce with neighbors list!
                self.set_feature("neighbor_areas", contact_area, update=True)
            else:
                contact_area = {self.feature("neighbor_areas")[cid] for cid in cell_ids}
            th_neig = {cid: [n for n in neig if contact_area[cid, n] >= min_area] for cid, neig in neighbors.items()}
        else:
            th_neig = neighbors

        return th_neig

    def number_of_neighbors(self, cell_ids=None, min_area=None, real=True):
        """Return the dictionary of cell neighbors.

        Parameters
        ----------
        cell_ids : int or list of int, optional
            If ``None`` (default) returns the dictionary for all cell ids.
            If an integer or a list of integers, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell ids.
        min_area : float, optional
            The minimum real contact area between two cells to be defined as neighbors. No minimum by default.
        real : bool, optional
            If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

        Returns
        -------
        dict
            Cell neighbors dictionary, ``{cid: neighbors(cid)}`` for ``cid`` in `cell_ids`.
        """
        if min_area is not None:
            return {cid: len(neigh) for cid, neigh in self.neighbors(cell_ids, min_area, real).items()}
        else:
            return number_of_neighbors(self.image, cell_ids=cell_ids)
