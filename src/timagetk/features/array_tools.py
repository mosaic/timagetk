#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Module dedicated to the transformation of coordinates array."""

import copy
import math

import numpy as np
from scipy import ndimage as nd
from scipy.linalg import svd
from scipy.spatial import distance
from sklearn import linear_model
from sklearn import metrics
from sklearn.metrics import pairwise_distances
from skspatial.objects import Vector
from timagetk.bin.logger import get_logger

log = get_logger(__name__)


def coordinates_centering(coordinates, mean=None):
    """Center a set of 'coordinates' around their 'mean'.

    Parameters
    ----------
    coordinates : numpy.ndarray
        A ``N x d`` array of coordinates to center, with ``N`` the number of
        points, and ``d`` the dimensionality (usually 2 or 3).
    mean : list or numpy.ndarray, optional
        If ``None`` (default), the mean is computed before centering.
        Else should be a list or array of same dimensionality than the `coordinates`.

    Returns
    -------
    numpy.ndarray
        The centered set of coordinates

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.features.array_tools import coordinates_centering
    >>> coords = np.array([[1, 1, 1], [0, 0, -1]])
    >>> coordinates_centering(coords)
    array([[ 0.5,  0.5,  1. ],
           [-0.5, -0.5, -1. ]])

    """
    if mean is None:
        mean = np.mean(coordinates, axis=0)

    return coordinates - mean


def covariance_matrix(coordinates):
    """Return the covariance matrix of a set of coordinates.

    Parameters
    ----------
    coordinates : numpy.ndarray
        A ``N x d`` array of coordinates, where ``N`` is the number of coordinates and ``d`` their dimensionality (*e.g.* ``d=3``).

    Returns
    -------
    numpy.ndarray
        The ``d x d`` covariance matrix.

    Raises
    ------
    ValueError
        If ``N <= d``, the number of point should be strictly greater than dimensionality.

    Notes
    -----
    The covariance matrix of `X`, a set of coordinates, is computed as follows:
        ``cov(X) = 1/N * X . X^T``,
    where:
        - `N` is the number of coordinate;
        - `.` is the dot product;
        - `^T` is the transposes matrix

    Example
    -------
    >>> import numpy as np
    >>> from timagetk.features.array_tools import covariance_matrix
    >>> coords = np.array([[1., 1., 1.1], [1., 1., 0.9], [5., 1., 1.], [1., 5., 1.], [5., 5., 1.], [5., 2., 1.]])
    >>> print(coords.shape)
    (6, 3)
    >>> np.round(covariance_matrix(coords), 3)
    array([[ 4.8e+00,  4.0e-01, -0.0e+00],
           [ 4.0e-01,  3.9e+00, -0.0e+00],
           [-0.0e+00, -0.0e+00,  4.0e-03]])

    """
    n_points, init_dim = coordinates.shape
    if not n_points > init_dim:
        msg = "Input `coordinates` should be of shape (N, d), with N > d. "
        msg += "Got ({}, {})".format(n_points, init_dim)
        raise ValueError(msg)

    return np.cov(coordinates, rowvar=False)


def eigen_values_vectors(cov_matrix):
    """Extract the eigen vectors and associated eigen values from a covariance matrix.

    Parameters
    ----------
    cov_matrix : numpy.ndarray
        Point-set of coordinates

    Returns
    -------
    numpy.ndarray
        Length-3 list of sorted eigen values associated to the eigen vectors
    numpy.ndarray
        3x3 array of eigen vectors --by rows-- associated to sorted eigen values

    """
    assert max(cov_matrix.shape) <= 3
    # np.linalg.eig return eigenvectors by column !!
    eig_val, eig_vec = np.linalg.eig(cov_matrix)
    decreasing_index = eig_val.argsort()[::-1]
    eig_val, eig_vec = eig_val[decreasing_index], eig_vec[:, decreasing_index]
    eig_vec = np.array(eig_vec).T  # ... our standard is by rows !
    return eig_val, eig_vec


def find_geometric_median(coordinates):
    """Function finding the geometric median of an array of coordinates.

    Parameters
    ----------
    coordinates : numpy.ndarray
        A Nx3 array of coordinates.

    Returns
    -------
    int
        The index of the geometric median point for the array of coordinates.

    Example:
    --------
    >>> import numpy as np
    >>> from timagetk.features.array_tools import find_geometric_median
    >>> ar = np.array([[0,0,0], [0,1,0], [0,2,0], [0,3,0], [0,4,0]])
    >>> find_geometric_median(ar)
    2
    >>> # To get the median coordinates use the index:
    >>> ar[2]
    [0, 2, 0]
    """
    # Need an array with 3D coordinates as rows:
    if coordinates.shape[0] == 3:
        coordinates = coordinates.T

    log.debug(f"Searching for geometric median of {coordinates.shape[0]} {coordinates.shape[1]}-D coordinates.")
    # Compute the pairwise distance matrix:
    if coordinates.shape[0] < 2500:
        pw_dist = np.linalg.norm(coordinates[:, np.newaxis] - coordinates[np.newaxis, :], axis=2)
    elif coordinates.shape[0] < 15000:
        import psutil
        mem = psutil.virtual_memory()
        pw_size = coordinates.shape[0] ** 2 * 8
        if pw_size > mem.available * 0.8:
            mat = round(pw_size / 1024 / 1024 / 1024, 1)  # Go
            mem = round(mem.available / 1024 / 1024 / 1024, 1)  # Go
            log.warning(f"Trying to create a {mat}Go matrix with {mem}Go of RAM available!")
            return None
        else:
            pw_dist = pairwise_distances(coordinates, metric='euclidean', n_jobs=-1)
    else:
        return None

    # Return the index of the point with the shortest distance to all others: the median voxel!
    return np.argmin(pw_dist.sum(axis=1))


def geometric_median(coordinates):
    """Function finding the geometric median of an array of coordinates.

    Parameters
    ----------
    coordinates : numpy.ndarray
        A Nx3 array of coordinates.

    Returns
    -------
    numpy.ndarray
        The geometric median for the array of coordinates.

    Example:
    --------
    >>> import numpy as np
    >>> from timagetk.features.array_tools import geometric_median
    >>> ar = np.array([[0,0,0], [0,1,0], [0,2,0], [0,3,0], [0,4,0]])
    >>> geometric_median(ar)

    """
    median_idx = find_geometric_median(coordinates)
    if median_idx is None:
        log.debug("Using convergence algorithm...")
        return convergence_geometric_median(coordinates)
    else:
        return coordinates[median_idx]


def convergence_geometric_median(X, eps=1e-5):
    """Yehuda Vardi and Cun-Hui Zhang's algorithm for the geometric median.

    Parameters
    ----------
    X : numpy.ndarray
        A Nx3 array of coordinates.
    eps : float, optional
        The minimal Eucliedean distance to stop the search.

    Returns
    -------
    numpy.ndarray
        The geometric median for the array of coordinates.

    Example:
    --------
    >>> import numpy as np
    >>> from timagetk.features.array_tools import convergence_geometric_median
    >>> ar = np.array([[0,0,0], [0,1,0], [0,2,0], [0,3,0], [0,4,0]])
    >>> convergence_geometric_median(ar)

    References
    ----------
    http://www.pnas.org/content/97/4/1423.full.pdf
    https://stackoverflow.com/a/30305181

    """
    from scipy.spatial.distance import cdist, euclidean
    y = np.mean(X, axis=0)

    while True:
        D = cdist(X, [y])
        nonzeros = (D != 0)[:, 0]

        Dinv = 1 / D[nonzeros]
        Dinvs = np.sum(Dinv)
        W = Dinv / Dinvs
        T = np.sum(W * X[nonzeros], axis=0)

        num_zeros = len(X) - np.sum(nonzeros)
        if num_zeros == 0:
            y1 = T
        elif num_zeros == len(X):
            return y
        else:
            R = (T - y) * Dinvs
            r = np.linalg.norm(R)
            rinv = 0 if r == 0 else num_zeros / r
            y1 = max(0, 1 - rinv) * T + min(1, rinv) * y

        if euclidean(y, y1) < eps:
            return y1

        y = y1


def projection_matrix(coordinates, subspace_rank=2):
    """Return the projection matrix of a set of coordinates.

    Parameters
    ----------
    coordinates : numpy.ndarray
        An ``N x d`` array of coordinates to center, with ``N`` the number of points, and ``d`` the dimensionality (usually 2 or 3).
    subspace_rank : int, optional
        The dimension reduction to apply, 2 by default.
        Should respect ``N`` > ``subspace_rank`` > 0.

    Returns
    -------
    numpy.ndarray
        The ``d x d`` projection matrix, with ``d`` the coordinates dimensionality.

    Raises
    ------
    ValueError
        If ``N <= d``, the number of point should be strictly greater than dimensionality.
        If ``d >= subspace_rank``, subspace should be lower than initial dimensionality.
        If ``subspace_rank <= 0``, subspace rank should be positive.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.features.array_tools import projection_matrix
    >>> coords = np.array([[1., 1., 1.1], [1., 1., 0.9], [5., 1., 1.], [1., 5., 1.], [5., 5., 1.], [5., 2., 1.]])
    >>> projection_matrix(coords)

    >>> # - Projection matrix using centered coordinates:
    >>> centroid = coords.mean(axis=0)
    >>> c_coords = coords - centroid
    >>> proj = projection_matrix(c_coords)
    >>> # Project the centered coordinates:
    >>> c_coords_2 = np.dot(c_coords, proj)

    >>> # - 3D view:
    >>> from mayavi.mlab import points3d, show
    >>> # -- Add the original points:
    >>> x, y, z = c_coords.T
    >>> points3d(x, y, z, color=(1., 1., 1.), mode='cube', scale_factor=0.1)
    >>> # -- Add the projected points:
    >>> xp, yp, zp = c_coords_2.T
    >>> points3d(xp, yp, zp, color=(0., 1., 0.), mode='sphere', scale_factor=0.1)
    >>> # -- Add a red sphere at [0, 0, 0] (center):
    >>> points3d(0, 0, 0, color=(1., 0., 0.), mode='sphere', scale_factor=0.2)
    >>> show()

    """
    n_points, init_dim = coordinates.shape
    try:
        assert n_points > init_dim
    except AssertionError:
        msg = "Input `coordinates` should be of shape (N, d), with N > d. "
        msg += "Got ({}, {})".format(n_points, init_dim)
        raise ValueError(msg)
    try:
        assert init_dim > subspace_rank
    except AssertionError:
        msg = "The subspace rank should be inferior to the coordinates initial dimensionality.\n"
        msg += "Got subspace rank = {} & coordinates dimensionality = {})"
        raise ValueError(msg.format(subspace_rank, init_dim))
    try:
        assert subspace_rank > 0
    except AssertionError:
        raise ValueError("Input `subspace_rank` should be strictly positive!")

    centroid = coordinates.mean(axis=0)
    if np.round(np.linalg.norm(centroid), decimals=10) != 0:
        log.warning("Given coordinates are not centered!")
        log.info("Coordinates centroid: {}".format(centroid))
    # -- Compute the centered coordinates:
    c_coords = coordinates_centering(coordinates)

    # -- Singular Value Decomposition (SVD) of centered coordinates:
    U, D, V = np.linalg.svd(c_coords)
    V = V.T
    # -- Projection matrix:
    H = np.dot(V[:, 0:subspace_rank], V[:, 0:subspace_rank].T)

    return H


def pointset_subspace_projection(coordinates, subspace_rank=2, pointset=None):
    """Project a set of coordinates onto its subspace of given rank.

    Parameters
    ----------
    coordinates : numpy.ndarray
        An ``N x d`` array of coordinates to center, with ``N`` the number of points, and ``d`` the dimensionality (usually 2 or 3).
    subspace_rank : int, optional
        The dimension reduction to apply, 2 by default. Should respect ``N`` > ``subspace_rank`` > 0.
    pointset : numpy.ndarray, optional
        If given, an ``K x d`` array of point to center, with ``K`` the number of points, and ``d`` the dimensionality (usually 2 or 3).

    Returns
    -------
    numpy.ndarray
        The projected (``N x d``) array of coordinate.
    numpy.ndarray
        If given, the(``K x d``) array of projected pointset, uses the ``coordinates`` projection matrix, else ``None``.

    Raises
    ------
    ValueError
        If ``N <= d``, need a number of point strictly greater than dimensionality.
        If ``d >= subspace_rank``, subspace should be lower than initial dimensionality.
        If ``subspace_rank <= 0``, subspace rank should be positive.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.features.array_tools import pointset_subspace_projection
    >>> coords = np.array([[1., 1., 1.1], [1., 1., 0.9], [5., 1., 1.], [1., 5., 1.], [5., 5., 1.], [5., 2., 1.]])
    >>> proj_coords, _ = pointset_subspace_projection(coords, 2)
    >>> print(proj_coords)

    >>> proj_coords, proj_points = pointset_subspace_projection(coords, 2, coords[0:2])
    >>> print(proj_points)

    """
    # Get the coordinates' centroid & center the coordinates:
    centroid = coordinates.mean(axis=0)
    c_coords = coordinates - centroid
    # Get the projection matrix:
    proj_mat = projection_matrix(c_coords, subspace_rank)
    # Project the coordinates:
    p_coords = np.dot(c_coords, proj_mat) + centroid

    if pointset is None:
        return p_coords, None
    else:
        return p_coords, np.dot(pointset - centroid, proj_mat) + centroid


def pointset_subspace_reduction(coordinates, subspace_rank=2, pointset=None, **kwargs):
    """Reduce a set of coordinates to the subspace of given rank.

    Parameters
    ----------
    coordinates : numpy.ndarray
        An ``N x d`` array of coordinates to project, with ``N`` the number of points, and ``d`` the dimensionality (usually 2 or 3).
    subspace_rank : int, optional
        The dimension reduction to apply, ``2`` by default. Should respect ``N`` > ``subspace_rank`` > 0.
    pointset : numpy.ndarray, optional
        If given, an ``K x d`` array of point to center, with ``K`` the number of points, and ``d`` the dimensionality (usually 2 or 3).

    Returns
    -------
    numpy.ndarray
        The projected (``N x subspace_rank``) array of coordinates.
    numpy.ndarray
        If given, the(``K x subspace_rank``) array of projected pointset, uses the ``coordinates`` projection matrix, else ``None``.

    Raises
    ------
    ValueError
        If ``N <= d``, need a number of point strictly greater than dimensionality.
        If ``d >= subspace_rank``, subspace should be lower than initial dimensionality.
        If ``subspace_rank <= 0``, subspace rank should be positive.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.features.array_tools import pointset_subspace_reduction
    >>> coords = np.array([[1., 1., 1.1], [1., 1., 0.9], [5., 1., 1.], [1., 5., 1.], [5., 5., 1.], [5., 2., 1.]])
    >>> coords_2d, _ = pointset_subspace_reduction(coords, 2)
    >>> print(coords_2d)

    >>> coords_2d, points_2d = pointset_subspace_reduction(coords, 2, coords[0:2])
    >>> print(points_2d)

    """
    dim = coordinates.shape[1]
    # Get the coordinates' centroid:
    centroid = coordinates.mean(axis=0)
    # Get the projection matrix and the centered coordinates:
    proj_coords, proj_points = pointset_subspace_projection(coordinates, subspace_rank, pointset)

    # - Compute the covariance matrix (inertia axis):
    cov = covariance_matrix(proj_coords)
    # - Decompose the covariance matrix to use rotation matrix as base change and project flat landmarks in their 2D subspace:
    R, D, Rt = np.linalg.svd(cov)
    sub_coords = np.dot(proj_coords - centroid, R)
    if kwargs.get("verbose", False):
        # - Compute and print the subspace reduction error:
        err = metrics.mean_squared_error(np.zeros_like(sub_coords[:, subspace_rank:dim + 1]),
                                         sub_coords[:, subspace_rank:dim + 1])
        log.info("Subspace reduction mean squared error: {}".format(err))

    sub_coords = sub_coords + centroid
    sub_coords = sub_coords[:, 0:subspace_rank]

    if pointset is None:
        return sub_coords, None
    else:
        sub_points = np.dot(proj_points, R[:, 0:subspace_rank])
        sub_points = sub_points + centroid[0:subspace_rank]
        return sub_coords, sub_points


def affine_deformation_tensor(xyz_t1, xyz_t2):
    """Compute the affine deformation matrix between two time-points (t1 & t2).

    With `xyz_t1` and `xyz_t2` being the landmarks coordinates, the affine transformation matrix A is obtained as
    follows: :math:`xyz_{t2} = A . xyz_{t1} + \epsilon`, where :math:`\epsilon` is the registration error.

    Parameters
    ----------
    xyz_t1 : numpy.ndarray
        (N x d) matrix giving the N landmarks coordinates before deformation.
        With 'd' the dimensionality (*i.e.* d=2 if 2D, d=3 if 3D)
    xyz_t2 : numpy.ndarray
        (N x d) matrix giving the N landmarks coordinates after deformation.
        With 'd' the dimensionality (*i.e.* d=2 if 2D, d=3 if 3D)

    Returns
    -------
    numpy.ndarray
        (d x d) matrix giving the affine transformation matrix between the centered vertices position of two time-points.
    float
        The R² score, assessing the quality of the registration.

    Notes
    -----
    R^2 (coefficient of determination) regression score function.
    Best possible score is 1.0, and it can be negative (because the model can be arbitrarily worse).
    A constant model that always predicts the expected value of y, disregarding the input features, would get a R² score of 0.0.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.features.array_tools import affine_deformation_tensor
    >>> xyz0 = np.array([[0, 0], [1, 0], [0, 1], [1, 1]], dtype=float)
    >>> xyz1 = np.array([[0, 0], [2, 0], [0, 2], [2, 2]], dtype=float)
    >>> A, r2 = affine_deformation_tensor(xyz0, xyz1)
    >>> print(A.diagonal())  # [2., 2.] as expected!
    >>> print(r2)  # No error since its an exact affine deformation!

    """
    regr = linear_model.LinearRegression(fit_intercept=True)
    regr.fit(xyz_t1, xyz_t2)
    return regr.coef_, metrics.r2_score(xyz_t2, regr.predict(xyz_t1))


def stretch_tensors(deformation_matrix):
    """Compute the stretch tensors, before & after registration, and the associated norms.

    Parameters
    ----------
    deformation_matrix : numpy.ndarray
        (d x d) deformation matrix (typically from 'affine_deformation_tensor');

    Returns
    -------
    array
        (d x d) array of vectors giving the "before deformation" directions;
    array
        Length-d array of floats giving the extent of the deformation for each directions;
    array
        (d x d) array of vectors giving the "after deformation" directions.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.features.array_tools import stretch_tensors
    >>> from timagetk.features.array_tools import affine_deformation_tensor
    >>> xyz0 = np.array([[0, 0], [1, 0], [0, 1], [1, 1]], dtype=float)
    >>> xyz1 = np.array([[0, 0], [2, 0], [0, 2], [2, 2]], dtype=float)
    >>> A, r2 = affine_deformation_tensor(xyz0, xyz1)
    >>> R, D, Q = stretch_tensors(A)
    >>> print(D)  # [2., 2.] as expected!

    """
    #  Singular Value Decomposition (SVD) of `deformation_matrix`.
    R, D, Q = svd(deformation_matrix)
    return R, D, Q.T


def fractional_anisotropy_eigenval(strain_rates):
    """Compute the fractional anisotropy of a diffusion tensor.

    Let :math:`D` be the diffusion tensor and :math:`\lambda_i` its eigenvalues:
    :math:`FA = \sqrt{\dfrac{3}{2}} \dfrac{\sqrt{(\lambda_1 - \hat{\lambda})^2 + (\lambda_2 - \hat{\lambda})^2 + (\lambda_3 - \hat{\lambda})^2}}{\sqrt{\lambda_1^2 + \lambda_2^2 + \lambda_3^2}}`
    with the mean :math:`\hat{\lambda} = (\lambda_1 + \lambda_2 + \lambda_3)/3`

    Parameters
    ----------
    strain_rates : list
        List of eigenvalues of a diffusion tensor

    Returns
    -------
    float
        The fractional anisotropy.

    Note
    ----
    Takes values in ``[0, 1[``, the closer to ``0`` the more isotropic.

    Examples
    --------
    >>> from timagetk.features.array_tools import fractional_anisotropy_eigenval
    >>> fractional_anisotropy_eigenval((3, 2, 1))
    0.4629100498862757
    >>> fractional_anisotropy_eigenval((3, 3, 3))
    0.0

    """
    assert len(strain_rates) == 3
    l1, l2, l3 = strain_rates
    if (l1 + l2 + l3) == 0:
        return None

    l = (l1 + l2 + l3) / 3.
    return math.sqrt(3. / 2.) * (
            math.sqrt((l1 - l) ** 2 + (l2 - l) ** 2 + (l3 - l) ** 2) / math.sqrt(l1 ** 2 + l2 ** 2 + l3 ** 2))


def fractional_anisotropy_tensor(stretch_tensor):
    """Compute fractional anisotropy of a diffusion tensor.

    Parameters
    ----------
    stretch_tensor : numpy.ndarray
        The stretch tensor to use to compute fractional anisotropy.

    Returns
    -------
    float
        The fractional anisotropy.

    Notes
    -----
    The fractional anisotropy of a diffusion tensor :math:`D` is computed as follows:
    :math:`\text{FA} = \sqrt{\dfrac{1}{2}} (3 - \dfrac{1}{trace(R^2)}`
    where :math:`R` is the "normalized" diffusion tensor: :math:`R=\dfrac{D}/{trace(D)}`.
    """
    assert stretch_tensor.shape == (3, 3)
    D = stretch_tensor
    R = D / np.matrix.trace(D)
    return math.sqrt(1. / 2. * (3 - (1. / np.matrix.trace(R ** 2))))


def strain_anisotropy_2d(eigenval):
    """Compute the 2D strain anisotropy.

    Parameters
    ----------
    eigenval : list
        Length-2 list of strain values.

    Returns
    -------
    float
        The 2D strain anisotropy.
    """
    l1, l2 = eigenval
    return (l1 - l2) / (l1 + l2)


def sort_boundingbox(boundingbox, label_1, label_2):
    """Use this to determine which label as the smaller bounding-box.

    Parameters
    ----------
    boundingbox : dict
        The dictionary of bounding-boxes, must contain `label_1` &`label_2` as keys.
    label_1, label_2 : int
        The index to use to compare bounding-boxes.

    Returns
    -------
    int
        The label corresponding to the one with the smallest bounding-box.
    """
    assert isinstance(boundingbox, dict)
    if (label_1 not in boundingbox) and label_2 in boundingbox:
        return label_2, label_1
    if label_1 in boundingbox and (label_2 not in boundingbox):
        return label_1, label_2
    if (label_1 not in boundingbox) and (label_2 not in boundingbox):
        return None, None

    box_1 = boundingbox[label_1]
    box_2 = boundingbox[label_2]
    vol_bbox_1 = (box_1[0].stop - box_1[0].start) * (box_1[1].stop - box_1[1].start) * (box_1[2].stop - box_1[2].start)
    vol_bbox_2 = (box_2[0].stop - box_2[0].start) * (box_2[1].stop - box_2[1].start) * (box_2[2].stop - box_2[2].start)

    return (label_1, label_2) if vol_bbox_1 < vol_bbox_2 else (label_2, label_1)


def find_smallest_boundingbox(image, label_1, label_2):
    """Return the smallest bounding-box within `image` between cell-labels `label_1` & `label_2`.

     Parameters
    ----------
    image : timagetk.LabelledImage
        The labelled image.
    label_1, label_2 : int
        The index to use to compare bounding-boxes.

    Returns
    -------
    tuple
        The bounding-box.
    """
    box = nd.find_objects(image, max_label=max([label_1, label_2]))
    box = {label_1: box[label_1 - 1],
           label_2: box[label_2 - 1]}  # we do 'label_x - 1' since 'nd.find_objects' start at '1' (and not '0') !
    label_1, label_2 = sort_boundingbox(box, label_1, label_2)
    return box[label_1]


def euclidean_distance(ptsA, ptsB, default=np.nan):
    """Function computing the Euclidean distance between two points (2D or 3D coordinates).

    Parameters
    ----------
    ptsA : list or numpy.ndarray
        Coordinates of point A.
    ptsB : list or numpy.ndarray
        Coordinates of point B.
    default : any, optional
        The default value to return if the distance cannot be computed.
        Default to ``np.nan``.

    Returns
    -------
    float
        The Euclidean distance between points A & B.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.features.array_tools import euclidean_distance
    >>> euclidean_distance([0, 0], [0, 5])  # distance in 2D
    5.0
    >>> euclidean_distance([0, 0, 0], [0, 0, 5])  # distance in 3D
    5.0
    >>> euclidean_distance([0, 0, 0], [np.nan, np.nan, np.nan])  # not defined, so default value is returned
    nan
    >>> euclidean_distance([0, 0, 0], np.nan)  # not defined, so default value is returned
    nan

    """
    from typing import Iterable
    # Return NaN if not possible to compute the distance:
    if isinstance(ptsA, Iterable):
        if any(np.isnan(ptsA)):
            return default
    else:
        if np.isnan(ptsA):
            return default
    if isinstance(ptsB, Iterable):
        if any(np.isnan(ptsB)):
            return default
    else:
        if np.isnan(ptsB):
            return default

    # Return NaN if not the same dimensionality:
    if len(ptsA) != len(ptsB):
        return default

    return distance.euclidean(ptsA, ptsB)


def quantile_exclusion(data, q=0.99):
    """Remove outliers, values above the qth quantile, from the data list.

    Parameters
    ----------
    data : list or numpy.ndarray
        A vector of scalar values to filter.
    q : float
        The q-th quantile, in ``]0, 1[``, to use for filtering.

    Returns
    -------
    numpy.ndarray
        The array without outlier values.

    """
    from numpy import nanquantile
    if isinstance(data, list):
        data = np.array(data)
    qv = nanquantile(data, q)
    return data[data <= qv]


def lof_exclusion(data, **kwargs):
    """Unsupervised Outlier Detection using Local Outlier Factor (LOF).

    Parameters
    ----------
    data: dict or list or tuple or set or numpy.ndarray
        A dictionary or vector of scalar values to filter.

    Returns
    -------
    Any
        The input `data` with outliers set to `np.nan`, input type is preserved.

    Notes
    -----
    Keyword arguments are passed to the ``sklearn.neighbors.LocalOutlierFactor`` method.

    Multi-dimensionnal arrays are accepted, observations are organized by columns and individuals by rows!

    References
    ----------
    https://scikit-learn.org/stable/auto_examples/neighbors/plot_lof_outlier_detection.html#sphx-glr-auto-examples-neighbors-plot-lof-outlier-detection-py
    https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.LocalOutlierFactor.html#sklearn.neighbors.LocalOutlierFactor

    Examples
    --------
    >>> from timagetk.algorithms.clustering import distance_matrix_from_vector
    >>> from timagetk import TissueImage3D
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.tasks.tissue_graph import tissue_graph_from_image
    >>> from matplotlib import pyplot as plt
    >>> from timagetk.features.array_tools import lof_exclusion
    >>> tissue = TissueImage3D(shared_data('flower_labelled', 0), background=1, not_a_label=0)
    >>> # EXAMPLE #1 : Outliers detection on a dictionary of cells' volume
    >>> volumes = tissue.cells.volume()  # get the dictionary of cells' volume
    >>> plt.hist(list(volumes.values()), bins=100)  # show tha we have some outliers (big values) from segmentation errors
    >>> c_volumes = lof_exclusion(volumes)  # detect outliers using 'Local Outlier Factor'
    >>> plt.hist(list(c_volumes.values()), bins=100)  # cell volume distribution is now "clearer"
    >>> # EXAMPLE #2 : Outliers detection on a list of cells' volume
    >>> volumes = list(tissue.cells.volume().values())  # get the list of cells' volume
    >>> c_volumes = lof_exclusion(volumes)  # detect outliers using 'Local Outlier Factor'
    >>> plt.hist(c_volumes, bins=100)  # cell volume distribution is now "clearer"

    """
    from sklearn.neighbors import LocalOutlierFactor

    input_data_type = type(data)
    if isinstance(data, dict):
        keys, values = list(data.keys()), list(data.values())
        data = np.array(values)
    elif isinstance(data, (list, tuple, set)):
        data = np.array(data)
    else:
        assert isinstance(data, np.ndarray)

    if data.ndim == 1:
        data = data.reshape(-1, 1)  # reshape as a column of data

    clf = LocalOutlierFactor(**kwargs)
    pred = clf.fit_predict(data)
    data[pred == -1] = np.nan

    if isinstance(input_data_type(), dict):
        data = dict(zip(keys, data))
    else:
        data = input_data_type(data)

    return data


def outliers_exclusion(data, std_multiplier=3, display_data_plot=False):
    """Return a list or a dict (same type as `data`) cleaned out of outliers.

    Outliers are detected according to a distance from standard deviation.
    """
    from numpy import std, mean
    tmp = copy.deepcopy(data)
    if isinstance(data, list):
        borne = mean(tmp) + std_multiplier * std(tmp)
        N = len(tmp)
        n = 0
        while n < N:
            if (tmp[n] > borne) or (tmp[n] < -borne):
                tmp.pop(n)
                N = len(tmp)
            else:
                n += 1
    if isinstance(data, dict):
        borne = mean(tmp.values()) + std_multiplier * std(tmp.values())
        for n in data:
            if (tmp[n] > borne) or (tmp[n] < -borne):
                tmp.pop(n)
    if display_data_plot:
        import matplotlib.pyplot as plt
        if isinstance(data, list):
            plt.plot(data)
            plt.plot(tmp)
        plt.show()
        if isinstance(data, dict):
            plt.plot(data.values())
            plt.plot(tmp.values())
        plt.show()
    return tmp


# ------------------------------------------------------------------------------
#
# VECTOR based functions:
#
# ------------------------------------------------------------------------------

def orient_vector(vec, ref_vec):
    """Orient a vector with respect to a reference vector.

    Parameters
    ----------
    vec : numpy.ndarray
        The vector to be oriented.
    ref_vec : numpy.ndarray
        The reference vector.

    Returns
    -------
    numpy.ndarray
        The oriented vector.
    """
    # If the vector correlation is negative, invert its direction:
    if np.dot(vec, ref_vec) < 0:
        vec = -vec
    return vec


def vector_correlation(vect1, vect2):
    """Compute correlation between two vectors.

    Parameters
    ----------
    vect1, vect2 : numpy.ndarray
        Two vectors to correlate, as arrays of shape``(d,)``, where ``d`` is the dimensionnality of the space.

    Returns
    -------
    float
        The vector correlation coefficient.

    Notes
    -----
    It is the cosine of the angle between two vectors in Euclidean space of any number of dimensions.
    The dot product is directly related to the cosine of the angle between two vectors if they are normed!

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.features.array_tools import vector_correlation
    >>> v1 = np.array([0, 0, 1])
    >>> v2 = np.array([0, 1, 1])
    >>> vector_correlation(v1, v2)
    0.5

    """
    from scipy.stats.stats import pearsonr
    return pearsonr(vect1, vect2)[0]


def angle_between_vectors(v1, v2, orient=None):
    """Compute the angles between two vectors.

    Parameters
    ----------
    v1, v2 : list, tuple or numpy.ndarray
        Two vectors to consider.
    orient : list, tuple or numpy.ndarray
        An orientation vector, used to define an oriented basis and return the complement angle if needed.

    Returns
    -------
    float
        The angle between the two vectors, in degrees.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.features.array_tools import angle_between_vectors
    >>> # Example #1 - 2D unit vectors
    >>> v1 = np.array([1, 0])  # X unit vector
    >>> v2 = np.array([0, 1])  # Y unit vector
    >>> angle_between_vectors(v1, v2)  # should be 90°
    90.0
    >>> # Example #2 - Opposite 3D Z unit vectors
    >>> v1 = np.array([0, 0, 1])  # Z unit vector
    >>> v2 = np.array([0, 0, -1])  # -Z unit vector
    >>> angle_between_vectors(v1, v2)  # should be 180°
    180.0
    """
    # Get the unit vectors:
    v1 = Vector(v1).unit()
    v2 = Vector(v2).unit()

    if orient is not None:
        orient = Vector(orient).unit()
        angle = v1.angle_signed_3d(v2, direction_positive=orient)
    else:
        angle = v1.angle_between(v2)

    return np.degrees(angle)


def angle_between_plane(n1, n2, orient=None):
    """Uses the normal vector of two planes to compute the angle they define.

    Parameters
    ----------
    n1, n2 : list, tuple or numpy.ndarray
        Two normal vectors to the planes to consider.
    orient : list, tuple or numpy.ndarray
        An orientation vector, used to define an oriented basis and return the complement angle if needed.

    Returns
    -------
    float
        The (oriented) angle between the two planes, in degrees.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.features.array_tools import angle_between_plane
    >>> n1 = np.array([0, 0, 1])  # Z unit vector
    >>> n2 = np.array([0, 0, -1])  # -Z unit vector
    >>> angle_between_plane(n1, n2)  # should be 0°
    0.
    """
    return 180 - angle_between_vectors(n1, n2, orient)
