#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Module for cell-wall feature extraction methods."""

from scipy.optimize import curve_fit
from tqdm.autonotebook import tqdm

from timagetk.bin.logger import get_logger
from timagetk.components.labelled_image import LabelledImage
from timagetk.features.array_tools import covariance_matrix
from timagetk.features.array_tools import geometric_median
from timagetk.features.cells import assert_dimensionality
from timagetk.features.features import Features
from timagetk.third_party.vt_features import get_neighbor_areas

log = get_logger(__name__)


def _poly_curve(x, a, b, c):
    return a * x + b * x ** 2 + c


def _curve_length(f, a, b, tol=1e-6):
    """Compute the line length of function f(x) for a <= x <= b.

    Stop when two consecutive approximations are closer than the value of tol.
    """
    from math import hypot

    nsteps = 1  # number of steps to compute
    oldlength = 1.0e20
    length = 1.0e10
    while abs(oldlength - length) >= tol:
        nsteps *= 2
        fx1 = f(a)
        xdel = (b - a) / nsteps  # space between x-values
        oldlength = length
        length = 0
        for i in range(1, nsteps + 1):
            fx0 = fx1  # previous function value
            fx1 = f(a + i * (b - a) / nsteps)  # new function value
            length += hypot(xdel, fx1 - fx0)  # length of small line segment
    return length


def length(image, wall_ids=None, real=True, **kwargs):
    """Compute the wall property *length*, require a 2D image.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Labelled image to use for property computation.
    wall_ids : int or list of int or str, optional
        If ``None`` (default) returns all cell-wall ids found in the image.
        If a len-2 tuple of int, or a list of them, filter returned dictionary keys with those ids, but make sure
        they are defined in the list of cell-wall ids.
    real : bool, optional
        If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

    Returns
    -------
    dict
        Cell-wall length dictionary, ``{wid: wall_length(wid)}`` for ``wid`` in `wall_ids`.

    Raises
    ------
    ValueError
        If the image is not 2D.

    Examples
    --------
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> image = shared_data('flower_labelled', 0)
    >>> mid_z = image.get_shape('z') // 2
    >>> image = image.get_slice(mid_z)
    >>> from timagetk.features.cell_walls import length
    >>> length(image, [(1, 10)])  # Get the epidermis length for cell 10
    {(1, 1001): 445.39374}
    >>> length(image, [(10, 1)])  # remember, to define a wall-id, cell-ids order is irrelevant
    {(1, 1001): 445.39374}
    >>> length(image)  # Get the area for every -detected- walls
    {(108, 916): 17.567963,
     (87, 229): 10.0086975,
     (128, 404): 18.54215,
     (462, 635): 107.0564,
      ...}

    """
    raise NotImplementedError("Please HELP!")
    assert_dimensionality(image, 'cell wall length', 2)
    verbose = kwargs.get('verbose', False)

    # Check the provided wall_ids:
    wall_ids = image.surfels(wall_ids)
    if verbose:
        log.info(f"Computing 'length' feature for {len(wall_ids)} cell walls... ")

    wall_length = {}
    for wall_id in wall_ids:
        # Get 2D cell-wall (face) coordinates:
        x, y = image.surfel_coordinates(wall_id, real=real)[wall_id].T
        # Add 2D cell-vertex (nodes) coordinates:
        x, y = image.pointel_coordinates(wall_id, real=real)[wall_id].T
        # Find the two extreme voxels index
        # TODO: use the nodes positions as boundaries?
        min_bound, max_bound = 0, -1
        # Fit a polynomial curve:
        (a, b, c), _ = curve_fit(_poly_curve, x, y,
                                 bounds=[[x[min_bound], y[min_bound]], [x[min_bound], y[min_bound]]])

        def _poly_fit(a, b, c):
            return _poly_curve(x, a, b, c)

        # Compute curve length
        wall_length[wall_id] = _curve_length(_poly_fit, [x[min_bound], y[min_bound]], [x[min_bound], y[min_bound]])

    return wall_length


def median_coordinate(image, wall_ids=None, real=True):
    """Compute the wall property geometric *median coordinate*.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Labelled image to use for property computation.
    wall_ids : int or list of int or str, optional
        If ``None`` (default) returns all cell-wall ids found in the image.
        If a len-2 tuple of int, or a list of them, filter returned dictionary keys with those ids, but make sure
        they are defined in the list of cell-wall ids.
    real : bool, optional
        If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

    Returns
    -------
    dict
        Cell-wall length dictionary, ``{wid: median(wid)}`` for ``wid`` in `wall_ids`.

    Raises
    ------
    ValueError
        If the image is not 3D.

    Examples
    --------
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> image = shared_data('flower_labelled', 0)
    >>> from timagetk.features.cell_walls import median_coordinate
    >>> median_coordinate(image, [(1, 1001)])  # Get the epidermis area for cell 1001
    {(1, 1001): array([29.84768081, 66.90688181, 38.46144104])}
    >>> median_coordinate(image, [(1001, 1)])  # remember, to define a wall-id, cell-ids order is irrelevant
    {(1, 1001): array([29.84768081, 66.90688181, 38.46144104])}
    >>> wmc = median_coordinate(image)  # Get the area for every -detected- walls

    """
    wall_ids = image.surfels(wall_ids)  # Check the provided wall_ids
    geom_median = {}
    for wall_id in tqdm(wall_ids, unit="wall"):
        geom_median[wall_id] = geometric_median(image.surfel_coordinates(wall_id, real=real)[wall_id])

    return geom_median


def inertia_axis(image, wall_ids=None, real=True):
    """Compute the wall property *inertia axis*.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Labelled image to use for property computation.
    wall_ids : int or list of int or str, optional
        If ``None`` (default) returns all cell-wall ids found in the image.
        If a len-2 tuple of int, or a list of them, filter returned dictionary keys with those ids, but make sure
        they are defined in the list of cell-wall ids.
    real : bool, optional
        If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

    Returns
    -------
    dict
        Cell-wall length dictionary, ``{wid: inertia_axis(wid)}`` for ``wid`` in `wall_ids`.

    Raises
    ------
    ValueError
        If the image is not 3D.

    Notes
    -----
    If a cell-wall is made of less than four coordinates (voxels), it is filtered out.

    Examples
    --------
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> image = shared_data('flower_labelled', 0)
    >>> from timagetk.features.cell_walls import inertia_axis
    >>> inertia_axis(image, (1001, 64))  # access the feature value for one wall
    {(64, 1001): array([[ 0.78908253, -0.26160915, -0.22924577],
                        [-0.26160915,  1.23360102, -0.59983372],
                        [-0.22924577, -0.59983372,  0.57947978]])}
    >>> i_axis = inertia_axis(image)  # get the feature dictionary for all walls

    """
    assert_dimensionality(image, 'cell wall inertia axis', 3)
    wall_ids = image.surfels(wall_ids)  # Check the provided wall_ids
    coords = image.surfel_coordinates(surfel_ids=wall_ids, real=real)
    return {wall: covariance_matrix(coord) for wall, coord in tqdm(coords.items(), unit="wall") if coord.shape[0] >= 4}


class Wall(Features):
    """Wall related feature class.

    Attributes
    ----------
    image
        The labelled image, should contain walls to analyse.
    """

    def __init__(self, image):
        """Wall feature class constructor.

        Parameters
        ----------
        image : timagetk.LabelledImage
            The labelled image, should contain walls to analyse.

        Examples
        --------
        >>> from timagetk.features.cell_walls import Wall
        >>> from timagetk.io.dataset import shared_data
        >>> image = shared_data('flower_labelled', 0)
        >>> wf = Wall(image)
        """
        super().__init__()
        self.image = image
        self.add_feature("geometric_median")
        self.add_feature("inertia_axis")

    def ids(self, wall_ids=None):
        """Return the list of cell-wall ids found in the image.

        Parameters
        ----------
        wall_ids : tuple of int or list of tuple of int, optional
            If ``None`` (default) returns all cell-wall ids found in the image.
            If a len-2 tuple of int, or a list of them, make sure it is or they are in the image.

        Returns
        -------
        list
            List of cell-wall ids found in the image.

        Examples
        --------
        >>> from timagetk.features.cell_walls import Wall
        >>> from timagetk.io.dataset import shared_data
        >>> image = shared_data('flower_labelled', 0)
        >>> wf = Wall(image)
        >>> wall_ids = wf.ids()
        >>> print(wall_ids[:10])  # print the first 10 cell-wall ids
        [(1, 751), (95, 157), (306, 569), (397, 724), (484, 612), (654, 1023), (160, 222), (538, 848), (716, 854), (194, 220)]
        """
        return self.image.surfels(wall_ids)

    def coordinates(self, wall_ids=None, real=True):
        """Return the real or voxel coordinates of each voxels representing a cell-wall.

        Parameters
        ----------
        wall_ids : tuple of int or list of tuple of int, optional
            If ``None`` (default) returns the dictionary for all cell-wall ids found in the image.
            If a len-2 tuple of int, or a list of them, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell-wall ids.
        real : bool, optional
            If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

        Returns
        -------
        dict
            Cell-wall coordinates dictionary, ``{wid: coordinates(wid)}`` for ``wid`` in `wall_ids`.

        Examples
        --------
        >>> from timagetk.features.cell_walls import Wall
        >>> from timagetk.io.dataset import shared_data
        >>> image = shared_data('flower_labelled', 0)
        >>> wf = Wall(image)
        >>> wall_coords = wf.coordinates()
        >>> print(wall_coords[(1, 751)][:5,:])  # print the first 5 coordinates for cell-wall (1, 715)
        [[65.10400176 12.82048035 16.42624044]
         [64.70336175 13.22112036 16.42624044]
         [64.70336175 13.62176037 16.42624044]
         [64.70336175 14.02240038 16.42624044]
         [64.70336175 14.42304039 16.42624044]]
        """
        return self.image.surfel_coordinates(wall_ids, real)

    def geometric_median(self, wall_ids=None, real=True):
        """Return the dictionary of wall geometric median.

        Parameters
        ----------
        wall_ids : tuple of int or list of tuple of int, optional
            If ``None`` (default) returns the dictionary for all cell-wall ids found in the image.
            If a len-2 tuple of int, or a list of them, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell-wall ids.
        real : bool, optional
            If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

        Returns
        -------
        dict
            Cell-wall geometric median dictionary, ``{wid: geometric_median(wid)}`` for ``wid`` in `wall_ids`.

        Examples
        --------
        >>> from timagetk.features.cell_walls import Wall
        >>> from timagetk.io import imread
        >>> from timagetk.io.dataset import shared_data
        >>> image = shared_data('flower_labelled', 0)
        >>> wf = Wall(image)
        >>> wall_gm = wf.geometric_median()
        """
        wall_ids = self.ids(wall_ids)
        wids_no_ppty = set(wall_ids) - set(self.feature("geometric_median").keys())
        if wids_no_ppty != set():
            log.info(f"Computing {len(wids_no_ppty)} cell-wall geometric median coordinates:")
            self.set_feature("geometric_median", median_coordinate(self.image, wall_ids=wids_no_ppty, real=real),
                             update=True)

        wids_no_ppty = set(wall_ids) - set(self.feature("geometric_median").keys())
        if wids_no_ppty != set():
            log.warning(f"Some cell-wall ({len(wids_no_ppty)}) could not obtain a valid geometric median!")

        wids = set(wall_ids) & set(self.feature("geometric_median").keys())
        return {wid: self.feature("geometric_median")[wid] for wid in wids}

    def inertia_axis(self, wall_ids=None, real=True):
        """Return the dictionary of wall inertia axis.

        Parameters
        ----------
        wall_ids : tuple of int or list of tuple of int, optional
            If ``None`` (default) returns the dictionary for all cell-wall ids found in the image.
            If a len-2 tuple of int, or a list of them, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell-wall ids.
        real : bool, optional
            If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

        Returns
        -------
        dict
            Cell-wall inertia axis dictionary, ``{wid: inertia_axis(wid)}`` for ``wid`` in `wall_ids`.

        Examples
        --------
        >>> from timagetk.features.cell_walls import Wall
        >>> from timagetk.io.dataset import shared_data
        >>> image = shared_data('flower_labelled', 0)
        >>> wf = Wall(image)
        >>> wall_ia = wf.inertia_axis()
        """
        wall_ids = self.ids(wall_ids)
        wids_no_ppty = set(wall_ids) - set(self.feature("inertia_axis").keys())
        if wids_no_ppty != set():
            log.info(f"Computing {len(wids_no_ppty)} cell-wall inertia axis matrices:")
            self.set_feature("inertia_axis", inertia_axis(self.image, wall_ids=wids_no_ppty, real=real), update=True)

        wids_no_ppty = set(wall_ids) - set(self.feature("inertia_axis").keys())
        if wids_no_ppty != set():
            log.warning(f"Some cell-wall ({len(wids_no_ppty)}) could not obtain a valid inertia axis matrix!")

        wids = set(wall_ids) & set(self.feature("inertia_axis").keys())
        return {wid: self.feature("inertia_axis")[wid] for wid in wids}


class Wall2D(Wall):
    """Wall related feature class, restricted to 2D."""

    def __init__(self, image):
        """Wall 2D feature class constructor.

        Parameters
        ----------
        image : timagetk.LabelledImage
            The 2D labelled image, should contain walls to analyse.

        Examples
        --------
        >>> from timagetk.features.cell_walls import Wall2D
        >>> from timagetk.io.dataset import shared_data
        >>> image = shared_data('flower_labelled', 0)
        >>> wf = Wall2D(image.get_slice(20, axis='z'))
        """
        Wall.__init__(self, image)
        self.add_feature("length")

    def length(self, wall_ids=None, real=True, **kwargs):
        """Length of the cell wall."""
        return NotImplementedError


class Wall3D(Wall):
    """Wall related feature class, restricted to 3D."""

    def __init__(self, image):
        """Wall 3D feature class constructor.

        Parameters
        ----------
        image : timagetk.LabelledImage
            The 3D labelled image, should contain walls to analyse.

        Examples
        --------
        >>> from timagetk.features.cell_walls import Wall3D
        >>> from timagetk.io.dataset import shared_data
        >>> image = shared_data('flower_labelled', 0)
        >>> wf = Wall3D(image)
        """
        Wall.__init__(self, image)
        self.add_feature("area")

    def area(self, wall_ids=None, real=True):
        """Return the dictionary of wall areas.

        Parameters
        ----------
        wall_ids : tuple of int or list of tuple of int, optional
            If ``None`` (default) returns the dictionary for all cell-wall ids found in the image.
            If a len-2 tuple of int, or a list of them, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell-wall ids.
        real : bool, optional
            If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

        Returns
        -------
        dict
            Cell-wall area dictionary, ``{wid: area(wid)}`` for ``wid`` in `wall_ids`.

        Examples
        --------
        >>> from timagetk.features.cell_walls import Wall3D
        >>> from timagetk.io.dataset import shared_data
        >>> image = shared_data('flower_labelled', 0)
        >>> wf = Wall3D(image)
        >>> wall_area = wf.area()
        """
        wall_ids = self.ids(wall_ids)  # filter and reorder wall-id tuples
        wids_no_ppty = set(wall_ids) - set(self.feature("area").keys())
        if wids_no_ppty != set():
            vt_area = get_neighbor_areas(self.image, real=real)
            self.set_feature("area", vt_area, update=True)

        wids_no_ppty = set(wall_ids) - set(self.feature("area").keys())
        if wids_no_ppty != set():
            log.warning(f"Some cell-wall ({len(wids_no_ppty)}) could not obtain a valid area value!")

        wids = set(wall_ids) & set(self.feature("area").keys())
        return {wid: self.feature("area")[wid] for wid in wids}
