#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

from copy import deepcopy
from timagetk.bin.logger import get_logger

log = get_logger(__name__)

class Features(object):
    """Class dedicated to store properties.

    Attributes
    ----------
    _feature : dict of str: dict
        Dictionary of computed properties.

    """

    def __init__(self):
        self._feature = {}

    def feature_names(self) -> list:
        """Get the list of existing features.

        Returns
        -------
        list of str
            The list of existing feature names.
        """
        return list(self._feature.keys())

    def add_feature(self, feature_name: str) -> None:
        """Adds an empty feature to the feature dict.

        Parameters
        ----------
        feature_name : str
            Name for the feature to add.
        """
        if feature_name not in self._feature:
            self._feature[feature_name] = {}
        else:
            log.warning(f"feature {feature_name} already exists!")

    def set_feature(self, feature_name: str, feature_values: dict, update: bool = False) -> None:
        """Update the values of a feature of the feature dict.

        Parameters
        ----------
        feature_name : str
            Name of the feature to update.
        feature_values : dict of int : any
            feature values to update the dictionary with.
        update : bool, optional
            Whether to update existing feature dictionary or simply overwrite it.
        """
        assert isinstance(feature_values, dict)

        if feature_name not in self._feature:
            self._feature[feature_name] = {}

        if update:
            self._feature[feature_name].update(feature_values)
        else:
            self._feature[feature_name] = feature_values

    def remove_feature(self, feature_name: str) -> None:
        """Remove an existing feature from the feature dict.

        Parameters
        ----------
        feature_name :  str
            Name of the feature to remove.
        """
        try:
            self._feature.pop(feature_name)
        except KeyError:
            log.error(f"Could not find a feature named {feature_name}!")

    def feature(self, feature_name: str, copy: bool = False) -> dict:
        """Get an existing feature from the feature dict.

        Parameters
        ----------
        feature_name : str
            Name of the feature to get.
        copy : bool, optional
            Whether to return a copy, or the original dictionary.

        Returns
        -------
        dict
            The dictionary corresponding to the feature.
        """
        if feature_name not in self._feature:
            raise KeyError(f"feature {feature_name} does not exist!")
        else:
            if copy:
                return deepcopy(self._feature[feature_name])
            else:
                return self._feature[feature_name]
