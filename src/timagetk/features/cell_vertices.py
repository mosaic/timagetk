#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Module for cell-vertex feature extraction methods."""

import numpy as np
from tqdm.autonotebook import tqdm

from timagetk.bin.logger import get_logger
from timagetk.features.cell_walls import geometric_median
from timagetk.features.features import Features

log = get_logger(__name__)


def pointel_unique_coordinate(pointel_coords):
    """Return the coordinate of a pointel for its set of topological coordinates.

    Parameters
    ----------
    pointel_coords : numpy.ndarray
        The ``N, d`` set of topological coordinates of an array, with ``N`` the number of coordinates of the pointel
        and ``d`` its dimensionality.

    Returns
    -------
    numpy.ndarray
        The coordinate of the pointel as an array of shape ``(d,)``

    Notes
    -----
    Returns the mean if ``N==2``.
    Returns the geometric median if ``N>=3``.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.features.cell_vertices import pointel_unique_coordinate
    >>> pointel_unique_coordinate(np.array([1., 2., 3.]))  # already of shape ``(d,)``
    array([1., 2., 3.])
    >>> pointel_unique_coordinate(np.array([[1., 2., 3.]]))  # of shape ``(d, 1)``
    array([1., 2., 3.])
    >>> pointel_unique_coordinate(np.array([[1., 2., 2.5], [1., 2., 3.5]]))  # of shape ``(d, 2)``
    array([1., 2., 3.])
    >>> pointel_unique_coordinate(np.array([[1., 2., 2.], [1., 2., 3.], [1., 2., 4.]]))  # of shape ``(d, 3)``
    array([1., 2., 3.])

    """
    try:
        N, d = pointel_coords.shape
    except ValueError:
        return pointel_coords

    if N == 1:
        uniq_coord = pointel_coords[0]
    elif N == 2:
        uniq_coord = np.mean(pointel_coords, axis=0)
    else:
        uniq_coord = geometric_median(pointel_coords)

    return uniq_coord


class Vertex3D(Features):

    def __init__(self, image):
        super().__init__()
        self.image = image
        self.add_feature('coordinates')

    def ids(self, vertex_ids=None):
        """Return the list of cell-vertex ids found in the image.

        Parameters
        ----------
        vertex_ids : tuple of int or list of tuple of int, optional
            If ``None`` (default) returns all cell-vertex ids found in the image.
            If a len-4 tuple of int, or a list of them, make sure it is or they are in the image.

        Returns
        -------
        list
            List of cell-vertex ids found in the image.
        """
        return self.image.pointels(vertex_ids)

    def coordinates(self, vertex_ids=None, real=True):
        """Return the real or voxel coordinates of each voxels representing a label.

        Parameters
        ----------
        vertex_ids : int or list of int or str, optional
            If ``None`` (default) returns the dictionary for all cell-vertex ids found in the image.
            If a len-4 tuple of int, or a list of them, filter returned dictionary keys with those ids, but make sure
            they are defined in the list of cell-vertex ids.
        real : bool, optional
            If ``True`` (default), returns the dictionary values in real world units, else in voxel units.

        Returns
        -------
        dict
            Cell-vertex coordinates dictionary, ``{vid: coordinates(vid)}`` for ``vid`` in `vertex_ids`.
        """
        vertex_ids = self.ids(vertex_ids)
        vids_no_ppty = set(vertex_ids) - set(self.feature('coordinates').keys())
        if vids_no_ppty != set():
            log.info(f"Computing {len(vids_no_ppty)} cell-vertex coordinates:")
            p_coords = self.image.pointel_coordinates(vertex_ids, real)
            v_coords = {pid: pointel_unique_coordinate(coords) for pid, coords in tqdm(p_coords.items(), unit="vertex")}
            self.set_feature('coordinates', v_coords, update=True)
        return {vid: self.feature('coordinates')[vid] for vid in vertex_ids}
