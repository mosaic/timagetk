from operator import itemgetter

import numpy as np
from sklearn.neighbors import LocalOutlierFactor


def find_missing_values(array, idx=None, n=0, axis=1):
    """Find the index of a 1D or 2D array with missing values.

    Parameters
    ----------
    array : numpy.ndarray
        The array to search for missing index.
    idx : Iterable, optional
        If not ``None`` (default), a list of index to filter by missing values.
    n : int, optional
        The number of allowed missing values.
        Defaults to ``0``.
    axis : {0, 1}, optional
        Axis used with 2D array to search for missing values.
        ``0`` mean search by columns, ``1`` is search by lines.
        Defaults to ``1``.

    Returns
    -------
    list
        Elements with a maximum of `n` missing value.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.features.utils import find_missing_values
    >>> array = np.array([1, 2, 3, np.nan])
    >>> find_missing_values(array)
    [3]
    >>> array = np.array([1, 2, 3, np.nan])
    >>> find_missing_values(array,["A", "B", "C", "D"])
    ['D']
    >>> array = np.array([[1, 2, 3, np.nan], [0.2, np.nan, 0.5, 0.4]]).T
    >>> find_missing_values(array,["A", "B", "C", "D"])
    ['B', 'D']

    """
    if array.ndim == 1:
        nan_idx = np.where(np.isnan(array))[0].tolist()
    elif array.ndim == 2:
        # Search for columns (axis=0) or lines (axis=1) with missing value (np.nan):
        nan_idx = np.where(np.isnan(array).sum(axis=axis) > n)[0].tolist()
    else:
        raise ValueError("Only 1D or 2D arrays are allowed here!")

    if idx is None:
        return nan_idx
    else:
        if len(nan_idx) == 1:
            idx = list(idx)
            # Get the index with a missing value:
            nan_idx = [idx[nan_idx[0]]]
        elif len(nan_idx) != 0:
            idx = list(idx)
            # Get the indexes with a missing value:
            nan_idx = list(itemgetter(*nan_idx)(idx))
        else:
            nan_idx = []
        return nan_idx


def lof_array(array, ids, **kwargs):
    """Detect outliers on a numpy array using the Local Outlier Factor method.

    Parameters
    ----------
    array : numpy.ndarray
        The individuals/observations (row/column) array to search for outliers.
    ids : Iterable
        A list of individual ids matching the order tof the `array`.

    Returns
    -------
    set
        The set of individual ids detected as outliers ro with missing data (``np.nan`` value), if any.
    dict
        The id indexed dictionary of LOF scores, excluding any id with missing data (``np.nan`` value).

    See Also
    --------
    sklearn.neighbors.LocalOutlierFactor

    Notes
    -----
    Any id with a missing value in the array will be returned as an outlier.
    Keyword argument are passed to `LocalOutlierFactor`.

    """
    ids = list(ids)
    # As the outlier detection method cannot work with nan values, we need to remove them first!
    nan_ids = find_missing_values(array, ids, n=0, axis=1)

    # If some nan values has been found, we need to remove them
    if len(nan_ids) != 0:
        # Get the set index of identifiers with a missing value:
        nan_idx = {ids.index(v) for v in nan_ids}
        # Defines the index of cell ids without any missing value:
        non_nan_idx = list(set(range(len(ids))) - nan_idx)
        if len(non_nan_idx) != 0:
            # Get the list of cell ids without any missing value:
            ids = list(itemgetter(*non_nan_idx)(ids))
            # Get the array of cell feature values without any missing value:
            array = array[non_nan_idx, :]
        else:
            raise ValueError("Every individual present at least one missing value!")

    clf = LocalOutlierFactor(**kwargs)
    pred = clf.fit_predict(array)
    outliers = pred == -1
    outlier_ids = set(itemgetter(*np.where(outliers)[0].tolist())(ids))
    lof_score = clf.negative_outlier_factor_
    return outlier_ids | set(nan_ids), {i: lof_score[n] for n, i in enumerate(ids)}


def lof_df(df, names=None, id_column=None, **kwargs):
    """Detect outliers on a dataframe using the Local Outlier Factor method.

    Parameters
    ----------
    df : pandas.DataFrame
        The dataframe containing the individual by lines and observation values by columns.
    names : list
        A list of observation names to use, else use them all.

    Returns
    -------
    set
        The set of individual ids detected as outliers, if any.
    dict
        The ids indexed dictionary of LOF scores.

    See Also
    --------
    sklearn.neighbors.LocalOutlierFactor

    Notes
    -----
    Keyword argument are passed to `LocalOutlierFactor`.

    Examples
    --------
    >>> import numpy as np
    >>> import pandas as pd
    >>> from timagetk.io.util import shared_folder
    >>> from timagetk.features.utils import lof_df
    >>> c_df = pd.read_csv(shared_folder('p58')+'/p58-t0_SEG_down_interp_2x_cell.csv', header=0, index_col='cell_id')
    >>> print(c_df.columns.tolist())
    ['neighbors', 'epidermis_area', 'volume', 'stack_margin', 'inertia_axis', 'number_of_neighbors', 'shape_anisotropy', 'area', 'epidermis', 'epidermis_median_x', 'epidermis_median_y', 'epidermis_median_z', 'barycenter_x', 'barycenter_y', 'barycenter_z', 'FloralMeristem']
    >>> outlier_ids, lof_score = lof_df(c_df, ['volume', 'number_of_neighbors'], n_neighbors=40)
    >>> len(outlier_ids)
    29
    >>> bool_outliers = [i in outlier_ids for i in c_df.index.to_list()]
    >>> from matplotlib import pyplot as plt
    >>> plt.title("Local Outlier Factor (LOF)")
    >>> plt.scatter(x=c_df['volume'].tolist(), y=c_df['number_of_neighbors'].tolist(), c=bool_outliers, s=3.)
    >>> plt.xlabel("volume")
    >>> plt.ylabel("number_of_neighbors")
    >>> plt.show()

    """
    # Get the names of the observations to use for outliers detection:
    if names is None:
        names = df.columns
    else:
        names = set(names) & set(df.columns)

    # Get the (ordered) list of individual ids
    if id_column is None:
        ids = df.index.to_list()
    else:
        ids = df[id_column].tolist()

    # Create the array with variable values. Shape is `self.ids` x `names` (lines x columns)
    array = np.array([df[name].tolist() for name in names]).T

    return lof_array(array, ids, **kwargs)


def flatten(l):
    """Flatten iterables to a non-nested list.

    Examples
    --------
    >>> from timagetk.features.utils import flatten
    >>> list(flatten([1,2,3,4]))
    [1, 2, 3, 4]
    >>> list(flatten([[1,2],[3,4]]))
    [1, 2, 3, 4]
    >>> list(flatten([[1,[2,3]],4]))
    [1, 2, 3, 4]

    """
    from collections.abc import Iterable
    for el in l:
        if isinstance(el, Iterable) and not isinstance(el, str):
            for sub in flatten(el):
                yield sub
        else:
            yield el
