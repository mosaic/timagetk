#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""This module is a wrapper for VT feature extraction algorithms.

It only works for 3D images!
The `get_*` methods in this module are not efficient as they will each create a
`vt.cellproperties.CellProperties` instance that will compute several or all the cell properties.

We try to minimize the computational cost by providing the right features to compute.

Use this only if you want to access a single property.
If yu intend to compute several, we suggest to look at:
  - the `vt.cellproperties.CellProperties` class
  - the `timagetk.features.cells.Cell` class
  - the `timagetk.tasks.tissue_graph.tissue_graph_from_image` function.

"""

import numpy as np
from vt.cellproperties import CellProperties

from timagetk.graphs.nx_graph import udict
from timagetk.util import stuple


def _ppty_get_label(ppty, labels=None):
    # Get labels from the property object
    vt_labels = ppty.labels()

    if labels is None:
        # If no specific labels provided, use all labels from property
        labels = vt_labels
    else:
        if isinstance(labels, int):
            # If labels is a single integer, convert it to a list
            labels = [labels]
        n_labels = len(labels)  # Number of labels provided by the user
        # Find intersection of provided labels and existing labels
        labels = list(set(vt_labels) & set(labels))
        if len(labels) == 0:
            # If no matching labels are found, print a warning message
            if len(vt_labels) != 0:
                print(f"None of the provided {n_labels} labels were found in the image!")

    return labels  # Return the filtered list of labels


def get_labels(image, labels=None):
    """Get the list of labels from the labelled image.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Labelled image to use for feature computation.
    labels : int or list of int, optional
        If ``None`` (default), returns all labels found in `image`.
        Else, should be a label or a list of labels to filter with those found within the image.

    Returns
    -------
    list
        List of labels.

    Examples
    --------
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk import LabelledImage
    >>> image = shared_data('flower_labelled', 0)
    >>> from timagetk.third_party.vt_features import get_labels
    >>> labels = get_labels(image)
    >>> print(len(labels))
    1040
    """
    ppty = CellProperties(image.to_vt_image(), ppty_list=['volume'], filter_neighbors=False)
    return _ppty_get_label(ppty, labels)


def _ppty_get_neighbors(ppty, labels=None):
    if labels is None:
        # If no specific labels provided, use all labels from property
        labels = ppty.labels()
    return ppty.neighbor(labels)


def get_neighbors(image, labels=None):
    """Get the labels' neighbors from the labelled image.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Labelled image to use for property computation.
    labels : int or list of int, optional
        If ``None`` (default), returns feature values of all labels found in `image`.
        Else, should be a label or a list of labels to filter with those found within the image.

    Returns
    -------
    dict
        Label indexed dictionary of neighbors.

    Examples
    --------
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk import LabelledImage
    >>> image = shared_data('flower_labelled', 0)
    >>> from timagetk.third_party.vt_features import get_neighbors
    >>> neighborhood = get_neighbors(image)
    >>> print(neighborhood[1001])
    [557, 39, 1, 888, 413, 949, 446, 399, 844, 836, 213, 467, 64, 161, 323]

    """
    ppty = CellProperties(image.to_vt_image(), ppty_list=["contact-surface"], filter_neighbors=True)
    return _ppty_get_neighbors(ppty, labels)


def _ppty_get_area(ppty, labels, real, vxs):
    if labels is None:
        # If no specific labels provided, use all labels from property
        labels = ppty.labels()
    if real:
        coef = vxs ** 2
        return {label: ppty.surface(label) * coef for label in labels}
    else:
        return ppty.surface(labels)


def get_area(image, labels=None, real=True):
    """Get the labels' area, from the labelled image.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Labelled image to use for feature computation.
    labels : int or list of int, optional
        If ``None`` (default), returns property for all labels found in `image`.
        Else, should be a label or a list of labels to filter with those found within the image.
    real : bool, optional
        If ``True`` (default), returns the values in real world units.
        Else returns the values in voxel units.

    Returns
    -------
    dict
        Label indexed dictionary of areas.

    Notes
    -----
    If the `image` is not isometric, it will be resampled to the highest resolution possible prior to area computation.

    References
    ----------
    .. [#] Lindblad, J. (2005). Surface Area Estimation of Digitized 3D Objects using Weighted Local Configurations. Image and Vision Computing, 23(2), 111–122. https://doi.org/doi:10.1016/j.imavis.2004.06.012

    Examples
    --------
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk import LabelledImage
    >>> image = shared_data('flower_labelled', 0)
    >>> from timagetk.third_party.vt_features import get_area
    >>> areas = get_area(image, real=False)
    >>> print(areas[1001])
    1652.550537109375
    >>> areas = get_area(image)
    >>> print(areas[1001])
    265.254883042759

    >>> from timagetk.third_party.vt_features import get_neighbor_areas
    >>> neighbor_areas = get_neighbor_areas(image)
    >>> print(sum(neighbor_areas[1001].values()))
    265.25487967507445

    >>> import numpy as np
    >>> from math import pi
    >>> from skimage.morphology import ball
    >>> from timagetk import LabelledImage
    >>> from timagetk.third_party.vt_features import get_area
    >>> radius = 100
    >>> image = LabelledImage(ball(radius)+1, voxelsize=(1., 0.5, 0.5), dtype='uint16', not_a_label=0)
    >>> area = get_area(image, real=False)
    >>> print(area[2])  # area of label 2 in voxel units
    125663.53125
    >>> print(4*pi*radius**2)  # area in voxel units
    125663.70614359173

    """
    from timagetk.algorithms.resample import isometric_resampling

    # As the VT method assume image isometry, we check that first!
    if not image.is_isometric():
        image = isometric_resampling(image, value='min', interpolation='cellbased', cell_based_sigma=2)

    vxs = image.get_voxelsize('x')  # any axis should do as it is (now) isometric!
    ppty = CellProperties(image.to_vt_image(), ppty_list=["contact-surface"], filter_neighbors=True)
    return _ppty_get_area(ppty, labels, real, vxs)


def _ppty_get_volume(ppty, labels, real, vxs):
    if labels is None:
        # If no specific labels provided, use all labels from property
        labels = ppty.labels()
    coef = np.prod(vxs)
    if real:
        return {label: ppty.volume(label) * coef for label in labels}
    else:
        return ppty.volume(labels)


def get_volume(image, labels=None, real=True):
    """Get the labels' volumes, in voxels unit, from the labelled image.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Labelled image to use for feature computation.
    labels : int or list of int, optional
        If ``None`` (default), returns feature values of all labels found in `image`.
        Else, should be a label or a list of labels to filter with those found within the image.
    real : bool, optional
        If ``True`` (default), returns the values in real world units.
        Else returns the values in voxel units.

    Returns
    -------
    dict
        Label indexed dictionary of volumes, in voxels unit.

    Examples
    --------
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk import LabelledImage
    >>> image = shared_data('flower_labelled', 0)
    >>> from timagetk.third_party.vt_features import get_volume
    >>> volumes = get_volume(image, real=False)  # Get it in voxel units
    >>> print(volumes[1001])
    4899
    >>> volumes = get_volume(image)  # Get it in real units
    >>> print(volumes[1001])
    315.0434075980686

    """
    ppty = CellProperties(image.to_vt_image(), ppty_list=["volume"], filter_neighbors=False)
    vxs = image.get_voxelsize()
    return _ppty_get_volume(ppty, labels, real, vxs)


def _ppty_get_barycenter(ppty, labels, real, vxs):
    if labels is None:
        # If no specific labels provided, use all labels from property
        labels = ppty.labels()
    if real:
        return {label: ppty.barycenter(label) * vxs for label in labels}  # XY(Z) sorted
    else:
        return ppty.barycenter(labels)  # XY(Z) sorted


def get_barycenter(image, labels=None, real=True):
    """Get the labels' barycenters coordinate, in voxels unit, from the labelled image.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Labelled image to use for feature computation.
    labels : int or list of int, optional
        If ``None`` (default), returns feature values of all labels found in `image`.
        Else, should be a label or a list of labels to filter with those found within the image.
    real : bool, optional
        If ``True`` (default), returns the values in real world units.
        Else returns the values in voxel units.

    Returns
    -------
    dict
        Label indexed dictionary of XY(Z) ordered barycenters.

    Examples
    --------
    >>> from timagetk.array_util import dummy_labelled_image_2D
    >>> from timagetk.third_party.vt_features import get_barycenter
    >>> im = dummy_labelled_image_2D((0.2, 0.2))  # ZYX sorted voxel-sizes
    >>> bary = get_barycenter(im, real=True)  # Get it in voxel units

    >>> from timagetk.array_util import dummy_labelled_image_3D
    >>> from timagetk.third_party.vt_features import get_barycenter
    >>> im = dummy_labelled_image_3D((0.5, 0.2, 0.2))  # ZYX sorted voxel-sizes
    >>> bary = get_barycenter(im, real=True)  # Get it in voxel units

    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk import LabelledImage
    >>> from timagetk.third_party.vt_features import get_barycenter
    >>> image = shared_data('flower_labelled', 0)
    >>> bary = get_barycenter(image, real=False)  # Get it in voxel units
    >>> print(bary[1001])
    [ 79.60522556 161.90038783  96.72708716]
    >>> bary = get_barycenter(image)  # Get it in real units
    >>> print(bary[1001])
    [31.89303843 64.86377314 38.75274125]

    """
    ppty = CellProperties(image.to_vt_image(), ppty_list=["volume"], filter_neighbors=False)
    vxs = image.get_voxelsize()[::-1]  # revert voxel-size array from ZYX to XYZ
    return _ppty_get_barycenter(ppty, labels, real, vxs)


def _ppty_get_covariance(ppty, labels, real, vxs):
    if labels is None:
        # If no specific labels provided, use all labels from property
        labels = ppty.labels()
    if real:
        return {label: vxs * ppty.covariance(label) * vxs.T for label in labels}
    else:
        return ppty.covariance(labels)


def get_covariance(image, labels=None, real=True):
    """Get the labels' covariance matrix, from the labelled image.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Labelled image to use for feature computation.
    labels : int or list of int, optional
        If ``None`` (default), returns feature values of all labels found in `image`.
        Else, should be a label or a list of labels to filter with those found within the image.
    real : bool, optional
        If ``True`` (default), returns the values in real world units.
        Else returns the values in voxel units.

    Returns
    -------
    dict
        Label indexed dictionary of XY(Z) ordered covariance matrices.

    Examples
    --------
    >>> from timagetk.third_party.vt_features import get_covariance
    >>> from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image
    >>> seg_img = example_layered_sphere_labelled_image()
    >>> cov = get_covariance(seg_img, real=False)
    >>> print(cov[2])
    [[31.33552391  0.          0.        ]
     [ 0.         31.33552391  0.        ]
     [ 0.          0.         31.33552391]]
    >>> import numpy as np
    >>> from timagetk.algorithms.resample import resample
    >>> seg_img = resample(seg_img, voxelsize=seg_img.get_voxelsize()*np.array([2., 1., 1.]))  # simulate lower resolution in Z
    >>> cov = get_covariance(seg_img, real=False)
    >>> print(cov[2])
    [[ 3.06441101e+01  2.81051645e-02  7.76019855e-02]
     [ 2.81051645e-02  3.07476822e+01 -5.97015469e-02]
     [ 7.76019855e-02 -5.97015469e-02  3.34125876e+01]]
    """
    from timagetk.algorithms.resample import isometric_resampling

    # As the VT method assume image isometry, we check that first!
    if not image.is_isometric():
        image = isometric_resampling(image, value='min')

    ppty = CellProperties(image.to_vt_image(), ppty_list=["volume"], filter_neighbors=False)
    vxs = np.array([image.get_voxelsize('x')] * 3)  # any axis should do as it is (now) isometric!
    return _ppty_get_covariance(ppty, labels, real, vxs)


def _ppty_get_vt_neighbor_areas(ppty, labels, real, vxs):
    if labels is None:
        # If no specific labels provided, use all labels from property
        labels = ppty.labels()

    if real:
        coef = vxs ** 2
        return {label: {nei_idx: csurf * coef for nei_idx, csurf in ppty.contactsurface(label).items()}
               for label in labels}
    else:
        return ppty.contactsurface(labels)


def get_vt_neighbor_areas(image, labels=None, real=True):
    """Get the areas between neighbors, from a labelled image.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Labelled image to use for property computation.
    labels : int or list of int, optional
        If ``None`` (default), returns feature values of all labels found in `image`.
        Else, should be a label or a list of labels to filter with those found within the image.
    real : bool, optional
        If ``True`` (default), returns the values in real world units.
        Else returns the values in voxel units.

    Returns
    -------
    dict
        Dictionary of wall areas between neighbors, oredered as ``{label: {label_neigbors: wall_area}}``.

    Notes
    -----
    If the `image` is not isometric, it will be resampled to the highest resolution possible prior to area computation.

    References
    ----------
    .. [#] Lindblad, J. (2005). Surface Area Estimation of Digitized 3D Objects using Weighted Local Configurations. Image and Vision Computing, 23(2), 111–122. https://doi.org/doi:10.1016/j.imavis.2004.06.012

    Examples
    --------
    >>> from timagetk.third_party.vt_features import get_vt_neighbor_areas
    >>> from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image
    >>> seg_img = example_layered_sphere_labelled_image()
    >>> neighborhood_area = get_vt_neighbor_areas(seg_img, real=False)
    >>> print(neighborhood_area[2][4])
    164.29844665527344
    >>> print(neighborhood_area[4][2])
    170.21531677246094
    >>> neighborhood_area = get_vt_neighbor_areas(seg_img, real=True)
    >>> print(neighborhood_area[2][4])
    59.14744079589843
    >>> print(neighborhood_area[4][2])
    61.27751403808593
    """
    from timagetk.algorithms.resample import isometric_resampling

    # As the VT method assume image isometry, we check that first!
    if not image.is_isometric():
        image = isometric_resampling(image, value='min', interpolation='cellbased', cell_based_sigma=1)

    # Now we can initialize the CellProperties instance:
    ppty = CellProperties(image.to_vt_image(), ppty_list=["contact-surface"], filter_neighbors=True)
    vxs = image.get_voxelsize('x')  # any axis should do as it is (now) isometric!
    return _ppty_get_vt_neighbor_areas(ppty, labels, real, vxs)


def _ppty_get_neighbor_areas(ppty, labels, real, vxs):
    vt_area = _ppty_get_vt_neighbor_areas(ppty, labels, real, vxs)
    if labels is not None:
        neig_pairs = {stuple((k, nei)) for k, v in vt_area.items() for nei in v.keys() if k in labels or nei in labels}
    else:
        neig_pairs = {stuple((k, nei)) for k, v in vt_area.items() for nei in v.keys()}

    nei_areas = udict(
        {wid: (vt_area[wid[0]][wid[1]] + vt_area[wid[1]][wid[0]]) / 2 if wid[0] != 0 else vt_area[wid[1]][0] for wid in
         neig_pairs})
    return nei_areas


def get_neighbor_areas(image, labels=None, real=True):
    """Get the areas between neighbors, from a labelled image.

    Parameters
    ----------
    image : timagetk.LabelledImage
        Labelled image to use for property computation.
    labels : int or list of int, optional
        If ``None`` (default), returns feature values of all labels found in `image`.
        Else, should be a label or a list of labels to filter with those found within the image.
    real : bool, optional
        If ``True`` (default), returns the values in real world units.
        Else returns the values in voxel units.

    Returns
    -------
    udict
        Dictionary of wall areas between neighbors, indexed as ``{(label1, label2): wall_area}}``.

    Notes
    -----
    If the `image` is not isometric, it will be resampled to the highest resolution possible prior to area computation.

    See Also
    --------
    timagetk.third_party.vt_features.get_vt_neighbor_areas

    References
    ----------
    .. [#] Lindblad, J. (2005). Surface Area Estimation of Digitized 3D Objects using Weighted Local Configurations. Image and Vision Computing, 23(2), 111–122. https://doi.org/doi:10.1016/j.imavis.2004.06.012

    Examples
    --------
    >>> from timagetk.third_party.vt_features import get_neighbor_areas
    >>> from timagetk.synthetic_data.labelled_image import example_layered_sphere_labelled_image
    >>> seg_img = example_layered_sphere_labelled_image()
    >>> neighborhood_area = get_neighbor_areas(seg_img, real=False)
    >>> print(neighborhood_area[(2, 4)])
    167.2568817138672
    >>> print(neighborhood_area[(4, 2)])
    167.2568817138672
    >>> neighborhood_area = get_neighbor_areas(seg_img, real=True)
    >>> print(neighborhood_area[(2, 4)])
    60.212477416992186
    >>> print(neighborhood_area[(4, 2)])
    60.212477416992186
    """
    from timagetk.algorithms.resample import isometric_resampling

    # As the VT method assume image isometry, we check that first!
    if not image.is_isometric():
        image = isometric_resampling(image, value='min', interpolation='cellbased', cell_based_sigma=1)

    # Now we can initialize the CellProperties instance:
    ppty = CellProperties(image.to_vt_image(), ppty_list=["contact-surface"], filter_neighbors=True)
    vxs = image.get_voxelsize('x')  # any axis should do as it is (now) isometric!

    return _ppty_get_neighbor_areas(ppty, labels, real, vxs)
