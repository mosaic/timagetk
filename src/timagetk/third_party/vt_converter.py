#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""Functions to convert objects between different data structures."""

import logging

import vt
from vt import vtImage
from vt.image import Image

from timagetk.components.spatial_image import SpatialImage
from timagetk.util import get_attributes
from timagetk.util import get_class_name

log = logging.getLogger(__name__)


def attr_from_spatialimage(input_array, **kwargs):
    """Override keyword arguments with known attributes of the SpatialImage instance.

    We replace the keyword argument value by the attribute value, except if it is ``None``.
    The list of compared attributes is:

      * axes_order
      * origin
      * unit
      * voxelsize
      * metadata

    Parameters
    ----------
    input_array : timagetk.SpatialImage
        SpatialImage from which the attribute should be compared against keyword arguments

    Other Parameters
    ----------------
    origin : list, optional
        (Z)YX coordinates of the origin in the image, default is ``[0, 0]`` if 2D or ``[0, 0, 0]`` if 3D.
    voxelsize : list, optional.
        (Z)YX image voxel size, default is ``[1.0, 1.0]`` if 2D or ``[1.0, 1.0, 1.0]`` if 3D.
    axes_order : dict
        Dictionary of axes orders, *e.g.* {'x': 2, 'y': 1, 'z': 0} by default for 3D image.
    unit : float
        The size unit, in International System of Units (meters), associated to the image.
    metadata : dict, optional
        Dictionary of image metadata, default is an empty dict.

    Returns
    -------
    dict
        Updated keyword arguments dictionary

    Example
    -------
    >>> import numpy as np
    >>> from timagetk.io import imread
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk import SpatialImage
    >>> from timagetk.third_party.vt_converter import attr_from_spatialimage
    >>> image_path = shared_dataset("p58", "intensity")[0]
    >>> spim = imread(image_path)
    >>> attr_from_spatialimage(spim)

    """
    log.debug("New SpatialImage from SpatialImage...")
    attr_list = ["axes_order", "origin", "unit", "voxelsize", "metadata"]
    # Get optional keyword arguments or set to None:
    kwds = {kwd: kwargs.get(kwd, None) for kwd in attr_list}

    attr_dict = get_attributes(input_array, attr_list)
    class_name = get_class_name(input_array)
    msg = "Overriding optional keyword arguments '{}' ({}) with defined attribute ({}) in given '{}'!"

    # -- Compare keyword argument values with attribute value:
    for attr, attr_val in attr_dict.items():
        kwd_val = kwds.get(attr, None)
        if attr_val is not None:
            log.debug(msg.format(attr, kwd_val, attr_val, class_name))
            kwargs[attr] = attr_dict[attr]
        else:
            log.debug("Got attribute '{}' = {}".format(attr, attr_val))
            log.debug("Got kwarg '{}' = {}".format(attr, kwd_val))

    return kwargs


def _new_from_vtimage(vt_img, copy):
    """Extract objects to create an image from a ``vt.vtImage`` or a ``vtImage`` instance.

    Parameters
    ----------
    vt_img : vtImage or vt.vtImage
        The ``vt.vtImage`` or  `vtImage`` to convert.

    Returns
    -------
    numpy.ndarray
        The converted array corresponding to the image.
    list
        The voxel-size of the image, should be a list of floats of same length than array dimensionality.
    list
        The origin of the image, should be a list of integers of same length than array dimensionality.
    dict
        The dictionary of metadata associated to the image.

    """
    assert isinstance(vt_img, (vtImage, Image))

    # - Get the `numpy.ndarray` data from `vt` instance
    if copy:
        array = vt_img.copy_to_array()
    else:
        array = vt_img.to_array()

    # - Get voxelsize  data from `vt` instance
    voxelsize = vt_img.spacing()
    # Fix voxelsize if a vtImage:
    if isinstance(vt_img, vtImage) and not isinstance(vt_img, Image):
        # ... reverse fix to voxelsize: XYZ -> ZYX
        voxelsize = voxelsize[::-1]
        if array.ndim == 2 and len(voxelsize) == 3:
            # ... 2D/3D fix to voxelsize: XYZ -> ZYX -> (Z)YX
            voxelsize = voxelsize[1:]

    return array, voxelsize, None, {}


def vt_to_spatial_image(vt_img, copy=True):
    """Convert an image object from `vt` library in a ``timagetk.SpatialImage``.

    Parameters
    ----------
    vt_img : vt.vtImage or vt.image.Image
        The image object from `vt` library to convert as ``timagetk.SpatialImage``.
    copy : bool, optional
        Copy the array before converting to ``timagetk.SpatialImage``.
        Else the `vt_img` is destroyed.
        Default is ``True``.

    Returns
    -------
    timagetk.SpatialImage
        The converted image.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.array_util import random_array
    >>> from timagetk import SpatialImage
    >>> from timagetk.third_party.vt_converter import vt_to_spatial_image
    >>> from timagetk.io.dataset import shared_data
    >>> from vt.image import Image
    >>> from vt import vtImage

    >>> # EXAMPLE - Image from a created 3D numpy array:
    >>> arr = random_array((3, 4, 5), dtype='uint8')
    >>> vt_img = Image(arr, [0.31, 0.32, 0.33])
    >>> print(vt_img.spacing())
    >>> print(vt_img.shape())
    >>> sp_img = vt_to_spatial_image(vt_img)
    >>> print(sp_img.voxelsize)
    >>> print(sp_img.shape)
    >>> np.testing.assert_array_equal(vt_img.copy_to_array(), sp_img)

    >>> # EXAMPLE - Image from a created 2D numpy array:
    >>> arr = random_array((4, 5), dtype='uint8')
    >>> print(arr.shape)
    >>> vt_img = Image(arr, [0.32, 0.33, 1.])
    >>> print(vt_img.spacing())
    >>> print(vt_img.shape())
    >>> sp_img = vt_to_spatial_image(vt_img)
    >>> print(sp_img.voxelsize)
    >>> print(sp_img.shape)
    >>> np.testing.assert_array_equal(vt_img.copy_to_array(), sp_img)

    >>> # EXAMPLE - vtImage from a created 3D numpy array:
    >>> arr = random_array((3, 4, 5), dtype='uint8')
    >>> vt_img = vtImage(arr, [0.31, 0.32, 0.33])
    >>> print(vt_img.spacing()[::-1])  # reverse voxelsize: XYZ -> ZYX
    >>> print(vt_img.shape()[::-1])  # reverse shape: XYZ -> ZYX
    >>> sp_img = vt_to_spatial_image(vt_img)
    >>> print(sp_img.voxelsize)
    >>> print(sp_img.shape)
    >>> np.testing.assert_array_equal(vt_img.copy_to_array(), sp_img)

    >>> # EXAMPLE - vtImage from a created 2D numpy array:
    >>> arr = random_array((4, 5), dtype='uint8')
    >>> print(arr.shape)
    >>> vt_img = vtImage(arr, [0.32, 0.33, 1.])
    >>> print(vt_img.spacing()[:-1][::-1])  # reverse to 2D voxelsize: XYZ -> YX
    >>> print(vt_img.shape()[:-1][::-1])  # reverse to 2D shape: XYZ -> YX
    >>> sp_img = vt_to_spatial_image(vt_img)
    >>> print(sp_img.voxelsize)
    >>> print(sp_img.shape)
    >>> np.testing.assert_array_equal(vt_img.copy_to_array(), sp_img)

    """
    if isinstance(vt_img, (vt.vtImage, Image)):
        arr, vxs, ori, md = _new_from_vtimage(vt_img, copy)
    else:
        msg = f"Unknown `vt` type for conversion: {type(vt_img)}"
        raise TypeError(msg)

    from timagetk.io.image import default_image_attributes
    return SpatialImage(arr, **default_image_attributes(arr, voxelsize=vxs, origin=ori, metadata=md))
