#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
Utils to convert between ``timagetk.SpatialImage`` and ``vt.image.Image`` or ``vt.vtImage``.
"""

from vt import vtImage
from vt.image import Image


def check_vt_image_not_null(vt_vtimg, vt_method):
    """Checks if the VT method returned image is NULL.

    This function verifies whether the image returned by a VT method is None.
    If the image is None, it raises a ValueError with a descriptive message.

    Parameters
    ----------
    vt_vtimg : Any
        The image object returned by the VT method. It can be of any type.
    vt_method : str
        The name of the VT method that returned the image.

    Raises
    ------
    ValueError
        If the image is None.
    """
    if vt_vtimg is None:
        null_msg = f"VT method '{vt_method}' returned NULL image!"
        raise ValueError(null_msg)


def spatial_to_vt_image(sp_img):
    """Convert a ``SpatialImage`` in a ``vtImage.image``.

    Parameters
    ----------
    sp_img : timagetk.SpatialImage
        The ``SpatialImage`` object to convert as ``Image``.

    Returns
    -------
    vt.image.Image
        The converted `vt` image.

    Examples
    --------
    >>> from vt.image import Image
    >>> from timagetk.third_party.vt_image import spatial_to_vt_image
    >>> from timagetk.array_util import random_spatial_image
    >>> # Initialize a random (uint8) 3D SpatialImage:
    >>> sp_img = random_spatial_image((3, 4, 5), voxelsize=[1., 0.51, 0.5], dtype='uint8')
    >>> print(sp_img.voxelsize)
    [1.0, 0.51, 0.5]
    >>> print(sp_img.shape)
    (3, 4, 5)
    >>> vt_img = spatial_to_vt_image(sp_img)
    >>> isinstance(vt_img, Image)
    True
    >>> print(vt_img.spacing())
    [1.0, 0.5099999904632568, 0.5]
    >>> print(vt_img.shape())
    [3, 4, 5]

    """
    return Image(sp_img, sp_img.voxelsize)


def spatial_to_vt_vtimage(sp_img):
    """Convert a ``SpatialImage`` in a ``vtImage.image``.

    Parameters
    ----------
    sp_img : timagetk.SpatialImage
        The ``SpatialImage`` object to convert as ``Image``.

    Returns
    -------
    vt.vtImage
        The converted `vt` image.

    Examples
    --------
    >>> from vt import vtImage
    >>> from timagetk.third_party.vt_image import spatial_to_vt_vtimage
    >>> from timagetk.array_util import random_spatial_image
    >>> # Initialize a random (uint8) 3D SpatialImage:
    >>> sp_img = random_spatial_image((3, 4, 5), voxelsize=[1., 0.51, 0.5], dtype='uint8')
    >>> print(sp_img.voxelsize)
    [1.0, 0.51, 0.5]
    >>> print(sp_img.shape)
    (3, 4, 5)
    >>> vt_img = spatial_to_vt_vtimage(sp_img)
    >>> isinstance(vt_img, vtImage)
    True
    >>> print(vt_img.spacing()[::-1])  # reverse voxelsize: XYZ -> ZYX
    >>> print(vt_img.shape()[::-1])  # reverse shape: XYZ -> ZYX

    """
    return vtImage(sp_img, sp_img.voxelsize[::-1])  # reverse voxelsize: ZYX -> XYZ
