#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""MorphoNet library."""

import getpass
from collections.abc import Iterable
from os.path import join

import numpy as np
from timagetk.third_party.ctrl.io import read_lineage
from timagetk.algorithms.resample import new_shape_from_max
from timagetk.algorithms.resample import resample
from timagetk.algorithms.pointmatching import pointmatching
from timagetk.algorithms.trsf import compose_trsf
from timagetk.algorithms.trsf import inv_trsf
from timagetk.bin.logger import get_logger
from timagetk.components.labelled_image import LabelledImage
from timagetk.components.labelled_image import labels_at_stack_margins
from timagetk.components.tissue_image import TissueImage3D
from timagetk.components.trsf import Trsf
from timagetk.third_party.vt_features import get_barycenter, get_volume
from timagetk.visu.pyvista import tissue_image_cell_polydatas
from timagetk.io import imread

logger = get_logger(__name__)

TYPE_MAPPING = {"observed": 0, "simulated": 1, "drawed": 2}


def _test_morphonet():
    """Test morphonet import."""
    try:
        import morphonet
    except ImportError:
        msg = "MorphoNet library is not available, please install it with: `pip install morphonet`!"
        raise ImportError(msg)


def load_mn(path):
    """Load OBJ mesh and TXT info files."""
    with open(path, 'r') as f:
        obj = f.read()
    return obj


def label_image_to_mesh(seg_img, time_idx=0, **kwargs):
    """Use MorphoNet tools to mesh a labelled image.

    Parameters
    ----------
    seg_img : timagetk.components.labelled_image.LabelledImage or timagetk.components.tissue_image.TissueImage3D
        The labelled image to mesh.
    time_idx : int, optional
        The temporal index corresponding to the labelled image. Defaults to ``0``.

    Other Parameters
    ----------------
    background : int
        Usefull if the provided labelled image has no attribute ``background``.
        Default to ``1``.
    kwargs : dict
        They are passed to ``morphonet.tools.convert_to_OBJ``.

    Returns
    -------
    str
        The string representing the mesh object.

    See Also
    --------
    morphonet.tools.convert_to_OBJ

    Examples
    --------
    >>> from timagetk.third_party.morphonet import label_image_to_mesh
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.io import imread
    >>> from timagetk import LabelledImage
    >>> from timagetk.components.labelled_image import image_without_labels
    >>> from timagetk.components.labelled_image import labels_at_stack_margins
    >>> # -- Load the segmented image:
    >>> seg_img = imread(shared_dataset('p58','segmented')[0], LabelledImage, not_a_label=0)
    >>> # -- Remove cells at stack margins:
    >>> cells2exclude = labels_at_stack_margins(seg_img, 2)
    >>> seg_img = image_without_labels(seg_img, cells2exclude)
    >>> seg_img[seg_img == 0] = 1
    >>> # -- Compute the mesh object:
    >>> mesh = label_image_to_mesh(seg_img)
    >>> # -- Save it to an OBJ file:
    >>> from morphonet.tools import save_mesh
    >>> save_mesh("/tmp/p58-t0_mn_mesh.obj", mesh)

    """
    import morphonet

    # Need a background value, try to get it from the image attribute:
    try:
        background = seg_img.background
    except AttributeError:
        background = None
    # Get the background value from the keyword argument or use default:
    if background is None:
        background = kwargs.pop('background', 1)

    mesh = morphonet.tools.convert_to_OBJ(seg_img, time_idx, background=background, VoxelSize=seg_img.voxelsize,
                                          path_write=None, **kwargs)

    return mesh


def upload_to_morphonet(dataset_name, login, obj_files=None, info_files=None, raw_files=None, **kwargs):
    """Upload files to MorphoNet in the corresponding dataset.

    Parameters
    ----------
    dataset_name : str
        Name to give to the dataset.
    login : str
        MorphoNet user name, *a.k.a.* login.
    obj_files : dict, optional
        Time indexed dictionary of mesh files to upload.
    info_files : dict, optional
        Dictionary of info files to upload, with info name as key and path to file as value.
    raw_files : dict, optional
        Time indexed dictionary of intensity image files to upload.

    Other Parameters
    ----------------
    password : str
        The password to connect to morphonet.
    id_NCBI : int
        The NCBI taxonomy id.
    force : bool
        If True, remove any existing dataset and upload selected.

    Examples
    --------
    >>> from timagetk.third_party.morphonet import time_series_to_morphonet_obj
    >>> from timagetk.io.dataset import shared_data
    >>> seg_imgs = shared_dataset('p58','segmented')
    >>> time_points = shared_dataset('p58','time-points')
    >>> lineages = shared_dataset('p58','lineage')
    >>> mesh_fnames, info_fnames = time_series_to_morphonet_obj(seg_imgs, time_points, lineages, path="/tmp", suffix="p58-")

    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph
    >>> from timagetk.third_party.morphonet import save_morphonet_info_files
    >>> ttg = example_temporal_tissue_graph('p58', extract_dual=False, features=['volume'])
    >>> fields = {'volume': ttg.cell_property_dict('volume'), 'ltr_v': ttg.log_relative_value('volume'),}
    >>> info_fnames = save_morphonet_info_files(seg_imgs, time_points, lineages, properties=fields, path="/tmp", suffix='p58-')

    >>> from timagetk.third_party.morphonet import upload_to_morphonet
    >>> upload_to_morphonet('p58', 'jlegrand', mesh_fnames, info_fnames, id_NCBI=3702)

    """
    _test_morphonet()
    import morphonet

    pwd = kwargs.get("password", None)
    try:
        assert pwd is not None
    except AssertionError:
        pwd = getpass.getpass()

    mn = morphonet.Net(login, pwd)

    # Check if `dataset_name` is not yet defined in MorphoNet database
    dataset_exist = mn.select_dataset_by_name(dataset_name) != -1

    if dataset_exist and kwargs.get('force', False):
        mn.delete_dataset()
        dataset_exist = mn.select_dataset_by_name(dataset_name) != -1

    if dataset_exist:
        if obj_files is not None:
            logger.critical(f"You are trying to upload OBJ data to a non-empty dataset {dataset_name}!")
            exit(1)
    else:
        if obj_files is None:
            logger.critical("When creating a new dataset, defining ``obj_files`` is mandatory!")
            exit(1)
        else:
            mn.create_dataset(dataset_name, 0, len(obj_files) - 1,
                              id_NCBI=kwargs.get('id_NCBI', 0),
                              id_type=kwargs.get('id_type', 0))
            mn.select_dataset_by_name(dataset_name)

    if obj_files is not None:
        for t, obj in obj_files.items():
            if isinstance(obj, str):
                obj = load_mn(obj)
            logger.info(f"Uploading t{t} mesh file...")
            mn.upload_mesh_at(t, obj) #TODO: add 'center'=True ?

    if info_files is not None:
        for info_name, info in info_files.items():
            if isinstance(info, str):
                info = load_mn(info)
            logger.info(f"Uploading {info_name} info file...")
            mn.upload_property(info_name, info)

    if raw_files is not None:
        for t, raw in raw_files.items():
            if isinstance(raw, str):
                logger.info(f"Loading the intensity image '{raw}'...")
                raw = imread(raw)
            if np.max(raw.get_shape()) > 256:
                logger.info("Resizing images to max dimensions of 256x256x256...")
                raw = resample(raw, shape=new_shape_from_max(raw.get_shape(), 256))
            logger.info(f"Uploading t{t} intensity image...")
            mn.upload_image_at(t, raw, raw.voxelsize)


def time_series_to_morphonet_obj(labelled_images: object, time_points: object, lineages: object = None, path: object = None, suffix: object = "", **kwargs: object) -> object:
    """Convert a time-series of segmented image into a mesh and info files ready to upload.

    Parameters
    ----------
    labelled_images : list of LabelledImage or list of str
        The list of labelled image (path) to mesh.
    time_points : list of int
        List of time-points values, describe the observation time.
    lineage : list[ctrl.Lineage], optional
        The list of lineage associated to the labelled image.
    path : str, optional
        Path to directory where to save the mesh and info files. If None, the mesh and info files are returned as a string.
    suffix : str, optional
        Suffix to append to the mesh and info file names.

    Other Parameters
    ----------------
    smoothing_iteration : int, optional
        The number of smoothing iterations to apply to each cell mesh.
    target_reduction : float or list of float, optional
        The percentage of reduction to achieve by the decimation step.
    voxelsize : float or list of float, optional
        Resample the labelled image to this voxelsize prior to meshing.
    background_id : int
        The id associated to the background location, if any.
    exclude_marginal_cells : bool
        Whether to exclude the cells at a certain distance of the stack margin or not.
    distance_from_margins : int
        The distance from the stack margins where cells will be considered as "marginal".
    cells_to_keep : list
        List of time-indexed cell ids ``(t, cid)`` to keep per time point.
        It will be intersected with the list of images labels.
        And labels at stack margins & background will be excluded from this list.
    rigid_registration : bool, optional
        Use this to register rigidly all the meshes onto the same geometry (last time-point). Rigid transformation
        is computed based on the lineage between two consecutive time-points. Default is False.
    return_process : bool, optional
        If True, return the processing file. Default is False.
    Returns
    -------
    dict
        Time indexed dictionary of filepath to mesh objects if path is non-empty else the mesh objects
    dict
        Name indexed dictionary of filepath to info objects if path is non-empty else the info objects
    dict, optional
        Name indexed dictionary of filepath to process objects.

    Examples
    --------
    >>> from timagetk.third_party.morphonet import time_series_to_morphonet_obj
    >>> from timagetk.io.dataset import shared_data
    >>> import os
    >>> import timagetk
    >>> seg_imgs = [shared_data('flower_labelled', t) for t in range(3)]
    >>> time_points = [0, 10, 20] # dummy time-points
    >>> _ROOT = os.path.abspath(timagetk.__path__[0] + "/../../")
    >>> lineages = [_ROOT + f'/data/p58/p58_t{t}-{t+1}_lineage.txt' for t in range(2)]
    >>> # Write the mesh & properties object in /tmp with suffix `p58-`
    >>> mesh_fnames, info_fnames = time_series_to_morphonet_obj(seg_imgs, time_points, lineages, path="/tmp",suffix="p58-")
    >>> # Do not write the mesh & properties, return them directly in the returned variables
    >>> meshes, infos = time_series_to_morphonet_obj(seg_imgs, time_points, lineages)

    """
    # Meshing KWARGS:
    smoothing_iteration = kwargs.get('smoothing_iteration', 10)
    target_reduction = kwargs.get('target_reduction', 0.5)
    voxelsize = kwargs.get('voxelsize', 0.5)
    # Properties KWARGS:
    properties = kwargs.get('properties', None)
    group_properties = kwargs.get('group_properties', None)
    # LabelledImage KWARGS:
    background_id = kwargs.get('background_id', 1)
    exclude_marginal_cells = kwargs.get('exclude_marginal_cells', True)
    distance_from_margins = kwargs.get('distance_from_margins', 2)
    cells_to_keep = kwargs.get('cells_to_keep', [])  # list to time-indexed cell ids [(t, cid), ...]
    rigid_registration = kwargs.get('rigid_registration', False)

    process_mesh = None
    if kwargs.get('return_process', False):
        process_mesh = {}

    # - Control the `decimation` factor, can be an int or a list of int (os same length than ``labelled_images``)
    n_imgs = len(labelled_images)
    if isinstance(target_reduction, Iterable) and len(target_reduction) == 1:
        target_reduction = target_reduction[0]
    if isinstance(target_reduction, (int, float)):
        target_reduction = [float(target_reduction)] * n_imgs
    try:
        assert len(target_reduction) == n_imgs
    except AssertionError:
        n = len(target_reduction)
        logger.critical(f"Not the same number of images ({n_imgs}) and target_reduction values ({n})!")
        exit(1)

    logger.info("Starting cell-based meshing task")
    obj_fnames = {}
    cell_barycenters = {}
    cell_volumes = {}
    for ti in range(n_imgs)[::-1]: # read in reversed order to allow rigid registration if needed
        seg_img = labelled_images[ti]
        if process_mesh is not None:
            process_mesh[f"{ti}"] = {"smoothing_iteration": smoothing_iteration,
                                     "target_reduction": target_reduction[ti],
                                     "exclude_marginal_cells": exclude_marginal_cells,
                                     "distance_from_margins": distance_from_margins,
                                     "cells_to_keep": cells_to_keep,
                                     "background_id": background_id}
        # - Load the labelled image if a list of str (file paths) is given as `labelled_image`
        if isinstance(seg_img, str):
            logger.info(f"Loading image file '{seg_img}' ({time_points[ti]}h)...")
            seg_img = imread(seg_img, LabelledImage, not_a_label=0)

            if process_mesh is not None:
                process_mesh[f"{ti}"] = labelled_images[ti]

            # - Get the list of labels in the image:
        labels = seg_img.labels()
        # - Build the list of cells to exclude: 'background' & 'labels at stack margins'
        cells2exclude = []
        if background_id in labels:
            cells2exclude.extend([background_id])
        if exclude_marginal_cells:
            logger.info("Performing labels at stack margins detection...")
            cells2exclude.extend(labels_at_stack_margins(seg_img, distance_from_margins))
        # - Filter the list of labels
        if cells_to_keep == []:
            labels = set(labels) - set(cells2exclude)
        else:
            tp_label = [l for tl, l in cells_to_keep if tl == ti]
            if len(tp_label) == 0:
                logger.warning(f"No labels manually selected for time-point {ti}!")
                logger.warning("Falling back to global selection!")
                labels = set(labels) - set(cells2exclude)
            else:
                labels = set(labels) & set(tp_label) - set(cells2exclude)

        # -- Resample the image to max the voxelsize in all axis
        # eg: if image is [0.6, 0.2 ,0.2] and voxelsize = 0.5, the resample to [0.6, 0.5, 0.5]
        if isinstance(voxelsize, float):
            voxelsize = [voxelsize] * seg_img.ndim
        voxelsize = np.max(np.array([voxelsize, seg_img.voxelsize]), axis=0)
        if any(vxs > seg_img.voxelsize[axis_idx] for axis_idx, vxs in enumerate(voxelsize)):
            logger.info(f"Original segmented image voxelsize: {seg_img.voxelsize}")
            seg_img = resample(seg_img, voxelsize=voxelsize, interpolation="cellbased", cell_based_sigma=1)
            if process_mesh:
                process_mesh[f"{ti}"]["voxelsize"] = voxelsize.tolist()

        logger.info("Meshing labelled image...")
        labels = set(labels) - set(cells2exclude)
        seg_img = TissueImage3D(seg_img, background=background_id, not_a_label=seg_img.not_a_label)
        cell_polydatas = tissue_image_cell_polydatas(seg_img.transpose('xyz'),
                                                     labels=labels, smoothing_iterations=smoothing_iteration,
                                                     target_reduction=target_reduction[ti])  # reverse axis order for MN

        if rigid_registration and (lineages is not None):
            logger.info("Register the mesh...")
            # compute the barycenters & volume of the cells
            cell_barycenters[ti] = get_barycenter(seg_img, seg_img.cell_ids(), real=True) # order xyz
            cell_volumes[ti] = get_volume(seg_img, seg_img.cell_ids(), real=True)
            if ti < n_imgs - 1:
                lineage = lineages[ti] # lineage between t and t+1
                if isinstance(lineage, str):
                    lineage = read_lineage(lineage)
                # compute anchors points based on the lineage & the cell barycenters
                ancestor_pts = []
                descendant_pts = []
                for alab, dlab in lineage.items():
                    # assert that the label properties have been computed (be careful because of resampling)
                    if (alab in cell_volumes[ti]) and (set(dlab).issubset(set(cell_volumes[ti+1].keys()))):
                        # anchor point for the image at t is the centroid of the ancestor
                        ancestor_pts.append(cell_barycenters[ti][alab])
                        # compute the anchor point for the  image at t+1 as the weighted barycenter of the
                        # union of the descendants
                        wc = np.sum([cell_barycenters[ti+1][lab] * cell_volumes[ti+1][lab] for lab in dlab], axis=0)
                        descendant_pts.append(wc / sum([cell_volumes[ti+1][lab] for lab in dlab]))
                # compute rigid transformation from the anchors points
                trsf = pointmatching(ancestor_pts, descendant_pts, method='rigid') # real trsf direction t+Δt-->t !
                trsf_rigid = compose_trsf([trsf, trsf_rigid])
            else:
                # initialize the identity transformation
                trsf_rigid = Trsf(np.diag(np.ones(4)))

            # apply the transformation to the polydata points
            inv_trsf_rigid = inv_trsf(trsf_rigid).get_array()
            cell_polydatas = {lab: polydata.transform(inv_trsf_rigid) # inv trsf to go forward
                              for lab, polydata in cell_polydatas.items()} # apply xyz trsf. onto xyz polydata
            if process_mesh:
                process_mesh[f"{ti}"]["trsf_rigid"] = inv_trsf_rigid.tolist()

        if path is not None:
            obj_fname = join(path, f"{suffix}t{ti}.obj")
            save_obj_time_cell_polydatas(cell_polydatas, obj_filename=obj_fname, time=ti)
            obj_fnames[ti] = obj_fname
        else:
            obj_mesh = save_obj_time_cell_polydatas(cell_polydatas, obj_filename=None, time=ti)
            obj_fnames[ti] = obj_mesh

    info_fnames, process_info = save_morphonet_info_files(labelled_images, time_points, lineages=lineages,
                                                          group_properties=group_properties, properties=properties,
                                                          path=path, suffix=suffix, return_process=True)

    if process_mesh:
        process = {"mesh": process_mesh,
                   "info": process_info}
        return obj_fnames, info_fnames, process
    else:
        return obj_fnames, info_fnames


def save_obj_time_cell_polydatas(cell_polydatas, obj_filename=None, time=0):
    """Custom writer of OBJ mesh from VTK PolyData.

    Parameters
    ----------
    cell_polydatas : dict
        Cell based dictionary of pv.Polydata (pyvista format).
    obj_filename : str, optional
        File nama and path of the mesh file to write. If None, the mesh is returned as a string.
    time : int, optional
        Time index of the mesh in the time series. Defaults to ``0``.

    Returns
    -------
    str or None
        If `obj_filename` is None, returns the OBJ file content as a string. Otherwise, writes the content to the file and returns None.

    See Also
    --------
    timagetk.visu.pyvista.tissue_image_cell_polydatas
    timagetk.third_party.morphonet.time_series_to_morphonet_obj

    Examples
    --------
    >>> from timagetk.third_party.morphonet import time_series_to_morphonet_obj
    >>> from timagetk.io.dataset import shared_data
    >>> seg_imgs = shared_dataset('p58','segmented')
    >>> lineages = shared_dataset('p58','lineage')
    >>> vtk_meshes = time_series_to_morphonet_obj(seg_imgs, lineages)
    >>> from timagetk.third_party.morphonet import save_obj_time_cell_polydatas
    >>> save_obj_time_cell_polydatas("/tmp/p58-t0_mesh.obj", vtk_meshes[0], time=0)

    """
    obj_content = []
    offset = 1

    for cell, polydata in cell_polydatas.items():
        obj_content.append(f"g {time},{cell}\n")
        vertex_dict = {}

        points = polydata.points
        if points is None:
            continue

        for p, point in enumerate(points):
            obj_content.append(f"v {' '.join(map(str, point))}\n")
            vertex_dict[p] = p + offset
        offset += len(points)

        faces = polydata.faces.reshape(-1, 4)[:, 1:]  # Skip the first element of each row (the size of the face)

        for face in faces:
            obj_content.append(f"f {vertex_dict[face[0]]} {vertex_dict[face[1]]} {vertex_dict[face[2]]}\n")

    obj_string = ''.join(obj_content)

    if obj_filename is not None:
        with open(obj_filename, "w") as obj_file:
            obj_file.write(obj_string)
        return None
    else:
        return obj_string

def format_morphonet_info(field_dict, field_name, field_type):
    """Format in time-indexed MorphoNet OTP format `t, id`.

    Parameters
    ----------
    field_dict : dict
        Time-indexed label dictionary of property to save in MorphoNet OTP format.
    field_name : str
        Name of the property to save.
    field_type : {'space', 'time', 'group', 'float'}
        Type of field to register, see [morphonet_format]_.

    Returns
    -------
    str
        The string of characters to save as MorphoNet information file.

    References
    ----------
    .. [morphonet_format] https://morphonet.org/help_format

    """
    try:
        assert field_type in ['space', 'time', 'group', 'float']
    except AssertionError:
        raise ValueError(
            f"Parameter `field_type` should be in ['space', 'time', 'group', 'float'], got '{field_type}'!")

    info_str = f"# MorphoNet '{field_name}' information\n"
    info_str += f'type:{field_type}\n'
    if field_type == 'time':
        info_str += _format_morphonet_time_info(field_dict)
    elif field_type == 'space':
        info_str += _format_morphonet_space_info(field_dict)
    elif field_type == 'group':
        info_str += _format_morphonet_group_info(field_dict)
    elif field_type == 'float':
        info_str += _format_morphonet_float_info(field_dict)

    return info_str


def _format_morphonet_time_info(field_dict):
    info_str = ""
    for (time_idx, label), descendants in field_dict.items():
        for descendant in descendants:
            info_str += f"{time_idx},{label}:{time_idx + 1},{descendant}\n"
    return info_str


def _format_morphonet_space_info(field_dict):
    info_str = ""
    for (time_idx, label), neighbors in field_dict.items():
        for neighbor in neighbors:
            info_str += f"{time_idx},{label}:{time_idx},{neighbor}\n"
    return info_str


def _format_morphonet_group_info(field_dict):
    info_str = ""
    for (time_idx, label), group in field_dict.items():
        info_str += f"{time_idx},{label}:{group}\n"
    return info_str


def _format_morphonet_float_info(field_dict):
    info_str = ""
    for (time_idx, label), ppty in field_dict.items():
        info_str += f"{time_idx},{label}:{ppty}\n"
    return info_str


def save_morphonet_info_files(images, time_points, lineages=None, group_properties=None, properties=None, path=None,
                              suffix="", **kwargs):
    """Create info files from a time-series and cell property dictionaries.

    Parameters
    ----------
    images : list of str or list of LabelledImage
        List of labelled image, used for neigbhorhood information.
    time_points : list of int
        List of time-points values, describe the observation time.
    lineages : list of str or list[ctrl.Lineage], optional
        List of cell lineages.
    group_properties : dict, optional
        Property indexed dictionary: `{ppty_name: {(t, cid): int or str}`.
    properties : dict, optional
        Property indexed dictionary: `{ppty_name: {(t, cid): float}`.
    path : str, optional
        Path to directory where to save the info files. If None, the info files are not saved.
    suffix : str, optional
        Suffix to append to the info file names.

    Other Parameters
    ----------------
    fmt : str
        Specify the file format of the lineage file if not a `ctrl.Lineage`.
        By default, we try to guess the format.
    return_process : bool, optional
        If True, return the processing file. Default is True.

    Returns
    -------
    dict
        Name indexed dictionary of filepath to info objects if path is not None, else the objects itself.

    Examples
    --------
    >>> from timagetk.third_party.morphonet import time_series_to_morphonet_obj
    >>> from timagetk.io.dataset import shared_data
    >>> from visu_core.vtk.utils.image_tools import image_to_vtk_cell_polydatas
    >>> seg_imgs = shared_dataset('p58','segmented')
    >>> time_points = shared_dataset('p58','time-points')
    >>> lineages = shared_dataset('p58','lineage')
    >>> from timagetk.third_party.morphonet import save_morphonet_info_files
    >>> info_files = save_morphonet_info_files(seg_imgs, time_points, lineages, path="/tmp", suffix='p58-')
    >>> print(info_files)
    {'space': '/tmp/p58-t0_t49_space_infos.txt', 'time': '/tmp/p58-t0_t49_time_infos.txt'}

    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph
    >>> ttg = example_temporal_tissue_graph('p58', extract_dual=False, features=['volume'])
    >>> fields = {'volume': ttg.cell_property_dict('volume'), 'ltr_v': ttg.log_relative_value('volume'),}

    >>> info_files = save_morphonet_info_files(seg_imgs, time_points, lineages, properties=fields, path="/tmp",suffix='p58-')
    >>> print(info_files)

    """
    field_names = ['space', 'time']
    field_types = ['space', 'time']
    info = {f: {} for f in field_names}

    process_info = None
    if kwargs.get("return_process", True):
        process_info = {f: {} for f in field_names}

    if group_properties is not None:
        group_ppty_names = list(group_properties.keys())
        n_group_ppties = len(group_ppty_names)
        field_names += group_ppty_names
        field_types += ['group'] * n_group_ppties
        info.update(group_properties)
    if properties is not None:
        float_ppty_names = list(properties.keys())
        n_float_ppties = len(float_ppty_names)
        field_names += float_ppty_names
        field_types += ['float'] * n_float_ppties
        info.update(properties)

    # -- Gather SPACE information:
    for time_idx, _time in enumerate(time_points):
        seg_img = images[time_idx]
        if isinstance(seg_img, str):
            if process_info is not None:
                process_info['space'][f"{time_idx}"] = seg_img

            seg_img = imread(seg_img, LabelledImage, not_a_label=0)
        neighborhood = seg_img.neighbors()
        for label, neighbors in neighborhood.items():
            info['space'][(time_idx, label)] = neighbors

    # -- Gather TIME information:
    if lineages is not None:
        for time_idx, _time in enumerate(time_points[:-1]):
            lineage = lineages[time_idx]
            if isinstance(lineage, str):
                if process_info is not None:
                    process_info['time'][f"{time_idx}"] = lineage
                lineage = read_lineage(lineage, fmt=kwargs.get('fmt', None))
            for mother, daughters in lineage.items():
                info['time'][(time_idx, mother)] = daughters

    first_tp, last_tp = np.min(time_points), np.max(time_points)

    # TODO: other properties need to be add to the process

    info_files = {}
    for field_name, field_type in zip(field_names, field_types):
        info_str = format_morphonet_info(info[field_name], field_name, field_type)

        if path is not None:
            info_filename = join(path, f"{suffix}t{first_tp}_t{last_tp}_{field_name}_infos.txt")
            info_file = open(info_filename, 'w+')
            info_file.write(info_str)
            info_file.write("\n")
            info_file.flush()
            info_file.close()
            info_files[field_name] = info_filename
        else:
            info_files[field_name] = info_str

    if process_info:
        return info_files, process_info
    else:
        return info_files


def temporal_graph_to_info_file(graph, cell_property, field_types, path="", suffix=""):
    """Export cell properties from a temporal graph to a MorphoNet info file.

    Parameters
    ----------
    graph : timagetk.graphs.TemporalTissueGraph
        A temporal tissue graph with cell properties to export.
    cell_property : list of str
        The list of cell properties to export.
    field_types : list of str
        List of property types, should be either:
          - 'float': for quantitative data
          - 'group': for group (semantic) data
    path : str, optional
        Path to directory where to save the info files.
    suffix : str, optional
        Suffix to append to the info file names.

    Returns
    -------
    list
        List of morphonet info files path.

    Examples
    --------
    >>> from timagetk.third_party.morphonet import temporal_graph_to_info_file
    >>> from timagetk.graphs.temporal_tissue_graph import example_temporal_tissue_graph_from_csv
    >>> ttg = example_temporal_tissue_graph_from_csv()
    >>> print(ttg.list_cell_properties())
    ['shape_anisotropy', 'epidermis_growth_tensor_before', 'affine_deformation_matrix', 'growth_tensor_after', 'epidermis_landmarks', 'fused_epidermis_median', 'epidermis_affine_deformation_r2', 'fused_barycenter', 'epidermis_affine_deformation_matrix', 'epidermis_growth_rates', 'volumetric_strain_rates', 'epidermis_area', 'epidermis_median', 'area', 'epidermis', 'growth_rates', 'number_of_neighbors', 'landmarks', 'inertia_axis', 'epidermis_areal_strain_rates', 'division_rate', 'epidermis_strain_anisotropy', 'barycenter', 'volume', 'growth_tensor_before', 'epidermis_growth_tensor_after', 'affine_deformation_r2']
    >>> identity_dict = ttg.cell_property_dict('shape_anisotropy', only_defined=True)
    >>> info_files = temporal_graph_to_info_file(ttg, ['volume'], ['float'], path="/tmp", suffix="p58-")
    >>> print(info_files)
    {'volume': '/tmp/p58-t0_t10_volume_infos.txt'}

    """
    from timagetk.tasks.features import TEMPORAL_DIFF_FEATURES
    ppty_dict = {}
    for ppty in cell_property:
        if ppty not in graph.list_cell_properties():
            if ppty in TEMPORAL_DIFF_FEATURES:
                eval(f"graph.{ppty}")
            else:
                continue
        ppty_dict[ppty] = graph.cell_property_dict(ppty, default=np.nan, only_defined=True)
        # May have some `None` values
        if None in list(ppty_dict[ppty].values()):
            ppty_dict[ppty] = {k: v for k, v in ppty_dict.items() if v is not None}

    first_tp, last_tp = np.min(graph._elapsed_time), np.max(graph._elapsed_time)

    info_files = {}
    for field_name, field_type in zip(cell_property, field_types):
        if field_name not in ppty_dict:
            continue
        logger.info(f"Formatting '{field_name}' field property as '{field_type}'...")
        info_filename = join(path, f"{suffix}t{first_tp}_t{last_tp}_{field_name}_infos.txt")
        info_file = open(info_filename, 'w+')
        info_str = format_morphonet_info(ppty_dict[field_name], field_name, field_type)
        info_file.write(info_str)
        info_file.write("\n")
        info_file.flush()
        info_file.close()
        info_files[field_name] = info_filename

    return info_files
