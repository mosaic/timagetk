#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

"""
PlantSeg Segmentation Module

This module provides functionalities for semantic and instance segmentation tailored to plant tissue datasets.
It features tools for deep learning-based predictions and post-processing, making it useful for researchers and developers working with biological image data.

Key Features
------------
- **UNet Predictions**: Perform patch-wise deep learning model predictions using `unet_predict` and tiled predictions for larger images with `unet_tiled_predict`.
- **Model Management**: List all available pre-trained UNet models with `list_models()`.
- **Multicut Segmentation**: Post-prediction refinement using `multicut_segmentation` to perform instance-level segmentation.
- **Customization Options**: Define rescaling factors, devices, and UNet configurations for tailored workflows.

Usage Examples
--------------
>>> from timagetk.third_party.plantseg import unet_tiled_predict
>>> from timagetk.third_party.plantseg import multicut_segmentation
>>> from timagetk.io.dataset import shared_data
>>> from timagetk.third_party.plantseg import list_models
>>> # List available pre-trained models:
>>> models = list_models()
>>> print(models)
['generic_confocal_3D_unet', 'generic_light_sheet_3D_unet', 'confocal_3D_unet_ovules_ds1x', 'confocal_3D_unet_ovules_ds2x', 'confocal_3D_unet_ovules_ds3x', 'confocal_2D_unet_ovules_ds2x', 'lightsheet_3D_unet_root_ds1x', 'lightsheet_3D_unet_root_ds2x', 'lightsheet_3D_unet_root_ds3x', 'lightsheet_2D_unet_root_ds1x', 'lightsheet_3D_unet_root_nuclei_ds1x', 'lightsheet_2D_unet_root_nuclei_ds1x', 'confocal_2D_unet_sa_meristem_cells', 'confocal_3D_unet_sa_meristem_cells', 'lightsheet_3D_unet_mouse_embryo_cells', 'confocal_3D_unet_mouse_embryo_nuclei', 'PlantSeg_3Dnuc_platinum']
>>> int_img = shared_data('flower_confocal', 0)
>>> # Example: UNet tiling-based prediction
>>> pred_img = unet_tiled_predict(int_img)
# Example: Perform multicut segmentation after prediction
>>> seg_img_A = multicut_segmentation(pred_img)

"""

import numpy as np
from skimage.util import img_as_float32
from skimage.util import img_as_int
from skimage.util import img_as_ubyte
from skimage.util import img_as_uint
from timagetk.util import check_type
from tqdm import tqdm

from timagetk import SpatialImage
from timagetk.algorithms.resample import resample
from timagetk.bin.logger import get_logger
from timagetk.components.labelled_image import LabelledImage
from timagetk.array_util import make_patches, check_patch_size

log = get_logger(__name__)

PLANTSEG_INSTALL_CPU = "conda install -c gnomon -c conda-forge plantseg pytorch"
PLANTSEG_INSTALL_CUDA = "conda install -c gnomon -c pytorch -c nvidia plantseg pytorch-gpu 'pytorch-cuda<=12.2'"
try:
    import plantseg
except ImportError:
    log.critical("Failed to import the `plantseg` library!")
    log.info(f"To install it without CUDA support, use:\n{PLANTSEG_INSTALL_CPU}")
    log.info(f"To enable CUDA support, install it with:\n{PLANTSEG_INSTALL_CUDA}")
    log.info("To ensure compatibility, use the 'pytorch-cuda' package to match CUDA with your installed driver version.")
    raise ImportError(f"Could not import the `plantseg` library.")

from plantseg.predictions import unet_predictions
from plantseg.segmentation import dt_watershed
from plantseg.segmentation import multicut

#: Default `rescaling`:
DEF_RESCALE = None
#: List of valid `device` choices:
DEVICES = ['cuda', 'cpu']


def is_gpu_available():
    """Checks if a CUDA-enabled GPU is available.

    Returns
    -------
    bool
        ``True`` if a CUDA-enabled GPU is available, otherwise ``False``

    Examples
    --------
    >>> from timagetk.third_party.plantseg import is_gpu_available
    >>> is_gpu_available()
    """
    import torch
    return True if torch.cuda.is_available() else False


#: Default `device`:
DEF_DEVICE = 'cuda' if is_gpu_available() else 'cpu'


def list_models():
    """List available 3D U-Net models.

    Examples
    --------
    >>> from timagetk.third_party.plantseg import list_models
    >>> models = list_models()
    >>> print(models)
    """
    try:
        # plantseg 1.6.0
        from plantseg.utils import list_models
        return list_models()
    except ImportError:
        # plantseg 1.8.1
        from plantseg.models.zoo import ModelZoo
        from tempfile import NamedTemporaryFile
        zoo_tmp = NamedTemporaryFile(prefix='plantseg_zoo')
        zoo_custom_tmp = NamedTemporaryFile(prefix='plantseg_zoo_custom')
        mz = ModelZoo(zoo_tmp, zoo_custom_tmp)
        return mz.list_models()


#: List of available pre-trained models for 3D U-Net:
UNET_MODELS = list_models()
#: Default 3D U-Net `model_name`:
DEF_UNET_MODEL = UNET_MODELS[0]




def unet_predict(image, model_name=DEF_UNET_MODEL, device=DEF_DEVICE, rescaling=DEF_RESCALE, patch=(80, 160, 160),
                 single_batch_mode=True, model_update=False, disable_tqdm=False):
    """Perform 3D U-Net prediction of a cell tissue.

    Parameters
    ----------
    image : timagetk.SpatialImage
        Intensity image to predict using trained `model_name`.
    model_name : str, optional
        Name of the trained model to use, defaults to ``'confocal_PNAS_3d'``.
    device : {'cuda', 'cpu'}, optional
        Name of the device to use to perform prediction, defaults to ``'cuda'``.
    rescaling : list, optional
        The image voxelsize to rescale to prior to perform prediction.
        Set it to ``None`` to avoid rescaling. Defaults to ``None``.
    patch : list, optional
        Patch size to use for prediction. Defaults to ``(80, 160, 160)``.
    single_batch_mode : bool, optional
        If ``True`` (default) will perform prediction using a single batch.
    model_update : bool, optional
        If ``True`` will update the model to the latest version. Defaults to ``False``.
    disable_tqdm : bool, optional
        If ``True`` will disable tqdm progress bar. Defaults to ``False``.

    Returns
    -------
    timagetk.SpatialImage
        The predicted intensity image, with a 'uint8' ``dtype``.

    Notes
    -----
    This is based on Plant-Seg `unet_predictions`.

    If no CUDA capable GPU is found, fall back to 'cpu' usage.

    See Also
    --------
    plantseg.predictions.functional.predictions.unet_predictions : PlantSeg method to perform the prediction.

    References
    ----------
    https://https://github.com/kreshuklab/plant-seg

    Example
    -------
    >>> from timagetk.third_party.plantseg import unet_predict
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.stack import orthogonal_view
    >>> # EXAMPLE 1 - Quick 3D U-Net prediction demo on synthetic data
    >>> image = shared_data('synthetic', 'wall')
    >>> pred_img = unet_predict(image, model_name='confocal_3D_unet_sa_meristem_cells', device='cuda')
    >>> v = orthogonal_view(image, suptitle="Original intensity image")
    >>> v = orthogonal_view(pred_img, suptitle="Predicted intensity image")
    >>> # EXAMPLE 2 - 3D U-Net prediction demo on real data using floral meristem
    >>> image = shared_data('flower_confocal', 0)
    >>> pred_img = unet_predict(image, model_name='confocal_3D_unet_sa_meristem_cells', device='cuda')
    >>> v = orthogonal_view(image, suptitle="Original intensity image")
    >>> v = orthogonal_view(pred_img, suptitle="Predicted intensity image")

    """
    from timagetk.components.image import get_image_attributes
    check_type(image, obj_name='image', obj_type=SpatialImage)

    if rescaling is not None and any(vxs < rescale for vxs, rescale in zip(image.get_voxelsize(), rescaling)):
        log.info(f"Rescaling voxel-size from {image.get_voxelsize()} to {rescaling}...")
        image = resample(image, voxelsize=rescaling, interpolation='cspline')

    # Get image attributes:
    img_attr = get_image_attributes(image)
    im_dtype = img_attr.pop('dtype')

    patch = check_patch_size(image.get_array(), patch)
    # Perform prediction:
    pred_image = unet_predictions(
        img_as_float32(image),
        model_name=model_name,
        device=device,
        patch=patch,
        single_batch_mode=single_batch_mode,
        model_update=model_update,
        disable_tqdm=disable_tqdm
    )

    # Convert to the correct image `dtype`:
    if im_dtype.name == 'uint8':
        pred_image = img_as_ubyte(pred_image)
    elif im_dtype.name == 'uint16':
        pred_image = img_as_uint(pred_image)
    elif im_dtype.name == 'int16':
        pred_image = img_as_int(pred_image)
    else:
        raise Exception("Unsupported image dtype!")

    return SpatialImage(pred_image, **img_attr)


def unet_tiled_predict(image, model_name=DEF_UNET_MODEL, device=DEF_DEVICE, rescaling=DEF_RESCALE,
                       patch=(80, 160, 160), single_batch_mode=True, model_update=False):
    """Perform 3D U-Net prediction of a cell tissue with tilling by patches avoiding stitching effects.

    Parameters
    ----------
    image : timagetk.SpatialImage
        Intensity image to predict using trained `model_name`.
    model_name : str, optional
        Name of the trained model to use, defaults to ``'confocal_PNAS_3d'``.
    device : {'cuda', 'cpu'}, optional
        Name of the device to use to perform prediction, defaults to ``'cuda'``.
    rescaling : list, optional
        The image voxelsize to rescale to prior to perform prediction.
        Set it to ``None`` to avoid rescaling. Defaults to ``None``.
    patch : list, optional
        Patch size to use for prediction. Defaults to ``(80, 160, 160)``.
    single_batch_mode : bool, optional
        If ``True`` (default) will perform prediction using a single batch.
    model_update : bool, optional
        If ``True`` will update the model to the latest version. Defaults to ``False``.

    Notes
    -----
    This is based on Plant-Seg `unet_predictions`.

    If no CUDA capable GPU is found, fall back to 'cpu' usage.

    See Also
    --------
    plantseg.predictions.functional.predictions.unet_predictions : PlantSeg method to perform the prediction.

    References
    ----------
    https://https://github.com/kreshuklab/plant-seg

    Returns
    -------
    timagetk.SpatialImage
        The predicted intensity image, with the original image ``dtype``.

    Example
    -------
    >>> from timagetk.third_party.plantseg import unet_tiled_predict
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.stack import orthogonal_view
    >>> # EXAMPLE 1 - Quick 3D U-Net prediction demo on synthetic data
    >>> image = shared_data('synthetic', 'wall')
    >>> pred_img = unet_tiled_predict(image, model_name='confocal_3D_unet_sa_meristem_cells', device='cuda')
    >>> v = orthogonal_view(image, suptitle="Original intensity image")
    >>> v = orthogonal_view(pred_img, suptitle="Predicted intensity image")
    >>> # EXAMPLE 2 - 3D U-Net prediction demo on real data using floral meristem
    >>> image = shared_data('flower_confocal', 0)
    >>> pred_img = unet_tiled_predict(image, model_name='confocal_3D_unet_sa_meristem_cells', device='cuda')
    >>> v = orthogonal_view(image, suptitle="Original intensity image")
    >>> v = orthogonal_view(pred_img, suptitle="Predicted intensity image")

    """
    from timagetk.components.image import get_image_attributes
    check_type(image, obj_name='image', obj_type=SpatialImage)

    if rescaling is not None and any(vxs < rescale for vxs, rescale in zip(image.get_voxelsize(), rescaling)):
        log.info(f"Rescaling voxel-size from {image.get_voxelsize()} to {rescaling}...")
        image = resample(image, voxelsize=rescaling, interpolation='cspline')

    # Get image attributes:
    img_attr = get_image_attributes(image, exclude=['shape'])
    im_dtype = img_attr.pop('dtype')

    image_array = img_as_float32(image.get_array())
    # Ensure patch size will be small enough given image size
    if any([s < p for s, p in zip(image.shape, patch)]):
        msg = f"Patch size {patch} is too large"
        patch = tuple(np.minimum(patch, image.shape))
        log.warning(f"{msg}, reducing to {patch} to match image size {image.shape}")
    # Create slices to match given patch size:
    patch_slices = make_patches(image_array, patch)

    # Run 3D-UNET predictions on image slices of given patch size
    patch_predictions = []
    for patch_slice in tqdm(patch_slices, unit="patch", desc=f"U-Net prediction on patches ({device.upper()})"):
        pred_image = unet_predictions(
            image_array[patch_slice],
            model_name=model_name,
            device=device,
            patch=patch,
            single_batch_mode=single_batch_mode,
            model_update=model_update,
            disable_tqdm=True
        )
        patch_predictions += [pred_image]

    # Create a weight matrix, distance based, to combine predicted image patches:
    patch_points = np.transpose(np.meshgrid(*tuple(range(p) for p in patch), indexing='ij'), (1, 2, 3, 0))
    patch_center_distances = np.square(patch_points - np.array(patch) / 2)
    patch_weights = np.exp(-patch_center_distances / np.square(np.array(patch) / 4)).prod(axis=-1)

    # Re-create the whole predicted intensity image using a weighted sum of patches:
    pred_image = np.zeros(image_array.shape, dtype=float)
    weight_image = np.zeros(image_array.shape, dtype=float)
    for patch_slice, patch_prediction in zip(patch_slices, patch_predictions):
        pred_image[patch_slice] += patch_weights * patch_prediction
        weight_image[patch_slice] += patch_weights
    pred_image = pred_image / weight_image

    # Convert to the correct image `dtype`:
    if im_dtype.name == 'uint8':
        pred_image = img_as_ubyte(pred_image)
    elif im_dtype.name == 'uint16':
        pred_image = img_as_uint(pred_image)
    elif im_dtype.name == 'int16':
        pred_image = img_as_int(pred_image)
    else:
        raise Exception("Unsupported image dtype!")
    return SpatialImage(pred_image, **img_attr)


def multicut_segmentation(image, **kwargs):
    """Performs MultiCut segmentation.

    Parameters
    ----------
    image : timagetk.SpatialImage
        Intensity image to segment.

    Other Parameters
    ----------------
    threshold : float
        value for the threshold applied before distance transform.
    sigma_seeds : float
        smoothing factor for the watershed seed map.
    sigma_weights : float
        smoothing factor for the watershed weight map (default: 2).
    min_size : int
        minimal size of watershed segments (default: 100)
    alpha : float
        alpha used to blend input_ and distance_transform in order to obtain the
        watershed weight map (default: .9)
    apply_nonmax_suppression : bool
        whether to apply non-maximum suppression to filter out seeds.
        Needs nifty. (default: False)
    beta : float
        beta parameter for the Multicut. A small value will steer the segmentation towards under-segmentation.
        While a high-value bias the segmentation towards the over-segmentation. (default: 0.5)
    post_minsize : int
        minimal size of the segments after Multicut. (default: 100)

    Returns
    -------
    timagetk.LabelledImage
        The segmented image.

    See Also
    --------
    plantseg.segmentation.functional.segmentation.dt_watershed
    plantseg.segmentation.functional.segmentation.multicut

    References
    ----------
    https://https://github.com/kreshuklab/plant-seg

    Example
    -------
    >>> from timagetk.third_party.plantseg import unet_predict
    >>> from timagetk.third_party.plantseg import multicut_segmentation
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.visu.stack import orthogonal_view
    >>> image = shared_data('synthetic', 'wall')
    >>> pred_img = unet_predict(image, model_name='confocal_3D_unet_sa_meristem_cells', device='cuda', rescaling=None)
    >>> seg_img = multicut_segmentation(pred_img)
    >>> v = orthogonal_view(seg_img, suptitle="Segmented intensity image", val_range='auto', cmap='viridis')

    """
    import scipy.ndimage as nd
    from timagetk.components.labelled_image import relabel_from_mapping
    from timagetk.components.labelled_image import labels_at_stack_margins
    from timagetk.components.image import get_image_attributes
    check_type(image, obj_name='image', obj_type=SpatialImage)
    attr = get_image_attributes(image, exclude=['dtype'])
    # Remove multicut related kwargs from
    multicut_kwargs = {'beta': kwargs.pop('beta', 0.5), 'post_minsize': kwargs.pop('post_minsize', 100)}
    superpixel_image = dt_watershed(img_as_float32(image.get_array()), **kwargs)
    segmented_image = multicut(img_as_float32(image.get_array()), superpixel_image, **multicut_kwargs)
    # Get the list of all labels found by multicut
    all_labels = list(np.unique(segmented_image))
    # Get the labels at the stack margins, background should be somewhere around:
    margin_labels = labels_at_stack_margins(segmented_image, 3)
    # Compute the volume of marginal labels, assume the biggest one is the background:
    volume = list(nd.sum(np.ones_like(image), segmented_image, index=np.uint16(margin_labels)))
    max_vol_idx = np.argmax(volume)
    bkgd_id = margin_labels[max_vol_idx]
    # Create a relabelling dictionary to get contiguous labels starting at `1` as background:
    last_id = 1
    mapping = {bkgd_id: 1}
    for old_label in set(all_labels) - {bkgd_id}:
        last_id += 1
        mapping[old_label] = last_id
    # Search a valid
    if 0 not in all_labels:
        tmp_nal = 0  # shortcut
    else:
        # Search for first available value:
        tmp_nal = 0
        while tmp_nal in all_labels:
            tmp_nal += 1
    # Create a `LabelledImage` then relabel:
    segmented_image = LabelledImage(segmented_image, not_a_label=tmp_nal, **attr)
    segmented_image = relabel_from_mapping(segmented_image, mapping)

    # Convert to smallest possible dtype:
    if len(all_labels) < 2 ** 8:
        segmented_image = segmented_image.astype('uint8')
    elif len(all_labels) < 2 ** 16:
        segmented_image = segmented_image.astype('uint16')
    else:
        segmented_image = segmented_image.astype('uint32')

    return LabelledImage(segmented_image, not_a_label=0)
