#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
#  Copyright (c) 2018-2025 Univ. Lyon, ENS de Lyon, UCB Lyon 1, CNRS, INRAe, Inria
#  All rights reserved.
#  This file is part of the TimageTK library, and is released under the "GPLv3"
#  license. Please see the LICENSE.md file that should have been included as
#  part of this package.
# ------------------------------------------------------------------------------

import numpy as np
import vt

from timagetk.bin.logger import get_logger
from timagetk.components.labelled_image import LabelledImage
from timagetk.components.spatial_image import SpatialImage
from timagetk.components.tissue_image import AbstractTissueImage
from timagetk.components.trsf import DEF_TRSF_TYPE
from timagetk.components.trsf import TRSF_TYPE
from timagetk.util import _method_check
from timagetk.util import clean_type

log = get_logger(__name__)


# ------------------------------------------------------------------------------
# General keyword arguments
# ------------------------------------------------------------------------------

def general_kwargs(**kwargs):
    """Keyword argument parser for VT general parameters.

    Other Parameters
    ----------------
    param : bool, optional
        If ``True``, default ``False``, print the used parameters
    verbose : bool, optional
        If ``True``, default ``False``, increase code verbosity
    time : bool, optional
        If ``True``, default ``False``, print the CPU & USER elapsed time
    debug : bool, optional
        If ``True``, default ``False``, print the debug parameters
    quiet : bool, optional
        If ``True``, default ``True``, no prints from C algorithm

    Returns
    -------
    str_param : str
        Formatted "general parameters" for VT CLI.

    Examples
    --------
    >>> from timagetk.third_party.vt_parser import general_kwargs
    >>> general_kwargs()
    ' -noverbose -notime -nodebug'
    >>> general_kwargs(param=True, time=True, quiet=True)
    ' -param -noverbose -time -nodebug'

    """
    str_param = ""
    # - Providing 'param=True' will result in printing parameters
    param = kwargs.pop('param', False)
    if param:
        str_param += " -param"
    # - Providing 'verbose=True' will result in increased verbosity of the code
    verbose = kwargs.pop('verbose', False)
    if verbose:
        str_param += " -verbose -verbose"
    # - Providing 'quiet=True' will result in low to null verbosity of the code
    quiet = kwargs.pop('quiet', True)
    if quiet:
        str_param += " -noverbose"
    # - Providing 'time=True' will result in printing CPU & User elapsed time
    time = kwargs.pop('time', False)
    if time:
        str_param += " -time"
    else:
        str_param += " -notime"
    # - Providing 'debug=True' will result in printing debug log
    debug = kwargs.pop('debug', False)
    if debug:
        str_param += " -debug"
    else:
        str_param += " -nodebug"

    return str_param


# ------------------------------------------------------------------------------
# Parallelism keyword arguments
# ------------------------------------------------------------------------------

#: List of valid parallelism methods to use with `parallel_type`.
PARALLEL_TYPE = ["default", "openmp", "omp", "pthread", "thread"]
#: The default parallelism method.
DEFAULT_PARALLEL = PARALLEL_TYPE[0]
#: List of valid OMP methods to use with `omp_scheduling`, requires `parallel_type` to be set to  ``'omp'`` or ``'openmp'``.
OMP_TYPE = ["default", "static", "dynamic-one", "dynamic", "guided"]


def parallel_kwargs(parallel=True, parallel_type=DEFAULT_PARALLEL, n_job=None, omp_scheduling=None, **kwargs):
    """Keyword argument parser for VT parallelism parameters.

    Parameters
    ----------
    parallel : bool, optional
        If ``True`` (default), use parallelism -if available- for method in use.
    parallel_type : {"default", "openmp", "omp", "pthread", "thread"}, optional
        Type of parallelism to use, should be in `PARALLEL_TYPE`, `DEFAULT_PARALLEL` by default.
    n_job : int, optional
        Number of core to use for parallel computing, by default use the maximum number of available cores.
    omp_scheduling : {"default", "static", "dynamic-one", "dynamic", "guided"}, optional
        Change 'OpenMP' scheduling option, should be in `OMP_TYPE`, `default` by default.
        Requires `parallel_type` to be set to  ``'omp'`` or ``'openmp'``.

    Returns
    -------
    str
        Formatted "parallelism parameters" for VT CLI.

    Raises
    ------
    ValueError
        If `parallel_type` is not in ``PARALLEL_TYPE``, if not ``None``.

    See Also
    --------
    PARALLEL_TYPE, DEFAULT_PARALLEL, OMP_TYPE

    Examples
    --------
    >>> from timagetk.third_party.vt_parser import parallel_kwargs
    >>> parallel_kwargs()
    ' -parallel -parallel-type default'
    >>> parallel_kwargs(parallel_type='thread', n_job=8)
    ' -parallel -parallel-type thread -max-chunks 8'

    """
    str_param = ""
    # - Check OMP-scheduling:
    if omp_scheduling is not None and (parallel_type != "openmp" or parallel_type != "omp"):
        parallel_type = "openmp"

    try:
        assert parallel_type in PARALLEL_TYPE
    except AssertionError:
        msg = f"Parameter `parallel_type` should be in {PARALLEL_TYPE}, got {parallel_type}!"
        raise ValueError(msg)

    # - Parallelism options:
    if parallel and parallel_type != DEFAULT_PARALLEL:
        str_param += " -parallel"
        str_param += f" -parallel-type {parallel_type}"
        if n_job is not None:
            str_param += f" -max-chunks {n_job}"
        if omp_scheduling is not None:
            str_param += f" -omp-scheduling {omp_scheduling}"
    if not parallel:
        str_param += " -no-parallel"

    return str_param


# ------------------------------------------------------------------------------
# Template image keyword arguments
# ------------------------------------------------------------------------------

def template_kwargs(shape=None, voxelsize=None, **kwargs):
    """Keyword argument parser for VT template parameters.

    Parameters
    ----------
    shape : list, optional
        Shape of the template image.
    voxelsize : list, optional
        Voxelsize of the template image.

    Returns
    -------
    str
        Formatted "template parameters" for VT CLI.

    Examples
    --------
    >>> from timagetk.third_party.vt_parser import template_kwargs
    >>> template_kwargs([20, 50, 50], (0.5, 0.2 ,0.2))
    ' -template-dim 20 50 50 -template-voxel 0.5 0.2 0.2'

    """
    str_param = ""
    if shape is not None:
        str_param += " -template-dim {}".format(" ".join(map(str, shape)))
    if voxelsize is not None:
        str_param += " -template-voxel {}".format(" ".join(map(str, voxelsize)))

    return str_param


# ------------------------------------------------------------------------------
# Connectivity keyword arguments
# ------------------------------------------------------------------------------

#: Valid connectivity values for 2D structuring elements.
CONNECTIVITIES_2D = [4, 8]
#: Valid connectivity values for 3D structuring elements.
CONNECTIVITIES_3D = [6, 10, 18, 26]
CONNECTIVITIES = CONNECTIVITIES_2D + CONNECTIVITIES_3D
#: Default connectivity of the 2D structuring elements.
DEF_CONNECTIVITY_2D = 8
#: Default connectivity of the 3D structuring elements.
DEF_CONNECTIVITY_3D = 26
#: Default radius of the structuring elements.
DEF_RADIUS = 1
#: Default number of iterations during morphological operations.
DEF_ITERS = 1


def _check_connectivity(connectivity, ndim):
    """Check the given connectivity values exists."""
    msg = "Parameter `connectivity` should be in {}, got {}!"

    if connectivity is None:
        # Defines default connectivity values depending on the image number of dimensions
        if ndim == 2:
            connectivity = DEF_CONNECTIVITY_2D
        else:
            connectivity = DEF_CONNECTIVITY_3D
    else:
        if ndim == 2:
            try:
                assert connectivity in CONNECTIVITIES_2D
            except AssertionError:
                raise ValueError(msg.format(CONNECTIVITIES_2D, connectivity))
        else:
            try:
                assert connectivity in CONNECTIVITIES_3D
            except AssertionError:
                raise ValueError(msg.format(CONNECTIVITIES_3D, connectivity))
    return connectivity


def structuring_element_kwargs(ndim, radius=DEF_RADIUS, iterations=DEF_ITERS,
                               connectivity=None, sphere=False, **kwargs):
    """Keyword argument parser for VT structuring element parameters.

    Parameters
    ----------
    ndim : {2, 3}
        Dimensionality of the structuring element to create.
        Used to define default connectivity value.
    radius : int, optional
        Radius of the structuring element, default is ``DEF_RADIUS``
    iterations : int, optional
        Number of iterations to performs with this structuring element,
        ``DEF_ITERS`` by default.
    connectivity : int, optional
        Defines the connectivity of the structuring element, default value depend on dimensionality.
    sphere : bool, optional
        If ``True``,  the structuring element is the true euclidean sphere, equivalent to ``connectivity=18``
        ``False`` by default.

    Returns
    -------
    str
        Formatted "structuring element parameters" for VT CLI.

    Raises
    ------
    ValueError
        If ``connectivity`` is not in ``CONNECTIVITIES_2D`` for a 2D image, if not ``None``.
        If ``connectivity`` is not in ``CONNECTIVITIES_3D`` for a 3D image, if not ``None``.

    See Also
    --------
    DEF_RADIUS, DEF_ITERS
    CONNECTIVITIES_2D, CONNECTIVITIES_3D
    DEF_CONNECTIVITY_2D, DEF_CONNECTIVITY_3D

    Notes
    -----
    '-connectivity' parameter override '-sphere' parameter.

    Connectivity is among the 4-, 6-, 8-, 18-, 26-neighborhoods.
    ``4`` and ``8`` are for 2D images, the others for 3D images.

    Examples
    --------
    >>> from timagetk.third_party.vt_parser import structuring_element_kwargs
    >>> structuring_element_kwargs(2)  # for 2D images
    ' -radius 1 -iterations 1 -connectivity 8'
    >>> structuring_element_kwargs(3)  # for 3D images
    ' -radius 1 -iterations 1 -connectivity 26'

    """
    str_param = ""
    # - Control the radius of the structuring elements:
    str_param += f" -radius {radius}"
    # - Control the number of iterations during morphological operations
    str_param += f" -iterations {iterations}"

    # In VT '-connectivity' parameter override '-sphere' parameter:
    if sphere:
        str_param += " -sphere"
    else:
        # - Control the connectivity of the structuring elements:
        connectivity = _check_connectivity(connectivity, ndim)
        str_param += f" -connectivity {connectivity}"

    return str_param


# ------------------------------------------------------------------------------
# IMAGE AVERAGING keyword arguments
# ------------------------------------------------------------------------------

#: The list of available image averaging methods.
AVERAGING_METHODS = ["mean", "robust-mean", "median", "minimum", "maximum", "quantile", "sum", "var", "stddev"]
#: The default image averaging method.
DEF_AVG_METHOD = AVERAGING_METHODS[0]


def averaging_kwargs(method=None, **kwargs):
    """Keyword argument parser for ``vt.mean_images``.

    Set parameters default values and make sure they are of the right type.

    Parameters
    ----------
    method : {'mean', 'robust-mean', 'median', 'minimum', 'maximum', "quantile", "sum", "var", "stddev"}
        The image averaging method to use, should be in ``AVERAGING_METHODS``,
        ``DEF_AVG_METHOD`` by default.

    Other Parameters
    ----------------
    window : list
        Window size for processing, default is ``[1, 1(, 1)]``.
    quantile_value : float in [0, 1]
        Quantile of the retained value. See the "Notes" section for a detailled explanations.
    lts_fraction : float in [0, 1]
        Fraction of points to be kept for the computation of the robust mean (trimmed estimation).

    Returns
    -------
    str
        Formatted parameters for ``vt.mean_images`` CLI.
    dict
        Remaining keyword arguments after parsing.

    Raises
    ------
    ValueError
        If ``method`` is not in ``AVERAGING_METHODS``, if not ``None``.
        If ``lts_fraction`` is not defined, if ``method`` is ``'robust_mean'``.
        If ``lts_fraction`` is not a float in ``[0, 1]``, if ``method`` is ``'robust_mean'``.
        If ``quantile_value`` is not defined, if ``method`` is ``'quantile'``.
        If ``quantile_value`` is not a float in ``[0, 1]``, if ``method`` is ``'quantile'``.
        If ``window`` is not a length-2 (2D image) or length-3 (3D image), if not ``None``.

    See Also
    --------
    AVERAGING_METHODS, DEF_AVG_METHOD

    Notes
    -----
    Definition of available ``AVERAGING_METHODS`` values:

      - **mean**, compute the mean value for each voxel;
      - **robust-mean**, compute the trimmed mean keeping only given fraction of values for each voxel;
      - **median**, compute the median value for each voxel;
      - **minimum**, keep the minimum value for each voxel;
      - **maximum**, keep the maximum value for each voxel;
      - **quantile**, retain only the given ``quantile_value`` for each voxel;
      - **sum**, compute the sum for each voxel;
      - **var**, compute the variance for each voxel;
      - **stddev**, compute the standard deviation for each voxel.

    Explanations for ``quantile_value`` parameters:

      - 0:   minimum value, equivalent to "min" method
      - 0.5: median value,  equivalent to "median" method
      - 1:   maximum value, equivalent to "max" method

    Examples
    --------
    >>> from timagetk.third_party.vt_parser import averaging_kwargs
    >>> averaging_kwargs()[0]
    ' -mean'
    >>> averaging_kwargs("robust_mean", lts_fraction=0.3)[0]
    ' -robust_mean -lts-fraction 0.3'
    >>> averaging_kwargs("quantile", quantile_value=0.3)[0]
    ' -quantile -quantile-value 0.3'
    >>> averaging_kwargs(window=[1, 1, 1])[0]
    ' -mean -window 1 1 1'

    """
    str_param = ""

    # - Set / check given method is valid:
    method = _method_check(method, AVERAGING_METHODS, DEF_AVG_METHOD)
    str_param += f" -{method}"

    msg = "With the '{}' method you should define '{}' as a float in [0, 1]!"

    if method == "robust_mean":
        try:
            assert "lts_fraction" in kwargs.keys()
            lts_fraction = kwargs.pop("lts_fraction")
            assert isinstance(lts_fraction, float)
            assert 0 <= lts_fraction <= 1
        except AssertionError:
            raise ValueError(msg.format(method, 'lts_fraction'))
        str_param += f" -lts-fraction {lts_fraction}"
    elif method == "quantile":
        try:
            assert "quantile_value" in kwargs.keys()
            quantile_value = kwargs.pop("quantile_value")
            assert isinstance(quantile_value, float)
            assert 0 <= quantile_value <= 1
        except AssertionError:
            raise ValueError(msg.format(method, 'quantile_value'))
        str_param += f" -quantile-value {quantile_value}"

    window = kwargs.pop('window', None)
    if window is not None:
        try:
            assert isinstance(window, list)
            assert len(window) in [2, 3]
        except AssertionError:
            msg = "Parameter 'window' should be a list of same length than image dimension."
            raise ValueError(msg)
        str_param += f" -window {' '.join(map(str, window))}"

    return str_param, kwargs


# ------------------------------------------------------------------------------
# BLOCKMATCHING keyword arguments
# ------------------------------------------------------------------------------

#: List of valid blockmatching methods, to use with ``method`` in ``blockmatching`` from ``timagetk.algorithms.blockmatching``.
BLOCKMATCHING_METHODS = TRSF_TYPE.copy()
BLOCKMATCHING_METHODS.remove("null")
#: The default blockmatching method used by ``blockmatching`` from ``timagetk.algorithms.blockmatching``.
DEF_BM_METHOD = "rigid"
#: Default ``pyramid_lowest_level`` value.
DEF_PY_LL = 2
#: Default ``pyramid_highest_level`` value.
DEF_PY_HL = 5


def blockmatching_kwargs(method=DEF_BM_METHOD, **kwargs):
    """Keyword argument parser for ``vt.blockmatching``.

    Set parameters default values and make sure they are of the right type.

    Parameters
    ----------
    method : {'similitude', 'rigid', 'affine', 'vectorfield'}, optional
        The registration method to use, should be in ``BLOCKMATCHING_METHODS``,
        ``DEF_BM_METHOD`` by default.

    Other Parameters
    ----------------
    pyramid_lowest_level : int, optional
        Lowest level at which to compute deformation, default is ``DEF_PY_LL`` (min is 0).
    pyramid_highest_level : int, optional
        Highest level at which to compute deformation, default is ``DEF_PY_HL`` (max is 5).
    estimator : {'wlts', 'lts', 'wls', 'ls'}, optional
        Transformation estimator. 'wlts' by default.
    elastic_sigma : float, optional
        Transformation regularization, sigma for elastic regularization (only for vector field).
        This is equivalent to a gaussian filtering of the final deformation field
    fluid_sigma :float, optional
        Sigma for fluid regularization ie field interpolation and regularization for pairings (only for vector field)
        This is equivalent to a gaussian filtering of the incremental deformation field
    lts_fraction : float, optional
        If defined, set the fraction of pairs that are kept, used with trimmed estimations. ``0.5`` by default.
    lts_deviation : float, optional
        If defined, set the threshold to discard pairings, used with trimmed estimations.
        See the "Notes" section for a detailled explanations.
    lts_iterations : int, optional
        If defined, set the maximal number of iterations, used with trimmed estimations. ``100`` by default.
    pyramid_gaussian_filtering : bool, optional
        Before subsampling, the images are filtered (_*i.e.*_ smoothed) by a gaussian kernel. ``False`` by default
    floating_low_threshold : int, optional
        Intensity values inferior or equal to low threshold are not considered in block selection, minimum by default.
    floating_high_threshold : int, optional
        Intensity values superior or equal to high threshold are not considered in block selection, maximum by default.
    reference_low_threshold : int, optional
        Intensity values inferior or equal to low threshold are not considered in block selection, minimum by default.
    reference_high_threshold : int, optional
        Intensity values superior or equal to high threshold are not considered in block selection, maximum by default.

    Returns
    -------
    str
        Formatted parameters for ``vt.blockmatching`` CLI.
    dict
        Remaining keyword arguments after parsing.

    Raises
    ------
    ValueError
        If ``method`` is not in ``BLOCKMATCHING_METHODS`` (if not ``None``).

    See Also
    --------
    BLOCKMATCHING_METHODS, DEF_BM_METHOD
    DEF_PY_LL, DEF_PY_HL

    Notes
    -----
    Definitions of available ``estimator`` values:

     - **wlts**: weighted least trimmed squares (default);
     - **lts**: least trimmed squares;
     - **wls**: weighted least squares;
     - **ls**: least squares.

    Parameter ``lts_deviation`` is used in the formulae:
        ``threshold = average + lts_deviation * standard_deviation``


    Examples
    --------
    >>> from timagetk.third_party.vt_parser import blockmatching_kwargs
    >>> blockmatching_kwargs()[0]
    ' -trsf-type rigid -pyramid-lowest-level 2 -pyramid-highest-level 5'
    >>> blockmatching_kwargs(fluid_sigma=0.55)[0]
    ' -trsf-type rigid -pyramid-lowest-level 2 -pyramid-highest-level 5'
    >>> blockmatching_kwargs(method='vectorfield', fluid_sigma=0.55)[0]
    ' -trsf-type vectorfield -pyramid-lowest-level 2 -pyramid-highest-level 5 -fluid-sigma 0.55'
    >>> blockmatching_kwargs(method='vectorfield', floating_low_threshold=1, reference_low_threshold=1)[0]
    ' -trsf-type vectorfield -floating-low-threshold 1 -reference-low-threshold 1 -pyramid-lowest-level 2 -pyramid-highest-level 5'
    >>> blockmatching_kwargs(method='vectorfield', pyramid_gaussian_filtering=True)[0]
    ' -trsf-type vectorfield -pyramid-gaussian-filtering -pyramid-lowest-level 2 -pyramid-highest-level 5'
    >>> blockmatching_kwargs(method='vectorfield', estimator="wlts", lts_fraction=0.55)[0]
    ' -trsf-type vectorfield -pyramid-lowest-level 2 -pyramid-highest-level 5 -estimator wlts -lts-fraction 0.55'


    """
    str_param = ""
    # - Set / check given method is valid:
    method = _method_check(method, BLOCKMATCHING_METHODS, DEF_BM_METHOD)
    str_param += f" -trsf-type {method}"

    # Floating image, high and low threshold:
    flo_lt = kwargs.pop("floating_low_threshold", None)
    if flo_lt is not None:
        str_param += f" -floating-low-threshold {flo_lt}"
    flo_ht = kwargs.pop("floating_high_threshold", None)
    if flo_ht is not None:
        str_param += f" -floating-high-threshold {flo_ht}"
    # Reference image, high and low threshold:
    ref_lt = kwargs.pop("reference_low_threshold", None)
    if ref_lt is not None:
        str_param += f" -reference-low-threshold {ref_lt}"
    ref_ht = kwargs.pop("reference_high_threshold", None)
    if ref_ht is not None:
        str_param += f" -reference-high-threshold {ref_ht}"

    # - Pyramid building:
    if kwargs.pop("pyramid_gaussian_filtering", False):
        str_param += " -pyramid-gaussian-filtering"
    # By default 'pyramid_lowest_level' is equal to 2:
    py_ll = kwargs.pop("pyramid_lowest_level", DEF_PY_LL)
    str_param += f" -pyramid-lowest-level {py_ll}"
    # By default 'pyramid_highest_level' is equal to 5:
    py_hl = kwargs.pop("pyramid_highest_level", DEF_PY_HL)
    str_param += f" -pyramid-highest-level {py_hl}"
    # TODO: make shorter version of param names available ? *i.e.* py_ll & py_hl ?

    estimator = kwargs.pop("estimator", None)
    if estimator is not None:
        try:
            assert estimator in ('wlts', 'lts', 'wls', 'ls')
        except AssertionError:
            log.warning(f"Wrong transformation estimator '{estimator}', should be in ['wlts', 'lts', 'wls', 'ls']")
            estimator = None  # fallback to default estimator (ls)
            log.warning("Falling back to default least-square transformation estimator!")
        else:
            str_param += f" -estimator {estimator}"
    # Trimmed estimators:
    if estimator in ["wlts", "lts"]:
        try:
            assert "lts_fraction" in kwargs.keys() or "lts_deviation" in kwargs.keys()
        except AssertionError:
            log.warning("Missing trimming parameter 'lts_fraction' or 'lts_deviation'!")
            log.warning("Falling back to default least-square transformation estimator!")
        else:
            lts_fraction = kwargs.pop("lts_fraction", None)
            if lts_fraction is not None:
                str_param += f" -lts-fraction {lts_fraction}"
            lts_deviation = kwargs.pop("lts_deviation", None)
            if lts_deviation is not None:
                str_param += f" -lts-deviation {lts_deviation}"
            lts_iterations = kwargs.pop("lts_iterations", None)
            if lts_iterations is not None:
                str_param += f" -lts-iterations {lts_iterations}"

    elastic_sigma = kwargs.pop("elastic_sigma", None)
    if elastic_sigma is not None:
        if method != 'vectorfield':
            log.warning(f"Unused parameter 'elastic_sigma' with '{method}' transformation!")
        else:
            str_param += f" -elastic-sigma {elastic_sigma}"

    fluid_sigma = kwargs.pop("fluid_sigma", None)
    if fluid_sigma is not None:
        if method != 'vectorfield':
            log.warning(f"Unused parameter 'fluid_sigma' with '{method}' transformation!")
        else:
            str_param += f" -fluid-sigma {fluid_sigma}"

    return str_param, kwargs


# ------------------------------------------------------------------------------
# POINTMATCHING keyword arguments
# ------------------------------------------------------------------------------

#: List of valid pointmatching estimator methods, to use with ``method``.
PM_ESTIMATORS = ["wlts", "lts", "wls", "ls"]
#: Default pointmatching estimator method.
DEF_PM_ESTIMATOR = PM_ESTIMATORS[-1]
#: List of valid pointmatching propagation methods, to use with ``vector_propagation_type`` keyword argument.
PROPAGATION_TYPES = ["direct", "skiz"]
#: Default pointmatching propagation method.
DEF_PROPAGATION_TYPE = PROPAGATION_TYPES[1]


def pointmatching_kwargs(method=DEF_BM_METHOD, **kwargs):
    """Keyword argument parser for VT `pointmatching` parameters.

    Set parameters default values and make sure they are of the right type.

    Parameters
    ----------
    method : {"wlts", "lts", "wls", "ls"}
        The estimation method to use, should be in ``PM_ESTIMATORS``, ``DEF_PM_ESTIMATOR`` by default.

    Other Parameters
    ----------------
    fluid_sigma : float or list of float
        Sigma for fluid regularization, *i.e.* field interpolation and regularization for pairings (only for vector field)
    vector_propagation_distance : float
        Defines the propagation distance of initial pairings (*i.e.* displacements).
        This implies the same displacement for the spanned sphere.
        Restricted to vector-field. Distance is in world units (not voxels).
    vector_fading_distance : float
        Area of fading for initial pairings (*i.e.* displacements).
        This allows progressive transition towards null displacements and thus avoid discontinuites.
        Restricted to vector-field. Distance is in world units (not voxels).
    vector_propagation_type : {"direct", "skiz"}
        Defines how vector are propagated, should be in ``PROPAGATION_TYPES``, ``DEF_PROPAGATION_TYPE`` by default.
        See notes for more details.
    lts_fraction : float
        If defined, set the fraction of pairs that are kept, used with trimmed estimations.
    lts_deviation : float
        If defined, set the threshold to discard pairings, used with trimmed estimations.
        See the "Notes" section for a detailled explanations.
    lts_iterations : int
        If defined, set the maximal number of iterations, used with trimmed estimations.
    real : bool, optional
        If ``True`` (default), given points are in real units else in voxels.

    Returns
    -------
    str
        Formatted parameters for ``vt.pointmatching`` CLI.
    dict
        Remaining keyword arguments after parsing.

    Raises
    ------
    AssertionError
        If ``method`` is not in ``BLOCKMATCHING_METHODS``, if not ``None``.
        If ``estimator_type`` is not in ``PM_ESTIMATORS``, if not ``None``.
        If "fluid_sigma", "vector_propagation_distance" and "vector_fading_distance" are not defined when using "vectorield" as ``method``.
        If "lts_fraction" or "lts_deviation" are not defined when using "wlts" or "lts" as ``method``.

    See Also
    --------
    BLOCKMATCHING_METHODS, DEF_BM_METHOD
    PM_ESTIMATORS, DEF_PM_ESTIMATOR
    PROPAGATION_TYPES, DEF_PROPAGATION_TYPE

    Notes
    -----
    Definitions of available ``PM_ESTIMATORS`` values:

     - **wlts**: weighted least trimmed squares;
     - **lts**: least trimmed squares;
     - **wls**: weighted least squares;
     - **ls**: least squares.

    Parameter ``lts_deviation`` is used in the formulae:
        ``threshold = average + lts_deviation * standard_deviation``

    Definitions of available ``PROPAGATION_TYPES`` values:

     - **direct**: exact propagation (but slow);
     - **skiz**: approximate propagation (but faster).

    Examples
    --------
    >>> # Calling the keyword argument parser function you can get the default parameters:
    >>> from timagetk.third_party.vt_parser import pointmatching_kwargs
    >>> pointmatching_kwargs()[0]
    ' -trsf-type rigid -estimator-type ls -unit real'
    >>> pointmatching_kwargs("vectorfield", fluid_sigma=5.0, vector_propagation_distance=20.0, vector_fading_distance=3.0)[0]
    ' -trsf-type vectorfield -fluid-sigma 5.0 -vector-propagation-distance 20.0 -vector-fading-distance 3.0 -estimator-type ls -unit real'
    >>> pointmatching_kwargs("vectorfield", fluid_sigma=5.0, vector_propagation_distance=20.0, vector_fading_distance=3.0, estimator_type="lts", lts_deviation=1.5)[0]
    ' -trsf-type vectorfield -fluid-sigma 5.0 -vector-propagation-distance 20.0 -vector-fading-distance 3.0 -estimator-type lts -lts-deviation 1.5 -unit real'

    """
    str_param = ""

    # - Set / check given ``method`` is valid:
    method = _method_check(method, BLOCKMATCHING_METHODS, DEF_BM_METHOD)
    str_param += f" -trsf-type {method}"

    if "vectorfield" in method:
        assert "fluid_sigma" in kwargs.keys()
        assert "vector_propagation_distance" in kwargs.keys()
        assert "vector_fading_distance" in kwargs.keys()
        str_param += f" -fluid-sigma {kwargs['fluid_sigma']}"
        str_param += f" -vector-propagation-distance {kwargs['vector_propagation_distance']}"
        str_param += f" -vector-fading-distance {kwargs['vector_fading_distance']}"

    # - Set / check given ``estimator_type`` is valid:
    propagation = kwargs.get("vector_propagation_type", DEF_PROPAGATION_TYPE)
    propagation = _method_check(propagation, PROPAGATION_TYPES, DEF_PROPAGATION_TYPE)
    str_param += f" -vector-propagation-type {propagation}"

    # - Set / check given ``estimator_type`` is valid:
    estimator = kwargs.get("estimator_type", DEF_PM_ESTIMATOR)
    estimator = _method_check(estimator, PM_ESTIMATORS, DEF_PM_ESTIMATOR)

    str_param += f" -estimator-type {estimator}"
    # Trimmed estimators:
    if estimator in ["wlts", "lts"]:
        # assert "lts_fraction" in kwargs.keys() or "lts_deviation" in kwargs.keys()
        lts_fraction = kwargs.pop("lts_fraction", None)
        if lts_fraction is not None:
            str_param += f" -lts-fraction {lts_fraction}"
        lts_deviation = kwargs.pop("lts_deviation", None)
        if lts_deviation is not None:
            str_param += f" -lts-deviation {lts_deviation}"
        lts_iterations = kwargs.pop("lts_iterations", None)
        if lts_iterations is not None:
            str_param += f" -lts-iterations {lts_iterations}"

    # - Parse real/voxel units:
    if kwargs.get("real", True):
        str_param += " -unit real"
    else:
        str_param += " -unit voxel"

    return str_param, kwargs


def cellfilter_kwargs(**kwargs):
    """Keyword argument parser for `vt.cellfilter`.

    Set parameters default values and make sure they are of the right type.

    Returns
    -------
    str
        Formatted parameters for ``vt.mean_images`` CLI.
    dict
        Remaining keyword arguments after parsing.

    Raises
    ------
    ValueError
        If ``connectivity`` is not in ``CONNECTIVITIES_2D`` for a 2D image, if not ``None``.
        If ``connectivity`` is not in ``CONNECTIVITIES_3D`` for a 3D image, if not ``None``.

    See Also
    --------
    structuring_element_kwargs

    Notes
    -----
    Have a look at ``structuring_element_kwargs`` for more keyword arguments.

    Examples
    --------
    >>> # Calling the keyword argument parser function you can get the default parameters:
    >>> from timagetk.third_party.vt_parser import cellfilter_kwargs
    >>> cellfilter_kwargs(ndim=2)[0]  # for 2D images
    ' -radius 1 -iterations 1 -connectivity 8'
    >>> cellfilter_kwargs(ndim=3)[0]  # for 3D images
    ' -radius 1 -iterations 1 -connectivity 26'

    """
    str_param = ""
    # - Control the structuring element:
    str_param += structuring_element_kwargs(**kwargs)
    return str_param, kwargs


#: Default low threshold for ``timagetk.algorithms.connexe.connexe()`` method, to use with ``low_threshold`` keyword argument.
DEF_LOW_THRESHOLD = None
#: Default high threshold for ``connexe`` method, to use with ``high_threshold`` keyword argument.
DEF_HIGH_THRESHOLD = None


def connexe_kwargs(**kwargs):
    """Keyword argument parser for ``vt.connexe``.

    Set parameters default values and make sure they are of the right type.

    Other Parameters
    ----------------
    low_threshold : int or float
        Low threshold to binarize input image.
    high_threshold : int or float
        High threshold to binarize input image.
    max : bool
        If ``True`` keep the largest connected component (implies binary output)
        Else all detected components are kept. Equivalent to 'max_number_cc=1'.
    min_size_cc : int
        Minimal size, in voxels, of the connected component.
        Otherwise, all detected components are kept by default.
    max_number_cc : int
        Maximal number of connected components to keep, the largest are kept first.
        Otherwise, all detected components are kept by default.
    connectivity : {4, 6, 8, 10, 18, 26}
        The connectivity of the structuring elements, see ``structuring_element_kwargs``.
    binary_output : bool
        If ``True`` all valid connected components have the same value (1).
        Else each detected components have their own label (default).
    label : bool
        If ``True`` (default) returns one label per connected component.
    size : bool
        If ``True`` the value attributed to points of a connected component is the size of the connected components
        This allows to select w.r.t to size afterwards.
    sort_sizes : bool
        If ``True`` the labels are ordered by decreasing size of connected  components, implies ``label=True`` (unless changed afterwards).

    Returns
    -------
    str
        Formatted parameters for ``vt.connexe`` CLI.
    dict
        Remaining keyword arguments after parsing.

    Raises
    ------
    ValueError
        If ``connectivity`` is not in ``CONNECTIVITIES_2D`` for a 2D image, if not ``None``.
        If ``connectivity`` is not in ``CONNECTIVITIES_3D`` for a 3D image, if not ``None``.

    See Also
    --------
    DEF_LOW_THRESHOLD, DEF_HIGH_THRESHOLD

    Notes
    -----
    Have a look at ``structuring_element_kwargs`` for more keyword arguments.

    Examples
    --------
    >>> # Calling the keyword argument parser function you can get the default parameters:
    >>> from timagetk.third_party.vt_parser import connexe_kwargs
    >>> connexe_kwargs(ndim=2)[0]  # for 2D images
    ' -connectivity 8 -labels'
    >>> connexe_kwargs(ndim=3)[0]  # for 3D images
    ' -connectivity 26 -labels'
    >>> connexe_kwargs(ndim=3, max=True)[0]

    """
    str_param = ""
    low_threshold = kwargs.pop("low_threshold", DEF_LOW_THRESHOLD)
    if low_threshold is not None:
        low_threshold = abs(int(low_threshold))
        str_param += f" -low-threshold {low_threshold}"
    high_threshold = kwargs.pop("high_threshold", DEF_HIGH_THRESHOLD)
    if high_threshold is not None:
        high_threshold = abs(int(high_threshold))
        str_param += f" -high-threshold {high_threshold}"

    # - Control the structuring element connectivity:
    connectivity = kwargs.pop("connectivity", None)
    ndim = kwargs.pop("ndim", None)
    connectivity = _check_connectivity(connectivity, ndim)
    str_param += f" -connectivity {connectivity}"

    min_size_cc = kwargs.pop("min_size_cc", False)
    if min_size_cc:
        str_param += f" -min-size-cc {min_size_cc}"
    max_number_cc = kwargs.pop("max_number_cc", False)
    if max_number_cc:
        str_param += f" -max-number-cc {max_number_cc}"

    # -- BOOLEAN parameters:
    get_max = kwargs.pop("max", False)
    if get_max:
        str_param += " -max"
    if kwargs.pop("binary_output", False):
        str_param += " -binary-output"
    if kwargs.pop("label", True) and not get_max:
        str_param += " -labels"
    if kwargs.pop("size", False):
        str_param += " -size"
    if kwargs.pop("sort-sizes", False):
        str_param += " -sort-sizes"

    return str_param, kwargs


#: List of valid regional extrema methods, to use with ``method`` in ``regionalext`` from ``timagetk.algorithms.regionalext``.
REGIONALEXT_METHODS = ["minima", "maxima"]
#: The default regional extrema method used by ``regionalext`` from ``timagetk.algorithms.regionalext``.
DEF_RE_METHOD = REGIONALEXT_METHODS[0]


def regionalext_kwargs(height, method=DEF_RE_METHOD, **kwargs):
    """Keyword argument parser for ``vt.regionalext``.

    Set parameters default values and make sure they are of the right type.

    Parameters
    ----------
    height : int
        The height of the extrema to detect.
    method : {'minima', 'maxima'}, optional
        The method to use for regional extrema detection, should be in
        ``REGIONALEXT_METHODS``, ``DEF_RE_METHOD`` by default.

    Other Parameters
    ----------------
    connectivity : {4, 6, 8, 10, 18, 26}
        The connectivity of the structuring elements, see
        ``structuring_element_kwargs``.

    Returns
    -------
    str
        Formatted parameters for ``vt.regionalext`` CLI.
    dict
        Remaining keyword arguments after parsing.

    Raises
    ------
    ValueError
        If ``method`` is not in ``REGIONALEXT_METHODS``, if not ``None``.
        If ``connectivity`` is not in ``CONNECTIVITIES_2D`` for a 2D image, if not ``None``.
        If ``connectivity`` is not in ``CONNECTIVITIES_3D`` for a 3D image, if not ``None``.

    Examples
    --------
    >>> # Calling the keyword argument parser function you can get the default parameters:
    >>> from timagetk.third_party.vt_parser import regionalext_kwargs
    >>> regionalext_kwargs(ndim=2)[0]  # for 2D images
    ' -minima -height 3 -radius 1 -iterations 1 -connectivity 8'
    >>> regionalext_kwargs(ndim=3)[0]  # for 3D images
    ' -minima -height 3 -radius 1 -iterations 1 -connectivity 26'

    """
    str_param = ""
    # - Set / check given method is valid:
    if "min" in method:
        method = 'minima'
    if "max" in method:
        method = 'maxima'
    method = _method_check(method, REGIONALEXT_METHODS, DEF_RE_METHOD)
    str_param += f" -{method}"

    str_param += f" -height {height}"
    # - Control the structuring element connectivity:
    connectivity = kwargs.pop("connectivity", None)
    ndim = kwargs.pop("ndim", None)
    connectivity = _check_connectivity(connectivity, ndim)
    str_param += f" -connectivity {connectivity}"

    return str_param, kwargs


#: List of valid filtering methods, to use with ``method`` in ``linearfilter`` from ``timagetk.algorithms.linearfilter``.
FILTERING_METHODS = [
    "smoothing", "gradient", "hessian", "laplacian",
    "zero-crossings-hessian", "zero-crossings-laplacian",
    "gradient-hessian", "gradient-laplacian", "gradient-extrema",
]
#: The default filtering method used by ``linearfilter`` from ``timagetk.algorithms.linearfilter``.
DEF_F_METHOD = "smoothing"

#: List of valid gaussian implementations, to use with ``gaussian_type`` keyword argument in ``linearfilter`` from ``timagetk.algorithms.linearfilter``.
#: Requires prior selection of 'smoothing' as ``method``.
GAUSSIAN_IMPLEMENTATION = ["deriche", "fidrich", "young-1995", "young-2002", "gabor-young-2002", "convolution"]


def linfilter_kwargs(method=DEF_F_METHOD, **kwargs):
    """Keyword argument parser for ``vt.linear_filter``.

    Set parameters default values and make sure they are of the right type.

    Parameters
    ----------
    method : str, optional
        The method to use for linear filtering, should be in ``FILTERING_METHODS``, ``DEF_F_METHOD`` by default.

    Other Parameters
    ----------------
    sigma : float or list of float, optional
        The sigma to apply for every axis if a float, for each axis if a list of floats
    real : bool, optional
        Define if the ``sigma`` to apply is in voxel (default) or real units
    gaussian_type : str, optional
        Implementation to use for gaussian smoothing.
        Should be in ``GAUSSIAN_IMPLEMENTATION``.
    positive : bool, optional
        If ``True`` (default), use a positive zero-crossing method.
        Requires ``method`` to be set to ``zero-crossings-*``.
    negative : bool, optional
        If ``True`` (default is ``False``), use a negative zero-crossing method.
        Requires ``method`` to be set to ``zero-crossings-*``.

    Returns
    -------
    str
        Formatted parameters for ``vt.linear_filter`` CLI.
    dict
        Remaining keyword arguments after parsing.

    Raises
    ------
    ValueError
        If ``method`` is not in ``FILTERING_METHODS``, if not ``None``.

    See Also
    --------
    FILTERING_METHODS, DEF_F_METHOD, GAUSSIAN_IMPLEMENTATION

    Examples
    --------
    >>> # Calling the keyword argument parser function you can get the default parameters:
    >>> from timagetk.third_party.vt_parser import linfilter_kwargs
    >>> linfilter_kwargs()[0]
    ' -smoothing -unit real -sigma 1.0'
    >>> linfilter_kwargs("smoothing", sigma=0.5)[0]
    ' -smoothing -real-sigma 0.5'
    >>> linfilter_kwargs("smoothing", sigma=2.0, real=False)[0]
    ' -smoothing -voxel-sigma 2.0'

    """
    str_param = ""
    # - Set / check given method is valid:
    method = _method_check(method, FILTERING_METHODS, DEF_F_METHOD)
    str_param += f" -{method}"

    # - Parse list of floats or float for sigma:
    sigma = kwargs.pop("sigma", 1.0)
    # - Parse real/voxel units:
    real = kwargs.pop('real', True)

    if isinstance(sigma, (list, tuple, np.ndarray)):
        str_param += f" -{'real' if real else 'voxel'}-sigma " + " ".join([str(abs(float(s))) for s in sigma])
    else:
        str_param += f" -{'real' if real else 'voxel'}-sigma {float(sigma)}"

    # Defines gaussian implementation if required:
    gaussian_type = kwargs.pop('gaussian_type', None)
    if gaussian_type is not None:
        if method == "smoothing":
            str_param += f" -gaussian-type {gaussian_type}"
        else:
            log.warning("Specifying `gaussian_type` is only useful with `method='smoothing'`!")
            log.info(f"Selected method is '{method}'.")

    # Set "positive=True" by default when selecting a "zero-crossing" method:
    if "zero-crossings" in method:
        if "positive" not in kwargs.keys() and "negative" not in kwargs.keys():
            kwargs.update({"positive": True})
    # Make sure both "positive" and "negative" have not been set to True:
    if kwargs.get("positive", False) and kwargs.get("negative", False):
        raise ValueError(
            "Please choose either `positive=True` OR `negative=True` for 'zero-crossings-*' methods!")
    # Parse positive or negative
    if kwargs.pop("positive", False):
        str_param += ' -pos'
    if kwargs.pop("negative", False):
        str_param += ' -neg'

    if kwargs.pop("2D", False):
        str_param += ' -2D'

    if kwargs.pop("edges", False):
        str_param += ' -edges'

    return str_param, kwargs


#: List of valid morphological operations, to use with ``method`` in ``morphology`` from ``timagetk.algorithms.morphology``.
INTENSITY_MORPHOLOGY_METHODS = [
    "dilation", "erosion", "closing", "opening",
    "hat-closing", "closing-hat", "hclo", "hfer",
    "hat-opening", "opening-hat", "hope", "houv",
    "contrast", "gradient",
    "oc_alternate_sequential_filter", "co_alternate_sequential_filter",
    "coc_alternate_sequential_filter", "oco_alternate_sequential_filter",
]

#: List of valid morphological operations, to use with ``method`` in ``label_filtering`` from ``timagetk.algorithms.morphology``.
LABELLED_MORPHOLOGY_METHODS = [
    "dilation", "erosion", "closing", "opening",
    "oc_alternate_sequential_filter", "co_alternate_sequential_filter",
    "coc_alternate_sequential_filter", "oco_alternate_sequential_filter",
]

#: The default morphological operation used by ``morphology`` & ``label_filtering`` from ``timagetk.algorithms.morphology``.
DEF_MORPHO_METHOD = 'dilation'


def morphology_kwargs(method=DEF_MORPHO_METHOD, methods_list=INTENSITY_MORPHOLOGY_METHODS, **kwargs):
    """Keyword argument parser for ``vt.morpho``.

    Set parameters default values and make sure they are of the right type.

    Parameters
    ----------
    method : str, optional
        The method to use for morphological filtering, should be in
         ``INTENSITY_MORPHOLOGY_METHODS``, ``DEF_MORPHO_METHOD`` by default.
    methods_list : list, optional
        List of valid methods.

    Other Parameters
    ----------------
    connectivity : {4, 6, 8, 10, 18, 26}
        The connectivity of the structuring elements, see ``structuring_element_kwargs``.

    Returns
    -------
    str
        Formatted parameters for ``vt.morpho`` CLI.
    dict
        Remaining keyword arguments after parsing.

    Raises
    ------
    ValueError
        If ``method`` is not in ``INTENSITY_MORPHOLOGY_METHODS``, if not ``None``.

    See Also
    --------
    INTENSITY_MORPHOLOGY_METHODS, DEF_MORPHO_METHOD, structuring_element_kwargs

    Notes
    -----
    Have a look at ``structuring_element_kwargs`` for more keyword arguments.

    Examples
    --------
    >>> # Calling the keyword argument parser function you can get the default parameters:
    >>> from timagetk.third_party.vt_parser import morphology_kwargs
    >>> morphology_kwargs(ndim=2)[0]  # for 2D images
    ' -dilation -radius 1 -iterations 1 -connectivity 8'
    >>> morphology_kwargs(ndim=3)[0]  # for 3D images
    ' -dilation -radius 1 -iterations 1 -connectivity 26'

    """
    str_param = ""
    # - Set / check given method is valid:
    method = _method_check(method, methods_list, DEF_MORPHO_METHOD)
    str_param += f" -{method}"

    # - Control the structuring element:
    str_param += structuring_element_kwargs(**kwargs)

    return str_param, kwargs


#: List of valid labelling conflict resolution methods, to use with ``labelchoice`` in ``watershed`` from ``timagetk.algorithms.watershed``.
LABELCHOICE_METHODS = ["first", "min", "most"]
#: The default labelling conflict resolution method used by ``watershed`` from ``timagetk.algorithms.watershed``.
DEF_LABELCHOICE = "first"


def watershed_kwargs(**kwargs):
    """Keyword argument parser for ``vt.watershed``.

    Set parameters default values and make sure they are of the right type.

    Other Parameters
    ----------------
    labelchoice : {"first", "min", "most"}, optional
        How to deal with "labels conflicts", *i.e.* where several labels meet.
        Should be in ``LABELCHOICE_METHODS``, ``DEF_LABELCHOICE`` by default.
        See the "Notes" section for a detailled explanations.
    max_iterations : int, optional
        Set a maximal number of iterations, stop the algorithm before convergence.
        ``0`` by default, *i.e.* wait for convergence.

    Returns
    -------
    str
        Formatted parameters for ``vt.watershed`` CLI.
    dict
        Remaining keyword arguments after parsing.

    Raises
    ------
    ValueError
        If ``labelchoice`` is not in ``LABELCHOICE_METHODS``, if not ``None``.

    See Also
    --------
    LABELCHOICE_METHODS, DEF_LABELCHOICE

    Notes
    -----
    Explanations of available ``LABELCHOICE_METHODS`` values:

     - **first**: the first label wins;
     - **min**: the less represented label wins;
     - **most**: the most represented label wins;

    Examples
    --------
    >>> # Calling the keyword argument parser function you can get the default parameters:
    >>> from timagetk.third_party.vt_parser import watershed_kwargs
    >>> watershed_kwargs()[0]
    ' -labelchoice first'

    """
    str_param = ""

    labelchoice = kwargs.pop("labelchoice", None)
    labelchoice = _method_check(labelchoice, LABELCHOICE_METHODS, DEF_LABELCHOICE)
    str_param += f" -labelchoice {labelchoice}"

    max_iterations = kwargs.pop("max_iterations", 0)
    if max_iterations > 0:
        str_param += f" -iterations {max_iterations}"

    return str_param, kwargs


#: List of valid interpolation methods for grayscale images, to use with ``method`` in ``apply_trsf`` from ``timagetk.algorithms.trsf``.
GRAY_INTERPOLATION_METHODS = ["linear", "cspline", "nearest"]
#: List of valid interpolation methods for labelled images, to use with ``method`` in ``apply_trsf`` from ``timagetk.algorithms.trsf``.
LABEL_INTERPOLATION_METHODS = ["cellbased", "nearest"]
#: List of all valid interpolation methods, to use with ``method`` in ``apply_trsf`` from ``timagetk.algorithms.trsf``.
INTERPOLATION_METHODS = list(set(GRAY_INTERPOLATION_METHODS) | set(LABEL_INTERPOLATION_METHODS))
#: The default interpolation method for grayscale images used by ``apply_trsf`` from ``timagetk.algorithms.trsf``.
DEF_GRAY_INTERP_METHOD = GRAY_INTERPOLATION_METHODS[0]
#: The default interpolation method for labelled images used by ``apply_trsf`` from ``timagetk.algorithms.trsf``.
DEF_LABEL_INTERP_METHOD = LABEL_INTERPOLATION_METHODS[0]
#: The default cell_based_sigma value:
DEF_CELL_BASED_SIGMA = 2


def apply_trsf_kwargs(image, method=None, **kwargs):
    """Keyword argument parser for `vt.apply_trsf`.

    Set parameters default values and make sure they are of the right type.

    Parameters
    ----------
    image : timagetk.SpatialImage or timagetk.LabelledImage
        Image to transform.
        The type is used to define default interpolation method.
    method : {'linear', 'cspline', 'cellbased', 'nearest'}, optional
        The interpolation method to use for image resampling.
        It depends on the type of image to resample, see ``interpolate_kwargs``.

    Other Parameters
    ----------------
    isotropic_voxel : float
        If set, ignore given ``trsf`` & ``template_img`` and performs isometric resampling to given voxelsize.
    cell_based_sigma : float
        Required when using "cellbased" interpolation method, sigma for cell-based interpolation. In voxel units!

    Returns
    -------
    str
        Formatted parameters for ``vt.apply_trsf`` CLI.
    dict
        Remaining keyword arguments after parsing.

    Raises
    ------
    ValueError
        If ``cell_based_sigma`` is not defined when using ``'cellbased'`` method.

    See Also
    --------
    timagetk.third_party.vt_parser.GRAY_INTERPOLATION_METHODS
    timagetk.third_party.vt_parser.LABEL_INTERPOLATION_METHODS
    timagetk.third_party.vt_parser.DEF_GRAY_INTERP_METHOD
    timagetk.third_party.vt_parser.DEF_LABEL_INTERP_METHOD
    timagetk.third_party.vt_parser.DEF_CELL_BASED_SIGMA
    timagetk.third_party.vt_parser.interpolate_kwargs

    Notes
    -----
    Have a look at ``interpolate_kwargs`` for more keyword arguments.

    Examples
    --------
    >>> # Calling the keyword argument parser function you can get the default parameters:
    >>> from timagetk.array_util import random_spatial_image
    >>> from timagetk.array_util import dummy_labelled_image_3D
    >>> from timagetk.third_party.vt_parser import apply_trsf_kwargs
    >>> apply_trsf_kwargs(random_spatial_image((3,5,5)))[0]
    ' -interpolation linear'
    >>> apply_trsf_kwargs(random_spatial_image((3,5,5)), isotropic_voxel=0.3)[0]
    ' -isotropic-voxel 0.3 -interpolation linear'
    >>> apply_trsf_kwargs(dummy_labelled_image_3D((0.2, 0.5, 0.5)), isotropic_voxel=0.3)[0]
    ' -isotropic-voxel 0.3 -interpolation cellbased -cell-based-sigma 2'

    """
    str_param = ""

    isotropic_voxel = kwargs.pop("isotropic_voxel", None)
    if isotropic_voxel is not None:
        str_param += f" -isotropic-voxel {isotropic_voxel}"

    if method is None:
        method = default_interpolation_method(image)
    str_param += interpolate_kwargs(image, method)

    if method == "cellbased":
        cell_based_sigma = kwargs.get("cell_based_sigma", DEF_CELL_BASED_SIGMA)
        # Warn if the `cell_based_sigma` parameter was missing:
        try:
            assert "cell_based_sigma" in kwargs.keys()
        except AssertionError:
            msg = f"Missing keyword argument `cell_based_sigma` for interpolation method {method}!"
            log.warning(msg)
            log.info(f"Using default `cell_based_sigma` value ({DEF_CELL_BASED_SIGMA}).")
        # Check if the `cell_based_sigma` parameter value is acceptable:
        try:
            assert cell_based_sigma >= 1
        except AssertionError:
            msg = f"Argument `cell_based_sigma` should be greater than or equal to 1, got '{cell_based_sigma}'!"
            log.warning(msg)
            log.info(f"Using default `cell_based_sigma` value ({DEF_CELL_BASED_SIGMA}).")
            cell_based_sigma = DEF_CELL_BASED_SIGMA
        # Append the `cell_based_sigma` parameter
        str_param += f" -cell-based-sigma {cell_based_sigma}"

    return str_param, kwargs


def inv_trsf_kwargs(**kwargs):
    """Keyword argument parser for ``vt.inv_trsf``.

    Set parameters default values and make sure they are of the right type.

    Other Parameters
    ----------------
    error : float
        Absolute error (in real world unit) to determine convergence.
    iteration : int
        Maximal number of iterations to reach convergence.

    Returns
    -------
    str
        Formatted parameters for ``vt.inv_trsf`` CLI.
    dict
        Remaining keyword arguments after parsing.

    Examples
    --------
    >>> # Calling the keyword argument parser function you can get the default parameters:
    >>> from timagetk.third_party.vt_parser import inv_trsf_kwargs
    >>> inv_trsf_kwargs()[0]
    ''
    >>> inv_trsf_kwargs(error=0.03)[0]
    ' -inversion-error 0.03'
    >>> inv_trsf_kwargs(iteration=30)[0]
    ' -inversion-iteration 30'

    """
    str_param = ""
    error = kwargs.pop("error", None)
    if error is not None:
        str_param += f" -inversion-error {error}"

    iteration = kwargs.pop("iteration", None)
    if iteration is not None:
        str_param += f" -inversion-iteration {iteration}"

    return str_param, kwargs


#: List of valid transformation creation methods, to use with ``trsf`` in ``create_trsf`` from ``timagetk.algorithms.trsf``.
CREATE_TRSF = ["identity", "random", "sinus"]
#: The default regional extrema method used by ``create_trsf`` from ``timagetk.algorithms.trsf``.
DEF_CREATE_TRSF = CREATE_TRSF[0]


def create_trsf_kwargs(method=DEF_CREATE_TRSF, **kwargs):
    """Keyword argument parser for ``vt.create_trsf``.

    Set parameters default values and make sure they are of the right type.

    Parameters
    ----------
    method : {"identity", "random", "sinus"}
        The generation method to use for transformation creation, should be in
        ``CREATE_TRSF``.

    Other Parameters
    ----------------
    trsf_type : {"rigid", "affine", "vectorfield"}
        Type of transformation to create, default is ``affine``.
    seed : int
        Seed use in random generator, can be useful to reproduce some tests.
        Limited to "rigid" & "affine" tranformations.
    angle_range : [float, float]
        Angle range of the linear tranformation, expressed in radian.
        Limited to "rigid" & "affine" tranformations.
    translation_range : [float, float]
        Angle range of the linear tranformation, expressed in ??? FIXME.
        Limited to "rigid" & "affine" tranformations.
    shear_range : [float, float]
        Shear range of the linear tranformation, expressed in ??? FIXME.
        Limited to "affine" tranformations.
    scale_range : [float, float]
        Scale range of the linear tranformation, expressed in ??? FIXME.
        Limited to "affine" tranformations.
    sinusoid_amplitude : [float, [float, [float]]]
        Sinusoid amplitude of the non-linear tranformation, expressed in radian.
        Limited to "vectorfield" tranformations.
    sinusoid_period : [float, [float, [float]]]
        Sinusoid period of the non-linear tranformation, expressed in radian.
        Limited to "vectorfield" tranformations.

    Returns
    -------
    str
        Formatted parameters for ``vt.create_trsf`` CLI.
    dict
        Remaining keyword arguments after parsing.

    Raises
    ------
    ValueError
        If ``method`` not in ``CREATE_TRSF``, if not ``None``.
    TypeError
        If ``trsf_type``

    See Also
    --------
    CREATE_TRSF, DEF_CREATE_TRSF
    timagetk.components.trsf.TRSF_TYPE

    Examples
    --------
    >>> # Calling the keyword argument parser function you can get the default parameters:
    >>> from timagetk.third_party.vt_parser import create_trsf_kwargs
    >>> create_trsf_kwargs()[0]
    ' -transformation-value identity -trsf-type affine'

    """
    str_param = ""

    # - Parse `method` parameter:
    try:
        assert method in CREATE_TRSF
    except AssertionError:
        msg = "Input `trsf` should be a string in '{}', got '{}'!"
        raise ValueError(msg.format(CREATE_TRSF, method))
    else:
        if method == 'sinus':
            if kwargs['ndim'] == 2:
                method = 'sinus2D'
            else:
                method = 'sinus3D'
        str_param += " -transformation-value {}".format(method)

    # - Parse `trsf_type` keyword argument:
    trsf_type = kwargs.pop("trsf_type", None)
    trsf_type = _method_check(trsf_type, BLOCKMATCHING_METHODS, DEF_TRSF_TYPE)
    str_param += f" -trsf-type {trsf_type}"

    kwd_msg = "Use of keyword argument '{}' is limited to method '{}' and trsf type '{}'."
    # - Parse `seed` keyword argument:
    # LIMITED to:
    #  - method = 'random'
    #  - trsf_type = {'rigid', 'affine'}
    seed = kwargs.pop("seed", None)
    if seed is not None:
        if method == "random" and trsf_type in ['rigid', 'affine']:
            str_param += f" -srandom {seed}"
        else:
            log.info(kwd_msg.format('seed', "random", ['rigid', 'affine']))

    # - Parse `angle_range` keyword argument:
    # LIMITED to:
    #  - method = 'random'
    #  - trsf_type in {'rigid', 'affine'}
    angle_range = kwargs.pop("angle_range", None)
    if angle_range is not None:
        if method == "random" and trsf_type in ['rigid', 'affine']:
            low_range, high_range = angle_range
            str_param += f" -angle-range {low_range} {high_range}"
        else:
            log.info(kwd_msg.format('angle_range', "random", ['rigid', 'affine']))

    # - Parse `translation_range` keyword argument:
    # LIMITED to:
    #  - method = 'random'
    #  - trsf_type in {'rigid', 'affine'}
    translation_range = kwargs.pop("translation_range", None)
    if translation_range is not None:
        if method == "random" and trsf_type in ['rigid', 'affine']:
            low_range, high_range = translation_range
            str_param += f" -translation-range {low_range} {high_range}"
        else:
            log.info(kwd_msg.format('translation_range', "random", ['rigid', 'affine']))

    # - Parse `scale_range` keyword argument:
    # LIMITED to:
    #  - method = 'random'
    #  - trsf_type = 'affine'
    scale_range = kwargs.pop("scale_range", None)
    if scale_range is not None:
        if method == "random" and trsf_type == 'affine':
            low_range, high_range = scale_range
            str_param += f" -scale-range {low_range} {high_range}"
        else:
            log.info(kwd_msg.format('scale_range', "random", 'affine'))

    # - Parse `shear_range` keyword argument:
    # LIMITED to:
    #  - method = 'random'
    #  - trsf_type = 'affine'
    shear_range = kwargs.pop("shear_range", None)
    if shear_range is not None:
        if method == "random" and trsf_type == 'affine':
            low_range, high_range = shear_range
            str_param += f" -shear-range {low_range} {high_range}"
        else:
            log.info(kwd_msg.format('shear_range', "random", 'affine'))

    # - Parse `sinusoid_amplitude` keyword argument:
    # LIMITED to:
    #  - method in {"sinus2D", "sinus3D"}
    #  - trsf_type = 'vectorfield'
    sinusoid_amplitude = kwargs.pop("sinusoid_amplitude", None)
    if sinusoid_amplitude is not None:
        if "sinus" in method and trsf_type == 'vectorfield':
            str_param += " -sinusoid-amplitude "
            str_param += " ".join(list(map(str, sinusoid_amplitude)))
        else:
            log.info(kwd_msg.format('sinusoid_amplitude', "sinus", 'vectorfield'))

    # - Parse `sinusoid_period` keyword argument:
    # LIMITED to:
    #  - method in {"sinus2D", "sinus3D"}
    #  - trsf_type = 'vectorfield'
    sinusoid_period = kwargs.pop("sinusoid_period", None)
    if sinusoid_period is not None:
        if "sinus" in method and trsf_type == 'vectorfield':
            str_param += " -sinusoid-period "
            str_param += " ".join(list(map(str, sinusoid_period)))
        else:
            log.info(kwd_msg.format('sinusoid_period', "sinus", 'vectorfield'))

    return str_param, kwargs


#: List of valid transformation averaging methods, to use with ``method`` in ``trsfs_averaging`` from ``timagetk.algorithms.trsf``.
AVERAGE_TRSFS = ["mean", "robust-mean"]
#: Default transformation averaging method.
DEF_AVERAGE_TRSFS = AVERAGE_TRSFS[0]


def trsfs_averaging_kwargs(method, trsf_type, **kwargs):
    """Keyword argument parser for ``vt.mean_trsf``.

    Parameters
    ----------
    method : {"mean", "robust-mean"}
        Transformations averaging method to use.
    trsf_type : {"rigid", "affine", "vectorfield"}
        Type of transformation to create.

    Other Parameters
    ----------------
    estimator_type : {"wlts", "lts", "wls", "ls"}
        Transformation estimator, see notes for more details.
    lts_fraction : float
        If defined, set the fraction of pairs that are kept, used with trimmed estimations.
    lts_deviation : float
        If defined, set the threshold to discard pairings (see notes), used with trimmed estimations.
    lts_iterations : int
        If defined, set the maximal number of iterations, used with trimmed estimations.
    fluid_sigma : float or list of float
        Sigma for fluid regularization, *i.e.* field interpolation and regularization for pairings (only for vector field)

    Returns
    -------
    str
        Formatted parameters for ``vt.mean_trsf`` CLI.
    dict
        Remaining keyword arguments after parsing.

    See Also
    --------
    AVERAGE_TRSFS, DEF_AVERAGE_TRSFS

    Notes
    -----
    Definitions for ``estimator_type``:

     - **wlts**: weighted least trimmed squares;
     - **lts**: least trimmed squares;
     - **wls**: weighted least squares;
     - **ls**: least squares.

    Parameter ``lts_deviation`` is used in the formulae:
        ``threshold = average + lts_deviation * standard_deviation``

    Examples
    --------
    >>> from timagetk.third_party.vt_parser import trsfs_averaging_kwargs
    >>> trsfs_averaging_kwargs('mean', 'vectorfield')
    (' -mean -trsf-type vectorfield -estimator-type ls', {})

    """
    str_param = ""

    # - Parse `method` parameter:
    method = _method_check(method, AVERAGE_TRSFS, DEF_AVERAGE_TRSFS)
    str_param += f" -{method}"

    # TODO: test if this can be removed! Trsf type should be known to vt method
    trsf_type = _method_check(trsf_type, BLOCKMATCHING_METHODS, DEF_TRSF_TYPE)
    str_param += f" -trsf-type {trsf_type}"

    estimator = kwargs.get("estimator_type", DEF_PM_ESTIMATOR)
    str_param += f" -estimator-type {estimator}"
    # Trimmed estimators:
    if estimator in ["wlts", "lts"]:
        assert "lts_fraction" in kwargs.keys() or "lts_deviation" in kwargs.keys()
        lts_fraction = kwargs.pop("lts_fraction", None)
        if lts_fraction is not None:
            str_param += f" -lts-fraction {lts_fraction}"
        lts_deviation = kwargs.pop("lts_deviation", None)
        if lts_deviation is not None:
            str_param += f" -lts-deviation {lts_deviation}"
        lts_iterations = kwargs.pop("lts_iterations", None)
        if lts_iterations is not None:
            str_param += f" -lts-iterations {lts_iterations}"

    fluid_sigma = kwargs.pop('fluid_sigma', None)
    if fluid_sigma is not None:
        str_param += f" -fluid-sigma {fluid_sigma}"

    return str_param, kwargs


def default_interpolation_method(image):
    """Define correct interpolation method to use depending on image type.

    Parameters
    ----------
    image : Image type
        The image to interpolate.

    Returns
    -------
    str
        The default method to use with this type of image.

    See Also
    --------
    timagetk.third_party.vt_parser.DEF_GRAY_INTERP_METHOD
    timagetk.third_party.vt_parser.DEF_LABEL_INTERP_METHOD

    Notes
    -----
    If given ``image`` is a ``LabelledImage`` instance, use ``DEF_LABEL_INTERP_METHOD`` by default or check it is in ``LABEL_INTERPOLATION_METHODS``.
    If given ``image`` is a ``SpatialImage`` instance, use ``DEF_GRAY_INTERP_METHOD`` by default or check it is in ``LABEL_INTERPOLATION_METHODS``.

    Examples
    --------
    >>> import numpy as np
    >>> from timagetk.third_party.vt_parser import default_interpolation_method
    >>> from timagetk import SpatialImage
    >>> from timagetk import LabelledImage
    >>> # EXAMPLE 1: Get default SpatialImage image interpolation method:
    >>> default_interpolation_method(SpatialImage(np.ones((3,3,3))))
    'linear'
    >>> # EXAMPLE 2: Get default LabelledImage interpolation method:
    >>> default_interpolation_method(LabelledImage(np.ones((3,3,3))))
    'cellbased'

    """
    if isinstance(image, (LabelledImage, AbstractTissueImage)):
        return DEF_LABEL_INTERP_METHOD
    elif isinstance(image, SpatialImage):
        return DEF_GRAY_INTERP_METHOD
    else:
        msg = "Input `image` is type {} has no default interpolation method or is unknown!"
        raise NotImplementedError(msg.format(clean_type(image)))


def interpolate_kwargs(image, method=None):
    """Check or define correct interpolation method to use with ``apply_trsf``.

    Parameters
    ----------
    image : Image type
        The image to interpolate.
    method : str in INTERPOLATION_METHODS, optional
        If ``None`` (default) try to guess the interpolation method from `image` type.
        Else check if it is an adequate or defined method.

    Returns
    -------
    str
        The string to use with ``params`` in ``apply_trsf``.

    See Also
    --------
    timagetk.third_party.vt_parser.GRAY_INTERPOLATION_METHODS
    timagetk.third_party.vt_parser.DEF_GRAY_INTERP_METHOD
    timagetk.third_party.vt_parser.LABEL_INTERPOLATION_METHODS
    timagetk.third_party.vt_parser.DEF_LABEL_INTERP_METHOD

    Notes
    -----
    If given ``image`` is a ``LabelledImage``, use ``DEF_LABEL_INTERP_METHOD`` by default or check it is in ``LABEL_INTERPOLATION_METHODS``.
    If given ``image`` is a ``SpatialImage``, use ``DEF_GRAY_INTERP_METHOD`` by default or check it is in ``LABEL_INTERPOLATION_METHODS``.

    Examples
    --------
    >>> from timagetk.third_party.vt_parser import interpolate_kwargs
    >>> from timagetk.io.dataset import shared_data
    >>> from timagetk.io import imread

    >>> # EXAMPLE 1: Get default grayscale image interpolation method:
    >>> ref_path = shared_dataset("p58")[0]
    >>> image = imread(ref_path)
    >>> interpolate_kwargs(image)
    ' -interpolation linear'
    >>> interpolate_kwargs(image, "nearest")

    >>> # EXAMPLE 2: Get default labelled image interpolation method:
    >>> from timagetk import LabelledImage
    >>> lab_image = LabelledImage(image)
    >>> interpolate_kwargs(lab_image)
    ' -interpolation cellbased'

    """
    method_msg = "Input `image` is a {}, option should be in {} but got '{}'!"

    if method is None:
        method = default_interpolation_method(image)

    if isinstance(image, LabelledImage):
        allowed_methods = ['label'] + LABEL_INTERPOLATION_METHODS
        try:
            assert method in allowed_methods
        except AssertionError:
            log.warning(method_msg.format(clean_type(image), allowed_methods, method))
    elif isinstance(image, SpatialImage):
        allowed_methods = ['gray', 'grey'] + GRAY_INTERPOLATION_METHODS
        try:
            assert method in allowed_methods
        except AssertionError:
            log.warning(method_msg.format("SpatialImage", allowed_methods, method))
    elif not isinstance(image, vt.vtImage):
        msg = "Input `image` is not an Image type: {}!"
        raise NotImplementedError(msg.format(type(image)))

    str_param = ""
    if method == 'gray' or method == 'grey':
        str_param += ' -interpolation {}'.format(DEF_GRAY_INTERP_METHOD)
    elif method == 'label':
        str_param += ' -interpolation {}'.format(DEF_LABEL_INTERP_METHOD)
    elif method in INTERPOLATION_METHODS:
        str_param += ' -interpolation {}'.format(method)
    else:
        msg = "Given interpolation 'method' ({}) is not available!\n".format(method)
        msg += "Choose among: {}".format(INTERPOLATION_METHODS)
        raise NotImplementedError(msg)

    return str_param
