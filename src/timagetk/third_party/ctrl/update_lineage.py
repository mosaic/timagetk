import numpy as np
import networkx as nx

from timagetk.bin.logger import get_logger
log = get_logger(__name__)

# This module is dedicated to the update of high-confidence pairings during iterative procedure

# Input : lineage, preservation_score, mother/daughter adjacency graphs, hcf lineage (high-confidence pairings)
# Output : hcf lineage updated

# The main rational is to aggregate new high-confidence pairings based on their preservation score and their adjacency with
# previous hcf.
# Initially, the X% best pairings (in the quality score sense) are selected. The largest connected-component
# (from topology) is retained as initial hcf region).
#
# TODO : Need to fix `update_hc` method --> at the end of the update process, multiple daughers can have same mother..

def initialize_hc(M, preservation_dict, lineage, initial_thresh=0.8, Nminadj=3):
    """Initialize the high-confidence pairings (hcf) from the current lineage using the context preservation score.

    Parameters
    ----------
    M : nx.Graph
        The mother adjacency graph.
    preservation_dict : dict
        The context preservation score for each mother label
    lineage : dict or Lineage
        The current mapping between mother and daughter labels.
    initial_thresh : float, optional
        The minimal context preservation score to initialize the hcf, by default 0.8.
    Nminadj : int, optional
        The minimal number of hcf neighbors to be converted in hcf, by default 3.

    Returns
    -------
    nx.Graph
        The modified mother graph enriched with attributes 'hcf', 'adjacency_score' and 'hcf_lineage'.
    """

    # - initialize the hcf cells, get all the cells with a preservation upper than initial_thresh
    mother_hcf = [lab for lab, val in preservation_dict.items() if val >= initial_thresh]

    # - get the largest connected component
    #S = M.subgraph(mother_hcf)
    #mother_hcf = list(max((S.subgraph(c) for c in nx.connected_components(S)), key=len))

    for mlab in mother_hcf:
        log.info(f'Add {mlab} --> {lineage[mlab]}')

    # - Save the lineage and the preservation score for the hcf
    nx.set_node_attributes(M, {lab: 1 if lab in mother_hcf else 0 for lab in M.nodes()}, 'hcf')
    nx.set_node_attributes(M,
                           {lab: score for lab, score in preservation_dict.items() if lab in mother_hcf},
                           'adjacency_score')
    nx.set_node_attributes(M,
                           {mlab: dlab for mlab, dlab in lineage.items() if mlab in mother_hcf},
                           'hcf_lineage')

    # - force the aggregation of more hcf if not enough to compute spatial transformation (at least 4 in affine transformation)
    while len(mother_hcf) < 5:
        # get the possible hcf
        possible_hcf = {mlab: dlab for mlab, dlab in lineage.items() if mlab not in mother_hcf}

        # find the current best one
        M, current_hcf, best_score = get_neighbors_hc(M, preservation_dict, mother_hcf, possible_hcf,
                                                       thresh=initial_thresh, Nminadj=Nminadj)
        # --> here set the thresh to initial_thresh to force aggregation of "very high-confidence pairings"

        # - update the mother hcf
        mother_hcf.append(current_hcf)

    log.info(f'Found {len(mother_hcf)} initial hc\n')

    return M

def aggregate_hc(M, preservation_dict, lineage, min_add=0.05, initial_thresh=0.8, Nminadj=3, min_thresh=0):
    """Aggregate additional high-confidence pairings (HCF) based on adjacency and preservation scores.

    This function refines and extends the high-confidence pairings within the mother graph M by
    identifying and integrating new HCF nodes that are adjacent to existing ones and exceed
    specified preservation score thresholds. The function first updates the graph using current
    lineage information and context scores, then iteratively seeks new pairings to enhance graph
    connectivity and lineage accuracy.

    Parameters
    ----------
    M : nx.Graph
        The mother adjacency graph, which is iteratively updated with enhanced HCF attributes.
    preservation_dict : dict
        Context preservation scores for each mother node, guiding the aggregation process.
    lineage : dict or Lineage
        Mapping of mother to daughter cell labels, used to propose new HCF.
    min_add : float, optional
        The minimum fraction of new HCF to add relative to existing lineages, default is 0.05.
    initial_thresh : float, optional
        Initial minimum context preservation score required for considering new HCF, default is 0.8.
    Nminadj : int, optional
        Minimum number of neighboring HCF nodes required for a new candidate, default is 3.
    min_thresh : float, optional
        The minimal score threshold for the second aggregation phase, allowing lower-score integrations, default is 0.

    Returns
    -------
    nx.Graph
        Updated mother graph M with expanded HCF, reflecting enhanced lineage mapping.
    """

    # - first start by check if any hcf can be updated or not (preservation increase or same)
    M, lineage = update_hc(M, preservation_dict, lineage)

    # - aggregate new hcf
    mother_hcf = [lab for lab, hcf in nx.get_node_attributes(M, 'hcf').items() if hcf]# and (lab in lineage)]
    N, Nadd = len(mother_hcf), len(lineage) * min_add

    log.info(f'\nAggregate hc : {len(mother_hcf)} hc currently...')

    # - get all the adjacent association with score > initial_thresh
    best_score = 1  # init
    while best_score >= initial_thresh:
        # get the possible hcf
        possible_hcf = {mlab: dlab for mlab, dlab in lineage.items() if mlab not in mother_hcf}

        if len(possible_hcf) == 0:
            break

        # find the current best one
        M, current_hcf, best_score = get_neighbors_hc(M, preservation_dict, mother_hcf, possible_hcf,
                                                       thresh=initial_thresh, Nminadj=Nminadj)

        if current_hcf is None:
            break

        # - update the mother hcf
        mother_hcf.append(current_hcf)

    # - get at least Nadd new high-confidence pairings
    while len(mother_hcf) < (N + Nadd):
        # get the possible hcf
        possible_hcf = {mlab: dlab for mlab, dlab in lineage.items() if mlab not in mother_hcf}

        if not possible_hcf:
            break

        # find the current best one
        M, current_hcf, best_score = get_neighbors_hc(M, preservation_dict, mother_hcf, possible_hcf,
                                                       thresh=min_thresh, Nminadj=Nminadj)

        if not current_hcf:
            break

        # - update the mother hcf
        mother_hcf.append(current_hcf)

    log.info(f'After aggregation : {len(mother_hcf)} hc')

    return M

def get_neighbors_hc(M, preservation_dict, current_hcf, possible_hcf, thresh, Nminadj=3):
    """Identify and select the best new high-confidence pairings (HCF) adjacent to current HCF.

    This function examines the neighborhood of current high-confidence pairings in the mother graph M.
    It selects the best candidate for new pairings based on adjacency with existing HCF and context
    preservation scores. If no suitable candidates are found, the adjacency constraint is relaxed.

    Parameters
    ----------
    M : nx.Graph
        The mother adjacency graph containing nodes and edges representing potential mappings.
    preservation_dict : dict
        A dictionary where keys are mother cell labels and values are their respective context
        preservation scores.
    current_hcf : list
        A list of current high-confidence pairing labels.
    possible_hcf : dict
        A mapping from potential mother cell labels to their corresponding daughter labels, representing
        potential high-confidence pairings.
    thresh : float
        The minimal context preservation score required for new pairings.
    Nminadj : int, optional
        The minimal number of neighboring high-confidence pairings required for a node to be considered as
        potential new HCF, default is 3.

    Returns
    -------
    tuple
        A tuple containing:
            - Updated mother graph M with added HCF attributes.
            - The label of the best new HCF candidate or None if no suitable candidate is found.
            - The context preservation score of the selected candidate or None.
    """

    # - get the current neighbors of the hcf cells
    boundary_cells = [lab for lab in nx.node_boundary(M, current_hcf) if lab in possible_hcf]

    if not boundary_cells:
        #log.info('No cells to add!')
        return None, None, None
    else:
        current_Nminadj = Nminadj

        while current_Nminadj > 0:
            # - filter the neighbors according to their adjacency
            selected_cells = {lab: preservation_dict[lab] for lab in boundary_cells
                              if (len(set(nx.neighbors(M, lab)) & set(current_hcf)) >= current_Nminadj)
                              and lab in preservation_dict}

            if not selected_cells:
                # - if no selected cells, decrease the constraint on the adjacency
                current_Nminadj = current_Nminadj - 1
            else:
                # - get the selected cells with the highest preservation score
                best_candidate = max(selected_cells, key=selected_cells.get)

                if thresh <= preservation_dict[best_candidate]:
                    # - update the adjacency graph according to the selection
                    M.nodes[best_candidate]['hcf'] = 1
                    M.nodes[best_candidate]['adjacency_score'] = preservation_dict[best_candidate]
                    M.nodes[best_candidate]['hcf_lineage'] = possible_hcf[best_candidate]

                    log.info(f'Add {best_candidate} --> {possible_hcf[best_candidate]},'
                                 f" score={preservation_dict[best_candidate]} (Nadj={current_Nminadj})")
                    return M, best_candidate, preservation_dict[best_candidate]
                else:
                    # - choose to decrease the constraint on the adjacency (here we consider that the preservation score
                    #   is more discriminant than the adjacency...not completely sure)
                    current_Nminadj = current_Nminadj - 1

        return M, None, None

def update_hc(M, preservation_dict, lineage):
    """Update the high-confidence pairings (HCF) from the lineage mapping in the mother graph.

    This function identifies and updates changes in high-confidence pairings (HCF) nodes within
    the mother graph M based on updated lineage information and context preservation scores.
    It checks for HCF nodes with no descendants or differing descendants in the current lineage
    and assesses whether changes improve lineage quality. The function also recalculates context
    scores and prunes inadequate lineage associations.

    Parameters
    ----------
    M : nx.Graph
        The mother adjacency graph enriched with current HCF attributes ('hcf', 'adjacency_score', 'hcf_lineage').
    preservation_dict : dict
        A dictionary containing context preservation scores for each mother node label.
    lineage : Lineage or dict
        The proposed mapping from mother to daughter cell labels, which updates the current lineage.

    Returns
    -------
    tuple
        Updated mother graph M with HCF changes and a modified copy of the lineage reflecting adjustments.
    """
    # - Make a copy of the lineage (if modification)
    lineage_cpy = lineage.copy_lineage()

    # - Get the current hc mothers
    mother_hc = [mlab for mlab, hcf in nx.get_node_attributes(M, 'hcf').items() if hcf]

    # - Spot the hc mothers that do not have descendants in the current lineage
    has_no_desc = list(set(mother_hc) - set(lineage_cpy))
    log.info(f'Detect {len(has_no_desc)} hc with no descendants in the current lineage')
    mother_hc = set(mother_hc) - set(has_no_desc) # remove it from the set to test if descendants has changed

    # - Get the list of the hc that changed
    has_changed_desc = [mlab for mlab in mother_hc if set(M.nodes[mlab]['hcf_lineage']) != set(lineage_cpy[mlab])]
    log.info(f'Detect {len(has_changed_desc)} hc with different descendants in the current lineage')

    # - We want to spatially clustered the hc that changed to see if, regionally, the context preservation is improved
    #   by the new lineage proposition.
    S = M.subgraph(has_no_desc + has_changed_desc) # get the subgraph containing the changing hc
    hc_clusters = [S.subgraph(c) for c in nx.connected_components(S)] # get the different connected subgraph
    log.info(f'Detect {len(hc_clusters)} unique cluster of changing hc')

    # - Each possible cluster will raises a new context preservation, calculated it. Take into account the hc
    #   neighborhood of each cluster (its preservation context will also changed!)
    for cluster in hc_clusters:
        # - get the hc neighborhood (or boundary) of this cluster (that do no changed)
        boundary_cells = [mlab for mlab in nx.node_boundary(M, cluster) if mlab in mother_hc]
        cluster_cells = boundary_cells + list(cluster)

        # - calculate the mean variation of the preservation context in this bunch of cells
        previous_score = np.mean([M.nodes[mlab]['adjacency_score'] for mlab in cluster_cells])
        current_score = np.mean([preservation_dict[mlab] for mlab in cluster_cells if mlab in preservation_dict])

        if set(cluster_cells) - set(preservation_dict): # non-empty
            log.info(f'For the cluster {list(cluster)}, {len(set(cluster_cells) - set(preservation_dict))} cells has no scores!')

        # - variation ?
        variation_score = current_score - previous_score
        log.info(f"Cluster {list(cluster)} : score={previous_score.astype('float16')} to score={current_score.astype('float16')}")
        if variation_score > 0:
            # - Update the hc
            for mlab in cluster:
                if mlab in lineage_cpy:
                    M.nodes[mlab]['hcf_lineage'] = lineage_cpy[mlab]
                    log.info(f'{mlab} --> {lineage_cpy[mlab]}')
                    del lineage_cpy[mlab] # remove from the lineage
                else:
                    # - remove the current hc (no descendant)
                    log.info(f'Remove {mlab} from the hc')
                    M.nodes[mlab]['hcf'] = 0
                    del M.nodes[mlab]['hcf_lineage']
                    del M.nodes[mlab]['adjacency_score']
        else:
            # - Remove from the current lineage, all the associations that implicates one of the daughters of the cluster
            for mlab in cluster:
                previous_desc = M.nodes[mlab]['hcf_lineage']
                lineage_cpy = {k: dlab for k, dlab in lineage_cpy.items() if not (set(previous_desc) & set(dlab))}

    # - Update the preservation score of the hcf
    mother_hc = [lab for lab, hcf in nx.get_node_attributes(M, 'hcf').items() if hcf]
    for mlab in mother_hc:
        if mlab in preservation_dict: # if hc has descendants in the current lineage (else keep the previous score)
            M.nodes[mlab]['adjacency_score'] = preservation_dict[mlab]

    return M, lineage_cpy