#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

import argparse
from os import mkdir
from os.path import split, abspath, join, exists

from timagetk.third_party.ctrl.io import DEFAULT_SEP, write_lineage, read_lineage
from timagetk.third_party.ctrl.io import DEFAULT_LINEAGE_FMT
from timagetk.third_party.ctrl.io import AVAILABLE_LINEAGE_FMT
from timagetk.third_party.ctrl.lineage import Lineage


def parsing() -> argparse.ArgumentParser:
    """Configures and returns an argument parser."""
    parser = argparse.ArgumentParser(
        description='Convert lineage file between different formats.',
        epilog="By default convert 'marsalt' files to 'basic' file format.")

    parser.add_argument('lineage', type=str, nargs='+',
                        help="list of lineage file to convert.")

    # optional arguments:
    fmt_args = parser.add_argument_group('Format options')
    fmt_args.add_argument('-ifmt', '--in_format', type=str,
                          default='marsalt',
                          choices=AVAILABLE_LINEAGE_FMT,
                          help="Name of the format to use.")
    fmt_args.add_argument('-isep', '--in_separator', type=str,
                          default=DEFAULT_SEP,
                          help="Separator used in 'basic' format.")

    out_args = parser.add_argument_group('Output options')
    out_args.add_argument('--path', type=str, default='converted/',
                          help="Path to write the converted lineage.")
    out_args.add_argument('-ofmt', '--out_format', type=str,
                          default=DEFAULT_LINEAGE_FMT,
                          choices=AVAILABLE_LINEAGE_FMT,
                          help="Name of the format to use for output file.")
    out_args.add_argument('-osep', '--out_separator', type=str,
                          default=DEFAULT_SEP,
                          help="Separator used in 'basic' format for output file.")

    return parser


def main():
    """For help, in a terminal: `$ lineage_convert.py -help`
    """
    parser = parsing()
    args = parser.parse_args()
    # - Variables definition from argument parsing:
    # -- Lineage:
    list_lineage = args.lineage
    n_lin = len(list_lineage)
    log.info("Got {} input lineage{}.".format(n_lin, 's' if n_lin > 1 else ''))

    for l_file in list_lineage:
        log.info("\n# - Converting lineage {}...".format(l_file))
        lineage = read_lineage(l_file, fmt=args.in_format,
                               sep=args.in_separator)
        # If the path is relative, get the absolute path:
        fpath, fname = split(l_file)
        if not args.path.startswith('.') and not args.path.startswith('/'):
            args.path = abspath(join(fpath, args.path))
        # Make sure the path exists:
        try:
            assert exists(args.path)
        except AssertionError:
            mkdir(args.path)
        # Write the converted file:
        write_lineage(lineage, join(args.path, fname), sep=args.out_separator)


if __name__ == '__main__':
    main()
