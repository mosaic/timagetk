import numpy as np

from timagetk.third_party.ctrl.matching_flow_graph import MatchingFlowGraph
from scipy.spatial import distance_matrix

from timagetk.bin.logger import get_logger
log = get_logger(__name__)


from skimage.transform import ProjectiveTransform
from skimage.measure import ransac

#### Main rational of this module is to map mother vertices to daughter vertices.
#    Because of cell division can happen between two consecutive time-points, the number of mother vertices is
#    usually lower that daughter vertices. We need to find the vertex mapping.
#    We assume that locally (cell level), only linear deformation happens : thus we look for an affine transformation
#    that can "align" the vertices pointcloud. More precisely, we want to find a transformation that minimize the residuals
#    between mapped vertices.
#    Depending on the usage, it is possible to have some global constraint on the mapping. For instance, if mother and
#    daughter vertices come from L1 cells : vertices that are at the surface should be mapped together.
#    These global constraint can help to determinate an initial transformation that will provide a better alignment of
#    the vertices pointcloud.

def vertex_assignement_problem(mother_vertex, daughter_vertex, init_trsf_method='similitude',
                               mother_group=None, daughter_group=None, max_pairing_dist=10):
    """Solve the vertex assignment problem between two groups of vertices: mother and daughter.

    This function finds a mapping between two sets of vertices using an iterative process
    that alternates between estimating spatial transformations and vertex mappings until
    convergence. The spatial transformation is determined using an affine model improved
    through a RANSAC (Random Sample Consensus) algorithm for robust fitting, akin to an
    ICP (Iterative Closest Point) procedure with robustness to outliers.

    Parameters
    ----------
    mother_vertex : np.ndarray
        Array of vertices representing the mother group, assumed to be in real units
        and ordered as (X, Y, Z).
    daughter_vertex : np.ndarray
        Array of vertices representing the daughter group, in the same unit and order as
        the mother group.
    init_trsf_method : str, optional
        The method used for initial transformation estimation, default is 'similitude'.
    mother_group : np.ndarray or None, optional
        A vector indicating sub-groups within the mother vertices, used for forced
        mapping of specific vertex categories (e.g., background/non-background). Each
        integer represents a possible group. Default is None.
    daughter_group : np.ndarray or None, optional
        Similar to mother_group, but for daughter vertices. Default is None.
    max_pairing_dist : float, optional
        Maximum allowed distance for pairing vertices, used in establishing initial
        and iterative matchings. Default value is 10.

    Returns
    -------
    tuple
        A tuple containing the matched mother and daughter vertices after convergence
        of the iterative process, or (None, None) if a valid solution is not found.

    Notes
    -----
    - The iterative matching process includes updating spatial transformations and solving
      a minimum cost flow problem to minimize spatial proximity costs between vertex pairs.
    - The algorithm stops when vertex mappings stabilize across iterations or the maximum
      number of iterations is reached.
    - Sub-group mapping can enhance the specificity of matches, catering to application-specific
      needs such as distinguishing between different vertex types.
    """
    # - Get the initial transformation
    #log.info(f'Iteration n°0')
    trsf = initial_vertex_trsf(mother_vertex, daughter_vertex, method=init_trsf_method,
                            mother_group=mother_group, daughter_group=daughter_group)

    # - Apply the transformation to the mother vertex
    p = np.concatenate([mother_vertex, np.ones_like(mother_vertex)[:, :1]], axis=1)
    mpts = trsf.dot(p.T).T[:, :3]

    # - Get the vertex matching that minimize the sum of the residual distance between matched vertices
    #log.info(f'Estimate an initial vertices mapping after registration...')
    cost_dict = get_vertices_matching_costs(mpts, daughter_vertex,
                                            mother_group=mother_group, daughter_group=daughter_group,
                                            dmax=max_pairing_dist)
    matching_idx = min_cost_flow_matching(cost_dict)

    niter = 1
    min_ransac_samples = 5
    MAX_ITER = 10
    prev_matching_idx = np.zeros_like(matching_idx)

    # - Iterate until convergence of the matching
    while (np.any(prev_matching_idx - matching_idx)) and (niter < MAX_ITER):
        #log.info(f'Iteration n°{iter}')

        # - Update the matching
        prev_matching_idx = matching_idx.copy()

        ransac_mother_vertex = np.vstack([mother_vertex[k[0]] for k in matching_idx])
        ransac_daughter_vertex = np.vstack([daughter_vertex[k[1]] for k in matching_idx])

        # - Assert that we have enough points
        if (ransac_mother_vertex.shape[0] < min_ransac_samples+1) or\
                (ransac_daughter_vertex.shape[0] < min_ransac_samples+1):
            # - could be done more properly...
            return None, None

        # - calculate the affine transformation corresponding to the matching using RANSAC
        #log.info(f'Estimate spatial transformation using ransac...')
        model, inliers = ransac((ransac_mother_vertex, ransac_daughter_vertex), AffineTransform3D,
                                min_samples=min_ransac_samples, residual_threshold=2, max_trials=100)

        # - Apply the current transformation to the point
        mpts = model(mother_vertex)

        # - Get the matching
        #log.info(f'Estimate vertices mapping...')
        cost_dict = get_vertices_matching_costs(mpts, daughter_vertex,
                                                mother_group=mother_group, daughter_group=daughter_group,
                                                dmax=max_pairing_dist)
        matching_idx = min_cost_flow_matching(cost_dict)

        # - Update iteration
        niter += 1

    # - Return the matched vertices
    mother_vertex_matched = [mother_vertex[ix[0]] for ix in matching_idx]
    daughter_vertex_matched = [daughter_vertex[ix[1]] for ix in matching_idx]

    return mother_vertex_matched, daughter_vertex_matched


def initial_vertex_trsf(mother_vertex, daughter_vertex, method='similitude', mother_group=None, daughter_group=None):
    """Estimate an initial transformation to apply to the mother vertex set before vertex matching.

    This function calculates a preliminary spatial transformation that aligns the position of
    two vertex groups, potentially using defined sub-groups for refined transformation estimation.
    Different transformation methods can be selected, such as translation, rigid, affine, or similitude.

    Parameters
    ----------
    mother_vertex : np.ndarray
        Array of mother vertices, each ordered as (X, Y, Z).
    daughter_vertex : np.ndarray
        Array of daughter vertices, ordered similarly as mother vertices.
    method : str, optional
        The transformation method to use. Options are 'translation', 'rigid', 'affine', or 'similitude'.
        The default method is 'similitude'.
    mother_group : np.ndarray or None, optional
        A vector defining sub-groups of mother vertices (e.g., [0, 1, 0,...]) where integers indicate
        the group label. This aids in aligning specific vertex categories together. Defaults to None.
    daughter_group : np.ndarray or None, optional
        Similar to mother_group, but for daughter vertices. Defaults to None.

    Returns
    -------
    np.ndarray
        A transformation matrix meant to align mother vertices with daughter vertices based on
        the selected method.

    Notes
    -----
    - If the transformation method relies on groups (e.g., 'rigid', 'affine') but groups are not
      provided, the function defaults to using 'translation'.
    - The method 'translation' will align centroids of the vertex point cloud, suitable for more than two defined groups.
    - When using methods like 'rigid' or 'similitude', the function calculates and aligns vectors
      between group centroids as part of the transformation process.
    """
    # - Assert that the cluster are given
    if (method != 'translation') and ((mother_group is None) or (daughter_group is None)):
        method = 'translation' # force translation --> can not estimate other trsf.

    # - Init
    T_init = np.eye(4)
    #log.info(f'Estimate {method} transformation...')

    if (np.max(mother_group) > 1) or (np.max(daughter_group) > 1):
        method = 'translation' # force translation --> do not worry, groups will be used later

    if method == 'translation':
        gm = np.mean(mother_vertex, axis=0)
        gd = np.mean(daughter_vertex, axis=0)

        T_init[:3, -1] = gd-gm

    elif method in ['rigid', 'affine', 'similitude']:
        # - get the centroids of each groups
        gm0 = np.mean([p for ix, p in zip(mother_group, mother_vertex) if ix == 0], axis=0)  #mother centroid,group 0
        gm1 = np.mean([p for ix, p in zip(mother_group, mother_vertex) if ix == 1], axis=0)  #mother centroid,group 1

        gd0 = np.mean([p for ix, p in zip(daughter_group, daughter_vertex) if ix == 0], axis=0)  #daughter centroid,group 0
        gd1 = np.mean([p for ix, p in zip(daughter_group, daughter_vertex) if ix == 1], axis=0)  #daughter centroid,group 1

        # - get the centroid of each pointcloud
        n1 = np.count_nonzero(mother_group)  # number of element of group 0
        n = len(mother_group)
        gm = (1 / n) * ((n - n1) * gm0 + n1 * gm1)

        if not np.allclose(gm, np.mean(mother_vertex, axis=0)):
            log.info(f'mother_centroid: {gm} vs.{np.mean(mother_vertex, axis=0)}')

        n1 = np.count_nonzero(daughter_group)  # number of element of group 0
        n = len(daughter_group)
        gd = (1 / n) * ((n - n1) * gd0 + n1 * gd1)

        if not np.allclose(gd, np.mean(daughter_vertex, axis=0)):
            log.info(f'daughter_centroid: {gd} vs.{np.mean(daughter_vertex, axis=0)}')

        # - form the basis (u0, u1, u2) where:
        #   u0: (gm, gm0)
        #   u2: (gm, gm0) x (gd, gd0)
        #   u1: u2 x u0
        u0 = gm0 - gm
        u2 = np.cross(u0, gd0 - gd)
        u1 = np.cross(u2, u0)

        u0 = u0 / np.linalg.norm(u0)  # unit vector
        u1 = u1 / np.linalg.norm(u1)  # unit vector
        u2 = u2 / np.linalg.norm(u2)  # unit vector

        P = np.column_stack([u0, u1, u2])  # basis

        # - Initialize scaling, shearing and rotation matrix
        R, S, C = np.eye(3), np.eye(3), np.eye(3)

        if method in ['rigid', 'similitude']:
            # - get the unit vector u'0
            ud0 = (gd0 - gd) / np.linalg.norm(gd0 - gd)

            # - estimate rotation matrix
            cosa = u0.dot(ud0)
            sina = u1.dot(ud0)

            R = np.array([[cosa, -sina, 0],
                          [sina, cosa, 0],
                          [0, 0, 1]])

            if method == 'similitude':
                # - Calculate a scaling matrix
                s = np.linalg.norm(gd0 - gd) / np.linalg.norm(gm0 - gm) # |u'0|/|u0|
                S = np.diag([s, s, s]) # global scaling

        elif method == 'affine':
            # - calculate a scaling matrix
            s = np.dot(P[:, 0], gd0 - gd) / np.linalg.norm(gm0 - gm)  # scaling in the direction u0
            S = np.diag([s, s, s]) # global scaling

            # - estimate the shearing matrix
            c = np.dot(P[:, 1], gd0 - gd) / (s * np.linalg.norm(gm0 - gm))  # shearing in the direction u1
            C[1, 0] = c

        # - Form the 3x3 vector matrix
        V = P.dot(R).dot(C).dot(S).dot(P.T)

        # - Add the translation and compute the transformation
        T1, T2, V1 = np.eye(4), np.eye(4), np.eye(4)
        T1[:3, -1] = -gm  # goes to the origin
        T2[:3, -1] = gd  # goes to the daughter centroid
        V1[:3, :3] = V  # apply rotation, shearing, scaling

        T_init = T2.dot(V1).dot(T1)
    else:
        log.error('Unknown method')
        return None

    return T_init

def get_vertices_matching_costs(mother_vertex, daughter_vertex, mother_group=None, daughter_group=None, dmax=10):
    """Calculate a cost dictionary for potential vertex pairings based on spatial proximity.

    This function computes the Euclidean distances between every pair of vertices from two
    sets (mother and daughter) and generates a cost associated with each pairing. The cost
    is primarily determined by spatial proximity, with the option to restrict pairings using
    group attributes.

    Parameters
    ----------
    mother_vertex : np.ndarray
        Array of mother vertices, each represented in a (X, Y, Z) format.
    daughter_vertex : np.ndarray
        Array of daughter vertices, ordered similarly as the mother vertices.
    mother_group : np.ndarray or None, optional
        Grouping vector for mother vertices, categorizing each vertex into different groups
        (e.g., [0, 1, 0, ...]). Pairings are restricted to same-group vertices from mother
        and daughter sets. Defaults to None.
    daughter_group : np.ndarray or None, optional
        Grouping vector for daughter vertices, analogous in function to `mother_group`. Defaults to None.
    dmax : float, optional
        Maximum allowed distance for pairing vertices, beyond which no pairing is considered.
        Default value is 10.

    Returns
    -------
    dict
        A dictionary where keys are tuples representing the indices of paired vertices, and values
        are the normalized spatial costs of these pairings.
    """

    cost_dict = {}

    # - Compute all the possible pair-wise distance between vertex
    D = distance_matrix(mother_vertex, daughter_vertex, p=2)  # euclidian distance

    # - Get the list of groups that were defined (if any)
    if (mother_group is not None) and (daughter_group is not None):
        assert np.all(np.unique(mother_group) == np.unique(daughter_group)) # check that same groups are defined

        for ix in np.ndindex(D.shape):
            if (D[ix] <= dmax) and (mother_group[ix[0]] == daughter_group[ix[1]]):
                # reindex all the vertices (to avoid special behavior with the index 0 --> see MatchingFlowGraph)
                cost_dict[(ix[0]+1, ix[1]+1)] = np.around(D[ix] / dmax, 5)
    else:
        for ix in np.ndindex(D.shape):
            if D[ix] <= dmax:
                # reindex all the vertices (to avoid special behavior with the index 0 --> see MatchingFlowGraph)
                cost_dict[(ix[0]+1, ix[1]+1)] = np.around(D[ix] / dmax, 5)

    return cost_dict

def min_cost_flow_matching(cost_dict):
    """Determine the optimal vertex index mapping that minimizes the total matching cost using a flow network approach.

    This function formulates a minimum cost flow problem from a given set of vertex association costs,
    deriving the most cost-effective alignment between two vertex sets. Utilizing a bipartite graph
    representation, it identifies pairs with the minimal cumulative cost.

    Parameters
    ----------
    cost_dict : dict
        A dictionary where each key is a tuple representing a potential vertex match (mother_index, daughter_index),
        and each value is the associated cost of that match, based on spatial proximity or other relevant metrics.

    Returns
    -------
    np.ndarray
        A 2D array where each row contains indices of the matched vertices from the two sets
    """

    # - Remove some ctrl log requests
    ctrl_log = log.getLogger('ctrl.matching_flow_graph')
    ctrl_log.setLevel(log.CRITICAL)

    # - Create the bipartite matching graph
    fg = MatchingFlowGraph(cost_dict, max_cost=1)

    # - Solve the assignement problem
    fg.optimize_flow()

    # - Get the matching index
    matching_index = np.vstack([(k[0] - 1, k[1] - 1) for k in fg.lineage_dict if (k[0] != 0) and (k[1] != 0)]) # re-index

    return matching_index

# - To use ransac implementation from skimage algorithm for the vertex mapping problem, we need to define an affine
#   transformation class.
class AffineTransform3D(ProjectiveTransform):
    """Affine transformation.
    Has the following form::
        X = a0*x + a1*y + a2 =
          = sx*x*cos(rotation) - sy*y*sin(rotation + shear) + a2
        Y = b0*x + b1*y + b2 =
          = sx*x*sin(rotation) + sy*y*cos(rotation + shear) + b2
    where ``sx`` and ``sy`` are scale factors in the x and y directions,
    and the homogeneous transformation matrix is::
        [[a0  a1  a2]
         [b0  b1  b2]
         [0   0    1]]
    In 2D, the transformation parameters can be given as the homogeneous
    transformation matrix, above, or as the implicit parameters, scale,
    rotation, shear, and translation in x (a2) and y (b2). For 3D and higher,
    only the matrix form is allowed.
    In narrower transforms, such as the Euclidean (only rotation and
    translation) or Similarity (rotation, translation, and a global scale
    factor) transforms, it is possible to specify 3D transforms using implicit
    parameters also.
    Parameters
    ----------
    matrix : (D+1, D+1) array, optional
        Homogeneous transformation matrix. If this matrix is provided, it is an
        error to provide any of scale, rotation, shear, or translation.
    scale : {s as float or (sx, sy) as array, list or tuple}, optional
        Scale factor(s). If a single value, it will be assigned to both
        sx and sy. Only available for 2D.
        .. versionadded:: 0.17
           Added support for supplying a single scalar value.
    rotation : float, optional
        Rotation angle in counter-clockwise direction as radians. Only
        available for 2D.
    shear : float, optional
        Shear angle in counter-clockwise direction as radians. Only available
        for 2D.
    translation : (tx, ty) as array, list or tuple, optional
        Translation parameters. Only available for 2D.
    dimensionality : int, optional
        The dimensionality of the transform. This is not used if any other
        parameters are provided.
    Attributes
    ----------
    params : (D+1, D+1) array
        Homogeneous transformation matrix.
    Raises
    ------
    ValueError
        If both ``matrix`` and any of the other parameters are provided.
    """

    def __init__(self, dimensionality=3):
        # these parameters get overwritten if a higher-D matrix is given
        self._coeffs = range(dimensionality * (dimensionality + 1))
        self.params = np.eye(dimensionality + 1)

    def _apply_mat(self, coords, matrix):
        ndim = matrix.shape[0] - 1
        coords = np.array(coords, copy=False, ndmin=2)

        src = np.concatenate([coords, np.ones((coords.shape[0], 1))], axis=1)
        dst = src @ matrix.T

        # below, we will divide by the last dimension of the homogeneous
        # coordinate matrix. In order to avoid division by zero,
        # we replace exact zeros in this column with a very small number.
        dst[dst[:, ndim] == 0, ndim] = np.finfo(float).eps
        # rescale to homogeneous coordinates
        dst[:, :ndim] /= dst[:, ndim:ndim+1]

        return dst[:, :ndim]

    def __call__(self, coords):
        """Apply forward transformation.
        Parameters
        ----------
        coords : (N, D) array
            Source coordinates.
        Returns
        -------
        coords_out : (N, D) array
            Destination coordinates.
        """
        return self._apply_mat(coords, self.params)

    def residuals(self, src, dst):
        """Determine residuals of transformed destination coordinates.
        For each transformed source coordinate the euclidean distance to the
        respective destination coordinate is determined.
        Parameters
        ----------
        src : (N, 2) array
            Source coordinates.
        dst : (N, 2) array
            Destination coordinates.
        Returns
        -------
        residuals : (N, ) array
            Residual for coordinate.
        """
        return np.sqrt(np.sum((self(src) - dst) ** 2, axis=1))

    def estimate(self, src, dst, weights=None):
        """Estimate the transformation from a set of corresponding points.
        You can determine the over-, well- and under-determined parameters
        with the total least-squares method.
        Number of source and destination coordinates must match.
        The transformation is defined as::
            X = (a0*x + a1*y + a2) / (c0*x + c1*y + 1)
            Y = (b0*x + b1*y + b2) / (c0*x + c1*y + 1)
        These equations can be transformed to the following form::
            0 = a0*x + a1*y + a2 - c0*x*X - c1*y*X - X
            0 = b0*x + b1*y + b2 - c0*x*Y - c1*y*Y - Y
        which exist for each set of corresponding points, so we have a set of
        N * 2 equations. The coefficients appear linearly so we can write
        A x = 0, where::
            A   = [[x y 1 0 0 0 -x*X -y*X -X]
                   [0 0 0 x y 1 -x*Y -y*Y -Y]
                    ...
                    ...
                  ]
            x.T = [a0 a1 a2 b0 b1 b2 c0 c1 c3]
        In case of total least-squares the solution of this homogeneous system
        of equations is the right singular vector of A which corresponds to the
        smallest singular value normed by the coefficient c3.
        Weights can be applied to each pair of corresponding points to
        indicate, particularly in an overdetermined system, if point pairs have
        higher or lower confidence or uncertainties associated with them. From
        the matrix treatment of least squares problems, these weight values are
        normalised, square-rooted, then built into a diagonal matrix, by which
        A is multiplied.
        In case of the affine transformation the coefficients c0 and c1 are 0.
        Thus the system of equations is::
            A   = [[x y 1 0 0 0 -X]
                   [0 0 0 x y 1 -Y]
                    ...
                    ...
                  ]
            x.T = [a0 a1 a2 b0 b1 b2 c3]
        Parameters
        ----------
        src : (N, 2) array
            Source coordinates.
        dst : (N, 2) array
            Destination coordinates.
        weights : (N,) array, optional
            Relative weight values for each pair of points.
        Returns
        -------
        success : bool
            True, if model estimation succeeds.
        """

        n, d = src.shape

        src_matrix, src = _center_and_normalize_points(src)
        dst_matrix, dst = _center_and_normalize_points(dst)
        if not np.all(np.isfinite(src_matrix + dst_matrix)):
            self.params = np.full((d, d), np.nan)
            return False

        # params: a0, a1, a2, b0, b1, b2, c0, c1
        A = np.zeros((n * d, (d + 1) ** 2))
        # fill the A matrix with the appropriate block matrices; see docstring
        # for 2D example — this can be generalised to more blocks in the 3D and
        # higher-dimensional cases.
        for ddim in range(d):
            A[ddim * n: (ddim + 1) * n, ddim * (d + 1): ddim * (d + 1) + d] = src
            A[ddim * n: (ddim + 1) * n, ddim * (d + 1) + d] = 1
            A[ddim * n: (ddim + 1) * n, -d - 1:-1] = src
            A[ddim * n: (ddim + 1) * n, -1] = -1
            A[ddim * n: (ddim + 1) * n, -d - 1:] *= -dst[:, ddim:(ddim + 1)]

        # Select relevant columns, depending on params
        A = A[:, list(self._coeffs) + [-1]]

        # Get the vectors that correspond to singular values, also applying
        # the weighting if provided
        if weights is None:
            _, _, V = np.linalg.svd(A)
        else:
            W = np.diag(np.tile(np.sqrt(weights / np.max(weights)), d))
            _, _, V = np.linalg.svd(W @ A)

        # if the last element of the vector corresponding to the smallest
        # singular value is close to zero, this implies a degenerate case
        # because it is a rank-defective transform, which would map points
        # to a line rather than a plane.
        if np.isclose(V[-1, -1], 0):
            return False

        H = np.zeros((d + 1, d + 1))
        # solution is right singular vector that corresponds to smallest
        # singular value
        H.flat[list(self._coeffs) + [-1]] = - V[-1, :-1] / V[-1, -1]
        H[d, d] = 1

        # De-center and de-normalize
        H = np.linalg.inv(dst_matrix) @ H @ src_matrix

        # Small errors can creep in if points are not exact, causing the last
        # element of H to deviate from unity. Correct for that here.
        H /= H[-1, -1]

        self.params = H

        return True

def _center_and_normalize_points(points):
    """Center and normalize image points.
    The points are transformed in a two-step procedure that is expressed
    as a transformation matrix. The matrix of the resulting points is usually
    better conditioned than the matrix of the original points.
    Center the image points, such that the new coordinate system has its
    origin at the centroid of the image points.
    Normalize the image points, such that the mean distance from the points
    to the origin of the coordinate system is sqrt(D).
    If the points are all identical, the returned values will contain nan.
    Parameters
    ----------
    points : (N, D) array
        The coordinates of the image points.
    Returns
    -------
    matrix : (D+1, D+1) array
        The transformation matrix to obtain the new points.
    new_points : (N, D) array
        The transformed image points.
    References
    ----------
    .. [1] Hartley, Richard I. "In defense of the eight-point algorithm."
           Pattern Analysis and Machine Intelligence, IEEE Transactions on 19.6
           (1997): 580-593.
    """
    n, d = points.shape
    centroid = np.mean(points, axis=0)

    centered = points - centroid
    rms = np.sqrt(np.sum(centered ** 2) / n)

    # if all the points are the same, the transformation matrix cannot be
    # created. We return an equivalent matrix with np.nans as sentinel values.
    # This obviates the need for try/except blocks in functions calling this
    # one, and those are only needed when actual 0 is reached, rather than some
    # small value; ie, we don't need to worry about numerical stability here,
    # only actual 0.
    if rms == 0:
        return np.full((d + 1, d + 1), np.nan), np.full_like(points, np.nan)

    norm_factor = np.sqrt(d) / rms

    part_matrix = norm_factor * np.concatenate(
        (np.eye(d), -centroid[:, np.newaxis]), axis=1
    )
    matrix = np.concatenate(
        (part_matrix, [[0, ] * d + [1]]), axis=0
    )

    points_h = np.row_stack([points.T, np.ones(n)])

    new_points_h = (matrix @ points_h).T

    new_points = new_points_h[:, :d]
    new_points /= new_points_h[:, d:]

    return matrix, new_points

