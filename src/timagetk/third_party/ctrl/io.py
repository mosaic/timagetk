#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
#       File contributor(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#           Vincent Mirabet <vincent.mirabet@ens-lyon.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#           Manuel Petit <manuel.petit@inria.fr>
#
# ------------------------------------------------------------------------------

from os.path import exists

import pandas as pd
from timagetk.third_party.ctrl.lineage import AddAncestorError
from timagetk.third_party.ctrl.lineage import AddDescandantError
from timagetk.third_party.ctrl.lineage import Lineage

from timagetk.bin.logger import get_logger
log = get_logger(__name__)

#: The default lineage format.
DEFAULT_LINEAGE_FMT = 'basic'
#: The list of known formats.
AVAILABLE_LINEAGE_FMT = ['basic', 'dict', 'marsalt']
#: The default separator used with 'basic' format.
DEFAULT_SEP = " "


def write_lineage(lineage, filename, sep=DEFAULT_SEP):
    """Write a lineage file as a list of ids separated by `sep`.

    Parameter
    ---------
    lineage : Lineage
        The lineage to save.
    filename : str
        Path to file to write.
    sep : str, optional
        The separator between cell ids, a space character by default.

    See Also
    --------
    DEFAULT_SEP

    """
    f = open(filename, 'w')

    for k, v in lineage.items():
        f.write(str(k) + sep + sep.join(map(str, v)) + '\n')

    f.close()
    return "Done saving '{}'!".format(filename)


def read_lineage(filename, fmt=None, sep=DEFAULT_SEP):
    """Read a lineage, formatting might be specified.

    Parameters
    ----------
    filename : str
        The path to a lineage file.
    fmt : {'basic', 'dict', 'marsalt'}
        The format name used to choose the correct reader to parse lineage file.
    sep : str, optional
        The separator between cell ids for 'basic' format, space by default.

    Returns
    -------
    Lineage
        The lineage object built from the file.

    Raises
    ------
    OSError
        If the given file path does not exists.

    See Also
    --------
    AVAILABLE_LINEAGE_FMT
    DEFAULT_LINEAGE_FMT
    DEFAULT_SEP

    Example
    -------
    >>> from timagetk.third_party.ctrl.io import read_lineage
    >>> from timagetk.third_party.ctrl.util import shared_data
    >>> lineage_file = shared_data('p58_t0-1_lineage.txt', subdir='lineage')
    >>> lineage = read_lineage(lineage_file, fmt='basic', sep=" ")
    >>> type(lineage)
    ctrl.lineage.Lineage
    >>> print(lineage)

    """
    try:
        assert exists(filename)
    except AssertionError:
        raise OSError("Given file does not exists: {}".format(filename))

    if fmt is None:
        fmt = _guess_format(filename)

    log.info(f"Loading lineage from file '{filename}'...")
    if fmt == "basic":
        lineage = _read_basic(filename, sep)
    elif fmt == "dict":
        lineage = _read_dict(filename)
    elif fmt == "marsalt":
        lineage = _read_marsalt(filename)
    else:
        msg = "Known lineage formats are: {}".format(AVAILABLE_LINEAGE_FMT)
        raise ValueError(msg)

    return lineage

def _guess_format(lineage_file):
    """Try to guess the lineage file format."""
    with open(lineage_file, 'r') as f:
        first_line = f.readline()
        if first_line.startswith('NombreDeCellules'):
            return 'marsalt'
        elif ":" in first_line:
            return 'dict'
        else:
            return 'basic'

def _read_basic(filename, sep=DEFAULT_SEP):
    """Create the cell lineage dictionary from a "basic lineage file".

    Parameter
    ---------
    filename : str
        Path to the lineage file to read.
    sep : str, optional
        The separator between cell ids, space by default.

    Returns
    -------
    ctrl.lineage.Lineage
        The lineage instance, with ancestors ids as keys associated to a list of descendants ids.

    See Also
    --------
    DEFAULT_SEP

    Notes
    -----
    A *basic lineage file* is formatted as follows:

      - each line indicate a different cell lineage that are `sep` separated
      - the fist element is the ancestor cell id
      - the following elements are the descendant cell id(s)

    For example, if one line is ``"5 3 6"``, the mother cell id is ``5``, and it has 2 daughter cells ``3`` & ``6``.

    """
    f = open(filename, 'r')
    lines = f.readlines()
    lineage = Lineage()
    for line in lines:
        numbers = line.split(sep=sep)
        # - We make sure of the uniqueness of the lineage: each mother has been associated ONCE!
        m_id = int(numbers[0])
        c_ids = [int(i) for i in numbers[1:]]
        try:
            lineage.add_lineage(m_id, c_ids)
        except AddAncestorError as e:
            log.critical(e)
            log.info("Removing lineage definition for this ancestor...")
            lineage.remove_lineage(ancestor=m_id)
        except AddDescandantError as e:
            log.critical(e)
            log.info("Removing lineage definition for these descendants...")
            lineage.remove_lineage(descendant=c_ids)

    f.close()

    return lineage


def _read_dict(filename):
    """Create the cell lineage dictionary from a "dict lineage file".

    Parameter
    ---------
    filename : str
        Path to the lineage file to read.

    Returns
    -------
    ctrl.lineage.Lineage
        The lineage instance, with ancestors ids as keys associated to a list of descendants ids.

    Notes
    -----
    A *dict lineage file* is formatted as follows:

      - each line indicate a different cell lineage that are `sep` separated
      - the fist element, separated by a  is the ancestor cell id
      - the following elements are the descendant cell id(s)

    For example, if one line is ``"5: 3, 6"``, the mother cell id is ``5``, and it has 2 daughter cells ``3`` & ``6``.

    """
    f = open(filename, 'r')
    lines = f.readlines()
    lineage = Lineage()
    for line in lines:
        ancestor_id, descentant_ids = line.split(":")
        ancestor_id = int(ancestor_id)
        descentant_ids = descentant_ids.split(",")
        descentant_ids = [int(i) for i in descentant_ids]
        try:
            lineage.add_lineage(ancestor_id, descentant_ids)
        except AddAncestorError as e:
            log.critical(e)
            log.info("Removing lineage definition for this ancestor...")
            lineage.remove_lineage(ancestor=ancestor_id)
        except AddDescandantError as e:
            log.critical(e)
            log.info("Removing lineage definition for these descendants...")
            lineage.remove_lineage(descendant=descentant_ids)

    f.close()

    return lineage


def _read_marsalt(filename):
    """Create the cell lineage dictionary from a "marsalt lineage file".

    Parameter
    ---------
    filename: str
        path to the lineage file to read.

    Returns
    -------
    ctrl.lineage.Lineage
        The lineage instance, with ancestors ids as keys associated to a list of descendants ids.

    """
    lineage = Lineage()
    with open(filename, "r") as f:
        # -- in case the file is a python module that encodes the mapping
        # as a dictionary. We simply evaluate the second part of the assignment
        # operator ( thus the split("=")[1] ) --
        try:
            lineage = Lineage(eval(f.read().split("=")[1]))
        except:
            # --rewind and try classical read. --
            f.seek(0)

        lines = f.readlines()
        # -- Find out if we are looking at a mapping in the Romain Fernandez format --
        rf_style = False
        for l in lines:
            if "NombreDeCellules" in l:
                rf_style = True
                break

        if rf_style:  # is RomainFernandez format
            for l in lines[1:]:
                if not "->" in l:
                    continue
                colon = l.index(":")
                arrow = l.index(">")
                m_id = eval(l[:colon])
                c_ids = list(eval(l[arrow + 1:])[:-1])
                try:
                    lineage.add_lineage(m_id, c_ids)
                except AddAncestorError as e:
                    log.critical(e)
                    log.info("Removing lineage definition for this ancestor...")
                    lineage.remove_lineage(ancestor=m_id)
                except AddDescandantError as e:
                    log.critical(e)
                    log.info("Removing lineage definition for these descendants...")
                    lineage.remove_lineage(descendant=c_ids)

        else:  # raw text (Mirabet Style)
            for l in lines:
                if (l == "\n") or (l.startswith("#")): continue
                # - Avoid sub_lineage and iterative list creation
                if "[" in l:
                    l = l.replace("[", "")
                    l = l.replace("]", "")
                # - Avoid commentary marked with "#":
                l = l.rsplit("#")[0]
                # - Avoid SyntaxError if '*' marker in the lineage file.
                try:
                    values = list(eval(l.replace(" ", ",")))
                except SyntaxError:
                    l = l.replace("*", "")
                    values = list(eval(l.replace(" ", ",")))
                if len(values) <= 1:
                    continue
                m_id = values[0]
                c_ids = values[1:]
                try:
                    lineage.add_lineage(m_id, c_ids)
                except AddAncestorError as e:
                    log.critical(e)
                    log.info("Removing lineage definition for this ancestor...")
                    lineage.remove_lineage(ancestor=m_id)
                except AddDescandantError as e:
                    log.critical(e)
                    log.info("Removing lineage definition for these descendants...")
                    lineage.remove_lineage(descendant=c_ids)

    return lineage


def to_morphonet(df, type='quantitative'):
    """ Convert a dataframe to a string that can be upload in morphonet to visualize lineage, group or quantitative
        information

        Morphonet color that can be used to labelled lineage correctness (with rgb values):
        Yellow = 3 (some) | [255, 255, 0]
        Red = 65 (none) | [186, 9, 0]
        Green = 107 (correct) | [30, 110, 0]
        Dark = 31 (missing) | [0, 0, 53]


    Parameters
    ----------
    df: dataframe
        dataframe that contains the information. dataframe should be organized as follows:

            time | label |   str     --> columns order for the type 'group' (time, label, group)
             0       25       L1
             1       46       L2
            ...      ...      ...

            time | label |    scalar     --> columns order for the type 'quantitative' (time, label, quantitative info)
             0       25         0.2
             1       46         0.6
            ...      ...        ...

            time (mother) | label (mother) |  time (daughter) |  label (daughter)   --> columns order for the type 'lineage'
                   0               25                 1                   26
                   1               46                 2                   45
                  ...              ...               ...                  ...
    type: str; optional
        type of the dataframe : 'group','lineage','quantitative'

    Returns
    -------
        info_to_upload: str
            string corresponding to the dataframe

    Example
    -------
    >>> import pandas as pd
    >>> from timagetk.third_party.ctrl.io import to_morphonet
    # Example with quantitative data
    >>> dict_group = [{'time':0,'label':5,'s':0.2},{'time':0,'label':15,'s':0.1}, {'time':0,'label':2,'s':0.15}, {'time':2,'label':56,'s':0.2}]
    >>> df = pd.DataFrame(dict_group)
    >>> to_morphonet(df, 'quantitative')
    # Example with group data
    >>> dict_group = [{'time':0,'label':5,'s':'L1'},{'time':0,'label':15,'s':'L2'}, {'time':0,'label':2,'s':'L1'}, {'time':2,'label':56,'s':'L1'}]
    >>> df = pd.DataFrame(dict_group)
    >>> to_morphonet(df, 'group')
    """

    try:
        assert isinstance(df, pd.DataFrame)
        assert type in ['lineage', 'group', 'quantitative', 'selection']
    except:
        raise ValueError

    if type == 'lineage':
        # - get the columns name and cast datatype
        try:
            mother_time, mother_label, daughter_time, daughter_label = df.columns
            df[mother_time] = df[mother_time].astype('int')
            df[mother_label] = df[mother_label].astype('int')
            df[daughter_time] = df[daughter_time].astype('int')
            df[daughter_label] = df[daughter_label].astype('int')
        except:
            raise ValueError

        info_to_upload = 'type:time\n'

        # - loop over the rows and create the output string
        for idx, row in df.iterrows():
            info_to_upload += str(row[mother_time]) + ','
            info_to_upload += str(row[mother_label]) + ':'
            info_to_upload += str(row[daughter_time]) + ','
            info_to_upload += str(row[daughter_label]) + '\n'

    elif type == 'group':
        # - get the columns name and cast datatype
        try:
            time, label, val = df.columns
            df[time] = df[time].astype('int')
            df[label] = df[label].astype('int')
            df[val] = df[val].astype('str')
        except:
            raise ValueError

        info_to_upload = 'type:group\n'

        # - loop over the rows and create the output string
        for idx, row in df.iterrows():
            info_to_upload += str(row[time]) + ','
            info_to_upload += str(row[label]) + ':'
            info_to_upload += row[val] + '\n'

    elif type == 'quantitative':
        # - get the columns name and cast datatype
        try:
            time, label, val = df.columns
            df[time] = df[time].astype('int')
            df[label] = df[label].astype('int')
            df[val] = df[val].astype('float')
        except:
            raise ValueError

        info_to_upload = 'type:float\n'

        # - loop over the rows and create the output string
        for idx, row in df.iterrows():
            info_to_upload += str(int(row[time])) + ','
            info_to_upload += str(int(row[label])) + ':'
            info_to_upload += str(row[val]) + '\n'

    elif type == 'selection':
        # - get the columns name and cast datatype
        try:
            time, label, val = df.columns
            df[time] = df[time].astype('int')
            df[label] = df[label].astype('int')
            df[val] = df[val].astype('int')
        except:
            raise ValueError

        info_to_upload = 'type:selection\n'

        # - loop over the rows and create the output string
        for idx, row in df.iterrows():
            info_to_upload += str(int(row[time])) + ','
            info_to_upload += str(int(row[label])) + ':'
            info_to_upload += str(int(row[val])) + '\n'

    return info_to_upload
