#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
#       File contributor(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#           Florent Papini <florent.papini@ens-lyon.fr>
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#           Manuel Petit <manuel.petit@inria.fr>
#
# ------------------------------------------------------------------------------

import numpy as np
from pandas.core.common import flatten

from timagetk.bin.logger import get_logger
log = get_logger(__name__)

class AddAncestorError(Exception):
    """Raised when an ancestor cannot be added to a Lineage.

    The attributes `element', `row', `property', and `message' give additional information.
    """

    def __init__(self, message, element=None, row=None, prop=None):
        self.message = message
        self.element = element
        self.row = row
        self.prop = prop

        s = ''
        if self.element:
            s += 'element %r: ' % self.element.name
        if self.row is not None:
            s += 'row %d: ' % self.row
        if self.prop:
            s += 'property %r: ' % self.prop.name
        s += self.message

        Exception.__init__(self, s)

    def __repr__(self):
        return ('AddAncestorError(%r, element=%r, row=%r, prop=%r)' % self.message, self.element, self.row, self.prop)


class AddDescandantError(Exception):
    """Raised when an ancestor cannot be added to a Lineage.

    The attributes `element', `row', `property', and `message' give additional information.
    """

    def __init__(self, message, element=None, row=None, prop=None):
        self.message = message
        self.element = element
        self.row = row
        self.prop = prop

        s = ''
        if self.element:
            s += 'element %r: ' % self.element.name
        if self.row is not None:
            s += 'row %d: ' % self.row
        if self.prop:
            s += 'property %r: ' % self.prop.name
        s += self.message

        Exception.__init__(self, s)

    def __repr__(self):
        return ('AddDescandantError(%r, element=%r, row=%r, prop=%r)' % self.message, self.element, self.row, self.prop)


def lineage_resampling(lineage_list, resampling_index, **kwargs):
    """Get new lineages from temporal down-sampling.

    Parameters
    ----------
    lineage_list: list
        list of lineages (filenames or dictionaries);
    resampling_index: list
        indexes of lineages to keep, extend in between (if necessary).

    Returns
    -------
    list
        List of resampled lineage(s).

    Examples
    --------
    >>> from timagetk.third_party.ctrl.lineage import Lineage
    >>> from timagetk.third_party.ctrl.lineage import lineage_resampling
    >>> l01 = Lineage({2: [3, 4]})
    >>> l12 = Lineage({3: [5], 4: [6]})
    >>> l02 = lineage_resampling([l01, l12], [0, 2])[0]
    >>> print(l02)
    {2: [5, 6]}

    >>> l23 = Lineage({5: [7, 8], 6: [9, 10]})
    >>> l03 = lineage_resampling([l01, l12, l23], [0, 3])[0]
    >>> print(l03)
    {2: [7, 8, 9, 10]}

    >>> l34 = Lineage({7: [11], 8: [12, 13], 9: [14], 10: [15, 16]})
    >>> l02, l24 = lineage_resampling([l01, l12, l23, l34], [0, 2, 4])
    >>> print(l24)
    {5: [11, 12, 13], 6: [14, 15, 16]}

    """
    verbose = kwargs.get('verbose', False)
    N_lin = len(lineage_list)
    # Check all required index (`resampling_index`) are accessible in the `lineage_list`:
    assert np.all([idx <= N_lin for idx in resampling_index])

    # Handle Lineage objects creation from `lineage_list`:
    for n, lin in enumerate(lineage_list):
        if isinstance(lin, str):
            lineage_list[n] = Lineage(lin)
        elif isinstance(lin, Lineage):
            lineage_list[n] = lin
        else:
            try:
                lineage_list[n] = Lineage(lin)
            except:
                msg = "Error loading the lineage {}/{}, ABORTING!"
                raise OSError(msg.format(n + 1, N_lin))

    resampled_lin = []
    for b, e in zip(resampling_index[:-1], resampling_index[1:]):
        if verbose:
            log.info("Concatenation of lineages index {} to {}...".format(b, e))
        for i in range(b, e):
            if i == b:
                tmp_lin = lineage_list[i]
            else:
                tmp_lin = extend_successive_lineage(tmp_lin, lineage_list[i])
        resampled_lin.append(tmp_lin)

    return resampled_lin


def extend_successive_lineage(lin_01, lin_12, flatten=True, **kwargs):
    """Extend lineage 0->1 with lineage 1->2 to return lineage 0->2.

    Parameters
    ----------
    lin_01: Lineage
        a lineage object
    lin_12: Lineage
        a lineage object
    flatten: bool
        if True, the extended lineage is flattened, else sub-lineage is kept (not-working yet!);

    Returns
    -------
    Lineage
        The extended lineage.

    Notes
    -----
    To keep sub-lineage change to ``flatten = False``.

    Example
    -------
    >>> from timagetk.third_party.ctrl.lineage import Lineage
    >>> from timagetk.third_party.ctrl.lineage import extend_successive_lineage
    >>> l01 = Lineage({2: [3, 4]})
    >>> l12 = Lineage({3: [5], 4: [6]})
    >>> l02 = extend_successive_lineage(l01, l12)
    >>> print(l02)
    Lineage object with 1 ancestor and 2 descendants.
    >>> l02
    {2: [5, 6]}
    >>> l02 = extend_successive_lineage(l01, l12, flatten=False)
    >>> print(l02)
    {2: [[5], [6]]}

    """
    flatten_list = lambda l: [item for sublist in l for item in sublist]

    lin_02 = {}
    missing_lineage = 0
    for a_cell in lin_01:
        try:
            if flatten:
                lin_02[a_cell] = list(flatten_list([lin_12[b_cell] for b_cell in lin_01[a_cell]]))
            else:
                lin_02[a_cell] = [lin_12[b_cell] for b_cell in lin_01[a_cell]]
        except:
            missing_lineage += 1

    if missing_lineage != 0:
        log.warning(f"Missing {missing_lineage}/{len(lin_01)} lineage extensions !")

    return Lineage(lin_02)


def extend_lineage(lineages, **kwargs):
    """Extent lineages over several successive intervals.

    Parameters
    ----------
    lineages : list
        List of lineages to extend. Can be files.

    Other Parameters
    ----------------
    fmt : {'basic', 'dict', 'marsalt'}
        The format name used to choose the correct reader to parse lineage file.
    sep : str, optional
        The separator between cell ids for 'basic' format, space by default.

    Returns
    -------
    Lineage
        the extended lineage

    Example
    -------
    >>> from timagetk.third_party.ctrl.lineage import Lineage
    >>> from timagetk.third_party.ctrl.lineage import extend_lineage
    >>> l01 = Lineage({2: [3, 4]})
    >>> l12 = Lineage({3: [5], 4: [6]})
    >>> l23 = Lineage({5: [7, 8], 6: [9]})
    >>> l03 = extend_lineage([l01, l12, l23])
    >>> print(l03)
    Lineage object with 1 ancestor and 3 descendants.
    >>> l03
    {2: [7, 8, 9]}

    """
    from timagetk.third_party.ctrl.io import read_lineage

    for n, lin in enumerate(lineages):
        if isinstance(lin, str):
            lineages[n] = read_lineage(lin, **kwargs)

    lineage = lineages[0]
    for lin in lineages[1:]:
        lineage = extend_successive_lineage(lineage, lin)

    return lineage

class Lineage(dict):
    """Define temporal relations between cell ids (labels) of two segmented images.

    Lineage rules:
     - an ancestor has at least one descendant;
     - a descendant can only have one ancestor;

    The two segmented images must be temporally related.

    TODO: make it sub-lineages capable? eg. more than dichotomic divisions {2: [3, [4, 5]]}, where cells #4 & #5 are the result of the division of the sibling cell of cell #3.
    """

    def __init__(self, input_lineage=None, **kwargs):
        """Constructor

        Parameters
        ----------
        input_lineage : dict, optional
            A mapping dictionary.

        Raises
        ------
        TypeError
            If the ``input_lineage`` is not a known type.

        Examples
        --------
        >>> from timagetk.third_party.ctrl.lineage import Lineage
        >>> lin = Lineage({2: [3, 4], 3: [5, 6]})
        >>> print(lin)
        {2: [3, 4], 3: [5, 6]}

        >>> lin = Lineage()
        >>> lin.add_lineage(2, [3, 4])
        >>> lin.add_lineage(3, [5, 6])
        >>> print(lin)
        {2: [3, 4], 3: [5, 6]}
        >>> lin.add_lineage(3, [7])
        ValueError: Given ancestor id 3 already exist in the Lineage object!
        It is associated to descendant(s): [5, 6]
        """
        super().__init__()
        self._inverted = None  # inverted dictionary

        if isinstance(input_lineage, dict):
            for anc, desc in input_lineage.items():
                self.add_lineage(anc, desc)
        elif input_lineage is None:
            pass
        else:
            msg = "Provide a dictionary or initialize an empty object!"
            msg += "Got {} as input."
            raise TypeError(msg.format(type(input_lineage)))

    def __str__(self):
        msg = f"Lineage object with {len(list(self.keys()))} ancestors and {len(list(flatten(self.values())))} descendants."
        return msg

    def add_lineage(self, ancestor, descendant):
        """Add an ancestor and its descendant(s) ids to the lineage.

        Parameters
        ----------
        ancestor : int
            Ancestor id to add to the lineage.
        descendant : list of int
            Descendant(s) id(s) of the given ancestor to add to the lineage.

        Raises
        ------
        ValueError
            If the ancestor id is already defined in the Lineage object.
            If a descendant id is already defined in the Lineage object.

        Example
        -------
        >>> from timagetk.third_party.ctrl.lineage import Lineage
        >>> lin = Lineage()
        >>> lin.add_lineage(2, [3, 4])
        >>> lin.add_lineage(3, [5, 6])
        >>> print(lin)
        {2: [3, 4], 3: [5, 6]}
        >>> lin.add_lineage(3, [7])
        ValueError: Given ancestor id 3 already exist in the Lineage object!
        It is associated to descendant(s): [5, 6]
        >>> lin.add_lineage(4, [6])
        ValueError: Given descendant id 6 already exist in the Lineage object!
        It is associated to ancestor: 3
        """
        try:
            assert ancestor not in self.keys()
        except AssertionError:
            msg = "Given ancestor id {} already exists in the Lineage object, "
            msg += "it is associated to descendant(s): {}"
            raise AddAncestorError(msg.format(ancestor, self[ancestor]))

        for desc in descendant:
            try:
                assert not self._known_descendant(desc)
            except AssertionError:
                msg = "Given descendant id {} already exists in the Lineage object, "
                msg += "it is associated to ancestor: {}"
                raise AddDescandantError(msg.format(desc, self._invert()[desc]))

        # Now that we are sure to safely add a new lineage entry, do it:
        self.update({ancestor: descendant})
        # Also update the 'inverted' dictionary:
        self._inverted = None

    def remove_lineage(self, ancestor=None, descendant=None, quiet=False):
        """Remove an ancestor OR a list of descendant from the lineage.

        Parameters
        ----------
        ancestor : int or list of int
            The ancestor cell label to remove from lineage.
        descendant : list(int)
            List of descendant cell labels to remove from lineage.

        Examples
        --------
        >>> from timagetk.third_party.ctrl.lineage import Lineage
        >>> lin = Lineage({2: [3, 4], 3: [5], 4: [6, 7]})
        >>> lin.remove_lineage(3)
        >>> print(lin)
        {2: [3, 4], 4: [6, 7]}
        >>> lin.remove_lineage(descendant=[6])
        >>> print(lin)
        {2: [3, 4]}
        >>> lin = Lineage({2: [3, 4], 3: [5], 4: [6, 7]})
        >>> lin.remove_lineage(2, 5)
        >>> print(lin)
        {4: [6, 7]}
        """
        if ancestor is not None:
            self._del_ancestor(ancestor, quiet)
        if descendant is not None:
            if isinstance(descendant, int):
                descendant = [descendant]
            try:
                assert isinstance(descendant, (list, tuple, np.array))
            except AssertionError:
                raise TypeError("Given descendants should be a list!")
            self._del_descendants(descendant, quiet)

    def _del_ancestor(self, ancestor, quiet):
        if isinstance(ancestor, list):
            return [self._del_ancestor(anc, quiet) for anc in ancestor]
        try:
            self.pop(ancestor)
        except KeyError:
            if not quiet:
                log.error(f"Unknown ancestor label {ancestor}!")
        else:
            if not quiet:
                log.info(f"Removed lineage for ancestor id {ancestor}!")

    def _del_descendants(self, descendants, quiet):
        for desc in descendants:
            if self._known_descendant(desc):
                ancestor = self._invert()[desc]
                if ancestor not in self.keys():  # we may have removed it already
                    continue
                self.pop(ancestor)
                if not quiet:
                    log.info(f"Removed lineage for ancestor id {ancestor}!")
            else:
                if not quiet:
                    log.error(f"Unknown descendant label {desc}!")

    def _known_descendant(self, descendant):
        """Check if given descendant is known, ie. already linked to an ancestor.

        Parameters
        ----------
        descendant : int
            The descendant id to test.

        Returns
        -------
        bool
            ``True`` if given descendant is known, else ``False``.
        """
        return descendant in self._invert()

    def _invert(self):
        """Invert the lineage dictionary.

        Returns
        -------
        dict
            The inverted lineage dictionary, ie. with descendants as keys.
        """
        if self._inverted is None:
            self._inverted = {desc: anc for anc, descendants in self.items() for desc in
                              descendants}
        return self._inverted

    def get_ancestors(self):
        """Returns the list of ancestors.

        Returns
        -------
        list
            The list of ancestor ids.

        Examples
        --------
        >>> from timagetk.third_party.ctrl.lineage import Lineage
        >>> lin = Lineage({2: [3, 4], 3: [5, 6]})
        >>> lin.get_ancestors()
        [2, 3]
        """
        return list(self.keys())

    def get_descendants(self):
        """Returns the list of descendants.

        Returns
        -------
        list
            The list of descendant ids.

        Example
        --------
        >>> from timagetk.third_party.ctrl.lineage import Lineage
        >>> lin = Lineage({2: [3, 4], 3: [5, 6]})
        >>> lin.get_descendants()
        [3, 4, 5, 6]
        """
        return list(self._invert().keys())

    def ancestor(self, cid):
        """Returns the ancestor of given descendant cell id.

        Examples
        --------
        >>> from timagetk.third_party.ctrl.lineage import Lineage
        >>> lin = Lineage({2: [3, 4], 3: [5, 6]})
        >>> lin.ancestor(3)
        2
        >>> lin.ancestor(4)
        2
        """
        try:
            return self._invert()[cid]
        except KeyError as e:
            # log.warning(e)
            return None

    def revert(self):
        """Revert the cell lineage to return a dictionary with descendants as keys."""
        return {d: m for m, daughters in self.items() for d in daughters}

    def copy_lineage(self):

        return Lineage(input_lineage=self.copy())

    def add_multiple_lineage(self, lineages):

        assert isinstance(lineages, Lineage)

        for ancestor, descendant in lineages.items():
            self.add_lineage(ancestor=ancestor, descendant=descendant)

    def check_ancestor_image(self, image):
        """Use the ancestor segmented image to check lineage instance.

        Parameters
        ----------
        image : timagetk.components.LabelledImage
            The ancestor segmented image.

        Notes
        -----
        Lineaged ancestors missing from the image will be removed!
        """
        ancestor_ids = self.get_ancestors()
        for aid in ancestor_ids:
            if aid not in image.labels():
                self.remove_lineage(ancestor=aid)

    def check_descendant_image(self, image):
        """Use the descendant segmented image to check lineage instance.

        Parameters
        ----------
        image : timagetk.components.LabelledImage
            The descendant segmented image.

        Notes
        -----
        Lineaged descendant missing from the image will be removed!
        """
        descendant_ids = self.get_descendants()
        for did in descendant_ids:
            if did not in image.labels():
                self.remove_lineage(descendant=did)
