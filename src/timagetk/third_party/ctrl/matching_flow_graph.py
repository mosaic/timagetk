#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
#       File contributor(s):
#           Guillaume Cerutti <guillaume.cerutti@ens-lyon.fr>
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#           Manuel Petit <manuel.petit@inria.fr>
#
# ------------------------------------------------------------------------------

"""
This module implement the MatchingFlowGraph class.
"""
import timeit

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from matplotlib import colors
from matplotlib.cm import get_cmap
from scipy.sparse import csc_matrix

from timagetk.bin.logger import get_logger
log = get_logger(__name__)

class MatchingFlowGraph(object):
    """Element matching using flow graph optimization.

    Build a bipartite flow graph based on a cost dictionary listing the costs
    of connecting elements *i* to *j*, where *i* belong to the left part of
    the bipartite graph and *j* to the right part.

    To ensure correct definition of node ids, we proceed to a relabelling.
    Initial node values are saved under the "label" attribute of each node.
    To known the type of the node, use the "group" attribute.
    Here is the groups list and their meaning:

      - **l**: the left group of the bipartite graph;
      - **r**: the right group of the bipartite graph;
      - **s**: the split group of the bipartite graph;
      - **m**: the merge group of the bipartite graph.

    Attributes
    ----------
    graph : nx.DiGraph
        The flow graph to optimize.
    source_node : int
        Id of the `source` node in the flow graph.
    sink_node : int
        Id of the `sink` node in the flow graph.
    appear_node : int
        Id of the `appear` node in the flow graph.
    disappear_node : int
        Id of the `disappear` node in the flow graph.
    lg2nid : dict
        Mapping dictionary matching (label, group) tuples to their node id.
    n_left: int
        The number of left nodes.
    n_right : int
        The number of right nodes.

    Examples
    --------
    >>> from timagetk.third_party.ctrl.matching_flow_graph import MatchingFlowGraph
    >>> cost_dict = {(1, 2): 0.9, (2, (2,3)): 0.1, (1, (2,3)): 0.01,  (3, 3): 0.91, (3, 5): 0.1, (4, 4): 0.05, (4, 1): 0.02, (1, 3): 0.8, (4, 5): 0.04, (4, 6): 0.91, (4, (5, 6)): 0.2 , (4, (4, 5, 6)): 0.8}
    >>> fg = MatchingFlowGraph(cost_dict)
    >>> appear_cost_dict = {2: 0.5, 3: 0.3, 4: 0.8, 6: 0.1}
    >>> fg.set_appear_cost(appear_cost_dict)
    >>> fg.optimize_flow()
    >>> fg.draw_optimal_graph()
    >>> fg.draw_incidence_matrix()

    >>> cost_dict = {(1, (1, 2)): 0.3, (1, 1): 0.8,  (2, 2): 0.8, (1, (1, 2, 3)): 0.01}
    >>> fg = MatchingFlowGraph(cost_dict)
    >>> fg.optimize_flow(force_networkx=True)
    >>> fg.draw_optimal_graph()
    >>> fg.optimize_flow(force_networkx=False)
    >>> fg.draw_optimal_graph()
    """

    def __init__(self, cost_dict, method='padfield', max_cost=None, scale=None, **kwargs):
        """Constructor.

        Parameters
        ----------
        cost_dict : dict
            {(i,j): cost_ij}: will create an edge between the left node i and the right node j
            {(i,(j1,j2)): cost_ij1j2}: will create a split node linking the left node i and the right nodes j1 and j2
            {(i,(j1,j2,j3)): cost_ij1j2j3}: will create a split node linking the left node i and the right nodes j1, j2 and j3
            {((i1,i2),j): cost_i1i2j}: will create a merge node linking the left nodes i1 and i2 to the right node j
            ...
        method: str, optional
            available methods are {'alt','padfield'}. default is 'padfield'.
        max_cost : int, optional
            define the maximal cost assigned to the edges
        scale: int, optional
            define the scaling that will be applied to all the costs in order to get only integer
            (defines the precision of the solver)

        Other Parameters
        ----------------
        ndiv : int
            maximal number of division expected (if using ALT method)
        appear_disappear_edge : bool
            If ``True``, create a flow graph with an edge between "appear" & "disappear" nodes.
            If ``False`, do not add this edge.
            If 'auto' (default), define automatically if it is required depending on the sets size:
            ``True if self.n_left != self.n_right``.
        auto_init : bool
            If ``True`` (default), the flow graph is initialized at constructor call.
            If ``False``, you have to call ``init_flow_graph()`` to initialize the flow graph.
        """
        # - Initialise empty attributes:
        self.graph = None
        self.source_node, self.sink_node = None, None
        self.appear_node, self.disappear_node = None, None

        self.lg2nid = {}
        self.n_left = None
        self.left_labels = []
        self.n_right = None
        self.right_labels = []
        self.flow_dict = {}
        self.nx_flow_dict = {}
        self.lineage_dict = {}

        self.incidence_matrix = None
        self.incidence_mapping = {}  # correspondence between row/col index of the IM and nodes/edges of the graph

        # - Assert that cost_dict is a dict organized as: {(left_node(s), right_node(s)):cost}
        try:
            assert isinstance(cost_dict, dict)
            assert all(type(i) is tuple for i in cost_dict.keys())
        except AssertionError:
            log.error("Parameter `cost_dict` should be a dictionary organized as {(left_node(s), right_node(s)):cost}")
            raise

        # - Initialise known attributes:
        self.cost_dict = cost_dict
        self.x = None

        if max_cost is None:
            self.max_cost = max(cost_dict.values())
        else:
            self.max_cost = max_cost

        if scale is None:
            self._set_cost_scale()
            log.info(f'Automatic detection of the cost precision, apply a scaling of {self.scale} * cost !')
        else:
            self.scale = scale

        self.method = method
        self.ndiv = kwargs.get('ndiv', 4)  # maximal number of division handle

        # - Initialize the flow graph if required:
        if kwargs.get('auto_init', True):
            self.init_flow_graph(add_appear_disappear_edge=kwargs.get('appear_disappear_edge', 'auto'))

    @staticmethod
    def _cost_dict_to_matching(cost_dict):
        """Extract the label matching from the cost dictionary.

        Parameters
        ----------
        cost_dict : dict
            {(i,j): cost_ij}

        Returns
        -------
        dict
            {i: [j]}
        """
        match = {l_i: [] for (l_i, _) in cost_dict.keys()}
        for (l_i, l_j) in cost_dict.keys():
            match[l_i].append(l_j)

        return match

    def flow_to_lineage(self):
        """ Return the lineage (start_node, end_node) of the current graph as well as the cost associated
        Important:
            1. sink and source node are discarded
            2. appear and disappear node are replaced by the label 0 (not_a_label)

        Returns
        -------
        dict
            (start_node, end_node):

            Elements of the list can be:
            - (0, 15): edge between the appear node and the right node n°15
            - (2, (2, 3)): edge between the left node n°2 and the right nodes n°2 and 3 (division behavior)
            - (5, 6): edge between the left node n°5 and the right node n°6
            - (3, 0): edge between the left node n°3 and the disappear node
            - ...
        """
        # - Check if a flow has already been optimize
        if not hasattr(self, 'flow_dict'):
            log.warning("You need to optimize a flow first!")
        else:
            # - Filter the flow dict to keep only the edges where a flow exists
            flow_dict = [(node_i, node_j) for node_i, v in self.flow_dict.items()
                         for node_j, flow in v.items() if flow != 0]
            lineage_dict = {}

            for left_id, right_id in flow_dict:
                # - Get the cost of this flow
                cost = self.graph[left_id][right_id]['weight'] / self.scale

                # - Check that the edges corresponds to an admissible edge for the lineage:
                #   * appear --> right
                #   * left --> right
                #   * left --> split (division)
                #   * left --> disappear
                left_grp = self.graph.nodes.data('group')[left_id]
                right_grp = self.graph.nodes.data('group')[right_id]

                if (left_grp, right_grp) == ('appear', 'r'):
                    left_node = 0
                    right_node = self.graph.nodes[right_id]['label']
                elif left_grp == 'l':
                    left_node = self.graph.nodes[left_id]['label']

                    if right_grp == 'disappear':
                        right_node = 0
                    elif right_grp in ['r', 's']:
                        right_node = self.graph.nodes[right_id]['label']
                    else:
                        continue
                else:
                    continue

                lineage_dict[(left_node, right_node)] = cost

            self.lineage_dict = lineage_dict

            return lineage_dict

    def get_lineage(self):
        """Get a lineage from the flow optimization

        Returns
        -------
        dict
            (left_label, right_label): cost
        """

    def relabel2node_ids(self):
        """Relabel the two sets of labels given in cost dictionary.

        Examples
        --------
        >>> from timagetk.third_party.ctrl.matching_flow_graph import MatchingFlowGraph
        >>> cost_dict = {
            (2, 2): 0.9,
            (2, 3): 0.1,
            (3, 3): 0.91,
            (3, 2): 0.1,
            (4, 4): 0.05,
            (4, 5): 0.04,
            (4, 6): 0.91
        }
        >>> fg = MatchingFlowGraph(cost_dict, auto_init=False)
        >>> fg.relabel2node_ids()
        >>> print("Number of left-labels: {}".format(fg.n_left))
        >>> print("Number of right-labels: {}".format(fg.n_right))
        """
        init_match = self._cost_dict_to_matching(self.cost_dict)

        flatten = lambda l: [item for sublist in l for item in sublist]
        self.left_labels = list(init_match.keys())
        self.right_labels = list(np.unique(flatten(init_match.values())))
        # Set the size of each parts of the flow graph:
        self.n_left = len(self.left_labels)
        self.n_right = len(self.right_labels)

        # Create list of tuples (label, group), with group in ['l', 'r']:
        label_tp_list = [(m, 'l') for m in self.left_labels] + [(d, 'r') for d in self.right_labels]

        # Initialize mapping dictionary matching (label, group) tuples to their node id:
        # Starts at 1 since "source id" is 0:
        self.lg2nid = dict(zip(label_tp_list, range(1, len(label_tp_list) + 1)))

    def special_nodes(self):
        return [self.source_node, self.sink_node, self.appear_node, self.disappear_node]

    def max_nid(self):
        """Get the maximum node id of the flow graph.

        Returns
        -------
        int
            The maximum node id of the flow graph
        """
        return np.max(self.graph.nodes)

    def init_flow_graph(self, add_appear_disappear_edge='auto'):
        """Initialize the flow graph.

        Parameters
        ----------
        add_appear_disappear_edge : bool
            If ``True``, create a flow graph with an edge between "appear" &
            "disappear" nodes. Forces ``appear_node`` & ``disappear_node`` to ``True``.
            If ``False``, do not add this edge.
            If 'auto' (default), define automatically if it is required
            depending on the sets size: ``True if self.n_left != self.n_right``.
        """
        # Get some information from the cost_dict: left/right labels
        # left side
        left_side = [lt for lt, _ in self.cost_dict.keys()]
        merge_tf = [type(lt) is tuple for lt in left_side]

        # right side
        right_side = [rt for _, rt in self.cost_dict.keys()]
        split_tf = [type(rt) is tuple for rt in right_side]

        # Assert that no couple contain split and merge at the same time
        try:
            assert all([sp and mg for sp, mg in zip(split_tf, merge_tf)]) is False
        except AssertionError:
            log.error("A couple can correspond to a split or merge behavior, never both at the same time!")
            raise

        # Function to flatten list of tuples and int
        flatten = lambda *n: (e for a in n
                              for e in (flatten(*a) if isinstance(a, (tuple, list)) else (a,)))

        # get unique labels
        self.left_labels = np.unique(list(flatten(left_side)))
        self.right_labels = np.unique(list(flatten(right_side)))

        # Set the size of each parts of the flow graph:
        self.n_left = len(self.left_labels)
        self.n_right = len(self.right_labels)

        # self.relabel2node_ids()
        self.graph = nx.DiGraph()

        # Add SOURCE node:
        self.source_node = 0
        self.graph.add_node(self.source_node, label=None, group='source')  # , demand=-max(self.n_left, self.n_right))

        # Add SINK node:
        self.sink_node = 1
        self.graph.add_node(self.sink_node, label=None, group='sink')  # , demand=max(self.n_left, self.n_right))

        # Define the first id of the left/right nodes
        start_id = 2

        # Depending on the number of left/right nodes and the appear_disappear input, Add APPEAR/DISAPPEAR nodes:
        if (add_appear_disappear_edge is True) | ((add_appear_disappear_edge == 'auto') &
                                                  ((self.n_left != self.n_right) | any(split_tf) | any(merge_tf))):
            # APPEAR node
            self.appear_node = 2
            self.graph.add_node(self.appear_node, label=0, group='appear')

            # DISAPPEAR node
            self.disappear_node = 3
            self.graph.add_node(self.disappear_node, label=0, group='disappear')

            # redefine the first id
            start_id += 2

        # Create list of tuples (label, group), with group in ['l', 'r']:
        label_tp_list = [(m, 'l') for m in self.left_labels] + [(d, 'r') for d in self.right_labels]
        self.lg2nid = dict(zip(label_tp_list, range(start_id, len(label_tp_list) + start_id)))

        # Add LEFT & RIGHT nodes:
        self.graph.add_nodes_from([(nid, {'label': lab, 'group': g}) for (lab, g), nid in self.lg2nid.items()])

        # Add edges:
        # - Add edges between "source" node and "left group" nodes:
        if self.method == 'padfield':
            source_edge_list = [(self.source_node, self.lg2nid[(l, g)],
                                 {'capacity': 1, 'weight': 0}) for (l, g) in
                                self.lg2nid.keys() if g == 'l']
            self.graph.add_edges_from(source_edge_list)
        else:
            # - ALT: set the capacity to ndiv for all the edges
            source_edge_list = [(self.source_node, self.lg2nid[(l, g)],
                                 {'capacity': self.ndiv, 'weight': 0}) for (l, g) in
                                self.lg2nid.keys() if g == 'l']
            self.graph.add_edges_from(source_edge_list)

        # - Add edges between "right group" nodes and "sink" nodes:
        sink_edge_list = [(self.lg2nid[(l, g)], self.sink_node,
                           {'capacity': 1, 'weight': 0}) for (l, g) in
                          self.lg2nid.keys() if g == 'r']
        self.graph.add_edges_from(sink_edge_list)

        # - Get a subset of the cost_dict corresponding to the left-right edges only (no split, no merge)
        tf = [not (i or j) for i, j in zip(merge_tf, split_tf)]
        sub_cost_dict = {k: int(v * self.scale) for k, v, i in zip(self.cost_dict, self.cost_dict.values(), tf) if i}

        # - Add edges between "left group" node and "right group" nodes:
        cell_to_cell_edge_list = [(self.lg2nid[(i, 'l')], self.lg2nid[(j, 'r')],
                                   {'capacity': 1, 'weight': cost}) for
                                  (i, j), cost in sub_cost_dict.items()]
        self.graph.add_edges_from(cell_to_cell_edge_list)

        # - Add edges for "appear" and "disappear" nodes:
        if self.appear_node is not None:
            # - between "source" node and "appear" node:
            self.graph.add_edge(self.source_node, self.appear_node, weight=0,
                                capacity=self.n_right)

            # - between "disappear" node and "sink" node:
            self.graph.add_edge(self.disappear_node, self.sink_node, weight=0,
                                capacity=self.n_left)

            # - between "appear" node and "right group" nodes:
            appear_edge_list = [(self.appear_node, self.lg2nid[(l, g)],
                                 {'capacity': 1, 'weight': int(self.scale * self.max_cost)}) for
                                (l, g) in self.lg2nid.keys() if g == 'r']
            self.graph.add_edges_from(appear_edge_list)

            # - between "left group" nodes and "disappear" node:
            disappear_edge_list = [(self.lg2nid[(l, g)], self.disappear_node,
                                    {'capacity': 1, 'weight': int(self.scale * self.max_cost)}) for
                                   (l, g) in self.lg2nid.keys() if g == 'l']
            self.graph.add_edges_from(disappear_edge_list)

            # - between "appear" node and "disappear" node:
            if self.method == 'padfield':
                self.graph.add_edge(self.appear_node, self.disappear_node,
                                    capacity=min(self.n_left, self.n_right), weight=0)
            else:
                self.graph.add_edge(self.appear_node, self.disappear_node,
                                    capacity=self.n_left + self.n_right, weight=0)

        # - Add split nodes and edges if needed
        if any(split_tf) and self.method == 'padfield':
            # - Get a subset of the cost_dict corresponding to the split nodes and edges only
            split_cost_dict = {k: int(self.scale * v) for k, v, i in
                               zip(self.cost_dict, self.cost_dict.values(), split_tf) if i}
            self._add_split_nodes(split_cost_dict)

        # - Add merge nodes and edges if needed
        if any(merge_tf) and self.method == 'padfield':
            # - Get a subset of the cost_dict corresponding to the merge nodes and edges only
            merge_cost_dict = {k: int(self.scale * v) for k, v, i in
                               zip(self.cost_dict, self.cost_dict.values(), merge_tf) if i}
            self._add_merge_nodes(merge_cost_dict)

        # - ALT method
        if self.method == 'alt':
            # - Set the demand at the nodes
            # TODO
            h = 1

    def _add_split_nodes(self, split_cost_dict):
        """Add split nodes to the flow graph.

        Parameters
        ----------
        split_cost_dict: dict {(i,(j1,j2,...)): cost_i_to_j1_2_...}
            keys indicated the i-eme left node connected to the j-eme right nodes through a split node

        Examples
        --------
        >>> from timagetk.third_party.ctrl.matching_flow_graph import MatchingFlowGraph
        >>> cost_dict = {
            (2, 2): 0.9,
            (2, 3): 0.1,
            (3, 3): 0.91,
            (3, 2): 0.1,
            (4, (4,5,6)): 0.05,
            (4, 5): 0.04,
            (4, 6): 0.91,
            (4,(1,2)): 0.87,
            (3,(1,2,3)):0.54,
            (2,(2,5)):0.1,
            (4,4):0.2}
        >>> fg = MatchingFlowGraph(cost_dict)
        >>> fg.draw_flow_graph()
        """
        # - Assert that the split_cost_dict is organized as {(int,tuple) : cost}
        try:
            assert all([type(i) is int for i, _ in split_cost_dict.keys()]) is True
            assert all([type(i) is tuple for _, i in split_cost_dict.keys()]) is True
        except AssertionError:
            raise ValueError("The split_cost_dict should be organized as {(int,tuple) : cost}")

        # - Assert that the APPEAR and DISAPPEAR nodes are available
        try:
            assert self.appear_node is not None
        except AssertionError:
            raise ValueError("The APPEAR and DISAPPEAR nodes are missing, cannot set any SPLIT nodes!")

        # - Get the left and right sides
        lab_div = set([i for _, i in split_cost_dict.keys()])  # remove duplicates
        ndiv = [len(i) for i in lab_div]  # number of divisions

        # Save the split mapping to link the split node id to the right labels
        max_node_id = self.max_nid()
        self.split_label2node_ids = dict(zip(lab_div, range(max_node_id + 1, len(lab_div) + max_node_id + 1)))

        # Add SPLIT nodes to the graph:
        sl2nid = self.split_label2node_ids
        self.graph.add_nodes_from([(nid, {'label': slab, 'group': 's'}) for slab, nid in sl2nid.items()])

        # Add edges between "appear" node & each "split" nodes:
        appear_edge_list = [(self.appear_node, sl2nid[slab], {'capacity': div - 1, 'weight': 0})
                            for slab, div in zip(sl2nid, ndiv)]
        self.graph.add_edges_from(appear_edge_list)

        # Add edges between "left group" nodes & each "split" nodes:
        # cost = split_cost_dict.values()
        # left_labels = [self.lg2nid[(l, 'l')] for l, _ in split_cost_dict.keys()]

        left_split_edge_list = [(self.lg2nid[(lid, 'l')], sl2nid[rid], {'capacity': 1, 'weight': w})
                                for (lid, rid), w in split_cost_dict.items()]

        # left_split_edge_list = [
        #    (l, s, {'capacity': 1, 'weight': w}) for l, s, w in zip(left_labels, sl2nid.values(), cost)]
        self.graph.add_edges_from(left_split_edge_list)

        flatten = lambda *n: (e for a in n
                              for e in (flatten(*a) if isinstance(a, (tuple, list)) else (a,)))

        # Add edges between "split" nodes & corresponding "right" nodes:
        right_labels = [self.lg2nid[(r, 'r')] for r in list(flatten(list(lab_div)))]
        lab_div = list(flatten([[i] * j for i, j in zip(sl2nid.values(), ndiv)]))

        split_right_edge_list = [(s, r, {'capacity': 1, 'weight': 0}) for s, r in zip(lab_div, right_labels)]
        self.graph.add_edges_from(split_right_edge_list)

    def _add_merge_nodes(self, merge_cost_dict):
        """Add merge nodes to the flow graph.

        Parameters
        ----------
        merge_cost_dict: dict {((i1,i2,...), j): cost_i1_2_..._to_j}
            keys indicated the i-eme left nodes connected to the j-eme right node through a merge node

        Examples
        --------
        >>> from ctrl.matching_flow_graph import MatchingFlowGraph
        >>> cost_dict = {
            (2, 2): 0.9,
            (2, 3): 0.1,
            (3, 3): 0.91,
            (3, 2): 0.1,
            (4, 4): 0.05,
            (4, 5): 0.04,
            (4, 6): 0.91
        }
        >>> fg = MatchingFlowGraph(cost_dict)
        """
        raise NotImplementedError("Adding merge nodes is not available yet!")

    def optimize_flow(self, force_networkx=False, max_time=None, best_gap=None):
        """Optimize the flow graph using max flow min cost algorithm.

        Examples
        --------
        >>> from ctrl.matching_flow_graph import MatchingFlowGraph
        >>> cost_dict = {(2, 2): 0.9, (2, 3): 0.1, (3, 3): 0.91, (3, 2): 0.1, (4, 4): 0.05, (4, 5): 0.04, (4, 6): 0.91}
        >>> fg = MatchingFlowGraph(cost_dict)
        >>> fg.optimize_flow()
        >>> fg.max_flow_dict
        """
        log.info("Start to optimize the flow...")
        log.info(f"Maximal flow of the graph: {self.n_left + self.n_right}")

        G = self.graph
        # - Check if graph contains split nodes or not
        if (not hasattr(self, 'split_label2node_ids')) or force_networkx:
            log.info("Split nodes were detected in the graph.")
            log.info("Solve 'max flow min cost' problem using networkx built-in function.")

            if self.method == 'alt':
                flow_dict = nx.max_flow_min_cost(G, self.source_node, self.sink_node)
            else:
                flow_dict = nx.max_flow_min_cost(G, self.source_node, self.sink_node)

            # - Convert the output in flow_dict: (start_node, end_node): flow
            self.flow_dict = self.nx_flow_dict = flow_dict
            # - Get the associated lineage
            self.flow_to_lineage()
            # - Verbose
            log.info(f"Optimal flow was found with an associated cost of {self.get_cost_of_flow()}")
        else:
            log.info("Split nodes were detected in the graph.")
            log.info("Solve 'max flow min cost' problem using coupling constraints.")

            # - Get the coupling incidence matrix
            self.get_incidence_matrix()

            # - Get the costs
            costs = self.incidence_mapping['costs'] / self.scale

            # - Get the edges
            edges = self.incidence_mapping['edges']

            # - Set the LP problem
            A = self.incidence_matrix
            b = [0] * A.shape[0]  # sum of the flow vector

            log.info('Size of the constraints: ' + str(A.shape))

            # - Set the maximal flow constraint
            b[self.source_node] = -(self.n_right + self.n_left)
            b[self.sink_node] = self.n_right + self.n_left

            # - Set the boundaries conditions: (0, capacity)
            ub = [G.edges[ed]['capacity'] if type(ed) is tuple else G.edges[ed[0]]['capacity'] for ed in edges]

            # - Solve the LP problem using MIP solver under PuLP library
            # - Get the dimensions of the constraint matrix
            nrow, ncol = A.shape

            start = timeit.default_timer()

            log.info('Use the CBC solver...')
            # - Create the model
            opt_model = pulp.LpProblem(name='flow-problem', sense=pulp.LpMinimize)

            # - Create the variables
            x_vars = {j: pulp.LpVariable(cat=pulp.LpInteger, lowBound=0, upBound=ub[j], name="{0}".format(j))
                      for j in range(ncol)}

            # Set the constraints
            constraints = {i: opt_model.addConstraint(pulp.LpConstraint(e=pulp.lpSum(A[i, cx] * x_vars[cx]
                                                                                     for cx in np.nonzero(A[i, :])[1]),
                                                                        sense=pulp.LpConstraintEQ,
                                                                        rhs=b[i],
                                                                        name="{0}".format(i)))
                           for i in range(nrow)}

            # objective function
            objective = pulp.lpSum(x_vars[j] * costs[j] for j in range(ncol))

            # set the objective function
            opt_model.setObjective(objective)

            # solving with CBC
            opt_model.solve()

            # Get the solution vector
            x = {int(var.name): int(var.varValue) for var in opt_model.variables()}

            # Sort the solution vector
            x = [x[ix] for ix in range(ncol)]

            stop = timeit.default_timer()
            ti = np.round(stop - start, 1)
            log.info('Solved the min-cost max-flow problem in ' + str(ti) + 's!')

            self.x = x

            log.info("Optimal flow was found with an associated cost of " + str(sum(x * costs)))

            # - Convert the solution as a flow_dict
            flow_dict = {}
            for i, eds in enumerate(self.incidence_mapping['edges']):
                if type(eds) is tuple:
                    in_node, out_node = eds
                    if in_node not in flow_dict:
                        flow_dict[in_node] = {out_node: x[i]}
                    else:
                        flow_dict[in_node][out_node] = x[i]
                else:
                    for ed in eds:
                        in_node, out_node = ed
                        if in_node not in flow_dict:
                            flow_dict[in_node] = {out_node: x[i]}
                        elif out_node in flow_dict[in_node]:
                            flow_dict[in_node][out_node] = max(x[i], flow_dict[in_node][out_node])
                        else:
                            flow_dict[in_node][out_node] = x[i]

            self.flow_dict = flow_dict

            # - Get the associated lineage
            self.flow_to_lineage()

        return self.lineage_dict

    def get_cost_of_flow(self):
        """Return the cost of flow for optimized flow graph.

        Returns
        -------
        float
            The cost of flow after flow graph optimization.

        Examples
        --------
        >>> from ctrl.matching_flow_graph import MatchingFlowGraph
        >>> cost_dict = {
            (2, 2): 0.9,
            (2, 3): 0.1,
            (3, 3): 0.91,
            (3, 2): 0.1,
            (4, 4): 0.05,
            (4, 5): 0.04,
            (4, 6): 0.91
        }
        >>> fg = MatchingFlowGraph(cost_dict)
        >>> fg.optimize_flow()
        >>> fg.get_cost_of_flow()
        """
        if not self.flow_dict:
            log.warning("Call `optimize_flow` first!")
            return None

        if hasattr(self, 'nx_flow_dict'):
            cost_of_flow = nx.cost_of_flow(self.graph, self.flow_dict)

        return cost_of_flow

    def get_max_flow_dict(self):
        G = self.graph
        return {(G.nodes[node_i]["label"], G.nodes[node_j]["label"]): flow for node_i, v in
                self.nx_flow_dict.items() for node_j, flow in v.items() if flow != 0}

    def get_flow_dict(self):
        """Return matched label flow dictionary.

        Source and sink nodes are removed from the dictionary.

        Returns
        -------
        dict
            {(i, j): flow_ij}, where *i* & *j* are labels belonging to left & right
            groups, respectively and *flow_ij* the associated flow value.

        Examples
        --------
        >>> from ctrl.matching_flow_graph import MatchingFlowGraph
        >>> cost_dict = {(2, 2): 0.9, (2, 3): 0.1, (3, 3): 0.91, (3, 2): 0.1, (4, 4): 0.05, (4, 5): 0.04, (4, 6): 0.91}
        >>> fg = MatchingFlowGraph(cost_dict)
        >>> fg.optimize_flow()
        >>> fg.get_flow_dict()
        """
        if not self.nx_flow_dict:
            log.warning("Call `optimize_flow` first!")
            return None

        G = self.graph
        source = self.source_node
        sink = self.sink_node
        return {
            (G.nodes[node_i]["label"], G.nodes[node_j]["label"]): flow for node_i, v in
            self.nx_flow_dict.items() if node_i != source for
            node_j, flow in v.items() if flow != 0 and node_j != sink}

    def get_matching_dict(self):
        """Return matched label dictionary.

        Source, sink, appear & disappear nodes are removed from the dictionary.

        Returns
        -------
        dict
            matched label dictionary {i: j}, where *i* & *j* are labels belonging
            to left & right time, respectively.

        Examples
        --------
        >>> from ctrl.matching_flow_graph import MatchingFlowGraph
        >>> cost_dict = {
            (2, 2): 0.9,
            (2, 3): 0.1,
            (3, 3): 0.91,
            (3, 2): 0.1,
            (4, 4): 0.05,
            (4, 5): 0.04,
            (4, 6): 0.91
        }
        >>> fg = MatchingFlowGraph(cost_dict)
        >>> fg.optimize_flow()
        >>> fg.get_matching_dict()
        """
        if not self.nx_flow_dict:
            log.warning("Call `optimize_flow` first!")
            return None

        G = self.graph
        spec = self.special_nodes()

        md = {}
        for node_i, v in self.nx_flow_dict.items():
            if node_i in spec:
                continue
            for node_j, flow in v.items():
                if flow != 0 and node_j not in spec:
                    if G.nodes[node_i]["label"] in md:
                        if not isinstance(G.nodes[node_i]["label"], list):
                            G.nodes[node_i]["label"] = [G.nodes[node_i]["label"]]
                        md[G.nodes[node_i]["label"]].append(G.nodes[node_j]["label"])
                    else:
                        md[G.nodes[node_i]["label"]] = G.nodes[node_j]["label"]

        cp_md = md.copy()
        for anc, descs in cp_md.items():
            if isinstance(descs, list):
                for desc in descs:
                    if isinstance(desc, tuple) and desc in md:
                        assert len(descs) == 1
                        md[anc] = md.pop(desc)
            elif isinstance(descs, tuple) and descs in md:
                md[anc] = md.pop(descs)
            else:
                pass

        return md

    def get_disappear_list(self):
        """Returns the list of disappearing labels.

        Returns
        -------
        list
            List of disappearing labels.

        Examples
        --------
        >>> from ctrl.matching_flow_graph import MatchingFlowGraph
        >>> cost_dict = {
            (2, 2): 0.9,
            (2, 3): 0.1,
            (3, 3): 0.91,
            (3, 2): 0.1,
            (4, 4): 0.05,
            (4, 5): 0.04,
            (4, 6): 0.91
        }
        >>> fg = MatchingFlowGraph(cost_dict)
        >>> fg.optimize_flow()
        >>> fg.get_disappear_list()
        """
        if not self.nx_flow_dict:
            log.warning("Call `optimize_flow` first!")
            return None
        if not self.disappear_node:
            log.warning("No 'disappear' node in the flow graph!")
            return None

        G = self.graph
        appear = self.appear_node
        disappear = self.disappear_node

        return [G.nodes[node_i]["label"] for node_i, v in
                self.nx_flow_dict.items() if node_i != appear for node_j, flow in v.items() if
                flow != 0 and node_j == disappear]

    def get_appear_list(self):
        """Returns the list of appearing labels.

        Returns
        -------
        list
            List of disappearing labels.

        Examples
        --------
        >>> from ctrl.matching_flow_graph import MatchingFlowGraph
        >>> cost_dict = {
            (2, 2): 0.9,
            (2, 3): 0.1,
            (3, 3): 0.91,
            (3, 2): 0.1,
            (4, 4): 0.05,
            (4, 5): 0.04,
            (4, 6): 0.91
        }
        >>> fg = MatchingFlowGraph(cost_dict)
        >>> fg.optimize_flow()
        >>> fg.get_appear_list()
        """
        if not self.nx_flow_dict:
            log.warning("Call `optimize_flow` first!")
            return None
        if not self.appear_node:
            log.warning("No 'appear' node in the flow graph!")
            return None

        G = self.graph
        appear = self.appear_node
        disappear = self.disappear_node
        appear_flow = self.nx_flow_dict[appear]

        return [G.nodes[nid]["label"] for nid, flow in appear_flow.items() if flow != 0 and nid != disappear]

    def get_unmatched_list(self):
        """Get the list of unmatched labels by flow optimization.

        Returns
        -------
        list
            List the unmatched labels by flow optimization
        """
        pass

    def get_all_events(self):
        """List all occurring events.

        Returns
        -------
        list
            List of disappearing labels.
        list
            List of appearing labels.
        dict
            Dictionary of matched labels.

        Examples
        --------
        >>> from ctrl.matching_flow_graph import MatchingFlowGraph
        >>> cost_dict = {
            (2, 2): 0.9,
            (2, 3): 0.1,
            (3, 3): 0.91,
            (3, 2): 0.1,
            (4, 4): 0.05,
            (4, 5): 0.04,
            (4, 6): 0.91
        }
        >>> fg = MatchingFlowGraph(cost_dict)
        >>> fg.optimize_flow()
        >>> fg.get_all_events()
        """
        return self.get_disappear_list(), self.get_appear_list(), self.get_matching_dict()

    def get_matching_graph(self):
        """

        Returns
        -------
        """
        # - Check that an optimal flow has already been found
        if not hasattr(self, 'flow_dict'):
            log.error("You need to find an optimal flow first!")

        M = self.graph.copy()
        edge_to_remove = [(node_i, node_j) for node_i, v in self.flow_dict.items()
                          for node_j, flow in v.items() if flow == 0]
        M.remove_edges_from(edge_to_remove)

        return M

    def get_incidence_matrix(self):
        """Return the incidence matrix of the graph. If the graph contains split or merge nodes, it is automaticaly
           converted in a coupled incidence matrix
        """
        G = self.graph

        # - Get the incidence matrix
        self.incidence_matrix = nx.incidence_matrix(G, oriented=True)

        # - Get the associated mapping (node, edge and costs)
        self.incidence_mapping['costs'] = np.array(list(nx.get_edge_attributes(G, 'weight').values()))
        self.incidence_mapping['nodes'] = np.array(list(G.nodes()))
        self.incidence_mapping['edges'] = list(G.edges())

        if hasattr(self, 'split_label2node_ids'):
            if self.split_label2node_ids is not None:
                log.info('Split nodes detected in the graph, coupled-incidence matrix will be returned!')

                start = timeit.default_timer()
                self._get_coupled_incidence_matrix()
                stop = timeit.default_timer()

                ti = np.round(stop - start, 1)
                log.info('Form the coupled-incidence matrix in ' + str(ti) + 's!')
            else:
                log.info('No split nodes found!')

    def _get_coupled_incidence_matrix(self):
        """Return the coupled incidence matrix of the graph and associated costs. See PadField and al., 2011.
        """
        # - Get the shape of the matrix (for verification at the end of the coupling process)
        s = self.incidence_matrix.shape

        row = []
        col = []
        data = []

        row_to_delete = []
        col_to_delete = []

        # - Find non-zero columns related to the current split nodes
        # Columns are related to: appear --> split, left --> split and split --> rights
        split_ix = list(self.split_label2node_ids.values())
        split_cols = self.incidence_matrix[split_ix, :].nonzero()

        # - Iterative over the split nodes to get the coupled-incidence matrix
        for ids, sn in self.split_label2node_ids.items():

            # - Get the coordinates
            split_col = split_cols[1][split_cols[0] == split_ix.index(sn)]

            # - Find the future coupled column (that have a non-zero cost) which is the edge: left node --> split node
            tf = self.incidence_mapping['costs'][split_col] > 0

            # - Update the edge mapping: replace split edges by a list of edges
            other_ed = [self.incidence_mapping['edges'][ed] for ed in split_col[~tf]]

            for ed in split_col[tf]:
                self.incidence_mapping['edges'][ed] = [self.incidence_mapping['edges'][ed]] + other_ed

            # - Get the number of daughters
            n = sum(~tf) - 1

            # - For each left nodes connected to this split nodes (with positive cost), modify the column left --> split
            #   to add the edge appear --> split and split --> right(s)
            ix = np.where(tf)[0]
            nix = len(ix)

            cix = [split_col[i] for i in ix]
            rix = [self.appear_node] + [self.lg2nid[i, 'r'] for i in ids]
            d = np.append(-1 * (n - 1), np.repeat(1, n))

            row = np.append(row, np.tile(rix, nix))
            col = np.append(col, np.repeat(cix, n + 1))
            data = np.append(data, np.tile(d, nix))

            # - Update the rows and columns to keep (only the coupled column and the non-split rows)
            row_to_delete = np.append(row_to_delete, sn)
            col_to_delete = np.append(col_to_delete, split_col[~tf])

        # - Get the rows and columns to keep
        row_to_keep = np.setdiff1d(np.arange(s[0]), row_to_delete)
        col_to_keep = np.setdiff1d(np.arange(s[1]), col_to_delete)

        # - Update all the incidence matrix
        self.incidence_matrix += csc_matrix((data, (row, col)), shape=s)

        # - Remove unwanted columns (non-coupled columns): appear --> split, split --> rights
        self.incidence_matrix = self.incidence_matrix[:, col_to_keep]
        self.incidence_mapping['costs'] = self.incidence_mapping['costs'][col_to_keep]
        self.incidence_mapping['edges'] = [self.incidence_mapping['edges'][int(col)] for col in col_to_keep]

        # - Remove unwanted rows: split rows
        self.incidence_matrix = self.incidence_matrix[row_to_keep, :]
        self.incidence_mapping['nodes'] = self.incidence_mapping['nodes'][row_to_keep]

        r = s[0] - self.incidence_matrix.shape[0]
        c = s[1] - self.incidence_matrix.shape[1]
        log.info("Size checked: " + str(r) + " rows and " + str(c) + " columns removed from the incidence matrix")

    def set_appear_cost(self, appear_cost_dict):
        """Set costs of edge connecting the appear node to the right nodes

        Parameters
        ----------
        appear_cost_dict: dict {i: cost_appear_to_i}
            keys indicated the i-eme right node connected to the appear node

        Return
        ------
        MatchingFlowGraph object

        Examples
        >>> from ctrl.matching_flow_graph import MatchingFlowGraph
        >>> cost_dict = {(2, 2): 0.9, (2, 3): 0.1, (3, 3): 0.91, (3, 2): 0.1, (4, 4): 0.05, (4, 5): 0.04, (4, 6): 0.91}
        >>> fg = MatchingFlowGraph(cost_dict)
        >>> appear_cost_dict = {2: 0.5, 3: 0.3, 4: 0.8, 6: 0.1}
        >>> fg.set_appear_cost(appear_cost_dict)
        >>> fg.draw_flow_graph()
        """
        G = self.graph

        # - Check the existence of the disappear node
        if self.appear_node is None:
            raise RuntimeError("Appear node does not exist. This method can not be used on this graph...for now.")

        for nid, cost in appear_cost_dict.items():
            # - assert node index validity
            if nid in self.right_labels:
                # - get true node index and set edge's cost
                r_idx = self.lg2nid[(nid, 'r')]
                G[self.appear_node][r_idx]['weight'] = int(cost * self.scale)

    def set_disappear_cost(self, disappear_cost_dict):
        """Set costs of edge connecting the left nodes to the disappear node

        Parameters
        ----------
        disappear_cost_dict: dict {i: cost_i_to_disappear}
            keys indicated the i-eme left node connected to the disappear node

        Return
        ------
        MatchingFlowGraph object

        Examples
        >>> from ctrl.matching_flow_graph import MatchingFlowGraph
        >>> cost_dict = {(2, 2): 0.9, (2, 3): 0.1, (3, 3): 0.91, (3, 2): 0.1, (4, 4): 0.05, (4, 5): 0.04, (4, 6): 0.91}
        >>> fg = MatchingFlowGraph(cost_dict)
        >>> disappear_cost_dict = {2: 0.2, 4: 0.4}
        >>> fg.set_disappear_cost(disappear_cost_dict)
        >>> fg.draw_flow_graph()
        """
        G = self.graph

        # - Check the existence of the disappear node
        if self.disappear_node is None:
            raise RuntimeError("Disappear node does not exist. This method can not be used on this graph...for now.")

        for nid, cost in disappear_cost_dict.items():
            # - assert node index validity
            if nid in self.left_labels:
                # - get true node index and set edge's cost
                l_idx = self.lg2nid[(nid, 'l')]
                G[l_idx][self.disappear_node]['weight'] = int(cost * self.scale)

    def _set_cost_scale(self):
        """ Check the cost_dict to find the precision of the input and set the corresponding scale
        """

        # Define a function to count the number of digits to the right of the decimal
        f = lambda x: len(str(x).split('.')[-1]) if len(str(x).split('.')) > 1 else 0

        # Get the number of digits
        ndec = [f(x) for x in self.cost_dict.values()]

        # get the max and define the scaling
        self.scale = 10 ** max(ndec)

    def draw_incidence_matrix(self, import_matrix=None, full_graph=False):
        """ Make an annotated heatmap of the current incidence matrix.
        """
        # - Check that an incidence matrix is available
        if not hasattr(self, 'incidence_matrix'):
            raise ValueError('An incidence matrix is required for the visualisation, see \'optimize_flow\'!')

        cmap = colors.ListedColormap(['white', 'black', 'mediumvioletred', 'navy', 'red'])

        # - Get the incidence matrix
        if import_matrix is None:
            I = self.incidence_matrix.todense()
        else:
            I = import_matrix.todense()

        # - Save the position of the +1 and -1 values
        loc_plus_row, loc_plus_col = np.where(I > 0)
        loc_minus_row, loc_minus_col = np.where(I < 0)

        # - Get the name of each nodes, modify the incidence matrix for heatmap visualisation:
        #   * S+/S- : rose color
        #   * A: blue color
        #   * D: red color
        #   * other connexions in black

        node_label = []
        for nid in self.incidence_mapping['nodes']:
            grp = self.graph.nodes.data('group')[nid]
            if nid in self.special_nodes():
                lab = grp[0].upper()
                if nid == self.source_node:
                    node_label.append(lab + '+')
                    I[nid, :] = 2 * I[nid, :]  # purple color
                elif nid == self.sink_node:
                    node_label.append(lab + '-')
                    I[nid, :] = 2 * I[nid, :]  # purple color
                elif nid == self.appear_node:
                    # use boolean operation to avoid problems with |value| > 1
                    I[nid, :] = 3 * (I[nid, :] != 0)  # navy color
                    node_label.append(lab)
                else:
                    I[nid, :] = 4 * I[nid, :]  # red color
                    node_label.append(lab)
            else:
                lab = self.graph.nodes.data('label')[nid]
                node_label.append(grp.upper() + str(lab))

        # - Create a figure
        fig, ax = plt.subplots(figsize=(15, 8))

        # - Plot the heatmap
        ax.imshow(np.abs(I), interpolation='none', aspect='auto', origin='upper', cmap=cmap)

        # - Reset the y-ticks and y-labels (nodes)
        ax.set_yticks(np.arange(len(node_label)))
        ax.set_yticklabels(node_label)

        # - Add the xlabel and x-ticks
        ax.set_xticks(np.arange(I.shape[1]) + 0.5, minor=True)
        ax.set_xlabel('Edge number')

        # - Add vertical grid
        ax.grid(b=True, which='minor', axis='x')

        # - Add '+' and '-' annotation at the position where value was positive or negative (directed graph)
        for i, j in zip(loc_minus_row, loc_minus_col):
            ax.text(j, i, '-', fontsize='x-large', ha="center", va="center", color="w")

        for i, j in zip(loc_plus_row, loc_plus_col):
            ax.text(j, i, '+', ha="center", va="center", color="w")

    def draw_optimal_graph(self):
        """ Represented the flow graph before and after optimization
        """
        # - Check that an optimal flow has already been found
        if not hasattr(self, 'flow_dict'):
            log.error("You need to find an optimal flow first!")

        # - Create a figure with two subplots (before and after optimization)
        fig = plt.figure(figsize=(30, 15))
        fig.clf()

        # - Before
        fig.add_subplot(1, 2, 1)
        self.draw_flow_graph()
        fig.gca().set_title('Graph before optimization')

        M = self.get_matching_graph()
        M.remove_nodes_from(list(nx.isolates(M)))  # remove nodes with no neighbors

        # - After
        fig.add_subplot(1, 2, 2)
        self.draw_flow_graph(import_graph=M)
        fig.gca().set_title('Graph after optimization')

    def draw_flow_graph(self, h_spacing=1., v_spacing=1.5, import_graph=None, **kwargs):
        """Represent the flow graph before optimization.

        Parameters
        ----------
        h_spacing : float, optional
            horizontal spacing between the nodes, default=1.
        v_spacing : float, optional
            vertical spacing between the nodes, default=1.5
        kwargs : dict, optional
            optional keyword arguments passed to 'draw_networkx'
        """
        if import_graph is None:
            G = self.graph
        else:
            G = import_graph

        coords, labels = {}, {}
        v_middle = np.max([self.n_left, self.n_right]) * v_spacing / 2

        # - Define the horizontal position of the right nodes according to the number of split nodes
        if hasattr(self, 'split_label2node_ids'):
            max_h_pos = 3 + np.max((len(self.split_label2node_ids) - 3) / 2, 0)
        else:
            max_h_pos = 3

        node_colors = []

        h0 = self.n_left
        h1 = self.n_right
        xs = 1.5

        for nid in sorted(G.nodes()):
            if nid == self.source_node:
                coords[nid] = [0, v_middle]
                labels[nid] = "T$_+$"
                node_colors += ['mediumvioletred']
            elif nid == self.sink_node:
                coords[nid] = [(max_h_pos + 1) * h_spacing, v_middle]
                labels[nid] = "T$_-$"
                node_colors += ['mediumvioletred']
            elif nid == self.appear_node:
                coords[nid] = [1 * h_spacing, -1 * v_spacing]
                labels[nid] = "A"
                node_colors += ['navy']
            elif nid == self.disappear_node:
                coords[nid] = [max_h_pos * h_spacing, -1 * v_spacing]
                labels[nid] = "D"
                node_colors += ['red']
            else:
                group = G.nodes[nid]['group']
                if group == 'l':
                    labels[nid] = "L$_{" + str(int(G.nodes[nid]['label'])) + "}$"
                    x = 1
                    coords[nid] = [x * h_spacing, h0 * v_spacing]
                    h0 -= 1
                    node_colors += ['k']
                elif group == 'r':
                    labels[nid] = "R$_{" + str(int(G.nodes[nid]['label'])) + "}$"
                    x = max_h_pos
                    coords[nid] = [x * h_spacing, h1 * v_spacing]
                    h1 -= 1
                    node_colors += ['k']
                elif group == 's':
                    list_idx = str(G.nodes[nid]['label'])
                    labels[nid] = "S$_{" + list_idx[1:-1] + "}$"
                    coords[nid] = [xs * h_spacing, -2 * v_spacing]
                    node_colors += ['forestgreen']
                    xs += 0.5
                else:
                    log.warning("Warning: ??")

        edge_color = []
        for eid in G.edges():
            cost = G.edges[eid]['weight']
            cost /= self.scale * self.max_cost
            edge_color += [cost]

        nx.drawing.nx_pylab.draw_networkx(G, coords, labels=labels,
                                          node_shape='o', node_color=node_colors,
                                          edge_color=edge_color, edge_cmap=get_cmap('coolwarm'), edge_vmin=0.5,
                                          edge_vmax=1., font_color='w', font_size=8, **kwargs)


def optimize_matching_graph(graph, cost_dict):
    graph.set_costs(cost_dict)
    graph.optimize_cost()
