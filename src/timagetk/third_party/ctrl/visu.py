from visu_core.matplotlib.colormap import to_rgb
from visu_core.vtk.utils.image_tools import  image_to_vtk_cell_polydatas
from visu_core.vtk.utils.polydata_tools import vtk_combine_polydatas
from visu_core.vtk.actor import vtk_actor, vtk_scalar_bar_actor
from visu_core.vtk.display import vtk_save_screenshot_actors
from visu_core.vtk.utils.image_tools import vtk_image_data_from_image
from visu_core.vtk.volume import vtk_image_volume
from visu_core.vtk.primitive_actors import vertex_scalar_property_polydata
from visu_core.matplotlib.glasbey import glasbey

import matplotlib.pyplot as plt
import matplotlib.image as image
from matplotlib.gridspec import SubplotSpec
import matplotlib.patches as patches

import numpy as np
import os
import vtk
import pickle
import re

from timagetk.third_party.ctrl.evaluate_lineage import compare_mapping_to_reference

from timagetk.third_party.vt_features import get_barycenter
from timagetk.io import imread, imsave
from timagetk.visu.mplt import grayscale_imshow

def accuracy2colormap(mother_accuracy, daughter_accuracy):
    set1_colormap = {'Correct': 0.25, 'Some':0.6, 'None':0.5, 'Missing':0.05, 'Unknown':1}

    mother_colormap = {lab: set1_colormap[val] for lab, val in mother_accuracy.items()}
    daughter_colormap = {lab: set1_colormap[val] for lab, val in daughter_accuracy.items()}

    return mother_colormap, daughter_colormap

def show_img(image, dict_cell_value=None, cmap=None, vmax=None, subsampling=None, smoothing=20, vmin=0., showlabels=False,
             add_missing_label=False, missing_label_colormap='Greys', missing_scalar=0.3):

    actors = []

    if dict_cell_value is None:
        labels = image.labels()
        dict_cell_value = {lab: lab % 255 for lab in labels}
        vmax = 255
        cmap = 'glasbey'
    else:
        labels = list(dict_cell_value)

        if cmap is None:
            cmap = 'Reds'

        if vmax is None:
            vmax = max(dict_cell_value.values())

    if showlabels:
        # - create the label mapper
        label_mapper = vtk.vtkLabeledDataMapper()

        # - get the label coordinates (median coordinate of the external surface)
        centers = get_barycenter(image, labels=labels)
        n = image.neighbors(1)[1]  # L1
        surface_center = {k: v[::-1] for k, v in centers.items() if k in n}

        # - create the polydata with the position of the labels
        center_polydata = vertex_scalar_property_polydata(np.array(list(surface_center.values())),
                                                          np.array(list(surface_center.keys())))

        # - assign text to the position
        label_mapper.SetInputData(center_polydata)
        label_mapper.SetLabelModeToLabelScalars()
        label_mapper.SetLabelFormat("%g")
        label_mapper.GetLabelTextProperty().SetColor(*to_rgb('k'))
        label_mapper.GetLabelTextProperty().SetFontSize(12)
        label_mapper.GetLabelTextProperty().ItalicOff()
        label_mapper.GetLabelTextProperty().SetJustificationToCentered()
        label_mapper.GetLabelTextProperty().SetVerticalJustificationToCentered()

        # - create the vtk actor
        actor = vtk.vtkActor2D()
        actor.SetMapper(label_mapper)

        actors += [actor]

    if subsampling is not None:
        subsampling = [int(np.round(subsampling / v)) for v in image.voxelsize]

    cell_polydatas = image_to_vtk_cell_polydatas(image,
                                                 subsampling=subsampling,
                                                 cell_property=dict_cell_value,
                                                 labels=labels,
                                                 smoothing=smoothing)

    cell_polydata = vtk_combine_polydatas([cell_polydatas[c] for c in labels if c != 1])
    actors += [vtk_actor(cell_polydata, colormap=cmap, value_range=(vmin, vmax))]

    if add_missing_label & (dict_cell_value is not None):
        # - Add other labels to the image
        other_labels = list(set(image.labels()) - set(labels))

        cell_polydatas2 = image_to_vtk_cell_polydatas(image,
                                                     subsampling=subsampling,
                                                     cell_property={lab: missing_scalar for lab in other_labels},
                                                     labels=other_labels,
                                                     smoothing=smoothing)

        cell_polydata2 = vtk_combine_polydatas([cell_polydatas2[c] for c in other_labels if c != 1])

        actors += [vtk_actor(cell_polydata2, colormap=missing_label_colormap, value_range=(0, 1))]

    return actors

def show_3D(img, cmap='gray', val_range=None):
    if val_range is None:
        val_range = (0, np.max(img))

    image_data = vtk_image_data_from_image(img.get_array(), voxelsize=img.voxelsize)
    volume = vtk_image_volume(image_data, colormap=cmap, value_range=val_range)

    return [volume]


def show_3D_lineage(mother_seg, daughter_seg, pred_dict, true_dict=None,
                    focal_point=(-1,0,0), view_up=(1, 0, 0), only_reference=False):

    if (true_dict is not None) & only_reference:
        pred_dict = {mlab: dlab for mlab, dlab in pred_dict.items() if mlab in true_dict}

    # - Show the predicted lineage
    actors = show_img(mother_seg,
                      {int(mlab): int(mlab) % 255 for mlab in pred_dict},
                      cmap='glasbey', vmin=0, vmax=255, subsampling=0.8)
    vtk_save_screenshot_actors(actors, 'mother_lineage_pred.png',
                               focal_point=focal_point, view_up=view_up)

    actors = show_img(daughter_seg,
                      {int(lab): int(mlab) % 255 for mlab, dlab in pred_dict.items() for lab in dlab},
                      cmap='glasbey', vmin=0, vmax=255, subsampling=0.8)
    vtk_save_screenshot_actors(actors, 'daughter_lineage_pred.png',
                               focal_point=focal_point, view_up=view_up)

    # - if ground-truth, get the difference with prediction
    if true_dict is not None:
        # - get the detailled accuracy (Correct, some, none, missing,...)
        mother_accuracy, daughter_accuracy = compare_mapping_to_reference(true_dict=true_dict, pred_dict=pred_dict)
        # - convert in colormap
        mother_colormap, daughter_colormap = accuracy2colormap(mother_accuracy, daughter_accuracy)

        # - Output accuracy
        actors = show_img(mother_seg,
                          dict_cell_value=mother_colormap,
                          cmap='Set1', vmin=0, vmax=1, subsampling=0.8)
        vtk_save_screenshot_actors(actors, 'lineage_mother_comparison.png',
                                   focal_point=focal_point, view_up=view_up)

        actors = show_img(daughter_seg,
                          dict_cell_value=daughter_colormap,
                          cmap='Set1', vmin=0, vmax=1, subsampling=0.8)
        vtk_save_screenshot_actors(actors, 'lineage_daughter_comparison.png',
                                   focal_point=focal_point, view_up=view_up)

        # - Show the reference lineage
        actors = show_img(mother_seg,
                          {int(mlab): int(mlab) % 255 for mlab in true_dict},
                          cmap='glasbey', vmin=0, vmax=255, subsampling=0.8)
        vtk_save_screenshot_actors(actors, 'mother_lineage_true.png',
                                   focal_point=focal_point, view_up=view_up)

        actors = show_img(daughter_seg,
                          {int(lab): int(mlab) % 255 for mlab, dlab in true_dict.items() for lab in dlab},
                          cmap='glasbey', vmin=0, vmax=255, subsampling=0.8)
        vtk_save_screenshot_actors(actors, 'daughter_lineage_true.png',
                                   focal_point=focal_point, view_up=view_up)

        # - Plot information
        nrow=3
        image_name = ['mother_lineage_pred.png', 'daughter_lineage_pred.png',
                      'mother_lineage_true.png', 'daughter_lineage_true.png',
                      'lineage_mother_comparison.png', 'lineage_daughter_comparison.png']
        subfigs_name = ['Predicted lineage', 'Reference lineage', 'Lineage difference']
    else:
        # - Plot information
        nrow = 1
        image_name = ['mother_lineage_pred.png', 'daughter_lineage_pred.png']
        subfigs_name = ['Predicted lineage']

    # - Build image
    fig, axs = plt.subplots(nrow, 2, figsize=(15, 6*nrow))
    for i, ax in enumerate(axs.flatten()):
        img = image.imread(image_name[i])  # open image
        img = resize_img(img, margin=0.05)

        ax.imshow(img)
        ax.axis('off')
        if i % 2 == 0:
            ax.set_title('Mother image')
        else:
            ax.set_title('Daughter image')

        os.remove(image_name[i])  # remove the image

    grid = plt.GridSpec(nrow, 2)
    for i in range(nrow):
        create_subtitle(fig, grid[i, ::], subfigs_name[i])

    return fig

def show_lineage_evaluation(mother_seg, daughter_seg, evaluation_dict, focal_point=(-1,0,0), view_up=(1, 0, 0), cmap='Reds'):

    # - Get the list of the criteria
    evaluation_criteria = list(evaluation_dict)

    # - Save the evaluation images
    for criteria in evaluation_criteria:
        if 'mother' in evaluation_dict[criteria]:
            # - Mother image
            actors = show_img(mother_seg,
                              evaluation_dict[criteria]['mother'],
                              cmap=cmap, vmin=0, vmax=1, subsampling=0.8)
            cbar = vtk_scalar_bar_actor(actors[0], width=100, title=f'{criteria}', color='k', font_size=10)
            actors.append(cbar)
            vtk_save_screenshot_actors(actors, f'mother_{criteria}.png',
                                       focal_point=focal_point, view_up=view_up)

        if 'daughter' in evaluation_dict[criteria]:
            # - Daughter image
            actors = show_img(daughter_seg,
                              evaluation_dict[criteria]['daughter'],
                              cmap=cmap, vmin=0, vmax=1, subsampling=0.8)
            cbar = vtk_scalar_bar_actor(actors[0], width=100, title=f'{criteria}', color='k', font_size=10)
            actors.append(cbar)
            vtk_save_screenshot_actors(actors, f'daughter_{criteria}.png',
                                       focal_point=focal_point, view_up=view_up)

    # - Get the number of criteria
    nrow = len(evaluation_criteria)

    # - Make a figure with all the criteria
    fig, axs = plt.subplots(nrow, 2, figsize=(15, 6*nrow))

    for i, ax in enumerate(axs.flatten()):
        # - Get the current object name
        name = "mother" if i % 2 == 0 else "daughter"

        # - Get the current evaluation metric
        criteria = evaluation_criteria[i // 2]

        if os.path.isfile(f'{name}_{criteria}.png'):
            img = image.imread(f'{name}_{criteria}.png')  # open image
            img = resize_img(img, margin=0.05)

            ax.imshow(img)
            ax.set_title(f'{name.capitalize()} image')

        ax.axis('off')

        if os.path.isfile(f'{name}_{criteria}.png'):
            os.remove(f'{name}_{criteria}.png')  # remove the image

    grid = plt.GridSpec(nrow, 2)
    for i, evaluation_criteria in enumerate(evaluation_dict):
        create_subtitle(fig, grid[i, ::], evaluation_criteria)

    return fig

def show_iterative_lineage(mother_seg, daughter_seg, track_file, true_dict=None, iter_range=None,
                           focal_point=(-1, 0, 0), view_up=(1, 0, 0)):
    # - Use an iterative file to see the iterative procedure
    #   Use the optional argument 'iter_range' to define a custom range of iterations

    # - Open the track_file
    if os.path.exists(track_file):
        f = open(track_file, 'rb')
        track_data = pickle.load(f)
        f.close()
    else:
        log.error('No track file found!')
        return None

    # - Check the given iter_range
    if iter_range is None:
        iter_range = (min(track_data), max(track_data))
    else:
        assert (iter_range[0] in track_data) and (iter_range[1] in track_data)

    # - Parameters of the plot
    max_col = 7

    if true_dict is not None:
        nrow = 7
        figsize = (18, 10)
    else:
        nrow = 5
        figsize = (18, 9)

    # - initialize figure
    list_fig = []
    fig, axs = plt.subplots(nrow, max_col, figsize=figsize)

    # - Loop over the range of iterations
    count = 0
    nfig = 0
    for i in np.arange(iter_range[0], iter_range[1]+1):
        # - check if all the columns are filled
        if (count != 0) and (count % max_col == 0):
            # - add iteration information
            grid = plt.GridSpec(nrow, max_col)
            subfigs_name = [f'Iteration n°{ix + iter_range[0]}' for ix
                            in np.arange(nfig * max_col, min(nfig * max_col + max_col, iter_range[1]+1))]

            for ix in range(len(subfigs_name)):
                create_subtitle(fig, grid[::, ix], subfigs_name[ix], fontsize=13)
            plt.tight_layout()
            list_fig.append(fig)  # store the previous figure

            # - create a new figure
            fig, axs = plt.subplots(nrow, max_col, figsize=figsize)

            nfig += 1

        # - Show the current lineage
        actors = show_img(mother_seg,
                          {int(mlab): int(mlab) % 255 for mlab in track_data[i]['lineage_dict']},
                          cmap='glasbey', vmin=0, vmax=255, subsampling=0.8)
        vtk_save_screenshot_actors(actors, f'mother_lineage_pred.png',
                                   focal_point=focal_point, view_up=view_up)

        actors = show_img(daughter_seg,
                          {int(lab): int(mlab) % 255 for mlab, dlab in track_data[i]['lineage_dict'].items()
                           for lab in dlab},
                          cmap='glasbey', vmin=0, vmax=255, subsampling=0.8)
        vtk_save_screenshot_actors(actors, f'daughter_lineage_pred.png',
                                   focal_point=focal_point, view_up=view_up)

        # - Show the current preservation
        actors = show_img(mother_seg,
                          {int(mlab): score for mlab, score in track_data[i]['preservation_score'].items()},
                          cmap='Reds_r', vmin=0.6, vmax=1, subsampling=0.8)
        vtk_save_screenshot_actors(actors, f'lineage_score.png',
                                   focal_point=focal_point, view_up=view_up)

        # - Show the current hc
        actors = show_img(mother_seg,
                          {int(mlab): int(mlab) % 255 for mlab in track_data[i]['hc_lineage']},
                          cmap='glasbey', vmin=0, vmax=255, subsampling=0.8)
        vtk_save_screenshot_actors(actors, f'mother_hc_pred.png',
                                   focal_point=focal_point, view_up=view_up)

        actors = show_img(daughter_seg,
                          {int(lab): int(mlab) % 255 for mlab, dlab in track_data[i]['hc_lineage'].items()
                           for lab in dlab},
                          cmap='glasbey', vmin=0, vmax=255, subsampling=0.8)
        vtk_save_screenshot_actors(actors, f'daughter_hc_pred.png',
                                   focal_point=focal_point, view_up=view_up)

        if true_dict is not None:
            # - get the detailled accuracy (Correct, some, none, missing,...)
            mother_accuracy, daughter_accuracy = compare_mapping_to_reference(true_dict=true_dict,
                                                                              pred_dict=track_data[i]['hc_lineage'])
            # - convert in colormap
            mother_colormap, daughter_colormap = accuracy2colormap(mother_accuracy, daughter_accuracy)

            # - Output accuracy
            actors = show_img(mother_seg,
                              dict_cell_value=mother_colormap,
                              cmap='Set1', vmin=0, vmax=1, subsampling=0.8)
            vtk_save_screenshot_actors(actors, 'lineage_mother_comparison.png',
                                       focal_point=focal_point, view_up=view_up)

            actors = show_img(daughter_seg,
                              dict_cell_value=daughter_colormap,
                              cmap='Set1', vmin=0, vmax=1, subsampling=0.8)
            vtk_save_screenshot_actors(actors, 'lineage_daughter_comparison.png',
                                       focal_point=focal_point, view_up=view_up)

        # - Plot the image on the figure
        image_name = ['mother_lineage_pred', 'daughter_lineage_pred',
                      'lineage_score', 'mother_hc_pred', 'daughter_hc_pred']

        if true_dict is not None:
            image_name.extend(['lineage_mother_comparison', 'lineage_daughter_comparison'])

        ii = (i-iter_range[0]) % max_col
        for j, n in enumerate(image_name):
            img = image.imread(f'{image_name[j]}.png')  # open image
            img = resize_img(img, margin=0.05) # remove unwanted white part
            axs[j][ii].imshow(img)
            axs[j][ii].axis('off')
            os.remove(f'{image_name[j]}.png')  # remove the image

            if j == 0:
                axs[j][ii].set_title('Mother lineage', fontsize=9)
            elif j == 1:
                axs[j][ii].set_title('Daughter lineage', fontsize=9)
            elif j == 2:
                axs[j][ii].set_title('Lineage quality', fontsize=9)
            elif j == 3:
                axs[j][ii].set_title('Mother HC lineage', fontsize=9)
            elif j == 4:
                axs[j][ii].set_title('Daughter HC lineage', fontsize=9)
            elif j == 5:
                axs[j][ii].set_title('Mother HC comparison', fontsize=9)
            elif j == 6:
                axs[j][ii].set_title('Daughter HC comparison', fontsize=9)
        count += 1

    # - Finish the last figure
    # - add iteration information
    grid = plt.GridSpec(nrow, max_col)
    subfigs_name = [f'Iteration n°{ix + iter_range[0]}' for ix
                    in np.arange(nfig * max_col, min(nfig * max_col + max_col, iter_range[1] + 1))]

    for ix in range(len(subfigs_name)):
        create_subtitle(fig, grid[::, ix], subfigs_name[ix], fontsize=13)
    plt.tight_layout()
    list_fig.append(fig)  # store the previous figure

    # - close unused axis
    last_col = count % max_col

    for i in np.arange(last_col, max_col):
        # run over the last columns
        for j in np.arange(nrow):
            # run over the rows
            axs[j][i].axis('off')

    return list_fig

def resize_img(img, margin=0.05):
    # - Remove the blank around the images
    # - Get the mask corresponding to the white part of the image
    mask = np.sum(img < 0.1, axis=2) == img.shape[2]

    # - Get the y_min and y_max position of the object
    y_mask = np.sum(mask, axis=0) != img.shape[0]

    obj_width = y_mask.sum()
    obj_ypos = np.where(y_mask)[0][0]

    obj_ymargin = int(margin * obj_width)

    y_min = max(0, obj_ypos - obj_ymargin)
    y_max = min(obj_ypos + obj_width + obj_ymargin, img.shape[0])

    # - Get the x_min and x_max position of the object
    x_mask = np.sum(mask, axis=1) != img.shape[1]

    obj_height = x_mask.sum()
    obj_xpos = np.where(x_mask)[0][0]

    obj_xmargin = int(margin * obj_height)

    x_min = max(0, obj_xpos - obj_xmargin)
    x_max = min(obj_xpos + obj_height + obj_xmargin, img.shape[1])

    return img[x_min:x_max, y_min:y_max, :]

def create_subtitle(fig: plt.Figure, grid: SubplotSpec, title: str, fontsize=18):
    # from https://stackoverflow.com/questions/27426668/row-titles-for-matplotlib-subplot
    "Sign sets of subplots with title"
    row = fig.add_subplot(grid)
    # the '\n' is important
    row.set_title(f'{title}\n', fontweight='semibold', fontsize=fontsize)
    # hide subplot
    row.set_frame_on(False)
    row.axis('off')

def visu_block_size(img, blk_size):

    # - Make images
    imsave('img_test.tif', img)
    os.system('buildPyramidImage img_test.tif img_pyramid_level%d.tif -py-ll 1')

    # - Open the images
    files = [re.match('img_pyramid_level(?P<pyll>\d).tif', file) for file in os.listdir()]
    list_img = {int(f.group('pyll')) : imread(f"img_pyramid_level{f.group('pyll')}.tif") for f in files if f}

    # - Sort images
    list_img = {k: list_img[k] for k in sorted(list_img)}

    # - Get the middle slice of each images
    slice_list_img = {k : img.get_slice(int(img.shape[0]/2), axis='z') for k, img in list_img.items()}

    # - Show
    fig, axis = grayscale_imshow(list(slice_list_img.values()), max_per_line=len(slice_list_img))

    # For each plot, add red rectangle corresponding to the bloc size
    for k, ax in zip(slice_list_img.keys(), axis.flatten()):
        blk_size_real = blk_size * np.array(slice_list_img[k].voxelsize)
        real_img_size = np.array(slice_list_img[k].shape) * np.array(slice_list_img[k].voxelsize)
        for i in np.arange(real_img_size[0], -blk_size_real[0], -blk_size_real[0]):
            for j in np.arange(real_img_size[1], -blk_size_real[1], -blk_size_real[1]):
                rect = patches.Rectangle((j, i), blk_size_real[1], blk_size_real[0], linewidth=1, edgecolor='r', facecolor='none')
                ax.add_patch(rect)

    # - Remove imgs
    os.remove('img_test.tif')
    for img in list_img.values():
        os.remove(img.filename)

    return fig