import networkx as nx

from timagetk.algorithms.resample import isometric_resampling
from timagetk.third_party.ctrl.lineage import Lineage
from timagetk.components.tissue_image import TissueImage3D

def create_adj_graph(img_seg, resample_vox=None, adj_weighted=True):
    """Create an adjacency graph from a segmented image.

    This function generates an adjacency graph where nodes represent cells, and edges indicate adjacency between cells.
    The adjacency can be either weighted (based on contact surface area) or unweighted (based solely on the presence of neighbors).

    Parameters
    ----------
    img_seg : TissueImage3D or LabelledImage
        A segmented image object where each label corresponds to a cell.
    resample_vox : float, optional
        Voxel size to resample the image to if `adj_weighted` is True. Defaults to None, which means no resampling.
    adj_weighted : bool, optional
        If true, edges are weighted by contact surface area between cells. Defaults to True.

    Returns
    -------
    nx.Graph
        An adjacency graph with node attributes of cell volume and edge attributes of contact surface.

    Notes
    -----
    If the image is not isotropic and `adj_weighted` is True, the image is resampled to ensure accurate computation
    of contact surfaces which require real units.
    """

    # - Assert that if the image is not isometric, it will be resampled (necessary when contact surface are in real units)
    if ((not img_seg.is_isometric()) and adj_weighted) and (resample_vox is not None):
        resample_vox = min(img_seg.voxelsize)

    if resample_vox is not None:
        img_seg = isometric_resampling(img_seg, value=resample_vox, interpolation='cellbased', cell_based_sigma=1)

    img_seg = TissueImage3D(img_seg, not_a_label=0, background=1)

    if adj_weighted:
        # - Calculate the surface contact
        adj_properties = img_seg.walls.area(real=True)
    else:
        # - Get the neighbors
        adj_properties = img_seg.neighbors()

    # - Calculate the volume of the cells
    cell_volume = img_seg.cells.volume(real=True)

    # - Create the adjacency graph of both images
    G = nx.Graph()

    node_data = [(lab, {'volume': cell_volume[lab] if lab in cell_volume else None}) for lab in img_seg.labels()]
    G.add_nodes_from(node_data)

    if adj_weighted:
        edge_data = [(lab1, lab2, {'contact': area}) for (lab1, lab2), area in adj_properties.items()]
    else:
        edge_data = [(lab1, lab2) for lab1, n in adj_properties.items() for lab2 in n]

    G.add_edges_from(edge_data)

    return G

def create_lineage_graph(mother_seg, daughter_seg, resample_vox=0.5):
    """Create adjacency graphs for mother and daughter images with weighted edges.

    This function generates weighted adjacency graphs for both mother and daughter images,
    where the weights on the edges represent the cell surface contact areas. It ensures that both
    images have the same resampling voxel size to facilitate comparison of surface areas.

    Parameters
    ----------
    mother_seg : TissueImage3D
        Segmented image representing the mother cells.
    daughter_seg : TissueImage3D
        Segmented image representing the daughter cells.
    resample_vox : float, optional
        Voxel size used for resampling images to ensure isotropy. Defaults to 0.5.

    Returns
    -------
    tuple of nx.Graph
        A tuple containing the adjacency graphs for the mother and daughter images, respectively.

    Notes
    -----
    Resampling is necessary to standardize voxel size and enable accurate comparison of surface areas.
    """

    # - Assert that both images are resampled with the same voxel size (to ensure correct comparaison of surfaces)
    #   Also add the contact surface (real unit)
    M = create_adj_graph(mother_seg, resample_vox=resample_vox, adj_weighted=True)
    D = create_adj_graph(daughter_seg, resample_vox=resample_vox, adj_weighted=True)

    return M, D

def get_mapping_graph(lineage_dict, M, D):
    """Construct quotient graphs based on a given lineage.

    This method creates two quotient graphs from mother and daughter graphs, following lineage equivalence classes.
    It modifies nodes and attributes to reflect lineage relationships and tracks missing cell labels.

    **Mother Graph (M_mapping):**
      - Retains equivalence to the original mother graph but marks nodes absent in the lineage with a "missing" label.
      - This facilitates the identification of missing contact surfaces later in analysis.

    **Daughter Graph (D_mapping):**
      - Nodes are merged or relabeled in accordance with the lineage.
      - If multiple daughter nodes share a mother, they are merged into a single node with updated topological and
        geometrical properties, adopting their common mother's label.
      - If a daughter node has a single mother, it is simply relabeled using the mother's label.
      - Nodes without mothers in the lineage are assigned new unique labels. Connected nodes without mothers are merged
        beforehand, and all such nodes receive a "missing" attribute to flag their absence in the lineage.

    Parameters
    ----------
    lineage_dict : Lineage
        Dictionary representing lineage mapping from mother to daughter cells.
    M : nx.Graph
        Mother adjacency graph.
    D : nx.Graph
        Daughter adjacency graph.

    Returns
    -------
    tuple of nx.Graph
        The modified graphs for the mother (M_mapping) and daughter (D_mapping) with updated lineage mappings.

    Notes
    -----
    - Mother graph nodes missing in lineage are tagged as "missing".
    - Daughter graph nodes are relabeled and merged based on lineage relationships.
    - Handles background and ensures each missing daughter is assigned a new label if not connected to a mother cell.
    """
    # - assert that background lineage are associated
    lineage = lineage_dict.copy_lineage()

    if 1 in lineage:
        if 1 not in lineage[1]:
            lineage[1].append(1)
    elif (1 in M) and (1 in D):
        lineage[1] = [1]

    # - Copy mother graph
    M_mapping = M.copy()

    # - Start by detecting the cells that will appear during the tracking
    missing_daughter = [lab for lab in D.nodes()
                        if lab not in [item for sublist in lineage.values() for item in sublist]]

    # - Create the partition of D
    partition = [set(k) for k in lineage.values()]

    # - Check the connectivity of the missing daughters and create one new labels for each connected subgraphs
    S = D.subgraph(missing_daughter)
    cc = list(S.subgraph(c) for c in nx.connected_components(S))

    max_label = max(max(D.nodes()), max(M.nodes())) + 1
    missing_daughter_partition = {max_label + ix: set(g) for ix, g in enumerate(cc)}

    partition.extend(list(missing_daughter_partition.values()))

    # - Rules to update the node and edge attributes
    def update_node(n):
        S = D.subgraph(n)
        values = nx.get_node_attributes(S, 'volume').values()

        if None in values:
            return {'volume': None}
        else:
            return {'volume': sum(values)}

    def update_edge(e1, e2):
        total_area = 0
        for i in e1:
            for j in e2:
                if j in D[i]:
                    total_area += D[i][j]['contact']
        return {'contact': total_area}

    D_mapping = nx.quotient_graph(D, partition, relabel=True, edge_data=update_edge, node_data=update_node)
    partition_label = list(lineage) + list(missing_daughter_partition)
    D_mapping = nx.relabel_nodes(D_mapping, {ix: mlab for ix, mlab in enumerate(partition_label)}, copy=True)

    # - Add the missing mother and missing daughter information
    nx.set_node_attributes(M_mapping, {lab: False if lab in lineage else True for lab in M_mapping.nodes()}, 'missing')
    nx.set_node_attributes(D_mapping, {lab: False if lab < max_label else True for lab in D_mapping.nodes()}, 'missing')

    return M_mapping, D_mapping

def filter_mapping(lineage, M, D, ratio_vol=0.4):
    """Filter a lineage association based on connectivity and volume criteria.

    This function applies two rules to filter a lineage: connectedness of daughter cells to a mother
    and volume retention from mother to daughters.

    Parameters
    ----------
    lineage : Lineage
        A dictionary with mother cell labels mapping to lists of daughter cell labels.
    M : nx.Graph
        Graph representing mother cells and their volumes.
    D : nx.Graph
        Graph representing daughter cells and their volumes.
    ratio_vol : float, optional
        Ratio determining the minimum allowable volume of descended cells relative to the mother cell volume. Defaults to 0.4.

    Returns
    -------
    Lineage
        A filtered lineage where unsuccessful associations have been removed.

    Notes
    -----
    Only mother-daughter associations meeting both connectivity and volume requirements are retained.
    """

    wrong_association_mother = []
    for mlab, dlab in lineage.items():
        S = D.subgraph(dlab)
        values = nx.get_node_attributes(S, 'volume').values()

        if None in values:
            continue

        # -volume?
        if sum(nx.get_node_attributes(S, 'volume').values()) < (ratio_vol * M.nodes[mlab]['volume']):
            wrong_association_mother.append(mlab)
            continue

        # -connected daughters?
        if len(dlab) > 1:
            if not nx.is_connected(S):
                wrong_association_mother.append(mlab)

    # - remove the unwanted associations
    lineage_filtered = {mlab: dlab for mlab, dlab in lineage.items()
                        if mlab not in wrong_association_mother}

    return Lineage(lineage_filtered)

def analyze_graph_mapping(M_mapping, D_mapping, ignore_background=False, ignore_missing=False, ignore_no_label_id=True):
    """Calculate lineage quality scores for mother-daughter associations.

    This function scores the fidelity of mother-to-daughter cell associations by examining how
    well spatial contexts and interactions are preserved across mapping transformations. The
    context preservation metric assesses the preservation of the relative contact surfaces through the cell mapping [1]

    **Context Preservation:**
      - For each non-missing mother cell, the function constructs vectors of contact surfaces for neighboring cells.
      - It calculates the proportion of contact surfaces each neighbor contributes relative to the total surface.
      - The goal is to preserve these distributions from mother to daughter mappings, ensuring cell interaction contexts are consistent.

    **Preservation Calculation:**
      - Constructs vectors reflecting relative contact areas for each mother and daughter cell.
      - If ignoring missing labels, sums up their contact surfaces separately to maintain comparison validity.
      - Reorders vectors to align corresponding elements, filling in zeros where associations are absent.

    **Score Computation:**
      - Differences between the vectors are computed to yield a preservation score.
      - Smaller deviations suggest better retention of spatial context, thus higher lineage quality.

    [1] Petit, M., Cerutti, G., Godin, C., & Malandain, G. (2022, March). Robust Plant Cell Tracking in Fluorescence
        Microscopy 3D+ T Series. In 2022 IEEE 19th International Symposium on Biomedical Imaging (ISBI) (pp. 1-4). IEEE.

    Parameters
    ----------
    M_mapping : nx.Graph
        The mother quotient graph with nodes denoting potential mother cells.
    D_mapping : nx.Graph
        The daughter quotient graph with nodes representing daughter cells.
    ignore_background : bool, optional
        If True, ignores the background in calculations. Defaults to False.
    ignore_missing : bool, optional
        If True, excludes missing cells from considerations. Defaults to False.
    ignore_no_label_id : bool, optional
        If True, excludes nodes with no label ID from calculations. Defaults to True.

    Returns
    -------
    dict
        Dictionary mapping mother cell labels to their lineage quality scores.
    """

    # - Calculate the score of preservation for each non-missing mother
    preservation_dict = {}

    for mlab in M_mapping.nodes():
        # - detect missing mother cells (or image background)
        if (M_mapping.nodes[mlab]['missing']) or (mlab not in D_mapping) or (mlab == 1):
            #preservation_dict[mlab] = 0 # should be ignore ??
            continue

        # - Get the surface area vectors :
        mother_area_vec = {lab: M_mapping[mlab][lab]['contact'] for lab in M_mapping.neighbors(mlab)}
        daughter_area_vec = {lab: D_mapping[mlab][lab]['contact'] for lab in D_mapping.neighbors(mlab)}

        if ignore_no_label_id:
            if 0 in mother_area_vec:
                del mother_area_vec[0]

            if 0 in daughter_area_vec:
                del daughter_area_vec[0]

        if ignore_background:
            if 1 in mother_area_vec:
                del mother_area_vec[1]

            if 1 in daughter_area_vec:
                del daughter_area_vec[1]

        # - calculate the total surface
        mother_total_surface = sum(list(mother_area_vec.values()))
        daughter_total_surface = sum(list(daughter_area_vec.values()))

        # - Calculate the relative contact surface of the non-missing cells
        mother_vector = {lab: s / mother_total_surface for lab, s in mother_area_vec.items()
                         if not M_mapping.nodes[lab]['missing']}
        daughter_vector = {lab: s / daughter_total_surface for lab, s in daughter_area_vec.items()
                           if not D_mapping.nodes[lab]['missing']}

        if not ignore_missing:
            # - Sum the surface of the missing labels and calculate their relative contact surface
            #   !! We do not distinguished if different number of missing labels from mother and daughter neighborhood
            mother_vector[-1] = sum([s for lab, s in mother_area_vec.items()
                                     if M_mapping.nodes[lab]['missing']]) / mother_total_surface  # label 0
            daughter_vector[-1] = sum([s for lab, s in daughter_area_vec.items()
                                       if D_mapping.nodes[lab]['missing']]) / daughter_total_surface  # label 0

        # - Re-order both vectors so each element index corresponds to the "same" neighbors (same = associated cells or missing regions)
        #   If one label is only represented on one side for a given index: add 0 on the other vector at the same index
        union_labels = set(mother_vector) | set(daughter_vector)
        mother_vector = {**mother_vector, **{lab: 0 for lab in union_labels if lab not in mother_vector}}
        daughter_vector = {**daughter_vector, **{lab: 0 for lab in union_labels if lab not in daughter_vector}}

        # - Calculate the lineage quality score for each associations
        preservation_dict[mlab] = 1 - (
                    sum([abs(s - daughter_vector[lab]) for lab, s in mother_vector.items()]) / 2)  # 2 for normalization

    return preservation_dict

#### ----- REPLACE THIS PART WITH MATRICE OPERATIONS ---- ####

def compare_mapping_to_reference(true_dict, pred_dict):
    """Compare predicted lineage mappings to reference mappings.

    This function evaluates mother and daughter accuracy based on expert reference maps,
    classifying them into categories like 'Correct', 'None', 'Some', 'Missing', and 'Unknown'.

    Parameters
    ----------
    true_dict : dict or Lineage
        Reference lineage dictionary where mother labels map to lists of daughter labels.
    pred_dict : dict or Lineage
        Predicted lineage dictionary to compare against the reference.

    Returns
    -------
    tuple of dict
        Dictionaries detailing the accuracy of mother and daughter cell mappings.

    Notes
    -----
    Provides fine-grained categorization of predictions based on complete, partial, or missing matches.
    """
    mother_accuracy = {}
    daughter_accuracy = {}

    # - Loop over the expertized lineage
    for mlab, true_dlab in true_dict.items():
        if mlab in pred_dict:
            # - if all correct
            pred_set = set(pred_dict[mlab])
            true_set = set(true_dlab)

            if pred_set == true_set:
                mother_accuracy[mlab] = 'Correct'  # correct
            elif len(pred_set & true_set) == 0:
                mother_accuracy[mlab] = 'None'  # none
            else:
                mother_accuracy[mlab] = 'Some'  # some
        else:
            mother_accuracy[mlab] = 'Missing'  # no daughters

    # - Add the unknown mother
    mother_accuracy = {**mother_accuracy, **{mlab: 'Unknown' for mlab in set(pred_dict) - set(true_dict)}}

    # - invert mapping
    pred_dict_inv = {lab: mlab for mlab, dlab in pred_dict.items() for lab in dlab}
    true_dict_inv = {lab: mlab for mlab, dlab in true_dict.items() for lab in dlab}

    for dlab, true_mlab in true_dict_inv.items():
        if dlab in pred_dict_inv:
            # - if all correct
            if pred_dict_inv[dlab] == true_mlab:
                daughter_accuracy[dlab] = 'Correct'  # correct
            else:
                daughter_accuracy[dlab] = 'None'  # none
        else:
            daughter_accuracy[dlab] = 'Missing'  # no mother

    # - Add the unknown mother
    daughter_accuracy = {**daughter_accuracy, **{dlab: 'Unknown' for dlab in set(pred_dict_inv) - set(true_dict_inv)}}

    return mother_accuracy, daughter_accuracy