#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Sophie Ribes <sophie.ribes@inria.fr>
#
#       File contributor(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#           Florent Papini <florent.papini@inria.fr>
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#           Manuel Petit <manuel.petit@inria.fr>
#
# ------------------------------------------------------------------------------
import numpy as np
from timagetk.algorithms.resample import resample
from timagetk.components.labelled_image import LabelledImage

from timagetk.bin.logger import get_logger
log = get_logger(__name__)

__all__ = ['cell_overlap']
POSS_CRITERIA = ['jaccard', 'target_mother', 'target_daughter', 'intersection']

def cell_overlap(mother_seg, daughter_seg, method='target_mother', ds=1,
                         mother_label=None, daughter_label=None, verbose=False,
                         decimal=5):
    """ Compute all the possible overlap between cells from two images. Different overlap estimation are available.

        Available criterion are:

      - **jaccard**: J(A, B) = |A n B|/|A u B| [1]
      - **target_(mother/daughter)**: T(A, B) = |A n B|/|B| OR T(B, A) = |A n B|/|A|
      - **intersection**: I(A, B) = |A n B|

    Parameters
    ----------
    mother_seg : LabelledImage
        Image labelled, voxelsize and size should be the same as daughter_seg
    daughter_seg: LabelledImage
        Image labelled, voxelsize and size should be the same as mother_seg
    method : str, optional
        estimation method used for the overlap: {‘target_mother','target_daughter','jaccard', 'intersection'}
    ds : int, optional
        downsampling factor
    mother_label : list, optional
        list of mother labels where the overlap need to be computed. default: None (all the mother labels)
    daughter_label : list, optional
        list of daughter labels where the overlap need to be computed. default: None (all the daughter labels)
    verbose : bool, optional
        display the couples found

    Returns
    -------
    dict

    Example
    -------
    >>> from timagetk.components.labelled_image import LabelledImage
    >>> from timagetk.third_party.ctrl.algorithm.image_overlap import cell_overlap
    >>> import numpy as np
    >>> I = np.array([[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],[1, 1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 3, 3, 3, 3, 1, 1, 1], [1, 1, 1, 3, 3, 3, 3, 1, 1, 1], [1, 1, 1, 3, 3, 3, 3, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]])
    >>> J = np.array([[1, 1, 1, 1, 1, 1, 1, 1, 1, 1],[1, 1, 1, 1, 2, 2, 2, 1, 1, 1], [1, 1, 1, 1, 2, 2, 2, 1, 1, 1], [1, 1, 1, 1, 2, 2, 2, 1, 1, 1], [1, 1, 1, 1, 2, 2, 2, 1, 1, 1], [1, 1, 1, 1, 2, 2, 2, 1, 1, 1], [1, 1, 1, 1, 2, 2, 2, 1, 1, 1], [1, 1, 1, 1, 2, 2, 2, 1, 1, 1], [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]])
    >>> I = LabelledImage(I, not_a_label = 0)
    >>> J = LabelledImage(J, not_a_label = 0)
    >>> ov_df = cell_overlap(I, J, method = 'target_daughter')
    >>> print(ov_df)

    """
    # - Check the input
    try:
        assert isinstance(mother_seg, LabelledImage) and isinstance(daughter_seg, LabelledImage)
    except:
        raise ValueError

    try:
        assert method in {'target_mother', 'target_daughter', 'jaccard', 'intersection'}
    except:
        raise ValueError

    # - Downsampling segmented images in order to accelerate computing
    if ds != 1:
        new_voxelsize = [vox * ds for vox in daughter_seg.voxelsize]
        mother_seg = resample(mother_seg, voxelsize=new_voxelsize, interpolation='cellbased', cell_based_sigma=1)
        daughter_seg = resample(daughter_seg, voxelsize=new_voxelsize, interpolation='cellbased', cell_based_sigma=1)

        mother_seg = LabelledImage(mother_seg, not_a_label=0)
        daughter_seg = LabelledImage(daughter_seg, not_a_label=0)

    if mother_label is None:
        mother_label = mother_seg.labels()

    if daughter_label is None:
        daughter_label = daughter_seg.labels()

    # - Get the bounding boxes
    mother_bboxes = mother_seg.boundingbox(labels=mother_label)
    daughter_bboxes = daughter_seg.boundingbox(labels=daughter_label)

    # - Precompute the volume of the cells: do not recalculate at each iteration later
    #   Volume are estimated in voxel units
    mother_volume_cell = {}
    for lab, mbbox in mother_bboxes.items():
        mother_volume_cell[lab] = np.sum(mother_seg[mbbox].get_array() == lab)

    daughter_volume_cell = {}
    for lab, dbbox in daughter_bboxes.items():
        daughter_volume_cell[lab] = np.sum(daughter_seg[dbbox].get_array() == lab)

    overlap_dict = {}
    # - Loop over the mother cells to compute all the possible overlap with the daughter cells
    for mlab, mbbox in mother_bboxes.items():
        for dlab, dbbox in daughter_bboxes.items():
            # - check if overlap between both bboxes (common borders)
            coord_min = [max(mb.start, db.start) for mb, db in zip(mbbox, dbbox)]
            coord_max = [min(mb.stop, db.stop) for mb, db in zip(mbbox, dbbox)]

            if all([dim_min < dim_max for dim_min, dim_max in zip(coord_min, coord_max)]):
                # - get the coordinates that corresponds to the intersection of both volumes: volume(mother_cell).intersection(volume(daughter_cell))
                bbox_intersection = tuple([slice(dim_min, dim_max, None) for dim_min, dim_max in zip(coord_min, coord_max)])

                # - get the subimage corresponding to the intersection
                mother_sub = np.array(mother_seg[bbox_intersection] == mlab)
                daughter_sub = np.array(daughter_seg[bbox_intersection] == dlab)

                # - calculate the intersection between both cells (voxel units)
                intersection = (mother_sub & daughter_sub).sum()

                # - according to the method, calculate the overlap coefficient
                if method == 'target_mother':
                    val = intersection / mother_volume_cell[mlab]
                elif method == 'target_daughter':
                    val = intersection / daughter_volume_cell[dlab]
                elif method == 'jaccard':  # jaccard: in this case the union is estimated from the respective cell volume and their intersection
                    val = intersection / (mother_volume_cell[mlab] + daughter_volume_cell[dlab] - intersection)
                elif method == 'intersection':
                    val = intersection
                else:
                    log.error('Unknown method!')

                val = np.around(val, decimals=decimal)

                if verbose:
                    log.info("--> Couple (" + str(mlab) + ", " + str(dlab) + ") : " + str(val))

                overlap_dict[(mlab, dlab)] = val

    return overlap_dict