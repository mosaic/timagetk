#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Sophie Ribes <sophie.ribes@inria.fr>
#
#       File contributor(s):
#           Florent Papini <florent.papini@ens-lyon.fr>
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
# ------------------------------------------------------------------------------

import numpy as np

POSS_SET_METRICS = ['jaccard coefficient', 'mean overlap', 'target overlap',
                    'volume similarity', 'false negative error',
                    'false positive error', 'sensitivity', 'conformity']
DEF_SET_METRIC = POSS_SET_METRICS[0]


def sets_metrics(set_1, set_2, metric=None):
    # TODO: add specificity, compound
    # TODO: add "overlap coefficient": https://en.wikipedia.org/wiki/Overlap_coefficient
    """Compute metrics between sets (overlap measurements for individual labelled region).

    Available metrics are:

      - **Jaccard coefficient**: J(A, B) = |A n B|/|A u B| [1]
      - **Mean overlap**: A.K.A. Dice coefficient, M(A, B) = 2 * (|A n B| / (|A|+|B|))
      - **Target overlap**: T(A, B) = |A n B|/|B|
      - **Volume similarity**: V(A, B) = 2 * (|A|-|B|)/(|A|+|B|)
      - **False negative error**: Fn(A, B) = |B \ A|/|B|
      - **False positive error**: Fp(A, B) = |A \ B|/|A|
      - **Sensitivity**: S(A, B) = |A n B| / (|A n B| + Fn(A, B))
      - **Conformity**: C(A, B) = 1 - ((Fp(A, B) + Fn(A, B))/|A n B|)

    Parameters
    ----------
    set_1 : set
        first set of values (source)
    set_2 : set
        second set of values (target)
    metric : str in POSS_SET_METRICS, optional
        set metric to use, default is 'Jaccard coefficient'.

    Returns
    -------
    float
        metric value depending on the selected `criterion`.

    Reference
    ---------
    .. [1] `https://en.wikipedia.org/wiki/Jaccard_index`_
    .. [#] Chang et al., *Performance measure characterization for evaluating neuroimage segmentation algorithms*, **NeuroImage**, 2009 Aug 1;47(1):122-35. `10.1016/j.neuroimage.2009.03.068 <https://doi.org/10.1016/j.neuroimage.2009.03.068>`_

    Example
    -------
    >>> from timagetk.third_party.ctrl.algorithm.metrics import sets_metrics
    >>> from timagetk.third_party.ctrl.algorithm.metrics import POSS_SET_METRICS
    >>> set_1 = {0, 1, 2, 3, 4, 5}
    >>> set_2 = {2, 3, 4, 5, 6, 7}
    >>> sets_metrics(set_1, set_2)
    0.5
    >>> {metric: sets_metrics(set_1, set_2, metric=metric) for metric in POSS_SET_METRICS}
    {'Jaccard coefficient': 0.5,
     'Mean overlap': 0.667,
     'Target overlap': 0.667,
     'Volume similarity': 0.0,
     'False negative error': 0.333,
     'False positive error': 0.333,
     'Sensitivity': 0.923,
     'Conformity': 0.833}

    """
    try:
        assert isinstance(set_1, set)
    except AssertionError:
        raise TypeError('Input `set_1` must be a ``set`` instance!')
    try:
        assert isinstance(set_2, set)
    except AssertionError:
        raise TypeError('Input `set_2` must be a ``set`` instance!')

    if metric.lower() not in POSS_SET_METRICS:
        log.info('Possible criteria can be either:', POSS_SET_METRICS)
        metric = DEF_SET_METRIC
        log.info('Setting criterion to:', metric)

    if metric.lower() == 'jaccard coefficient':
        # Jaccard distance
        jaccard_coefficient = float(len(set_1.intersection(set_2))) / float(len(set_1.union(set_2)))
        metric = jaccard_coefficient

    elif metric.lower() == 'mean overlap':
        # Mean overlap (Dice coefficient)
        mean_overlap = 2.0 * (float(len(set_1.intersection(set_2))) / (float(len(set_1)) + float(len(set_2))))
        metric = mean_overlap

    elif metric.lower() == 'target overlap':
        # Target overlap
        target_overlap = float(len(set_1.intersection(set_2))) / float(len(set_2))
        metric = target_overlap

    elif metric.lower() == 'volume similarity':
        # Volume similarity
        volume_similarity = 2.0 * ((float(len(set_1)) - float(len(set_2))) / (float(len(set_1)) + float(len(set_2))))
        metric = volume_similarity

    elif metric.lower() == 'false negative error':
        # False negative error
        false_negative_error = float(len(set_2.difference(set_1))) / float(len(set_2))
        metric = false_negative_error

    elif metric.lower() == 'false positive error':
        # False positive error
        false_positive_error = float(len(set_1.difference(set_2))) / float(len(set_1))
        metric = false_positive_error

    elif metric.lower() == 'sensitivity':
        # Sensitivity
        false_negative_error = float(len(set_2.difference(set_1))) / float(len(set_2))
        sensitivity = float(len(set_1.intersection(set_2))) / (float(len(set_1.intersection(set_2))) + false_negative_error)
        metric = sensitivity

    elif metric.lower() == 'conformity':
        # Conformity
        tp = float(len(set_1.intersection(set_2)))
        fpe = float(len(set_1.difference(set_2))) / float(len(set_1))
        fne = float(len(set_2.difference(set_1))) / float(len(set_2))
        if tp != 0:
            metric = 1.0 - ((fpe + fne) / tp)
        elif tp == 0:
            # failure !
            metric = np.inf
    else:
        pass  # `metric` was evaluated at the beginning so you should not end up here!

    return np.around(metric, decimals=3).tolist()


def img_metrics(mask1, mask2, metric='Jaccard coefficient'):
    """Compute metrics between masks (overlap measurements for individual labelled region).

    Available metrics are:

      - **Jaccard coefficient**: J(A, B) = |A n B|/|A u B| [1]
      - **Mean overlap**: A.K.A. Dice coefficient, M(A, B) = 2 * (|A n B| / (|A|+|B|))
      - **Target overlap**: T(A, B) = |A n B|/|B|
      - **Volume similarity**: V(A, B) = 2 * (|A|-|B|)/(|A|+|B|)
      - **False negative error**: Fn(A, B) = |B \ A|/|B|
      - **False positive error**: Fp(A, B) = |A \ B|/|A|
      - **Sensitivity**: S(A, B) = |A n B| / (|A n B| + Fn(A, B))
      - **Conformity**: C(A, B) = 1 - ((Fp(A, B) + Fn(A, B))/|A n B|)

    Parameters
    ----------
    mask1 : ndarray
        image 1 (source)
    mask2 : ndarray
        image 2 (target)
    metric : str, optional
        metric to use, default is 'Jaccard coefficient'.

    Returns
    -------
    float
        metric value


    Example
    -------
    >>> import numpy as np
    >>> from timagetk.third_party.ctrl.algorithm.metrics import img_metrics
    >>> from timagetk.third_party.ctrl.algorithm.metrics import POSS_SET_METRICS
    >>> mask1 = np.array([[0, 0, 1, 1, 1],
                          [0, 1, 1, 1, 0],
                          [1, 1, 1, 0, 0]])
    >>> mask2 = np.array([[1, 1, 1, 0, 0],
                          [0, 1, 1, 1, 0],
                          [0, 0, 1, 1, 1]])
    >>> img_metrics(mask1, mask2)
    0.385
    >>> {metric: img_metrics(mask1, mask2, metric=metric) for metric in POSS_SET_METRICS}
    {'Jaccard coefficient': 0.385,
     'Mean overlap': 0.556,
     'Target overlap': 0.556,
     'Volume similarity': 17.0,
     'False negative error': 0.444,
     'False positive error': 0.444,
     'Sensitivity': 1.444,
     'Conformity': 0.822}

    """
    if metric not in POSS_SET_METRICS:
        log.info('Possible criteria can be either:', POSS_SET_METRICS)
        metric = DEF_SET_METRIC
        log.info('Setting criterion to:', metric)

    if metric.lower() == 'jaccard coefficient':
        # Jaccard distance
        jaccard_coefficient = (mask1 & mask2).sum() / float((mask1 | mask2).sum())
        metric = jaccard_coefficient

    elif metric.lower() == 'mean overlap':
        # Mean overlap (Dice coefficient)
        mean_overlap = 2.0 * ((mask1 & mask2).sum()) / float(mask1.sum() + mask2.sum())
        metric = mean_overlap

    elif metric.lower() == 'target overlap':
        # Target overlap
        target_overlap = (mask1 & mask2).sum() / float(mask2.sum())
        metric = target_overlap

    elif metric.lower() == 'volume similarity':
        # Volume similarity
        volume_similarity = 2.0 * (mask1.sum() - mask2.sum() / float(mask1.sum() + mask2.sum()))
        metric = volume_similarity

    elif metric.lower() == 'false negative error':
        # False negative error
        false_negative_error = (mask1 & (mask1 ^ mask2)).sum() / float(mask2.sum())
        metric = false_negative_error

    elif metric.lower() == 'false positive error':
        # False positive error
        false_positive_error = (mask2 & (mask1 ^ mask2)).sum() / float(mask1.sum())
        metric = false_positive_error

    elif metric.lower() == 'sensitivity':
        # Sensitivity
        false_negative_error = (mask1 & (mask1 ^ mask2)).sum() / float(mask2.sum())
        sensitivity = (mask1 & mask2).sum() / (mask1 & mask2).sum() + false_negative_error
        metric = sensitivity

    elif metric.lower() == 'conformity':
        # Conformity
        tp = (mask1 & mask2).sum()
        fpe = (mask2 & (mask1 ^ mask2)).sum() / float(mask1.sum())
        fne = (mask1 & (mask1 ^ mask2)).sum() / float(mask2.sum())
        if tp != 0:
            metric = 1.0 - ((fpe + fne) / tp)
        elif tp == 0:
            # failure !
            metric = np.inf
    else:
        pass  # `metric` was evaluated at the beginning so you should not end up here!

    return np.around(metric, decimals=3).tolist()
