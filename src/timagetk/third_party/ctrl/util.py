#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Florent Papini <florent.papini@ens-lyon.fr>
#
#       File contributor(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#           Manuel Petit <manuel.petit@inria.fr>
#
# ------------------------------------------------------------------------------

import os
import numpy as np

import timagetk
from timagetk.third_party.ctrl.lineage import Lineage

from timagetk.bin.logger import get_logger
log = get_logger(__name__)

_ROOT = os.path.abspath(timagetk.__path__[0] + "/../../")


def is_segmented(img):
    """Check if an input image is segmented

    Parameters
    ----------
    img : LabelledImage
        image to verify

    Returns
    -------
    boolean
     True is the image is segmented, False if not

    """
    # - Compare the initial image to a translated one
    return (img.get_array()[:-1] == img.get_array()[1:]).sum() / float(np.prod(img[:-1].shape)) > 0.8


def shared_folder(subdir=''):
    """Get the absolute path to the shared folder ``ctrl/data/``.

    Parameters
    ----------
    subdir : str, optional
        sub directory to use, will be created if missing

    Returns
    -------
    str
        the absolute path to shared folder
    """
    shared_dir = os.path.join(_ROOT, 'data', 'p58_down_interp_2x', subdir)
    if not os.path.exists(shared_dir):
        os.mkdir(shared_dir)

    return shared_dir


def shared_data(filename, subdir=''):
    """Get absolute path to given ``filename`` must be a shared data.

    Append given ``filename`` to the absolute path to the shared folder
    ``ctrl/share/data/``.

    Parameters
    ----------
    filename : str
        name of a shared file found in the ``shared_folder()``
    subdir : str, optional
        sub directory to use, will be created if missing

    Returns
    -------
    str
        absolute path to the filename
    """
    shared_dir = shared_folder(subdir)
    return os.path.join(shared_dir, str(filename))


def csv_lineage(lineage, t1, t2, datapath, name):
    """Save the lineage as a csv file

    Parameters
    ----------
    lineage : dictionary {tuple : float} or list [tuples]
         dictionary with the lineage and the according Jaccard coefficient
    t1 : str
        time of the first image
    t2 : str
        time of the second image
    datapath : str
        path to save the file
    name : str
        name of the file

    Returns
    -------
        save the lineage in a csv file

    """
    # - File creation
    if not name.endswith('.csv'):
        name += '.csv'
    csvfile = open(os.path.join(datapath, name), 'w')
    csvfile.write("mother_label,label,previous_time,time\n")

    # - Adding lineages to the file
    if isinstance(lineage, list):
        for tupl in lineage:
            csvfile.write('{},{},{},{}\n'.format(tupl[0], tupl[1], t1, t2))
    elif isinstance(lineage, dict):
        for key in lineage:
            # - If lineage is {(2, 2) : 0.8, ...}
            if isinstance(lineage[key], float):
                csvfile.write('{},{},{},{}\n'.format(key[0], key[1], t1, t2))
            # - If lineage is {2 : [2], ...}
            elif isinstance(lineage[key], list):
                for daughter in lineage[key]:
                    csvfile.write('{},{},{},{}\n'.format(key, daughter, t1, t2))
    elif isinstance(lineage, Lineage):
        lin = lineage.lineage_dict
        for key in lin:
            for daughter in lin[key]:
                csvfile.write('{},{},{},{}\n'.format(key, daughter, t1, t2))
    else:
        log.error("Wrong lineage type. Lineages can be : list, dict, Lineage")


def lin_to_csv(input_path, output_path, name):
    """Convert files .lin in a .csv for easier comparison

    Parameters
    ----------
    input_path : str
        path of the directory containing .lin files
    output_path : str
        path of the output directory
    name : str
        name of the file

    Returns
    -------
    save the csv files in a chosen directory

    """
    for file in os.listdir(input_path[:-1]):
        lin = np.load(input_path + file, encoding='bytes', allow_pickle=True)

        # - Reverse the tuple in order tu have (float_img, ref_img)
        lin_reversed = sorted([x[::-1] for x in lin[0]])
        filename = file.split('_')
        csv_lineage(lin_reversed, str(filename[-3][:-1]), str(filename[-1][:-5]), output_path, name)