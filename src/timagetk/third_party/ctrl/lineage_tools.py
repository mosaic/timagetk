#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Florent Papini <florent.papini@ens-lyon.fr>
#
#       File contributor(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#           Manuel Petit <manuel.petit@inria.fr>
#
# ------------------------------------------------------------------------------

import numpy as np
import pandas as pd

from timagetk.components.spatial_image import SpatialImage
from timagetk.algorithms.trsf import apply_trsf
from timagetk.components.labelled_image import LabelledImage, relabel_from_mapping

from timagetk.bin.logger import get_logger
log = get_logger(__name__)

def filter_filiation_by_label(df_filiation, mother_to_keep=None, daughter_to_keep=None, keep_filiation=True):
    """ Filter the filiation in the dataframe based on labels (mother, daughter, both)

    Parameters
    ----------
    df_filiation : dataframe
        dataframe containing the filiation as returned by CellTracking.lineage_dict
    mother_to_keep : list, optional
        list of the mother label to keep. if None all the mother label are kept
    daughter_to_keep : list, optional
        list of the daughter label to keep. if None all the daughter label are kept
    keep_filiation : bool, optional
        if True the filtering respect the filiation in case of division (if not all the daughter and mother are kept, all are avoided)
    Returns
    -------
    df_filiation_filtered : dataframe
        dataframe containing only the wanted label (mother, daughter, both)

    Examples
    --------
    # construct the data:
    >>> import pandas as pd
    >>> from timagetk.third_party.ctrl.lineage_tools import filter_filiation_by_label
    >>> data = {'mother_label':[1, 2, 3, 3, 4, 5, 6, 6, 7, 8], 'daughter_label':[10, 9, 8, 7, 6, 5, 4, 3, 2, 1], 'cost':[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]} # cost is here to fit with the output format of CellTracking, not needed
    >>> df_filiation = pd.DataFrame(data, columns = list(data.keys()))
    >>> print('The following dataframe need to be filtered:')
    >>> print(df_filiation)
    >>> lab1, lab2 = [1, 3, 5, 6, 8], [10, 8, 7, 6, 5, 4, 1]
    # Example 1: Filter the mother label only
    >>> df_filiation_filtered = filter_filiation_by_label(df_filiation, mother_to_keep = lab1)
    >>> print("")
    >>> print('We want to keep the following mother label : ' + str(lab1))
    >>> print(df_filiation_filtered)
    # Example 2: Filter the daughter label only, do not respect filiation
    >>> df_filiation_filtered = filter_filiation_by_label(df_filiation, daughter_to_keep = lab2, keep_filiation=False)
    >>> print("")
    >>> print('We want to keep the following daughter label : ' + str(lab2))
    >>> print(df_filiation_filtered)
    >>> print('Notice that the mother n°6 has lost one of her daughter (n°3)\n')
    # Example 3: Filter the daughter label only, respect filiation
    >>> print('This is the same filtering with filiation taking in account:')
    >>> df_filiation_filtered = filter_filiation_by_label(df_filiation, daughter_to_keep = lab2)
    >>> print(df_filiation_filtered)
    """
    # - do some check on the data type and format
    try:
        assert isinstance(df_filiation, pd.DataFrame)
    except:
        raise ValueError

    try:
        assert all(col in df_filiation.columns.to_list() for col in ['mother_label', 'daughter_label'])
    except:
        raise ValueError

    if mother_to_keep is None:
        mother_to_keep = np.unique(df_filiation.mother_label.values)

    if daughter_to_keep is None:
        daughter_to_keep = np.unique(df_filiation.daughter_label.values)

    # - filter mother label
    df = df_filiation.loc[df_filiation.mother_label.isin(mother_to_keep)]

    if keep_filiation:
        # - get the list of the mother label removed according to the given daughter label list (if same mother for multiple daughters!)
        mother_to_delete = df.mother_label.loc[~df.daughter_label.isin(daughter_to_keep)].values

        # - filter daughter label
        df_filiation_filtered = df.loc[~df.mother_label.isin(mother_to_delete)]
    else:
        # - filter daughter label
        df_filiation_filtered = df.loc[df.daughter_label.isin(daughter_to_keep)]

    return df_filiation_filtered


def analyze_filiation(df_pred=None, df_true=None, averageFlag=False):
    """Analyze the filiation qualitatively (correct filiation, partially correct filiation, ...)

    Parameters
    ----------
    df_pred: dataframe
        must contain the column {'mother_label','daughter_label','cost'}. can use the attributes 'lineage_dict' from tracking
    df_true: dataframe
        must contain the column {'mother_label','daughter_label'}
    Returns
    -------
    df_mother_filiation : dataframe
        dataframe containing the analyze of the filiation quality from the mother point of view
    df_daughter_filiation : dataframe
        dataframe containing the analyze of the filiation quality from the daughter point of view

    """
    # - check that the columns and type are valid
    try:
        assert isinstance(df_pred, pd.DataFrame)
        assert isinstance(df_true, pd.DataFrame)
    except:
        raise ValueError

    try:
        assert all(col in df_pred.columns.to_list() for col in ['mother_label', 'daughter_label', 'cost'])
        assert all(col in df_true.columns.to_list() for col in ['mother_label', 'daughter_label'])

    except:
        raise ValueError

    df_true = df_true[['mother_label', 'daughter_label']]
    df_pred = df_pred[['mother_label', 'daughter_label', 'cost']]

    # MOTHER #
    # Group the filiation according to the mother label
    gb_pred = df_pred.groupby('mother_label')['daughter_label'].unique()
    gb_true = df_true.groupby('mother_label')['daughter_label'].unique()
    df = pd.merge(gb_pred, gb_true, how='right', left_index=True, right_index=True, suffixes=('_pred', '_true'))

    # Fill the missing value with an empty list
    for row in df.loc[df.daughter_label_pred.isnull(), 'daughter_label_pred'].index:
        df.at[row, 'daughter_label_pred'] = []

    # Analyze the filiation quality
    mother_filiation = []
    true_number_daughter = []
    pred_number_daughter = []
    for idx, row in df.iterrows():
        pred_set = set(row.daughter_label_pred)

        if len(pred_set) == 0:
            mother_filiation.append("Missing")
        else:
            true_set = set(row.daughter_label_true)
            if pred_set == true_set:
                mother_filiation.append("Correct")
            elif len(pred_set.intersection(true_set)) > 0:
                mother_filiation.append("Some")
            else:
                mother_filiation.append("None")

        true_number_daughter.append(len(row.daughter_label_true))
        pred_number_daughter.append(len(row.daughter_label_pred))

    # add cost (mean for multiple daughter)
    if averageFlag is True:
        gb_cost = df_pred.groupby('mother_label')['cost'].mean()
    else:
        gb_cost = df_pred.groupby('mother_label')['cost'].unique()

    df_cost = pd.merge(gb_cost, gb_true, how='right', left_index=True, right_index=True, suffixes=('_pred', '_true'))
    df_cost.fillna(1, inplace=True)

    # count number of daughter
    gb_division = df_pred.groupby('mother_label')['daughter_label'].count()
    df_division = pd.merge(gb_division, gb_true, how='right', left_index=True, right_index=True,
                           suffixes=('_pred', '_true'))
    df_division.fillna(0, inplace=True)

    # df_division.daughter_label_pred.values.tolist()
    df_mother_filiation = pd.DataFrame(list(
        zip(df.index.to_list(), mother_filiation, df_cost.cost.values.tolist(),
            true_number_daughter, pred_number_daughter)),
        columns=['label', 'daughter_filiation', 'mean_cost_filiation', 'true_number_daughter', 'pred_number_daughter'])

    # DAUGHTER #
    df_pred.daughter_label.fillna(-1, inplace=True)  # avoid issue for mother with missing daughters
    df = pd.merge(df_pred, df_true[['mother_label', 'daughter_label']], how='right', on='daughter_label',
                  suffixes=('_pred', '_true'))
    df = df.set_index('daughter_label')

    df.cost.fillna(1, inplace=True)

    # Analyze the filiation quality
    daughter_filiation = []
    for idx, row in df.iterrows():
        if np.isnan(row.mother_label_pred):
            daughter_filiation.append("Missing")
        elif row.mother_label_pred == row.mother_label_true:
            daughter_filiation.append("Correct")
        else:
            daughter_filiation.append("None")

    df_daughter_filiation = pd.DataFrame(
        list(zip(df.index.to_list(), daughter_filiation, df.cost.values.tolist())),
        columns=['label', 'mother_filiation', 'cost_filiation'])

    return df_mother_filiation, df_daughter_filiation


def filiation_accuracy(df_pred=None, df_true=None, normalize=True, verbose=False):
    """ Calculate the percentage of lineage predicted that matched the true lineage

    Parameters
    ----------
    df_pred: dataframe
        must contain the column {'mother_label','daughter_label','cost'}. can use the attributes 'lineage_dict' from tracking
    df_true: dataframe
        must contain the column {'mother_label','daughter_label'}
    normalize: bool, optional
        normalize the score using the number of mother cell
    verbose: bool, optional
        print the comparison between the true lineage and predicted one when discrepancies are found

    Returns
    -------
        score: float
    """

    # - check that the columns and type are valid
    try:
        assert isinstance(df_pred, pd.DataFrame)
        assert isinstance(df_true, pd.DataFrame)
    except:
        raise ValueError

    try:
        assert all(col in df_pred.columns.to_list() for col in ['mother_label', 'daughter_label', 'cost'])
        assert all(col in df_true.columns.to_list() for col in ['mother_label', 'daughter_label'])

    except:
        raise ValueError

    # - group the filiation according to the mother label
    gb_pred = df_pred.groupby('mother_label')['daughter_label'].unique()
    gb_true = df_true.groupby('mother_label')['daughter_label'].unique()

    # - create a dataframe to gather the filiation according to the ground truth
    df = pd.merge(gb_pred, gb_true, how='right', left_index=True, right_index=True, suffixes=('_pred', '_true'))

    # - replace nan value by empty list if any (if mother_label exists in the ground_truth but not in the predicted df)
    for row in df.loc[df.daughter_label_pred.isnull(), 'daughter_label_pred'].index:
        df.at[row, 'daughter_label_pred'] = []

    # - compare both columns
    tf = df.apply(lambda row: set(row.daughter_label_pred) == set(row.daughter_label_true), axis=1)

    # - compute the accuracy score
    score = tf.values.sum()

    if verbose:
        log.info(str(score) + '/' + str(len(tf)) + ' of the filiation are correct')

    if normalize:
        score /= len(tf)

    # - if verbose, show the incorrect filiation
    if verbose:
        log.info('List of the incorrect filiation:')
        log.info(df.loc[tf == False])

    n = len(tf)  # number of filiation

    return score, df.loc[tf == False], n


def registration_quality(float_img, ref_img, res_img, ref_seg=None):
    """Estimate registration quality

    Parameters
    ----------
    float_img : SpatialImage
        Image to compare
    ref_img : SpatialImage
        Image to compare
    res_img : SpatialImage
        Image to compare
    ref_seg : LabelledImage, optional
        image from which to extract background

    Returns
    -------
        Return quadratic error between the images before and after the registration

    """
    err_quad_before, err_quad, ratio = None, None, None
    # - Computes quadratic error before registration
    if float_img is not None and float_img is not None:
        float_in_ref = apply_trsf(float_img, trsf=None, template_img=ref_img)
        err_quad_before = quadratic_error_computing(ref_img, float_in_ref, ref_seg)
        # - Computes quadratic error after registration
        if res_img is not None:
            err_quad = quadratic_error_computing(ref_img, res_img, ref_seg)
            ratio = err_quad_before / err_quad
            log.info("Alignment was improved by : " + str(ratio) + ", and the final quadratic error is : " +
                  str(err_quad) + "this error should be below 0,15 for a good alignment")
        else:
            log.error("Wrong parameters given")
    else:
        log.error("Wrong parameters given")
    return err_quad_before, err_quad, ratio


def quadratic_error_computing(img1, img2, img1_seg=None):
    """Estimate quadratic error between two images

    Parameters
    ----------
    img1 : SpatialImage
        image to compare
    img2 : SpatialImage
        image to compare
    img1_seg : LabelledImage, optional
        image from which to extract background

    Returns
    -------
        Return quadratic error between the two input images

    """

    # - Computing useful coefficients
    mu1 = np.mean(img1[img1_seg != 1])
    sigma1 = np.std(img1[img1_seg != 1])
    mu2 = np.mean(img2[img1_seg != 1])
    sigma2 = np.std(img2[img1_seg != 1])

    # - Computing intensities normalisation
    img1_norm = 0.5 + 1 / 6. * ((img1.get_array().astype(float) - mu1) / sigma1)
    img2_norm = 0.5 + 1 / 6. * ((img2.get_array().astype(float) - mu2) / sigma2)

    # - Computing quadratic error
    diff = img1_norm - img2_norm
    signal = np.maximum(img1_norm, img2_norm)
    err_quad = np.sqrt(np.sum(np.power(diff, 2) * signal) / np.sum(signal))

    log.info("Quadratic error for registration is : " + str(err_quad))

    return err_quad


def neighbors_sim_weighted(mother_seg, daughter_seg, lineage_dict, method='jaccard'):
    """Neighborhood preservation criteria for a given lineage evaluate using contact area as weight

    Parameters
    ----------
    mother_seg : LabelledImage
        input image to compute neighbors that contains mother cells
    daughter_seg : LabelledImage
        input image to compute neighbors that contains daughter cells
    lineage_dict : dictionary {daughter : mother}
        a dictionary of the lineage
    method : str, optional
        define the way the neighborhood preservation is compute {'mother','daughter','jaccard'}
        'mother' :   |set(mother_neigh).intersection(set(daughter_neigh))| / |set(mother_neigh)|
        'daughter' : [set(mother_neigh).intersection(set(daughter_neigh))| / |set(daughter_neigh)|
        'jaccard' : [set(mother_neigh).intersection(set(daughter_neigh))| / |set(daughter_neigh).union(set(mother_neigh))|

        Note: | | == len()

    Returns
    -------
    neighbors_sim_weighted : dict
        returns the neighborhood conformity for each mother label regions
    """

    try:
        assert isinstance(mother_seg, LabelledImage) and isinstance(daughter_seg, LabelledImage)
    except AssertionError:
        msg = 'Input images should be LabelledImage object'

        raise TypeError(msg)

    try:
        assert method in ['mother', 'daughter', 'jaccard']
    except:
        raise ValueError

    # - Assert that the couple_dict contains only integer
    lineage_dict = {int(k): int(v) for k, v in lineage_dict.items()}

    # - Get the mother to remap on the segmented images
    mother_map = {k: k for k in lineage_dict.values()}

    # - Relabel mother and daughter images
    mother_seg_remap = relabel_from_mapping(mother_seg, mother_map)
    daughter_seg_remap = relabel_from_mapping(daughter_seg, lineage_dict)

    # - Get the area of contact and select neighbors
    mother_face = mother_seg_remap.face_coordinates()
    daughter_face = daughter_seg_remap.face_coordinates()

    #########################################################
    # BUG FOUND IN .FACE_COORDINATES()
    #
    # In the following example:
    # im =  [2, 2, 2, 2, 3, 3, 3],
    #       [2, 2, 2, 3, 3, 3, 3],
    #       [4, 4, 4, 4, 5, 5, 5],
    #       [4, 4, 4, 4, 5, 5, 5],
    # im.faces() do not contain (3, 4)
    # However im.neighbors() return 3 and 4 as neighbors
    # So output from this function and neighbors_sim can be different
    #########################################################

    mother_area = {k: v.shape[0] for k, v in mother_face.items()}  # get the area from the number of surface pixel
    daughter_area = {k: v.shape[0] for k, v in daughter_face.items()}

    # - Get the neighbors

    # - Reformat dict for easier access
    mother_neigh = {l: {(k[0] if k[0] != l else k[1]): v for k, v in mother_area.items() if (k[0] == l) | (k[1] == l)}
                    for l in mother_seg_remap.labels()}
    daughter_neigh = {l: {(k[0] if k[0] != l else k[1]): v for k, v in daughter_area.items() if (k[0] == l) | (k[1] == l)}
                      for l in daughter_seg_remap.labels()}

    # - Check if mother are missing (will produce an error lator if method 'mother' or 'jaccard' is selected
    mother_label = set(mother_seg_remap.labels())
    daughter_label = set(daughter_seg_remap.labels())

    missing_mother = mother_label - daughter_label
    missing_daughter = mother_label - daughter_label

    # - Remove them from the neighbors list
    mother_neigh = {k: v for k, v in mother_neigh.items() if k not in missing_mother}

    # - Get the neighbors that have changed according to a given metric
    if method == 'mother':
        neighbors_diff = {k: set(v) - set(v).intersection(set(daughter_neigh[k])) for k, v in mother_neigh.items()}
    elif method == 'daughter':
        neighbors_diff = {k: set(daughter_neigh[k]) - set(v).intersection(set(daughter_neigh[k])) for k, v in mother_neigh.items()}
    else:
        neighbors_diff = {k: set(v).union(set(daughter_neigh[k])) - set(v).intersection(set(daughter_neigh[k])) for k, v in mother_neigh.items()}

    # - For each non-empty keys, calculate the weighted part of the missing neighbors based on its contact area
    neighbors_sim = {k: 1 for k in mother_neigh}

    for k, neigh_list in neighbors_diff.items():
        if len(neigh_list) > 0:
            # - For each missing label, get its proportional weight based on contact area and substract this weight to the current neighbors_sim
            for n in neigh_list:
                if n in mother_neigh[k]:
                    neighbors_sim[k] -= mother_neigh[k][n] / sum(mother_neigh[k].values())
                    log.info("Cell n°" + str(k) + " : missing labels found --> " + str(n) + "  " + str(mother_neigh[k][n]))
                else:
                    neighbors_sim[k] -= daughter_neigh[k][n] / sum(daughter_neigh[k].values())
                    log.info("Cell n°" + str(k) + " : missing labels found --> " + str(n) + "  " + str(daughter_neigh[k][n]))

    # - Add the missing labels
    neighbors_sim = {**neighbors_sim, **{k: 0 for k in missing_mother}}

    if len(missing_daughter) > 0:
        neighbors_sim = {**neighbors_sim, **{'missing_daughter': missing_daughter}}

    return neighbors_sim


def neighbors_sim(mother_seg, daughter_seg, lineage_dict, method='jaccard', flag_cross_pattern=False):
    """Neighborhood preservation criteria for a given lineage

    Parameters
    ----------
    mother_seg : LabelledImage
        input image to compute neighbors that contains mother cells
    daughter_seg : LabelledImage
        input image to compute neighbors that contains daughter cells
    lineage_dict : dictionary {daughter : mother}
        a dictionary of the lineage
    method : str, optional
        define the way the neighborhood preservation is compute {'mother','daughter','jaccard'}
        'mother' :   |set(mother_neigh).intersection(set(daughter_neigh))| / |set(mother_neigh)|
        'daughter' : [set(mother_neigh).intersection(set(daughter_neigh))| / |set(daughter_neigh)|
        'jaccard' : [set(mother_neigh).intersection(set(daughter_neigh))| / |set(daughter_neigh).union(set(mother_neigh))|

        Note: | | == len()

    Returns
    -------
    neighbors_sim : dict
        returns the neighborhood conformity for each mother label regions
    """

    try:
        assert isinstance(mother_seg, LabelledImage) and isinstance(daughter_seg, LabelledImage)
    except AssertionError:
        msg = 'Input images should be LabelledImage object'

        raise TypeError(msg)

    try:
        assert method in ['mother', 'daughter', 'jaccard']
    except:
        raise ValueError

    # - Assert that the couple_dict contains only integer
    lineage_dict = {int(k): int(v) for k, v in lineage_dict.items()}

    # - Get the mother to remap on the segmented images
    mother_map = {k: k for k in lineage_dict.values()}

    # - Relabel mother and daughter images
    mother_seg_remap = relabel_from_mapping(mother_seg, mother_map)
    daughter_seg_remap = relabel_from_mapping(daughter_seg, lineage_dict)

    # - Compute neighbors
    mother_neigh = mother_seg_remap.neighbors()
    daughter_neigh = daughter_seg_remap.neighbors()

    # - Check if mother are missing (will produce an error lator if method 'mother' or 'jaccard' is selected
    mother_label = set(mother_seg_remap.labels())
    daughter_label = set(daughter_seg_remap.labels())

    missing_mother = mother_label - daughter_label
    missing_daughter = mother_label - daughter_label

    # - Remove them from the neighbors list
    mother_neigh = {k: v for k, v in mother_neigh.items() if k not in missing_mother}
    daughter_neigh = {k: v for k, v in daughter_neigh.items() if k not in missing_daughter}

    # - Compute neighbors similarity
    if method == 'mother':
        denominator = {k: len(v) for k, v in mother_neigh.items()}
    elif method == 'daughter':
        denominator = {k: len(v) for k, v in daughter_neigh.items()}
    else:
        denominator = {k: len(set(mother_neigh[k]).union(set(daughter_neigh[k]))) for k in mother_neigh}

    neighbors_sim = {k: len(set(mother_neigh[k]).intersection(set(daughter_neigh[k]))) / denominator[k] for k
                     in mother_neigh}

    ##################################################
    # EXPERIMENTAL PART TO TREAT THE PROBLEM OF CROSSED-PATTERNED:
    # Situation at t:
    # B B B B B B C C C C
    # B B B B C C C C C C
    # A A A D D D D D D D    --> B and D are neighbors but not A and C
    # A A A D D D D D D D
    # A A A A A D D D D D
    #
    # Situation at t+dt
    # B B B B B B C C C C
    # B B B B B C C C C C
    # A A A A A C C C C C    --> A and C are neighbors but not B and D
    # A A A A A A A C C C
    # A A A A A A A D D D
    #
    # We assume that because the contact area between A and C is greater at t+dt --> A and C should be neighbors all the time
    # However for segmentations reasons and few pixels difference it is not the case
    #
    # We can track this issue by checking all the labels with neighborhood preservation problem:
    # For each labels that we will called A (as in the example above), we want to find B, C and D with the following conditions:
    # 1. A and C should be neighbors at one time but not at the other
    # 2. B and D should be neighbors at one time but not at the other
    # 3. A should have B and D as neighbors for both time
    # 4. C should have B and D as neighbors for both time

    # copy the neighborhood dictionary to avoid problems
    if flag_cross_pattern:
        mother_neigh_cp = mother_neigh.copy()
        daughter_neigh_cp = daughter_neigh.copy()

        list_potential_A = [k for k in neighbors_sim if neighbors_sim[k] != 1]
        cross_pattern_list = []
        for a in list_potential_A:
            if a not in cross_pattern_list:
                # find potential C: A and C should be neighbors at one time but not at the other
                list_potential_C = set(i for i in set(mother_neigh[a]).symmetric_difference(set(daughter_neigh[a])) if i != 0)

                # find potential B and D: A should have B and D as neighbors for both time
                list_potential_B_D = set(i for i in set(mother_neigh[a]).intersection(set(daughter_neigh[a])) if i != 0)

                # loop over the potential C and check conditions
                for c in list_potential_C:
                    if c not in cross_pattern_list:

                        # filter potential B and D: C should have B and D as neighbors for both time
                        B_D = set(list_potential_B_D).intersection(set(mother_neigh[c]).intersection(set(daughter_neigh[c])))

                        if len(B_D) > 1:  # should have two neighbors at least (B, D)

                            # filter B and D: B and D should be neighbors at one time but not at the other
                            for b in B_D:
                                if b not in cross_pattern_list:
                                    neigh_b_with_problem = set(mother_neigh[b]).symmetric_difference(set(daughter_neigh[b]))
                                    D = B_D.intersection(neigh_b_with_problem)

                                    if len(D) > 0:
                                        d = list(D)
                                        if len(D) > 1:
                                            log.warning("PROBLEM!!")
                                        else:
                                            d = d[0]
                                            # - correct the neighborhood dictionary
                                            if c not in mother_neigh[a]:  # check at which time is the problem
                                                mother_neigh_cp[a].append(c)
                                                mother_neigh_cp[c].append(a)
                                                mother_neigh_cp[b].remove(d)
                                                mother_neigh_cp[d].remove(b)
                                            else:
                                                daughter_neigh_cp[a].append(c)
                                                daughter_neigh_cp[c].append(a)
                                                daughter_neigh_cp[b].remove(d)
                                                daughter_neigh_cp[d].remove(b)

                                            cross_pattern_list.append(a)
                                            cross_pattern_list.append(b)
                                            cross_pattern_list.append(c)
                                            cross_pattern_list.append(d)

                                            log.info("Found cross-pattern with A = " + str(a) + ", C = " + str(
                                                c) + ", B = " + str(b) + ", D = " + str(d))

        # - Update the preservation of the neighbors with the new dictionaries
        if method == 'mother':
            denominator = {k: len(v) for k, v in mother_neigh_cp.items()}
        elif method == 'daughter':
            denominator = {k: len(v) for k, v in daughter_neigh_cp.items()}
        else:
            denominator = {k: len(set(mother_neigh_cp[k]).union(set(daughter_neigh_cp[k]))) for k in mother_neigh_cp}

        neighbors_sim = {k: len(set(mother_neigh_cp[k]).intersection(set(daughter_neigh_cp[k]))) / denominator[k] for k
                         in mother_neigh_cp}
    ###############################################################
    # END EXPERIMENTAL PART
    ###############################################################

    # - Add the missing labels
    neighbors_sim = {**neighbors_sim, **{k: 0 for k in missing_mother}}

    if len(missing_daughter) > 0:
        neighbors_sim = {**neighbors_sim, **{'missing_daughter': missing_daughter}}

    return neighbors_sim


def get_lineage_over_time(df_lineage, start_time=None, end_time=None, keep_filiation=True):
    """

    Parameters
    ----------
    df_lineage : dataframe
        dataframe containing filiations at multiple time. It must contains (at least) the following columns:

        * mother_time: time for the mother cell considered. it must be an integer
        * daughter_time: time for the daughter cell considered. it must be an integer
        * mother_label: label for the mother cell considered.
        * daughter_label: label for the daughter cell considered

        Note: mother_time and daughter_time should be an array of consecutive integer.
              if one time is missing, an error will be raise.

        Example of correct input dataframe:

        mother_time | daughter_time | mother_label | daughter_label
              0             1               23              25
              0             1               25              45
              0             1               29              55
              0             1               29              56
              1             2               31              47
              1             2               49              50
              1             2               49              51
              ...           ...             ...             ...

    start_time : int, optional
        integer that indicates the mother_time returned by the function. if None the first mother_time is set
    end_time : int, optional
        integer that indicates the daughter_time returned by the function. if None the last daughter_time is set

    Returns
    -------
    df : dataframe
        dataframe containing the filiation conserved between the start_time and the end_time

    Examples
    --------
    >>> import pandas as pd
    >>> from timagetk.third_party.ctrl.lineage_tools import get_lineage_over_time
    # - Create dataframe
    >>> data = {'mother_time':[0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2], 'daughter_time':[1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3], 'mother_label':[2, 3, 4, 5, 5, 20, 21, 21, 24, 23, 23, 25, 26, 27, 28, 28, 29], 'daughter_label':[20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36]} # cost is here to fit with the output format of CellTracking, not needed
    >>> df_lineage = pd.DataFrame(data, columns = list(data.keys()))
    >>> print(df_lineage)

    # - Select filiations between time 0 and 2
    >>> df_0_2 = get_lineage_over_time(df_lineage, start_time = 0, end_time = 2)
    >>> print(df_0_2)

    # - Selection filiations between time 0 and 3
    >>> df_0_3 = get_lineage_over_time(df_lineage, start_time = 0, end_time = 3)
    >>> print(df_0_3)
    >>> print('Notice that the daughter_cell 30 (time 1-2) is not conserved at time 2-3 thus all the filiations are removed')

    """
    # - Assert that the input dataframe contains the required columns
    try:
        assert isinstance(df_lineage, pd.DataFrame)
        assert all(i in df_lineage.columns.to_list() for i in ['mother_time', 'daughter_time', 'mother_label', 'daughter_label'])
    except:
        raise ValueError("Input dataframe do not contain all the required columns!")

    if start_time is None:
        start_time = min(df_lineage.mother_time)

    if end_time is None:
        end_time = max(df_lineage.daughter_time)

    # - Assert that all the intermediate time between start_time and end_time exists
    try:
        assert all(x in df_lineage.mother_time.values for x in np.arange(start_time, end_time))
        assert all(x in df_lineage.daughter_time.values for x in np.arange(start_time + 1, end_time + 1))
    except:
        raise ValueError("Intermediate times are missing!")

    df = df_lineage.loc[df_lineage.mother_time == start_time]

    # - Loop over the intermediate time to select filiations
    for t in np.arange(start_time, end_time - 1):
        # - get a subset of the lineage
        next_df = df_lineage.loc[df_lineage.mother_time == t + 1]

        # - remove the filiations where one of several daughters are missing at the next time
        filiation = df.groupby('mother_label')['daughter_label'].unique()  # group by mother_label

        if keep_filiation:
            is_conserved = filiation.apply(lambda x: all([i in next_df.mother_label.values for i in x]))
        else:
            is_conserved = filiation.apply(lambda x: True)

        # - remove the mother label that do not conserved all their daughters
        df = df.loc[df.mother_label.isin(is_conserved.loc[is_conserved].index.values)]

        # - merge the dataframe at time t and the dataframe at time t+dt
        df = pd.merge(left=df.loc[df.mother_time == start_time], right=next_df, how='inner',
                      left_on=['daughter_time', 'daughter_label'], right_on=['mother_time', 'mother_label'])

        # - keep only the wanted columns
        df = df[['mother_time_x', 'daughter_time_y', 'mother_label_x', 'daughter_label_y']]

        # - rename the columns
        df.columns = [k.replace('_x', '').replace('_y', '') for k in df.columns.to_list()]

    return df
