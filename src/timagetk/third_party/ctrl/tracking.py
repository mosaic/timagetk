#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       File author(s):
#           Florent Papini <florent.papini@ens-lyon.fr>
#
#       File contributor(s):
#           Guillaume Cerutti <guillaume.cerutti@inria.fr>
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#           Manuel Petit <manuel.petit@inria.fr>
#
#       File maintainer(s):
#           Jonathan Legrand <jonathan.legrand@ens-lyon.fr>
#           Manuel Petit <manuel.petit@inria.fr>
#
# ------------------------------------------------------------------------------
import os
import pickle

import networkx as nx
import numpy as np
import scipy.ndimage as nd

from scipy.spatial.distance import pdist, squareform

from timagetk.third_party.ctrl.algorithm.image_overlap import cell_overlap
from timagetk.third_party.ctrl.lineage import Lineage
from timagetk.third_party.ctrl.matching_flow_graph import MatchingFlowGraph
from timagetk.third_party.ctrl.evaluate_lineage import create_lineage_graph, analyze_graph_mapping, get_mapping_graph, create_adj_graph, filter_mapping
from timagetk.third_party.ctrl.vertex_matching import vertex_assignement_problem
from timagetk.third_party.ctrl.update_lineage import aggregate_hc, initialize_hc

from timagetk.algorithms.blockmatching import blockmatching
from timagetk.algorithms.pointmatching import pointmatching, apply_trsf_to_points
from timagetk.algorithms.trsf import apply_trsf, compose_trsf, inv_trsf
from timagetk.components.tissue_image import TissueImage3D
from timagetk.components.labelled_image import LabelledImage
from timagetk.components.spatial_image import SpatialImage
from timagetk.components.trsf import Trsf

from timagetk.bin.logger import get_logger
log = get_logger(__name__)

#: Valid lineage algorithm names for `CellTracking` instance.
CT_METHODS = ['naive', 'flow_graph']
#: Valid lineage algorithm names for `RobustLineaging` instance.
RL_METHODS = ['iterative']
#: Valid values for transformation direction.
TRSF_DIRECTIONS = ['backward', 'forward']
#: Default value for transformation direction.
DEF_TRSF_DIRECTION = TRSF_DIRECTIONS[0]


class CellTracking(object):
    """Computes lineages by tracking cells between two segmented images using naive or flow graph optimization methods.

    Attributes
    ----------
    mother_img : timagetk.SpatialImage
        Intensity image at time t.
    daughter_img : timagetk.SpatialImage
        Intensity image at time t+dt.
    mother_seg : timagetk.LabelledImage
        Segmented image at time t.
    daughter_seg : timagetk.LabelledImage
        Segmented image at time t+dt.
    trsf : timagetk.Trsf
        Transformation that register the daughter image on the mother image if `trsf_direction == 'backward'` (default).
        If `trsf_direction == 'forward'`, register the mother image on the daughter image.
    init_trsf : timagetk.Trsf
        An initial linear transformation that register the daughter image on the mother image.
        Further improved by a round of blockmatching. Requires the intensity images.
        Typically, a transformation obtained from ``pointmatching``.
    previous_lineage : Lineage
        A previous lineage or expert lineage used to initialise non-linear registration.
    ds : int
        Down-sampling coefficient for image overlap evaluation. Used by the naive lineage method.
    max_cost : float
        Threshold for the cost value, if the value is higher than the couple is deleted from the lineage dictionary.
        Used by the flow lineage method.
    sigma : float
        Parameter for intensity image gaussian filtering.
    hmin : int
        Height value to use in h-extrema transformation.
        Used by automatic watershed method when segmented image are missing.
    trsf_direction : {'backward', 'forward'}
        Defines the temporal direction of initial transformation and estimated registration transformation.
        "backward" lineaging (default), transformation resample daughter image into mother reference frame
        "forward" lineaging, transformation resample mother image into daughter reference frame
    mother_seg_reg : timagetk.LabelledImage
        Segmented image (time t) registered onto the other segmented image (time t+dt) if `trsf_direction` is 'forward'.
        Used to compute the lineage.
    daughter_seg_reg : timagetk.LabelledImage
        Segmented image (time t+dt) registered onto the other segmented image (time t) if `trsf_direction` is 'backward'.
        Used to compute the lineage.
    cost_dict : dict
        Cost dictionary of the possible cell couples (match, division, apparition, ...), {tuple : float}
    lineage_dict : dict
        Lineage dictionary with mother cell and her daughter(s), {int : [int, ...]}
    lineage_cost : dict
        Cost of the choosen `cost_dict` association(s).
    flow_graph : MatchingFlowGraph
        The class used for flow graph optimization method.

    Examples
    --------
    >>> from timagetk.third_party.ctrl.tracking import CellTracking
    >>> from timagetk.components.labelled_image import LabelledImage
    >>> from timagetk.io.dataset import shared_data
    >>> mother_int_img = shared_data('flower_confocal', 0)
    >>> daughter_int_img = shared_data('flower_confocal', 1)
    >>> mother_seg_img = shared_data('flower_labelled', 0)
    >>> daughter_seg_img = shared_data('flower_labelled', 1)
    >>> ct = CellTracking(mother_int_img, daughter_int_img, mother_seg_img, daughter_seg_img)
    >>> lineage_dict = ct.naive_lineage()
    >>> print(lineage_dict)
    >>> from timagetk.third_party.ctrl.visu import show_3D_lineage
    >>> show_3D_lineage(mother_seg_img, daughter_seg_img, lineage_dict)

    >>> # Interactive 3D lineage viewer
    >>> from timagetk.bin.lineage_viewer import lineage_viewer
    >>> lineage_viewer([mother_seg_img, daughter_seg_img], lineage_dict, layers=[1, 2])


    >>> from timagetk.visu.mplt import grayscale_imshow
    >>> grayscale_imshow([mother_int_img, daughter_int_img], title=['t0', 't1'])
    >>> ct = CellTracking(mother_int_img, daughter_int_img, mother_seg_img, daughter_seg_img)
    >>> lineage_dict = ct.naive_lineage()

    >>> # Interactive 3D lineage viewer
    >>> from timagetk.bin.lineage_viewer import lineage_viewer
    >>> lineage_viewer([mother_seg_img, daughter_seg_img], lineage_dict, layers=[1, 2])

    """

    def __init__(self, mother_img=None, daughter_img=None, mother_seg=None, daughter_seg=None, trsf=None, **kwargs):
        """ Constructor.

        Parameters
        ----------
        mother_img : SpatialImage, optional
            Intensity image at time t. If given, the other intensity image should be given.
        daughter_img : SpatialImage, optional
            Intensity image at time t+dt. If given, the other intensity image should be given.
        mother_seg : LabelledImage, optional
            Segmented image at time t. If given, the other segmented image should be given.
        daughter_seg : LabelledImage, optional
            Segmented image at time t+dt. If given, the other segmented image should be given.
        trsf : Trsf, optional
            Transformation that register the daughter image on the mother image if `trsf_direction == 'backward'` (default).
            If `trsf_direction == 'forward'`, register the mother image on the daughter image.
            Warning: if only ``LabelledImage`` are given, the transformation is compulsory.

        Other Parameters
        ----------------
        init_trsf : Trsf
            An initial linear transformation that register the daughter image on the mother image.
            Further improved by a round of blockmatching. Requires the intensity images.
            Typically, a transformation obtained from ``pointmatching``.
        init_lineage_dict : Lineage
            A previous lineage or expert lineage used to initialise non-linear registration.
        ds : int
            Down-sampling coefficient for image overlap evaluation. Used by the naive lineage method.
        max_cost : float
            Threshold for the cost value, if the value is higher than the couple is deleted from the lineage dictionary.
            Used by the flow lineage method.
        sigma : float
            Parameter for intensity image gaussian filtering.
        hmin : int
            Height value to use in h-extrema transformation.
            Used by automatic watershed method when segmented image are missing.
        trsf_direction : {'backward', 'forward'}
            Defines the temporal direction of initial transformation and estimated registration transformation.
            "backward" lineaging (default), transformation resample daughter image into mother reference frame
            "forward" lineaging, transformation resample mother image into daughter reference frame
        reg_highest_level : int
            Highest pyramid level used to estimate image-based transformations by block-matching registration.
        reg_lowest_level : int
            Lowest pyramid level used to estimate image-based transformations by block-matching registration.
        pts_highest_level : int
            Highest pyramid level used to estimate anchors-based transformations by block-matching registration.
        pts_lowest_level : int
            Lowest pyramid level used to estimate anchors-based transformations by block-matching registration.
        anchors_method : {'centroids', 'vertices'}
            Method used to extract anchor points from previous or expert cell lineages.
        max_topo_dist : int
            Topological distance up to which nudges will affect the estimated lineage.
        """
        self.mother_img = mother_img
        self.daughter_img = daughter_img
        self.mother_seg = mother_seg
        self.daughter_seg = daughter_seg
        self.trsf = trsf

        self.init_trsf = kwargs.get('init_trsf', None)
        self.init_lineage_dict = kwargs.get('init_lineage_dict', {})
        self.ds = kwargs.get('ds', 1)
        self.max_cost = kwargs.get('max_cost', 2)
        self.sigma = kwargs.get('sigma', 3)
        self.hmin = kwargs.get('hmin', 6)
        self.trsf_direction = kwargs.get('trsf_direction', DEF_TRSF_DIRECTION)
        self.reg_highest_level = kwargs.get('reg_highest_level', 5)
        self.reg_lowest_level = kwargs.get('reg_lowest_level', 2)
        self.pts_highest_level = kwargs.get('pts_highest_level', 2)
        self.pts_lowest_level = kwargs.get('pts_lowest_level', 2)
        
        self.anchors_method = kwargs.get('anchors_method', 'centroids')
        self.max_topo_dist = kwargs.get('max_topo_dist', 3) # change to 4 if needed

        self.mother_seg_reg = None
        self.daughter_seg_reg = None
        self.cost_dict = None
        self.lineage_dict = None
        self.lineage_cost = None
        self.flow_graph = None

        self.adj_graph_mother = None
        self.adj_graph_daughter = None

        self._check_init()

        self.init_vf_trsf = None # to test initialize trsf if using anchors
        self.true_dict = kwargs.get('true_dict', {}) # to remove (MANU)
        self.matched_pts = {} # to store anchors if using vertices (too long to recompute)
        self.dist_scale = 1 # to modify pointmatching parameters if using vertices instead of centroids

    def _check_init(self):
        if self.mother_img is not None:
            try:
                assert isinstance(self.mother_img, SpatialImage)
            except AssertionError:
                raise TypeError("Mother intensity image should be a SpatialImage instance!")
        if self.daughter_img is not None:
            try:
                assert isinstance(self.daughter_img, SpatialImage)
            except AssertionError:
                raise TypeError("Daughter intensity image should be a SpatialImage instance!")
        if self.mother_seg is not None:
            try:
                assert isinstance(self.mother_seg, LabelledImage)
            except AssertionError:
                raise TypeError("Mother segmented image should be a LabelledImage instance!")
        if self.daughter_seg is not None:
            try:
                assert isinstance(self.daughter_seg, LabelledImage)
            except AssertionError:
                raise TypeError("Daughter segmented image should be a LabelledImage instance!")
        if self.trsf is not None:
            try:
                assert isinstance(self.trsf, Trsf)
            except AssertionError:
                raise TypeError("Transformation should be a Trsf instance!")
        if self.init_trsf is not None:
            try:
                assert self.init_trsf.is_linear()
            except AssertionError:
                raise TypeError("Given `init_trsf` should be a linear transformation!")
        if self.init_lineage_dict: # check if non-empty
            try:
                assert isinstance(self.init_lineage_dict, Lineage)
            except AssertionError:
                raise TypeError("Given `previous_lineage` should be a `Lineage` instance!")
            else:
                self.init_lineage_dict.check_ancestor_image(self.mother_seg)
                self.init_lineage_dict.check_descendant_image(self.daughter_seg)
        try:
            assert self.trsf_direction in TRSF_DIRECTIONS
        except AssertionError:
            raise TypeError(f"Given `trsf_direction` should be in {TRSF_DIRECTIONS}, got '{self.trsf_direction}'!")

    def naive_lineage(self):
        """Compute a lineage using the naive method.

        Returns
        -------
        dict
            Cost dictionary
        Lineage, {int : [int, ...]}
            lineage dictionary with couples and their metrics
        """
        self.init_lineage()
        log.info("Estimating lineage using naive method...")

        # - Computing costs (cells superposition)
        self.eval_cost_from_overlap()

        # - Filter the costs to keep only the minimum
        self.optimize_cost(method='min')

        # - Add the initial lineage if any
        if self.init_lineage_dict:
            self.lineage_dict.add_multiple_lineage(self.init_lineage_dict)

        log.info('Lineage estimated ! \n')

        return self.lineage_dict

    def flow_lineage(self):
        """Compute a lineage using the flow method

        Returns
        -------
        dict
        """
        log.info("Not implemented yet...")

        return None

    def eval_cost_from_overlap(self):
        """Evaluate the cost of the association between mother and daughter cells from volume overlapping

        Returns
        -------
        dict
            Cost dictionary ``{(mother_label, daughter_label): cost}``
        """

        # - Do not compute costs for cells in init_lineage_dict (should be correct --> from expert)
        if self.init_lineage_dict:
            log.info(f'Initial lineage was given with {len(self.init_lineage_dict)} associations')
            mother_labels = set(self.mother_seg_reg.labels()) - set(self.init_lineage_dict)
            daughter_labels = set(self.daughter_seg_reg.labels()) - set([item for sublist in self.init_lineage_dict.values()
                                                                         for item in sublist])
        else:
            mother_labels, daughter_labels = None, None

        # - Computing metrics (cells superposition): union(daughter, mother) / daughter
        overlap_dict = cell_overlap(self.mother_seg_reg, self.daughter_seg_reg,
                                    mother_label=mother_labels, daughter_label=daughter_labels,
                                    method='target_daughter', ds=self.ds, verbose=False)

        # - Calculate the cost according to the overlapping value
        self.cost_dict = {k: np.float16(1 - overlap) for k, overlap in overlap_dict.items()}

    def optimize_cost(self, method='min'):
        """Optimize the cost dict to find for each daughter cell label the optimal mother cell label

        Notes
        -----
        Accepted `method` may be:
         - 'min' just take the minimal cost for each daughter cell label
         - 'flow' use the maximal-flow minimal-cost algorithm to find the optimal cost for each daughter cell label

        Parameters
        ----------
        method: str
            define the method use to optimize the cost dict

        Returns
        -------
        dict
            Lineage dict {daughter_label : mother_label}
        """

        if method == 'min':
            # - Get the labels
            mother_ov_labels, daughter_ov_label = np.vstack(list(self.cost_dict.keys())).T
            costs = np.hstack(list(self.cost_dict.values()))

            # - For each daughter, get the mother label index for which the minimal cost was calculated
            daughter_best_index = nd.minimum_position(costs,
                                                      daughter_ov_label,
                                                      np.unique(daughter_ov_label))
            daughter_best_index = np.array(daughter_best_index).flatten()

            # - Format the lineage : {mother_label: daughter_label(s)}
            ancestor_lineage = dict(zip(daughter_ov_label[daughter_best_index],
                                        mother_ov_labels[daughter_best_index]))

            lineage_dict = {}
            for dlab, mlab in ancestor_lineage.items():
                if mlab in lineage_dict:
                    lineage_dict[mlab].append(dlab)
                else:
                    lineage_dict[mlab] = [dlab]

            self.lineage_dict = Lineage(lineage_dict)

            # - Store the costs
            mlab = np.unique(mother_ov_labels[daughter_best_index])
            mother_costs = nd.mean(costs[daughter_best_index],
                                   mother_ov_labels[daughter_best_index],
                                   mlab).astype('float16')
            self.lineage_cost = {'mother': dict(zip(mlab, mother_costs)),
                                 'daughter': dict(zip(daughter_ov_label[daughter_best_index],
                                                      costs[daughter_best_index]))}

        else:
            raise ValueError('No other method implemented!')

        return self.lineage_dict

    def estimate_trsf(self, blk_params=''):
        """ Automatic registration of two consecutive time-point images.
            If an initial transformation is available (typically, a rigid transformation), it will be used
            to initialize the procedure.
        """
        log.info(f"Automatic estimation of non-linear transformation in '{self.trsf_direction}' direction...")

        # If given `trsf_direction` is unknown, use default method:
        if self.trsf_direction not in ('backward', 'forward'):
            log.error(f"Unknown `trsf_direction` attribute value '{self.trsf_direction}'!")
            log.info("Valid `trsf_direction` attribute values are 'forward' or 'backward'!")
            self.trsf_direction = DEF_TRSF_DIRECTION
            log.info(f"Using default method: '{self.trsf_direction}'.")

        # Defines floating and reference images depending on registration temporal direction
        if self.trsf_direction == 'backward':
            floating_int, reference_int = self.daughter_img, self.mother_img
        else:
            floating_int, reference_int = self.mother_img, self.daughter_img

        # - Registration: rigid --> affine --> deformable (py-ll = 2, py-hl = 5)
        if self.init_trsf is not None:
            log.info(f"Automatic linear registration with initialisation...")
            # - If a initial (linear) transformation is given, use it to initialize the 'affine' registration
            lin_trsf = blockmatching(floating_image=floating_int, reference_image=reference_int, method='affine',
                                     init_trsf=self.init_trsf,
                                     pyramid_lowest_level=self.reg_lowest_level,
                                     pyramid_highest_level=self.reg_highest_level)

        else:
            log.info(f"Fully automatic linear registration...")
            # - rigid
            trsf_rigid = blockmatching(floating_image=floating_int, reference_image=reference_int, method='rigid',
                                       pyramid_lowest_level=self.reg_lowest_level,
                                       pyramid_highest_level=self.reg_highest_level)
            # - affine
            lin_trsf = blockmatching(floating_image=floating_int, reference_image=reference_int, method='affine',
                                     pyramid_lowest_level=self.reg_lowest_level,
                                     pyramid_highest_level=self.reg_highest_level)

        # - deformable
        log.info(f"Estimate non-linear transformation using linear transformation")
        trsf_deformable = blockmatching(floating_image=floating_int, reference_image=reference_int,
                                        method='vectorfield', init_trsf=lin_trsf,
                                        pyramid_lowest_level=self.reg_lowest_level,
                                        pyramid_highest_level=self.reg_highest_level-1,
                                        params=blk_params)
        self.trsf = trsf_deformable

    def make_nudge(self, nudge_lineage, method=None, blk_params=""):
        """ Use a dictionnary of expertized cell pairings to update a current lineage
        """
        if method is None:
            method = self.anchors_method
        
        # - Assert that a lineage is already computed
        if self.lineage_dict is None:
            log.warning('You need to compute a lineage prior the use of nudges')

        # - Assert that the given expertized lineage is correct (and coherent with previous expertized lineage given)
        lineage = Lineage(nudge_lineage)

        if self.init_lineage_dict: # non-empty ?
            self.init_lineage_dict.add_multiple_lineage(lineage) # will be added to the previous expertized lineage
        else:
            self.init_lineage_dict = lineage

        log.info(f'Improve the current trsf. and lineage using {len(nudge_lineage)} nudges (method={method}).')

        # - Get the anchors corresponding to the nudge_lineage
        mother_pts, daughter_pts = self.get_anchors_from_lineage(lineage=nudge_lineage, method=method)

        if self.adj_graph_mother is None: # if no adjacency graph exists
            log.info('Extract the adj. graph of mother image')
            self.adj_graph_mother = create_adj_graph(self.mother_seg, adj_weighted=False) # just get the topology information

        ### - Add other anchors points away from the nudges (help to maintain the lineage far away from nudges) - ###
        # - Get a subgraph where the background is removed (will help to calculate topological distance)
        M = self.adj_graph_mother.subgraph([lab for lab in self.mother_seg.labels() if lab != 1])

        # - Get the list of the cells that are localized at a topological distance inferior to 4 to any mother nudges
        topological_cell_dist = nx.multi_source_dijkstra_path_length(M, list(nudge_lineage), cutoff=self.max_topo_dist)

        # - Get the set of cells that are localized at a topological distance superior to 4 (or equal)
        mother_anchors = set(self.mother_seg.labels()) - set(topological_cell_dist)

        # - Get the current lineage of these cells (will be used to add additional anchors)
        other_anchors = {mlab: self.lineage_dict[mlab] for mlab in mother_anchors if (mlab in self.lineage_dict) and (mlab != 1)}

        # - Get barycenter of these additional anchors
        mother_anchors, daughter_anchors = self.get_anchors_from_lineage(lineage=other_anchors, method='centroids')

        # - Concatenate anchors
        mother_pts = np.vstack([mother_pts, mother_anchors])
        daughter_pts = np.vstack([daughter_pts, daughter_anchors])

        # - Estimate an incremental vector-field and compose with current transformation
        self.estimate_trsf_from_anchors(mother_pts, daughter_pts, incremental_trsf=True, blk_params=blk_params)

        # - Re-estimate the current lineage
        self.naive_lineage()

    # Should be move outside
    def get_anchors_from_lineage(self, lineage=None, method='centroids', remove_border=False):
        """ Get the anchors corresponding to a given lineage.
            Output order of spatial position is xyz (and real units).
        """
        # - Convert the input segmented image if needed
        if isinstance(self.mother_seg, LabelledImage):
            self.mother_seg = TissueImage3D(self.mother_seg, not_a_label=self.mother_seg.not_a_label, background=1)

        if isinstance(self.daughter_seg, LabelledImage):
            self.daughter_seg = TissueImage3D(self.daughter_seg, not_a_label=self.daughter_seg.not_a_label,
                                              background=1)

        # - Compute the cell properties
        mother_centroids = self.mother_seg.cells.barycenter(real=True)  # order xyz
        daughter_centroids = self.daughter_seg.cells.barycenter(real=True)  # order xyz
        daughter_volumes = self.daughter_seg.cells.volume(real=True)

        if lineage is None:
            lineage = self.init_lineage_dict

        if remove_border: # to test : maybe to remove (MANU)
            log.info('Do not use hc lineage at border...')
            from timagetk.components.labelled_image import labels_at_stack_margins
            border_cells = labels_at_stack_margins(self.mother_seg, voxel_distance_from_margin=1)
            lineage = {mlab: dlab for mlab, dlab in lineage.items() if mlab not in border_cells}

        if method == 'centroids':
            log.info('Extract barycenter anchors...')
            # - Get the hcf centroids (XYZ order)
            mother_pts = [mother_centroids[lab] for lab in list(lineage)]
            daughter_pts = []

            for mlab, dlab in lineage.items():
                if len(dlab) < 2:
                    daughter_pts.append(daughter_centroids[dlab[0]])
                else:
                    wc = np.sum([daughter_centroids[lab] * daughter_volumes[lab] for lab in dlab], axis=0)
                    wc = wc / sum([daughter_volumes[lab] for lab in dlab])
                    daughter_pts.append(wc)

        elif method == 'vertices': # TODO : to be re-implemented, procedure could be speed up (?). Also HC information is not available in CellTracking --> use 'true_dict' attribute (but ugly)
            log.info('Extract vertices anchors (can be quite long)...')
            # - Compute the vertices
            mother_vertices = self.mother_seg.pointel_coordinates(real=True, axes_order='xyz')
            daughter_vertices = self.daughter_seg.pointel_coordinates(real=True, axes_order='xyz')

            # - Prepare hc list if any
            hc_daughters = [dlab for dlabs in self.true_dict.values() for dlab in dlabs]

            mother_pts, daughter_pts = [], [] # init

            # - Loop over the lineage:
            for mlab, dlab in lineage.items():
                if mlab in self.matched_pts:
                    if set(dlab) == set(self.matched_pts[mlab]['lineage']):
                        # - do not recompute the anchors
                        mother_pts.extend(self.matched_pts[mlab]['mpts'])
                        daughter_pts.extend(self.matched_pts[mlab]['dpts'])
                        continue

                # - Use vertices only for specific associations : if L1 associations OR if HC associations at
                # frontier (some of the vertices are not HC). Else use centroids
                #log.info(f'Treat {mlab} --> {dlab}...')

                # - Get the vertices of these cells
                mv = {k: np.mean(v, axis=0) for k, v in mother_vertices.items() if mlab in k}
                dv = {k: np.mean(v, axis=0) for k, v in daughter_vertices.items() if len(set(dlab) & set(k)) > 0}

                # - Remove the inter-cells vertices
                if len(dlab) > 1:
                    dv = {k: v for k, v in dv.items() if len(set(k) & set(dlab)) < 2}

                mpts = np.vstack(list(mv.values()))
                dpts = np.vstack(list(dv.values()))

                # - check for background vertices
                tf_mbackground = ['1' if 1 in k else '0' for k in mv.keys()]
                tf_dbackground = ['1' if 1 in k else '0' for k in dv.keys()]

                # - check for hc vertices (ugly)
                current_hc_mother = [lab for lab in self.true_dict if lab != mlab]
                current_hc_daughter = [lab for lab in hc_daughters if lab not in dlab] # remove the current cells
                tf_mhc = ['1' if set(k) & set(current_hc_mother) else '0' for k in mv.keys()]
                tf_dhc = ['1' if set(k) & set(current_hc_daughter) else '0' for k in dv.keys()]

                # - convert in a unique group id
                mother_groups = np.array([int(tf1 + tf2, 2) for tf1, tf2 in zip(tf_mhc, tf_mbackground)])
                daughter_groups = np.array([int(tf1 + tf2, 2) for tf1, tf2 in zip(tf_dhc, tf_dbackground)])

                #log.info(f'Found mg = {np.unique(mother_groups)} and dg = {np.unique(daughter_groups)}')

                # - assert that all groups are listed in both side
                alone_group = set(mother_groups) ^ set(daughter_groups) # groups that only appear on one side

                if alone_group: # non-empty
                    #log.info('Detect alone groups!')
                    # - remove all the vertices that corresponds to these groups
                    for g in alone_group:
                        mpts = mpts[mother_groups != g, :]
                        mother_groups = mother_groups[mother_groups != g]

                        dpts = dpts[daughter_groups != g, :]
                        daughter_groups = daughter_groups[daughter_groups != g]

                # - check if only two groups are detected : convert group labels (to {0, 1})
                if len(np.unique(mother_groups)) == 2:
                    #log.info('Convert in binary')
                    # - assert that the groups are labelled 0 and 1
                    i = np.min(mother_groups)
                    mother_groups = np.array([0 if k == i else 1 for k in mother_groups])
                    daughter_groups = np.array([0 if k == i else 1 for k in daughter_groups])

                # - If only group is detected : use centroids else use vertices
                if len(np.unique(mother_groups)) == 1:
                    #log.info('Use centroids')
                    mother_pts.append(mother_centroids[mlab])

                    if len(dlab) < 2:
                        daughter_pts.append(daughter_centroids[dlab[0]])
                    else:
                        wc = np.sum([daughter_centroids[lab] * daughter_volumes[lab] for lab in dlab], axis=0)
                        wc = wc / sum([daughter_volumes[lab] for lab in dlab])
                        daughter_pts.append(wc)
                else:
                    #log.info('Use vertices')
                    mvertex_matched, dvertex_matched = vertex_assignement_problem(mpts, dpts,
                                                                                  init_trsf_method='similitude',
                                                                                   mother_group=mother_groups,
                                                                                  daughter_group=daughter_groups,
                                                                                   max_pairing_dist=30)

                    if mvertex_matched is not None:
                        mother_pts.extend(mvertex_matched)
                        daughter_pts.extend(dvertex_matched)

                        # - Store results for later
                        self.matched_pts[mlab] = {'lineage': dlab, 'mpts': mvertex_matched, 'dpts': dvertex_matched}

        else:
            log.error('Unknown method')
            return None, None

        return mother_pts, daughter_pts

    def estimate_trsf_from_anchors(self, mother_pts, daughter_pts,
                                   fs_coef=2.0, vpd_coef=1.0, vfd_coef=2.5, #fs_coef=3.5, vpd_coef=0.5, vfd_coef=2.5 --> jo
                                   incremental_trsf=False, blk_params=""):
        """ Re-estimate a registration of two consecutive time-point images based on anchors points.
            If incremental_trsf is True, it will calculate an incremental transformation to the current trsf.
            Input anchors points should be in real unit and ordered as (X, Y, Z).
        """

        # If given `trsf_direction` is unknown, use default method:
        if self.trsf_direction not in ('backward', 'forward'):
            log.error(f"Unknown `trsf_direction` attribute value '{self.trsf_direction}'!")
            log.info("Valid `trsf_direction` attribute values are 'forward' or 'backward'!")
            self.trsf_direction = DEF_TRSF_DIRECTION
            log.info(f"Using default method: '{self.trsf_direction}'.")

        # Defines floating and reference images depending on registration temporal direction
        if self.trsf_direction == 'backward':
            floating_int, reference_int = self.daughter_img, self.mother_img
            floating_pts, reference_pts = daughter_pts, mother_pts
        else:
            floating_int, reference_int = self.mother_img, self.daughter_img
            floating_pts, reference_pts = mother_pts, daughter_pts

        if incremental_trsf:
            # - Apply the current transformation to the floating points
            #   Warning: the trsf should be inverted
            floating_pts_reg = apply_trsf_to_points(floating_pts, inv_trsf(self.trsf, template_img=reference_int))

            # - Evaluate the median minimal distance that exists between two floating landmarks
            dist = squareform(pdist(floating_pts_reg))  # calculate the pairwise point distances
            dist = np.median(np.where(dist > 0, dist, np.inf).min(
                axis=0)) * self.dist_scale # get the median of the minimal non-zero distances for each pair of landmarks
            log.info(f'Minimal median distance between floating anchors : {dist} um')

            # - Estimate a non-linear transformation based on the anchors points
            init_vf_trsf = pointmatching(points_flo=floating_pts_reg, points_ref=reference_pts,
                                         template_img=reference_int, method='vectorfield',
                                         vector_propagation_distance=vpd_coef * dist,
                                         vector_fading_distance=vfd_coef * dist, fluid_sigma=fs_coef * dist)

            self.init_vf_trsf = compose_trsf([self.trsf, init_vf_trsf], template_img=reference_int)

            log.info(f"Update the non-linear registration using an incremental vector-field from {len(reference_pts)} anchors points")
            # Note : set the pyramid_highest_level to 3 : avoid to move too far from the initial vector-field
            trsf_deformable = blockmatching(floating_image=floating_int, reference_image=reference_int,
                                            method='vectorfield', init_trsf=compose_trsf([self.trsf, init_vf_trsf]),
                                            pyramid_lowest_level=self.pts_lowest_level,
                                            pyramid_highest_level=self.pts_lowest_level,
                                            params=blk_params)
            self.trsf = trsf_deformable
        else:
            # - Estimate a linear transformation based on the anchors points
            lin_trsf = pointmatching(points_flo=floating_pts, points_ref=reference_pts, method='affine')

            # - Apply the affine transformation on the floating points
            #   Note: Transformation are ordered as (X, Y, Z)
            inv_lin_trsf = inv_trsf(lin_trsf).get_array() # inverse trsf (go from flo --> ref intead of ref --> flo)
            floating_aff_pts = [np.dot(inv_lin_trsf, np.transpose(np.append(pts, 1)))[:-1]
                                for pts in floating_pts]

            # - Calculate the median minimal distance that exists between two floating landmarks (after affine registration)
            dist = squareform(pdist(floating_aff_pts)) # calculate the pairwise point distances
            dist = np.median(np.where(dist>0, dist, np.inf).min(axis=0)) * self.dist_scale # apply a given scaling if needed
            log.info(f'Minimal median distance between floating anchors : {dist} um')

            # - Estimate a non-linear transformation using the residual of list of given points
            init_vf_trsf = pointmatching(points_flo=floating_aff_pts, points_ref=reference_pts,
                                     template_img=reference_int, method='vectorfield',
                                     vector_propagation_distance=vpd_coef * dist,
                                     vector_fading_distance=vfd_coef * dist, fluid_sigma=fs_coef * dist)

            self.init_vf_trsf = compose_trsf([lin_trsf, init_vf_trsf], template_img=reference_int)

            log.info(f"Non-linear registration using the initial vector-field from {len(reference_pts)} anchors points")
            trsf_deformable = blockmatching(floating_image=floating_int, reference_image=reference_int,
                                            method='vectorfield', init_trsf=compose_trsf([lin_trsf, init_vf_trsf],
                                            template_img=reference_int),
                                            pyramid_lowest_level=self.pts_lowest_level,
                                            pyramid_highest_level=self.pts_highest_level,
                                            params=blk_params)

            self.trsf = trsf_deformable

    def _auto_segment(self):
        # - Segmentation (best parameters : sigma=3.0 or 4.0 / 20>h>3)
        log.info("Automatic segmentation of intensity image...")
        raise ValueError("Problem: not implemented yet! Do the segmentation step prior the tracking!")

    def init_lineage(self):
        """Required parameters : At least two Spatial images (intensity) or
        at least two Labelled images (segmented) and a transformation

        Examples
        --------
        >>> from timagetk.third_party.ctrl.tracking import CellTracking
        >>> from timagetk.components.labelled_image import LabelledImage
        >>> from timagetk.third_party.ctrl.util import shared_data
        >>> from timagetk.io import imread
        >>> mother_int_path = shared_data('sphere_membrane_t0.inr.gz')
        >>> daughter_int_path = shared_data('sphere_membrane_t1.inr.gz')
        >>> mother_seg_path = shared_data('sphere_membrane_t0_seg.inr.gz')
        >>> daughter_seg_path = shared_data('sphere_membrane_t1_seg.inr.gz')
        >>> mother_int_img = imread(mother_int_path)
        >>> daughter_int_img = imread(daughter_int_path)
        >>> mother_seg_img = LabelledImage(imread(mother_seg_path), not_a_label=0)
        >>> daughter_seg_img = LabelledImage(imread(daughter_seg_path), not_a_label=0)
        >>> ct = CellTracking(mother_int_img, daughter_int_img, mother_seg_img, daughter_seg_img)
        >>> ct.init_lineage()

        >>> from timagetk.algorithms.trsf import apply_trsf
        >>> from timagetk.visu.mplt import grayscale_imshow
        >>> imgs = [ct.mother_img, ct.daughter_img, ct.mother_img, apply_trsf(ct.daughter_img, ct.trsf, interpolation="linear")]
        >>> titles = ["Reference", "Floating", "Reference", "Registered floating"]
        >>> grayscale_imshow(imgs, slice_id=110, suptitle="Blockmatching registration", title=titles, val_range=[0, 255], max_per_line=2)
        >>> titles = ["Reference", "Floating", "Registered reference", "Registered floating"]
        >>> imgs = [ct.mother_seg, ct.daughter_seg, ct.mother_seg_reg, ct.daughter_seg_reg]
        >>> grayscale_imshow(imgs, slice_id=110, suptitle="Blockmatching registration", val_range='auto', cmap='viridis', title=titles, max_per_line=2)
        """
        log.info("Lineage pre-processing: segmentation and registration...")

        # - Check if automatic segmentation should be performed
        if (self.mother_seg is None) or (self.daughter_seg is None):
            log.warning("Missing segmented image required for lineage computation!")
            self._auto_segment()
        else:
            log.info('Using provided segmented image for lineage computation.')

        # - Check if automatic registration should be performed (if no trsf)
        if self.trsf is None:
            try:
                assert self.mother_img is not None
                assert self.daughter_img is not None
            except:
                raise ValueError('Both intensity images are required to estimate lineage!')
            log.warning("Missing transformation object required for lineage computation!")

            if self.init_lineage_dict: # check if non-empty
                mother_pts, daughter_pts = self.get_anchors_from_lineage(method=self.anchors_method)
                self.estimate_trsf_from_anchors(mother_pts, daughter_pts, incremental_trsf=False)
            else:
                self.estimate_trsf()

        else:
            log.info('Using provided transformation object for lineage computation.')

        # - Apply the transformation to the floating image (floating image)
        if self.trsf_direction == 'backward':
            log.info('Backward temporal registration: applying estimated transformation to the descendant image...')
            self.daughter_seg_reg = apply_trsf(self.daughter_seg, self.trsf, template_img=self.mother_seg,
                                               interpolation='cellbased', cell_based_sigma=1, quiet=True)

            # change the not_a_label to avoid ignoring this label (for overlapping, neighbors,...)
            #self.daughter_seg_reg[self.daughter_seg_reg == 0] = np.max(self.daughter_seg_reg.get_array()) + 1 #ugly but output of apply_trsf is SpatialImage

            # - Apply the identity transformation to the daughter image (reference image) to smooth it like the mother one
            log.info('Applying identity transformation to the ancestor image...')
            self.mother_seg_reg = apply_trsf(self.mother_seg, template_img=self.mother_seg, interpolation='cellbased',
                                             cell_based_sigma=1, quiet=True)
        else:
            log.info("Forward temporal registration: applying estimated transformation to the ancestor image...")
            self.mother_seg_reg = apply_trsf(self.mother_seg, self.trsf, template_img=self.daughter_seg,
                                             interpolation='cellbased', cell_based_sigma=1, quiet=True)

            # change the not_a_label to avoid ignoring this label (for overlapping, neighbors,...)
           # self.mother_seg_reg[self.mother_seg_reg == 0] = np.max(self.mother_seg_reg.get_array()) + 1 #ugly but output of apply_trsf is SpatialImage

            # - Apply the identity transformation to the daughter image (reference image) to smooth it like the mother one
            log.info('Applying identity transformation to the descendant image...')
            self.daughter_seg_reg = apply_trsf(self.daughter_seg, template_img=self.daughter_seg,
                                               interpolation='cellbased', cell_based_sigma=1, quiet=True)

# - overwrite the Robust method just for the moment...
class IterativeCellTracking(CellTracking):
    """ Computes lineages by tracking cells between two segmented images using an iterative approach.

    The method finds a mapping between two sets of cells using an iterative process
    that alternates between estimating spatial transformations and cell mappings until
    convergence [1]

    [1] Petit, M., Cerutti, G., Godin, C., & Malandain, G. (2022, March). Robust Plant Cell Tracking in Fluorescence
        Microscopy 3D+ T Series. In 2022 IEEE 19th International Symposium on Biomedical Imaging (ISBI) (pp. 1-4). IEEE.


    Attributes
    ----------
    aggregation_fraction : float
        Fraction of novel high-confidence pairing to aggregate at each iteration.
    min_number_hcf_adj : float
        Minimum number of neighbors high-confidence pairing (HCF) to be added to the high-confidence set.
    init_hcf_thresh : float
        Initial threshold value for defining high-confidence set during the tracking initialization.
    ignore_background : bool
        Boolean flag to indicate whether background labels should be ignored in the cell mapping evaluation.
    ignore_missing : bool
        Boolean flag that, if true, enables ignoring missing labels in the cell mapping evaluation.
    ignore_no_label_id : bool
        Boolean flag that, if true, excludes entries without a label identifier in the cell mapping evaluation.
    save_track_file : str
        File path where tracking results will be saved during or after the completion of the process.
    track : dict
        Dictionary storing the tracking results, where keys typically represent time points or label identifiers,
        and values contain tracking data.
    blk_iter_params : str
        Parameters for advanced configuration of the blockmatching algorithm. See `params` in
        `timagetk.algorithms.blockmatching` for more information.
    max_iter : int
        Maximum number of iterations allowed for the iterative tracking algorithm before termination.
    niter : int
        Current number of iterations that have been executed in the iterative tracking process.
    preservation_score : dict
        Dictionary mapping mother cell labels to their lineage quality scores.
    previous_lineage : dict
        Previously established lineage data that can be used to seed the current tracking process for better accuracy.
    hc_lineage : dict
        High-confidence lineage data used to guide the iterative refinement steps during tracking.

    """
    def __init__(self, mother_img, daughter_img, mother_seg=None, daughter_seg=None, trsf=None, **kwargs):
        """Constructor

        Parameters
        ----------
        mother_img : timagetk.SpatialImage, optional
            Intensity image at time t. If given, the other intensity image should be given.
        daughter_img : timagetk.SpatialImage, optional
            Intensity image at time t+dt. If given, the other intensity image should be given.
        mother_seg : timagetk.LabelledImage, optional
            Segmented image at time t. If given, the other segmented image should be given.
        daughter_seg : timagetk.LabelledImage, optional
            Segmented image at time t+dt. If given, the other segmented image should be given.
        trsf : timagetk.Trsf, optional
            Transformation that register the daughter image on the mother image if `trsf_direction == 'backward'` (default).
            If `trsf_direction == 'forward'`, register the mother image on the daughter image.
            Warning: if only ``LabelledImage`` are given, the transformation is compulsory.

        Other Parameters
        ----------------
        init_trsf : timagetk.Trsf
            An initial linear transformation that register the daughter image on the mother image.
            Further improved by a round of blockmatching. Requires the intensity images.
            Typically, a transformation obtained from ``pointmatching``.
        init_lineage_dict : Lineage
            A previous lineage or expert lineage used to initialise non-linear registration.
        ds : int
            Down-sampling coefficient for image overlap evaluation. Used by the naive lineage method.
        max_cost : float
            Threshold for the cost value, if the value is higher than the couple is deleted from the lineage dictionary.
            Used by the flow lineage method.
        sigma : float
            Parameter for intensity image gaussian filtering.
        hmin : int
            Height value to use in h-extrema transformation.
            Used by automatic watershed method when segmented image are missing.
        trsf_direction : {'backward', 'forward'}
            Defines the temporal direction of initial transformation and estimated registration transformation.
            "backward" lineaging (default), transformation resample daughter image into mother reference frame
            "forward" lineaging, transformation resample mother image into daughter reference frame
        aggregation_fraction : float
            Fraction of novel high-confidence pairing to aggregate at each iteration.
        min_number_hcf_adj : float
            Minimum number of neighbors high-confidence pairing (HCF) to be added to the high-confidence set.
        init_hcf_thresh : float
            Initial threshold value for defining high-confidence set during the tracking initialization.
        ignore_background : bool
            Boolean flag to indicate whether background labels should be ignored in the cell mapping evaluation.
        ignore_missing : bool
            Boolean flag that, if true, enables ignoring missing labels in the cell mapping evaluation.
        ignore_no_label_id : bool
            Boolean flag that, if true, excludes entries without a label identifier in the cell mapping evaluation.
        save_track_file : str
            File path where tracking results will be saved during or after the completion of the process.
        track : dict
            Dictionary storing the tracking results, where keys typically represent time points or label identifiers,
            and values contain tracking data.

        Examples
        --------
        >>> from timagetk.third_party.ctrl.tracking import IterativeCellTracking
        >>> from timagetk.components.labelled_image import LabelledImage
        >>> from timagetk.io.dataset import shared_data
        >>> mother_int_img = shared_data('flower_confocal', 0)
        >>> daughter_int_img = shared_data('flower_confocal', 1)
        >>> mother_seg_img = shared_data('flower_labelled', 0)
        >>> daughter_seg_img = shared_data('flower_labelled', 1)
        >>> ict = IterativeCellTracking(mother_int_img, daughter_int_img, mother_seg_img, daughter_seg_img, max_iter=5)
        >>> ict.iterative_naive_tracking()
        """
        CellTracking.__init__(self, mother_img=mother_img, daughter_img=daughter_img,
                              mother_seg=mother_seg, daughter_seg=daughter_seg,
                              trsf=trsf, **kwargs)

        self.aggregation_fraction = kwargs.get('aggregation_fraction', 0.05)
        self.min_number_hcf_adj = kwargs.get('min_number_hcf_adj', 3)
        self.init_hcf_thresh = kwargs.get('init_hcf_thresh', 0.8)
        self.min_hcf_thresh = kwargs.get('min_hcf_thresh', 0)

        self.ignore_background = kwargs.get('ignore_background', False)
        self.ignore_missing = kwargs.get('ignore_missing', False)
        self.ignore_no_label_id = kwargs.get('ignore_no_label_id', True)

        self.blk_iter_params = kwargs.get('blk_iter_params', '')
        self.max_iter = kwargs.get('max_iter', int(1/self.aggregation_fraction))

        self.niter = 0
        self.preservation_score = {}
        self.previous_lineage = {}
        self.hc_lineage = {}

        # filepath to save the tracking process. If the file already exists, the tracking starts from the last iteration
        self.track_file = kwargs.get('track_file', None)

        self._init_tracking_from_file() # to update the tracking object if 'track_file' exists

    def evaluate_lineage(self):
        """Evaluate the lineage dictionary, if any.

        Notes
        -----
        Update attributes ``adj_graph_mother``, ``adj_graph_daughter`` & ``preservation_score``.
        """
        # - Check if there is an available lineage
        if self.lineage_dict is None:
            log.warning('You need to get a lineage to evaluate it!')
            return None

        # - Check if adjacency graph are already constructed
        if (self.adj_graph_mother is None) or (self.adj_graph_daughter is None):
            self.adj_graph_mother, self.adj_graph_daughter = create_lineage_graph(self.mother_seg,
                                                                                  self.daughter_seg,
                                                                                  resample_vox=0.5) # use voxelsize 0.5

        # - Filter the current lineage (assert that 1) daughters form a single connected-component and 2) volume of
        #   descendant is not decreased too much according to the ascendant volume)
        filtered_lineage = filter_mapping(self.lineage_dict,
                                          self.adj_graph_mother, self.adj_graph_daughter, ratio_vol=0.4)

        # - Modify the adjacency graph to fit with the current lineage
        log.info('Calculate the preservation score (lineage quality index)...')
        M_mapping, D_mapping = get_mapping_graph(lineage_dict=filtered_lineage,
                                                 M=self.adj_graph_mother,
                                                 D=self.adj_graph_daughter)

        # - Calculate the lineage quality score
        preservation_score = analyze_graph_mapping(M_mapping=M_mapping, D_mapping=D_mapping,
                                                   ignore_background=self.ignore_background,
                                                   ignore_missing=self.ignore_missing,
                                                   ignore_no_label_id=self.ignore_no_label_id)

        self.preservation_score = preservation_score
        return

    def update_hc_lineage(self):
        """
        """
        log.info('Update the current HC lineage...')

        # - According to the current lineage, high-confidence lineage and lineage quality score --> update the high-confidence lineage
        preservation_dict = self.preservation_score.copy()

        # - set the initial lineage (if any) to the maximal preservation : it will not be updated
        for mlab in self.init_lineage_dict:
            preservation_dict[mlab] = 1

        # - Is it an aggregation of an initialization of the HC lineage ?
        if self.hc_lineage: # test if hc_lineage is non-empty
            M = aggregate_hc(self.adj_graph_mother, preservation_dict, self.lineage_dict,
                             min_add=self.aggregation_fraction, min_thresh=self.min_hcf_thresh,
                             initial_thresh=self.init_hcf_thresh, Nminadj=self.min_number_hcf_adj)
        else:
            M = initialize_hc(self.adj_graph_mother, preservation_dict, self.lineage_dict,
                             initial_thresh=self.init_hcf_thresh, Nminadj=self.min_number_hcf_adj)

        # - Store the new hc_lineage and updated adjacency mother graph
        self.hc_lineage = Lineage(dict(nx.get_node_attributes(M, 'hcf_lineage')))
        self.adj_graph_mother = M

        return

    def update_trsf(self):
        # - Re-estimate the temporal registration transformation using HC lineage as anchors

        if self.anchors_method == 'vertices':
            self.true_dict = self.hc_lineage.copy() # for now (to pass the current hc list to CellTracking)

        # - Get the anchors points (hc barycenters)
        mother_pts, daughter_pts = self.get_anchors_from_lineage(lineage=self.hc_lineage,
                                                                 method=self.anchors_method)

        # - Re-estimate the transformation
        self.estimate_trsf_from_anchors(mother_pts=mother_pts, daughter_pts=daughter_pts,
                                        fs_coef=1.5, vpd_coef=2.0, vfd_coef=2.0,
                                        incremental_trsf=False, blk_params=self.blk_iter_params)
        return

    def iterative_naive_tracking(self):

        log.info(f'\n\n Iteration n°{self.niter} \n\n')

        lineage_dict = self.naive_lineage() # - build a naive lineage

        # - stop the iterative procedure when the current lineage is the same as previously
        if self.previous_lineage == self.lineage_dict:
            log.info(f'The lineage has not evolved between two iterations, END the procedure...')
            return self.lineage_dict

        self.previous_lineage = lineage_dict.copy_lineage() # - store the current lineage for the next iteration

        # - evaluate the lineage (and build the adjacency graph)
        self.evaluate_lineage()
        log.info(f'Mean adjacency score: {np.mean(list(self.preservation_score.values()))}\n')

        self.update_hc_lineage()  # - initialize the HC lineage

        if self.track_file is not None:
            self._update_tracking_file() # update the track file is any

        self.update_trsf()  # - update the trsf using HC lineage as anchors (cell barycenter of associations)

        # - start the iterative procedure
        self.niter += 1
        while (self.niter != self.max_iter):
            log.info(f'\n\n Iteration n°{self.niter} \n\n')
            lineage_dict = self.naive_lineage() # - build a naive lineage

            # - stop the iterative procedure when the current lineage is the same as previously
            if self.previous_lineage == self.lineage_dict:
                log.info(f'The lineage has not evolved between two iterations, END the procedure...')
                break

            self.previous_lineage = lineage_dict.copy_lineage() # - store the current lineage

            self.evaluate_lineage()  # - evaluate the lineage
            log.info(f'Mean adjacency score: {np.mean(list(self.preservation_score.values()))}\n')

            self.update_hc_lineage() # - aggregate and modify current HC lineage

            if self.track_file is not None:
                self._update_tracking_file() # update the track file is any

            self.update_trsf() # - update the trsf

            self.niter += 1

        return self.lineage_dict

    def _read_track_file(self):
        # - test the existence of the file
        if not os.path.exists(self.track_file):
            log.info(f'Create a track file at {self.track_file}')
            # - Create it
            f = open(self.track_file, 'wb')
            pickle.dump({}, f) # empty file
            f.close()

        # - read the file
        f = open(self.track_file, 'rb')
        track_data = pickle.load(f)
        f.close()

        return track_data

    def _write_track_file(self, track_data):
        # - write the file
        f = open(self.track_file, 'wb')
        pickle.dump(track_data, f)
        f.close()

        return

    def _init_tracking_from_file(self):
        # - Test if a track file exists
        if self.track_file is not None:
            track_data = self._read_track_file()

            if track_data: # non-empty ?
                log.info(f'Found a previous track file, start from the last iteration!')

                # - update the tracking objet
                N = max(track_data) # last iteration
                self.hc_lineage = track_data[N]['hc_lineage']
                self.lineage_dict = track_data[N]['hc_lineage']
                self.lineage_cost = track_data[N]['lineage_cost']
                self.preservation_score = track_data[N]['preservation_score']
                self.init_lineage_dict = track_data[N]['init_lineage_dict']
                self.previous_lineage = track_data[N]['previous_lineage']

                # - get the adjacency graphs
                self.adj_graph_mother, self.adj_graph_daughter = create_lineage_graph(self.mother_seg,
                                                                                      self.daughter_seg,
                                                                                      resample_vox=0.5)

                # - fill the adjacency graphs
                nx.set_node_attributes(self.adj_graph_mother, {lab: 1 if lab in  self.hc_lineage else 0
                                                               for lab in self.adj_graph_mother.nodes()}, 'hcf')
                nx.set_node_attributes(self.adj_graph_mother, {lab: score for lab, score
                                                               in track_data[N]['hc_score'].items()},
                                       'adjacency_score')
                nx.set_node_attributes(self.adj_graph_mother, {mlab: dlab for mlab, dlab in self.hc_lineage.items()},
                                       'hcf_lineage')

                # - re-estimate a trsf from the hc
                self.update_trsf()  # - update the trsf

                self.niter = N + 1 # start at the next iteration

        return

    def _update_tracking_file(self):
        # - Get the file
        track_data = self._read_track_file()

        # - Store the current information
        track_data[self.niter] = {'hc_lineage': self.hc_lineage, 'lineage_dict': self.lineage_dict,
                                  'lineage_cost': self.lineage_cost, 'preservation_score': self.preservation_score,
                                  'init_lineage_dict': self.init_lineage_dict, 'previous_lineage': self.previous_lineage,
                                  'hc_score': dict(nx.get_node_attributes(self.adj_graph_mother, 'adjacency_score'))}

        # - Save the file
        self._write_track_file(track_data)

        return